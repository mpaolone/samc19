
//This file has MANY functions that were auto-generated from SNAKE simulation.  At the end of this file, the interpolation functions (linear) are defined.

#include "snake/fp.h"
#include "snake/rev.h"
#include "snake/q1en_1.h"
#include "snake/q1en_2.h"
#include "snake/q1ex.h"
#include "snake/q1q2_1.h"
#include "snake/q1q2_2.h"
#include "snake/q1q2_3.h"
#include "snake/q2en.h"
#include "snake/q2ex_1.h"
#include "snake/q2ex_2.h"
#include "snake/qd.h"
#include "snake/predent.h"
#include "snake/dent.h"
#include "snake/dext.h"
#include "snake/dq.h"
#include "snake/q3en.h"
#include "snake/q3ex_1.h"
#include "snake/q3ex_2.h" //old q3ex
#include "snake/old.h" //old J. LeRose functions

class SAMCSnake{
  const float xin = 0;
  const float yin = 0;
  const float thin = 0;
  const float phin = 0;
  const float dpin = 0;
  const float centp = 0;
  int msize = 5;
  //int m = 5;
  //vector<float> x;
  float matrix[5]={0};
  const float q1r = 14.92; //hardcoded Q1 radius, rot now;
  const float q2r = 30.0; //hardcoded Q2 radius, rot now;
  const float q3r = 30.0; //hardcoded Q1 radius, rot now;
  const float dextR = 46.188; //dipole exit R
  const float dextM = -0.01610808002; //dipole exit slope
  const float dxmin = -522.088; //dipole entrance xmin
  const float dxmax = -498.099; //dipole entrance xmax
  const float dint = 11.756; //dipole entrance intercept
  const float dslope = -0.06202842969; //dipole entrance slope
  float Lq3x_off = 3.0; //offset for Left arm Q3x
  
  const float DXOld = 40.0;
  const float DYOld = 12.5;

  float pv[19] = {100.0, 125.0, 150.0, 175.0, 200.0, 250.0, 300.0, 350.0, 400.0, 449.0, 450.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1200.0};

  public:

  float q1en_1_x=1e6;
  float q1en_2_x=1e6;
  float q1ex_x=1e6;
  float q1q2_1_x=1e6;
  float q1q2_2_x=1e6;
  float q1q2_3_x=1e6;
  float q2en_x=1e6;
  float q2ex_1_x=1e6;
  float q2ex_2_x=1e6;
  float qd_x=1e6;
  float predent_x=1e6;
  float dent_x=1e6;
  float dext_x=1e6;
  float dq_x=1e6;
  float q3en_x=1e6;
  float q3ex_1_x=1e6;
  float q3ex_2_x=1e6;

  float q1en_1_y=1e6;
  float q1en_2_y=1e6;
  float q1ex_y=1e6;
  float q1q2_1_y=1e6;
  float q1q2_2_y=1e6;
  float q1q2_3_y=1e6;
  float q2en_y=1e6;
  float q2ex_1_y=1e6;
  float q2ex_2_y=1e6;
  float qd_y=1e6;
  float predent_y=1e6;
  float dent_y=1e6;
  float dext_y=1e6;
  float dq_y=1e6;
  float q3en_y=1e6;
  float q3ex_1_y=1e6;
  float q3ex_2_y=1e6;

  float q3ex_2_t=1e6;
  float q3ex_2_p=1e6;

  SAMCSnake(float centp1, float x1, float y1, float th1, float ph1, float dp1): centp(centp1), xin(x1), yin(y1), thin(th1), phin(ph1), dpin(dp1){

	matrix[0]=xin/100.0;
	matrix[1]=thin;
	matrix[2]=yin/100.0;
	matrix[3]=phin;
	matrix[4]=dpin;
	if(centp < 450){
		 Lq3x_off = 5.0;
	 }
  };
  

  float x_q1en_1(){
	if(centp < pv[0]){
	  return x_e_q1en_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1en_1_125(matrix,msize) -  x_e_q1en_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1en_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1en_1_150(matrix,msize) -  x_e_q1en_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1en_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1en_1_175(matrix,msize) -  x_e_q1en_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1en_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1en_1_200(matrix,msize) -  x_e_q1en_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1en_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1en_1_250(matrix,msize) -  x_e_q1en_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1en_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1en_1_300(matrix,msize) -  x_e_q1en_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1en_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1en_1_350(matrix,msize) -  x_e_q1en_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1en_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1en_1_400(matrix,msize) -  x_e_q1en_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1en_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1en_1_449(matrix,msize) -  x_e_q1en_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1en_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1en_1_500(matrix,msize) -  x_e_q1en_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1en_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1en_1_600(matrix,msize) -  x_e_q1en_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1en_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1en_1_700(matrix,msize) -  x_e_q1en_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1en_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1en_1_800(matrix,msize) -  x_e_q1en_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1en_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1en_1_900(matrix,msize) -  x_e_q1en_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1en_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1en_1_1000(matrix,msize) -  x_e_q1en_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1en_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1en_1_1100(matrix,msize) -  x_e_q1en_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1en_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1en_1_1200(matrix,msize) -  x_e_q1en_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1en_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1en_1_1200(matrix,msize);
  };
  float y_q1en_1(){
	if(centp < pv[0]){
	  return y_e_q1en_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1en_1_125(matrix,msize) -  y_e_q1en_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1en_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1en_1_150(matrix,msize) -  y_e_q1en_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1en_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1en_1_175(matrix,msize) -  y_e_q1en_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1en_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1en_1_200(matrix,msize) -  y_e_q1en_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1en_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1en_1_250(matrix,msize) -  y_e_q1en_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1en_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1en_1_300(matrix,msize) -  y_e_q1en_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1en_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1en_1_350(matrix,msize) -  y_e_q1en_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1en_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1en_1_400(matrix,msize) -  y_e_q1en_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1en_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1en_1_449(matrix,msize) -  y_e_q1en_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1en_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1en_1_500(matrix,msize) -  y_e_q1en_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1en_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1en_1_600(matrix,msize) -  y_e_q1en_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1en_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1en_1_700(matrix,msize) -  y_e_q1en_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1en_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1en_1_800(matrix,msize) -  y_e_q1en_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1en_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1en_1_900(matrix,msize) -  y_e_q1en_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1en_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1en_1_1000(matrix,msize) -  y_e_q1en_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1en_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1en_1_1100(matrix,msize) -  y_e_q1en_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1en_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1en_1_1200(matrix,msize) -  y_e_q1en_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1en_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1en_1_1200(matrix,msize);
  };
  bool passQ1ent_1(){
	float x = x_q1en_1()*100.0;
	float y = y_q1en_1()*100.0;
	q1en_1_x = x;
	q1en_1_y = y;
	if(x!=x || y!=y || x*x + y*y > q1r*q1r){
	  return false;
	}
	return true;
  };
  float x_q1en_2(){
	if(centp < pv[0]){
	  return x_e_q1en_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1en_2_125(matrix,msize) -  x_e_q1en_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1en_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1en_2_150(matrix,msize) -  x_e_q1en_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1en_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1en_2_175(matrix,msize) -  x_e_q1en_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1en_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1en_2_200(matrix,msize) -  x_e_q1en_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1en_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1en_2_250(matrix,msize) -  x_e_q1en_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1en_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1en_2_300(matrix,msize) -  x_e_q1en_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1en_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1en_2_350(matrix,msize) -  x_e_q1en_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1en_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1en_2_400(matrix,msize) -  x_e_q1en_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1en_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1en_2_449(matrix,msize) -  x_e_q1en_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1en_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1en_2_500(matrix,msize) -  x_e_q1en_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1en_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1en_2_600(matrix,msize) -  x_e_q1en_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1en_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1en_2_700(matrix,msize) -  x_e_q1en_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1en_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1en_2_800(matrix,msize) -  x_e_q1en_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1en_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1en_2_900(matrix,msize) -  x_e_q1en_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1en_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1en_2_1000(matrix,msize) -  x_e_q1en_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1en_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1en_2_1100(matrix,msize) -  x_e_q1en_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1en_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1en_2_1200(matrix,msize) -  x_e_q1en_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1en_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1en_2_1200(matrix,msize);
  };
  float y_q1en_2(){
	if(centp < pv[0]){
	  return y_e_q1en_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1en_2_125(matrix,msize) -  y_e_q1en_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1en_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1en_2_150(matrix,msize) -  y_e_q1en_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1en_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1en_2_175(matrix,msize) -  y_e_q1en_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1en_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1en_2_200(matrix,msize) -  y_e_q1en_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1en_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1en_2_250(matrix,msize) -  y_e_q1en_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1en_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1en_2_300(matrix,msize) -  y_e_q1en_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1en_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1en_2_350(matrix,msize) -  y_e_q1en_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1en_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1en_2_400(matrix,msize) -  y_e_q1en_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1en_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1en_2_449(matrix,msize) -  y_e_q1en_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1en_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1en_2_500(matrix,msize) -  y_e_q1en_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1en_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1en_2_600(matrix,msize) -  y_e_q1en_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1en_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1en_2_700(matrix,msize) -  y_e_q1en_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1en_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1en_2_800(matrix,msize) -  y_e_q1en_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1en_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1en_2_900(matrix,msize) -  y_e_q1en_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1en_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1en_2_1000(matrix,msize) -  y_e_q1en_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1en_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1en_2_1100(matrix,msize) -  y_e_q1en_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1en_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1en_2_1200(matrix,msize) -  y_e_q1en_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1en_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1en_2_1200(matrix,msize);
  };
  bool passQ1ent_2(){
	float x = x_q1en_2()*100.0;
	float y = y_q1en_2()*100.0;
	q1en_2_x = x;
	q1en_2_y = y;
	if(x!=x || y!=y || x*x + y*y > q1r*q1r){
	  return false;
	}
	return true;
  };
  float x_q1ex(){
	if(centp < pv[0]){
	  return x_e_q1ex_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1ex_125(matrix,msize) -  x_e_q1ex_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1ex_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1ex_150(matrix,msize) -  x_e_q1ex_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1ex_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1ex_175(matrix,msize) -  x_e_q1ex_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1ex_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1ex_200(matrix,msize) -  x_e_q1ex_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1ex_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1ex_250(matrix,msize) -  x_e_q1ex_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1ex_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1ex_300(matrix,msize) -  x_e_q1ex_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1ex_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1ex_350(matrix,msize) -  x_e_q1ex_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1ex_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1ex_400(matrix,msize) -  x_e_q1ex_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1ex_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1ex_449(matrix,msize) -  x_e_q1ex_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1ex_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1ex_500(matrix,msize) -  x_e_q1ex_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1ex_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1ex_600(matrix,msize) -  x_e_q1ex_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1ex_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1ex_700(matrix,msize) -  x_e_q1ex_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1ex_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1ex_800(matrix,msize) -  x_e_q1ex_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1ex_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1ex_900(matrix,msize) -  x_e_q1ex_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1ex_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1ex_1000(matrix,msize) -  x_e_q1ex_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1ex_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1ex_1100(matrix,msize) -  x_e_q1ex_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1ex_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1ex_1200(matrix,msize) -  x_e_q1ex_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1ex_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1ex_1200(matrix,msize);
  };
  float y_q1ex(){
	if(centp < pv[0]){
	  return y_e_q1ex_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1ex_125(matrix,msize) -  y_e_q1ex_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1ex_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1ex_150(matrix,msize) -  y_e_q1ex_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1ex_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1ex_175(matrix,msize) -  y_e_q1ex_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1ex_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1ex_200(matrix,msize) -  y_e_q1ex_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1ex_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1ex_250(matrix,msize) -  y_e_q1ex_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1ex_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1ex_300(matrix,msize) -  y_e_q1ex_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1ex_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1ex_350(matrix,msize) -  y_e_q1ex_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1ex_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1ex_400(matrix,msize) -  y_e_q1ex_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1ex_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1ex_449(matrix,msize) -  y_e_q1ex_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1ex_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1ex_500(matrix,msize) -  y_e_q1ex_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1ex_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1ex_600(matrix,msize) -  y_e_q1ex_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1ex_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1ex_700(matrix,msize) -  y_e_q1ex_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1ex_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1ex_800(matrix,msize) -  y_e_q1ex_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1ex_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1ex_900(matrix,msize) -  y_e_q1ex_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1ex_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1ex_1000(matrix,msize) -  y_e_q1ex_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1ex_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1ex_1100(matrix,msize) -  y_e_q1ex_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1ex_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1ex_1200(matrix,msize) -  y_e_q1ex_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1ex_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1ex_1200(matrix,msize);
  };
  bool passQ1ext(){
	float x = x_q1ex()*100.0;
	float y = y_q1ex()*100.0;
	q1ex_x = x;
	q1ex_y = y;
	if(x!=x || y!=y || x*x + y*y > q1r*q1r){
	  return false;
	}
	return true;
  };
  float x_q1q2_1(){
	if(centp < pv[0]){
	  return x_e_q1q2_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1q2_1_125(matrix,msize) -  x_e_q1q2_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1q2_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1q2_1_150(matrix,msize) -  x_e_q1q2_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1q2_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1q2_1_175(matrix,msize) -  x_e_q1q2_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1q2_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1q2_1_200(matrix,msize) -  x_e_q1q2_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1q2_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1q2_1_250(matrix,msize) -  x_e_q1q2_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1q2_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1q2_1_300(matrix,msize) -  x_e_q1q2_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1q2_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1q2_1_350(matrix,msize) -  x_e_q1q2_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1q2_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1q2_1_400(matrix,msize) -  x_e_q1q2_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1q2_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1q2_1_449(matrix,msize) -  x_e_q1q2_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1q2_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1q2_1_500(matrix,msize) -  x_e_q1q2_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1q2_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1q2_1_600(matrix,msize) -  x_e_q1q2_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1q2_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1q2_1_700(matrix,msize) -  x_e_q1q2_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1q2_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1q2_1_800(matrix,msize) -  x_e_q1q2_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1q2_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1q2_1_900(matrix,msize) -  x_e_q1q2_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1q2_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1q2_1_1000(matrix,msize) -  x_e_q1q2_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1q2_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1q2_1_1100(matrix,msize) -  x_e_q1q2_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1q2_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1q2_1_1200(matrix,msize) -  x_e_q1q2_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1q2_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1q2_1_1200(matrix,msize);
  };
  float y_q1q2_1(){
	if(centp < pv[0]){
	  return y_e_q1q2_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1q2_1_125(matrix,msize) -  y_e_q1q2_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1q2_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1q2_1_150(matrix,msize) -  y_e_q1q2_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1q2_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1q2_1_175(matrix,msize) -  y_e_q1q2_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1q2_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1q2_1_200(matrix,msize) -  y_e_q1q2_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1q2_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1q2_1_250(matrix,msize) -  y_e_q1q2_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1q2_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1q2_1_300(matrix,msize) -  y_e_q1q2_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1q2_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1q2_1_350(matrix,msize) -  y_e_q1q2_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1q2_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1q2_1_400(matrix,msize) -  y_e_q1q2_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1q2_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1q2_1_449(matrix,msize) -  y_e_q1q2_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1q2_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1q2_1_500(matrix,msize) -  y_e_q1q2_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1q2_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1q2_1_600(matrix,msize) -  y_e_q1q2_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1q2_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1q2_1_700(matrix,msize) -  y_e_q1q2_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1q2_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1q2_1_800(matrix,msize) -  y_e_q1q2_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1q2_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1q2_1_900(matrix,msize) -  y_e_q1q2_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1q2_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1q2_1_1000(matrix,msize) -  y_e_q1q2_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1q2_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1q2_1_1100(matrix,msize) -  y_e_q1q2_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1q2_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1q2_1_1200(matrix,msize) -  y_e_q1q2_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1q2_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1q2_1_1200(matrix,msize);
  };
  bool passQ1Q2_1(){
	float x = x_q1q2_1()*100.0;
	float y = y_q1q2_1()*100.0;
	q1q2_1_x = x;
	q1q2_1_y = y;
	if(x!=x || y!=y || x*x + y*y > q1r*q1r){
	  return false;
	}
	return true;
  };
  float x_q1q2_2(){
	if(centp < pv[0]){
	  return x_e_q1q2_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1q2_2_125(matrix,msize) -  x_e_q1q2_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1q2_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1q2_2_150(matrix,msize) -  x_e_q1q2_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1q2_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1q2_2_175(matrix,msize) -  x_e_q1q2_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1q2_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1q2_2_200(matrix,msize) -  x_e_q1q2_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1q2_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1q2_2_250(matrix,msize) -  x_e_q1q2_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1q2_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1q2_2_300(matrix,msize) -  x_e_q1q2_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1q2_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1q2_2_350(matrix,msize) -  x_e_q1q2_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1q2_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1q2_2_400(matrix,msize) -  x_e_q1q2_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1q2_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1q2_2_449(matrix,msize) -  x_e_q1q2_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1q2_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1q2_2_500(matrix,msize) -  x_e_q1q2_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1q2_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1q2_2_600(matrix,msize) -  x_e_q1q2_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1q2_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1q2_2_700(matrix,msize) -  x_e_q1q2_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1q2_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1q2_2_800(matrix,msize) -  x_e_q1q2_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1q2_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1q2_2_900(matrix,msize) -  x_e_q1q2_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1q2_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1q2_2_1000(matrix,msize) -  x_e_q1q2_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1q2_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1q2_2_1100(matrix,msize) -  x_e_q1q2_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1q2_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1q2_2_1200(matrix,msize) -  x_e_q1q2_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1q2_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1q2_2_1200(matrix,msize);
  };
  float y_q1q2_2(){
	if(centp < pv[0]){
	  return y_e_q1q2_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1q2_2_125(matrix,msize) -  y_e_q1q2_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1q2_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1q2_2_150(matrix,msize) -  y_e_q1q2_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1q2_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1q2_2_175(matrix,msize) -  y_e_q1q2_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1q2_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1q2_2_200(matrix,msize) -  y_e_q1q2_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1q2_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1q2_2_250(matrix,msize) -  y_e_q1q2_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1q2_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1q2_2_300(matrix,msize) -  y_e_q1q2_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1q2_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1q2_2_350(matrix,msize) -  y_e_q1q2_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1q2_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1q2_2_400(matrix,msize) -  y_e_q1q2_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1q2_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1q2_2_449(matrix,msize) -  y_e_q1q2_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1q2_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1q2_2_500(matrix,msize) -  y_e_q1q2_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1q2_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1q2_2_600(matrix,msize) -  y_e_q1q2_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1q2_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1q2_2_700(matrix,msize) -  y_e_q1q2_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1q2_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1q2_2_800(matrix,msize) -  y_e_q1q2_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1q2_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1q2_2_900(matrix,msize) -  y_e_q1q2_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1q2_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1q2_2_1000(matrix,msize) -  y_e_q1q2_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1q2_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1q2_2_1100(matrix,msize) -  y_e_q1q2_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1q2_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1q2_2_1200(matrix,msize) -  y_e_q1q2_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1q2_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1q2_2_1200(matrix,msize);
  };
  bool passQ1Q2_2(){
	float x = x_q1q2_2()*100.0;
	float y = y_q1q2_2()*100.0;
	q1q2_2_x = x;
	q1q2_2_y = y;
	if(x!=x || y!=y || x*x + y*y > q2r*q2r){
	  return false;
	}
	return true;
  };
  float x_q1q2_3(){
	if(centp < pv[0]){
	  return x_e_q1q2_3_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q1q2_3_125(matrix,msize) -  x_e_q1q2_3_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q1q2_3_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q1q2_3_150(matrix,msize) -  x_e_q1q2_3_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q1q2_3_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q1q2_3_175(matrix,msize) -  x_e_q1q2_3_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q1q2_3_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q1q2_3_200(matrix,msize) -  x_e_q1q2_3_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q1q2_3_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q1q2_3_250(matrix,msize) -  x_e_q1q2_3_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q1q2_3_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q1q2_3_300(matrix,msize) -  x_e_q1q2_3_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q1q2_3_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q1q2_3_350(matrix,msize) -  x_e_q1q2_3_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q1q2_3_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q1q2_3_400(matrix,msize) -  x_e_q1q2_3_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q1q2_3_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q1q2_3_449(matrix,msize) -  x_e_q1q2_3_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q1q2_3_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q1q2_3_500(matrix,msize) -  x_e_q1q2_3_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q1q2_3_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q1q2_3_600(matrix,msize) -  x_e_q1q2_3_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q1q2_3_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q1q2_3_700(matrix,msize) -  x_e_q1q2_3_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q1q2_3_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q1q2_3_800(matrix,msize) -  x_e_q1q2_3_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q1q2_3_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q1q2_3_900(matrix,msize) -  x_e_q1q2_3_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q1q2_3_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q1q2_3_1000(matrix,msize) -  x_e_q1q2_3_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q1q2_3_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q1q2_3_1100(matrix,msize) -  x_e_q1q2_3_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q1q2_3_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q1q2_3_1200(matrix,msize) -  x_e_q1q2_3_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q1q2_3_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q1q2_3_1200(matrix,msize);
  };
  float y_q1q2_3(){
	if(centp < pv[0]){
	  return y_e_q1q2_3_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q1q2_3_125(matrix,msize) -  y_e_q1q2_3_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q1q2_3_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q1q2_3_150(matrix,msize) -  y_e_q1q2_3_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q1q2_3_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q1q2_3_175(matrix,msize) -  y_e_q1q2_3_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q1q2_3_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q1q2_3_200(matrix,msize) -  y_e_q1q2_3_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q1q2_3_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q1q2_3_250(matrix,msize) -  y_e_q1q2_3_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q1q2_3_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q1q2_3_300(matrix,msize) -  y_e_q1q2_3_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q1q2_3_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q1q2_3_350(matrix,msize) -  y_e_q1q2_3_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q1q2_3_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q1q2_3_400(matrix,msize) -  y_e_q1q2_3_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q1q2_3_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q1q2_3_449(matrix,msize) -  y_e_q1q2_3_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q1q2_3_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q1q2_3_500(matrix,msize) -  y_e_q1q2_3_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q1q2_3_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q1q2_3_600(matrix,msize) -  y_e_q1q2_3_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q1q2_3_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q1q2_3_700(matrix,msize) -  y_e_q1q2_3_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q1q2_3_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q1q2_3_800(matrix,msize) -  y_e_q1q2_3_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q1q2_3_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q1q2_3_900(matrix,msize) -  y_e_q1q2_3_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q1q2_3_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q1q2_3_1000(matrix,msize) -  y_e_q1q2_3_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q1q2_3_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q1q2_3_1100(matrix,msize) -  y_e_q1q2_3_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q1q2_3_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q1q2_3_1200(matrix,msize) -  y_e_q1q2_3_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q1q2_3_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q1q2_3_1200(matrix,msize);
  };
  bool passQ1Q2_3(){
	float x = x_q1q2_3()*100.0;
	float y = y_q1q2_3()*100.0;
	q1q2_3_x = x;
	q1q2_3_y = y;
	if(x!=x || y!=y || x*x + y*y > q2r*q2r){
	  return false;
	}
	return true;
  };
  float x_q2en(){
	if(centp < pv[0]){
	  return x_e_q2en_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q2en_125(matrix,msize) -  x_e_q2en_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q2en_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q2en_150(matrix,msize) -  x_e_q2en_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q2en_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q2en_175(matrix,msize) -  x_e_q2en_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q2en_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q2en_200(matrix,msize) -  x_e_q2en_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q2en_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q2en_250(matrix,msize) -  x_e_q2en_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q2en_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q2en_300(matrix,msize) -  x_e_q2en_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q2en_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q2en_350(matrix,msize) -  x_e_q2en_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q2en_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q2en_400(matrix,msize) -  x_e_q2en_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q2en_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q2en_449(matrix,msize) -  x_e_q2en_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q2en_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q2en_500(matrix,msize) -  x_e_q2en_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q2en_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q2en_600(matrix,msize) -  x_e_q2en_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q2en_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q2en_700(matrix,msize) -  x_e_q2en_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q2en_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q2en_800(matrix,msize) -  x_e_q2en_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q2en_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q2en_900(matrix,msize) -  x_e_q2en_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q2en_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q2en_1000(matrix,msize) -  x_e_q2en_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q2en_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q2en_1100(matrix,msize) -  x_e_q2en_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q2en_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q2en_1200(matrix,msize) -  x_e_q2en_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q2en_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q2en_1200(matrix,msize);
  };
  float y_q2en(){
	if(centp < pv[0]){
	  return y_e_q2en_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q2en_125(matrix,msize) -  y_e_q2en_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q2en_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q2en_150(matrix,msize) -  y_e_q2en_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q2en_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q2en_175(matrix,msize) -  y_e_q2en_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q2en_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q2en_200(matrix,msize) -  y_e_q2en_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q2en_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q2en_250(matrix,msize) -  y_e_q2en_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q2en_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q2en_300(matrix,msize) -  y_e_q2en_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q2en_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q2en_350(matrix,msize) -  y_e_q2en_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q2en_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q2en_400(matrix,msize) -  y_e_q2en_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q2en_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q2en_449(matrix,msize) -  y_e_q2en_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q2en_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q2en_500(matrix,msize) -  y_e_q2en_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q2en_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q2en_600(matrix,msize) -  y_e_q2en_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q2en_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q2en_700(matrix,msize) -  y_e_q2en_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q2en_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q2en_800(matrix,msize) -  y_e_q2en_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q2en_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q2en_900(matrix,msize) -  y_e_q2en_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q2en_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q2en_1000(matrix,msize) -  y_e_q2en_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q2en_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q2en_1100(matrix,msize) -  y_e_q2en_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q2en_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q2en_1200(matrix,msize) -  y_e_q2en_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q2en_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q2en_1200(matrix,msize);
  };
  bool passQ2ent(){
	float x = x_q2en()*100.0;
	float y = y_q2en()*100.0;
	q2en_x = x;
	q2en_y = y;
	if(x!=x || y!=y || x*x + y*y > q2r*q2r){
	  return false;
	}
	return true;
  };
  float x_q2ex_1(){
	if(centp < pv[0]){
	  return x_e_q2ex_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q2ex_1_125(matrix,msize) -  x_e_q2ex_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q2ex_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q2ex_1_150(matrix,msize) -  x_e_q2ex_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q2ex_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q2ex_1_175(matrix,msize) -  x_e_q2ex_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q2ex_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q2ex_1_200(matrix,msize) -  x_e_q2ex_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q2ex_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q2ex_1_250(matrix,msize) -  x_e_q2ex_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q2ex_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q2ex_1_300(matrix,msize) -  x_e_q2ex_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q2ex_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q2ex_1_350(matrix,msize) -  x_e_q2ex_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q2ex_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q2ex_1_400(matrix,msize) -  x_e_q2ex_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q2ex_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q2ex_1_449(matrix,msize) -  x_e_q2ex_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q2ex_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q2ex_1_500(matrix,msize) -  x_e_q2ex_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q2ex_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q2ex_1_600(matrix,msize) -  x_e_q2ex_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q2ex_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q2ex_1_700(matrix,msize) -  x_e_q2ex_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q2ex_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q2ex_1_800(matrix,msize) -  x_e_q2ex_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q2ex_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q2ex_1_900(matrix,msize) -  x_e_q2ex_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q2ex_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q2ex_1_1000(matrix,msize) -  x_e_q2ex_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q2ex_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q2ex_1_1100(matrix,msize) -  x_e_q2ex_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q2ex_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q2ex_1_1200(matrix,msize) -  x_e_q2ex_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q2ex_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q2ex_1_1200(matrix,msize);
  };
  float y_q2ex_1(){
	if(centp < pv[0]){
	  return y_e_q2ex_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q2ex_1_125(matrix,msize) -  y_e_q2ex_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q2ex_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q2ex_1_150(matrix,msize) -  y_e_q2ex_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q2ex_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q2ex_1_175(matrix,msize) -  y_e_q2ex_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q2ex_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q2ex_1_200(matrix,msize) -  y_e_q2ex_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q2ex_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q2ex_1_250(matrix,msize) -  y_e_q2ex_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q2ex_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q2ex_1_300(matrix,msize) -  y_e_q2ex_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q2ex_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q2ex_1_350(matrix,msize) -  y_e_q2ex_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q2ex_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q2ex_1_400(matrix,msize) -  y_e_q2ex_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q2ex_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q2ex_1_449(matrix,msize) -  y_e_q2ex_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q2ex_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q2ex_1_500(matrix,msize) -  y_e_q2ex_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q2ex_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q2ex_1_600(matrix,msize) -  y_e_q2ex_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q2ex_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q2ex_1_700(matrix,msize) -  y_e_q2ex_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q2ex_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q2ex_1_800(matrix,msize) -  y_e_q2ex_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q2ex_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q2ex_1_900(matrix,msize) -  y_e_q2ex_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q2ex_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q2ex_1_1000(matrix,msize) -  y_e_q2ex_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q2ex_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q2ex_1_1100(matrix,msize) -  y_e_q2ex_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q2ex_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q2ex_1_1200(matrix,msize) -  y_e_q2ex_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q2ex_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q2ex_1_1200(matrix,msize);
  };
  bool passQ2ext_1(){
	float x = x_q2ex_1()*100.0;
	float y = y_q2ex_1()*100.0;
	q2ex_1_x = x;
	q2ex_1_y = y;
	if(x!=x || y!=y || x*x + y*y > q2r*q2r){
	  return false;
	}
	return true;
  };
  float x_q2ex_2(){
	if(centp < pv[0]){
	  return x_e_q2ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q2ex_2_125(matrix,msize) -  x_e_q2ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q2ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q2ex_2_150(matrix,msize) -  x_e_q2ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q2ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q2ex_2_175(matrix,msize) -  x_e_q2ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q2ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q2ex_2_200(matrix,msize) -  x_e_q2ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q2ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q2ex_2_250(matrix,msize) -  x_e_q2ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q2ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q2ex_2_300(matrix,msize) -  x_e_q2ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q2ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q2ex_2_350(matrix,msize) -  x_e_q2ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q2ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q2ex_2_400(matrix,msize) -  x_e_q2ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q2ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q2ex_2_449(matrix,msize) -  x_e_q2ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q2ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q2ex_2_500(matrix,msize) -  x_e_q2ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q2ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q2ex_2_600(matrix,msize) -  x_e_q2ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q2ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q2ex_2_700(matrix,msize) -  x_e_q2ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q2ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q2ex_2_800(matrix,msize) -  x_e_q2ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q2ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q2ex_2_900(matrix,msize) -  x_e_q2ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q2ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q2ex_2_1000(matrix,msize) -  x_e_q2ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q2ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q2ex_2_1100(matrix,msize) -  x_e_q2ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q2ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q2ex_2_1200(matrix,msize) -  x_e_q2ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q2ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q2ex_2_1200(matrix,msize);
  };
  float y_q2ex_2(){
	if(centp < pv[0]){
	  return y_e_q2ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q2ex_2_125(matrix,msize) -  y_e_q2ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q2ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q2ex_2_150(matrix,msize) -  y_e_q2ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q2ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q2ex_2_175(matrix,msize) -  y_e_q2ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q2ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q2ex_2_200(matrix,msize) -  y_e_q2ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q2ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q2ex_2_250(matrix,msize) -  y_e_q2ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q2ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q2ex_2_300(matrix,msize) -  y_e_q2ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q2ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q2ex_2_350(matrix,msize) -  y_e_q2ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q2ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q2ex_2_400(matrix,msize) -  y_e_q2ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q2ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q2ex_2_449(matrix,msize) -  y_e_q2ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q2ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q2ex_2_500(matrix,msize) -  y_e_q2ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q2ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q2ex_2_600(matrix,msize) -  y_e_q2ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q2ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q2ex_2_700(matrix,msize) -  y_e_q2ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q2ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q2ex_2_800(matrix,msize) -  y_e_q2ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q2ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q2ex_2_900(matrix,msize) -  y_e_q2ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q2ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q2ex_2_1000(matrix,msize) -  y_e_q2ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q2ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q2ex_2_1100(matrix,msize) -  y_e_q2ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q2ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q2ex_2_1200(matrix,msize) -  y_e_q2ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q2ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q2ex_2_1200(matrix,msize);
  };
  bool passQ2ext_2(){
	float x = x_q2ex_2()*100.0;
	float y = y_q2ex_2()*100.0;
	q2ex_2_x = x;
	q2ex_2_y = y;
	if(x!=x || y!=y || x*x + y*y > q2r*q2r){
	  return false;
	}
	return true;
  };
  float x_qd(){
	if(centp < pv[0]){
	  return x_e_qd_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_qd_125(matrix,msize) -  x_e_qd_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_qd_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_qd_150(matrix,msize) -  x_e_qd_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_qd_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_qd_175(matrix,msize) -  x_e_qd_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_qd_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_qd_200(matrix,msize) -  x_e_qd_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_qd_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_qd_250(matrix,msize) -  x_e_qd_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_qd_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_qd_300(matrix,msize) -  x_e_qd_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_qd_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_qd_350(matrix,msize) -  x_e_qd_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_qd_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_qd_400(matrix,msize) -  x_e_qd_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_qd_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_qd_449(matrix,msize) -  x_e_qd_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_qd_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_qd_500(matrix,msize) -  x_e_qd_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_qd_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_qd_600(matrix,msize) -  x_e_qd_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_qd_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_qd_700(matrix,msize) -  x_e_qd_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_qd_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_qd_800(matrix,msize) -  x_e_qd_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_qd_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_qd_900(matrix,msize) -  x_e_qd_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_qd_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_qd_1000(matrix,msize) -  x_e_qd_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_qd_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_qd_1100(matrix,msize) -  x_e_qd_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_qd_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_qd_1200(matrix,msize) -  x_e_qd_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_qd_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_qd_1200(matrix,msize);
  };
  float y_qd(){
	if(centp < pv[0]){
	  return y_e_qd_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_qd_125(matrix,msize) -  y_e_qd_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_qd_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_qd_150(matrix,msize) -  y_e_qd_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_qd_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_qd_175(matrix,msize) -  y_e_qd_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_qd_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_qd_200(matrix,msize) -  y_e_qd_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_qd_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_qd_250(matrix,msize) -  y_e_qd_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_qd_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_qd_300(matrix,msize) -  y_e_qd_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_qd_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_qd_350(matrix,msize) -  y_e_qd_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_qd_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_qd_400(matrix,msize) -  y_e_qd_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_qd_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_qd_449(matrix,msize) -  y_e_qd_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_qd_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_qd_500(matrix,msize) -  y_e_qd_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_qd_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_qd_600(matrix,msize) -  y_e_qd_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_qd_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_qd_700(matrix,msize) -  y_e_qd_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_qd_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_qd_800(matrix,msize) -  y_e_qd_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_qd_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_qd_900(matrix,msize) -  y_e_qd_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_qd_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_qd_1000(matrix,msize) -  y_e_qd_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_qd_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_qd_1100(matrix,msize) -  y_e_qd_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_qd_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_qd_1200(matrix,msize) -  y_e_qd_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_qd_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_qd_1200(matrix,msize);
  };
  bool passQD(){
	float x = x_qd()*100.0;
	float y = y_qd()*100.0;
	qd_x = x;
	qd_y = y;
	if(x!=x || y!=y || pow((x-131.653)/25.981,2) + pow(y/30,2) > 1){
	  return false;
	}
	return true;
  };

  float x_predent(){
	if(centp < pv[0]){
	  return x_e_predent_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_predent_125(matrix,msize) -  x_e_predent_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_predent_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_predent_150(matrix,msize) -  x_e_predent_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_predent_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_predent_175(matrix,msize) -  x_e_predent_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_predent_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_predent_200(matrix,msize) -  x_e_predent_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_predent_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_predent_250(matrix,msize) -  x_e_predent_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_predent_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_predent_300(matrix,msize) -  x_e_predent_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_predent_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_predent_350(matrix,msize) -  x_e_predent_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_predent_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_predent_400(matrix,msize) -  x_e_predent_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_predent_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_predent_449(matrix,msize) -  x_e_predent_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_predent_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_predent_500(matrix,msize) -  x_e_predent_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_predent_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_predent_600(matrix,msize) -  x_e_predent_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_predent_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_predent_700(matrix,msize) -  x_e_predent_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_predent_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_predent_800(matrix,msize) -  x_e_predent_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_predent_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_predent_900(matrix,msize) -  x_e_predent_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_predent_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_predent_1000(matrix,msize) -  x_e_predent_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_predent_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_predent_1100(matrix,msize) -  x_e_predent_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_predent_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_predent_1200(matrix,msize) -  x_e_predent_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_predent_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_predent_1200(matrix,msize);
  };
  float y_predent(){
	if(centp < pv[0]){
	  return y_e_predent_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_predent_125(matrix,msize) -  y_e_predent_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_predent_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_predent_150(matrix,msize) -  y_e_predent_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_predent_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_predent_175(matrix,msize) -  y_e_predent_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_predent_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_predent_200(matrix,msize) -  y_e_predent_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_predent_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_predent_250(matrix,msize) -  y_e_predent_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_predent_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_predent_300(matrix,msize) -  y_e_predent_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_predent_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_predent_350(matrix,msize) -  y_e_predent_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_predent_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_predent_400(matrix,msize) -  y_e_predent_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_predent_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_predent_449(matrix,msize) -  y_e_predent_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_predent_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_predent_500(matrix,msize) -  y_e_predent_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_predent_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_predent_600(matrix,msize) -  y_e_predent_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_predent_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_predent_700(matrix,msize) -  y_e_predent_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_predent_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_predent_800(matrix,msize) -  y_e_predent_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_predent_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_predent_900(matrix,msize) -  y_e_predent_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_predent_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_predent_1000(matrix,msize) -  y_e_predent_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_predent_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_predent_1100(matrix,msize) -  y_e_predent_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_predent_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_predent_1200(matrix,msize) -  y_e_predent_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_predent_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_predent_1200(matrix,msize);
  };
  bool passPreDent(){
	float x = x_predent()*100.0;
	float y = y_predent()*100.0;
	predent_x = x;
	predent_y = y;
	//if(x!=x || y!=y || x*x + y*y > PreDent*PreDent){
	if ( x!=x || y!=y || x<-616.999 || x>-596.293 || fabs(y)>14.55 ){ //nan
	  return false;
	}
	return true;
  };
  float x_dent(){
	if(centp < pv[0]){
	  return x_e_dent_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_dent_125(matrix,msize) -  x_e_dent_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_dent_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_dent_150(matrix,msize) -  x_e_dent_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_dent_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_dent_175(matrix,msize) -  x_e_dent_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_dent_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_dent_200(matrix,msize) -  x_e_dent_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_dent_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_dent_250(matrix,msize) -  x_e_dent_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_dent_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_dent_300(matrix,msize) -  x_e_dent_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_dent_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_dent_350(matrix,msize) -  x_e_dent_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_dent_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_dent_400(matrix,msize) -  x_e_dent_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_dent_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_dent_449(matrix,msize) -  x_e_dent_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_dent_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_dent_500(matrix,msize) -  x_e_dent_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_dent_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_dent_600(matrix,msize) -  x_e_dent_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_dent_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_dent_700(matrix,msize) -  x_e_dent_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_dent_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_dent_800(matrix,msize) -  x_e_dent_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_dent_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_dent_900(matrix,msize) -  x_e_dent_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_dent_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_dent_1000(matrix,msize) -  x_e_dent_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_dent_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_dent_1100(matrix,msize) -  x_e_dent_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_dent_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_dent_1200(matrix,msize) -  x_e_dent_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_dent_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_dent_1200(matrix,msize);
  };
  float y_dent(){
	if(centp < pv[0]){
	  return y_e_dent_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_dent_125(matrix,msize) -  y_e_dent_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_dent_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_dent_150(matrix,msize) -  y_e_dent_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_dent_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_dent_175(matrix,msize) -  y_e_dent_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_dent_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_dent_200(matrix,msize) -  y_e_dent_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_dent_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_dent_250(matrix,msize) -  y_e_dent_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_dent_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_dent_300(matrix,msize) -  y_e_dent_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_dent_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_dent_350(matrix,msize) -  y_e_dent_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_dent_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_dent_400(matrix,msize) -  y_e_dent_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_dent_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_dent_449(matrix,msize) -  y_e_dent_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_dent_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_dent_500(matrix,msize) -  y_e_dent_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_dent_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_dent_600(matrix,msize) -  y_e_dent_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_dent_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_dent_700(matrix,msize) -  y_e_dent_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_dent_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_dent_800(matrix,msize) -  y_e_dent_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_dent_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_dent_900(matrix,msize) -  y_e_dent_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_dent_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_dent_1000(matrix,msize) -  y_e_dent_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_dent_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_dent_1100(matrix,msize) -  y_e_dent_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_dent_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_dent_1200(matrix,msize) -  y_e_dent_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_dent_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_dent_1200(matrix,msize);
  };
  bool passDent(){
	float x = x_dent()*100.0;
	float y = y_dent()*100.0;
	dent_x = x;
	dent_y = y;
	//if(x!=x || y!=y || x*x + y*y > Dent*Dent){
	if ( (x!=x) || (y!=y) || (x<-522.088) || (x>-498.099) || fabs(y)>11.756-0.06202842969*(x+498.099) ){ //nan
	  return false;
	}
	return true;
  };
  float x_dext(){
	if(centp < pv[0]){
	  return x_e_dext_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_dext_125(matrix,msize) -  x_e_dext_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_dext_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_dext_150(matrix,msize) -  x_e_dext_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_dext_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_dext_175(matrix,msize) -  x_e_dext_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_dext_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_dext_200(matrix,msize) -  x_e_dext_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_dext_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_dext_250(matrix,msize) -  x_e_dext_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_dext_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_dext_300(matrix,msize) -  x_e_dext_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_dext_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_dext_350(matrix,msize) -  x_e_dext_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_dext_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_dext_400(matrix,msize) -  x_e_dext_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_dext_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_dext_449(matrix,msize) -  x_e_dext_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_dext_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_dext_500(matrix,msize) -  x_e_dext_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_dext_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_dext_600(matrix,msize) -  x_e_dext_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_dext_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_dext_700(matrix,msize) -  x_e_dext_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_dext_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_dext_800(matrix,msize) -  x_e_dext_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_dext_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_dext_900(matrix,msize) -  x_e_dext_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_dext_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_dext_1000(matrix,msize) -  x_e_dext_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_dext_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_dext_1100(matrix,msize) -  x_e_dext_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_dext_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_dext_1200(matrix,msize) -  x_e_dext_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_dext_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_dext_1200(matrix,msize);
  };
  float y_dext(){
	if(centp < pv[0]){
	  return y_e_dext_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_dext_125(matrix,msize) -  y_e_dext_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_dext_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_dext_150(matrix,msize) -  y_e_dext_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_dext_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_dext_175(matrix,msize) -  y_e_dext_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_dext_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_dext_200(matrix,msize) -  y_e_dext_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_dext_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_dext_250(matrix,msize) -  y_e_dext_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_dext_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_dext_300(matrix,msize) -  y_e_dext_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_dext_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_dext_350(matrix,msize) -  y_e_dext_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_dext_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_dext_400(matrix,msize) -  y_e_dext_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_dext_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_dext_449(matrix,msize) -  y_e_dext_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_dext_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_dext_500(matrix,msize) -  y_e_dext_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_dext_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_dext_600(matrix,msize) -  y_e_dext_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_dext_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_dext_700(matrix,msize) -  y_e_dext_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_dext_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_dext_800(matrix,msize) -  y_e_dext_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_dext_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_dext_900(matrix,msize) -  y_e_dext_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_dext_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_dext_1000(matrix,msize) -  y_e_dext_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_dext_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_dext_1100(matrix,msize) -  y_e_dext_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_dext_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_dext_1200(matrix,msize) -  y_e_dext_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_dext_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_dext_1200(matrix,msize);
  };
  bool passDext(){
	float x = x_dext()*100.0;
	float y = y_dext()*100.0;
	dext_x = x;
	dext_y = y;
	//if(x!=x || y!=y || x*x + y*y > Dext*Dext){
	if ( (x!=x) || (y!=y)  || fabs(x)>46.188 || fabs(y)>11.756-0.01610808002*(x-46.188) ){ //nan
	  return false;
	}
	return true;
  };
  float x_dq(){
	if(centp < pv[0]){
	  return x_e_dq_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_dq_125(matrix,msize) -  x_e_dq_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_dq_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_dq_150(matrix,msize) -  x_e_dq_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_dq_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_dq_175(matrix,msize) -  x_e_dq_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_dq_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_dq_200(matrix,msize) -  x_e_dq_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_dq_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_dq_250(matrix,msize) -  x_e_dq_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_dq_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_dq_300(matrix,msize) -  x_e_dq_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_dq_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_dq_350(matrix,msize) -  x_e_dq_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_dq_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_dq_400(matrix,msize) -  x_e_dq_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_dq_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_dq_449(matrix,msize) -  x_e_dq_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_dq_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_dq_500(matrix,msize) -  x_e_dq_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_dq_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_dq_600(matrix,msize) -  x_e_dq_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_dq_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_dq_700(matrix,msize) -  x_e_dq_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_dq_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_dq_800(matrix,msize) -  x_e_dq_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_dq_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_dq_900(matrix,msize) -  x_e_dq_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_dq_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_dq_1000(matrix,msize) -  x_e_dq_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_dq_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_dq_1100(matrix,msize) -  x_e_dq_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_dq_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_dq_1200(matrix,msize) -  x_e_dq_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_dq_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_dq_1200(matrix,msize);
  };
  float y_dq(){
	if(centp < pv[0]){
	  return y_e_dq_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_dq_125(matrix,msize) -  y_e_dq_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_dq_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_dq_150(matrix,msize) -  y_e_dq_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_dq_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_dq_175(matrix,msize) -  y_e_dq_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_dq_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_dq_200(matrix,msize) -  y_e_dq_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_dq_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_dq_250(matrix,msize) -  y_e_dq_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_dq_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_dq_300(matrix,msize) -  y_e_dq_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_dq_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_dq_350(matrix,msize) -  y_e_dq_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_dq_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_dq_400(matrix,msize) -  y_e_dq_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_dq_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_dq_449(matrix,msize) -  y_e_dq_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_dq_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_dq_500(matrix,msize) -  y_e_dq_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_dq_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_dq_600(matrix,msize) -  y_e_dq_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_dq_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_dq_700(matrix,msize) -  y_e_dq_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_dq_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_dq_800(matrix,msize) -  y_e_dq_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_dq_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_dq_900(matrix,msize) -  y_e_dq_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_dq_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_dq_1000(matrix,msize) -  y_e_dq_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_dq_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_dq_1100(matrix,msize) -  y_e_dq_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_dq_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_dq_1200(matrix,msize) -  y_e_dq_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_dq_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_dq_1200(matrix,msize);
  };
  bool passDQ(){
	float x = x_dq()*100.0;
	float y = y_dq()*100.0;
	dq_x = x;
	dq_y = y;
	if ( (x!=x) || (y!=y) || fabs(y)>15 || x<15.359 || x>84.641 ) {
	  return false;
	}
	return true;
  };
  float x_q3en(){
	if(centp < pv[0]){
	  return x_e_q3en_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q3en_125(matrix,msize) -  x_e_q3en_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q3en_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q3en_150(matrix,msize) -  x_e_q3en_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q3en_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q3en_175(matrix,msize) -  x_e_q3en_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q3en_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q3en_200(matrix,msize) -  x_e_q3en_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q3en_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q3en_250(matrix,msize) -  x_e_q3en_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q3en_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q3en_300(matrix,msize) -  x_e_q3en_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q3en_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q3en_350(matrix,msize) -  x_e_q3en_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q3en_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q3en_400(matrix,msize) -  x_e_q3en_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q3en_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q3en_449(matrix,msize) -  x_e_q3en_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q3en_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q3en_500(matrix,msize) -  x_e_q3en_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q3en_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q3en_600(matrix,msize) -  x_e_q3en_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q3en_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q3en_700(matrix,msize) -  x_e_q3en_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q3en_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q3en_800(matrix,msize) -  x_e_q3en_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q3en_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q3en_900(matrix,msize) -  x_e_q3en_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q3en_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q3en_1000(matrix,msize) -  x_e_q3en_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q3en_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q3en_1100(matrix,msize) -  x_e_q3en_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q3en_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q3en_1200(matrix,msize) -  x_e_q3en_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q3en_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q3en_1200(matrix,msize);
  };
  float y_q3en(){
	if(centp < pv[0]){
	  return y_e_q3en_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q3en_125(matrix,msize) -  y_e_q3en_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q3en_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q3en_150(matrix,msize) -  y_e_q3en_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q3en_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q3en_175(matrix,msize) -  y_e_q3en_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q3en_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q3en_200(matrix,msize) -  y_e_q3en_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q3en_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q3en_250(matrix,msize) -  y_e_q3en_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q3en_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q3en_300(matrix,msize) -  y_e_q3en_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q3en_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q3en_350(matrix,msize) -  y_e_q3en_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q3en_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q3en_400(matrix,msize) -  y_e_q3en_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q3en_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q3en_449(matrix,msize) -  y_e_q3en_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q3en_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q3en_500(matrix,msize) -  y_e_q3en_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q3en_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q3en_600(matrix,msize) -  y_e_q3en_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q3en_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q3en_700(matrix,msize) -  y_e_q3en_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q3en_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q3en_800(matrix,msize) -  y_e_q3en_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q3en_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q3en_900(matrix,msize) -  y_e_q3en_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q3en_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q3en_1000(matrix,msize) -  y_e_q3en_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q3en_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q3en_1100(matrix,msize) -  y_e_q3en_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q3en_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q3en_1200(matrix,msize) -  y_e_q3en_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q3en_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q3en_1200(matrix,msize);
  };
  bool passQ3ent(){
	float x = x_q3en()*100.0;
	float y = y_q3en()*100.0;
	q3en_x = x;
	q3en_y = y;
	if(x!=x || y!=y || x*x + y*y > q3r*q3r){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>13.5||y<-13.5){
	  return false;
	}
	return true;
  };
  float x_q3ex_1(){
	if(centp < pv[0]){
	  return x_e_q3ex_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q3ex_1_125(matrix,msize) -  x_e_q3ex_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q3ex_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q3ex_1_150(matrix,msize) -  x_e_q3ex_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q3ex_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q3ex_1_175(matrix,msize) -  x_e_q3ex_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q3ex_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q3ex_1_200(matrix,msize) -  x_e_q3ex_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q3ex_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q3ex_1_250(matrix,msize) -  x_e_q3ex_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q3ex_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q3ex_1_300(matrix,msize) -  x_e_q3ex_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q3ex_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q3ex_1_350(matrix,msize) -  x_e_q3ex_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q3ex_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q3ex_1_400(matrix,msize) -  x_e_q3ex_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q3ex_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q3ex_1_449(matrix,msize) -  x_e_q3ex_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q3ex_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q3ex_1_500(matrix,msize) -  x_e_q3ex_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q3ex_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q3ex_1_600(matrix,msize) -  x_e_q3ex_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q3ex_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q3ex_1_700(matrix,msize) -  x_e_q3ex_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q3ex_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q3ex_1_800(matrix,msize) -  x_e_q3ex_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q3ex_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q3ex_1_900(matrix,msize) -  x_e_q3ex_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q3ex_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q3ex_1_1000(matrix,msize) -  x_e_q3ex_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q3ex_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q3ex_1_1100(matrix,msize) -  x_e_q3ex_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q3ex_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q3ex_1_1200(matrix,msize) -  x_e_q3ex_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q3ex_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q3ex_1_1200(matrix,msize);
  };
  float y_q3ex_1(){
	if(centp < pv[0]){
	  return y_e_q3ex_1_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q3ex_1_125(matrix,msize) -  y_e_q3ex_1_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q3ex_1_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q3ex_1_150(matrix,msize) -  y_e_q3ex_1_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q3ex_1_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q3ex_1_175(matrix,msize) -  y_e_q3ex_1_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q3ex_1_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q3ex_1_200(matrix,msize) -  y_e_q3ex_1_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q3ex_1_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q3ex_1_250(matrix,msize) -  y_e_q3ex_1_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q3ex_1_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q3ex_1_300(matrix,msize) -  y_e_q3ex_1_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q3ex_1_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q3ex_1_350(matrix,msize) -  y_e_q3ex_1_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q3ex_1_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q3ex_1_400(matrix,msize) -  y_e_q3ex_1_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q3ex_1_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q3ex_1_449(matrix,msize) -  y_e_q3ex_1_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q3ex_1_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q3ex_1_500(matrix,msize) -  y_e_q3ex_1_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q3ex_1_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q3ex_1_600(matrix,msize) -  y_e_q3ex_1_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q3ex_1_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q3ex_1_700(matrix,msize) -  y_e_q3ex_1_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q3ex_1_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q3ex_1_800(matrix,msize) -  y_e_q3ex_1_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q3ex_1_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q3ex_1_900(matrix,msize) -  y_e_q3ex_1_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q3ex_1_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q3ex_1_1000(matrix,msize) -  y_e_q3ex_1_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q3ex_1_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q3ex_1_1100(matrix,msize) -  y_e_q3ex_1_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q3ex_1_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q3ex_1_1200(matrix,msize) -  y_e_q3ex_1_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q3ex_1_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q3ex_1_1200(matrix,msize);
  };
  bool passQ3ext_1(){
	float x = x_q3ex_1()*100.0;
	float y = y_q3ex_1()*100.0;
	q3ex_1_x = x;
	q3ex_1_y = y;
	if(x!=x || y!=y || x*x + y*y > q3r*q3r){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>13.5||y<-13.5){
	  return false;
	}
	return true;
  };
  float x_q3ex_2(){
	if(centp < pv[0]){
	  return x_e_q3ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_q3ex_2_125(matrix,msize) -  x_e_q3ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_q3ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_q3ex_2_150(matrix,msize) -  x_e_q3ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_q3ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_q3ex_2_175(matrix,msize) -  x_e_q3ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_q3ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_q3ex_2_200(matrix,msize) -  x_e_q3ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_q3ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_q3ex_2_250(matrix,msize) -  x_e_q3ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_q3ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_q3ex_2_300(matrix,msize) -  x_e_q3ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_q3ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_q3ex_2_350(matrix,msize) -  x_e_q3ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_q3ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_q3ex_2_400(matrix,msize) -  x_e_q3ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_q3ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_q3ex_2_449(matrix,msize) -  x_e_q3ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_q3ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_q3ex_2_500(matrix,msize) -  x_e_q3ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_q3ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_q3ex_2_600(matrix,msize) -  x_e_q3ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_q3ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_q3ex_2_700(matrix,msize) -  x_e_q3ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_q3ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_q3ex_2_800(matrix,msize) -  x_e_q3ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_q3ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_q3ex_2_900(matrix,msize) -  x_e_q3ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_q3ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_q3ex_2_1000(matrix,msize) -  x_e_q3ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_q3ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_q3ex_2_1100(matrix,msize) -  x_e_q3ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_q3ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_q3ex_2_1200(matrix,msize) -  x_e_q3ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_q3ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_q3ex_2_1200(matrix,msize);
  };
  float y_q3ex_2(){
	if(centp < pv[0]){
	  return y_e_q3ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_q3ex_2_125(matrix,msize) -  y_e_q3ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_q3ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_q3ex_2_150(matrix,msize) -  y_e_q3ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_q3ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_q3ex_2_175(matrix,msize) -  y_e_q3ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_q3ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_q3ex_2_200(matrix,msize) -  y_e_q3ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_q3ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_q3ex_2_250(matrix,msize) -  y_e_q3ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_q3ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_q3ex_2_300(matrix,msize) -  y_e_q3ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_q3ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_q3ex_2_350(matrix,msize) -  y_e_q3ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_q3ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_q3ex_2_400(matrix,msize) -  y_e_q3ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_q3ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_q3ex_2_449(matrix,msize) -  y_e_q3ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_q3ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_q3ex_2_500(matrix,msize) -  y_e_q3ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_q3ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_q3ex_2_600(matrix,msize) -  y_e_q3ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_q3ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_q3ex_2_700(matrix,msize) -  y_e_q3ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_q3ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_q3ex_2_800(matrix,msize) -  y_e_q3ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_q3ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_q3ex_2_900(matrix,msize) -  y_e_q3ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_q3ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_q3ex_2_1000(matrix,msize) -  y_e_q3ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_q3ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_q3ex_2_1100(matrix,msize) -  y_e_q3ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_q3ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_q3ex_2_1200(matrix,msize) -  y_e_q3ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_q3ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_q3ex_2_1200(matrix,msize);
  };
  float t_q3ex_2(){
	if(centp < pv[0]){
	  return t_e_q3ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (t_e_q3ex_2_125(matrix,msize) -  t_e_q3ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = t_e_q3ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (t_e_q3ex_2_150(matrix,msize) -  t_e_q3ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = t_e_q3ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (t_e_q3ex_2_175(matrix,msize) -  t_e_q3ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = t_e_q3ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (t_e_q3ex_2_200(matrix,msize) -  t_e_q3ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = t_e_q3ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (t_e_q3ex_2_250(matrix,msize) -  t_e_q3ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = t_e_q3ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (t_e_q3ex_2_300(matrix,msize) -  t_e_q3ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = t_e_q3ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (t_e_q3ex_2_350(matrix,msize) -  t_e_q3ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = t_e_q3ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (t_e_q3ex_2_400(matrix,msize) -  t_e_q3ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = t_e_q3ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (t_e_q3ex_2_449(matrix,msize) -  t_e_q3ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = t_e_q3ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (t_e_q3ex_2_500(matrix,msize) -  t_e_q3ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = t_e_q3ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (t_e_q3ex_2_600(matrix,msize) -  t_e_q3ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = t_e_q3ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (t_e_q3ex_2_700(matrix,msize) -  t_e_q3ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = t_e_q3ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (t_e_q3ex_2_800(matrix,msize) -  t_e_q3ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = t_e_q3ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (t_e_q3ex_2_900(matrix,msize) -  t_e_q3ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = t_e_q3ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (t_e_q3ex_2_1000(matrix,msize) -  t_e_q3ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = t_e_q3ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (t_e_q3ex_2_1100(matrix,msize) -  t_e_q3ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = t_e_q3ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (t_e_q3ex_2_1200(matrix,msize) -  t_e_q3ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = t_e_q3ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return t_e_q3ex_2_1200(matrix,msize);
  };
  float p_q3ex_2(){
	if(centp < pv[0]){
	  return p_e_q3ex_2_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (p_e_q3ex_2_125(matrix,msize) -  p_e_q3ex_2_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = p_e_q3ex_2_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (p_e_q3ex_2_150(matrix,msize) -  p_e_q3ex_2_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = p_e_q3ex_2_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (p_e_q3ex_2_175(matrix,msize) -  p_e_q3ex_2_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = p_e_q3ex_2_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (p_e_q3ex_2_200(matrix,msize) -  p_e_q3ex_2_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = p_e_q3ex_2_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (p_e_q3ex_2_250(matrix,msize) -  p_e_q3ex_2_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = p_e_q3ex_2_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (p_e_q3ex_2_300(matrix,msize) -  p_e_q3ex_2_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = p_e_q3ex_2_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (p_e_q3ex_2_350(matrix,msize) -  p_e_q3ex_2_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = p_e_q3ex_2_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (p_e_q3ex_2_400(matrix,msize) -  p_e_q3ex_2_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = p_e_q3ex_2_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (p_e_q3ex_2_449(matrix,msize) -  p_e_q3ex_2_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = p_e_q3ex_2_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (p_e_q3ex_2_500(matrix,msize) -  p_e_q3ex_2_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = p_e_q3ex_2_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (p_e_q3ex_2_600(matrix,msize) -  p_e_q3ex_2_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = p_e_q3ex_2_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (p_e_q3ex_2_700(matrix,msize) -  p_e_q3ex_2_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = p_e_q3ex_2_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (p_e_q3ex_2_800(matrix,msize) -  p_e_q3ex_2_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = p_e_q3ex_2_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (p_e_q3ex_2_900(matrix,msize) -  p_e_q3ex_2_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = p_e_q3ex_2_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (p_e_q3ex_2_1000(matrix,msize) -  p_e_q3ex_2_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = p_e_q3ex_2_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (p_e_q3ex_2_1100(matrix,msize) -  p_e_q3ex_2_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = p_e_q3ex_2_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (p_e_q3ex_2_1200(matrix,msize) -  p_e_q3ex_2_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = p_e_q3ex_2_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return p_e_q3ex_2_1200(matrix,msize);
  };

  bool passQ3ext_2(){
	float x = x_q3ex_2()*100.0;
	float y = y_q3ex_2()*100.0;
	float t = t_q3ex_2();
	float p = p_q3ex_2();
	q3ex_2_x = x;
	q3ex_2_y = y;
	q3ex_2_t = t;
	q3ex_2_p = p;
	if(x!=x || y!=y || x*x + y*y > q3r*q3r){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>12||y<-14){
	//if(x!=x || y!=y || x*x + y*y > q3r*q3r||y>13.5||y<-13.5){
	  return false;
	}
	return true;
  };

  bool passAperture_1(){
	float x = xin + 65.686*thin;
	float y = yin + 65.686*phin;
	if(x!=x || y!=y || x*x + y*y > pow(7.3787,2) ){
	  return false;
	}
	return true;
  }

  bool passAperture_2(){
	float x = xin + 80.436*thin;
	float y = yin + 80.436*phin;
	if(x!=x || y!=y || x*x + y*y > pow(7.4092,2) ){
	  return false;
	}
	return true;
  }

  bool passAperture_3(){
	float x = xin + 135.064*thin;
	float y = yin + 135.064*phin;
	if(x!=x || y!=y || x*x + y*y > pow(12.5222,2) ){
	  return false;
	}
	return true;
  }

  bool passDrift_1(){
	float x = q3ex_2_x + (2080.38746-2054.88)*q3ex_2_t;
	float y = q3ex_2_y + (2080.38746-2054.88)*q3ex_2_p;
	if(x!=x || y!=y || fabs(x)>35.56 || fabs(y)>17.145 ){
	  return false;
	}
	return true;
  }

  bool passDrift_2(){
	float x = q3ex_2_x + (2327.47246-2054.88)*q3ex_2_t;
	float y = q3ex_2_y + (2327.47246-2054.88)*q3ex_2_p;
	if(x!=x || y!=y || fabs(x)>99.76635 || fabs(y)>17.145 ){
	  return false;
	}
	return true;
  }

  bool passAllMags(){
	if(!passAperture_1()) { return false; }
	if(!passAperture_2()) { return false; }
	if(!passAperture_3()) { return false; }

	if(!passQ1ent_1()) { return false; }
	if(!passQ1ent_2()) { return false; }
	if(!passQ1ext()) { return false; }
	if(!passQ1Q2_1()) { return false; }
	if(!passQ1Q2_2()) { return false; }
	if(!passQ1Q2_3()) { return false; }
	if(!passQ2ent()) { return false; }
	if(!passQ2ext_1()) { return false; }
	if(!passQ2ext_2()) { return false; }
	if(!passQD()) { return false; }
	if(!passPreDent()) { return false; }
	if(!passDent()) { return false; }
	if(!passDext()) { return false; }
	if(!passDQ()) { return false; }
	if(!passQ3ent()) { return false; }
	if(!passQ3ext_1()) { return false; }
	if(!passQ3ext_2()) { return false; }

	if(!passDrift_1()) { return false; }
	if(!passDrift_2()) { return false; }
	return true;
  };

  float x_fp(){
	if(centp < pv[0]){
	  return x_e_fp_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (x_e_fp_125(matrix,msize) -  x_e_fp_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = x_e_fp_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (x_e_fp_150(matrix,msize) -  x_e_fp_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = x_e_fp_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (x_e_fp_175(matrix,msize) -  x_e_fp_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = x_e_fp_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (x_e_fp_200(matrix,msize) -  x_e_fp_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = x_e_fp_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (x_e_fp_250(matrix,msize) -  x_e_fp_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = x_e_fp_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (x_e_fp_300(matrix,msize) -  x_e_fp_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = x_e_fp_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (x_e_fp_350(matrix,msize) -  x_e_fp_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = x_e_fp_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (x_e_fp_400(matrix,msize) -  x_e_fp_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = x_e_fp_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (x_e_fp_449(matrix,msize) -  x_e_fp_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = x_e_fp_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (x_e_fp_500(matrix,msize) -  x_e_fp_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = x_e_fp_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (x_e_fp_600(matrix,msize) -  x_e_fp_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = x_e_fp_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (x_e_fp_700(matrix,msize) -  x_e_fp_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = x_e_fp_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (x_e_fp_800(matrix,msize) -  x_e_fp_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = x_e_fp_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (x_e_fp_900(matrix,msize) -  x_e_fp_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = x_e_fp_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (x_e_fp_1000(matrix,msize) -  x_e_fp_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = x_e_fp_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (x_e_fp_1100(matrix,msize) -  x_e_fp_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = x_e_fp_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (x_e_fp_1200(matrix,msize) -  x_e_fp_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = x_e_fp_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return x_e_fp_1200(matrix,msize);
  };
  float y_fp(){
	if(centp < pv[0]){
	  return y_e_fp_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (y_e_fp_125(matrix,msize) -  y_e_fp_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = y_e_fp_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (y_e_fp_150(matrix,msize) -  y_e_fp_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = y_e_fp_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (y_e_fp_175(matrix,msize) -  y_e_fp_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = y_e_fp_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (y_e_fp_200(matrix,msize) -  y_e_fp_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = y_e_fp_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (y_e_fp_250(matrix,msize) -  y_e_fp_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = y_e_fp_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (y_e_fp_300(matrix,msize) -  y_e_fp_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = y_e_fp_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (y_e_fp_350(matrix,msize) -  y_e_fp_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = y_e_fp_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (y_e_fp_400(matrix,msize) -  y_e_fp_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = y_e_fp_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (y_e_fp_449(matrix,msize) -  y_e_fp_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = y_e_fp_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (y_e_fp_500(matrix,msize) -  y_e_fp_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = y_e_fp_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (y_e_fp_600(matrix,msize) -  y_e_fp_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = y_e_fp_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (y_e_fp_700(matrix,msize) -  y_e_fp_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = y_e_fp_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (y_e_fp_800(matrix,msize) -  y_e_fp_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = y_e_fp_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (y_e_fp_900(matrix,msize) -  y_e_fp_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = y_e_fp_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (y_e_fp_1000(matrix,msize) -  y_e_fp_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = y_e_fp_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (y_e_fp_1100(matrix,msize) -  y_e_fp_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = y_e_fp_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (y_e_fp_1200(matrix,msize) -  y_e_fp_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = y_e_fp_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return y_e_fp_1200(matrix,msize);
  };
  float th_fp(){
	if(centp < pv[0]){
	  return t_e_fp_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (t_e_fp_125(matrix,msize) -  t_e_fp_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = t_e_fp_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (t_e_fp_150(matrix,msize) -  t_e_fp_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = t_e_fp_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (t_e_fp_175(matrix,msize) -  t_e_fp_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = t_e_fp_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (t_e_fp_200(matrix,msize) -  t_e_fp_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = t_e_fp_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (t_e_fp_250(matrix,msize) -  t_e_fp_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = t_e_fp_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (t_e_fp_300(matrix,msize) -  t_e_fp_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = t_e_fp_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (t_e_fp_350(matrix,msize) -  t_e_fp_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = t_e_fp_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (t_e_fp_400(matrix,msize) -  t_e_fp_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = t_e_fp_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (t_e_fp_449(matrix,msize) -  t_e_fp_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = t_e_fp_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (t_e_fp_500(matrix,msize) -  t_e_fp_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = t_e_fp_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (t_e_fp_600(matrix,msize) -  t_e_fp_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = t_e_fp_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (t_e_fp_700(matrix,msize) -  t_e_fp_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = t_e_fp_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (t_e_fp_800(matrix,msize) -  t_e_fp_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = t_e_fp_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (t_e_fp_900(matrix,msize) -  t_e_fp_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = t_e_fp_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (t_e_fp_1000(matrix,msize) -  t_e_fp_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = t_e_fp_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (t_e_fp_1100(matrix,msize) -  t_e_fp_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = t_e_fp_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (t_e_fp_1200(matrix,msize) -  t_e_fp_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = t_e_fp_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return t_e_fp_1200(matrix,msize);
  };
  float ph_fp(){
	if(centp < pv[0]){
	  return p_e_fp_100(matrix,msize);
	}else if(centp < pv[1]){
	  float m =  (p_e_fp_125(matrix,msize) -  p_e_fp_100(matrix,msize))/(pv[1] - pv[0]);
	  float b = p_e_fp_125(matrix,msize) - m*pv[1];
	  return b + m*centp;
	}else if(centp < pv[2]){
	  float m =  (p_e_fp_150(matrix,msize) -  p_e_fp_125(matrix,msize))/(pv[2] - pv[1]);
	  float b = p_e_fp_150(matrix,msize) - m*pv[2];
	  return b + m*centp;
	}else if(centp < pv[3]){
	  float m =  (p_e_fp_175(matrix,msize) -  p_e_fp_150(matrix,msize))/(pv[3] - pv[2]);
	  float b = p_e_fp_175(matrix,msize) - m*pv[3];
	  return b + m*centp;
	}else if(centp < pv[4]){
	  float m =  (p_e_fp_200(matrix,msize) -  p_e_fp_175(matrix,msize))/(pv[4] - pv[3]);
	  float b = p_e_fp_200(matrix,msize) - m*pv[4];
	  return b + m*centp;
	}else if(centp < pv[5]){
	  float m =  (p_e_fp_250(matrix,msize) -  p_e_fp_200(matrix,msize))/(pv[5] - pv[4]);
	  float b = p_e_fp_250(matrix,msize) - m*pv[5];
	  return b + m*centp;
	}else if(centp < pv[6]){
	  float m =  (p_e_fp_300(matrix,msize) -  p_e_fp_250(matrix,msize))/(pv[6] - pv[5]);
	  float b = p_e_fp_300(matrix,msize) - m*pv[6];
	  return b + m*centp;
	}else if(centp < pv[7]){
	  float m =  (p_e_fp_350(matrix,msize) -  p_e_fp_300(matrix,msize))/(pv[7] - pv[6]);
	  float b = p_e_fp_350(matrix,msize) - m*pv[7];
	  return b + m*centp;
	}else if(centp < pv[8]){
	  float m =  (p_e_fp_400(matrix,msize) -  p_e_fp_350(matrix,msize))/(pv[8] - pv[7]);
	  float b = p_e_fp_400(matrix,msize) - m*pv[8];
	  return b + m*centp;
	}else if(centp < pv[9]){
	  float m =  (p_e_fp_449(matrix,msize) -  p_e_fp_400(matrix,msize))/(pv[9] - pv[8]);
	  float b = p_e_fp_449(matrix,msize) - m*pv[9];
	  return b + m*centp;
	  //here we see the hardware change, so no interpolation between 10 and 11.
	}else if(centp < pv[11]){
	  float m =  (p_e_fp_500(matrix,msize) -  p_e_fp_450(matrix,msize))/(pv[11] - pv[10]);
	  float b = p_e_fp_500(matrix,msize) - m*pv[11];
	  return b + m*centp;
	}else if(centp < pv[12]){
	  float m =  (p_e_fp_600(matrix,msize) -  p_e_fp_500(matrix,msize))/(pv[12] - pv[11]);
	  float b = p_e_fp_600(matrix,msize) - m*pv[12];
	  return b + m*centp;
	}else if(centp < pv[13]){
	  float m =  (p_e_fp_700(matrix,msize) -  p_e_fp_600(matrix,msize))/(pv[13] - pv[12]);
	  float b = p_e_fp_700(matrix,msize) - m*pv[13];
	  return b + m*centp;
	}else if(centp < pv[14]){
	  float m =  (p_e_fp_800(matrix,msize) -  p_e_fp_700(matrix,msize))/(pv[14] - pv[13]);
	  float b = p_e_fp_800(matrix,msize) - m*pv[14];
	  return b + m*centp;
	}else if(centp < pv[15]){
	  float m =  (p_e_fp_900(matrix,msize) -  p_e_fp_800(matrix,msize))/(pv[15] - pv[14]);
	  float b = p_e_fp_900(matrix,msize) - m*pv[15];
	  return b + m*centp;
	}else if(centp < pv[16]){
	  float m =  (p_e_fp_1000(matrix,msize) -  p_e_fp_900(matrix,msize))/(pv[16] - pv[15]);
	  float b = p_e_fp_1000(matrix,msize) - m*pv[16];
	  return b + m*centp;
	}else if(centp < pv[17]){
	  float m =  (p_e_fp_1100(matrix,msize) -  p_e_fp_1000(matrix,msize))/(pv[17] - pv[16]);
	  float b = p_e_fp_1100(matrix,msize) - m*pv[17];
	  return b + m*centp;
	}else if(centp < pv[18]){
	  float m =  (p_e_fp_1200(matrix,msize) -  p_e_fp_1100(matrix,msize))/(pv[18] - pv[17]);
	  float b = p_e_fp_1200(matrix,msize) - m*pv[18];
	  return b + m*centp;
	}
	//if none of the above, must be high energy
	return p_e_fp_1200(matrix,msize);
  };

  float dp_tg(){
	msize = 4;
	if(centp<pv[9])
	{
	  return ldelta_400(matrix,msize);
	}
	else
	{
	  return ldelta_1100(matrix,msize);
	}
  };
  float th_tg(){
	msize =4;
	if(centp<pv[9])
	{
	  return ltheta_400(matrix,msize);
	}
	else
	{
	  return ltheta_1100(matrix,msize);
	}
  };
  float ph_tg(){
	msize = 4;
	if(centp<pv[9])
	{
	  return lphi_400(matrix,msize);
	}
	else
	{
	  return lphi_1100(matrix,msize);
	}
  };
  float y_tg(){
	msize =4;
	if(centp<pv[9])
	{
	  return ly00_400(matrix,msize);
	}
	else
	{
	  return ly00_1100(matrix,msize);
	}
  };

float x_fp_old_e(){
	return x_e_fp_old_(matrix,msize);
};
float y_fp_old_e(){
	return y_e_fp_old_(matrix,msize);
};
float th_fp_old_e(){
	return t_e_fp_old_(matrix,msize) - LTXFIT_old_(x_e_fp_old_(matrix,msize));
};
float ph_fp_old_e(){
	return p_e_fp_old_(matrix,msize);
};
float x_fp_old_h(){
	return x_h_fp_old_(matrix,msize);
};
float y_fp_old_h(){
	return y_h_fp_old_(matrix,msize);
};
float th_fp_old_h(){
	return t_h_fp_old_(matrix,msize)  - RTXFIT_old_(x_h_fp_old_(matrix,msize));
};
float ph_fp_old_h(){
	return p_h_fp_old_(matrix,msize);
};

float dp_tg_old_e(){
	return ldelta_old_(matrix,msize);
};
float y_tg_old_e(){
	return ly00_old_(matrix,msize);
};
float th_tg_old_e(){
	return ltheta_old_(matrix,msize);
};
float ph_tg_old_e(){
	return lphi_old_(matrix,msize);
};
float dp_tg_old_h(){
	return rdelta_old_(matrix,msize);
};
float y_tg_old_h(){
	return ry00_old_(matrix,msize);
};
float th_tg_old_h(){
	return rtheta_old_(matrix,msize);
};
float ph_tg_old_h(){
	return rphi_old_(matrix,msize);
};

bool passQ1exOld_e(){
	float x = x_e_q1ex_old_(matrix,msize)*100.0;
	float y = y_e_q1ex_old_(matrix,msize)*100.0;
		
	if(x!=x || y!=y || x*x + y*y > q1r*q1r){
		return false;
	}
	return true;
};
	
bool passDentOld_e(){
	float x = x_e_dent_old_(matrix,msize)*100.0;
	x *= -3.346065;
	float y = y_e_dent_old_(matrix,msize)*100.0;
	
	if(y < 0.) y -= 1.25;
	//y -= 1.25;
	//y -= 1.25*4.0;
	//cout << y << endl;
	
	if(x!=x || y!=y || fabs(x)>DXOld || fabs(y)>DYOld*(1.0-1.25*x/840.0)){
		return false;
	}
	return true;
};

bool passDextOld_e(){
	float x = x_e_dext_old_(matrix,msize)*100.0;
	x *= 0.866025;
	float y = y_e_dext_old_(matrix,msize)*100.0;
	if(y < 0.) y -= 1.25;
	//y -= 1.25;
	//y -= 1.25*4.0;
	
	
	if(x!=x || y!=y || fabs(x)>DXOld || fabs(y)>DYOld*(1.0-1.25*x/840.0)){
		return false;
	}
	return true;
};

bool passQ3enOld_e(){
	float x = x_e_q3en_old_(matrix,msize)*100.0;
	float y = y_e_q3en_old_(matrix,msize)*100.0;
	if(x>0) x += Lq3x_off;
	//if(x < 0.) x -= 10.25;
	//x += 2.0;
	//x += 2.0;
	if(x!=x || y!=y || x*x + y*y > q3r*q3r){
		return false;
	}
	return true;
};

bool passQ3exOld_e(){
	float x = x_e_q3ex_old_(matrix,msize)*100.0;
	float y = y_e_q3ex_old_(matrix,msize)*100.0;
	
	if(x>0) x += Lq3x_off;
	// x += 2.0;
	//x += 2.0;
	//if(x<0) x -= 2.0;
	//if(x>0) x *= 1.0/sqrt(0.7);
	if(x!=x || y!=y || x*x + y*y > q3r*q3r){
		return false;
	}
	return true;
};

bool passQ1exOld_h(){
		float x = x_h_q1ex_old_(matrix,msize)*100.0;
		float y = y_h_q1ex_old_(matrix,msize)*100.0;
		if(x!=x || y!=y || x*x + y*y > q1r*q1r){
			return false;
		}
		return true;
};

bool passDentOld_h(){
			float x = x_h_dent_old_(matrix,msize)*100.0;
			x *= -3.346065;
			float y = y_h_dent_old_(matrix,msize)*100.0;
			//cout << y << endl;
			
			if(y > 0.) y += 0.35;
			if(x!=x || y!=y || fabs(x)>DXOld || fabs(y)>DYOld*(1.0-1.25*x/840.0)){
				return false;
			}
			return true;
};

bool passDextOld_h(){
				float x = x_h_dext_old_(matrix,msize)*100.0;
				x *= 0.866025;
				float y = y_h_dext_old_(matrix,msize)*100.0;
				
				if(y > 0.) y += 0.35;
				if(x!=x || y!=y || fabs(x)>DXOld || fabs(y)>DYOld*(1.0-1.25*x/840.0)){
					return false;
				}
				return true;
};

bool passQ3enOld_h(){
			float x = x_h_q3en_old_(matrix,msize)*100.0;
			float y = y_h_q3en_old_(matrix,msize)*100.0;
			if(x>0) x += Lq3x_off;
			if(x!=x || y!=y || x*x + y*y > q3r*q3r){
				return false;
			}
			return true;
};

bool passQ3exOld_h(){
			float x = x_h_q3ex_old_(matrix,msize)*100.0;
			float y = y_h_q3ex_old_(matrix,msize)*100.0;
			if(x>0) x += Lq3x_off;
			if(x!=x || y!=y || x*x + y*y > q3r*q3r){
				return false;
			}
			return true;
};

bool passAllMagsOld_e(){
	if(!passQ1exOld_e()) return false;
	//cout << "passed Q1 ex" << endl;
	if(!passDentOld_e()) return false;
	//cout << "passed D ent" << endl;
	if(!passDextOld_e()) return false;
	//cout << "passed D ex" << endl;
	if(!passQ3enOld_e()) return false;
	//cout << "passed Q3 ent" << endl;
	if(!passQ3exOld_e()) return false;
	//cout << "passed Q3 ex" << endl;
	return true;
};

bool passAllMagsOld_h(){
	if(!passQ1exOld_h()) return false;
	//cout << "passed Q1 ex" << endl;
	if(!passDentOld_h()) return false;
	//cout << "passed D ent" << endl;
	if(!passDextOld_h()) return false;
	//cout << "passed D ex" << endl;
	if(!passQ3enOld_h()) return false;
	//cout << "passed Q3 ent" << endl;
	if(!passQ3exOld_h()) return false;
	//cout << "passed Q3 ex" << endl;
	return true;
};

  };
