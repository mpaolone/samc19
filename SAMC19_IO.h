//input and output for SAMC19
#include "TXMLEngine.h"
#if (!SAMC19_H)
#include "SAMC19.h"
#endif



class SAMCInput{
public:
	SAMCVars *v;
	SAMCInput(SAMCVars *vin): v{vin}{
		//constructor
	};
	virtual ~SAMCInput(){};
	
	float bt(Material mat){
		
		//particle book equation 27.20
		//Lrad=elastic form factor F_el, scattering on the nucleus
		//Lrad_prime=inelastic form factor F_inel, scattering on the shell electrons
		//f(Z)=Coulomb correction
		
		float lrad = log(184.15*pow(mat.Z,-1./3.));
		float lradp = log(1194.*pow(mat.Z,-2./3.));
		//float eta = log(1440.0*pow(mat.Z,-2./3.))/lrad);
		float eta = lrad/lradp;
		float b = 4./3.*(1.+1./9.*(mat.Z+1.)/(mat.Z+eta)/lrad);
		
		if(mat.TR)
			return b*mat.TR;
		float a = pow(mat.Z/137.0,2);
		float fZ = a*(1./(1+a)+0.20206-0.0369*a+0.0083*a*a-0.002*a*a*a);
		//return b*716.408*mat.A/(mat.Z*mat.Z*(lrad-fZ)+ mat.Z*lradp)*mat.T;
		return b*mat.T/716.408/mat.A*(mat.Z*mat.Z*(lrad-fZ)+ mat.Z*lradp);
	}
	
	void LoadFile(TString filename){
		//Here we parse in the SAMC XML inputfile
		TXMLEngine xml;
		auto xmldoc = xml.ParseFile(filename);
		auto mainnode = xml.DocGetRootElement(xmldoc);
	
		//We have the main node, now we loop through subnodes.
		auto child = xml.GetChild(mainnode);
		while(child !=0 ){
			SAMCNode(xml, child);
			child = xml.GetNext(child);
		}
		
		//ok, if we hav->ve target and material arrays, we need to sort them by zlocation:
		int numTargets = (int)vTargMatname.size();
		if(numTargets){
			//we need to make sure our materials are lined up correctly:
			int ind[numTargets];
			TMath::Sort(numTargets,&vTargMatZloc[0],ind, false);
			for(int i = 0; i < numTargets; i++){
				Material mat;
				mat.name = vTargMatname[ind[i]];
				mat.L = vTargMatL[ind[i]];
				mat.Zloc = vTargMatZloc[ind[i]];
				mat.Z = vTargMatZ[ind[i]];
				mat.A = vTargMatA[ind[i]];
				mat.X0 = vTargMatX0[ind[i]];
				mat.rho = vTargMatRho[ind[i]];
				mat.T = mat.L*mat.rho;
				mat.TR = mat.T/mat.X0;
				mat.bt = bt(mat);
				if((int)vTargMatAng.size())
					mat.angle = vTargMatAng[ind[i]];
				if((int)vTargMatXoff.size())
					mat.xoff = vTargMatXoff[ind[i]];
				
				v->vTarg.push_back(mat);
			}
		}

		//do the same thing for the other materials:
		int numPreMats = (int)vPreMatname.size();
		if(numPreMats){
			//we need to make sure our materials are lined up correctly:
			int ind[numPreMats];
			TMath::Sort(numPreMats,&vPreMatZloc[0],ind, false);
			for(int i = 0; i < numPreMats; i++){
				Material mat;
				mat.name = vPreMatname[ind[i]];
				mat.L = vPreMatL[ind[i]];
				mat.Zloc = vPreMatZloc[ind[i]];
				mat.Z = vPreMatZ[ind[i]];
				mat.A = vPreMatA[ind[i]];
				mat.X0 = vPreMatX0[ind[i]];
				mat.rho = vPreMatRho[ind[i]];
				mat.T = mat.L*mat.rho;
				mat.TR = mat.T/mat.X0;
				mat.bt = bt(mat);
				v->vPreMag.push_back(mat);
			}
		}
		
		int numPostMats = (int)vPostMatname.size();
		if(numPostMats){
			//we need to make sure our materials are lined up correctly:
			int ind[numPostMats];
			TMath::Sort(numPostMats,&vPostMatZloc[0],ind, false);
			for(int i = 0; i < numPostMats; i++){
				Material mat;
				mat.name = vPostMatname[ind[i]];
				mat.L = vPostMatL[ind[i]];
				mat.Zloc = vPostMatZloc[ind[i]];
				mat.Z = vPostMatZ[ind[i]];
				mat.A = vPostMatA[ind[i]];
				mat.X0 = vPostMatX0[ind[i]];
				mat.rho = vPostMatRho[ind[i]];
				mat.T = mat.L*mat.rho;
				mat.TR = mat.T/mat.X0;
				mat.bt = bt(mat);
				v->vPostMag.push_back(mat);
			}
		}
		
		
	};

	//parse and read xml variables.  See test.xml
	void SAMCNode(TXMLEngine &xml, XMLNodePointer_t node){
		TString nodeName = (TString)xml.GetNodeName(node);
		auto attr = xml.GetFirstAttr(node);
		while(attr != 0){
			if(nodeName.EqualTo("Header")){
				while(attr !=0){
					if(((TString)xml.GetAttrName(attr)).EqualTo("beamE"))
						v->beamE = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("centP"))
						v->centP = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("angle"))
						v->angle = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("matOpt"))
						v->matOpt = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("opticsFile"))
						v->opticsFile = TString(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("outputFile"))
						v->outputFile = TString(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("inputFile"))
						v->inputFile = TString(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("numEvents"))
						v->numEvents = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("setSeed"))
						v->setSeed = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("spectrometerOpt"))
						v->spectrometerOpt = TString(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("dpGenLim"))
						v->dpGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("thGenLim"))
						v->thGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("phGenLim"))
						v->phGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("thGenLim"))
						v->thGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("transOpt"))
						v->transOpt = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("reconOpt"))
						v->reconOpt = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("bxGenLim"))
						v->bxGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("byGenLim"))
						v->byGenLim = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("bxCent"))
						v->bxCent = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("byCent"))
						v->byCent = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("missPointX"))
						v->missPointX = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("missPointY"))
						v->missPointY = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("IsMultiScat"))
						v->IsMultiScat = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("IsEnergyLoss"))
						v->IsEnergyLoss = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("numWeights"))
						v->numWeights = atoi(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("resScale"))
						v->resScale = atof(xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("resOff"))
						v->resOff = atof(xml.GetAttrValue(attr));
					attr = xml.GetNextAttr(attr);
				}
			}
			
			if(nodeName.EqualTo("TargetMaterial")){
				while(attr !=0){
					if(((TString)xml.GetAttrName(attr)).EqualTo("Name"))
						vTargMatname.push_back((TString)xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("L"))
						vTargMatL.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Zloc"))
						vTargMatZloc.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Z"))
						vTargMatZ.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("A"))
						vTargMatA.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("X0"))
						vTargMatX0.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Rho"))
						vTargMatRho.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("targAng"))
						vTargMatAng.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Xoff"))
						vTargMatXoff.push_back(atof(xml.GetAttrValue(attr)));
					attr = xml.GetNextAttr(attr);
				}
			}
			
			if(nodeName.EqualTo("PreMagMaterial")){
				while(attr !=0){
					if(((TString)xml.GetAttrName(attr)).EqualTo("Name"))
						vPreMatname.push_back((TString)xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("L"))
						vPreMatL.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Zloc"))
						vPreMatZloc.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Z"))
						vPreMatZ.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("A"))
						vPreMatA.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("X0"))
						vPreMatX0.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Rho"))
						vPreMatRho.push_back(atof(xml.GetAttrValue(attr)));
					attr = xml.GetNextAttr(attr);
				}
			}
		
			if(nodeName.EqualTo("PostMagMaterial")){
				while(attr !=0){
					if(((TString)xml.GetAttrName(attr)).EqualTo("Name"))
						vPostMatname.push_back((TString)xml.GetAttrValue(attr));
					if(((TString)xml.GetAttrName(attr)).EqualTo("L"))
						vPostMatL.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Zloc"))
						vPostMatZloc.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Z"))
						vPostMatZ.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("A"))
						vPostMatA.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("X0"))
						vPostMatX0.push_back(atof(xml.GetAttrValue(attr)));
					if(((TString)xml.GetAttrName(attr)).EqualTo("Rho"))
						vPostMatRho.push_back(atof(xml.GetAttrValue(attr)));
					attr = xml.GetNextAttr(attr);
				}
			}
			
			attr = xml.GetNextAttr(attr);
		}
	};
private:
	vector<TString> vTargMatname = {};
	vector<TString> vPreMatname = {};
	vector<TString> vPostMatname = {};

	vector<float> vPreMatL = {};
	vector<float> vPreMatZloc = {};
	vector<float> vPreMatZ = {};
	vector<float> vPreMatA = {};
	vector<float> vPreMatX0 = {};
	vector<float> vPreMatRho = {};
	
	
	vector<float> vPostMatL = {};
	vector<float> vPostMatZloc = {};
	vector<float> vPostMatZ = {};
	vector<float> vPostMatA = {};
	vector<float> vPostMatX0 = {};
	vector<float> vPostMatRho = {};
	
	vector<float> vTargMatL = {};
	vector<float> vTargMatZloc = {};
	vector<float> vTargMatZ = {};
	vector<float> vTargMatA = {};
	vector<float> vTargMatX0 = {};
	vector<float> vTargMatRho = {};
	vector<float> vTargMatAng = {};
	vector<float> vTargMatXoff = {};
};

//output handles tree creation, filling, writing.
class SAMCOutput{
public:
	SAMCVars *v = {};
	SAMCRootVars *rv ={};
	TTree *T = {};
	TTree *Header = {};
	TFile *ofile = {};
	SAMCOutput(SAMCVars *vin, SAMCRootVars *rvin): v(vin), rv(rvin){
		//constructor
		
		//create header tree and branches and add one event with header values:
		ofile = new TFile(v->outputFile,"RECREATE");
		Header = new TTree("Header","Header info");
		Header->Branch("numEvents",&v->numEvents,"numEvents/I");
		Header->Branch("setSeed",&v->setSeed,"setSeed/I");
		Header->Branch("beamE",&v->beamE,"beamE/F");
		Header->Branch("centP",&v->centP,"centP/F");
		Header->Branch("angle",&v->angle,"angle/F");
		Header->Branch("dpGenLim",&v->dpGenLim,"dpGenLim/F");
		Header->Branch("thGenLim",&v->thGenLim,"thGenLim/F");
		Header->Branch("phGenLim",&v->phGenLim,"phGenLim/F");
		Header->Branch("resScale",&v->resScale,"resScale/F");
		Header->Branch("resOff",&v->resOff,"resOff/F");
		Header->Branch("missPointX",&v->missPointX,"missPointX/F");
		Header->Branch("missPointY",&v->missPointY,"missPointY/F");
		Header->Branch("missPointY",&v->spectrometerOpt,"spectrometerOpt");
		Header->Branch("IsMultScat",&v->IsMultiScat,"IsMultScat/I");
		Header->Branch("IsEnergyLoss",&v->IsEnergyLoss,"IsEnergyLoss/I");
		Header->Branch("transOpt",&v->transOpt,"transOpt/I");
		Header->Branch("reconOpt",&v->reconOpt,"reconOpt/I");
		Header->Fill();
		//Header->Write();
		
		T = new TTree("SAMC","Event data tree");
		T->Branch("dp_gen",&rv->dpgen,"dp_gen/F");
		T->Branch("th_tg_gen",&rv->thgen,"th_tg_gen/F");
		T->Branch("ph_tg_gen",&rv->phgen,"ph_tg_gen/F");
		
		T->Branch("dp_ref",&rv->dp_ref,"dp_ref/F");
		T->Branch("th_tg_ref",&rv->th_tg_ref,"th_tg_ref/F");
		T->Branch("ph_tg_ref",&rv->ph_tg_ref,"ph_tg_ref/F");
		T->Branch("y_tg_ref",&rv->y_tg_ref,"y_tg_ref/F");
		
		T->Branch("dp_rec",&rv->dp_rec,"dp_rec/F");
		T->Branch("th_tg_rec",&rv->th_tg_rec,"th_tg_rec/F");
		T->Branch("ph_tg_rec",&rv->ph_tg_rec,"ph_tg_rec/F");
		T->Branch("y_tg_rec",&rv->y_tg_rec,"y_tg_rec/F");
		
		T->Branch("xd",&rv->xd,"xd/F");
		T->Branch("yd",&rv->yd,"yd/F");
		T->Branch("yd2",&rv->yd2,"yd2/F");
		T->Branch("thd",&rv->thd,"thd/F");
		T->Branch("phd",&rv->phd,"phd/F");
		T->Branch("phd2",&rv->phd2,"phd2/F");
		
		T->Branch("x_fp",&rv->x_fp,"x_fp/F");
		T->Branch("y_fp",&rv->y_fp,"y_fp/F");
		T->Branch("th_fp",&rv->th_fp,"th_fp/F");
		T->Branch("ph_fp",&rv->ph_fp,"ph_fp/F");
		
		T->Branch("beamx",&rv->beamx,"beamx/F");
		T->Branch("beamy",&rv->beamy,"beamy/F");
		T->Branch("zvert",&rv->zvert,"zvert/F");
		T->Branch("IsPassed",&rv->IsPassed,"IsPassed/I");
		if(v->numWeights){
			T->Branch("weight",&rv->weight);
		}
		
		
	};
	void updateVars(SAMCRootVars *rvin){
		*rv = *rvin;
	};
	void fillTree(){
		T->Fill();
	};
	void writeToTree(){
		ofile->Write();
	};
};	
