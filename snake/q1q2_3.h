float x_e_q1q2_3_1200                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.9178525E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.92003460E-03, 0.12088735E+00, 0.38046530E-02, 0.64631359E-03,
         0.12442376E-03,-0.56613825E-03,-0.11804820E-02,-0.90999989E-03,
        -0.24852276E-03,-0.34677805E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_1200                              =v_x_e_q1q2_3_1200                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_1200                              ;
}
float t_e_q1q2_3_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6903861E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.74867598E-05,-0.19907241E-02,-0.97752630E-03, 0.22902687E-02,
        -0.10482469E-02,-0.96476689E-03, 0.52115965E-05,-0.24516322E-03,
         0.71993170E-04,-0.28597596E-03,-0.39249187E-03,-0.86040040E-04,
        -0.81969070E-03,-0.64111978E-03,-0.19863450E-04,-0.23514929E-04,
        -0.18212006E-04, 0.82931401E-04, 0.53742879E-04,-0.46825796E-03,
         0.16247190E-04,-0.11107171E-04, 0.95978976E-05,-0.62101731E-05,
         0.11440678E-04, 0.13299315E-04, 0.33771184E-05,-0.26279257E-03,
        -0.55387878E-03, 0.10646382E-04, 0.15243598E-04,-0.94635279E-05,
         0.44072913E-05, 0.78205849E-05, 0.14917666E-05,-0.38652179E-05,
         0.47268368E-05,-0.49057148E-05,-0.90270460E-05, 0.21503993E-04,
         0.84667527E-05, 0.61577175E-05, 0.11795948E-04,-0.60945575E-04,
         0.83771047E-05,-0.49993196E-04,-0.11237495E-04, 0.93765666E-05,
         0.60069156E-05,-0.95443120E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_1200                              =v_t_e_q1q2_3_1200                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_1200                              =v_t_e_q1q2_3_1200                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23*x32    *x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x21*x31    *x52
        +coeff[ 25]    *x21        *x53
    ;
    v_t_e_q1q2_3_1200                              =v_t_e_q1q2_3_1200                              
        +coeff[ 26]*x11*x22*x32        
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]    *x23    *x43    
        +coeff[ 30]*x12*x22*x31    *x51
        +coeff[ 31]*x11*x21            
        +coeff[ 32]    *x22*x31        
        +coeff[ 33]*x13*x21            
        +coeff[ 34]        *x33*x41    
    ;
    v_t_e_q1q2_3_1200                              =v_t_e_q1q2_3_1200                              
        +coeff[ 35]    *x22    *x42    
        +coeff[ 36]*x13            *x51
        +coeff[ 37]*x12*x21        *x51
        +coeff[ 38]*x11*x22        *x51
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x11*x21        *x52
        +coeff[ 41]*x12*x23            
        +coeff[ 42]*x12*x21*x31*x41    
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_q1q2_3_1200                              =v_t_e_q1q2_3_1200                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x13*x21        *x51
        +coeff[ 47]*x11*x23        *x51
        +coeff[ 48]*x12*x21*x31    *x51
        +coeff[ 49]*x11*x21*x32    *x51
        ;

    return v_t_e_q1q2_3_1200                              ;
}
float y_e_q1q2_3_1200                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.1378154E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.56221776E-04, 0.16923660E+00, 0.10627945E+00,-0.24836196E-02,
        -0.22641383E-02, 0.31746423E-03,-0.15749515E-03,-0.14069134E-03,
         0.20280972E-03, 0.13084634E-03, 0.10899173E-03, 0.95773968E-04,
        -0.32041187E-03,-0.33564127E-05,-0.22786619E-04, 0.56966172E-04,
        -0.25948443E-03,-0.21369477E-03, 0.10282567E-05, 0.26423074E-05,
         0.13060369E-04, 0.49906626E-04, 0.18625995E-04,-0.14559625E-03,
        -0.65537890E-04,-0.80544123E-04,-0.52211617E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_1200                              =v_y_e_q1q2_3_1200                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q1q2_3_1200                              =v_y_e_q1q2_3_1200                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]    *x22    *x43*x51
        +coeff[ 19]                *x51
        +coeff[ 20]    *x21*x32*x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]        *x33    *x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23    *x41    
    ;
    v_y_e_q1q2_3_1200                              =v_y_e_q1q2_3_1200                              
        +coeff[ 26]*x11*x23*x31        
        ;

    return v_y_e_q1q2_3_1200                              ;
}
float p_e_q1q2_3_1200                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.7884663E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.47215155E-04, 0.30969812E-01, 0.64586088E-01,-0.14594954E-02,
         0.88000834E-05,-0.13236193E-02, 0.40811839E-03, 0.24355319E-03,
         0.22292495E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_1200                              =v_p_e_q1q2_3_1200                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_1200                              ;
}
float l_e_q1q2_3_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2173789E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21930980E-02,-0.35488112E-02,-0.91850641E-03,-0.36088757E-02,
        -0.41300533E-02,-0.29383117E-03, 0.77340222E-03,-0.14239448E-02,
         0.17780589E-03, 0.59740702E-04, 0.36379311E-04, 0.52475185E-04,
        -0.37644093E-03,-0.11858885E-03,-0.47356746E-03,-0.28437620E-03,
        -0.46741520E-03,-0.13584311E-02,-0.25542435E-03, 0.11305628E-03,
         0.28649057E-03,-0.27105963E-03,-0.38363365E-03,-0.27173481E-03,
         0.62952010E-03,-0.45201901E-04,-0.29880933E-02,-0.11575719E-02,
        -0.13296637E-02,-0.56322984E-03,-0.13313093E-02, 0.20860764E-02,
         0.60158269E-03, 0.38775208E-02, 0.34741987E-03, 0.14424351E-02,
        -0.90162922E-03, 0.50807220E-03,-0.66324143E-03,-0.25920633E-02,
         0.16429282E-02, 0.74119150E-03,-0.65284013E-03,-0.19829364E-02,
         0.15024397E-02, 0.46567773E-03,-0.71861676E-03, 0.10439896E-02,
         0.90595458E-04, 0.96184300E-03,-0.53223834E-03,-0.28466212E-03,
        -0.68292778E-03,-0.12094679E-02,-0.77689055E-03, 0.26414951E-02,
         0.12648827E-02, 0.16002050E-02,-0.99641178E-03,-0.25801142E-02,
         0.16643227E-02, 0.23096791E-02, 0.11099018E-02,-0.13605435E-02,
        -0.11332579E-02, 0.33319376E-02,-0.79898856E-03, 0.76517806E-03,
        -0.18358682E-02, 0.28447311E-02,-0.38694793E-02,-0.24439520E-02,
         0.97212405E-03, 0.11287769E-02,-0.25648037E-02,-0.81798679E-03,
         0.14500959E-02, 0.29486560E-02, 0.46089091E-04,-0.32739317E-04,
         0.68256537E-04,-0.97461605E-04, 0.22792684E-03,-0.68716290E-04,
        -0.34259105E-03, 0.14545429E-03,-0.66310557E-03, 0.17055139E-03,
         0.16990985E-03, 0.13665011E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21        *x51
        +coeff[  6]*x11*x23*x32        
        +coeff[  7]*x12    *x32*x41*x51
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[  8]        *x31        
        +coeff[  9]                *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11            *x51
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21    *x41*x51
        +coeff[ 15]        *x33*x41    
        +coeff[ 16]            *x44    
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 17]*x11    *x32*x41    
        +coeff[ 18]*x11    *x31*x42    
        +coeff[ 19]*x11        *x43    
        +coeff[ 20]*x11*x22        *x51
        +coeff[ 21]*x11    *x31*x41*x51
        +coeff[ 22]*x11*x21        *x52
        +coeff[ 23]*x11        *x41*x52
        +coeff[ 24]*x12    *x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x33*x41*x51
        +coeff[ 29]    *x21*x31*x42*x51
        +coeff[ 30]        *x32*x42*x51
        +coeff[ 31]        *x31*x43*x51
        +coeff[ 32]*x11    *x34        
        +coeff[ 33]*x11*x22*x31*x41    
        +coeff[ 34]*x12*x21*x32        
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 35]*x12*x21*x31*x41    
        +coeff[ 36]*x12*x21    *x42    
        +coeff[ 37]*x12*x21*x31    *x51
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]        *x31*x44*x51
        +coeff[ 40]        *x31*x42*x53
        +coeff[ 41]*x11*x24    *x41    
        +coeff[ 42]*x11*x22    *x43    
        +coeff[ 43]*x11*x21*x31*x42*x51
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 44]*x11    *x32*x41*x52
        +coeff[ 45]*x11*x21*x31    *x53
        +coeff[ 46]*x11*x21    *x41*x53
        +coeff[ 47]*x11    *x31*x41*x53
        +coeff[ 48]*x12*x24            
        +coeff[ 49]*x12*x22        *x52
        +coeff[ 50]*x12    *x31    *x53
        +coeff[ 51]*x12            *x54
        +coeff[ 52]    *x23*x33    *x51
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 53]    *x22*x34    *x51
        +coeff[ 54]    *x22*x31*x43*x51
        +coeff[ 55]        *x32*x44*x51
        +coeff[ 56]    *x22*x32    *x53
        +coeff[ 57]    *x22*x31    *x54
        +coeff[ 58]        *x33    *x54
        +coeff[ 59]*x11*x24*x31*x41    
        +coeff[ 60]*x11    *x34*x42    
        +coeff[ 61]*x11    *x33*x41*x52
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 62]*x11*x22    *x42*x52
        +coeff[ 63]*x11        *x44*x52
        +coeff[ 64]*x11    *x31*x41*x54
        +coeff[ 65]*x12*x21*x32*x42    
        +coeff[ 66]*x12*x22*x32    *x51
        +coeff[ 67]*x12*x21    *x41*x53
        +coeff[ 68]*x13*x22*x31*x41    
        +coeff[ 69]    *x23*x34    *x51
        +coeff[ 70]    *x21*x34*x42*x51
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 71]        *x34*x43*x51
        +coeff[ 72]    *x24*x31*x41*x52
        +coeff[ 73]*x13        *x42*x52
        +coeff[ 74]    *x23*x32    *x53
        +coeff[ 75]    *x23*x31*x41*x53
        +coeff[ 76]        *x34*x41*x53
        +coeff[ 77]    *x21*x32*x42*x53
        +coeff[ 78]    *x21            
        +coeff[ 79]*x11                
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 80]        *x31    *x51
        +coeff[ 81]            *x41*x51
        +coeff[ 82]*x11        *x41    
        +coeff[ 83]    *x23            
        +coeff[ 84]    *x22*x31        
        +coeff[ 85]        *x33        
        +coeff[ 86]    *x21*x31*x41    
        +coeff[ 87]    *x21    *x42    
        +coeff[ 88]    *x21*x31    *x51
    ;
    v_l_e_q1q2_3_1200                              =v_l_e_q1q2_3_1200                              
        +coeff[ 89]        *x32    *x51
        ;

    return v_l_e_q1q2_3_1200                              ;
}
float x_e_q1q2_3_1100                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.8046808E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.79129712E-03, 0.12089733E+00, 0.38039575E-02, 0.64746401E-03,
         0.12465887E-03,-0.56312175E-03,-0.11825180E-02,-0.91040408E-03,
        -0.24641730E-03,-0.35072915E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_1100                              =v_x_e_q1q2_3_1100                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_1100                              ;
}
float t_e_q1q2_3_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1454849E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.13642234E-04,-0.19893425E-02,-0.97718427E-03, 0.22941281E-02,
        -0.10327096E-02,-0.95533830E-03, 0.62242871E-05,-0.27238275E-03,
         0.71520612E-04,-0.27586040E-03,-0.37465489E-03,-0.81232640E-04,
        -0.86052890E-03,-0.65518491E-03,-0.25429248E-04,-0.37073834E-04,
        -0.30317491E-04, 0.96775089E-04, 0.63478175E-04,-0.83285004E-05,
        -0.24578354E-03,-0.47310890E-03, 0.85604106E-05, 0.39677008E-04,
        -0.66116704E-05,-0.14646405E-04,-0.34968698E-05, 0.36957581E-05,
        -0.20925521E-04,-0.56299655E-03,-0.20132586E-04, 0.17844744E-04,
         0.18074352E-04,-0.10308372E-04, 0.25956849E-05,-0.31635986E-05,
        -0.50485983E-05, 0.64513956E-05,-0.37056075E-05,-0.41006333E-05,
         0.35564615E-05,-0.28071177E-05, 0.25365848E-05, 0.30877109E-05,
         0.55367304E-05,-0.58600504E-05, 0.70396954E-05, 0.10709751E-04,
         0.96952881E-05, 0.82740798E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_1100                              =v_t_e_q1q2_3_1100                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_1100                              =v_t_e_q1q2_3_1100                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x11*x22*x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]*x12*x21*x32    *x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x22*x31        
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_3_1100                              =v_t_e_q1q2_3_1100                              
        +coeff[ 26]*x13*x21            
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21    *x42*x52
        +coeff[ 31]*x13*x23            
        +coeff[ 32]*x12*x22        *x52
        +coeff[ 33]    *x22*x31    *x53
        +coeff[ 34]        *x31        
    ;
    v_t_e_q1q2_3_1100                              =v_t_e_q1q2_3_1100                              
        +coeff[ 35]    *x22            
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11*x21*x31        
        +coeff[ 38]        *x33        
        +coeff[ 39]*x11*x21    *x41    
        +coeff[ 40]        *x31*x42    
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]*x11    *x31    *x51
        +coeff[ 43]    *x21*x31    *x51
    ;
    v_t_e_q1q2_3_1100                              =v_t_e_q1q2_3_1100                              
        +coeff[ 44]*x12    *x31*x41    
        +coeff[ 45]*x11*x21    *x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x11    *x31*x41*x51
        +coeff[ 49]*x11        *x42*x51
        ;

    return v_t_e_q1q2_3_1100                              ;
}
float y_e_q1q2_3_1100                              (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.2000726E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.29732010E-03, 0.16921715E+00, 0.10631272E+00,-0.24886399E-02,
        -0.22566624E-02, 0.27127686E-03,-0.18562487E-03,-0.18016263E-03,
         0.17777809E-03, 0.10577586E-03,-0.30596912E-04, 0.90142996E-04,
         0.75682256E-04,-0.27141898E-03,-0.41234372E-04, 0.60578383E-04,
        -0.26874078E-03, 0.25181698E-05, 0.11199451E-04,-0.15200294E-04,
        -0.14499034E-05, 0.40866980E-04, 0.25072313E-04, 0.65123852E-04,
        -0.10568440E-03,-0.81594379E-04,-0.42666357E-04, 0.49443526E-04,
        -0.20423911E-04, 0.34095912E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_1100                              =v_y_e_q1q2_3_1100                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q1q2_3_1100                              =v_y_e_q1q2_3_1100                              
        +coeff[ 17]                *x51
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]        *x32*x41*x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_3_1100                              =v_y_e_q1q2_3_1100                              
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]    *x22    *x41*x52
        +coeff[ 28]    *x21*x33*x42    
        +coeff[ 29]        *x33    *x52
        ;

    return v_y_e_q1q2_3_1100                              ;
}
float p_e_q1q2_3_1100                              (float *x,int m){
    int ncoeff=  9;
    float avdat=  0.1418888E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
        -0.52812215E-04, 0.30976944E-01, 0.64576119E-01,-0.14603697E-02,
        -0.10188422E-05,-0.13203443E-02, 0.39881171E-03, 0.23712772E-03,
         0.21858109E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_1100                              =v_p_e_q1q2_3_1100                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_1100                              ;
}
float l_e_q1q2_3_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2163121E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21925634E-02,-0.96623640E-04,-0.35108014E-02,-0.56678074E-03,
        -0.34263942E-02,-0.43628602E-02,-0.54353173E-03, 0.74038480E-03,
        -0.12795112E-03,-0.14604932E-03, 0.12342184E-03,-0.23188569E-03,
        -0.20104942E-03,-0.25321920E-04,-0.70464710E-04, 0.10458081E-04,
         0.60594524E-03,-0.18022675E-03, 0.51971956E-03, 0.13210647E-02,
         0.19312231E-03, 0.14711337E-03,-0.40667501E-03, 0.14338746E-02,
        -0.59497630E-03,-0.84966648E-03,-0.14350472E-03, 0.81396103E-03,
        -0.42672033E-03, 0.32320333E-03, 0.32923059E-03, 0.10416901E-03,
         0.51671756E-03,-0.42508012E-02, 0.15794302E-03,-0.53381763E-03,
        -0.11121104E-02,-0.28074681E-03,-0.21669206E-02,-0.64238440E-03,
         0.11732410E-03,-0.40990772E-03, 0.37659620E-03, 0.65850484E-03,
         0.34297106E-02,-0.89030329E-03,-0.17174509E-02,-0.10801697E-02,
         0.52881602E-03, 0.43341977E-03,-0.16205208E-02,-0.32815285E-03,
         0.91099000E-03, 0.11566333E-02, 0.93911990E-03, 0.74898574E-03,
        -0.50962321E-03,-0.93055813E-03, 0.38563245E-03, 0.72594220E-03,
         0.50812931E-03, 0.10261282E-02, 0.30496893E-02,-0.58008690E-03,
        -0.10106822E-02, 0.72754192E-03, 0.24255731E-02,-0.19267297E-02,
         0.17932313E-02, 0.93818177E-03, 0.27400542E-02,-0.18764563E-02,
        -0.14319245E-02, 0.17045233E-02, 0.37281075E-02, 0.78239839E-03,
        -0.11980315E-02, 0.11711080E-02, 0.12917090E-02,-0.96725801E-03,
         0.11126759E-03,-0.12035639E-02,-0.13550867E-02,-0.13377932E-02,
         0.75908256E-03,-0.39339713E-02, 0.13772907E-02, 0.16859733E-02,
         0.71941421E-03,-0.32557677E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x23*x33        
        +coeff[  7]*x13*x24            
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[  8]*x11                
        +coeff[  9]    *x21*x31        
        +coeff[ 10]        *x31    *x51
        +coeff[ 11]*x12                
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x24            
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x21*x31*x42    
        +coeff[ 24]*x11*x22*x31        
        +coeff[ 25]*x11*x21*x32        
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 26]*x11        *x43    
        +coeff[ 27]*x11*x21*x31    *x51
        +coeff[ 28]*x11    *x32    *x51
        +coeff[ 29]*x11            *x53
        +coeff[ 30]*x12*x22            
        +coeff[ 31]*x13*x21            
        +coeff[ 32]    *x22*x32    *x51
        +coeff[ 33]    *x21*x31*x42*x51
        +coeff[ 34]            *x44*x51
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 35]    *x21*x31    *x53
        +coeff[ 36]        *x31    *x54
        +coeff[ 37]*x11*x21*x33        
        +coeff[ 38]*x11    *x32*x42    
        +coeff[ 39]*x11    *x31*x43    
        +coeff[ 40]*x12    *x33        
        +coeff[ 41]*x12    *x31*x41*x51
        +coeff[ 42]*x12*x21        *x52
        +coeff[ 43]*x13    *x32        
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 44]    *x22*x31*x41*x52
        +coeff[ 45]        *x33*x41*x52
        +coeff[ 46]    *x22    *x42*x52
        +coeff[ 47]    *x21*x31*x42*x52
        +coeff[ 48]    *x21    *x43*x52
        +coeff[ 49]            *x44*x52
        +coeff[ 50]        *x31*x42*x53
        +coeff[ 51]            *x43*x53
        +coeff[ 52]        *x31*x41*x54
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 53]*x11    *x34    *x51
        +coeff[ 54]*x11*x22*x31    *x52
        +coeff[ 55]*x11*x21*x31*x41*x52
        +coeff[ 56]*x11*x22        *x53
        +coeff[ 57]*x11*x21*x31    *x53
        +coeff[ 58]*x12*x22*x31    *x51
        +coeff[ 59]*x12    *x31*x42*x51
        +coeff[ 60]*x12*x21*x31    *x52
        +coeff[ 61]*x13*x21*x32        
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 62]    *x21*x33*x42*x51
        +coeff[ 63]*x13            *x53
        +coeff[ 64]    *x21*x33    *x53
        +coeff[ 65]    *x22    *x42*x53
        +coeff[ 66]    *x21*x31*x42*x53
        +coeff[ 67]*x11    *x33*x43    
        +coeff[ 68]*x11*x23    *x42*x51
        +coeff[ 69]*x11*x21*x32*x42*x51
        +coeff[ 70]*x11*x21*x31*x43*x51
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 71]*x11*x22*x32    *x52
        +coeff[ 72]*x11*x22*x31*x41*x52
        +coeff[ 73]*x11*x22    *x42*x52
        +coeff[ 74]*x11    *x31*x43*x52
        +coeff[ 75]*x11        *x44*x52
        +coeff[ 76]*x11    *x31*x41*x54
        +coeff[ 77]*x12*x21*x33*x41    
        +coeff[ 78]*x12*x21*x31*x42*x51
        +coeff[ 79]*x12    *x33    *x52
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 80]*x13*x22*x31*x41    
        +coeff[ 81]*x13*x22    *x42    
        +coeff[ 82]    *x23*x33*x42    
        +coeff[ 83]        *x34*x44    
        +coeff[ 84]        *x33*x44*x51
        +coeff[ 85]    *x24*x31*x41*x52
        +coeff[ 86]    *x22*x32*x42*x52
        +coeff[ 87]        *x32*x44*x52
        +coeff[ 88]    *x23*x31    *x54
    ;
    v_l_e_q1q2_3_1100                              =v_l_e_q1q2_3_1100                              
        +coeff[ 89]    *x21            
        ;

    return v_l_e_q1q2_3_1100                              ;
}
float x_e_q1q2_3_1000                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.5666575E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.53247862E-03, 0.12087952E+00, 0.38054851E-02, 0.64681447E-03,
         0.12408569E-03,-0.56276296E-03,-0.11735518E-02,-0.91013702E-03,
        -0.24621948E-03,-0.34251832E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_1000                              =v_x_e_q1q2_3_1000                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_1000                              ;
}
float t_e_q1q2_3_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.9266229E-05;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.88951974E-05,-0.19901644E-02,-0.96803374E-03, 0.71105293E-04,
         0.22872698E-02,-0.10639658E-02,-0.96480676E-03,-0.77915201E-05,
        -0.26719444E-03,-0.27580731E-03,-0.38037266E-03,-0.88000081E-04,
        -0.83962129E-03,-0.65064681E-03,-0.21509544E-04,-0.26574877E-04,
        -0.22396873E-04, 0.10113720E-03, 0.57681780E-04,-0.21087193E-03,
        -0.44985182E-03,-0.65002796E-05,-0.13953982E-04, 0.31237465E-04,
        -0.45946865E-04,-0.26755502E-04,-0.52213523E-03,-0.14386255E-04,
         0.41045849E-04, 0.17473403E-04, 0.17798253E-05, 0.52548721E-05,
         0.29637440E-05, 0.79086908E-06,-0.38383714E-05,-0.25081463E-05,
        -0.27066089E-05,-0.92717664E-05,-0.59411341E-05, 0.32001194E-05,
        -0.32377654E-05, 0.64992337E-05, 0.52291057E-05,-0.87028138E-05,
         0.33296853E-05, 0.52895621E-05,-0.11584505E-04, 0.86320770E-05,
        -0.10024993E-04, 0.58875157E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_1000                              =v_t_e_q1q2_3_1000                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_1000                              =v_t_e_q1q2_3_1000                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]*x12*x21*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1q2_3_1000                              =v_t_e_q1q2_3_1000                              
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x22*x33    *x51
        +coeff[ 28]    *x21*x32*x42*x51
        +coeff[ 29]*x12*x21        *x53
        +coeff[ 30]*x11    *x31        
        +coeff[ 31]    *x22*x31        
        +coeff[ 32]    *x22    *x41    
        +coeff[ 33]    *x21*x31    *x51
        +coeff[ 34]*x11            *x52
    ;
    v_t_e_q1q2_3_1000                              =v_t_e_q1q2_3_1000                              
        +coeff[ 35]        *x31    *x52
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]*x11    *x33        
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]    *x23    *x41    
        +coeff[ 41]    *x21*x31*x42    
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]    *x22    *x41*x51
    ;
    v_t_e_q1q2_3_1000                              =v_t_e_q1q2_3_1000                              
        +coeff[ 44]            *x43*x51
        +coeff[ 45]*x11            *x53
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]*x11*x21*x33        
        +coeff[ 48]*x12*x21*x31    *x51
        +coeff[ 49]*x11*x22*x31    *x51
        ;

    return v_t_e_q1q2_3_1000                              ;
}
float y_e_q1q2_3_1000                              (float *x,int m){
    int ncoeff= 27;
    float avdat=  0.2368036E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
        -0.20058212E-03, 0.16912837E+00, 0.10630351E+00,-0.24753807E-02,
        -0.22601464E-02, 0.28550229E-03,-0.19995346E-03,-0.17105846E-03,
         0.18057173E-03, 0.88340945E-04, 0.10635136E-03, 0.97785174E-04,
        -0.28930570E-03, 0.53682975E-05,-0.19221410E-04, 0.68392095E-04,
        -0.26168910E-03,-0.10047171E-03,-0.19902909E-04, 0.92084520E-04,
         0.13410024E-04, 0.57151086E-04,-0.17440199E-04,-0.45055360E-04,
         0.38816066E-04,-0.62332372E-04, 0.29175839E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_1000                              =v_y_e_q1q2_3_1000                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q1q2_3_1000                              =v_y_e_q1q2_3_1000                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]    *x22    *x43*x51
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x22    *x43    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x21*x31*x44    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_3_1000                              =v_y_e_q1q2_3_1000                              
        +coeff[ 26]    *x23*x32*x41    
        ;

    return v_y_e_q1q2_3_1000                              ;
}
float p_e_q1q2_3_1000                              (float *x,int m){
    int ncoeff=  9;
    float avdat=  0.3494804E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
        -0.21574611E-04, 0.30972866E-01, 0.64538136E-01,-0.14617451E-02,
         0.12253950E-04,-0.13273356E-02, 0.40475017E-03, 0.23716825E-03,
         0.22298585E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_1000                              =v_p_e_q1q2_3_1000                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_1000                              ;
}
float l_e_q1q2_3_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2143560E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21548308E-02,-0.34901174E-02,-0.29680313E-03,-0.33815557E-02,
        -0.43944209E-02,-0.67069253E-04,-0.63538580E-03,-0.25325979E-03,
         0.59272331E-03,-0.38634025E-05,-0.18036815E-04, 0.48586010E-03,
         0.74569311E-03, 0.37522669E-03,-0.58053603E-03, 0.50855894E-03,
        -0.32694309E-03, 0.50973834E-03, 0.11214022E-03,-0.12918634E-03,
        -0.60006679E-03,-0.90940570E-03,-0.54492155E-03, 0.67310012E-03,
        -0.16255737E-02, 0.15940501E-02, 0.58804796E-03, 0.18027787E-03,
        -0.11297797E-02,-0.32042150E-03,-0.44237066E-03, 0.19634741E-02,
         0.21830271E-02,-0.31837789E-03,-0.58220234E-03, 0.76386245E-03,
         0.28193375E-04, 0.30058023E-03, 0.46061617E-04, 0.21659976E-02,
         0.86218788E-04, 0.71846368E-03,-0.13753744E-02,-0.74962946E-03,
         0.89027069E-03, 0.10013132E-02,-0.57560590E-03,-0.14443541E-02,
         0.51872880E-03, 0.12853806E-03,-0.24115641E-02, 0.13381545E-02,
        -0.84078975E-03,-0.10613633E-02, 0.24875678E-03, 0.40762691E-03,
        -0.49412122E-03,-0.20278931E-03,-0.11708534E-03, 0.29845845E-04,
         0.14713661E-03, 0.14796785E-03,-0.88700981E-04, 0.25364722E-03,
        -0.10664473E-03, 0.40303852E-03,-0.96368844E-04, 0.61096682E-04,
         0.17337203E-03, 0.33787597E-03,-0.25168218E-03, 0.19814866E-03,
        -0.12577955E-03, 0.23082840E-03, 0.25262521E-03,-0.10691722E-03,
        -0.15592622E-03, 0.17332328E-03, 0.10755366E-03,-0.36784887E-03,
         0.94656937E-03,-0.10738743E-02, 0.73177146E-03,-0.12836589E-02,
        -0.18460830E-03,-0.67903187E-04, 0.25092001E-03, 0.19519028E-03,
        -0.12715039E-03,-0.35922235E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x34        
        +coeff[  7]    *x21*x31    *x51
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[  8]*x11*x21    *x41    
        +coeff[  9]*x11*x21        *x51
        +coeff[ 10]*x11        *x41*x51
        +coeff[ 11]*x12            *x51
        +coeff[ 12]    *x23*x31        
        +coeff[ 13]    *x21*x33        
        +coeff[ 14]        *x34        
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]        *x33    *x51
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 17]    *x21*x31    *x52
        +coeff[ 18]*x11*x23            
        +coeff[ 19]*x12*x21        *x51
        +coeff[ 20]    *x24        *x51
        +coeff[ 21]*x11*x21    *x43    
        +coeff[ 22]*x12    *x32    *x51
        +coeff[ 23]*x13        *x41*x51
        +coeff[ 24]    *x23*x31    *x52
        +coeff[ 25]*x11*x22    *x43    
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 26]*x11    *x32*x43    
        +coeff[ 27]*x11    *x31*x44    
        +coeff[ 28]*x11    *x34    *x51
        +coeff[ 29]*x11*x23    *x41*x51
        +coeff[ 30]*x11*x22    *x41*x52
        +coeff[ 31]*x11*x21*x31    *x53
        +coeff[ 32]*x11    *x31*x41*x53
        +coeff[ 33]*x12    *x32*x42    
        +coeff[ 34]*x12*x22*x31    *x51
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 35]*x12*x21        *x53
        +coeff[ 36]*x13    *x33        
        +coeff[ 37]    *x24*x32    *x51
        +coeff[ 38]*x13    *x31*x41*x51
        +coeff[ 39]    *x22*x32*x42*x51
        +coeff[ 40]        *x33*x43*x51
        +coeff[ 41]    *x23    *x42*x52
        +coeff[ 42]*x11*x21*x33*x42    
        +coeff[ 43]*x11    *x34*x41*x51
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 44]*x11*x23    *x42*x51
        +coeff[ 45]*x11*x22    *x43*x51
        +coeff[ 46]*x11    *x34    *x52
        +coeff[ 47]*x11*x21*x32*x41*x52
        +coeff[ 48]*x12    *x31*x41*x53
        +coeff[ 49]*x13        *x44    
        +coeff[ 50]    *x23*x32*x42*x51
        +coeff[ 51]    *x21*x34*x42*x51
        +coeff[ 52]*x13        *x43*x51
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 53]*x13    *x31    *x53
        +coeff[ 54]*x13            *x54
        +coeff[ 55]        *x34    *x54
        +coeff[ 56]    *x21*x31        
        +coeff[ 57]    *x21    *x41    
        +coeff[ 58]                *x52
        +coeff[ 59]*x11            *x51
        +coeff[ 60]    *x21*x31*x41    
        +coeff[ 61]        *x31*x42    
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 62]    *x21    *x41*x51
        +coeff[ 63]        *x31*x41*x51
        +coeff[ 64]*x11*x22            
        +coeff[ 65]*x11    *x31    *x51
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x13                
        +coeff[ 68]    *x24            
        +coeff[ 69]    *x23    *x41    
        +coeff[ 70]        *x33*x41    
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 71]        *x31*x43    
        +coeff[ 72]    *x23        *x51
        +coeff[ 73]        *x32*x41*x51
        +coeff[ 74]    *x21    *x42*x51
        +coeff[ 75]            *x43*x51
        +coeff[ 76]    *x22        *x52
        +coeff[ 77]                *x54
        +coeff[ 78]*x11    *x33        
        +coeff[ 79]*x11*x22    *x41    
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 80]*x11    *x31*x42    
        +coeff[ 81]*x11*x21*x31    *x51
        +coeff[ 82]*x11    *x32    *x51
        +coeff[ 83]*x11    *x31*x41*x51
        +coeff[ 84]*x11        *x42*x51
        +coeff[ 85]*x11    *x31    *x52
        +coeff[ 86]*x12*x21*x31        
        +coeff[ 87]*x12*x21    *x41    
        +coeff[ 88]*x12            *x52
    ;
    v_l_e_q1q2_3_1000                              =v_l_e_q1q2_3_1000                              
        +coeff[ 89]*x13    *x31        
        ;

    return v_l_e_q1q2_3_1000                              ;
}
float x_e_q1q2_3_900                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.4011826E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.37321742E-03, 0.12089861E+00, 0.38041039E-02, 0.64803410E-03,
         0.12443306E-03,-0.56192861E-03,-0.11802183E-02,-0.91401057E-03,
        -0.25105671E-03,-0.34870647E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_900                              =v_x_e_q1q2_3_900                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_900                              ;
}
float t_e_q1q2_3_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1047102E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.99003328E-05,-0.19899062E-02,-0.96144900E-03, 0.22854062E-02,
        -0.10478800E-02,-0.96307910E-03, 0.66823618E-05,-0.25058677E-03,
         0.72690593E-04,-0.28013732E-03,-0.38323196E-03,-0.84176223E-04,
        -0.81398204E-03,-0.63764950E-03,-0.49316930E-03,-0.19715944E-04,
        -0.26214952E-04,-0.21419288E-04, 0.10854140E-03, 0.54534674E-04,
        -0.24763748E-03,-0.58046030E-03, 0.60589782E-05, 0.11221447E-04,
        -0.13159784E-04,-0.80336213E-05, 0.58647561E-05, 0.15121174E-05,
         0.26331425E-04,-0.19460795E-05,-0.46788937E-04,-0.33244807E-04,
         0.13422195E-04,-0.22827995E-04, 0.54270236E-04,-0.84617119E-06,
         0.16008738E-05,-0.33119811E-05,-0.20947853E-05,-0.27335379E-05,
        -0.48920729E-05,-0.95559399E-05,-0.60206507E-05,-0.51863462E-05,
        -0.72128214E-05, 0.51356069E-05, 0.57976754E-05,-0.51132638E-05,
         0.74072213E-05, 0.69757930E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_900                              =v_t_e_q1q2_3_900                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_900                              =v_t_e_q1q2_3_900                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x12*x21*x32    *x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x21*x31    *x51
    ;
    v_t_e_q1q2_3_900                              =v_t_e_q1q2_3_900                              
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x21*x32    *x51
        +coeff[ 33]*x12*x22    *x41*x51
        +coeff[ 34]    *x21*x32*x42*x51
    ;
    v_t_e_q1q2_3_900                              =v_t_e_q1q2_3_900                              
        +coeff[ 35]                *x51
        +coeff[ 36]    *x22            
        +coeff[ 37]        *x31    *x51
        +coeff[ 38]                *x52
        +coeff[ 39]*x13                
        +coeff[ 40]        *x31*x42    
        +coeff[ 41]*x11*x22    *x41    
        +coeff[ 42]    *x23    *x41    
        +coeff[ 43]*x11    *x31*x42    
    ;
    v_t_e_q1q2_3_900                              =v_t_e_q1q2_3_900                              
        +coeff[ 44]    *x21*x31*x42    
        +coeff[ 45]*x12    *x31    *x51
        +coeff[ 46]*x11    *x32    *x51
        +coeff[ 47]*x11*x21    *x41*x51
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x12*x22*x31        
        ;

    return v_t_e_q1q2_3_900                              ;
}
float y_e_q1q2_3_900                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.2103494E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.14796156E-03, 0.16916716E+00, 0.10628968E+00,-0.24814107E-02,
        -0.22574069E-02, 0.28695716E-03,-0.14116727E-03,-0.14498873E-03,
         0.17582107E-03, 0.14102188E-03, 0.10081509E-03, 0.93594412E-04,
        -0.33476669E-03,-0.49455073E-04, 0.16967520E-04,-0.51309878E-04,
         0.15007462E-04, 0.58696838E-04,-0.25007990E-03,-0.24199698E-03,
        -0.17678617E-03, 0.22342812E-04, 0.14290041E-04, 0.21297546E-04,
        -0.33326382E-04, 0.13055944E-04,-0.40588831E-04, 0.20366466E-04,
         0.31332231E-04, 0.26882230E-04, 0.70474140E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_900                              =v_y_e_q1q2_3_900                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22    *x41*x51
    ;
    v_y_e_q1q2_3_900                              =v_y_e_q1q2_3_900                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33*x42    
        +coeff[ 21]    *x21    *x42    
        +coeff[ 22]    *x21*x31*x41    
        +coeff[ 23]        *x31*x42*x51
        +coeff[ 24]    *x21    *x44    
        +coeff[ 25]*x11        *x42*x51
    ;
    v_y_e_q1q2_3_900                              =v_y_e_q1q2_3_900                              
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]*x12        *x43    
        +coeff[ 28]*x11    *x31*x42*x51
        +coeff[ 29]*x11*x23    *x42    
        +coeff[ 30]    *x24    *x41*x51
        ;

    return v_y_e_q1q2_3_900                              ;
}
float p_e_q1q2_3_900                              (float *x,int m){
    int ncoeff=  9;
    float avdat=  0.5923967E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
        -0.36730784E-04, 0.30966392E-01, 0.64555526E-01,-0.14596505E-02,
         0.52622290E-05,-0.13226063E-02, 0.40206593E-03, 0.23553589E-03,
         0.21940246E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_900                              =v_p_e_q1q2_3_900                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_900                              ;
}
float l_e_q1q2_3_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2128189E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21743733E-02,-0.35924634E-02,-0.96574065E-03,-0.32504646E-02,
        -0.41539748E-02,-0.16022615E-03,-0.10236985E-02,-0.10291553E-02,
         0.24778995E-03, 0.82891562E-03,-0.16838274E-03, 0.81594553E-04,
        -0.17715260E-03, 0.11887128E-03,-0.61517430E-03, 0.82317856E-03,
        -0.65698504E-03,-0.71823964E-03,-0.15810312E-04,-0.21391064E-03,
         0.34723800E-03,-0.73914351E-04, 0.22078100E-03,-0.33552162E-03,
         0.32349210E-03,-0.12075544E-03,-0.51653129E-03, 0.83472283E-03,
        -0.61072176E-03, 0.46344573E-03, 0.21408930E-03,-0.14374652E-02,
        -0.89071441E-03,-0.13549428E-02, 0.61232160E-03,-0.46894880E-03,
         0.20370823E-02, 0.12820731E-02, 0.68927882E-07, 0.11224933E-02,
         0.67515718E-03, 0.51482674E-03,-0.26195977E-03,-0.17617785E-02,
         0.55456016E-03, 0.10006664E-02,-0.15983761E-02,-0.84635831E-03,
         0.28444314E-03, 0.33575896E-03,-0.36398365E-03,-0.62826043E-03,
        -0.20703264E-02, 0.74681209E-03, 0.11757351E-02, 0.20164370E-02,
         0.11993611E-02, 0.12940608E-02, 0.16715660E-02,-0.27645440E-02,
        -0.23229849E-02,-0.26461168E-02, 0.11078151E-02, 0.10990477E-02,
        -0.12393768E-02, 0.15013886E-02, 0.15850598E-02,-0.10963248E-02,
        -0.61538356E-03,-0.17716244E-02, 0.63219701E-03, 0.10977031E-02,
         0.64780348E-03,-0.79889526E-03,-0.24120968E-03,-0.12037117E-03,
        -0.76390839E-04, 0.43684995E-04, 0.93942952E-04, 0.10024671E-03,
        -0.11049862E-03,-0.78128745E-04, 0.21796278E-03, 0.21729284E-03,
        -0.10297147E-03, 0.14594600E-03, 0.24496851E-03, 0.34728722E-03,
        -0.16617001E-03, 0.22923376E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x21*x31*x42    
        +coeff[  7]*x12*x21    *x41*x51
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[  8]*x12            *x53
        +coeff[  9]    *x23*x33        
        +coeff[ 10]    *x21    *x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 17]*x11        *x41*x51
        +coeff[ 18]*x12*x21            
        +coeff[ 19]*x12    *x31        
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]*x11    *x31*x41*x51
        +coeff[ 22]*x12    *x31*x41    
        +coeff[ 23]*x12    *x31    *x51
        +coeff[ 24]    *x21*x34        
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]    *x22*x31    *x52
        +coeff[ 31]    *x22    *x41*x52
        +coeff[ 32]*x11*x21*x33        
        +coeff[ 33]*x11*x21*x31*x42    
        +coeff[ 34]*x11*x21*x32    *x51
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]*x11    *x31*x42*x51
        +coeff[ 37]*x11*x21    *x41*x52
        +coeff[ 38]*x11    *x31    *x53
        +coeff[ 39]*x11        *x41*x53
        +coeff[ 40]*x12    *x31*x42    
        +coeff[ 41]    *x21*x32    *x53
        +coeff[ 42]    *x21    *x42*x53
        +coeff[ 43]*x11*x22*x32*x41    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 44]*x11    *x32*x43    
        +coeff[ 45]*x11*x22*x32    *x51
        +coeff[ 46]*x11    *x31*x43*x51
        +coeff[ 47]*x11    *x32    *x53
        +coeff[ 48]*x11    *x31    *x54
        +coeff[ 49]*x12*x24            
        +coeff[ 50]*x12        *x44    
        +coeff[ 51]*x13*x22*x31        
        +coeff[ 52]    *x23*x32*x42    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 53]*x13    *x32    *x51
        +coeff[ 54]*x13    *x31*x41*x51
        +coeff[ 55]    *x22*x32*x42*x51
        +coeff[ 56]    *x22    *x43*x52
        +coeff[ 57]    *x22*x31*x41*x53
        +coeff[ 58]*x11*x22*x34        
        +coeff[ 59]*x11*x22*x33    *x51
        +coeff[ 60]*x11    *x31*x44*x51
        +coeff[ 61]*x11*x21*x32*x41*x52
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 62]*x11    *x33*x41*x52
        +coeff[ 63]*x11*x22*x31    *x53
        +coeff[ 64]*x11    *x32*x41*x53
        +coeff[ 65]*x12*x21*x32    *x52
        +coeff[ 66]*x12*x22    *x41*x52
        +coeff[ 67]*x12    *x31*x41*x53
        +coeff[ 68]*x13*x24            
        +coeff[ 69]*x13*x22*x32        
        +coeff[ 70]*x13*x22    *x42    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 71]*x13*x22*x31    *x51
        +coeff[ 72]*x13    *x32    *x52
        +coeff[ 73]    *x22*x32*x41*x53
        +coeff[ 74]*x11                
        +coeff[ 75]    *x21*x31        
        +coeff[ 76]    *x21        *x51
        +coeff[ 77]                *x52
        +coeff[ 78]*x11    *x31        
        +coeff[ 79]*x11        *x41    
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 80]*x11            *x51
        +coeff[ 81]        *x33        
        +coeff[ 82]    *x21*x31*x41    
        +coeff[ 83]        *x32*x41    
        +coeff[ 84]    *x21*x31    *x51
        +coeff[ 85]    *x21    *x41*x51
        +coeff[ 86]        *x31*x41*x51
        +coeff[ 87]*x11*x22            
        +coeff[ 88]*x11*x21        *x51
    ;
    v_l_e_q1q2_3_900                              =v_l_e_q1q2_3_900                              
        +coeff[ 89]*x13                
        ;

    return v_l_e_q1q2_3_900                              ;
}
float x_e_q1q2_3_800                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.2366595E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.25599891E-04, 0.12091563E+00, 0.38013433E-02, 0.64893434E-03,
         0.12478356E-03,-0.56150922E-03,-0.11784249E-02,-0.90989308E-03,
        -0.24967239E-03,-0.34931218E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_800                              =v_x_e_q1q2_3_800                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_800                              ;
}
float t_e_q1q2_3_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1878809E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18345507E-04,-0.19895437E-02,-0.95186784E-03, 0.22806006E-02,
        -0.10445374E-02,-0.96850796E-03, 0.31794004E-05,-0.26145310E-03,
         0.73825126E-04,-0.27576604E-03,-0.37947824E-03,-0.89609188E-04,
        -0.81462495E-03,-0.63250674E-03,-0.47384927E-03,-0.19805722E-04,
        -0.16539019E-04,-0.26128602E-04, 0.74117699E-04, 0.74895885E-04,
        -0.54410758E-03, 0.10935581E-04,-0.14299037E-04, 0.34417004E-04,
        -0.46631390E-04,-0.21465610E-04,-0.24698646E-03,-0.23472185E-04,
         0.20012058E-04,-0.12214530E-04, 0.23831521E-04, 0.14392930E-04,
         0.14245743E-04, 0.51903335E-04,-0.32608409E-05,-0.39393776E-05,
        -0.41333155E-05, 0.20512095E-05,-0.33332767E-05,-0.24370959E-05,
         0.30551410E-05,-0.73870638E-05,-0.11244638E-04, 0.60573807E-05,
        -0.10010809E-05, 0.56152485E-05, 0.49820801E-05, 0.50422454E-05,
         0.11601988E-04,-0.45825186E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_800                              =v_t_e_q1q2_3_800                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_800                              =v_t_e_q1q2_3_800                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11    *x33*x41    
    ;
    v_t_e_q1q2_3_800                              =v_t_e_q1q2_3_800                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]    *x23*x31    *x51
        +coeff[ 29]*x11*x22        *x52
        +coeff[ 30]*x11*x22*x33        
        +coeff[ 31]    *x23    *x43    
        +coeff[ 32]*x13*x21*x31    *x51
        +coeff[ 33]    *x23*x31*x41*x51
        +coeff[ 34]    *x22            
    ;
    v_t_e_q1q2_3_800                              =v_t_e_q1q2_3_800                              
        +coeff[ 35]*x12*x21            
        +coeff[ 36]*x12        *x41    
        +coeff[ 37]            *x43    
        +coeff[ 38]*x11*x21        *x51
        +coeff[ 39]    *x22        *x51
        +coeff[ 40]*x11    *x31    *x51
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]*x11*x22*x31        
        +coeff[ 43]    *x23*x31        
    ;
    v_t_e_q1q2_3_800                              =v_t_e_q1q2_3_800                              
        +coeff[ 44]    *x22*x31*x41    
        +coeff[ 45]    *x22    *x42    
        +coeff[ 46]*x11    *x31*x42    
        +coeff[ 47]*x12*x21        *x51
        +coeff[ 48]    *x23        *x51
        +coeff[ 49]    *x21*x31    *x52
        ;

    return v_t_e_q1q2_3_800                              ;
}
float y_e_q1q2_3_800                              (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.7066485E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.73925877E-03, 0.16917086E+00, 0.10629109E+00,-0.24855668E-02,
        -0.22549245E-02, 0.31970872E-03,-0.14991601E-03,-0.80080819E-04,
         0.18934389E-03, 0.10629537E-03, 0.11022048E-03, 0.96054442E-04,
        -0.38128038E-03, 0.20215964E-04,-0.25746698E-04,-0.33743479E-05,
         0.57054549E-04,-0.24117509E-03,-0.99925295E-04,-0.33697684E-03,
         0.16711752E-04,-0.40657058E-04,-0.24266796E-03,-0.16146434E-04,
        -0.82679522E-04,-0.51394298E-04, 0.25601543E-04,-0.61339379E-04,
         0.78785546E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_800                              =v_y_e_q1q2_3_800                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22    *x41*x51
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_3_800                              =v_y_e_q1q2_3_800                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]*x11*x23    *x41    
        +coeff[ 19]    *x24*x32*x41    
        +coeff[ 20]*x11*x21    *x42    
        +coeff[ 21]    *x22    *x43    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]        *x31*x43*x51
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x21*x32*x41    
    ;
    v_y_e_q1q2_3_800                              =v_y_e_q1q2_3_800                              
        +coeff[ 26]    *x21*x32*x41*x51
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]    *x24    *x41*x51
        ;

    return v_y_e_q1q2_3_800                              ;
}
float p_e_q1q2_3_800                              (float *x,int m){
    int ncoeff=  9;
    float avdat=  0.3245902E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
        -0.33662788E-03, 0.30964129E-01, 0.64554669E-01,-0.14612452E-02,
         0.10960922E-04,-0.13251748E-02, 0.40614410E-03, 0.23568679E-03,
         0.21456428E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_800                              =v_p_e_q1q2_3_800                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_800                              ;
}
float l_e_q1q2_3_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2126337E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21281077E-02,-0.36471554E-02,-0.67736057E-03,-0.31349729E-02,
        -0.42366078E-02, 0.52495929E-03, 0.86264277E-03,-0.89909072E-03,
        -0.73130512E-04,-0.95618110E-04, 0.72165567E-03, 0.48532698E-03,
         0.99299056E-03,-0.14515093E-03, 0.34861863E-03, 0.29653913E-03,
         0.15240573E-03,-0.19274563E-03,-0.43628583E-03,-0.73986663E-03,
         0.25136073E-03,-0.58438949E-03, 0.59676066E-03,-0.52698428E-03,
         0.23456124E-03, 0.96376124E-03,-0.41310952E-03,-0.61036251E-03,
         0.14811105E-02, 0.72988507E-03, 0.32197458E-02,-0.35935128E-03,
        -0.29920854E-03,-0.44115591E-02,-0.16284169E-02, 0.89048914E-03,
         0.61456184E-03,-0.61547308E-03,-0.12458102E-02,-0.59666601E-03,
        -0.15474809E-02, 0.14899126E-02,-0.25039364E-03, 0.46608367E-03,
        -0.89567242E-03,-0.87376463E-03,-0.29370002E-02,-0.15297373E-02,
        -0.94917987E-03, 0.25143574E-02, 0.53336862E-02,-0.27388383E-02,
        -0.18253796E-03, 0.12323169E-03,-0.23987016E-06,-0.24195779E-04,
        -0.31807920E-03, 0.15567812E-03,-0.31035548E-03, 0.49651029E-04,
        -0.25238111E-03, 0.89898196E-04, 0.89889181E-04, 0.19640014E-04,
        -0.15689335E-03, 0.18702171E-03, 0.18905556E-03,-0.22921502E-03,
         0.20746220E-03, 0.16903004E-03,-0.27145279E-03, 0.13327552E-03,
         0.14035085E-03,-0.28691263E-03, 0.16930597E-03, 0.54844952E-03,
         0.14170153E-04, 0.35400837E-03,-0.57304744E-03, 0.79356745E-03,
        -0.15817618E-03, 0.15010255E-03,-0.17547855E-03, 0.24493085E-03,
        -0.20019956E-03, 0.18032084E-03, 0.60223800E-03,-0.56861382E-03,
         0.40551208E-03,-0.24469494E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]            *x42*x51
        +coeff[  6]*x11*x21*x31*x42    
        +coeff[  7]    *x22    *x42*x54
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[  8]    *x21        *x51
        +coeff[  9]*x12                
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]    *x24            
        +coeff[ 12]    *x22*x31*x41    
        +coeff[ 13]    *x22*x31    *x51
        +coeff[ 14]*x11*x21*x32        
        +coeff[ 15]*x11*x21*x31    *x51
        +coeff[ 16]*x11    *x32    *x51
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 17]*x11*x21        *x52
        +coeff[ 18]*x12*x21    *x41    
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]        *x33*x41*x51
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]            *x42*x53
        +coeff[ 24]*x11    *x33    *x51
        +coeff[ 25]*x11    *x31*x41*x52
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 26]*x12    *x32*x41    
        +coeff[ 27]*x13    *x31*x41    
        +coeff[ 28]    *x22*x32*x42    
        +coeff[ 29]    *x24*x31    *x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]        *x33*x42*x51
        +coeff[ 32]        *x33    *x53
        +coeff[ 33]    *x21*x31*x41*x53
        +coeff[ 34]*x11*x24    *x41    
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 35]*x11*x22    *x43    
        +coeff[ 36]*x11*x24        *x51
        +coeff[ 37]*x11*x22        *x53
        +coeff[ 38]*x12    *x32*x42    
        +coeff[ 39]        *x33*x44    
        +coeff[ 40]*x13*x21    *x41*x51
        +coeff[ 41]    *x21*x34*x41*x51
        +coeff[ 42]    *x23    *x43*x51
        +coeff[ 43]    *x21*x33    *x53
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 44]*x12*x23    *x41*x51
        +coeff[ 45]*x12*x22*x31    *x52
        +coeff[ 46]    *x23*x33*x41*x51
        +coeff[ 47]        *x34*x43*x51
        +coeff[ 48]*x13*x22        *x52
        +coeff[ 49]        *x34*x42*x52
        +coeff[ 50]    *x21*x33*x41*x53
        +coeff[ 51]        *x32*x42*x54
        +coeff[ 52]    *x21*x31        
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 53]    *x21    *x41    
        +coeff[ 54]            *x41*x51
        +coeff[ 55]                *x52
        +coeff[ 56]*x11    *x31        
        +coeff[ 57]*x11            *x51
        +coeff[ 58]    *x22*x31        
        +coeff[ 59]    *x21*x32        
        +coeff[ 60]        *x32*x41    
        +coeff[ 61]            *x41*x52
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 62]*x11*x22            
        +coeff[ 63]*x11    *x32        
        +coeff[ 64]*x11        *x42    
        +coeff[ 65]*x11            *x52
        +coeff[ 66]    *x23*x31        
        +coeff[ 67]    *x22*x32        
        +coeff[ 68]    *x22    *x41*x51
        +coeff[ 69]            *x43*x51
        +coeff[ 70]    *x22        *x52
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 71]    *x21*x31    *x52
        +coeff[ 72]        *x32    *x52
        +coeff[ 73]        *x31*x41*x52
        +coeff[ 74]                *x54
        +coeff[ 75]*x11*x22    *x41    
        +coeff[ 76]*x11    *x32*x41    
        +coeff[ 77]*x11    *x31*x42    
        +coeff[ 78]*x11*x22        *x51
        +coeff[ 79]*x11*x21    *x41*x51
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 80]*x11    *x31*x41*x51
        +coeff[ 81]*x11    *x31    *x52
        +coeff[ 82]*x12*x22            
        +coeff[ 83]*x12        *x42    
        +coeff[ 84]*x12        *x41*x51
        +coeff[ 85]*x13    *x31        
        +coeff[ 86]    *x24*x31        
        +coeff[ 87]    *x23*x32        
        +coeff[ 88]    *x21*x34        
    ;
    v_l_e_q1q2_3_800                              =v_l_e_q1q2_3_800                              
        +coeff[ 89]    *x23*x31*x41    
        ;

    return v_l_e_q1q2_3_800                              ;
}
float x_e_q1q2_3_700                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.6026659E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.60601626E-03, 0.12092359E+00, 0.38001977E-02, 0.64997846E-03,
         0.12422906E-03,-0.55696734E-03,-0.11806795E-02,-0.90911955E-03,
        -0.23845144E-03,-0.35360653E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_700                              =v_x_e_q1q2_3_700                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_700                              ;
}
float t_e_q1q2_3_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1901648E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18737226E-04,-0.19897930E-02,-0.94818487E-03, 0.22945781E-02,
        -0.10284963E-02,-0.95483212E-03, 0.15764534E-05,-0.27995856E-03,
         0.71486604E-04,-0.26215453E-03,-0.36579170E-03,-0.86336353E-04,
        -0.84248366E-03,-0.65751566E-03,-0.49115944E-03,-0.25046291E-04,
        -0.22981736E-04,-0.22073093E-04, 0.82534068E-04, 0.56069552E-04,
        -0.25583603E-03,-0.57334878E-03, 0.21674035E-04, 0.12024078E-05,
        -0.97675556E-05,-0.14230915E-04,-0.15471200E-04,-0.20373247E-04,
         0.30991763E-04,-0.98035698E-05, 0.18953966E-04,-0.17874603E-05,
         0.76010874E-05, 0.21594433E-05,-0.31278178E-05,-0.52055152E-05,
         0.49372370E-05, 0.14803960E-04, 0.42824945E-05,-0.38773373E-05,
        -0.40251375E-05,-0.48155594E-04, 0.88484894E-05, 0.13770149E-04,
        -0.36158912E-04, 0.31122690E-04,-0.99651652E-06, 0.18181663E-04,
        -0.10090421E-04,-0.41676099E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_700                              =v_t_e_q1q2_3_700                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_700                              =v_t_e_q1q2_3_700                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_t_e_q1q2_3_700                              =v_t_e_q1q2_3_700                              
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]*x12*x23            
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]*x12*x22        *x51
        +coeff[ 30]*x11*x22*x32    *x51
        +coeff[ 31]                *x52
        +coeff[ 32]*x12*x21            
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11            *x52
    ;
    v_t_e_q1q2_3_700                              =v_t_e_q1q2_3_700                              
        +coeff[ 35]*x11*x23            
        +coeff[ 36]*x11*x21*x31    *x51
        +coeff[ 37]    *x21*x32    *x51
        +coeff[ 38]*x11*x21        *x52
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]*x11*x22*x32        
        +coeff[ 41]*x11*x22*x31*x41    
        +coeff[ 42]*x11*x21*x32*x41    
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_t_e_q1q2_3_700                              =v_t_e_q1q2_3_700                              
        +coeff[ 44]*x11*x22    *x42    
        +coeff[ 45]*x11*x21*x31*x42    
        +coeff[ 46]    *x22*x31*x42    
        +coeff[ 47]*x11*x21    *x43    
        +coeff[ 48]    *x22    *x43    
        +coeff[ 49]*x13*x21        *x51
        ;

    return v_t_e_q1q2_3_700                              ;
}
float y_e_q1q2_3_700                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3010462E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.35479679E-03, 0.16911978E+00, 0.10627960E+00,-0.24763029E-02,
        -0.22560393E-02, 0.28991132E-03,-0.14487225E-03,-0.15206129E-03,
         0.19012179E-03, 0.13656834E-03,-0.33493361E-04, 0.10692725E-03,
         0.89727568E-04,-0.33363083E-03, 0.32669616E-05, 0.60838960E-04,
        -0.25109405E-03,-0.24856554E-03, 0.49848564E-04,-0.17019133E-03,
         0.16453090E-07,-0.65957311E-05,-0.10972156E-04, 0.44384124E-04,
        -0.38238002E-04, 0.32546828E-04,-0.37470727E-04,-0.71723574E-04,
        -0.41355488E-04,-0.69622671E-04, 0.30621803E-04, 0.27618647E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_700                              =v_y_e_q1q2_3_700                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q1q2_3_700                              =v_y_e_q1q2_3_700                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]    *x24    *x41*x51
        +coeff[ 19]    *x22*x33*x42    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x12        *x41    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]*x11*x22    *x42    
        +coeff[ 25]*x11    *x32*x42    
    ;
    v_y_e_q1q2_3_700                              =v_y_e_q1q2_3_700                              
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]*x11*x23    *x41    
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11        *x45    
        +coeff[ 30]    *x23*x31*x42    
        +coeff[ 31]    *x21*x31*x43*x51
        ;

    return v_y_e_q1q2_3_700                              ;
}
float p_e_q1q2_3_700                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.1431308E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.16296124E-03, 0.30954795E-01, 0.64528964E-01,-0.14610309E-02,
         0.96032647E-06,-0.13204599E-02, 0.40683933E-03, 0.23901867E-03,
         0.22441661E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_700                              =v_p_e_q1q2_3_700                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_700                              ;
}
float l_e_q1q2_3_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2132962E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20763604E-02,-0.32427169E-02,-0.10721921E-02,-0.32836513E-02,
        -0.42559444E-02,-0.21362706E-04, 0.68098883E-03,-0.19444942E-02,
         0.77230259E-04,-0.15521140E-03,-0.16888037E-03, 0.24051305E-03,
         0.14537433E-02, 0.58309099E-03,-0.64370525E-03, 0.58343861E-03,
        -0.42355096E-03, 0.33129836E-03,-0.54206105E-03,-0.94719807E-03,
        -0.10295974E-02, 0.10264353E-02, 0.11425510E-02,-0.27610485E-02,
         0.66914072E-03, 0.28697721E-03, 0.88062567E-04, 0.25491085E-03,
        -0.14146873E-02,-0.13220911E-02,-0.17213996E-02, 0.56685210E-03,
         0.15848440E-03,-0.52416918E-03, 0.27719332E-03,-0.75872045E-03,
        -0.71911933E-03, 0.54992491E-03, 0.58513629E-03,-0.44339002E-03,
         0.62849442E-03, 0.19965691E-02,-0.16400538E-02, 0.87079528E-03,
        -0.49505016E-03,-0.44458525E-03, 0.18005064E-02, 0.10331859E-02,
         0.36869559E-03,-0.14086689E-02,-0.68133645E-03, 0.75387018E-03,
        -0.98504813E-03,-0.16141868E-02,-0.14457059E-02,-0.99878758E-03,
         0.15912661E-02,-0.27965466E-02, 0.28110407E-02,-0.11986041E-04,
         0.27300391E-03, 0.56645564E-04,-0.97699878E-04, 0.97617616E-04,
        -0.96103191E-04, 0.19288865E-03, 0.57322398E-03,-0.74321212E-03,
        -0.34675654E-03, 0.98550605E-04, 0.14441903E-03, 0.66964660E-03,
         0.15571131E-03,-0.46681281E-03,-0.18717548E-03, 0.13087496E-03,
         0.42107652E-03,-0.21684250E-03,-0.28068307E-03, 0.23515924E-03,
         0.16222248E-03, 0.35771827E-03, 0.93456343E-04, 0.16189759E-03,
         0.23329869E-03,-0.10423569E-02, 0.34611279E-03, 0.14138511E-03,
         0.24416664E-03,-0.21765995E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]*x11*x24    *x41    
        +coeff[  7]*x11    *x32*x43    
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[  8]                *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]            *x43*x51
        +coeff[ 12]        *x31*x41*x52
        +coeff[ 13]*x11*x21*x31*x41    
        +coeff[ 14]*x11    *x31*x42    
        +coeff[ 15]*x12        *x42    
        +coeff[ 16]*x12*x21        *x51
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 17]*x12            *x52
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x21*x31*x42*x51
        +coeff[ 20]*x11*x24            
        +coeff[ 21]*x11    *x31*x43    
        +coeff[ 22]*x11        *x44    
        +coeff[ 23]*x11    *x32*x41*x51
        +coeff[ 24]*x11        *x43*x51
        +coeff[ 25]*x13*x22            
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 26]    *x23    *x42*x51
        +coeff[ 27]    *x22*x32    *x52
        +coeff[ 28]    *x21*x32*x41*x52
        +coeff[ 29]    *x21    *x43*x52
        +coeff[ 30]        *x31*x43*x52
        +coeff[ 31]    *x21*x31    *x54
        +coeff[ 32]*x11*x23*x32        
        +coeff[ 33]*x12*x24            
        +coeff[ 34]*x12    *x34        
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 35]*x12*x22    *x41*x51
        +coeff[ 36]*x12            *x54
        +coeff[ 37]*x13*x21*x32        
        +coeff[ 38]*x13    *x32*x41    
        +coeff[ 39]*x13*x22        *x51
        +coeff[ 40]    *x23*x33    *x51
        +coeff[ 41]*x13*x21    *x41*x51
        +coeff[ 42]    *x24*x31*x41*x51
        +coeff[ 43]    *x22*x33*x41*x51
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 44]        *x33    *x54
        +coeff[ 45]    *x22    *x41*x54
        +coeff[ 46]*x11*x23*x32    *x51
        +coeff[ 47]*x11    *x34*x41*x51
        +coeff[ 48]*x11*x23        *x53
        +coeff[ 49]*x11*x21*x32    *x53
        +coeff[ 50]*x12*x21    *x43*x51
        +coeff[ 51]*x12*x22*x31    *x52
        +coeff[ 52]    *x24*x34        
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 53]*x13    *x31*x43    
        +coeff[ 54]        *x34*x44    
        +coeff[ 55]*x13*x21*x31*x41*x51
        +coeff[ 56]*x13    *x32*x41*x51
        +coeff[ 57]    *x23*x31*x42*x52
        +coeff[ 58]    *x21*x32*x41*x54
        +coeff[ 59]*x11                
        +coeff[ 60]        *x31    *x51
        +coeff[ 61]    *x22    *x41    
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 62]        *x32*x41    
        +coeff[ 63]    *x21    *x42    
        +coeff[ 64]        *x32    *x51
        +coeff[ 65]        *x31*x41*x51
        +coeff[ 66]*x11*x22            
        +coeff[ 67]*x11        *x42    
        +coeff[ 68]*x11*x21        *x51
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x12        *x41    
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 71]    *x22*x32        
        +coeff[ 72]        *x34        
        +coeff[ 73]        *x33*x41    
        +coeff[ 74]    *x22    *x42    
        +coeff[ 75]    *x21*x31*x42    
        +coeff[ 76]        *x32*x42    
        +coeff[ 77]            *x44    
        +coeff[ 78]        *x33    *x51
        +coeff[ 79]    *x21    *x42*x51
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 80]    *x22        *x52
        +coeff[ 81]    *x21    *x41*x52
        +coeff[ 82]    *x21        *x53
        +coeff[ 83]*x11*x21*x31    *x51
        +coeff[ 84]*x11    *x32    *x51
        +coeff[ 85]*x11*x21    *x41*x51
        +coeff[ 86]*x11    *x31*x41*x51
        +coeff[ 87]*x11        *x42*x51
        +coeff[ 88]*x12    *x31*x41    
    ;
    v_l_e_q1q2_3_700                              =v_l_e_q1q2_3_700                              
        +coeff[ 89]*x12    *x31    *x51
        ;

    return v_l_e_q1q2_3_700                              ;
}
float x_e_q1q2_3_600                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1105559E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.86248030E-04, 0.12096654E+00, 0.38027137E-02, 0.64888853E-03,
         0.12275424E-03,-0.56117255E-03,-0.11766013E-02,-0.90993074E-03,
        -0.24408450E-03,-0.35025252E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_600                              =v_x_e_q1q2_3_600                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_600                              ;
}
float t_e_q1q2_3_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3957502E-05;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.31044513E-05,-0.19891616E-02,-0.91568718E-03, 0.74582043E-04,
         0.22880372E-02,-0.10454161E-02,-0.98094519E-03,-0.21188348E-05,
        -0.22115246E-03,-0.29065521E-03,-0.39848380E-03,-0.90405083E-04,
        -0.80959557E-03,-0.62093645E-03,-0.47471479E-03,-0.25280237E-04,
        -0.28931110E-04,-0.27671393E-04, 0.63939457E-04, 0.61676248E-04,
        -0.52566465E-03, 0.18396315E-04, 0.85679976E-05,-0.13875165E-04,
        -0.20489626E-04, 0.23633978E-04,-0.24578813E-03, 0.53666641E-04,
         0.17601000E-07, 0.29583921E-05, 0.20459261E-05,-0.73639294E-05,
         0.30239812E-05, 0.26748648E-05,-0.26172120E-05,-0.30866115E-05,
         0.28657175E-05,-0.13010413E-04, 0.51706297E-05,-0.12625940E-04,
        -0.18006349E-04,-0.14895383E-04, 0.63763910E-05,-0.55934265E-05,
         0.44312196E-05, 0.58449955E-05,-0.75813273E-05,-0.12321000E-04,
        -0.13696858E-04,-0.13514174E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_600                              =v_t_e_q1q2_3_600                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_600                              =v_t_e_q1q2_3_600                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]    *x21*x31        
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x21*x33        
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q1q2_3_600                              =v_t_e_q1q2_3_600                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]    *x21*x31*x43*x51
        +coeff[ 28]            *x42    
        +coeff[ 29]*x13                
        +coeff[ 30]*x12    *x31        
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x22    *x41    
        +coeff[ 33]    *x22        *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_t_e_q1q2_3_600                              =v_t_e_q1q2_3_600                              
        +coeff[ 35]*x11            *x52
        +coeff[ 36]            *x41*x52
        +coeff[ 37]*x11*x21*x32        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]*x11*x21*x31*x41    
        +coeff[ 40]    *x21*x32*x41    
        +coeff[ 41]    *x21*x31*x42    
        +coeff[ 42]*x11*x21        *x52
        +coeff[ 43]    *x22        *x52
    ;
    v_t_e_q1q2_3_600                              =v_t_e_q1q2_3_600                              
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x12*x22    *x41    
        +coeff[ 47]*x12*x21*x31*x41    
        +coeff[ 48]*x11*x21*x32*x41    
        +coeff[ 49]*x11    *x33*x41    
        ;

    return v_t_e_q1q2_3_600                              ;
}
float y_e_q1q2_3_600                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.6783980E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.75216783E-03, 0.16907658E+00, 0.10628527E+00,-0.24813588E-02,
        -0.22543971E-02, 0.27808521E-03,-0.15889661E-03,-0.17284122E-03,
         0.19670064E-03, 0.13351752E-03, 0.10705487E-03, 0.92491056E-04,
        -0.30954296E-03,-0.93636074E-04,-0.17723409E-04,-0.30491778E-04,
         0.36420017E-04, 0.63971354E-04, 0.59547518E-04,-0.22905285E-03,
        -0.66022018E-04, 0.65341624E-05,-0.17907447E-03,-0.21086948E-04,
        -0.48954458E-04,-0.16977090E-04,-0.76181153E-04,-0.40045074E-04,
        -0.54972930E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_600                              =v_y_e_q1q2_3_600                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x23*x31        
        +coeff[ 14]*x13*x21    *x41    
        +coeff[ 15]    *x24*x32*x41    
        +coeff[ 16]        *x31*x42*x51
    ;
    v_y_e_q1q2_3_600                              =v_y_e_q1q2_3_600                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]*x11*x23    *x41    
        +coeff[ 21]        *x31*x41    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]*x12        *x41*x51
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]    *x21    *x42*x52
    ;
    v_y_e_q1q2_3_600                              =v_y_e_q1q2_3_600                              
        +coeff[ 26]        *x31*x44*x51
        +coeff[ 27]*x11        *x44*x51
        +coeff[ 28]*x11    *x31*x43*x51
        ;

    return v_y_e_q1q2_3_600                              ;
}
float p_e_q1q2_3_600                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.2694443E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.29707598E-03, 0.30950123E-01, 0.64509466E-01,-0.14573087E-02,
        -0.53803001E-06,-0.13180510E-02, 0.40676052E-03, 0.24155111E-03,
         0.21558300E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_600                              =v_p_e_q1q2_3_600                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_600                              ;
}
float l_e_q1q2_3_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2123923E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21503626E-02, 0.18870333E-03,-0.33287888E-02,-0.61684253E-03,
        -0.34907181E-02,-0.44462788E-02,-0.10632960E-02, 0.83632283E-02,
        -0.14117344E-03, 0.40554311E-04,-0.21000058E-03,-0.38944441E-03,
         0.44357501E-04, 0.33779477E-03,-0.18983920E-05,-0.46379623E-05,
         0.36158276E-03, 0.27323031E-03,-0.10079918E-02, 0.17033263E-03,
         0.77380403E-03,-0.53272594E-03, 0.20670054E-03,-0.10479059E-02,
        -0.12643466E-03,-0.55945350E-03, 0.49292727E-03,-0.76403830E-03,
        -0.41855813E-03, 0.10973611E-02,-0.56756265E-03,-0.19243526E-02,
        -0.14436682E-03,-0.48505442E-03, 0.22876132E-03,-0.53069048E-03,
        -0.86596777E-03, 0.13790861E-02, 0.11400100E-02,-0.14724160E-02,
        -0.31582185E-02,-0.45399112E-03,-0.11210503E-02,-0.14502803E-03,
         0.60791144E-03, 0.55379071E-03,-0.60991303E-03, 0.17514261E-02,
         0.39269790E-03, 0.48861938E-03,-0.16215418E-02, 0.19166128E-02,
        -0.64608612E-03, 0.14188053E-02,-0.11763488E-02,-0.38356421E-03,
         0.10953334E-02, 0.61123131E-03, 0.12174785E-02,-0.17433063E-03,
         0.43808148E-03, 0.10363034E-02,-0.11063417E-02,-0.15619415E-02,
        -0.12433717E-02,-0.25358356E-02, 0.20675375E-02,-0.26035553E-02,
         0.14546430E-02, 0.14606749E-02, 0.16952431E-02, 0.61152829E-03,
         0.23577835E-02,-0.70420356E-03,-0.67218480E-03,-0.61124173E-03,
         0.18434064E-02,-0.19106141E-02,-0.49680280E-02,-0.10771719E-02,
        -0.15151069E-02, 0.87746815E-03, 0.28524771E-02, 0.12643246E-02,
         0.18953929E-02, 0.59351318E-04, 0.13452176E-02,-0.16671617E-02,
        -0.34593625E-02, 0.20064334E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x32        
        +coeff[  7]    *x22*x32*x42    
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[  8]    *x21        *x51
        +coeff[  9]                *x52
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]                *x53
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]    *x21*x33        
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 17]    *x21*x32*x41    
        +coeff[ 18]        *x32*x42    
        +coeff[ 19]    *x21    *x41*x52
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]*x11    *x32*x41    
        +coeff[ 22]*x11        *x43    
        +coeff[ 23]*x11*x21    *x41*x51
        +coeff[ 24]*x12*x21    *x41    
        +coeff[ 25]*x12*x21        *x51
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]        *x31*x43*x51
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]        *x31*x42*x52
        +coeff[ 32]            *x43*x52
        +coeff[ 33]        *x31*x41*x53
        +coeff[ 34]            *x42*x53
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 35]*x11*x22*x32        
        +coeff[ 36]*x11*x22*x31*x41    
        +coeff[ 37]*x11    *x31*x43    
        +coeff[ 38]*x11    *x31*x42*x51
        +coeff[ 39]*x11*x22        *x52
        +coeff[ 40]*x11    *x31*x41*x52
        +coeff[ 41]*x11    *x31    *x53
        +coeff[ 42]*x12*x21*x31*x41    
        +coeff[ 43]*x12    *x31*x41*x51
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 44]*x12        *x42*x51
        +coeff[ 45]*x12    *x31    *x52
        +coeff[ 46]*x13    *x31*x41    
        +coeff[ 47]    *x24*x31*x41    
        +coeff[ 48]*x13        *x42    
        +coeff[ 49]*x13        *x41*x51
        +coeff[ 50]    *x23    *x41*x52
        +coeff[ 51]    *x21*x31*x41*x53
        +coeff[ 52]    *x22        *x54
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 53]    *x21    *x41*x54
        +coeff[ 54]*x11*x22*x32    *x51
        +coeff[ 55]*x11*x21*x31*x42*x51
        +coeff[ 56]*x11*x21    *x43*x51
        +coeff[ 57]*x11*x22        *x53
        +coeff[ 58]*x12*x21*x32    *x51
        +coeff[ 59]    *x24*x33        
        +coeff[ 60]    *x24*x32    *x51
        +coeff[ 61]    *x23*x32    *x52
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 62]    *x22*x32*x41*x52
        +coeff[ 63]        *x32*x43*x52
        +coeff[ 64]    *x21*x32    *x54
        +coeff[ 65]*x11*x23*x31*x42    
        +coeff[ 66]*x11*x21*x33*x42    
        +coeff[ 67]*x11*x23*x31*x41*x51
        +coeff[ 68]*x11*x21*x33*x41*x51
        +coeff[ 69]*x11*x21*x31*x43*x51
        +coeff[ 70]*x11*x24        *x52
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 71]*x11*x21*x32    *x53
        +coeff[ 72]*x11    *x31*x41*x54
        +coeff[ 73]*x12*x23*x32        
        +coeff[ 74]*x12*x22*x33        
        +coeff[ 75]*x13*x24            
        +coeff[ 76]    *x24*x34        
        +coeff[ 77]    *x23*x33*x42    
        +coeff[ 78]    *x22*x34*x42    
        +coeff[ 79]    *x21*x34*x43    
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 80]    *x24    *x44    
        +coeff[ 81]    *x23*x33*x41*x51
        +coeff[ 82]    *x21*x34*x42*x51
        +coeff[ 83]*x13*x22        *x52
        +coeff[ 84]*x13    *x31*x41*x52
        +coeff[ 85]    *x24*x31*x41*x52
        +coeff[ 86]    *x24    *x42*x52
        +coeff[ 87]    *x23*x32    *x53
        +coeff[ 88]    *x23*x31*x41*x53
    ;
    v_l_e_q1q2_3_600                              =v_l_e_q1q2_3_600                              
        +coeff[ 89]    *x21*x31*x43*x53
        ;

    return v_l_e_q1q2_3_600                              ;
}
float x_e_q1q2_3_500                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1272582E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.12680700E-02, 0.12101052E+00, 0.38011875E-02, 0.65059820E-03,
         0.12304302E-03,-0.56309596E-03,-0.11789446E-02,-0.90862543E-03,
        -0.24280661E-03,-0.35089298E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_500                              =v_x_e_q1q2_3_500                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_500                              ;
}
float t_e_q1q2_3_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3515214E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35215173E-04,-0.19889574E-02,-0.90413552E-03, 0.74305848E-04,
         0.22852395E-02,-0.10428391E-02,-0.95831015E-03, 0.43974906E-05,
        -0.24320003E-03,-0.27569215E-03,-0.37984541E-03,-0.89163485E-04,
        -0.82900323E-03,-0.65199455E-03,-0.30060497E-04,-0.38695296E-04,
        -0.25628413E-04, 0.97308497E-04, 0.69445523E-04,-0.24233507E-03,
        -0.49785135E-03, 0.18847912E-04,-0.11260341E-04,-0.56804633E-05,
        -0.22547133E-05, 0.27784010E-04,-0.58303762E-03, 0.86459058E-05,
        -0.12947673E-04, 0.31222422E-04,-0.21831644E-04, 0.25655519E-04,
         0.47609155E-04,-0.17336588E-04, 0.36881676E-04, 0.32536832E-06,
        -0.67362939E-05, 0.33316326E-05, 0.26791004E-05, 0.38676608E-05,
        -0.14852331E-04, 0.52036853E-05,-0.30520644E-05,-0.91075044E-05,
         0.66800571E-05, 0.56336635E-05, 0.79805213E-05,-0.11798904E-04,
         0.56307217E-05, 0.92007012E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_500                              =v_t_e_q1q2_3_500                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_500                              =v_t_e_q1q2_3_500                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x22    *x41    
        +coeff[ 24]    *x21*x32*x41    
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q1q2_3_500                              =v_t_e_q1q2_3_500                              
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]*x12*x22        *x51
        +coeff[ 28]*x11        *x42*x52
        +coeff[ 29]*x12*x21*x33        
        +coeff[ 30]    *x21*x33*x42    
        +coeff[ 31]*x11*x22*x32    *x51
        +coeff[ 32]*x12*x22    *x41*x51
        +coeff[ 33]*x11*x23    *x41*x51
        +coeff[ 34]*x12    *x31*x42*x51
    ;
    v_t_e_q1q2_3_500                              =v_t_e_q1q2_3_500                              
        +coeff[ 35]    *x21*x31        
        +coeff[ 36]        *x31    *x51
        +coeff[ 37]*x11*x21*x31        
        +coeff[ 38]        *x32*x41    
        +coeff[ 39]*x11*x21        *x51
        +coeff[ 40]*x12*x21*x31        
        +coeff[ 41]*x11*x22*x31        
        +coeff[ 42]    *x22*x32        
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q1q2_3_500                              =v_t_e_q1q2_3_500                              
        +coeff[ 44]    *x21    *x43    
        +coeff[ 45]*x12*x21        *x51
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22    *x41*x51
        +coeff[ 48]        *x32*x41*x51
        +coeff[ 49]*x13*x22            
        ;

    return v_t_e_q1q2_3_500                              ;
}
float y_e_q1q2_3_500                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3737484E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.33510485E-03, 0.16911764E+00, 0.10623707E+00,-0.24875170E-02,
        -0.22549448E-02, 0.28737332E-03,-0.14667305E-03,-0.15449875E-03,
         0.18330112E-03, 0.12577487E-03, 0.10973766E-03, 0.96470292E-04,
        -0.32070678E-03, 0.21128345E-07, 0.68583649E-04, 0.62071544E-04,
         0.20058455E-04,-0.25350589E-03,-0.22449337E-03,-0.95768723E-04,
        -0.89200905E-04,-0.16583942E-03, 0.16909373E-04, 0.12089708E-04,
         0.90161402E-05,-0.24854211E-04,-0.12993788E-04,-0.19344432E-04,
        -0.42678443E-04,-0.22192942E-04,-0.22703207E-04,-0.26247493E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_500                              =v_y_e_q1q2_3_500                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x22    *x41*x51
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_3_500                              =v_y_e_q1q2_3_500                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]*x11*x23    *x41    
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]    *x22*x33*x42    
        +coeff[ 22]            *x42    
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42*x51
        +coeff[ 25]            *x44    
    ;
    v_y_e_q1q2_3_500                              =v_y_e_q1q2_3_500                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]*x12    *x31*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x21    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41    
        +coeff[ 31]    *x21*x31*x43*x51
        ;

    return v_y_e_q1q2_3_500                              ;
}
float p_e_q1q2_3_500                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.1715774E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.15628195E-03, 0.30930756E-01, 0.64516395E-01,-0.14606513E-02,
         0.35698147E-05,-0.13216838E-02, 0.40178851E-03, 0.23728673E-03,
         0.21631873E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_500                              =v_p_e_q1q2_3_500                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_500                              ;
}
float l_e_q1q2_3_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2127918E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20626453E-02,-0.34653074E-02,-0.82049519E-03,-0.32046377E-02,
        -0.41696504E-02, 0.67358727E-04, 0.11796538E-02,-0.14062083E-03,
        -0.13994012E-03, 0.10883708E-03, 0.30562562E-04,-0.31720429E-04,
         0.40462802E-03, 0.21267575E-03,-0.33596891E-03, 0.81217336E-03,
        -0.41493605E-03, 0.31720323E-03, 0.20016111E-03,-0.23140680E-03,
         0.12049099E-02,-0.74689958E-03, 0.15291901E-02,-0.85001416E-03,
        -0.94980036E-03, 0.80580940E-03,-0.19014080E-02, 0.15300394E-03,
         0.50771370E-03,-0.87137992E-03, 0.60109806E-03, 0.28407369E-02,
         0.14907074E-02, 0.14504348E-03,-0.51360188E-04, 0.73417288E-03,
        -0.15989684E-03, 0.15846371E-02, 0.35030983E-03,-0.10843331E-02,
         0.42267839E-03,-0.11676345E-02, 0.96220808E-03,-0.18599875E-02,
         0.69965265E-03,-0.10304945E-02, 0.80354203E-03,-0.46976746E-03,
         0.30050316E-03, 0.18141759E-03,-0.85431611E-03,-0.56466262E-03,
        -0.61607431E-03, 0.17230454E-02,-0.11123216E-03, 0.24859956E-02,
         0.27778740E-02,-0.13080260E-02, 0.20464718E-03, 0.89273881E-03,
        -0.64810814E-03,-0.50047170E-02,-0.19233765E-02,-0.16461302E-02,
        -0.69028285E-03,-0.40289825E-02, 0.22601103E-02, 0.14559274E-02,
         0.29862425E-02,-0.14502145E-02,-0.19804253E-02, 0.61910372E-03,
        -0.19812647E-02,-0.71496097E-03, 0.26719004E-02,-0.10865400E-03,
         0.12688313E-03,-0.24690104E-04, 0.58282585E-04, 0.17833874E-03,
        -0.16653237E-03, 0.97204291E-04, 0.10542013E-03, 0.13179051E-03,
         0.15664978E-03,-0.99619315E-03,-0.28313970E-03,-0.11741056E-03,
        -0.17956580E-03,-0.19391684E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31*x42    
        +coeff[  6]*x11*x21*x32    *x51
        +coeff[  7]    *x21            
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[  8]            *x41    
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]            *x43    
        +coeff[ 13]                *x53
        +coeff[ 14]*x12        *x41    
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]    *x23    *x41    
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]            *x43*x51
        +coeff[ 19]                *x54
        +coeff[ 20]*x11*x22        *x51
        +coeff[ 21]*x11*x21*x31    *x51
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]    *x24        *x51
        +coeff[ 24]    *x21*x31*x41*x52
        +coeff[ 25]        *x32*x41*x52
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 26]    *x22        *x53
        +coeff[ 27]    *x21        *x54
        +coeff[ 28]*x11*x21*x33        
        +coeff[ 29]*x11*x22    *x41*x51
        +coeff[ 30]*x11        *x41*x53
        +coeff[ 31]*x12    *x32*x41    
        +coeff[ 32]*x12    *x31*x41*x51
        +coeff[ 33]*x12        *x42*x51
        +coeff[ 34]*x13*x21*x31        
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 35]    *x22*x33*x41    
        +coeff[ 36]    *x21*x34    *x51
        +coeff[ 37]    *x23*x31*x41*x51
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]    *x21*x31*x41*x53
        +coeff[ 40]        *x32*x41*x53
        +coeff[ 41]            *x43*x53
        +coeff[ 42]*x11*x24*x31        
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 44]*x11    *x34    *x51
        +coeff[ 45]*x11*x21    *x42*x52
        +coeff[ 46]*x11*x21*x31    *x53
        +coeff[ 47]*x12    *x32    *x52
        +coeff[ 48]*x12    *x31    *x53
        +coeff[ 49]*x13*x23            
        +coeff[ 50]*x13*x22*x31        
        +coeff[ 51]*x13*x21*x31*x41    
        +coeff[ 52]*x13*x22        *x51
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 53]    *x22*x34    *x51
        +coeff[ 54]    *x21*x33*x42*x51
        +coeff[ 55]    *x21*x33*x41*x52
        +coeff[ 56]    *x24        *x53
        +coeff[ 57]    *x22*x32    *x53
        +coeff[ 58]    *x23    *x41*x53
        +coeff[ 59]    *x21*x31*x42*x53
        +coeff[ 60]*x11    *x34*x41*x51
        +coeff[ 61]*x11*x21*x32*x42*x51
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 62]*x11*x21*x31*x43*x51
        +coeff[ 63]*x11*x21*x31*x41*x53
        +coeff[ 64]*x12*x21*x33*x41    
        +coeff[ 65]*x12    *x34*x41    
        +coeff[ 66]*x12*x23    *x41*x51
        +coeff[ 67]*x12*x22    *x42*x51
        +coeff[ 68]*x12*x21*x31*x42*x51
        +coeff[ 69]*x12    *x32*x42*x51
        +coeff[ 70]*x12    *x31*x41*x53
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 71]    *x23*x34*x41    
        +coeff[ 72]        *x34*x43*x51
        +coeff[ 73]*x13*x21*x31    *x52
        +coeff[ 74]        *x32*x43*x53
        +coeff[ 75]        *x31        
        +coeff[ 76]                *x51
        +coeff[ 77]*x11                
        +coeff[ 78]    *x21*x31        
        +coeff[ 79]                *x52
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 80]*x11            *x51
        +coeff[ 81]    *x23            
        +coeff[ 82]    *x21*x32        
        +coeff[ 83]        *x33        
        +coeff[ 84]    *x21*x31*x41    
        +coeff[ 85]        *x32*x41    
        +coeff[ 86]    *x21*x31    *x51
        +coeff[ 87]        *x32    *x51
        +coeff[ 88]    *x21    *x41*x51
    ;
    v_l_e_q1q2_3_500                              =v_l_e_q1q2_3_500                              
        +coeff[ 89]            *x41*x52
        ;

    return v_l_e_q1q2_3_500                              ;
}
float x_e_q1q2_3_450                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.4668793E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.47518878E-03, 0.12101891E+00, 0.37978587E-02, 0.65195927E-03,
         0.12351778E-03,-0.55634929E-03,-0.11742929E-02,-0.90449688E-03,
        -0.24615336E-03,-0.34558380E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_450                              =v_x_e_q1q2_3_450                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_450                              ;
}
float t_e_q1q2_3_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1361586E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.14124282E-04,-0.19885174E-02,-0.88716962E-03, 0.22935073E-02,
        -0.10708463E-02,-0.97318337E-03, 0.72899233E-07,-0.26354814E-03,
         0.74569645E-04,-0.27767103E-03,-0.37238732E-03,-0.89222696E-04,
        -0.79703046E-03,-0.60794345E-03,-0.36579801E-04,-0.13492662E-04,
        -0.19244948E-04, 0.67930690E-04, 0.69837944E-04,-0.55210415E-03,
        -0.45220423E-03, 0.35651265E-04,-0.28788509E-05, 0.83155928E-05,
         0.16799362E-04,-0.11629533E-04,-0.15749047E-04, 0.22363389E-04,
        -0.17565884E-04, 0.75775565E-05,-0.46194036E-04,-0.18822309E-04,
        -0.23110330E-03,-0.26000554E-04,-0.14265579E-04, 0.23653294E-04,
         0.35343714E-05, 0.22289565E-04, 0.38521503E-04, 0.56364711E-05,
         0.40803711E-05,-0.61027104E-05,-0.47089579E-05, 0.22179465E-05,
         0.19105776E-05,-0.29799978E-05,-0.44136300E-05, 0.54903171E-05,
         0.57436664E-05,-0.58448850E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_450                              =v_t_e_q1q2_3_450                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_450                              =v_t_e_q1q2_3_450                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x21*x32    *x53
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11*x22*x31        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]*x11*x21    *x41*x51
    ;
    v_t_e_q1q2_3_450                              =v_t_e_q1q2_3_450                              
        +coeff[ 26]*x11*x21        *x52
        +coeff[ 27]*x13*x22            
        +coeff[ 28]*x13    *x32        
        +coeff[ 29]*x11*x21*x33        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11    *x33*x41    
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_t_e_q1q2_3_450                              =v_t_e_q1q2_3_450                              
        +coeff[ 35]*x11*x22    *x42*x51
        +coeff[ 36]    *x22*x32    *x52
        +coeff[ 37]*x11*x21*x31*x41*x52
        +coeff[ 38]    *x21*x31*x41*x53
        +coeff[ 39]*x11*x21            
        +coeff[ 40]    *x22            
        +coeff[ 41]*x12*x21            
        +coeff[ 42]    *x22*x31        
        +coeff[ 43]        *x33        
    ;
    v_t_e_q1q2_3_450                              =v_t_e_q1q2_3_450                              
        +coeff[ 44]    *x22        *x51
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x12*x22            
        +coeff[ 47]    *x22*x32        
        +coeff[ 48]*x11*x22    *x41    
        +coeff[ 49]*x12*x21        *x51
        ;

    return v_t_e_q1q2_3_450                              ;
}
float y_e_q1q2_3_450                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.6522780E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.63290424E-03, 0.16912451E+00, 0.10621526E+00,-0.24800077E-02,
        -0.22495841E-02, 0.28488145E-03,-0.17394939E-03,-0.16569246E-03,
         0.17458721E-03, 0.10283708E-03, 0.10251776E-03, 0.97628334E-04,
        -0.30279995E-03,-0.74603859E-05,-0.32854448E-04,-0.27868591E-03,
         0.88065190E-05,-0.56532161E-08, 0.68439144E-05, 0.50848463E-04,
         0.22240825E-04, 0.26650505E-05, 0.48606555E-04, 0.59332830E-04,
        -0.17861636E-04,-0.19493282E-04,-0.15725967E-04,-0.91571994E-04,
         0.17090983E-04,-0.62332991E-04,-0.62294937E-04,-0.40445437E-04,
        -0.23920977E-04,-0.32508789E-04,-0.46612022E-04,-0.17342994E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_3_450                              =v_y_e_q1q2_3_450                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_y_e_q1q2_3_450                              =v_y_e_q1q2_3_450                              
        +coeff[ 17]        *x33        
        +coeff[ 18]            *x44    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x21    *x44    
        +coeff[ 21]*x11        *x42*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]*x11*x21    *x41*x51
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_y_e_q1q2_3_450                              =v_y_e_q1q2_3_450                              
        +coeff[ 26]    *x21    *x43*x51
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11*x21*x32*x41    
        +coeff[ 32]*x12*x21    *x42    
        +coeff[ 33]*x12    *x31*x42    
        +coeff[ 34]*x11*x23*x31        
    ;
    v_y_e_q1q2_3_450                              =v_y_e_q1q2_3_450                              
        +coeff[ 35]            *x45*x51
        ;

    return v_y_e_q1q2_3_450                              ;
}
float p_e_q1q2_3_450                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.2303788E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.22298918E-03, 0.30918919E-01, 0.64510733E-01,-0.14598122E-02,
        -0.48999249E-06,-0.13169170E-02, 0.40354405E-03, 0.23725651E-03,
         0.22198279E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]            *x43    
    ;
    v_p_e_q1q2_3_450                              =v_p_e_q1q2_3_450                              
        +coeff[  8]        *x34*x41    
        ;

    return v_p_e_q1q2_3_450                              ;
}
float l_e_q1q2_3_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2162727E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20071727E-02,-0.30588801E-02,-0.38797682E-03,-0.32283657E-02,
        -0.43552299E-02, 0.41715062E-03,-0.44361787E-03, 0.66434091E-03,
         0.24247537E-02, 0.85413124E-03,-0.17889636E-02, 0.10856499E-02,
         0.15883679E-03, 0.62604689E-04,-0.48185168E-04, 0.25172869E-03,
         0.48359850E-03,-0.55163336E-03, 0.13770927E-03, 0.21131671E-03,
         0.17255788E-03,-0.38265440E-03,-0.49403281E-03,-0.17309427E-03,
        -0.19361386E-03,-0.23913872E-03,-0.91905840E-03, 0.34208980E-02,
        -0.10469819E-04,-0.44363306E-03, 0.97010355E-03, 0.70994504E-03,
        -0.36253530E-03, 0.23613888E-03, 0.12599347E-02,-0.62407163E-03,
         0.14860427E-03,-0.22521589E-03, 0.63343969E-03, 0.35167930E-02,
         0.66015875E-03, 0.68334985E-03, 0.98184124E-03,-0.42979259E-03,
         0.50296122E-03,-0.23603076E-03,-0.21547677E-02,-0.15301537E-02,
        -0.58488181E-03, 0.28761200E-03, 0.50943712E-03,-0.11863987E-02,
         0.15095809E-02, 0.19929998E-05, 0.16212290E-02,-0.29172872E-02,
        -0.39920202E-02,-0.10809655E-02,-0.91433967E-03,-0.46864091E-03,
        -0.74621354E-03, 0.34562551E-03,-0.22409342E-02, 0.11709573E-02,
        -0.31643566E-02, 0.62082038E-03,-0.89994533E-03,-0.87373127E-03,
         0.94121060E-03,-0.48993994E-03,-0.29052921E-04,-0.19751399E-05,
        -0.68952424E-04, 0.11788253E-03,-0.89658060E-04,-0.79104109E-04,
         0.13018407E-03, 0.10755352E-03,-0.11625564E-03,-0.27485329E-03,
        -0.70289672E-04, 0.19252986E-03,-0.12214824E-03, 0.12431746E-03,
        -0.12100157E-03, 0.14753477E-03, 0.36958882E-03, 0.17723108E-03,
         0.24096883E-03, 0.14669242E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]            *x42*x51
        +coeff[  6]*x11            *x52
        +coeff[  7]*x11    *x31*x42*x51
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[  8]*x12    *x31*x43    
        +coeff[  9]*x13    *x31*x42    
        +coeff[ 10]    *x22*x34    *x51
        +coeff[ 11]*x13*x21    *x42*x51
        +coeff[ 12]                *x51
        +coeff[ 13]*x11                
        +coeff[ 14]    *x21    *x41    
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x31*x41    
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]    *x21    *x41*x51
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]*x12    *x31        
        +coeff[ 21]*x12            *x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]        *x34        
        +coeff[ 24]*x11*x21*x32        
        +coeff[ 25]*x12*x22            
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 26]*x12    *x31*x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]            *x42*x53
        +coeff[ 30]*x11    *x31*x41*x52
        +coeff[ 31]*x12    *x32    *x51
        +coeff[ 32]*x13    *x31    *x51
        +coeff[ 33]    *x22*x33    *x51
        +coeff[ 34]    *x23*x31*x41*x51
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 35]        *x33*x42*x51
        +coeff[ 36]*x13            *x52
        +coeff[ 37]    *x24        *x52
        +coeff[ 38]    *x21*x33    *x52
        +coeff[ 39]        *x32*x42*x52
        +coeff[ 40]*x11*x21*x34        
        +coeff[ 41]*x11*x23    *x42    
        +coeff[ 42]*x11*x21*x31*x41*x52
        +coeff[ 43]*x12*x23*x31        
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 44]*x12        *x44    
        +coeff[ 45]*x12            *x54
        +coeff[ 46]    *x24*x32*x41    
        +coeff[ 47]    *x22*x32*x43    
        +coeff[ 48]*x13    *x32    *x51
        +coeff[ 49]    *x24*x32    *x51
        +coeff[ 50]*x13*x21    *x41*x51
        +coeff[ 51]    *x24*x31*x41*x51
        +coeff[ 52]    *x21*x32*x42*x52
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 53]*x13            *x53
        +coeff[ 54]*x11    *x34*x42    
        +coeff[ 55]*x11    *x33*x41*x52
        +coeff[ 56]*x11    *x32*x42*x52
        +coeff[ 57]*x11    *x32*x41*x53
        +coeff[ 58]*x12*x21*x32*x42    
        +coeff[ 59]*x13    *x34        
        +coeff[ 60]*x13*x22*x31    *x51
        +coeff[ 61]    *x23*x32*x42*x51
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 62]    *x23*x31*x43*x51
        +coeff[ 63]*x13    *x32    *x52
        +coeff[ 64]        *x34*x42*x52
        +coeff[ 65]    *x22*x33    *x53
        +coeff[ 66]        *x34*x41*x53
        +coeff[ 67]    *x23    *x41*x54
        +coeff[ 68]    *x21*x32*x41*x54
        +coeff[ 69]            *x44*x54
        +coeff[ 70]    *x21            
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 71]        *x31        
        +coeff[ 72]        *x31    *x51
        +coeff[ 73]            *x41*x51
        +coeff[ 74]*x11*x21            
        +coeff[ 75]*x11    *x31        
        +coeff[ 76]*x11            *x51
        +coeff[ 77]    *x21*x32        
        +coeff[ 78]    *x21    *x42    
        +coeff[ 79]        *x31*x42    
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 80]            *x43    
        +coeff[ 81]        *x31*x41*x51
        +coeff[ 82]    *x21        *x52
        +coeff[ 83]*x11*x22            
        +coeff[ 84]*x11    *x32        
        +coeff[ 85]*x11*x21    *x41    
        +coeff[ 86]*x11    *x31*x41    
        +coeff[ 87]*x11        *x42    
        +coeff[ 88]*x11    *x31    *x51
    ;
    v_l_e_q1q2_3_450                              =v_l_e_q1q2_3_450                              
        +coeff[ 89]*x11        *x41*x51
        ;

    return v_l_e_q1q2_3_450                              ;
}
float x_e_q1q2_3_449                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1055171E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.10419578E-02, 0.12178175E+00, 0.37711691E-02, 0.67557418E-03,
         0.12245057E-03,-0.57407573E-03,-0.12846868E-02,-0.89892198E-03,
        -0.24294302E-03, 0.63385138E-04,-0.47447055E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_449                              =v_x_e_q1q2_3_449                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        ;

    return v_x_e_q1q2_3_449                              ;
}
float t_e_q1q2_3_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1900005E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.19672450E-04,-0.19722737E-02,-0.42954023E-03, 0.72710194E-04,
         0.22787540E-02,-0.11609503E-02,-0.95399370E-03, 0.19110650E-04,
        -0.32597390E-03,-0.27943385E-03,-0.47012439E-03,-0.85439868E-04,
        -0.92797593E-03,-0.65188995E-03,-0.30319741E-04,-0.24375338E-04,
        -0.26153260E-04, 0.97686738E-04, 0.57230565E-04,-0.81178378E-05,
        -0.64724142E-03,-0.50040992E-03, 0.31658194E-04,-0.75109124E-05,
        -0.87178696E-05,-0.14439222E-07,-0.12341565E-04,-0.30211682E-03,
         0.13711411E-04,-0.19755036E-04,-0.17417211E-05,-0.38293415E-05,
        -0.12967299E-05, 0.31410493E-05, 0.64264459E-05, 0.59755848E-05,
         0.76083802E-05,-0.46987896E-06, 0.40064406E-05, 0.43885266E-05,
         0.17394706E-04, 0.75587718E-05,-0.51071070E-05,-0.43471468E-05,
         0.20495741E-04,-0.37671127E-05,-0.98024311E-05,-0.50842350E-05,
        -0.14837238E-04,-0.20281384E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_449                              =v_t_e_q1q2_3_449                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_449                              =v_t_e_q1q2_3_449                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x21*x31        
        +coeff[ 24]*x12*x21            
        +coeff[ 25]    *x22*x31        
    ;
    v_t_e_q1q2_3_449                              =v_t_e_q1q2_3_449                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]*x12*x22        *x51
        +coeff[ 29]    *x23*x31    *x51
        +coeff[ 30]                *x51
        +coeff[ 31]    *x22            
        +coeff[ 32]            *x43    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]    *x21*x31    *x51
    ;
    v_t_e_q1q2_3_449                              =v_t_e_q1q2_3_449                              
        +coeff[ 35]*x12*x21*x31        
        +coeff[ 36]    *x22*x32        
        +coeff[ 37]*x11    *x33        
        +coeff[ 38]*x11*x21*x31*x41    
        +coeff[ 39]*x11    *x32*x41    
        +coeff[ 40]    *x21*x31*x42    
        +coeff[ 41]    *x21    *x43    
        +coeff[ 42]*x11*x21*x31    *x51
        +coeff[ 43]    *x22*x31    *x51
    ;
    v_t_e_q1q2_3_449                              =v_t_e_q1q2_3_449                              
        +coeff[ 44]    *x21*x32    *x51
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]*x11*x23    *x41    
        +coeff[ 48]*x13    *x31*x41    
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q1q2_3_449                              ;
}
float y_e_q1q2_3_449                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.7421542E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.75410964E-03, 0.16858748E+00, 0.11752925E+00,-0.24569235E-02,
        -0.24772591E-02, 0.36188887E-03,-0.91343194E-04,-0.55032709E-04,
        -0.26527210E-03, 0.19667936E-03,-0.10417851E-03, 0.17826201E-03,
        -0.36658450E-04, 0.96650481E-04, 0.78602192E-04,-0.37528208E-03,
        -0.17160430E-04, 0.58513298E-04,-0.39105694E-03,-0.14982119E-03,
         0.50919982E-04,-0.40105035E-04,-0.75816529E-05,-0.30168251E-03,
         0.14595265E-04,-0.44439596E-04, 0.39801118E-04,-0.29440997E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_449                              =v_y_e_q1q2_3_449                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x13*x21    *x41    
    ;
    v_y_e_q1q2_3_449                              =v_y_e_q1q2_3_449                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x24    *x41*x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]            *x44    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_3_449                              =v_y_e_q1q2_3_449                              
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]*x11*x21*x32*x42    
        ;

    return v_y_e_q1q2_3_449                              ;
}
float p_e_q1q2_3_449                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.3057105E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.30891690E-03, 0.34073077E-01, 0.64220078E-01,-0.14499854E-02,
        -0.14449551E-02, 0.40813861E-03, 0.21758718E-03, 0.23453205E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]            *x43    
        +coeff[  7]        *x34*x41    
        ;

    return v_p_e_q1q2_3_449                              ;
}
float l_e_q1q2_3_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2100558E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21254031E-02, 0.95278745E-04,-0.34089757E-02,-0.10057262E-02,
        -0.38400819E-02,-0.44197482E-02,-0.64826905E-04, 0.74615360E-04,
        -0.12830490E-02, 0.60595466E-04,-0.49964170E-03, 0.46980896E-03,
        -0.10560832E-03, 0.21891638E-02, 0.14741301E-03,-0.18617114E-03,
        -0.76437107E-04, 0.24988336E-03, 0.80231094E-03, 0.10600358E-02,
         0.93654863E-03, 0.10951287E-02, 0.11562686E-02, 0.10796983E-02,
        -0.17184381E-03, 0.80026244E-03,-0.68847777E-03,-0.50024060E-03,
         0.78847248E-03,-0.74076990E-03,-0.85544074E-03, 0.13549603E-02,
         0.81791484E-03, 0.32850070E-03, 0.51500939E-03,-0.23135969E-02,
        -0.11463431E-03,-0.91563194E-03, 0.12634433E-02, 0.37677013E-03,
         0.94745593E-03, 0.66142675E-03,-0.52093720E-03, 0.50038949E-03,
         0.84894600E-04, 0.16077269E-02, 0.11466449E-02,-0.11188749E-02,
         0.61170134E-03,-0.57465612E-03,-0.29132370E-04, 0.54378738E-03,
        -0.19627593E-02, 0.10340749E-02,-0.53270481E-03,-0.57221908E-03,
        -0.47294059E-03,-0.43454216E-03, 0.33779296E-02,-0.66785037E-03,
        -0.10311534E-02,-0.12602563E-02, 0.32945062E-03, 0.96047943E-03,
         0.12666305E-02,-0.17980873E-02, 0.13883581E-02,-0.45954945E-03,
         0.67411631E-03,-0.73999085E-03,-0.10955043E-02,-0.94602286E-03,
         0.24195828E-02, 0.16001045E-02,-0.10521215E-02, 0.97061740E-03,
        -0.10110305E-02, 0.22453815E-03,-0.58674232E-04, 0.87071807E-04,
        -0.90300062E-04, 0.50551629E-04,-0.11954884E-03, 0.16007911E-03,
        -0.30146053E-03,-0.61571883E-03,-0.35534170E-03, 0.10478845E-03,
         0.15429824E-03,-0.35761128E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x21*x33        
        +coeff[  7]            *x41    
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x33        
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x12    *x31        
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]        *x32*x41*x51
        +coeff[ 14]        *x31*x42*x51
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]*x11*x22        *x51
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 17]*x11        *x42*x51
        +coeff[ 18]*x11    *x31    *x52
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]    *x24*x31        
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x21    *x44    
        +coeff[ 25]    *x24        *x51
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 26]    *x21*x32    *x52
        +coeff[ 27]*x11*x21*x33        
        +coeff[ 28]*x11*x21*x32*x41    
        +coeff[ 29]*x11    *x33    *x51
        +coeff[ 30]*x12    *x33        
        +coeff[ 31]*x12*x21*x31*x41    
        +coeff[ 32]*x12*x21    *x42    
        +coeff[ 33]*x12*x21    *x41*x51
        +coeff[ 34]*x13    *x31    *x51
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 35]        *x34*x41*x51
        +coeff[ 36]    *x22*x31*x42*x51
        +coeff[ 37]    *x21*x33    *x52
        +coeff[ 38]    *x21*x31*x42*x52
        +coeff[ 39]            *x44*x52
        +coeff[ 40]    *x21*x31    *x54
        +coeff[ 41]        *x31*x41*x54
        +coeff[ 42]*x11*x23*x32        
        +coeff[ 43]*x11    *x34*x41    
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 44]*x11*x22*x31*x41*x51
        +coeff[ 45]*x11*x22    *x42*x51
        +coeff[ 46]*x11*x21*x31*x42*x51
        +coeff[ 47]*x11        *x44*x51
        +coeff[ 48]*x11*x21    *x42*x52
        +coeff[ 49]*x11*x21*x31    *x53
        +coeff[ 50]*x12*x22*x32        
        +coeff[ 51]*x12    *x34        
        +coeff[ 52]*x12*x22*x31*x41    
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 53]*x12*x23        *x51
        +coeff[ 54]*x12*x21*x32    *x51
        +coeff[ 55]*x12*x22        *x52
        +coeff[ 56]*x12*x21        *x53
        +coeff[ 57]*x12        *x41*x53
        +coeff[ 58]    *x22*x33*x42    
        +coeff[ 59]*x13    *x31    *x52
        +coeff[ 60]        *x31*x44*x52
        +coeff[ 61]        *x32*x41*x54
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 62]            *x43*x54
        +coeff[ 63]*x11*x23*x33        
        +coeff[ 64]*x11*x22*x31*x43    
        +coeff[ 65]*x11*x22*x31*x42*x51
        +coeff[ 66]*x12    *x33    *x52
        +coeff[ 67]    *x24*x34        
        +coeff[ 68]*x13*x22    *x42    
        +coeff[ 69]    *x24    *x44    
        +coeff[ 70]    *x24*x33    *x51
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 71]*x13    *x32*x41*x51
        +coeff[ 72]    *x22*x33*x42*x51
        +coeff[ 73]    *x21*x34*x41*x52
        +coeff[ 74]        *x33*x43*x52
        +coeff[ 75]        *x34*x41*x53
        +coeff[ 76]    *x23*x31    *x54
        +coeff[ 77]        *x31        
        +coeff[ 78]    *x21        *x51
        +coeff[ 79]        *x31    *x51
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 80]            *x41*x51
        +coeff[ 81]                *x52
        +coeff[ 82]    *x23            
        +coeff[ 83]    *x21*x32        
        +coeff[ 84]    *x21*x31*x41    
        +coeff[ 85]        *x32*x41    
        +coeff[ 86]    *x21    *x42    
        +coeff[ 87]            *x42*x51
        +coeff[ 88]    *x21        *x52
    ;
    v_l_e_q1q2_3_449                              =v_l_e_q1q2_3_449                              
        +coeff[ 89]        *x31    *x52
        ;

    return v_l_e_q1q2_3_449                              ;
}
float x_e_q1q2_3_400                              (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.2126738E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.22674736E-03, 0.12187235E+00, 0.37682308E-02, 0.68031455E-03,
         0.12161044E-03,-0.57260820E-03,-0.12888963E-02,-0.89884142E-03,
        -0.24523784E-03, 0.53952539E-04,-0.47196972E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_400                              =v_x_e_q1q2_3_400                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        ;

    return v_x_e_q1q2_3_400                              ;
}
float t_e_q1q2_3_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2127767E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.20702386E-04,-0.19706131E-02,-0.35136650E-03, 0.22563336E-02,
        -0.11790872E-02,-0.96236705E-03, 0.24143981E-05,-0.29101854E-03,
         0.73968375E-04,-0.28432682E-03,-0.48248627E-03,-0.83098304E-04,
        -0.86942792E-03,-0.61569939E-03,-0.26241716E-04, 0.94153402E-04,
        -0.29297267E-04,-0.15817584E-04, 0.41372426E-04,-0.64141490E-03,
        -0.49763743E-03,-0.81737135E-05,-0.14916443E-04,-0.16208322E-05,
        -0.75272187E-05, 0.69870257E-04,-0.89990808E-05, 0.16973976E-04,
        -0.19596488E-04, 0.15623762E-04,-0.40932955E-04,-0.30021244E-03,
        -0.28636658E-04, 0.16622102E-04,-0.13854775E-04,-0.16328218E-04,
        -0.24458115E-04,-0.16882774E-04, 0.54598015E-04, 0.41873212E-04,
         0.13960767E-04,-0.16487176E-04, 0.20006268E-04,-0.35448757E-04,
        -0.16233563E-05, 0.29173193E-05, 0.37409127E-05, 0.38444800E-05,
        -0.30834365E-05,-0.65430704E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_400                              =v_t_e_q1q2_3_400                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_400                              =v_t_e_q1q2_3_400                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q1q2_3_400                              =v_t_e_q1q2_3_400                              
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x13*x21*x31        
        +coeff[ 29]*x11*x21*x33        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11        *x42*x52
    ;
    v_t_e_q1q2_3_400                              =v_t_e_q1q2_3_400                              
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]*x13*x22    *x41    
        +coeff[ 37]*x11*x22*x32    *x51
        +coeff[ 38]*x12*x21*x31*x41*x51
        +coeff[ 39]    *x23    *x42*x51
        +coeff[ 40]*x13*x21        *x52
        +coeff[ 41]*x11*x22*x31    *x52
        +coeff[ 42]*x12*x21        *x53
        +coeff[ 43]    *x21*x32    *x53
    ;
    v_t_e_q1q2_3_400                              =v_t_e_q1q2_3_400                              
        +coeff[ 44]        *x32        
        +coeff[ 45]*x11        *x41    
        +coeff[ 46]*x12        *x41    
        +coeff[ 47]*x11*x21    *x41    
        +coeff[ 48]        *x32*x41    
        +coeff[ 49]*x11*x21        *x51
        ;

    return v_t_e_q1q2_3_400                              ;
}
float y_e_q1q2_3_400                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.7789440E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.83209283E-03, 0.16859239E+00, 0.11745175E+00,-0.24558743E-02,
        -0.24659582E-02, 0.37220595E-03,-0.10523999E-03,-0.57261193E-04,
        -0.29849543E-03, 0.19670730E-03,-0.72626906E-04, 0.18558136E-03,
        -0.31149993E-04, 0.11685817E-03, 0.10322857E-03,-0.38264701E-03,
        -0.60949966E-04,-0.50160179E-04, 0.52383861E-04,-0.37246037E-03,
        -0.11534613E-03,-0.23119119E-05,-0.70264368E-05,-0.24741863E-04,
        -0.13856543E-04,-0.19775467E-04, 0.68667439E-04, 0.12639582E-04,
         0.13908640E-04,-0.29824112E-03,-0.13931951E-04, 0.65420172E-04,
        -0.49782855E-04,-0.32945976E-04,-0.65459717E-04, 0.33715060E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_400                              =v_y_e_q1q2_3_400                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_400                              =v_y_e_q1q2_3_400                              
        +coeff[ 17]        *x31*x42*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]                *x51
        +coeff[ 22]*x11        *x42    
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]        *x32*x42    
        +coeff[ 25]*x11    *x31*x42    
    ;
    v_y_e_q1q2_3_400                              =v_y_e_q1q2_3_400                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]        *x31*x41*x52
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]*x11        *x41*x52
        +coeff[ 31]    *x22    *x44    
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]    *x22    *x41*x52
        +coeff[ 34]    *x22    *x43*x51
    ;
    v_y_e_q1q2_3_400                              =v_y_e_q1q2_3_400                              
        +coeff[ 35]*x11*x21    *x41*x52
        ;

    return v_y_e_q1q2_3_400                              ;
}
float p_e_q1q2_3_400                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3381421E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.35716101E-03, 0.34024835E-01, 0.64302787E-01,-0.14497536E-02,
        -0.14414591E-02, 0.39250491E-03, 0.35380540E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_400                              ;
}
float l_e_q1q2_3_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2115629E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21654507E-02,-0.36333913E-02,-0.71789121E-03,-0.32777695E-02,
        -0.43726591E-02,-0.16653321E-03,-0.15158059E-04, 0.47887763E-03,
        -0.30319387E-03,-0.74315110E-04,-0.92194059E-04,-0.16052224E-03,
         0.36799925E-03,-0.89285342E-04, 0.35306215E-03, 0.18470732E-02,
        -0.69186476E-03, 0.13025932E-03, 0.24954515E-03,-0.10099075E-03,
        -0.17490918E-03,-0.14890799E-02,-0.25530328E-03, 0.23337480E-03,
         0.24736841E-03, 0.13644138E-02,-0.30218763E-03, 0.85653221E-04,
        -0.24550837E-02, 0.17855625E-02, 0.71488944E-03, 0.58625871E-03,
        -0.16450552E-03,-0.14304520E-02,-0.35231930E-03,-0.53903804E-03,
        -0.74011949E-03, 0.62892906E-03, 0.18992214E-02, 0.96366194E-03,
        -0.18693076E-03, 0.43516400E-04,-0.13275250E-02, 0.54043479E-03,
        -0.14233269E-02, 0.42287007E-03,-0.10884580E-02, 0.36872056E-03,
        -0.11962623E-02,-0.25519384E-02,-0.18313850E-03, 0.28857524E-02,
        -0.72545803E-03,-0.52127510E-03,-0.12998733E-02,-0.41387826E-02,
         0.24204126E-02,-0.18346260E-02, 0.14994725E-02,-0.25596975E-02,
        -0.41589909E-03, 0.61510998E-03,-0.14417822E-02,-0.52845292E-03,
         0.33800726E-03,-0.83376822E-03,-0.96289947E-03,-0.67454478E-03,
         0.18424341E-03, 0.10843321E-02,-0.14119369E-02, 0.12420454E-02,
        -0.86266757E-03, 0.16058959E-02, 0.24111067E-04, 0.18478367E-03,
        -0.17671869E-03, 0.12741535E-03, 0.33641089E-03, 0.20248428E-03,
         0.10403859E-03,-0.33459265E-03, 0.30831221E-03, 0.14044886E-03,
         0.26257368E-03,-0.23418244E-03, 0.81326027E-04, 0.17545965E-03,
        -0.41014020E-03,-0.17315455E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11    *x32        
        +coeff[  6]*x11            *x52
        +coeff[  7]        *x31*x43*x51
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[  8]        *x31        
        +coeff[  9]            *x41    
        +coeff[ 10]                *x51
        +coeff[ 11]*x11                
        +coeff[ 12]            *x41*x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]        *x32*x41    
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]                *x53
        +coeff[ 19]*x11*x21        *x51
        +coeff[ 20]*x11        *x41*x51
        +coeff[ 21]        *x31*x43    
        +coeff[ 22]            *x41*x53
        +coeff[ 23]*x11*x22    *x41    
        +coeff[ 24]*x11*x21*x31    *x51
        +coeff[ 25]*x11    *x32    *x51
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x21*x33*x41    
        +coeff[ 29]        *x34*x41    
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]*x13            *x51
        +coeff[ 33]    *x22    *x42*x51
        +coeff[ 34]    *x23        *x52
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 35]    *x21*x31*x41*x52
        +coeff[ 36]*x11*x21*x32    *x51
        +coeff[ 37]*x11*x21    *x42*x51
        +coeff[ 38]*x11*x22        *x52
        +coeff[ 39]*x12    *x31*x41*x51
        +coeff[ 40]*x13*x21    *x41    
        +coeff[ 41]        *x34*x42    
        +coeff[ 42]        *x32*x44    
        +coeff[ 43]    *x24        *x52
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 44]        *x32    *x54
        +coeff[ 45]*x11*x24        *x51
        +coeff[ 46]*x11    *x34    *x51
        +coeff[ 47]    *x23*x32*x42    
        +coeff[ 48]    *x21*x33*x43    
        +coeff[ 49]    *x21*x32*x44    
        +coeff[ 50]    *x21*x33*x42*x51
        +coeff[ 51]    *x22    *x44*x51
        +coeff[ 52]*x13*x21        *x52
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 53]    *x24*x31    *x52
        +coeff[ 54]    *x23*x32    *x52
        +coeff[ 55]    *x23*x31*x41*x52
        +coeff[ 56]    *x21*x33*x41*x52
        +coeff[ 57]        *x33*x41*x53
        +coeff[ 58]    *x21*x31*x42*x53
        +coeff[ 59]        *x32*x42*x53
        +coeff[ 60]*x11*x24*x32        
        +coeff[ 61]*x11*x21*x32*x43    
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 62]*x11*x24        *x52
        +coeff[ 63]*x11*x23*x31    *x52
        +coeff[ 64]*x11*x21    *x41*x54
        +coeff[ 65]*x12*x24*x31        
        +coeff[ 66]*x12    *x34*x41    
        +coeff[ 67]*x12*x23*x31    *x51
        +coeff[ 68]*x12    *x34    *x51
        +coeff[ 69]*x13*x21*x33        
        +coeff[ 70]*x13*x23    *x41    
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 71]    *x22*x34*x41*x51
        +coeff[ 72]        *x32*x43*x53
        +coeff[ 73]        *x34    *x54
        +coeff[ 74]    *x21            
        +coeff[ 75]*x11*x21            
        +coeff[ 76]*x11            *x51
        +coeff[ 77]    *x23            
        +coeff[ 78]        *x33        
        +coeff[ 79]        *x32    *x51
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 80]    *x21    *x41*x51
        +coeff[ 81]*x11*x21*x31        
        +coeff[ 82]*x11*x21    *x41    
        +coeff[ 83]*x13                
        +coeff[ 84]    *x21*x33        
        +coeff[ 85]        *x34        
        +coeff[ 86]    *x23    *x41    
        +coeff[ 87]    *x22*x31*x41    
        +coeff[ 88]    *x21*x31*x42    
    ;
    v_l_e_q1q2_3_400                              =v_l_e_q1q2_3_400                              
        +coeff[ 89]    *x21    *x43    
        ;

    return v_l_e_q1q2_3_400                              ;
}
float x_e_q1q2_3_350                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2510251E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.25129356E-03, 0.12204413E+00, 0.37633199E-02, 0.68570778E-03,
         0.12291840E-03,-0.55466307E-03,-0.12796539E-02,-0.89174695E-03,
        -0.24186756E-03,-0.43674943E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_350                              =v_x_e_q1q2_3_350                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_350                              ;
}
float t_e_q1q2_3_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1597226E-04;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.15565451E-04,-0.19623581E-02,-0.25015307E-03, 0.22552346E-02,
        -0.11464601E-02,-0.94912009E-03, 0.10816862E-04,-0.31405152E-03,
         0.76005388E-04,-0.27956153E-03,-0.47016487E-03,-0.86753505E-04,
        -0.89051394E-03,-0.61803759E-03,-0.26293570E-04, 0.12232376E-03,
        -0.42269636E-04,-0.28746425E-04, 0.51793733E-04, 0.71033050E-04,
         0.12638113E-04,-0.70051086E-03,-0.53824397E-03,-0.23742026E-04,
         0.46875621E-05,-0.45089023E-05, 0.27664775E-04,-0.53416502E-05,
         0.14288241E-04,-0.33178626E-03,-0.15022438E-04,-0.28612732E-04,
        -0.17832894E-04,-0.22740098E-04,-0.50226331E-05,-0.24349512E-06,
        -0.21022968E-05,-0.60441116E-05, 0.27082265E-05, 0.75252119E-05,
        -0.55596556E-05, 0.43867658E-05,-0.88093111E-05,-0.79109050E-05,
         0.35816347E-05, 0.57270140E-05,-0.39586275E-05, 0.46976834E-05,
        -0.50298922E-05, 0.50632416E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_350                              =v_t_e_q1q2_3_350                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_350                              =v_t_e_q1q2_3_350                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x32    *x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x22    *x41    
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q1q2_3_350                              =v_t_e_q1q2_3_350                              
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]    *x21*x33*x41    
        +coeff[ 30]*x12*x21    *x42    
        +coeff[ 31]*x11*x22*x33        
        +coeff[ 32]*x11*x23    *x41*x51
        +coeff[ 33]    *x21*x32*x41*x52
        +coeff[ 34]*x11    *x31        
    ;
    v_t_e_q1q2_3_350                              =v_t_e_q1q2_3_350                              
        +coeff[ 35]    *x21*x31        
        +coeff[ 36]            *x42    
        +coeff[ 37]*x13                
        +coeff[ 38]*x11        *x41*x51
        +coeff[ 39]*x13*x21            
        +coeff[ 40]*x11*x23            
        +coeff[ 41]*x13    *x31        
        +coeff[ 42]*x12*x21*x31        
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q1q2_3_350                              =v_t_e_q1q2_3_350                              
        +coeff[ 44]    *x23    *x41    
        +coeff[ 45]*x11*x21*x31*x41    
        +coeff[ 46]    *x22    *x42    
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]*x13            *x51
        +coeff[ 49]*x11*x22        *x51
        ;

    return v_t_e_q1q2_3_350                              ;
}
float y_e_q1q2_3_350                              (float *x,int m){
    int ncoeff= 27;
    float avdat=  0.1699479E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
        -0.16480881E-02, 0.16829619E+00, 0.11732345E+00,-0.24443779E-02,
        -0.24679783E-02,-0.18746551E-03,-0.23365654E-03, 0.32322446E-03,
        -0.17982797E-03,-0.18737113E-04, 0.14136771E-03,-0.38450321E-05,
         0.99318611E-04, 0.10428135E-03,-0.32395642E-03, 0.19820106E-03,
        -0.31590605E-05,-0.97522963E-04, 0.93965818E-05, 0.52358329E-04,
         0.52738960E-04,-0.11872545E-03,-0.69272333E-04,-0.78281919E-04,
        -0.28633995E-04, 0.20939975E-04, 0.42269938E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q1q2_3_350                              =v_y_e_q1q2_3_350                              
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_350                              =v_y_e_q1q2_3_350                              
        +coeff[ 17]*x11*x23*x31        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]*x11*x23    *x41    
        +coeff[ 24]*x12    *x31*x42    
        +coeff[ 25]*x11*x23    *x42    
    ;
    v_y_e_q1q2_3_350                              =v_y_e_q1q2_3_350                              
        +coeff[ 26]    *x22    *x45    
        ;

    return v_y_e_q1q2_3_350                              ;
}
float p_e_q1q2_3_350                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.6056222E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.59000601E-03, 0.33949971E-01, 0.64156488E-01,-0.14463216E-02,
        -0.14389717E-02, 0.39342724E-03, 0.35422563E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_350                              ;
}
float l_e_q1q2_3_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2153837E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21236420E-02,-0.32634132E-02,-0.10822919E-02,-0.37264188E-02,
        -0.44193869E-02,-0.17008411E-03,-0.10537283E-03,-0.22516005E-03,
        -0.80516323E-03, 0.26291484E-03, 0.48272614E-03,-0.11648587E-02,
         0.17694004E-03,-0.76101068E-03, 0.23633658E-03, 0.65500860E-03,
        -0.28951617E-03,-0.52220887E-03, 0.24069501E-02,-0.33255611E-03,
         0.74680004E-03, 0.10975316E-02, 0.40165330E-02, 0.35930550E-03,
        -0.68834203E-03, 0.20498717E-03,-0.19620194E-02,-0.27875500E-02,
        -0.37685491E-03,-0.12462669E-02, 0.17471850E-02,-0.18908932E-02,
         0.95448288E-03,-0.13211915E-03, 0.15740505E-03,-0.26702741E-02,
        -0.25172972E-02,-0.16874203E-02, 0.31702579E-02, 0.33127624E-03,
        -0.30546708E-02, 0.11140108E-02, 0.16294271E-02,-0.24646686E-02,
        -0.92158711E-03,-0.15701459E-02,-0.17024651E-02,-0.80326112E-03,
        -0.22080720E-02, 0.24055336E-02, 0.11103513E-02,-0.43226953E-03,
         0.31299450E-03, 0.55402197E-04,-0.38635026E-05,-0.24860777E-03,
        -0.87968037E-04, 0.25983376E-03,-0.37665875E-03,-0.43802214E-04,
        -0.11548601E-03, 0.14999371E-03,-0.11394989E-03, 0.23144699E-03,
        -0.50599017E-03,-0.11739226E-03, 0.54543611E-03, 0.55163208E-03,
         0.10443702E-02, 0.50367170E-03, 0.21103040E-03, 0.81393693E-04,
         0.42153729E-03,-0.17365036E-03,-0.15206273E-03,-0.62979940E-04,
        -0.15020462E-03,-0.11201978E-03,-0.20395374E-03, 0.52976125E-03,
        -0.11795201E-03, 0.24536575E-03,-0.34973986E-03,-0.52089774E-03,
         0.53074444E-03,-0.18540028E-03, 0.88678050E-03, 0.32479258E-03,
         0.90552721E-03, 0.19081190E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31        
        +coeff[  6]    *x21*x31        
        +coeff[  7]    *x23            
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]        *x33        
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]*x13                
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]*x11*x21    *x41*x51
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]*x12        *x41*x51
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 17]        *x33*x41*x51
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]*x11*x21*x33        
        +coeff[ 20]*x11*x22*x31*x41    
        +coeff[ 21]*x11*x21*x32    *x51
        +coeff[ 22]*x11*x21*x31    *x52
        +coeff[ 23]*x11            *x54
        +coeff[ 24]*x12    *x31    *x52
        +coeff[ 25]*x13    *x32        
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 26]        *x34*x42    
        +coeff[ 27]        *x33*x43    
        +coeff[ 28]    *x21    *x43*x52
        +coeff[ 29]*x11*x22        *x53
        +coeff[ 30]*x12*x22*x32        
        +coeff[ 31]*x12*x22        *x52
        +coeff[ 32]*x12            *x54
        +coeff[ 33]    *x22*x31*x44    
        +coeff[ 34]    *x24*x31    *x52
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 35]    *x22*x32*x41*x52
        +coeff[ 36]*x11*x23*x33        
        +coeff[ 37]*x11*x24    *x41*x51
        +coeff[ 38]*x11*x21*x33*x41*x51
        +coeff[ 39]*x11*x22    *x43*x51
        +coeff[ 40]*x11*x21*x31    *x54
        +coeff[ 41]*x12*x22*x31*x42    
        +coeff[ 42]*x13*x21*x33        
        +coeff[ 43]        *x34*x44    
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 44]    *x24*x33    *x51
        +coeff[ 45]*x13*x21*x31*x41*x51
        +coeff[ 46]*x13*x21*x31    *x52
        +coeff[ 47]*x13    *x31*x41*x52
        +coeff[ 48]    *x23*x32*x41*x52
        +coeff[ 49]    *x23    *x43*x52
        +coeff[ 50]    *x22    *x44*x52
        +coeff[ 51]*x13*x21        *x53
        +coeff[ 52]    *x21            
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 53]            *x41    
        +coeff[ 54]*x11                
        +coeff[ 55]                *x52
        +coeff[ 56]*x11            *x51
        +coeff[ 57]    *x22*x31        
        +coeff[ 58]    *x21    *x42    
        +coeff[ 59]            *x43    
        +coeff[ 60]    *x21        *x52
        +coeff[ 61]            *x41*x52
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 62]*x11    *x31    *x51
        +coeff[ 63]*x11        *x41*x51
        +coeff[ 64]*x11            *x52
        +coeff[ 65]*x12        *x41    
        +coeff[ 66]        *x34        
        +coeff[ 67]    *x22*x31*x41    
        +coeff[ 68]        *x32*x42    
        +coeff[ 69]    *x22*x31    *x51
        +coeff[ 70]    *x22    *x41*x51
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 71]        *x32*x41*x51
        +coeff[ 72]    *x22        *x52
        +coeff[ 73]    *x21*x31    *x52
        +coeff[ 74]        *x32    *x52
        +coeff[ 75]    *x21        *x53
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]*x11*x21*x32        
        +coeff[ 78]*x11*x21*x31*x41    
        +coeff[ 79]*x11*x22        *x51
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 80]*x11*x21        *x52
        +coeff[ 81]*x11            *x53
        +coeff[ 82]*x12    *x32        
        +coeff[ 83]    *x22*x33        
        +coeff[ 84]    *x21*x34        
        +coeff[ 85]    *x23*x31*x41    
        +coeff[ 86]    *x22*x32*x41    
        +coeff[ 87]    *x21*x33*x41    
        +coeff[ 88]    *x21*x32*x42    
    ;
    v_l_e_q1q2_3_350                              =v_l_e_q1q2_3_350                              
        +coeff[ 89]        *x33*x42    
        ;

    return v_l_e_q1q2_3_350                              ;
}
float x_e_q1q2_3_300                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.8072755E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.83007477E-03, 0.12226100E+00, 0.37556749E-02, 0.69272000E-03,
         0.12206679E-03,-0.54718123E-03,-0.12651263E-02,-0.88204222E-03,
        -0.23683882E-03,-0.43328980E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_300                              =v_x_e_q1q2_3_300                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_300                              ;
}
float t_e_q1q2_3_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.6057385E-05;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.67859596E-05,-0.19610012E-02,-0.11101623E-03, 0.22473149E-02,
        -0.11558469E-02,-0.94237825E-03, 0.12967143E-04,-0.31437140E-03,
         0.72818286E-04,-0.27390744E-03,-0.47001863E-03,-0.87717810E-04,
        -0.90190693E-03,-0.65735879E-03,-0.20893951E-04,-0.41319170E-04,
        -0.29292718E-04, 0.92161303E-04, 0.76903205E-04, 0.21696235E-05,
        -0.31288280E-03,-0.66755031E-03,-0.50957775E-03,-0.65382169E-05,
        -0.43558789E-05, 0.77216946E-05,-0.88015241E-06,-0.76991064E-05,
        -0.17961278E-04, 0.57985710E-06,-0.63439375E-05, 0.50117895E-04,
         0.18433871E-04, 0.15316225E-04,-0.52360054E-04,-0.35849735E-04,
         0.41038729E-04, 0.16348440E-04,-0.21891258E-05, 0.13346322E-05,
        -0.41157914E-05, 0.35268840E-05,-0.21806688E-05,-0.16616093E-05,
         0.65068325E-05, 0.12354206E-04,-0.41916437E-05,-0.16777976E-06,
         0.10608804E-04, 0.56561798E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_300                              =v_t_e_q1q2_3_300                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_300                              =v_t_e_q1q2_3_300                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x21*x32    *x53
        +coeff[ 25]    *x22            
    ;
    v_t_e_q1q2_3_300                              =v_t_e_q1q2_3_300                              
        +coeff[ 26]            *x42    
        +coeff[ 27]*x12*x21            
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x22    *x41    
        +coeff[ 30]    *x22    *x42    
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]    *x21        *x53
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]*x12*x22*x31*x41    
    ;
    v_t_e_q1q2_3_300                              =v_t_e_q1q2_3_300                              
        +coeff[ 35]*x12*x22    *x42    
        +coeff[ 36]    *x21*x31*x43*x51
        +coeff[ 37]*x13*x21        *x52
        +coeff[ 38]*x11*x21            
        +coeff[ 39]        *x31    *x51
        +coeff[ 40]*x13                
        +coeff[ 41]*x12            *x51
        +coeff[ 42]            *x41*x52
        +coeff[ 43]                *x53
    ;
    v_t_e_q1q2_3_300                              =v_t_e_q1q2_3_300                              
        +coeff[ 44]*x11*x21*x31*x41    
        +coeff[ 45]    *x22*x31*x41    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]*x11    *x32    *x51
        +coeff[ 48]*x11    *x31*x41*x51
        +coeff[ 49]*x11        *x42*x51
        ;

    return v_t_e_q1q2_3_300                              ;
}
float y_e_q1q2_3_300                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.6888207E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.67905145E-03, 0.16829473E+00, 0.11718872E+00,-0.24525053E-02,
        -0.24637335E-02, 0.34090577E-03,-0.11336146E-03,-0.23740589E-04,
        -0.23544180E-03, 0.18463349E-03,-0.14085928E-03, 0.15708563E-03,
         0.10734224E-03, 0.10990757E-03,-0.36575354E-03,-0.63690650E-05,
         0.56672146E-04, 0.64825508E-04, 0.28038623E-05,-0.90171285E-04,
        -0.93204115E-04, 0.60522594E-04,-0.23077351E-03,-0.29924131E-03,
        -0.12009866E-04,-0.10951858E-03, 0.18308878E-04,-0.27519267E-04,
        -0.13688305E-04,-0.53004100E-04, 0.20104266E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_300                              =v_y_e_q1q2_3_300                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22    *x41*x51
    ;
    v_y_e_q1q2_3_300                              =v_y_e_q1q2_3_300                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]*x11*x21    *x43    
        +coeff[ 19]*x11*x23    *x41    
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]    *x21*x33*x43    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_3_300                              =v_y_e_q1q2_3_300                              
        +coeff[ 26]    *x21    *x42*x52
        +coeff[ 27]    *x21*x31*x43*x51
        +coeff[ 28]*x12        *x44    
        +coeff[ 29]    *x22*x31*x42*x51
        +coeff[ 30]    *x21    *x43*x52
        ;

    return v_y_e_q1q2_3_300                              ;
}
float p_e_q1q2_3_300                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2824265E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.28072900E-03, 0.33868227E-01, 0.64123265E-01,-0.14411538E-02,
        -0.14351858E-02, 0.37106604E-03, 0.32569733E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_300                              ;
}
float l_e_q1q2_3_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2122879E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20685662E-02,-0.30765876E-02,-0.99643914E-03,-0.29547650E-02,
        -0.41882219E-02, 0.29904844E-03,-0.25370208E-03,-0.52016077E-03,
        -0.13315086E-02, 0.17821267E-02, 0.57523814E-03, 0.25155781E-04,
         0.35177372E-03, 0.83462728E-04,-0.43889106E-03, 0.20515105E-03,
        -0.13348150E-03, 0.98773604E-03, 0.11619939E-02, 0.75447862E-03,
        -0.19307229E-02,-0.52795006E-03,-0.31183098E-03, 0.11281517E-03,
        -0.43936106E-03, 0.42363195E-03, 0.66191290E-03, 0.47751333E-03,
        -0.13299710E-02,-0.88033150E-03, 0.11160292E-02,-0.43126964E-03,
         0.63993566E-03, 0.59948675E-03, 0.14777125E-02, 0.93438971E-03,
        -0.17072106E-02, 0.14515343E-02,-0.15715772E-03,-0.11223162E-03,
        -0.61348907E-03,-0.15368166E-02,-0.15137788E-02,-0.84203738E-03,
         0.26200694E-03, 0.69851731E-03,-0.76173770E-03,-0.87663840E-03,
         0.72908204E-03, 0.13085323E-02,-0.32804438E-03, 0.11623421E-02,
         0.13357325E-02,-0.10786837E-02, 0.13466612E-02,-0.74433628E-03,
        -0.15669969E-03,-0.16094337E-02,-0.60371764E-03,-0.12560096E-02,
        -0.12058800E-02, 0.15707921E-02, 0.14407682E-02,-0.73790748E-03,
         0.55931509E-03, 0.21267424E-02, 0.11399621E-02, 0.24632338E-03,
         0.18360673E-02,-0.11037651E-03,-0.93218878E-04,-0.16811283E-03,
         0.11242793E-03, 0.53222149E-04, 0.17862691E-03,-0.44376895E-03,
        -0.28031605E-03, 0.11573764E-03, 0.48201797E-04,-0.18594807E-03,
        -0.40614940E-03,-0.23393493E-03, 0.11799402E-03,-0.80009515E-04,
         0.79774065E-04, 0.12811064E-03, 0.11432117E-03,-0.29672586E-03,
         0.23562975E-03, 0.17199773E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x23            
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32*x41    
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[  8]*x12*x21*x31*x41    
        +coeff[  9]    *x24*x31*x41    
        +coeff[ 10]*x13        *x42*x52
        +coeff[ 11]                *x51
        +coeff[ 12]*x11*x21            
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]                *x53
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11    *x31    *x51
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]    *x22*x31*x41    
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x22        *x52
        +coeff[ 23]    *x21    *x41*x52
        +coeff[ 24]        *x31*x41*x52
        +coeff[ 25]        *x31    *x53
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 26]*x11    *x32*x41    
        +coeff[ 27]*x11    *x31*x42    
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]*x12*x21        *x51
        +coeff[ 31]*x13*x21            
        +coeff[ 32]    *x22        *x53
        +coeff[ 33]        *x31*x41*x53
        +coeff[ 34]            *x42*x53
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 35]*x11    *x34        
        +coeff[ 36]*x11    *x31*x43    
        +coeff[ 37]*x11*x21*x31*x41*x51
        +coeff[ 38]*x11*x21    *x42*x51
        +coeff[ 39]*x12*x23            
        +coeff[ 40]*x12*x21    *x42    
        +coeff[ 41]    *x21*x33*x42    
        +coeff[ 42]*x13    *x31    *x51
        +coeff[ 43]*x13        *x41*x51
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 44]    *x24    *x41*x51
        +coeff[ 45]    *x21*x33*x41*x51
        +coeff[ 46]    *x21    *x43*x52
        +coeff[ 47]*x11    *x33*x41*x51
        +coeff[ 48]*x11    *x31*x41*x53
        +coeff[ 49]*x11        *x41*x54
        +coeff[ 50]*x12*x24            
        +coeff[ 51]*x12*x22*x31*x41    
        +coeff[ 52]*x12*x21*x31*x41*x51
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 53]*x12*x21        *x53
        +coeff[ 54]    *x23*x33*x41    
        +coeff[ 55]    *x24    *x43    
        +coeff[ 56]    *x24*x31    *x52
        +coeff[ 57]    *x22*x33    *x52
        +coeff[ 58]    *x21    *x44*x52
        +coeff[ 59]            *x44*x53
        +coeff[ 60]*x11    *x34*x42    
        +coeff[ 61]*x11*x21    *x44*x51
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 62]*x11*x21    *x43*x52
        +coeff[ 63]*x12*x24        *x51
        +coeff[ 64]*x12*x21    *x43*x51
        +coeff[ 65]*x12*x22*x31    *x52
        +coeff[ 66]*x12*x22    *x41*x52
        +coeff[ 67]*x13*x23*x31        
        +coeff[ 68]    *x23*x32*x42*x51
        +coeff[ 69]*x11                
        +coeff[ 70]    *x21        *x51
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 71]        *x31    *x51
        +coeff[ 72]*x11        *x41    
        +coeff[ 73]    *x21*x32        
        +coeff[ 74]        *x33        
        +coeff[ 75]    *x21*x31*x41    
        +coeff[ 76]        *x31*x42    
        +coeff[ 77]        *x32    *x51
        +coeff[ 78]        *x31    *x52
        +coeff[ 79]            *x41*x52
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 80]*x11    *x32        
        +coeff[ 81]*x11*x21    *x41    
        +coeff[ 82]*x11            *x52
        +coeff[ 83]*x12    *x31        
        +coeff[ 84]*x12        *x41    
        +coeff[ 85]*x12            *x51
        +coeff[ 86]*x13                
        +coeff[ 87]    *x22*x32        
        +coeff[ 88]        *x34        
    ;
    v_l_e_q1q2_3_300                              =v_l_e_q1q2_3_300                              
        +coeff[ 89]    *x23    *x41    
        ;

    return v_l_e_q1q2_3_300                              ;
}
float x_e_q1q2_3_250                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1319051E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.13475635E-02, 0.12258495E+00, 0.37451072E-02, 0.70231268E-03,
         0.12064317E-03,-0.55172900E-03,-0.12700032E-02,-0.88710064E-03,
        -0.23654831E-03,-0.43636840E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_250                              =v_x_e_q1q2_3_250                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_250                              ;
}
float t_e_q1q2_3_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.4152654E-05;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.41742419E-05,-0.19580263E-02, 0.78957783E-04, 0.71770730E-04,
         0.22455216E-02,-0.11640050E-02,-0.94341539E-03, 0.14026428E-04,
        -0.31436520E-03,-0.27702298E-03,-0.46772865E-03,-0.88381559E-04,
        -0.89688611E-03,-0.62900491E-03,-0.19700143E-04,-0.25236346E-04,
        -0.17542763E-04, 0.97372875E-04, 0.85690051E-04, 0.50369081E-05,
        -0.29026251E-03,-0.63659187E-03,-0.48023200E-03, 0.26528840E-04,
         0.38855407E-04,-0.14765536E-05,-0.25779178E-06, 0.75591192E-05,
        -0.16747796E-04, 0.40239418E-04,-0.13424409E-04,-0.81928432E-04,
        -0.62078856E-04, 0.20185673E-04, 0.11991995E-04, 0.43479113E-05,
         0.12973339E-04,-0.18447063E-04,-0.89439038E-06, 0.34098809E-05,
        -0.25972672E-05, 0.61275014E-05,-0.41435005E-05,-0.67943442E-05,
        -0.49499013E-05,-0.11601828E-05,-0.33683568E-05, 0.61614328E-05,
         0.44112871E-05,-0.51188958E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_250                              =v_t_e_q1q2_3_250                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_250                              =v_t_e_q1q2_3_250                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x23*x31*x41*x51
        +coeff[ 25]*x11        *x41    
    ;
    v_t_e_q1q2_3_250                              =v_t_e_q1q2_3_250                              
        +coeff[ 26]*x13                
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x22*x32        
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]    *x21*x31*x42*x51
        +coeff[ 34]*x11*x22        *x52
    ;
    v_t_e_q1q2_3_250                              =v_t_e_q1q2_3_250                              
        +coeff[ 35]*x12            *x53
        +coeff[ 36]*x13*x21    *x41*x51
        +coeff[ 37]*x11*x22    *x41*x52
        +coeff[ 38]*x12                
        +coeff[ 39]    *x21    *x41*x51
        +coeff[ 40]    *x21*x33        
        +coeff[ 41]    *x23        *x51
        +coeff[ 42]    *x22*x31    *x51
        +coeff[ 43]*x11    *x32    *x51
    ;
    v_t_e_q1q2_3_250                              =v_t_e_q1q2_3_250                              
        +coeff[ 44]*x11    *x31*x41*x51
        +coeff[ 45]*x12            *x52
        +coeff[ 46]    *x22        *x52
        +coeff[ 47]*x11            *x53
        +coeff[ 48]*x13*x21    *x41    
        +coeff[ 49]*x12*x22    *x41    
        ;

    return v_t_e_q1q2_3_250                              ;
}
float y_e_q1q2_3_250                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.8307840E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.81508659E-03, 0.16801961E+00, 0.11700466E+00,-0.24268029E-02,
        -0.24474163E-02, 0.32591101E-03,-0.11584337E-03,-0.33213102E-04,
        -0.27445331E-03, 0.16618482E-03,-0.97270269E-04, 0.15825860E-03,
         0.98760240E-04, 0.10219482E-03,-0.35217233E-03,-0.52795745E-04,
        -0.12069147E-04,-0.26456555E-04,-0.33015799E-03,-0.12481741E-03,
         0.79947822E-04, 0.69914844E-04,-0.11189209E-05,-0.50915787E-05,
         0.13983062E-04,-0.21748601E-04,-0.24469235E-04, 0.77421246E-04,
        -0.24164349E-03,-0.30104351E-04,-0.21786376E-04,-0.73612086E-04,
        -0.38108225E-04,-0.25821208E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_250                              =v_y_e_q1q2_3_250                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_3_250                              =v_y_e_q1q2_3_250                              
        +coeff[ 17]        *x31*x43*x51
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x24    *x41*x51
        +coeff[ 21]    *x24*x31    *x51
        +coeff[ 22]*x11                
        +coeff[ 23]*x11*x21            
        +coeff[ 24]*x11        *x42    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_250                              =v_y_e_q1q2_3_250                              
        +coeff[ 26]            *x43*x51
        +coeff[ 27]*x11*x21    *x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x42*x52
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]*x11*x21    *x44    
        +coeff[ 32]*x11*x21    *x41*x52
        +coeff[ 33]    *x21*x31*x45    
        ;

    return v_y_e_q1q2_3_250                              ;
}
float p_e_q1q2_3_250                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2566056E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.25104696E-03, 0.33748355E-01, 0.63965939E-01,-0.14357266E-02,
        -0.14326609E-02, 0.37933508E-03, 0.33646607E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_250                              ;
}
float l_e_q1q2_3_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2089848E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19667938E-02, 0.24411222E-04,-0.33451531E-02,-0.84657368E-03,
        -0.34249907E-02,-0.40611066E-02, 0.17928798E-03, 0.57897752E-03,
        -0.14319632E-03, 0.45880288E-03, 0.62486116E-03, 0.15765172E-03,
         0.56111551E-03,-0.16243735E-03,-0.28143539E-04, 0.33299017E-03,
         0.20677860E-03,-0.36987092E-03,-0.15141591E-03,-0.28841349E-03,
        -0.32817395E-03, 0.22852863E-03, 0.90076128E-05,-0.78680186E-03,
        -0.59814443E-03, 0.34747797E-03, 0.33479411E-03, 0.19314035E-03,
        -0.40664224E-03,-0.26612697E-03,-0.84780547E-03,-0.11946532E-02,
        -0.78572950E-03, 0.11506649E-02,-0.47642665E-03, 0.27540277E-02,
        -0.84470514E-04,-0.10592806E-02, 0.44057667E-02, 0.67413501E-04,
        -0.57615276E-03,-0.88456087E-03,-0.30441119E-03, 0.45338590E-03,
         0.11731900E-02,-0.84383972E-03, 0.23754193E-02,-0.58540597E-03,
         0.55564422E-03, 0.21594558E-02, 0.57213265E-03,-0.24141304E-02,
        -0.13436665E-02, 0.37210892E-03,-0.12331620E-02, 0.76757911E-04,
        -0.13065193E-02, 0.29892544E-03,-0.18241651E-03, 0.14256462E-02,
        -0.37214460E-02,-0.21497595E-04, 0.13367612E-02, 0.77879743E-03,
         0.11387514E-02,-0.38005721E-02,-0.12356107E-03,-0.16881419E-03,
         0.24634952E-03,-0.14097631E-04, 0.28984761E-03,-0.70806367E-04,
        -0.12346654E-03, 0.31565785E-03, 0.78002784E-04,-0.95315190E-04,
        -0.11827084E-03, 0.36591484E-03,-0.20004463E-03,-0.12270297E-03,
         0.56120352E-03,-0.48539139E-03, 0.28107720E-03,-0.17418600E-03,
        -0.19675260E-03,-0.10080983E-03, 0.12941638E-03, 0.47908491E-03,
        -0.16231685E-03,-0.60983544E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11        *x41    
        +coeff[  7]            *x42*x51
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[  8]*x11*x21    *x41    
        +coeff[  9]        *x31*x42*x51
        +coeff[ 10]*x11*x21        *x54
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]            *x43    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 17]*x11    *x32        
        +coeff[ 18]*x12            *x51
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x22        *x52
        +coeff[ 21]            *x42*x52
        +coeff[ 22]*x11    *x32*x41    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]*x11    *x32    *x51
        +coeff[ 25]*x11            *x53
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 26]*x12*x22            
        +coeff[ 27]*x12*x21        *x51
        +coeff[ 28]*x12            *x52
        +coeff[ 29]    *x21    *x43*x51
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]*x11    *x33*x41    
        +coeff[ 32]*x11    *x33    *x51
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]*x11    *x32*x41*x51
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 35]*x11*x21    *x42*x51
        +coeff[ 36]*x11*x21    *x41*x52
        +coeff[ 37]*x12        *x43    
        +coeff[ 38]    *x21*x32*x41*x52
        +coeff[ 39]        *x33*x41*x52
        +coeff[ 40]        *x32    *x54
        +coeff[ 41]*x11    *x31*x44    
        +coeff[ 42]*x11*x24        *x51
        +coeff[ 43]*x12    *x34        
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 44]*x13    *x32    *x51
        +coeff[ 45]    *x22*x33    *x52
        +coeff[ 46]        *x33*x42*x52
        +coeff[ 47]    *x21*x33    *x53
        +coeff[ 48]    *x22*x31*x41*x53
        +coeff[ 49]        *x32*x41*x54
        +coeff[ 50]*x11*x24*x31    *x51
        +coeff[ 51]*x11*x23*x32    *x51
        +coeff[ 52]*x11*x21*x32*x41*x52
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 53]*x11*x23        *x53
        +coeff[ 54]*x12    *x33*x42    
        +coeff[ 55]*x12*x22    *x41*x52
        +coeff[ 56]*x12    *x32*x41*x52
        +coeff[ 57]*x12    *x31    *x54
        +coeff[ 58]    *x23*x34*x41    
        +coeff[ 59]*x13*x21*x32    *x51
        +coeff[ 60]*x13*x21    *x42*x51
        +coeff[ 61]*x13    *x31*x41*x52
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 62]*x13*x21        *x53
        +coeff[ 63]        *x34*x41*x53
        +coeff[ 64]    *x22*x31*x42*x53
        +coeff[ 65]    *x21*x32*x41*x54
        +coeff[ 66]    *x21            
        +coeff[ 67]            *x41    
        +coeff[ 68]*x11                
        +coeff[ 69]    *x21    *x41    
        +coeff[ 70]                *x52
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 71]*x11*x21            
        +coeff[ 72]*x11            *x51
        +coeff[ 73]    *x23            
        +coeff[ 74]    *x22*x31        
        +coeff[ 75]        *x33        
        +coeff[ 76]    *x22    *x41    
        +coeff[ 77]    *x21        *x52
        +coeff[ 78]            *x41*x52
        +coeff[ 79]                *x53
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 80]*x11    *x31*x41    
        +coeff[ 81]*x11*x21        *x51
        +coeff[ 82]*x11    *x31    *x51
        +coeff[ 83]*x11        *x41*x51
        +coeff[ 84]*x11            *x52
        +coeff[ 85]*x12*x21            
        +coeff[ 86]*x12    *x31        
        +coeff[ 87]*x12        *x41    
        +coeff[ 88]*x13                
    ;
    v_l_e_q1q2_3_250                              =v_l_e_q1q2_3_250                              
        +coeff[ 89]    *x21*x32*x41    
        ;

    return v_l_e_q1q2_3_250                              ;
}
float x_e_q1q2_3_200                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1383656E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.17236474E-03, 0.12304603E+00, 0.37260430E-02, 0.71795192E-03,
         0.12148306E-03,-0.55448426E-03,-0.12664080E-02,-0.88268227E-03,
        -0.24701832E-03,-0.43512639E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_200                              =v_x_e_q1q2_3_200                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_200                              ;
}
float t_e_q1q2_3_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1642923E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15633908E-04,-0.19441546E-02, 0.37107366E-03, 0.22501620E-02,
        -0.11549874E-02,-0.94932067E-03, 0.99167823E-06,-0.30859630E-03,
         0.72066192E-04,-0.28088418E-03,-0.46173908E-03,-0.85313863E-04,
        -0.89904532E-03,-0.62251370E-03,-0.35700108E-04, 0.10166880E-03,
        -0.37330450E-04,-0.25832929E-04, 0.43447257E-04, 0.57000780E-04,
         0.80617037E-05,-0.29970988E-03,-0.66007045E-03,-0.50476252E-03,
        -0.78333869E-05,-0.19819072E-04, 0.20433539E-04, 0.46243285E-05,
         0.73512438E-05,-0.89811056E-05,-0.89915038E-05,-0.51152037E-05,
        -0.16799055E-04,-0.24171024E-04,-0.18436563E-05, 0.23642297E-05,
         0.46891823E-05,-0.74340819E-05,-0.11659410E-04,-0.60886023E-05,
        -0.74871737E-05,-0.39132092E-05,-0.31036091E-05, 0.88108090E-05,
         0.36548538E-05, 0.51517022E-05, 0.16821512E-04,-0.28020302E-05,
        -0.13107860E-04, 0.15807864E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_200                              =v_t_e_q1q2_3_200                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_200                              =v_t_e_q1q2_3_200                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x32    *x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]*x13                
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_3_200                              =v_t_e_q1q2_3_200                              
        +coeff[ 26]*x11*x21    *x41    
        +coeff[ 27]                *x53
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x12*x21        *x51
        +coeff[ 31]*x11    *x31    *x52
        +coeff[ 32]*x11*x21    *x41*x52
        +coeff[ 33]    *x22    *x42*x52
        +coeff[ 34]                *x51
    ;
    v_t_e_q1q2_3_200                              =v_t_e_q1q2_3_200                              
        +coeff[ 35]                *x52
        +coeff[ 36]*x12*x22            
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x21*x31*x41    
        +coeff[ 39]    *x22*x31*x41    
        +coeff[ 40]*x11*x21    *x42    
        +coeff[ 41]*x12    *x31    *x51
        +coeff[ 42]    *x22    *x41*x51
        +coeff[ 43]    *x21*x31    *x52
    ;
    v_t_e_q1q2_3_200                              =v_t_e_q1q2_3_200                              
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]*x13*x22            
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]*x13*x21    *x41    
        +coeff[ 49]*x12*x21*x31*x41    
        ;

    return v_t_e_q1q2_3_200                              ;
}
float y_e_q1q2_3_200                              (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.1170307E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.13194550E-03, 0.16776149E+00, 0.11668459E+00,-0.24333624E-02,
        -0.24365508E-02, 0.32589151E-03,-0.10851657E-03,-0.27669215E-04,
        -0.25316040E-03, 0.17311277E-03,-0.12154836E-03, 0.14617923E-03,
        -0.27508520E-04, 0.10942372E-03, 0.10353929E-03,-0.35901679E-03,
        -0.42115367E-04, 0.64923675E-04, 0.55325738E-04,-0.29563133E-03,
        -0.11321394E-03,-0.21895377E-06, 0.99495201E-05, 0.32828473E-04,
        -0.18523967E-03, 0.23944245E-04, 0.20812860E-04,-0.22369280E-04,
        -0.53847474E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_200                              =v_y_e_q1q2_3_200                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_200                              =v_y_e_q1q2_3_200                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x12        *x42    
        +coeff[ 23]        *x32*x43    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]*x11    *x31*x43    
    ;
    v_y_e_q1q2_3_200                              =v_y_e_q1q2_3_200                              
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]*x11    *x31*x42*x51
        +coeff[ 28]*x11*x23*x31        
        ;

    return v_y_e_q1q2_3_200                              ;
}
float p_e_q1q2_3_200                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.4521597E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.14892915E-05, 0.33557110E-01, 0.63700639E-01,-0.14265700E-02,
        -0.14231591E-02, 0.39506148E-03, 0.21005732E-03, 0.22878575E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]            *x43    
        +coeff[  7]        *x34*x41    
        ;

    return v_p_e_q1q2_3_200                              ;
}
float l_e_q1q2_3_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2153621E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20774789E-02, 0.45042263E-04,-0.34375268E-02,-0.65485691E-03,
        -0.29753728E-02,-0.40789419E-02,-0.70216978E-03,-0.13914650E-03,
        -0.16184549E-02, 0.89405413E-03, 0.58220181E-03,-0.92522641E-04,
        -0.70268311E-05,-0.42832668E-04,-0.10865204E-03, 0.54122315E-03,
         0.37074796E-03,-0.13235966E-02, 0.80015219E-03,-0.60185097E-03,
         0.22891055E-03,-0.22331225E-03, 0.10600094E-02, 0.17694895E-02,
         0.40715697E-03,-0.11748245E-02, 0.42077317E-03,-0.79188088E-03,
        -0.12655061E-02,-0.12156492E-02,-0.38410627E-03, 0.81298128E-03,
         0.32039217E-03,-0.42982405E-03,-0.83169632E-03,-0.11460539E-02,
        -0.40680156E-03, 0.17525253E-02, 0.20631864E-03, 0.31641773E-02,
         0.16347819E-03,-0.22750949E-02,-0.88267162E-03, 0.16990987E-02,
        -0.12222962E-02,-0.75998442E-03,-0.11216807E-02,-0.68918272E-03,
         0.11447771E-02,-0.11605520E-02, 0.21587496E-02, 0.15573570E-02,
        -0.13562798E-02,-0.11365829E-02,-0.86194027E-03, 0.16123841E-02,
        -0.19069748E-02, 0.18155141E-02, 0.15650237E-02,-0.10548019E-02,
         0.60021627E-03, 0.24676751E-02,-0.40340077E-04,-0.42894550E-03,
         0.69770802E-04,-0.13501014E-02, 0.12426791E-03, 0.69361457E-04,
        -0.11974078E-03, 0.28070499E-03, 0.18966044E-03, 0.11679933E-03,
        -0.25605800E-03, 0.37721847E-03, 0.18316023E-03,-0.47138278E-03,
        -0.28684750E-03,-0.62122964E-03,-0.20136531E-03, 0.47781007E-03,
        -0.33847091E-05, 0.20053373E-03, 0.19215124E-03,-0.10758342E-03,
         0.42024427E-03,-0.22664733E-03, 0.11059159E-03,-0.15079686E-03,
         0.32778556E-03, 0.99841319E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21*x31    *x52
        +coeff[  7]*x12*x22        *x51
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[  8]        *x32*x42*x52
        +coeff[  9]*x11*x22*x32*x41    
        +coeff[ 10]*x13    *x33        
        +coeff[ 11]        *x31        
        +coeff[ 12]            *x41    
        +coeff[ 13]*x12                
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]*x12    *x31        
        +coeff[ 16]    *x23        *x51
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 17]        *x31*x41*x52
        +coeff[ 18]    *x21        *x53
        +coeff[ 19]*x11    *x31*x42    
        +coeff[ 20]*x11    *x31*x41*x51
        +coeff[ 21]*x11        *x41*x52
        +coeff[ 22]    *x22*x31    *x52
        +coeff[ 23]    *x21*x32    *x52
        +coeff[ 24]*x11    *x32*x41*x51
        +coeff[ 25]*x11*x21    *x42*x51
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 26]*x11        *x43*x51
        +coeff[ 27]*x11        *x42*x52
        +coeff[ 28]*x12*x22*x31        
        +coeff[ 29]*x12        *x42*x51
        +coeff[ 30]*x12    *x31    *x52
        +coeff[ 31]*x13        *x41*x51
        +coeff[ 32]    *x23*x31    *x52
        +coeff[ 33]    *x23        *x53
        +coeff[ 34]*x11*x21*x34        
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 35]    *x23*x32    *x52
        +coeff[ 36]    *x22*x33    *x52
        +coeff[ 37]    *x23*x31*x41*x52
        +coeff[ 38]*x11*x24*x31*x41    
        +coeff[ 39]*x11*x22*x32*x42    
        +coeff[ 40]*x11    *x34*x42    
        +coeff[ 41]*x11    *x32*x42*x52
        +coeff[ 42]*x11        *x43*x53
        +coeff[ 43]*x12*x24*x31        
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 44]*x12*x22*x33        
        +coeff[ 45]*x12*x24        *x51
        +coeff[ 46]*x12*x21*x33    *x51
        +coeff[ 47]*x12    *x33*x41*x51
        +coeff[ 48]*x12*x22    *x41*x52
        +coeff[ 49]*x13*x22    *x42    
        +coeff[ 50]    *x22*x34*x41*x51
        +coeff[ 51]*x13*x21    *x42*x51
        +coeff[ 52]*x13        *x43*x51
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 53]    *x23*x33    *x52
        +coeff[ 54]*x13    *x31*x41*x52
        +coeff[ 55]*x13        *x42*x52
        +coeff[ 56]    *x22*x31*x43*x52
        +coeff[ 57]    *x22*x33    *x53
        +coeff[ 58]    *x24    *x41*x53
        +coeff[ 59]    *x22*x32    *x54
        +coeff[ 60]        *x32*x42*x54
        +coeff[ 61]        *x31*x43*x54
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 62]    *x21            
        +coeff[ 63]    *x21        *x51
        +coeff[ 64]    *x23            
        +coeff[ 65]    *x21*x32        
        +coeff[ 66]    *x21    *x42    
        +coeff[ 67]    *x21*x31    *x51
        +coeff[ 68]    *x21    *x41*x51
        +coeff[ 69]            *x42*x51
        +coeff[ 70]*x11    *x31*x41    
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 71]*x12*x21            
        +coeff[ 72]*x12        *x41    
        +coeff[ 73]*x12            *x51
        +coeff[ 74]        *x33*x41    
        +coeff[ 75]        *x31*x43    
        +coeff[ 76]        *x33    *x51
        +coeff[ 77]    *x22    *x41*x51
        +coeff[ 78]    *x21    *x42*x51
        +coeff[ 79]        *x31*x42*x51
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 80]            *x43*x51
        +coeff[ 81]    *x22        *x52
        +coeff[ 82]            *x41*x53
        +coeff[ 83]*x11*x22*x31        
        +coeff[ 84]*x11*x21*x32        
        +coeff[ 85]*x11    *x32*x41    
        +coeff[ 86]*x11        *x43    
        +coeff[ 87]*x12        *x42    
        +coeff[ 88]    *x23*x32        
    ;
    v_l_e_q1q2_3_200                              =v_l_e_q1q2_3_200                              
        +coeff[ 89]    *x21*x34        
        ;

    return v_l_e_q1q2_3_200                              ;
}
float x_e_q1q2_3_175                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.7057902E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.72940724E-03, 0.12336969E+00, 0.37161601E-02, 0.72941807E-03,
         0.12224534E-03,-0.54730749E-03,-0.12452292E-02,-0.86890470E-03,
        -0.23916693E-03,-0.42947588E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_175                              =v_x_e_q1q2_3_175                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_175                              ;
}
float t_e_q1q2_3_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1414317E-04;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.13814722E-04,-0.19418371E-02, 0.58072846E-03, 0.22291045E-02,
        -0.11684404E-02,-0.96157996E-03, 0.37270847E-05,-0.15116809E-05,
        -0.28694607E-03,-0.28737864E-03,-0.47129739E-03,-0.87222616E-04,
         0.71137751E-04,-0.86766545E-03,-0.59602008E-03,-0.29644780E-04,
        -0.38952894E-04,-0.26494752E-04, 0.61956991E-04, 0.45494355E-04,
        -0.59034652E-03,-0.46149615E-03,-0.22018377E-04,-0.18391263E-04,
         0.10161043E-04, 0.35202133E-05,-0.12709040E-04,-0.44113453E-05,
         0.74620248E-05,-0.11097291E-04, 0.34688484E-04,-0.16744414E-04,
        -0.59538761E-05, 0.15037898E-04, 0.14780596E-04,-0.27294501E-03,
         0.30247815E-04, 0.34362241E-04, 0.43451659E-04, 0.47852642E-04,
         0.43127118E-04, 0.49366303E-04, 0.15753488E-05, 0.20640125E-05,
         0.28843094E-05, 0.44347917E-05,-0.22146976E-05, 0.19451425E-05,
        -0.36515710E-05, 0.25863396E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x13            *x51
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1q2_3_175                              =v_t_e_q1q2_3_175                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]*x11            *x51
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_3_175                              =v_t_e_q1q2_3_175                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]*x11*x22        *x52
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]    *x21*x31        
    ;
    v_t_e_q1q2_3_175                              =v_t_e_q1q2_3_175                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]*x11    *x31    *x51
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]*x12*x21        *x51
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]*x11*x21    *x41*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21        *x53
        +coeff[ 34]*x13*x22            
    ;
    v_t_e_q1q2_3_175                              =v_t_e_q1q2_3_175                              
        +coeff[ 35]    *x21*x33*x41    
        +coeff[ 36]*x12*x23        *x51
        +coeff[ 37]*x11*x23*x31    *x51
        +coeff[ 38]*x11*x23    *x41*x51
        +coeff[ 39]*x11*x21*x32*x41*x51
        +coeff[ 40]*x12*x21    *x42*x51
        +coeff[ 41]    *x21*x31*x41*x53
        +coeff[ 42]*x11    *x31        
        +coeff[ 43]    *x21    *x41    
    ;
    v_t_e_q1q2_3_175                              =v_t_e_q1q2_3_175                              
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]    *x21*x31    *x51
        +coeff[ 47]        *x31*x41*x51
        +coeff[ 48]            *x42*x51
        +coeff[ 49]            *x41*x52
        ;

    return v_t_e_q1q2_3_175                              ;
}
float y_e_q1q2_3_175                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.6050892E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.50291838E-03, 0.16750051E+00, 0.11646761E+00,-0.24000311E-02,
        -0.24304253E-02, 0.33749349E-03,-0.10692716E-03,-0.26532212E-04,
        -0.27179311E-03, 0.18563482E-03,-0.34758160E-03,-0.93602881E-04,
         0.11429087E-03, 0.90386311E-04, 0.10665386E-03,-0.55122815E-04,
        -0.46065976E-04, 0.61878360E-04,-0.21636239E-03, 0.50765146E-04,
         0.11431776E-04,-0.24180621E-04,-0.11233319E-04,-0.98127002E-05,
         0.18581488E-04,-0.33831195E-03, 0.55609577E-04,-0.13034639E-03,
        -0.42488940E-04, 0.11798843E-04, 0.32185755E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_175                              =v_y_e_q1q2_3_175                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x22    *x41    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x23*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_175                              =v_y_e_q1q2_3_175                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]        *x33        
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]    *x21    *x43    
        +coeff[ 23]*x11        *x41*x51
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_3_175                              =v_y_e_q1q2_3_175                              
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]        *x31*x42*x52
        +coeff[ 29]*x13        *x42    
        +coeff[ 30]*x11*x21    *x43*x51
        ;

    return v_y_e_q1q2_3_175                              ;
}
float p_e_q1q2_3_175                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1779319E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.13929517E-03, 0.33427410E-01, 0.63643657E-01,-0.14187088E-02,
        -0.14117867E-02, 0.37689830E-03, 0.33167520E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_175                              ;
}
float l_e_q1q2_3_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2095418E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20082616E-02,-0.33890647E-02,-0.13367299E-03,-0.34889134E-02,
        -0.43679019E-02,-0.71346492E-03,-0.25650277E-02,-0.43400982E-02,
        -0.35552104E-03, 0.12655940E-03, 0.70068585E-04, 0.18842897E-03,
         0.14750475E-03, 0.11208201E-03,-0.33474932E-03, 0.37750654E-03,
        -0.13260255E-03, 0.98540550E-04,-0.46548870E-04, 0.10218704E-02,
        -0.13504600E-02,-0.24880937E-03, 0.26356875E-04,-0.56696392E-03,
         0.14819980E-03, 0.40410328E-03,-0.18042796E-03, 0.24549107E-03,
        -0.28638046E-02,-0.15366665E-03,-0.71122299E-03,-0.27168775E-03,
        -0.16096389E-03,-0.75801152E-04,-0.10477075E-03,-0.65611042E-04,
        -0.24943002E-02,-0.45661465E-04, 0.15769696E-02,-0.13090360E-02,
         0.49326057E-03, 0.10953024E-02,-0.38574514E-03, 0.19741214E-02,
         0.86702715E-03, 0.97494549E-03, 0.14097102E-02,-0.95129217E-03,
        -0.14073252E-02, 0.81177393E-03, 0.11495339E-02, 0.25565538E-03,
         0.18175049E-02,-0.66289946E-03,-0.80834766E-03, 0.43907083E-03,
         0.11659844E-02, 0.17276234E-02,-0.71806519E-03,-0.13835907E-02,
         0.23795301E-02, 0.48159636E-03, 0.17501687E-02,-0.10050031E-02,
         0.14941713E-02, 0.14359005E-02,-0.76060661E-03,-0.22639723E-02,
         0.20702877E-02, 0.12861318E-02, 0.12245576E-02,-0.32251447E-02,
         0.27476428E-02,-0.62674661E-02,-0.16111202E-02,-0.12613737E-02,
         0.79671678E-03,-0.65496372E-03, 0.70542301E-03,-0.11139543E-02,
         0.13103718E-02, 0.50024366E-04, 0.27842243E-03, 0.51569994E-04,
         0.13496916E-03, 0.10290950E-03,-0.14560077E-03, 0.55326920E-04,
         0.10857637E-03,-0.80116159E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x34        
        +coeff[  6]*x11*x21*x31    *x51
        +coeff[  7]*x11*x22*x31*x42*x51
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[  8]*x11*x22*x31    *x53
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41    
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31    *x52
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 17]*x12            *x51
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x41*x52
        +coeff[ 21]*x11*x22*x31        
        +coeff[ 22]*x11        *x42*x51
        +coeff[ 23]*x11*x21        *x52
        +coeff[ 24]*x12*x21*x31        
        +coeff[ 25]*x13*x21            
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 26]*x13        *x41    
        +coeff[ 27]    *x22*x32    *x51
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]*x11*x22*x31    *x51
        +coeff[ 30]*x11*x21    *x42*x51
        +coeff[ 31]*x11        *x43*x51
        +coeff[ 32]*x11*x21        *x53
        +coeff[ 33]*x12    *x32*x41    
        +coeff[ 34]*x12*x22        *x51
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 35]*x13    *x31*x41    
        +coeff[ 36]    *x23*x32*x41    
        +coeff[ 37]    *x22*x33*x41    
        +coeff[ 38]    *x23*x32    *x51
        +coeff[ 39]    *x21*x34    *x51
        +coeff[ 40]    *x23*x31    *x52
        +coeff[ 41]        *x33*x41*x52
        +coeff[ 42]    *x21*x32    *x53
        +coeff[ 43]    *x21    *x41*x54
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 44]            *x42*x54
        +coeff[ 45]*x11*x23*x31    *x51
        +coeff[ 46]*x11*x21*x33    *x51
        +coeff[ 47]*x11*x22    *x42*x51
        +coeff[ 48]*x11*x23        *x52
        +coeff[ 49]*x11        *x42*x53
        +coeff[ 50]*x11*x21        *x54
        +coeff[ 51]*x11        *x41*x54
        +coeff[ 52]*x12*x21*x32    *x51
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 53]*x12    *x31*x41*x52
        +coeff[ 54]*x12*x21        *x53
        +coeff[ 55]    *x23*x34        
        +coeff[ 56]*x13*x21*x31    *x51
        +coeff[ 57]    *x21*x34    *x52
        +coeff[ 58]    *x21*x31*x43*x52
        +coeff[ 59]*x11*x24    *x42    
        +coeff[ 60]*x11*x24*x31    *x51
        +coeff[ 61]*x11    *x31*x43*x52
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 62]*x12*x22    *x42*x51
        +coeff[ 63]*x12*x23        *x52
        +coeff[ 64]*x12*x21*x32    *x52
        +coeff[ 65]*x12    *x32*x41*x52
        +coeff[ 66]*x12        *x42*x53
        +coeff[ 67]*x13*x22*x31*x41    
        +coeff[ 68]    *x23*x32*x43    
        +coeff[ 69]*x13*x22    *x41*x51
        +coeff[ 70]    *x21*x34*x42*x51
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 71]    *x23*x33    *x52
        +coeff[ 72]    *x24*x31*x41*x52
        +coeff[ 73]    *x22*x33*x41*x52
        +coeff[ 74]        *x34*x42*x52
        +coeff[ 75]    *x22    *x44*x52
        +coeff[ 76]*x13*x21        *x53
        +coeff[ 77]*x13        *x41*x53
        +coeff[ 78]        *x33*x42*x53
        +coeff[ 79]    *x22*x32    *x54
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 80]    *x21*x33    *x54
        +coeff[ 81]*x11                
        +coeff[ 82]    *x21    *x41    
        +coeff[ 83]            *x41*x51
        +coeff[ 84]    *x21*x31    *x51
        +coeff[ 85]    *x21    *x41*x51
        +coeff[ 86]            *x41*x52
        +coeff[ 87]                *x53
        +coeff[ 88]*x11*x21*x31        
    ;
    v_l_e_q1q2_3_175                              =v_l_e_q1q2_3_175                              
        +coeff[ 89]*x11    *x32        
        ;

    return v_l_e_q1q2_3_175                              ;
}
float x_e_q1q2_3_150                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3075139E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.29659190E-03, 0.12381934E+00, 0.37041518E-02, 0.74346684E-03,
         0.12202436E-03,-0.54764573E-03,-0.12376833E-02,-0.86708844E-03,
        -0.23792937E-03,-0.42014549E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_150                              =v_x_e_q1q2_3_150                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_150                              ;
}
float t_e_q1q2_3_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.7498640E-05;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.80830087E-05,-0.19332551E-02, 0.85614680E-03, 0.22287392E-02,
        -0.11508429E-02,-0.94752712E-03, 0.10384848E-04,-0.28154044E-03,
         0.69516886E-04,-0.28552365E-03,-0.47041962E-03,-0.80346785E-04,
        -0.86210889E-03,-0.61112782E-03,-0.24450917E-04, 0.11036919E-03,
         0.74830736E-04,-0.27749013E-04,-0.99661665E-05, 0.37671743E-04,
         0.72231660E-05,-0.28922810E-03,-0.62532153E-03,-0.48933865E-03,
        -0.19758138E-04, 0.66288048E-05,-0.12195715E-04,-0.50646275E-04,
        -0.17813321E-04,-0.29225583E-04, 0.13301275E-04,-0.25099691E-04,
        -0.22941535E-04,-0.26163058E-04, 0.78111543E-05, 0.28474849E-05,
         0.18064579E-05, 0.14006320E-05,-0.33972626E-05,-0.38346579E-05,
        -0.17203035E-05, 0.61130445E-05,-0.48246948E-05,-0.51889469E-05,
         0.45754086E-05,-0.67418450E-05, 0.75242515E-05, 0.41945232E-05,
        -0.40844834E-05,-0.52550049E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_150                              =v_t_e_q1q2_3_150                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q1q2_3_150                              =v_t_e_q1q2_3_150                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x21    *x41*x51
    ;
    v_t_e_q1q2_3_150                              =v_t_e_q1q2_3_150                              
        +coeff[ 26]*x11*x21    *x42    
        +coeff[ 27]*x11*x22*x31*x41    
        +coeff[ 28]*x13        *x42    
        +coeff[ 29]*x11*x22    *x42    
        +coeff[ 30]    *x23    *x43    
        +coeff[ 31]*x12*x22*x31    *x51
        +coeff[ 32]*x12*x22    *x41*x51
        +coeff[ 33]*x11*x22*x31*x41*x51
        +coeff[ 34]    *x23*x31    *x52
    ;
    v_t_e_q1q2_3_150                              =v_t_e_q1q2_3_150                              
        +coeff[ 35]*x11*x21            
        +coeff[ 36]    *x21*x31        
        +coeff[ 37]        *x32        
        +coeff[ 38]            *x41*x51
        +coeff[ 39]*x12*x21            
        +coeff[ 40]*x12        *x41    
        +coeff[ 41]*x13*x21            
        +coeff[ 42]*x11*x23            
        +coeff[ 43]    *x22    *x42    
    ;
    v_t_e_q1q2_3_150                              =v_t_e_q1q2_3_150                              
        +coeff[ 44]*x13            *x51
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]*x12        *x41*x51
        +coeff[ 47]*x11*x21    *x41*x51
        +coeff[ 48]*x11*x21        *x52
        +coeff[ 49]        *x31*x41*x52
        ;

    return v_t_e_q1q2_3_150                              ;
}
float y_e_q1q2_3_150                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.4356913E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.12254063E-03, 0.16722608E+00, 0.11619679E+00,-0.24022695E-02,
        -0.24118475E-02, 0.33480630E-03,-0.10770543E-03,-0.67557448E-04,
        -0.27003337E-03, 0.18444854E-03,-0.34515429E-03,-0.72762123E-04,
         0.15766648E-03, 0.94689982E-04, 0.10193000E-03,-0.54206994E-04,
        -0.23901992E-04,-0.33956618E-03,-0.11472946E-03, 0.77210723E-04,
         0.61231738E-04,-0.57042729E-04, 0.96270887E-05, 0.25576717E-04,
         0.25708869E-04,-0.13975366E-04,-0.26298367E-03,-0.31394397E-04,
         0.27955857E-04,-0.97959537E-05, 0.42237392E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1q2_3_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_150                              =v_y_e_q1q2_3_150                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x22    *x41    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_3_150                              =v_y_e_q1q2_3_150                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]    *x22*x33        
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]    *x24*x31    *x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]*x11        *x43    
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]*x11        *x42*x51
    ;
    v_y_e_q1q2_3_150                              =v_y_e_q1q2_3_150                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x22    *x44    
        +coeff[ 28]        *x32*x44    
        +coeff[ 29]            *x42*x53
        +coeff[ 30]*x13*x21    *x41    
        ;

    return v_y_e_q1q2_3_150                              ;
}
float p_e_q1q2_3_150                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.2516441E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.54486271E-04, 0.33253588E-01, 0.63376747E-01,-0.14144366E-02,
        -0.14101013E-02, 0.38843439E-03, 0.20817108E-03, 0.22103752E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]            *x43    
        +coeff[  7]        *x34*x41    
        ;

    return v_p_e_q1q2_3_150                              ;
}
float l_e_q1q2_3_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2066013E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21556269E-02, 0.18894911E-03,-0.35840471E-02,-0.12907087E-02,
        -0.36425458E-02,-0.45704120E-02,-0.20750098E-03, 0.11698762E-03,
         0.83939849E-04,-0.36607514E-03,-0.31375987E-03,-0.60661201E-03,
         0.33162424E-03, 0.36530482E-03, 0.33351989E-03,-0.33230780E-03,
         0.30482752E-03, 0.66338963E-03,-0.85984042E-03,-0.67979831E-03,
        -0.64656895E-03, 0.10818591E-02, 0.46299619E-03,-0.23988888E-02,
        -0.12166461E-03,-0.90399740E-03, 0.60577068E-03,-0.34872652E-03,
        -0.51134825E-03, 0.38857016E-03,-0.43685903E-03,-0.49284444E-03,
        -0.92145032E-03,-0.24454021E-02, 0.21408778E-02, 0.19276257E-02,
         0.81158255E-03, 0.21960937E-03,-0.54763182E-03, 0.50827890E-03,
         0.18293388E-02, 0.48733607E-04,-0.12686361E-02, 0.11597734E-02,
        -0.63365465E-03, 0.19016651E-03, 0.72645553E-03,-0.64592797E-03,
        -0.12833576E-02, 0.14792715E-02, 0.75957476E-03, 0.25359120E-02,
         0.95995114E-03, 0.19340258E-02, 0.19380059E-02,-0.96995215E-03,
         0.16953500E-02, 0.67615043E-03,-0.18551205E-02,-0.42940097E-03,
         0.40294757E-03, 0.16459541E-02, 0.10127953E-02,-0.75677520E-03,
        -0.39222888E-02,-0.24974823E-02,-0.29163767E-03, 0.13043506E-02,
        -0.27293416E-02,-0.21918174E-02, 0.29407041E-02, 0.23083538E-02,
         0.84764510E-03, 0.95633448E-04,-0.34445111E-04, 0.61185092E-04,
        -0.11439135E-03, 0.44613433E-04, 0.16141876E-03,-0.43097604E-03,
        -0.10683994E-03,-0.44216189E-03, 0.19884871E-03,-0.13965924E-03,
        -0.24291658E-03, 0.89523666E-04, 0.14554984E-03, 0.32319652E-03,
         0.37216872E-03, 0.18141267E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x41*x51
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[  8]                *x52
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11    *x31*x41    
        +coeff[ 11]*x11*x21        *x51
        +coeff[ 12]*x11        *x41*x51
        +coeff[ 13]*x13                
        +coeff[ 14]            *x44    
        +coeff[ 15]        *x32*x41*x51
        +coeff[ 16]*x13    *x31        
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 17]    *x22    *x43    
        +coeff[ 18]        *x32*x43    
        +coeff[ 19]    *x23*x31    *x51
        +coeff[ 20]    *x22*x32    *x51
        +coeff[ 21]    *x21*x33    *x51
        +coeff[ 22]    *x22*x31    *x52
        +coeff[ 23]    *x21*x31*x41*x52
        +coeff[ 24]    *x21    *x42*x52
        +coeff[ 25]        *x31*x42*x52
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 26]        *x32    *x53
        +coeff[ 27]    *x21    *x41*x53
        +coeff[ 28]    *x21        *x54
        +coeff[ 29]*x11*x22    *x42    
        +coeff[ 30]*x11        *x44    
        +coeff[ 31]*x11*x22    *x41*x51
        +coeff[ 32]*x11*x21*x31*x41*x51
        +coeff[ 33]*x11*x22        *x52
        +coeff[ 34]*x12*x21*x31*x41    
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 35]*x12*x21    *x42    
        +coeff[ 36]*x12*x21*x31    *x51
        +coeff[ 37]*x12        *x42*x51
        +coeff[ 38]*x12        *x41*x52
        +coeff[ 39]*x13*x21        *x51
        +coeff[ 40]    *x24    *x41*x51
        +coeff[ 41]*x13            *x52
        +coeff[ 42]*x11*x21*x31*x41*x52
        +coeff[ 43]*x12*x21*x31*x41*x51
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 44]*x12*x21    *x41*x52
        +coeff[ 45]*x12*x21        *x53
        +coeff[ 46]*x13*x21*x31*x41    
        +coeff[ 47]    *x23*x31*x43    
        +coeff[ 48]    *x23    *x44    
        +coeff[ 49]    *x21    *x44*x52
        +coeff[ 50]    *x23        *x54
        +coeff[ 51]    *x21*x31*x41*x54
        +coeff[ 52]*x11*x24        *x52
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 53]*x11*x22*x32    *x52
        +coeff[ 54]*x11    *x33*x41*x52
        +coeff[ 55]*x11    *x31*x43*x52
        +coeff[ 56]*x11*x22        *x54
        +coeff[ 57]*x12    *x34*x41    
        +coeff[ 58]*x12*x21*x33    *x51
        +coeff[ 59]*x12    *x34    *x51
        +coeff[ 60]*x12    *x33*x41*x51
        +coeff[ 61]*x12*x22    *x42*x51
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 62]*x12*x21    *x43*x51
        +coeff[ 63]*x12*x23        *x52
        +coeff[ 64]*x12*x21*x31*x41*x52
        +coeff[ 65]*x12*x21    *x42*x52
        +coeff[ 66]*x13*x22*x32        
        +coeff[ 67]*x13*x22*x31*x41    
        +coeff[ 68]    *x24*x32*x41*x51
        +coeff[ 69]    *x24    *x43*x51
        +coeff[ 70]    *x22*x32*x43*x51
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 71]    *x22*x33*x41*x52
        +coeff[ 72]    *x22*x32    *x54
        +coeff[ 73]            *x41    
        +coeff[ 74]*x11                
        +coeff[ 75]    *x21        *x51
        +coeff[ 76]*x11    *x31        
        +coeff[ 77]    *x23            
        +coeff[ 78]    *x21*x32        
        +coeff[ 79]    *x21    *x42    
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 80]            *x43    
        +coeff[ 81]    *x21*x31    *x51
        +coeff[ 82]        *x31*x41*x51
        +coeff[ 83]                *x53
        +coeff[ 84]*x11            *x52
        +coeff[ 85]*x12    *x31        
        +coeff[ 86]    *x23*x31        
        +coeff[ 87]    *x22*x32        
        +coeff[ 88]        *x34        
    ;
    v_l_e_q1q2_3_150                              =v_l_e_q1q2_3_150                              
        +coeff[ 89]    *x21*x32*x41    
        ;

    return v_l_e_q1q2_3_150                              ;
}
float x_e_q1q2_3_125                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3071721E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.31791528E-03, 0.12443189E+00, 0.36794066E-02, 0.76393830E-03,
         0.12050079E-03,-0.53922919E-03,-0.12179366E-02,-0.85440191E-03,
        -0.24387937E-03,-0.41176853E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_125                              =v_x_e_q1q2_3_125                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_125                              ;
}
float t_e_q1q2_3_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.7015125E-05;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.72324515E-05,-0.19227694E-02, 0.12398753E-02, 0.22135440E-02,
        -0.11417815E-02,-0.94921351E-03,-0.26920072E-05,-0.28059538E-03,
         0.70966627E-04,-0.28436381E-03,-0.45888257E-03,-0.76273900E-04,
        -0.25671423E-04,-0.84714993E-03,-0.59704529E-03,-0.35958354E-04,
        -0.25457894E-04, 0.80231985E-04, 0.69928967E-04,-0.54812481E-05,
        -0.30843445E-03,-0.65598811E-03,-0.50702342E-03, 0.12935716E-04,
         0.64213486E-05,-0.14047950E-04, 0.26324876E-05, 0.31627194E-04,
         0.17378510E-04,-0.69128810E-05,-0.23056689E-04,-0.77526238E-05,
        -0.15529646E-04,-0.14140397E-04,-0.93448380E-05, 0.42953303E-04,
         0.74225727E-06,-0.21219503E-05, 0.16915520E-05,-0.38910830E-05,
        -0.25280951E-05, 0.31071895E-05, 0.66862453E-05, 0.22706699E-05,
        -0.22058052E-05, 0.60254492E-05,-0.32211474E-05, 0.97985248E-05,
        -0.28262452E-05, 0.48372676E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_3_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_125                              =v_t_e_q1q2_3_125                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]*x11*x22            
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_125                              =v_t_e_q1q2_3_125                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x13                
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_3_125                              =v_t_e_q1q2_3_125                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]*x12*x21    *x42    
        +coeff[ 29]*x11*x23        *x51
        +coeff[ 30]*x12*x21*x31    *x51
        +coeff[ 31]*x13            *x52
        +coeff[ 32]    *x21*x32    *x52
        +coeff[ 33]*x13*x21*x31    *x51
        +coeff[ 34]    *x22*x33    *x51
    ;
    v_t_e_q1q2_3_125                              =v_t_e_q1q2_3_125                              
        +coeff[ 35]    *x21*x31*x43*x51
        +coeff[ 36]                *x51
        +coeff[ 37]    *x22            
        +coeff[ 38]        *x32        
        +coeff[ 39]    *x22*x31        
        +coeff[ 40]*x11*x21    *x41    
        +coeff[ 41]        *x31*x42    
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]*x11        *x41*x51
    ;
    v_t_e_q1q2_3_125                              =v_t_e_q1q2_3_125                              
        +coeff[ 44]    *x21*x33        
        +coeff[ 45]*x12*x21    *x41    
        +coeff[ 46]    *x23    *x41    
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]*x12        *x42    
        +coeff[ 49]*x11*x21    *x42    
        ;

    return v_t_e_q1q2_3_125                              ;
}
float y_e_q1q2_3_125                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.3570820E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.37492966E-03, 0.16688919E+00, 0.11574984E+00,-0.23802971E-02,
        -0.23978951E-02, 0.29745747E-03,-0.19410693E-03, 0.74580879E-04,
        -0.18068745E-03, 0.17057885E-03, 0.11339343E-03,-0.27633351E-03,
        -0.20989726E-03,-0.31993233E-04, 0.10262267E-03, 0.10371981E-03,
        -0.39991555E-04, 0.59959242E-04,-0.49009031E-05,-0.11616582E-04,
         0.55488181E-04, 0.80642065E-04,-0.18680073E-04,-0.38431797E-04,
        -0.45686505E-04,-0.33665943E-04,-0.18965013E-04,-0.22426326E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_3_125                              =v_y_e_q1q2_3_125                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_3_125                              =v_y_e_q1q2_3_125                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22    *x43*x51
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]        *x31*x44*x51
    ;
    v_y_e_q1q2_3_125                              =v_y_e_q1q2_3_125                              
        +coeff[ 26]*x12        *x42*x51
        +coeff[ 27]    *x23    *x42*x51
        ;

    return v_y_e_q1q2_3_125                              ;
}
float p_e_q1q2_3_125                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1448377E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.14967925E-03, 0.32994058E-01, 0.63240647E-01,-0.14037148E-02,
        -0.13981403E-02, 0.37267405E-03, 0.32632129E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]        *x32*x43    
        ;

    return v_p_e_q1q2_3_125                              ;
}
float l_e_q1q2_3_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2105261E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.20195253E-02,-0.11248357E-03,-0.35605528E-02,-0.56385878E-03,
        -0.32036565E-02,-0.40813405E-02, 0.32312705E-03,-0.59978140E-03,
         0.32995595E-03,-0.18624670E-03, 0.20487960E-03,-0.28306092E-03,
         0.69800415E-03,-0.92982649E-04, 0.31525435E-03,-0.71574672E-03,
        -0.44743929E-03,-0.49889309E-03,-0.11641467E-03, 0.95916133E-04,
         0.29720392E-03,-0.45491999E-03,-0.35094409E-03,-0.33395903E-03,
        -0.37897145E-03, 0.24299042E-02, 0.77267403E-04, 0.25705373E-03,
        -0.70120196E-03, 0.11140250E-02,-0.81174454E-03,-0.15451328E-03,
        -0.38154423E-03, 0.33413217E-03,-0.16577156E-03,-0.68089768E-03,
         0.14861953E-02,-0.54767827E-03,-0.75522176E-03,-0.40444185E-03,
        -0.79509243E-03, 0.36790236E-03, 0.36482210E-03, 0.56641572E-03,
         0.81998925E-03, 0.17230387E-02, 0.17098390E-02, 0.53606351E-03,
        -0.62202936E-03,-0.80455490E-03,-0.13362973E-02,-0.13891298E-02,
         0.12374003E-02, 0.33864961E-03,-0.10561242E-02, 0.62657747E-03,
        -0.74416900E-03,-0.19904787E-02,-0.14385741E-02, 0.10433623E-02,
        -0.81506395E-03,-0.22484686E-02, 0.14631219E-02, 0.22773496E-02,
        -0.12927878E-02,-0.15310015E-02, 0.10365451E-02,-0.60965179E-03,
         0.14892885E-02,-0.78120723E-03, 0.84346480E-03,-0.42055771E-03,
        -0.20475848E-05, 0.13587445E-03,-0.97424519E-04, 0.84475156E-04,
         0.11181001E-03,-0.77967685E-04,-0.80678983E-04, 0.20658935E-03,
        -0.93417715E-04, 0.23298811E-03,-0.12815783E-03, 0.12670332E-03,
         0.79734760E-04,-0.81696344E-04, 0.19628590E-03, 0.26517245E-03,
         0.99902129E-04, 0.39552967E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x12            *x53
        +coeff[  7]*x11*x21*x34        
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[  8]        *x31*x43*x53
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x12                
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]*x11*x21        *x51
        +coeff[ 16]        *x32    *x52
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 17]        *x31*x41*x52
        +coeff[ 18]*x11    *x33        
        +coeff[ 19]*x11*x22    *x41    
        +coeff[ 20]*x11*x21    *x41*x51
        +coeff[ 21]*x12    *x32        
        +coeff[ 22]*x12    *x31*x41    
        +coeff[ 23]*x12    *x31    *x51
        +coeff[ 24]*x13        *x41    
        +coeff[ 25]        *x33*x42    
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]*x13            *x51
        +coeff[ 28]    *x21*x33    *x51
        +coeff[ 29]*x11*x23        *x51
        +coeff[ 30]*x11*x22*x31    *x51
        +coeff[ 31]*x11    *x33    *x51
        +coeff[ 32]*x11    *x31*x42*x51
        +coeff[ 33]*x11        *x43*x51
        +coeff[ 34]*x12    *x33        
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 35]*x12*x22        *x51
        +coeff[ 36]*x12        *x42*x51
        +coeff[ 37]*x12        *x41*x52
        +coeff[ 38]    *x23*x33        
        +coeff[ 39]    *x21*x34*x41    
        +coeff[ 40]        *x34*x41*x51
        +coeff[ 41]    *x24        *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]*x11    *x34*x41    
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 44]*x11*x22*x31*x42    
        +coeff[ 45]*x11*x23*x31    *x51
        +coeff[ 46]*x11*x23    *x41*x51
        +coeff[ 47]*x11*x22*x31*x41*x51
        +coeff[ 48]*x11*x21*x31*x42*x51
        +coeff[ 49]*x11*x22        *x53
        +coeff[ 50]*x11*x21    *x41*x53
        +coeff[ 51]*x12*x21*x31*x41*x51
        +coeff[ 52]*x12    *x32*x41*x51
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 53]*x13*x21    *x42    
        +coeff[ 54]        *x33*x44    
        +coeff[ 55]*x13*x22        *x51
        +coeff[ 56]*x13*x21*x31    *x51
        +coeff[ 57]    *x24*x31*x41*x51
        +coeff[ 58]    *x22*x32*x42*x51
        +coeff[ 59]    *x23    *x43*x51
        +coeff[ 60]    *x21*x32*x43*x51
        +coeff[ 61]    *x21    *x44*x52
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 62]    *x21    *x42*x54
        +coeff[ 63]*x11    *x33*x42*x51
        +coeff[ 64]*x12    *x33*x42    
        +coeff[ 65]*x12        *x44*x51
        +coeff[ 66]*x12*x22*x31    *x52
        +coeff[ 67]    *x23*x34    *x51
        +coeff[ 68]    *x23*x33*x41*x51
        +coeff[ 69]*x13    *x31    *x53
        +coeff[ 70]    *x21*x34    *x53
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 71]            *x44*x54
        +coeff[ 72]*x11                
        +coeff[ 73]    *x21*x31        
        +coeff[ 74]    *x21        *x51
        +coeff[ 75]        *x31    *x51
        +coeff[ 76]*x11        *x41    
        +coeff[ 77]        *x33        
        +coeff[ 78]    *x22    *x41    
        +coeff[ 79]    *x21    *x42    
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 80]        *x32    *x51
        +coeff[ 81]        *x31*x41*x51
        +coeff[ 82]        *x31    *x52
        +coeff[ 83]            *x41*x52
        +coeff[ 84]                *x53
        +coeff[ 85]*x11*x22            
        +coeff[ 86]*x11    *x32        
        +coeff[ 87]*x11    *x31*x41    
        +coeff[ 88]*x11        *x42    
    ;
    v_l_e_q1q2_3_125                              =v_l_e_q1q2_3_125                              
        +coeff[ 89]*x11    *x31    *x51
        ;

    return v_l_e_q1q2_3_125                              ;
}
float x_e_q1q2_3_100                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.9211587E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.95515238E-03, 0.12534696E+00, 0.36451102E-02, 0.79400837E-03,
         0.11951025E-03,-0.53898420E-03,-0.11916602E-02,-0.84060169E-03,
        -0.23852814E-03,-0.40395491E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_3_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_3_100                              =v_x_e_q1q2_3_100                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        ;

    return v_x_e_q1q2_3_100                              ;
}
float t_e_q1q2_3_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1206845E-04;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12275888E-04,-0.19055816E-02, 0.18211928E-02, 0.21944426E-02,
        -0.11397979E-02,-0.93944528E-03,-0.65785257E-05,-0.28982191E-03,
         0.71973598E-04,-0.28209868E-03,-0.45386681E-03,-0.82863182E-04,
        -0.84343343E-03,-0.59367809E-03,-0.12564059E-04,-0.34170884E-04,
        -0.23544035E-04, 0.10904597E-03, 0.71629089E-04,-0.16427898E-05,
        -0.58512687E-03,-0.45644192E-03, 0.37611379E-04, 0.32311393E-04,
         0.50291328E-05,-0.15739026E-04, 0.18626954E-04,-0.27778913E-03,
        -0.16079526E-04,-0.14847536E-04, 0.12458191E-04, 0.18603021E-06,
         0.12421592E-04,-0.32596993E-05, 0.15319118E-05,-0.63888983E-05,
         0.27316316E-05, 0.22180195E-05,-0.23929590E-05,-0.17460487E-04,
        -0.36360152E-05,-0.55309038E-05, 0.71820341E-05,-0.61217352E-05,
        -0.67783412E-05,-0.64059300E-05,-0.16159416E-04, 0.50312374E-05,
        -0.14681194E-04,-0.95293944E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_3_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]*x12*x21*x32        
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_3_100                              =v_t_e_q1q2_3_100                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_3_100                              =v_t_e_q1q2_3_100                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]*x12*x21*x32    *x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x13                
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_3_100                              =v_t_e_q1q2_3_100                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]    *x22*x31*x41*x51
        +coeff[ 29]    *x22*x33    *x51
        +coeff[ 30]*x11*x23        *x52
        +coeff[ 31]        *x31        
        +coeff[ 32]*x11*x21            
        +coeff[ 33]*x11        *x41    
        +coeff[ 34]                *x52
    ;
    v_t_e_q1q2_3_100                              =v_t_e_q1q2_3_100                              
        +coeff[ 35]    *x22*x31        
        +coeff[ 36]*x12        *x41    
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]    *x22        *x51
        +coeff[ 39]*x11*x23            
        +coeff[ 40]*x11*x22*x31        
        +coeff[ 41]*x11*x22    *x41    
        +coeff[ 42]*x11    *x32*x41    
        +coeff[ 43]*x13            *x51
    ;
    v_t_e_q1q2_3_100                              =v_t_e_q1q2_3_100                              
        +coeff[ 44]*x12*x21        *x51
        +coeff[ 45]    *x22    *x41*x51
        +coeff[ 46]*x11*x21        *x52
        +coeff[ 47]*x11            *x53
        +coeff[ 48]*x13*x22            
        +coeff[ 49]    *x22*x32*x41    
        ;

    return v_t_e_q1q2_3_100                              ;
}
float y_e_q1q2_3_100                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1353030E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.12444750E-02, 0.16625525E+00, 0.11517151E+00,-0.23568405E-02,
        -0.23696541E-02, 0.32069921E-03,-0.11104449E-03, 0.56216730E-04,
        -0.45957593E-04,-0.11120828E-03, 0.15686594E-03,-0.23355622E-04,
         0.11737025E-03, 0.97015865E-04,-0.34103101E-04,-0.31635919E-03,
         0.16177390E-03,-0.43364427E-04,-0.21941923E-03,-0.21926123E-03,
         0.96610693E-05, 0.67013111E-05, 0.55774519E-04, 0.52907024E-04,
        -0.18213057E-04,-0.28591021E-03,-0.59538728E-04, 0.15900274E-04,
        -0.10657116E-03,-0.51851890E-04,-0.46068762E-04,-0.41995489E-04,
         0.29082365E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_3_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]            *x45    
    ;
    v_y_e_q1q2_3_100                              =v_y_e_q1q2_3_100                              
        +coeff[  8]    *x22    *x43    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q1q2_3_100                              =v_y_e_q1q2_3_100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x21*x31*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x21    *x43*x51
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_3_100                              =v_y_e_q1q2_3_100                              
        +coeff[ 26]*x11*x21*x31*x42    
        +coeff[ 27]*x11    *x32*x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]            *x43*x52
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]        *x32*x45    
        +coeff[ 32]*x11    *x31*x43*x51
        ;

    return v_y_e_q1q2_3_100                              ;
}
float p_e_q1q2_3_100                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.4715090E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.43155623E-03, 0.32632239E-01, 0.62763944E-01,-0.13859619E-02,
        -0.13806164E-02, 0.37391504E-03, 0.20517729E-03, 0.20831473E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_3_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]            *x43    
        +coeff[  7]        *x34*x41    
        ;

    return v_p_e_q1q2_3_100                              ;
}
float l_e_q1q2_3_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2103396E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.21586772E-02,-0.34833797E-02,-0.11690672E-02,-0.34586415E-02,
        -0.40851524E-02,-0.23206865E-03, 0.23752176E-03, 0.14504533E-03,
        -0.12668449E-03, 0.38490735E-03,-0.92262408E-03, 0.13179815E-03,
         0.41690582E-03, 0.40824449E-03,-0.61560614E-03, 0.14352998E-03,
         0.67392312E-03, 0.26505906E-03,-0.33215442E-03,-0.44646353E-03,
         0.39873645E-04,-0.21320628E-02,-0.15661748E-02,-0.12316483E-03,
        -0.36438880E-03, 0.48707219E-03, 0.11241050E-03,-0.18514324E-02,
         0.95624907E-03,-0.53629198E-03, 0.29958987E-02, 0.14606706E-02,
         0.20934893E-02,-0.62570436E-03, 0.14954818E-03,-0.80270896E-03,
         0.41841491E-03,-0.47022049E-03, 0.80610497E-03,-0.74134284E-03,
         0.49128133E-03,-0.37807814E-03,-0.55493816E-03, 0.35256581E-03,
         0.81677368E-03, 0.22896102E-02, 0.14539286E-02,-0.20045645E-02,
         0.14444183E-03, 0.34420213E-03,-0.87890128E-03,-0.48757947E-03,
         0.51976251E-03,-0.71190763E-03,-0.33039492E-03, 0.22505925E-02,
         0.74657140E-03, 0.52383350E-03, 0.31143555E-02,-0.27109161E-02,
        -0.29767028E-02,-0.77239895E-03,-0.41348900E-03, 0.27577434E-03,
        -0.24793344E-02,-0.17700867E-02,-0.35770034E-03, 0.60057454E-03,
         0.17871604E-02,-0.14930646E-02, 0.71034627E-03,-0.11562138E-02,
        -0.11292917E-02, 0.88081649E-03, 0.62568515E-03,-0.46517796E-03,
         0.47728401E-02, 0.36186392E-02, 0.94722532E-03,-0.13160928E-03,
        -0.12461591E-03,-0.13759961E-03,-0.32440937E-03, 0.13495513E-03,
         0.43831129E-04,-0.18860128E-03, 0.11243674E-03,-0.49526250E-03,
         0.25044102E-03, 0.10052401E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_3_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31        
        +coeff[  6]*x11*x21            
        +coeff[  7]*x11    *x31        
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[  8]            *x42*x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x12        *x41    
        +coeff[ 12]*x13                
        +coeff[ 13]        *x34        
        +coeff[ 14]            *x44    
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]            *x42*x52
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 17]        *x31    *x53
        +coeff[ 18]                *x54
        +coeff[ 19]*x11*x21*x32        
        +coeff[ 20]*x11    *x33        
        +coeff[ 21]*x11    *x32*x41    
        +coeff[ 22]*x11    *x31*x42    
        +coeff[ 23]*x12*x21        *x51
        +coeff[ 24]    *x23        *x52
        +coeff[ 25]        *x31*x41*x53
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 26]*x11*x24            
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]*x11*x21*x32*x41    
        +coeff[ 29]*x11*x21*x31*x42    
        +coeff[ 30]*x11    *x31*x42*x51
        +coeff[ 31]*x11*x22        *x52
        +coeff[ 32]*x11*x21*x31    *x52
        +coeff[ 33]*x11        *x42*x52
        +coeff[ 34]*x12    *x32    *x51
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 35]    *x23*x32*x41    
        +coeff[ 36]    *x23    *x43    
        +coeff[ 37]        *x33*x43    
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x22    *x43*x51
        +coeff[ 40]        *x33    *x53
        +coeff[ 41]*x11*x24*x31        
        +coeff[ 42]*x12*x22*x32        
        +coeff[ 43]*x12*x22        *x52
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 44]*x12*x21    *x41*x52
        +coeff[ 45]*x13    *x32*x41    
        +coeff[ 46]*x13    *x31*x42    
        +coeff[ 47]    *x21*x34*x42    
        +coeff[ 48]    *x23*x31*x43    
        +coeff[ 49]*x13*x21*x31    *x51
        +coeff[ 50]    *x23*x33    *x51
        +coeff[ 51]    *x21*x33*x42*x51
        +coeff[ 52]        *x32*x44*x51
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 53]    *x23*x31*x41*x52
        +coeff[ 54]        *x34    *x53
        +coeff[ 55]    *x21*x31*x42*x53
        +coeff[ 56]    *x21    *x43*x53
        +coeff[ 57]            *x44*x53
        +coeff[ 58]*x11*x23*x33        
        +coeff[ 59]*x11    *x31*x44*x51
        +coeff[ 60]*x11*x21*x33    *x52
        +coeff[ 61]*x11    *x33*x41*x52
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 62]*x11*x22*x31    *x53
        +coeff[ 63]*x11    *x32*x41*x53
        +coeff[ 64]*x11    *x31*x42*x53
        +coeff[ 65]*x11*x22        *x54
        +coeff[ 66]*x11    *x32    *x54
        +coeff[ 67]*x12*x21*x34        
        +coeff[ 68]*x12*x22*x32    *x51
        +coeff[ 69]*x12*x22    *x42*x51
        +coeff[ 70]*x12        *x44*x51
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 71]*x12*x21    *x42*x52
        +coeff[ 72]*x13*x24            
        +coeff[ 73]*x13*x23    *x41    
        +coeff[ 74]*x13*x22    *x42    
        +coeff[ 75]*x13        *x43*x51
        +coeff[ 76]    *x22*x33*x41*x52
        +coeff[ 77]    *x22*x32*x42*x52
        +coeff[ 78]    *x22*x32    *x54
        +coeff[ 79]*x11                
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 80]    *x21    *x41    
        +coeff[ 81]    *x21        *x51
        +coeff[ 82]        *x31    *x51
        +coeff[ 83]            *x41*x51
        +coeff[ 84]*x11            *x51
        +coeff[ 85]    *x23            
        +coeff[ 86]    *x21*x32        
        +coeff[ 87]    *x21*x31*x41    
        +coeff[ 88]    *x21    *x42    
    ;
    v_l_e_q1q2_3_100                              =v_l_e_q1q2_3_100                              
        +coeff[ 89]        *x31*x42    
        ;

    return v_l_e_q1q2_3_100                              ;
}
