float x_e_q2ex_1_1200                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.1288991E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.12922779E-02, 0.16514899E+00, 0.81075542E-02,-0.41458561E-05,
        -0.32564814E-02, 0.32982638E-03,-0.12163920E-02,-0.29430904E-02,
        -0.21743921E-02, 0.15336246E-05,-0.14007234E-02,-0.37770407E-03,
        -0.18251436E-02,-0.16514596E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_q2ex_1_1200                              =v_x_e_q2ex_1_1200                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_1200                              ;
}
float t_e_q2ex_1_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4221789E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.42337494E-03,-0.24592064E-02, 0.51837657E-01, 0.24729022E-02,
         0.17347929E-03,-0.19834931E-03, 0.25889464E-03, 0.64764266E-04,
        -0.50026953E-03,-0.82259299E-04,-0.19721860E-03,-0.57938360E-04,
        -0.16017352E-02,-0.12274530E-02,-0.60809023E-04,-0.48361522E-04,
         0.76358992E-04, 0.17115104E-03, 0.13068935E-03,-0.98157628E-03,
         0.66659713E-05,-0.23833139E-04,-0.14719150E-04, 0.25485842E-04,
        -0.53664274E-03,-0.11713414E-02, 0.66579346E-05, 0.56072371E-04,
         0.15592721E-04,-0.10255049E-04,-0.12117306E-03,-0.10200112E-03,
         0.13790110E-04, 0.96499716E-05,-0.70673697E-04, 0.20171252E-04,
         0.29543498E-04,-0.24914771E-04,-0.12702030E-04, 0.67114925E-05,
         0.32474563E-05, 0.27293252E-05, 0.11334067E-04, 0.53294420E-05,
        -0.27314900E-04, 0.46049240E-05,-0.12888376E-04,-0.65317445E-05,
         0.81650178E-05, 0.93734097E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_1200                              =v_t_e_q2ex_1_1200                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_1200                              =v_t_e_q2ex_1_1200                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23*x32    *x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11            *x52
        +coeff[ 23]    *x21        *x53
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_t_e_q2ex_1_1200                              =v_t_e_q2ex_1_1200                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x12*x21        *x52
        +coeff[ 33]    *x23*x32*x41    
        +coeff[ 34]    *x22*x32*x42    
    ;
    v_t_e_q2ex_1_1200                              =v_t_e_q2ex_1_1200                              
        +coeff[ 35]*x12*x22*x31    *x51
        +coeff[ 36]*x13        *x42*x51
        +coeff[ 37]*x11*x22    *x42*x51
        +coeff[ 38]*x11*x21            
        +coeff[ 39]        *x31*x41    
        +coeff[ 40]*x11*x21*x31        
        +coeff[ 41]    *x22*x31        
        +coeff[ 42]*x13*x21            
        +coeff[ 43]    *x22*x32        
    ;
    v_t_e_q2ex_1_1200                              =v_t_e_q2ex_1_1200                              
        +coeff[ 44]    *x22*x31*x41    
        +coeff[ 45]    *x21    *x43    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]*x12*x21        *x51
        +coeff[ 48]*x11    *x31*x41*x51
        +coeff[ 49]*x11*x21        *x52
        ;

    return v_t_e_q2ex_1_1200                              ;
}
float y_e_q2ex_1_1200                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2142405E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.11016316E-03, 0.21551304E+00, 0.11993705E+00,-0.19784877E-02,
        -0.12512071E-02,-0.66287658E-03,-0.41127004E-03,-0.31305352E-03,
        -0.24144239E-03,-0.66796623E-04,-0.40723866E-04, 0.89060646E-04,
         0.19076835E-04,-0.49530022E-03,-0.25905354E-03,-0.34897406E-04,
         0.54135046E-04, 0.84954816E-04,-0.42759135E-03,-0.45435457E-03,
         0.96367221E-05, 0.85943160E-04,-0.52746054E-04, 0.21581172E-04,
        -0.42007639E-03,-0.15374673E-03,-0.11963217E-03,-0.87220011E-04,
         0.25279263E-04,-0.14075229E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_1200                              =v_y_e_q2ex_1_1200                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q2ex_1_1200                              =v_y_e_q2ex_1_1200                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]    *x24    *x41*x51
        +coeff[ 22]    *x22*x31*x42*x52
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q2ex_1_1200                              =v_y_e_q2ex_1_1200                              
        +coeff[ 26]*x11*x23    *x41    
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]    *x21*x32*x43    
        +coeff[ 29]    *x22    *x45    
        ;

    return v_y_e_q2ex_1_1200                              ;
}
float p_e_q2ex_1_1200                              (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.6308155E-06;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.77588547E-05,-0.16799726E-01,-0.16667185E-01, 0.16408915E-02,
         0.28345173E-02,-0.13798166E-02,-0.77385613E-03,-0.14262064E-02,
        -0.85466565E-03,-0.91368820E-05,-0.78514672E-03,-0.15001357E-03,
        -0.14903165E-03, 0.35369003E-05, 0.36471327E-04,-0.10239335E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_1200                              =v_p_e_q2ex_1_1200                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_1200                              ;
}
float l_e_q2ex_1_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2692505E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.27025861E-02,-0.40740641E-02,-0.12395974E-02,-0.45831189E-02,
        -0.54179248E-02,-0.16732953E-03,-0.89005753E-03, 0.20596337E-03,
         0.24887484E-04,-0.37943083E-03,-0.43378636E-03,-0.27348250E-03,
        -0.21338710E-03,-0.40809234E-03,-0.10815611E-02,-0.18129533E-03,
         0.30027848E-03, 0.31296775E-03,-0.28492245E-03, 0.68342040E-03,
         0.35570280E-03,-0.62642473E-03, 0.83357265E-03,-0.34892088E-03,
         0.55728434E-03, 0.36451572E-02, 0.48881408E-03,-0.61263371E-03,
        -0.31912483E-02, 0.94845577E-03, 0.84520195E-03,-0.80130080E-03,
        -0.18085131E-02, 0.10420277E-02, 0.43986234E-03,-0.73183648E-03,
         0.10116892E-02,-0.40719286E-03, 0.23950698E-03, 0.51342172E-03,
        -0.90910000E-03, 0.22624778E-03, 0.99708384E-03,-0.95877651E-03,
         0.17543382E-02,-0.20294164E-02,-0.23095121E-02, 0.13638139E-02,
         0.10986690E-02, 0.17626847E-02,-0.13149251E-02, 0.64336671E-03,
        -0.20703073E-02, 0.38379896E-02,-0.21292812E-02,-0.24105078E-02,
        -0.16288824E-02, 0.79946592E-03, 0.10862091E-02, 0.10587048E-02,
        -0.19295129E-02,-0.10717842E-02, 0.23119429E-02, 0.16149370E-04,
        -0.89988305E-06, 0.17266117E-03, 0.10369383E-03,-0.37274868E-03,
        -0.18977486E-03,-0.55281981E-03,-0.13476309E-03, 0.27656555E-03,
         0.90384347E-04,-0.21156423E-03,-0.39281917E-03,-0.10432467E-03,
         0.11771404E-03,-0.11744454E-03,-0.28396779E-03, 0.26175290E-03,
         0.10555857E-03,-0.96879150E-04,-0.68113551E-03, 0.55431999E-03,
        -0.25548751E-03,-0.72936493E-03,-0.71381211E-04, 0.32544098E-03,
         0.12722295E-02,-0.52183657E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21*x32        
        +coeff[  6]*x12    *x32*x41*x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[  8]*x11            *x51
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]*x11*x21        *x51
        +coeff[ 12]        *x33*x41    
        +coeff[ 13]            *x44    
        +coeff[ 14]*x11    *x32*x41    
        +coeff[ 15]*x11    *x31*x42    
        +coeff[ 16]*x11        *x43    
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 17]*x11*x22        *x51
        +coeff[ 18]*x11    *x31*x41*x51
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x31*x42*x51
        +coeff[ 22]        *x33    *x52
        +coeff[ 23]            *x43*x52
        +coeff[ 24]*x11    *x34        
        +coeff[ 25]*x11*x22*x31*x41    
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 26]*x12*x21*x31    *x51
        +coeff[ 27]*x13    *x32        
        +coeff[ 28]        *x31*x44*x51
        +coeff[ 29]*x11*x23*x32        
        +coeff[ 30]*x11*x24    *x41    
        +coeff[ 31]*x11*x22    *x43    
        +coeff[ 32]*x11*x21*x31*x42*x51
        +coeff[ 33]*x11    *x32*x41*x52
        +coeff[ 34]*x11*x21*x31    *x53
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 35]*x11*x21    *x41*x53
        +coeff[ 36]*x11    *x31*x41*x53
        +coeff[ 37]*x11*x21        *x54
        +coeff[ 38]*x12*x24            
        +coeff[ 39]*x12*x22        *x52
        +coeff[ 40]    *x21*x34*x42    
        +coeff[ 41]    *x22*x33    *x52
        +coeff[ 42]    *x24    *x41*x52
        +coeff[ 43]    *x21*x33    *x53
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 44]    *x22*x31    *x54
        +coeff[ 45]        *x33    *x54
        +coeff[ 46]*x11*x24*x31*x41    
        +coeff[ 47]*x11    *x34*x42    
        +coeff[ 48]*x11    *x33*x41*x52
        +coeff[ 49]*x11*x22    *x42*x52
        +coeff[ 50]*x11        *x44*x52
        +coeff[ 51]*x12*x21    *x41*x53
        +coeff[ 52]*x13*x22*x31*x41    
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 53]    *x23*x34    *x51
        +coeff[ 54]    *x23*x32*x42*x51
        +coeff[ 55]    *x21*x34*x42*x51
        +coeff[ 56]        *x34*x43*x51
        +coeff[ 57]*x13        *x42*x52
        +coeff[ 58]        *x33*x43*x52
        +coeff[ 59]*x13    *x31    *x53
        +coeff[ 60]    *x23*x32    *x53
        +coeff[ 61]    *x23*x31*x41*x53
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 62]    *x21*x32*x42*x53
        +coeff[ 63]                *x51
        +coeff[ 64]*x11                
        +coeff[ 65]            *x41*x51
        +coeff[ 66]*x11*x21            
        +coeff[ 67]    *x22*x31        
        +coeff[ 68]    *x22    *x41    
        +coeff[ 69]    *x21*x31*x41    
        +coeff[ 70]    *x21    *x42    
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 71]    *x21*x31    *x51
        +coeff[ 72]                *x53
        +coeff[ 73]*x11*x22            
        +coeff[ 74]*x11    *x31    *x51
        +coeff[ 75]*x11        *x41*x51
        +coeff[ 76]*x11            *x52
        +coeff[ 77]*x12            *x51
        +coeff[ 78]    *x24            
        +coeff[ 79]    *x23*x31        
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 80]    *x23    *x41    
        +coeff[ 81]    *x21*x32*x41    
        +coeff[ 82]    *x21*x31*x42    
        +coeff[ 83]        *x32*x42    
        +coeff[ 84]    *x21    *x43    
        +coeff[ 85]    *x21*x32    *x51
        +coeff[ 86]        *x33    *x51
        +coeff[ 87]    *x21    *x42*x51
        +coeff[ 88]        *x31*x42*x51
    ;
    v_l_e_q2ex_1_1200                              =v_l_e_q2ex_1_1200                              
        +coeff[ 89]            *x43*x51
        ;

    return v_l_e_q2ex_1_1200                              ;
}
float x_e_q2ex_1_1100                              (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.1079756E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.10619299E-02,-0.32592521E-02, 0.16517937E+00, 0.81054382E-02,
         0.33811267E-03,-0.12138954E-02,-0.29426389E-02,-0.21964498E-02,
         0.30116339E-05,-0.13952847E-02,-0.37333259E-03,-0.18077057E-02,
        -0.16111835E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_1100                              =v_x_e_q2ex_1_1100                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_1100                              ;
}
float t_e_q2ex_1_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3325132E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.32656227E-03,-0.24592746E-02, 0.51855706E-01, 0.24867258E-02,
         0.17756231E-03,-0.19009008E-03, 0.27145952E-03, 0.70057125E-04,
        -0.18404101E-03,-0.52929501E-03,-0.63474748E-04,-0.67495195E-04,
        -0.16433832E-02,-0.12367623E-02,-0.86723616E-04,-0.62440180E-04,
         0.69157228E-04, 0.18782867E-03, 0.84718740E-04,-0.11832255E-02,
        -0.99061092E-03, 0.17299375E-04,-0.34274337E-04,-0.52211853E-03,
        -0.31506347E-05, 0.39317988E-05, 0.59287631E-05,-0.84637395E-05,
         0.11102070E-04, 0.56050023E-04, 0.61781220E-05,-0.28105942E-04,
         0.15631253E-04,-0.20209840E-04,-0.29672736E-04,-0.27989930E-04,
        -0.25224961E-04, 0.18928142E-04, 0.21742309E-04, 0.49605445E-04,
         0.53575553E-04, 0.30192246E-04,-0.20383643E-04, 0.44029075E-04,
        -0.16537614E-05, 0.44221979E-05,-0.39470196E-05,-0.62129038E-05,
         0.10921144E-04, 0.29711600E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_1100                              =v_t_e_q2ex_1_1100                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_1100                              =v_t_e_q2ex_1_1100                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]*x11*x21            
        +coeff[ 25]    *x22*x31        
    ;
    v_t_e_q2ex_1_1100                              =v_t_e_q2ex_1_1100                              
        +coeff[ 26]        *x31*x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x12            *x52
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x11*x21*x33        
        +coeff[ 33]    *x22*x33        
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q2ex_1_1100                              =v_t_e_q2ex_1_1100                              
        +coeff[ 35]    *x21    *x42*x52
        +coeff[ 36]*x11*x21        *x53
        +coeff[ 37]*x13*x23            
        +coeff[ 38]    *x22*x32*x41*x51
        +coeff[ 39]*x11*x22    *x42*x51
        +coeff[ 40]    *x23    *x42*x51
        +coeff[ 41]*x11    *x31*x43*x51
        +coeff[ 42]*x12*x21    *x41*x52
        +coeff[ 43]    *x21    *x42*x53
    ;
    v_t_e_q2ex_1_1100                              =v_t_e_q2ex_1_1100                              
        +coeff[ 44]    *x21*x31        
        +coeff[ 45]*x13                
        +coeff[ 46]*x12*x21            
        +coeff[ 47]*x11*x21    *x41    
        +coeff[ 48]*x11*x21        *x51
        +coeff[ 49]*x11    *x31    *x51
        ;

    return v_t_e_q2ex_1_1100                              ;
}
float y_e_q2ex_1_1100                              (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.1672854E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.29312327E-03, 0.21546285E+00, 0.11997409E+00,-0.19806949E-02,
        -0.12559262E-02,-0.76287426E-03,-0.54683909E-03,-0.39013510E-03,
        -0.30369574E-03,-0.72988005E-04,-0.57047411E-04,-0.36097888E-04,
         0.45074967E-05,-0.37752313E-03,-0.39492178E-03,-0.18401684E-03,
        -0.85345542E-04, 0.50568673E-04, 0.86262800E-04, 0.63652733E-04,
         0.74823103E-04, 0.11780901E-04,-0.17048485E-04, 0.30886211E-04,
        -0.33850512E-04,-0.14153855E-03,-0.10341670E-03,-0.61051993E-04,
         0.62056890E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_1100                              =v_y_e_q2ex_1_1100                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24    *x41    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_1_1100                              =v_y_e_q2ex_1_1100                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22    *x41*x52
        +coeff[ 20]    *x24    *x41*x51
        +coeff[ 21]*x12        *x41    
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]        *x32*x41*x51
        +coeff[ 24]        *x31*x44    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q2ex_1_1100                              =v_y_e_q2ex_1_1100                              
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]    *x22    *x45    
        ;

    return v_y_e_q2ex_1_1100                              ;
}
float p_e_q2ex_1_1100                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.4783789E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.57074983E-04,-0.16815796E-01,-0.16680207E-01, 0.16426119E-02,
         0.28352609E-02,-0.13816404E-02,-0.77618076E-03,-0.14235716E-02,
        -0.85487432E-03,-0.12014123E-04,-0.78340096E-03,-0.14653854E-03,
        -0.14796447E-03,-0.13959483E-04,-0.92730494E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_1100                              =v_p_e_q2ex_1_1100                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_1100                              ;
}
float l_e_q2ex_1_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2683496E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26414883E-02,-0.11630188E-03,-0.41011833E-02,-0.71776548E-03,
        -0.44089751E-02,-0.54462026E-02,-0.37763748E-03,-0.66620538E-04,
        -0.22175266E-03,-0.53813728E-04, 0.61052956E-03, 0.72231935E-03,
         0.22033078E-03, 0.13964962E-02, 0.21644488E-03,-0.26817992E-03,
        -0.11465477E-02, 0.14015695E-02,-0.82347920E-04,-0.60888805E-03,
         0.27845005E-03,-0.37556808E-03, 0.42932385E-03, 0.29149238E-03,
         0.32245723E-03, 0.25912790E-03,-0.29460518E-03,-0.75055804E-03,
        -0.35540317E-02,-0.90840599E-03,-0.27247122E-03,-0.11271301E-02,
        -0.12012353E-02,-0.29476781E-03, 0.91912771E-04,-0.42770224E-03,
         0.30348063E-03,-0.21838579E-03, 0.54307265E-03, 0.74447884E-03,
         0.29113826E-02, 0.18280891E-02,-0.19037713E-02,-0.11960162E-02,
         0.51246467E-03, 0.45422159E-03, 0.14070475E-02,-0.53121470E-03,
         0.11022711E-02, 0.89640508E-03,-0.74826984E-03,-0.46745210E-03,
         0.74672385E-03, 0.71886909E-03, 0.38297134E-03,-0.11693838E-02,
         0.20536263E-02,-0.54865924E-03, 0.22049660E-02,-0.10108125E-02,
        -0.14446563E-02,-0.10365212E-02, 0.35810850E-02, 0.12782139E-02,
        -0.11261895E-02, 0.12024528E-02, 0.13967651E-02,-0.93809242E-03,
         0.51265152E-03,-0.12955955E-02,-0.14223837E-02,-0.30623740E-02,
         0.15286625E-02,-0.25235128E-02, 0.82917541E-03,-0.22800411E-02,
         0.83448322E-05,-0.64234046E-04,-0.12009853E-03,-0.55721626E-04,
         0.11500309E-03,-0.10306024E-03,-0.12502231E-03, 0.13308880E-03,
        -0.31374945E-03, 0.10444110E-03, 0.11491099E-03,-0.22122396E-03,
        -0.28889781E-03,-0.33825700E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x23*x33        
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[  8]*x12                
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21*x31    *x51
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 17]    *x21*x31*x42    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]*x11*x22*x31        
        +coeff[ 20]*x11*x21*x31    *x51
        +coeff[ 21]*x11    *x32    *x51
        +coeff[ 22]*x11        *x41*x52
        +coeff[ 23]*x11            *x53
        +coeff[ 24]*x12*x22            
        +coeff[ 25]*x13*x21            
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 26]    *x24*x31        
        +coeff[ 27]    *x22*x31*x41*x51
        +coeff[ 28]    *x21*x31*x42*x51
        +coeff[ 29]        *x32*x42*x51
        +coeff[ 30]    *x22    *x41*x52
        +coeff[ 31]    *x21*x31    *x53
        +coeff[ 32]        *x31    *x54
        +coeff[ 33]*x11*x21*x33        
        +coeff[ 34]*x12    *x33        
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]*x12*x21        *x52
        +coeff[ 37]*x13*x22            
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]    *x24*x32        
        +coeff[ 40]    *x22*x31*x41*x52
        +coeff[ 41]        *x33*x41*x52
        +coeff[ 42]    *x22    *x42*x52
        +coeff[ 43]    *x21*x31*x42*x52
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 44]    *x21    *x43*x52
        +coeff[ 45]            *x44*x52
        +coeff[ 46]        *x31*x41*x54
        +coeff[ 47]*x11*x23*x32        
        +coeff[ 48]*x11    *x34    *x51
        +coeff[ 49]*x11*x22*x31    *x52
        +coeff[ 50]*x11        *x43*x52
        +coeff[ 51]*x11*x22        *x53
        +coeff[ 52]*x12    *x31*x43    
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 53]*x12*x22*x31    *x51
        +coeff[ 54]*x12*x21*x31    *x52
        +coeff[ 55]*x12    *x31*x41*x52
        +coeff[ 56]    *x21*x33*x42*x51
        +coeff[ 57]*x13            *x53
        +coeff[ 58]    *x21*x31*x42*x53
        +coeff[ 59]        *x31*x43*x53
        +coeff[ 60]*x11*x22*x32    *x52
        +coeff[ 61]*x11*x22*x31*x41*x52
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 62]*x11    *x31*x43*x52
        +coeff[ 63]*x11        *x44*x52
        +coeff[ 64]*x11    *x31*x41*x54
        +coeff[ 65]*x12*x21*x33*x41    
        +coeff[ 66]*x12*x21*x31*x42*x51
        +coeff[ 67]*x12    *x33    *x52
        +coeff[ 68]*x13*x24            
        +coeff[ 69]    *x23*x33*x42    
        +coeff[ 70]        *x34*x44    
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 71]    *x24*x31*x41*x52
        +coeff[ 72]    *x22*x32*x42*x52
        +coeff[ 73]        *x33*x43*x52
        +coeff[ 74]    *x23*x31    *x54
        +coeff[ 75]        *x33*x41*x54
        +coeff[ 76]    *x21            
        +coeff[ 77]            *x41    
        +coeff[ 78]*x11                
        +coeff[ 79]            *x41*x51
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 80]                *x52
        +coeff[ 81]*x11        *x41    
        +coeff[ 82]    *x23            
        +coeff[ 83]            *x41*x52
        +coeff[ 84]*x11    *x31*x41    
        +coeff[ 85]*x11        *x41*x51
        +coeff[ 86]*x12*x21            
        +coeff[ 87]    *x23*x31        
        +coeff[ 88]    *x22*x31*x41    
    ;
    v_l_e_q2ex_1_1100                              =v_l_e_q2ex_1_1100                              
        +coeff[ 89]        *x33*x41    
        ;

    return v_l_e_q2ex_1_1100                              ;
}
float x_e_q2ex_1_1000                              (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.7642781E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.71783009E-03,-0.32584262E-02, 0.16519438E+00, 0.80958102E-02,
         0.33913061E-03,-0.12170475E-02,-0.29376522E-02,-0.21839940E-02,
         0.27896413E-04,-0.14080929E-02,-0.39260567E-03,-0.17970185E-02,
        -0.16245215E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_1000                              =v_x_e_q2ex_1_1000                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_1000                              ;
}
float t_e_q2ex_1_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2377206E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.22288480E-03,-0.24571174E-02, 0.51880434E-01, 0.24802082E-02,
         0.17767008E-03,-0.19025221E-03, 0.23260376E-03, 0.56891058E-04,
        -0.20079463E-03,-0.52168319E-03,-0.83838509E-04,-0.64056767E-04,
        -0.16212111E-02,-0.12333625E-02,-0.65986904E-04,-0.53835749E-04,
         0.19542992E-03, 0.10431869E-03,-0.46926714E-03,-0.95996907E-03,
         0.15636138E-04,-0.31863594E-04,-0.12599050E-04, 0.51793824E-04,
         0.47261219E-04,-0.11212248E-02,-0.88118186E-05, 0.11174740E-04,
        -0.75411640E-05,-0.72569574E-05, 0.14388985E-04,-0.91275360E-04,
        -0.64278931E-04,-0.22641001E-04, 0.19932337E-04, 0.25903677E-04,
         0.51149771E-04, 0.54719312E-04, 0.27875574E-04,-0.13762827E-05,
        -0.42039755E-05, 0.76455535E-05,-0.21264807E-05,-0.11612391E-05,
         0.40457244E-05, 0.46511204E-05,-0.35366309E-05,-0.32125879E-05,
         0.45348079E-05,-0.34184295E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_1000                              =v_t_e_q2ex_1_1000                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q2ex_1_1000                              =v_t_e_q2ex_1_1000                              
        +coeff[ 17]    *x21    *x42*x51
        +coeff[ 18]    *x21*x33*x41    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23*x32    *x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11            *x52
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_t_e_q2ex_1_1000                              =v_t_e_q2ex_1_1000                              
        +coeff[ 26]*x11    *x33        
        +coeff[ 27]*x12*x21        *x51
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]    *x21        *x53
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_1000                              =v_t_e_q2ex_1_1000                              
        +coeff[ 35]    *x22    *x41*x52
        +coeff[ 36]    *x23    *x42*x51
        +coeff[ 37]    *x21*x32*x42*x51
        +coeff[ 38]*x11*x22        *x53
        +coeff[ 39]        *x31        
        +coeff[ 40]*x12*x21            
        +coeff[ 41]    *x22*x31        
        +coeff[ 42]        *x32*x41    
        +coeff[ 43]    *x21*x31    *x51
    ;
    v_t_e_q2ex_1_1000                              =v_t_e_q2ex_1_1000                              
        +coeff[ 44]*x11        *x41*x51
        +coeff[ 45]    *x21    *x41*x51
        +coeff[ 46]            *x41*x52
        +coeff[ 47]*x11*x23            
        +coeff[ 48]*x13    *x31        
        +coeff[ 49]    *x23*x31        
        ;

    return v_t_e_q2ex_1_1000                              ;
}
float y_e_q2ex_1_1000                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.2118838E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.16635814E-03, 0.21532582E+00, 0.11995319E+00,-0.19849169E-02,
        -0.12574251E-02,-0.77475735E-03,-0.50650694E-03,-0.39795006E-03,
        -0.29573831E-03,-0.71557581E-04,-0.34276924E-04,-0.13021439E-04,
        -0.66674793E-05,-0.42909701E-03,-0.15821838E-03,-0.20361433E-03,
         0.53335814E-04, 0.41341078E-04, 0.84682761E-04,-0.37091103E-03,
        -0.10368755E-03, 0.14523431E-04, 0.21533731E-04, 0.80796963E-04,
        -0.12793945E-03,-0.11731514E-03, 0.85717911E-04, 0.96656790E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_1000                              =v_y_e_q2ex_1_1000                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]*x11*x23    *x41    
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q2ex_1_1000                              =v_y_e_q2ex_1_1000                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x41*x52
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q2ex_1_1000                              =v_y_e_q2ex_1_1000                              
        +coeff[ 26]    *x22*x31*x42*x51
        +coeff[ 27]    *x24    *x41*x51
        ;

    return v_y_e_q2ex_1_1000                              ;
}
float p_e_q2ex_1_1000                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.6062966E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.56611563E-04,-0.16824402E-01,-0.16691661E-01, 0.16422997E-02,
         0.28348162E-02,-0.13781085E-02,-0.77429658E-03,-0.14236491E-02,
        -0.85184089E-03,-0.17791161E-04,-0.77979302E-03,-0.15127784E-03,
        -0.14934473E-03, 0.69171579E-05, 0.37539568E-04,-0.10644159E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_1000                              =v_p_e_q2ex_1_1000                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_1000                              ;
}
float l_e_q2ex_1_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2662497E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26550894E-02,-0.42349035E-02,-0.54558308E-03,-0.45548594E-02,
        -0.55592186E-02,-0.73268457E-04, 0.49130293E-04,-0.64206228E-03,
         0.87067136E-04,-0.58636979E-05,-0.25820016E-03, 0.11458487E-02,
         0.64431841E-03,-0.57254269E-05, 0.31897158E-03,-0.55773335E-03,
         0.40376309E-03,-0.57204417E-03, 0.53495070E-03,-0.30479729E-03,
        -0.36283105E-03,-0.10041660E-03,-0.11957143E-02,-0.11315630E-02,
        -0.95430214E-03, 0.42481837E-03, 0.65444544E-03, 0.94590592E-03,
         0.17351455E-02,-0.27750098E-05,-0.10330130E-02,-0.66097022E-03,
         0.11963695E-02, 0.19605525E-02, 0.21756552E-02,-0.66605676E-03,
         0.71165891E-03, 0.12731202E-02,-0.10014066E-03,-0.14441769E-02,
        -0.96911797E-03, 0.95514156E-03,-0.51583326E-03,-0.13559944E-02,
        -0.14187276E-02, 0.84621098E-03,-0.10634342E-02, 0.25010058E-02,
        -0.13224338E-02, 0.12566772E-02, 0.30514627E-03, 0.31499105E-04,
         0.17101638E-03, 0.94317635E-04, 0.30007179E-05, 0.67496898E-04,
        -0.11792036E-03, 0.12125594E-03,-0.23612694E-03, 0.33906935E-03,
        -0.13501018E-03, 0.49579558E-04, 0.18546848E-03, 0.21783782E-03,
        -0.13556091E-03, 0.25348956E-03, 0.34547094E-03,-0.12496616E-03,
         0.24270795E-03,-0.22799200E-03,-0.54563535E-03, 0.31819750E-03,
         0.96941745E-03,-0.11629168E-02,-0.10473769E-02,-0.33362568E-03,
        -0.17857274E-04,-0.16869800E-03, 0.12836975E-03, 0.15696649E-03,
        -0.21389837E-03, 0.17676396E-03,-0.27337723E-03,-0.53823489E-03,
         0.26004750E-03,-0.57143803E-04, 0.70187717E-03, 0.15682410E-03,
        -0.44846768E-03, 0.48458154E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q2ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x24        *x51
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[  8]*x12*x22        *x51
        +coeff[  9]                *x51
        +coeff[ 10]    *x21*x31    *x51
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]*x12            *x51
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]    *x21*x33        
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 17]        *x34        
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]        *x33    *x51
        +coeff[ 20]    *x21*x31    *x52
        +coeff[ 21]*x12*x21        *x51
        +coeff[ 22]    *x22*x31*x41*x51
        +coeff[ 23]        *x33*x41*x51
        +coeff[ 24]*x11*x21    *x43    
        +coeff[ 25]*x11        *x41*x53
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 26]*x12    *x32    *x51
        +coeff[ 27]*x11*x23    *x42    
        +coeff[ 28]*x11*x22    *x43    
        +coeff[ 29]*x11    *x32*x43    
        +coeff[ 30]*x11*x21    *x44    
        +coeff[ 31]*x11*x21*x32*x41*x51
        +coeff[ 32]*x11*x21    *x42*x52
        +coeff[ 33]*x11*x21*x31    *x53
        +coeff[ 34]*x11    *x31*x41*x53
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 35]*x12*x22*x31    *x51
        +coeff[ 36]*x12*x21        *x53
        +coeff[ 37]    *x23    *x44    
        +coeff[ 38]*x13    *x31*x41*x51
        +coeff[ 39]*x11*x21*x33*x42    
        +coeff[ 40]*x11    *x34*x41*x51
        +coeff[ 41]*x11*x23    *x42*x51
        +coeff[ 42]*x11    *x34    *x52
        +coeff[ 43]*x11*x21*x32*x41*x52
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 44]*x12    *x34    *x51
        +coeff[ 45]*x13*x23*x31        
        +coeff[ 46]*x13    *x33    *x51
        +coeff[ 47]    *x23*x31*x43*x51
        +coeff[ 48]    *x21*x33*x43*x51
        +coeff[ 49]    *x23*x32*x41*x52
        +coeff[ 50]*x13        *x42*x52
        +coeff[ 51]*x12                
        +coeff[ 52]    *x22*x31        
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 53]        *x33        
        +coeff[ 54]    *x21*x31*x41    
        +coeff[ 55]        *x31*x42    
        +coeff[ 56]    *x21    *x41*x51
        +coeff[ 57]            *x42*x51
        +coeff[ 58]*x11*x21*x31        
        +coeff[ 59]*x11    *x31    *x51
        +coeff[ 60]*x12    *x31        
        +coeff[ 61]*x13                
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 62]    *x24            
        +coeff[ 63]        *x31*x43    
        +coeff[ 64]    *x23        *x51
        +coeff[ 65]        *x32*x41*x51
        +coeff[ 66]    *x21    *x42*x51
        +coeff[ 67]            *x43*x51
        +coeff[ 68]        *x32    *x52
        +coeff[ 69]    *x21    *x41*x52
        +coeff[ 70]*x11*x22    *x41    
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 71]*x11    *x32*x41    
        +coeff[ 72]*x11    *x31*x42    
        +coeff[ 73]*x11*x21*x31    *x51
        +coeff[ 74]*x11    *x31*x41*x51
        +coeff[ 75]*x11*x21        *x52
        +coeff[ 76]*x11    *x31    *x52
        +coeff[ 77]*x11        *x41*x52
        +coeff[ 78]*x12*x21    *x41    
        +coeff[ 79]*x12    *x31*x41    
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 80]*x12            *x52
        +coeff[ 81]*x13*x21            
        +coeff[ 82]*x13    *x31        
        +coeff[ 83]    *x24*x31        
        +coeff[ 84]    *x23*x32        
        +coeff[ 85]    *x24    *x41    
        +coeff[ 86]    *x23*x31*x41    
        +coeff[ 87]        *x34*x41    
        +coeff[ 88]    *x23    *x42    
    ;
    v_l_e_q2ex_1_1000                              =v_l_e_q2ex_1_1000                              
        +coeff[ 89]    *x22*x31*x42    
        ;

    return v_l_e_q2ex_1_1000                              ;
}
float x_e_q2ex_1_900                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.5349966E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.49731822E-03,-0.32544301E-02, 0.16531134E+00, 0.81022596E-02,
         0.33780144E-03,-0.13799664E-02,-0.37280424E-02,-0.29182998E-02,
        -0.33367878E-05,-0.13114635E-02,-0.37971686E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_900                              =v_x_e_q2ex_1_900                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2ex_1_900                              ;
}
float t_e_q2ex_1_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1657986E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15394668E-03,-0.24561721E-02, 0.51914178E-01, 0.24706095E-02,
         0.17847052E-03,-0.19691112E-03, 0.24551942E-03, 0.53137173E-04,
        -0.19632641E-03,-0.49812300E-03,-0.76356839E-04,-0.63500294E-04,
        -0.15766952E-02,-0.12040723E-02,-0.69272115E-04,-0.57166595E-04,
         0.70709502E-04, 0.20149750E-03, 0.12991045E-03,-0.11895529E-02,
        -0.10099108E-02, 0.80488653E-05,-0.29656920E-04, 0.59124784E-04,
        -0.51299407E-03,-0.23940089E-04,-0.88032266E-05, 0.75471262E-05,
         0.20083423E-05, 0.16297472E-04,-0.78097772E-04,-0.58209873E-04,
         0.15317579E-04, 0.22447593E-04,-0.70082577E-04,-0.40659204E-04,
         0.25472988E-04, 0.68968220E-04, 0.49535454E-04,-0.94856432E-06,
         0.20211951E-05,-0.17065817E-05,-0.17692769E-05,-0.23790756E-05,
        -0.25911484E-05,-0.33247975E-05,-0.54599941E-05,-0.65279478E-05,
        -0.25640247E-05, 0.32210592E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_900                              =v_t_e_q2ex_1_900                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_900                              =v_t_e_q2ex_1_900                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x12*x21*x31    *x51
    ;
    v_t_e_q2ex_1_900                              =v_t_e_q2ex_1_900                              
        +coeff[ 26]*x11            *x52
        +coeff[ 27]*x11*x22*x31        
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x21*x32    *x51
        +coeff[ 33]*x12*x21        *x52
        +coeff[ 34]*x11*x21*x33    *x51
    ;
    v_t_e_q2ex_1_900                              =v_t_e_q2ex_1_900                              
        +coeff[ 35]*x12*x22    *x41*x51
        +coeff[ 36]*x11*x21*x31*x42*x51
        +coeff[ 37]    *x21*x32*x42*x51
        +coeff[ 38]*x11*x21*x31    *x53
        +coeff[ 39]                *x51
        +coeff[ 40]    *x22            
        +coeff[ 41]*x11        *x41    
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]        *x31    *x51
    ;
    v_t_e_q2ex_1_900                              =v_t_e_q2ex_1_900                              
        +coeff[ 44]                *x52
        +coeff[ 45]*x13                
        +coeff[ 46]*x12*x21            
        +coeff[ 47]        *x31*x42    
        +coeff[ 48]            *x43    
        +coeff[ 49]*x11        *x41*x51
        ;

    return v_t_e_q2ex_1_900                              ;
}
float y_e_q2ex_1_900                              (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.2342229E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.15769027E-03, 0.21535084E+00, 0.11991292E+00,-0.19859031E-02,
        -0.12498408E-02,-0.66172797E-03,-0.48716710E-03,-0.34484820E-03,
        -0.22544325E-03,-0.76252203E-04,-0.47826772E-04,-0.48345108E-04,
         0.57178268E-05,-0.39948625E-03,-0.21359841E-03, 0.49021732E-04,
         0.32429245E-04, 0.88063032E-04,-0.41541492E-03,-0.48374344E-03,
         0.11670745E-03,-0.20064159E-03,-0.78925987E-05, 0.18552380E-04,
         0.15968006E-04, 0.46265206E-04, 0.15331649E-04, 0.44216711E-04,
        -0.32669079E-03, 0.21308595E-04,-0.87210283E-04, 0.34522873E-04,
        -0.78881247E-04, 0.29416651E-04,-0.11094248E-03, 0.89071480E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_900                              =v_y_e_q2ex_1_900                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]            *x45    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]    *x22    *x41*x51
    ;
    v_y_e_q2ex_1_900                              =v_y_e_q2ex_1_900                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x32*x43    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x21*x31*x41    
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x41*x52
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_q2ex_1_900                              =v_y_e_q2ex_1_900                              
        +coeff[ 26]*x11        *x42*x51
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]*x11*x23    *x42    
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q2ex_1_900                              =v_y_e_q2ex_1_900                              
        +coeff[ 35]    *x24    *x41*x51
        ;

    return v_y_e_q2ex_1_900                              ;
}
float p_e_q2ex_1_900                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.3524668E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.27448632E-04,-0.16836397E-01,-0.16720094E-01, 0.16424274E-02,
         0.28366607E-02,-0.13806103E-02,-0.77658030E-03,-0.14221863E-02,
        -0.85124117E-03,-0.13015880E-04,-0.78336999E-03,-0.15122346E-03,
        -0.14823822E-03, 0.40382470E-05, 0.39098824E-04,-0.10419917E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_900                              =v_p_e_q2ex_1_900                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_900                              ;
}
float l_e_q2ex_1_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2649713E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26878910E-02,-0.43355636E-02,-0.12207822E-02,-0.42539584E-02,
        -0.53125424E-02,-0.16660713E-03, 0.17642869E-03,-0.99102850E-03,
        -0.77136129E-03,-0.14871775E-03, 0.10104547E-03, 0.33881541E-03,
         0.80955314E-03,-0.61776908E-03,-0.68436726E-03,-0.12855815E-03,
         0.69452223E-03,-0.52992540E-03,-0.28940971E-03, 0.73781284E-03,
        -0.32319198E-03,-0.31405897E-03,-0.49040263E-03,-0.88403752E-03,
        -0.12744350E-02,-0.39512006E-03, 0.22411610E-02, 0.11805842E-02,
        -0.39764218E-04, 0.10573575E-02,-0.52251307E-04,-0.40506179E-03,
        -0.98468713E-03,-0.10359826E-02, 0.35608807E-03,-0.12442366E-02,
         0.95697911E-03,-0.42285083E-03,-0.20531786E-02, 0.57370309E-03,
         0.11247102E-02,-0.18044176E-02,-0.37920481E-03, 0.33622934E-03,
         0.30863969E-03,-0.84586942E-03,-0.65582019E-03, 0.18825276E-02,
        -0.79102942E-03,-0.73726987E-03, 0.76873734E-03, 0.16560501E-02,
        -0.29540565E-02,-0.26551138E-02,-0.25126773E-02, 0.11458570E-02,
         0.10569710E-02,-0.12951348E-02, 0.26294822E-02, 0.49967953E-03,
         0.17513540E-02, 0.18945450E-02,-0.66083792E-03,-0.17708588E-02,
         0.73342584E-03, 0.12070198E-02, 0.64064114E-03, 0.58987411E-04,
         0.17808003E-04,-0.24893542E-03,-0.80793467E-03, 0.28007271E-03,
         0.55818087E-04,-0.77108525E-04,-0.10707416E-03, 0.16686671E-03,
        -0.11337168E-03, 0.20550711E-03,-0.36206844E-03, 0.35622934E-03,
         0.23470545E-03, 0.10400212E-02, 0.27473323E-03, 0.93333866E-03,
         0.34029028E-04, 0.22235882E-03, 0.31868881E-03,-0.10218055E-03,
        -0.33830586E-03, 0.21384143E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x42    
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[  8]    *x23*x33        
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]        *x32    *x51
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11        *x41*x51
        +coeff[ 15]*x12    *x31        
        +coeff[ 16]*x11    *x31*x41*x51
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 17]*x12        *x42    
        +coeff[ 18]*x12    *x31    *x51
        +coeff[ 19]*x12            *x52
        +coeff[ 20]        *x34*x41    
        +coeff[ 21]    *x24        *x51
        +coeff[ 22]    *x21        *x54
        +coeff[ 23]*x11*x21*x33        
        +coeff[ 24]*x11*x21*x31*x42    
        +coeff[ 25]*x11    *x33    *x51
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 26]*x11    *x31*x42*x51
        +coeff[ 27]*x11*x21    *x41*x52
        +coeff[ 28]*x11    *x31    *x53
        +coeff[ 29]*x11        *x41*x53
        +coeff[ 30]*x12*x21*x32        
        +coeff[ 31]*x12*x21*x31*x41    
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]*x12        *x42*x51
        +coeff[ 34]*x12            *x53
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 35]        *x32*x42*x52
        +coeff[ 36]            *x44*x52
        +coeff[ 37]            *x42*x54
        +coeff[ 38]*x11*x22*x32*x41    
        +coeff[ 39]*x11    *x32*x43    
        +coeff[ 40]*x11*x22*x32    *x51
        +coeff[ 41]*x11    *x31*x43*x51
        +coeff[ 42]*x11    *x32    *x53
        +coeff[ 43]*x11    *x31    *x54
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 44]*x12*x24            
        +coeff[ 45]*x12            *x54
        +coeff[ 46]*x13*x22*x31        
        +coeff[ 47]    *x22*x32*x42*x51
        +coeff[ 48]    *x22    *x44*x51
        +coeff[ 49]    *x21    *x43*x53
        +coeff[ 50]    *x23        *x54
        +coeff[ 51]*x11*x22*x34        
        +coeff[ 52]*x11*x22*x33    *x51
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 53]*x11    *x31*x44*x51
        +coeff[ 54]*x11*x21*x32*x41*x52
        +coeff[ 55]*x11    *x33*x41*x52
        +coeff[ 56]*x11*x22*x31    *x53
        +coeff[ 57]*x11    *x32*x41*x53
        +coeff[ 58]*x12*x21*x31*x43    
        +coeff[ 59]*x12*x21    *x44    
        +coeff[ 60]*x12        *x44*x51
        +coeff[ 61]*x12*x21*x32    *x52
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 62]*x13*x24            
        +coeff[ 63]*x13*x22*x32        
        +coeff[ 64]*x13*x22    *x42    
        +coeff[ 65]*x13*x22*x31    *x51
        +coeff[ 66]*x13    *x32    *x52
        +coeff[ 67]    *x21            
        +coeff[ 68]        *x31        
        +coeff[ 69]*x11                
        +coeff[ 70]    *x21*x31        
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 71]*x11    *x31        
        +coeff[ 72]*x11        *x41    
        +coeff[ 73]*x11            *x51
        +coeff[ 74]        *x33        
        +coeff[ 75]    *x21    *x42    
        +coeff[ 76]    *x21*x31    *x51
        +coeff[ 77]        *x31*x41*x51
        +coeff[ 78]    *x21        *x52
        +coeff[ 79]*x11*x22            
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 80]*x13                
        +coeff[ 81]    *x23*x31        
        +coeff[ 82]    *x22*x32        
        +coeff[ 83]    *x21*x33        
        +coeff[ 84]    *x22        *x52
        +coeff[ 85]    *x21*x31    *x52
        +coeff[ 86]        *x32    *x52
        +coeff[ 87]            *x41*x53
        +coeff[ 88]*x11    *x33        
    ;
    v_l_e_q2ex_1_900                              =v_l_e_q2ex_1_900                              
        +coeff[ 89]*x11*x22    *x41    
        ;

    return v_l_e_q2ex_1_900                              ;
}
float x_e_q2ex_1_800                              (float *x,int m){
    int ncoeff= 13;
    float avdat=  0.9163215E-05;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
        -0.62815652E-05,-0.32543635E-02, 0.16530289E+00, 0.80996463E-02,
         0.33385484E-03,-0.12135811E-02,-0.29665218E-02,-0.22142748E-02,
        -0.12662445E-04,-0.13883132E-02,-0.38409111E-03,-0.17424125E-02,
        -0.15548770E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_800                              =v_x_e_q2ex_1_800                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_800                              ;
}
float t_e_q2ex_1_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1974520E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18029532E-04,-0.24547440E-02, 0.51955000E-01, 0.24696372E-02,
         0.17773190E-03,-0.18965926E-03, 0.25881117E-03, 0.48506081E-04,
        -0.19601831E-03,-0.51948521E-03,-0.68026573E-04,-0.67497778E-04,
        -0.15910009E-02,-0.12023585E-02,-0.56644054E-04,-0.60785322E-04,
         0.84025269E-04, 0.15017159E-03, 0.15574778E-03,-0.10027933E-02,
         0.14681267E-04,-0.32263673E-04, 0.64243170E-04,-0.51874452E-03,
        -0.11678196E-02,-0.10596558E-05,-0.18813378E-04,-0.11045283E-04,
        -0.86630062E-04,-0.24908295E-04,-0.55684864E-04, 0.22242597E-04,
         0.27152988E-04, 0.25511410E-04, 0.23253016E-04, 0.28558967E-04,
         0.43416607E-04, 0.82189319E-04,-0.22979448E-04,-0.28933619E-04,
        -0.30845861E-05,-0.29676278E-05,-0.54686184E-05, 0.28201655E-05,
         0.37124507E-05,-0.10227408E-04,-0.56935555E-05, 0.28978454E-05,
        -0.51218940E-05,-0.83967216E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_800                              =v_t_e_q2ex_1_800                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_800                              =v_t_e_q2ex_1_800                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23*x32    *x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x21*x32*x42    
        +coeff[ 25]    *x21    *x41    
    ;
    v_t_e_q2ex_1_800                              =v_t_e_q2ex_1_800                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x11*x22*x31*x41    
        +coeff[ 29]*x11    *x33*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]*x13*x21        *x51
        +coeff[ 32]    *x23*x31    *x51
        +coeff[ 33]    *x22*x31*x41*x51
        +coeff[ 34]*x11*x22*x33        
    ;
    v_t_e_q2ex_1_800                              =v_t_e_q2ex_1_800                              
        +coeff[ 35]    *x23    *x43    
        +coeff[ 36]*x13*x21*x31    *x51
        +coeff[ 37]    *x23*x31*x41*x51
        +coeff[ 38]*x11*x22*x31    *x52
        +coeff[ 39]*x11*x21*x31    *x53
        +coeff[ 40]    *x22            
        +coeff[ 41]*x12*x21            
        +coeff[ 42]*x12        *x41    
        +coeff[ 43]            *x43    
    ;
    v_t_e_q2ex_1_800                              =v_t_e_q2ex_1_800                              
        +coeff[ 44]*x11    *x31    *x51
        +coeff[ 45]    *x21*x31    *x51
        +coeff[ 46]        *x31*x41*x51
        +coeff[ 47]    *x23*x31        
        +coeff[ 48]    *x22*x31*x41    
        +coeff[ 49]    *x21*x32*x41    
        ;

    return v_t_e_q2ex_1_800                              ;
}
float y_e_q2ex_1_800                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.9888210E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.10292010E-02, 0.21533073E+00, 0.11989029E+00,-0.19794009E-02,
        -0.12504703E-02,-0.60213107E-03,-0.38732251E-03,-0.31588893E-03,
        -0.23898493E-03,-0.61698433E-04,-0.44850178E-04, 0.38423626E-04,
         0.11136618E-04,-0.51119085E-03,-0.24390209E-03,-0.30088306E-05,
         0.51845622E-04, 0.91411293E-05, 0.83233150E-04,-0.48472377E-03,
        -0.51099650E-03, 0.42369127E-04,-0.14025449E-03,-0.48126218E-04,
         0.18984258E-04,-0.47157306E-03, 0.20828482E-04,-0.17631748E-03,
        -0.66305605E-04,-0.98529934E-04,-0.40397797E-04,-0.15558868E-03,
         0.99043376E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_800                              =v_y_e_q2ex_1_800                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q2ex_1_800                              =v_y_e_q2ex_1_800                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]            *x43*x52
        +coeff[ 22]*x11*x23    *x41    
        +coeff[ 23]    *x22*x31*x44    
        +coeff[ 24]*x11*x21    *x42    
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q2ex_1_800                              =v_y_e_q2ex_1_800                              
        +coeff[ 26]        *x32*x42*x51
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11*x21*x32*x41    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]        *x32*x43*x51
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x24    *x41*x51
        ;

    return v_y_e_q2ex_1_800                              ;
}
float p_e_q2ex_1_800                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.3326900E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.37367023E-04,-0.16857963E-01,-0.16749918E-01, 0.16437666E-02,
         0.28381189E-02,-0.13812288E-02,-0.77552430E-03,-0.14266659E-02,
        -0.85380278E-03,-0.63999000E-05,-0.78860554E-03,-0.14924874E-03,
        -0.14954567E-03, 0.10725198E-05, 0.38536356E-04,-0.10172557E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_800                              =v_p_e_q2ex_1_800                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_800                              ;
}
float l_e_q2ex_1_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2644459E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26597085E-02,-0.44829184E-02,-0.94310654E-03,-0.41156425E-02,
        -0.53957598E-02, 0.31545252E-03,-0.76928016E-04,-0.98262579E-04,
         0.16074310E-03, 0.93439950E-04, 0.50925848E-03, 0.92650822E-03,
        -0.41019041E-03, 0.32761044E-03, 0.15113929E-03,-0.44371400E-03,
        -0.72031235E-03, 0.46699491E-03,-0.28233870E-02, 0.15885566E-02,
         0.60650904E-03,-0.33717597E-03, 0.45382567E-04, 0.26098144E-03,
         0.96742227E-03, 0.53976622E-03,-0.42567335E-03,-0.49273937E-03,
         0.12878985E-02, 0.86335733E-03, 0.31165178E-02,-0.84516424E-03,
         0.16126885E-03,-0.42726053E-02,-0.15554789E-02, 0.78051822E-03,
         0.62293536E-03,-0.60040446E-03,-0.11971812E-02,-0.14469005E-02,
         0.38256897E-02, 0.12916449E-02,-0.69240504E-03, 0.10308138E-02,
         0.67593891E-03,-0.26440562E-02,-0.18336443E-02, 0.28268297E-02,
         0.49907886E-02, 0.81374304E-03,-0.46615818E-03,-0.27952387E-02,
        -0.64306907E-04,-0.13584673E-03, 0.17690193E-03, 0.13988388E-03,
        -0.32614167E-04, 0.13049376E-03,-0.31330078E-03, 0.14764324E-03,
        -0.15173969E-03, 0.59584284E-03,-0.44738928E-04, 0.15191328E-03,
         0.10112835E-03, 0.18463630E-03,-0.20703083E-03,-0.16501766E-03,
        -0.45241686E-03, 0.17261400E-03, 0.56235975E-03, 0.15560385E-04,
         0.32723407E-03,-0.56733855E-03, 0.75098185E-03,-0.18942311E-03,
        -0.21689893E-03, 0.14426773E-03,-0.17158709E-03, 0.25097639E-03,
        -0.21318594E-03, 0.19806130E-03, 0.19498510E-03,-0.61462243E-03,
         0.85100828E-03,-0.59138192E-03, 0.88318827E-03,-0.10591901E-02,
        -0.39701659E-03,-0.10071450E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21*x32        
        +coeff[  6]    *x21        *x51
        +coeff[  7]*x12                
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[  8]            *x42*x51
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]    *x24            
        +coeff[ 11]    *x22*x31*x41    
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]*x11*x21*x31    *x51
        +coeff[ 14]*x11    *x32    *x51
        +coeff[ 15]*x12*x21    *x41    
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 17]    *x22    *x43    
        +coeff[ 18]    *x22*x31*x41*x51
        +coeff[ 19]        *x32*x42*x51
        +coeff[ 20]        *x31*x43*x51
        +coeff[ 21]    *x22        *x53
        +coeff[ 22]*x11*x21*x32*x41    
        +coeff[ 23]*x11    *x33    *x51
        +coeff[ 24]*x11    *x31*x41*x52
        +coeff[ 25]*x12*x22        *x51
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 26]*x12    *x32    *x51
        +coeff[ 27]*x13    *x31*x41    
        +coeff[ 28]    *x22*x32*x42    
        +coeff[ 29]    *x24*x31    *x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]        *x33*x42*x51
        +coeff[ 32]    *x22*x31*x41*x52
        +coeff[ 33]    *x21*x31*x41*x53
        +coeff[ 34]*x11*x24    *x41    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 35]*x11*x22    *x43    
        +coeff[ 36]*x11*x24        *x51
        +coeff[ 37]*x11*x22        *x53
        +coeff[ 38]*x12    *x32*x42    
        +coeff[ 39]*x13*x21    *x41*x51
        +coeff[ 40]    *x24*x31*x41*x51
        +coeff[ 41]    *x21*x34*x41*x51
        +coeff[ 42]    *x23    *x43*x51
        +coeff[ 43]*x11*x21*x31*x44    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 44]*x12*x21*x33    *x51
        +coeff[ 45]    *x23*x33*x41*x51
        +coeff[ 46]        *x34*x43*x51
        +coeff[ 47]        *x34*x42*x52
        +coeff[ 48]    *x21*x33*x41*x53
        +coeff[ 49]    *x22*x31*x41*x54
        +coeff[ 50]    *x22    *x42*x54
        +coeff[ 51]        *x32*x42*x54
        +coeff[ 52]            *x41    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 53]    *x21*x31        
        +coeff[ 54]    *x21    *x41    
        +coeff[ 55]            *x41*x51
        +coeff[ 56]                *x52
        +coeff[ 57]*x11*x21            
        +coeff[ 58]*x11    *x31        
        +coeff[ 59]*x11            *x51
        +coeff[ 60]    *x21*x32        
        +coeff[ 61]        *x31*x41*x51
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 62]        *x31    *x52
        +coeff[ 63]            *x41*x52
        +coeff[ 64]*x12    *x31        
        +coeff[ 65]    *x23*x31        
        +coeff[ 66]    *x22        *x52
        +coeff[ 67]    *x21    *x41*x52
        +coeff[ 68]        *x31*x41*x52
        +coeff[ 69]                *x54
        +coeff[ 70]*x11*x22    *x41    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 71]*x11    *x32*x41    
        +coeff[ 72]*x11    *x31*x42    
        +coeff[ 73]*x11*x22        *x51
        +coeff[ 74]*x11*x21    *x41*x51
        +coeff[ 75]*x11    *x31*x41*x51
        +coeff[ 76]*x11*x21        *x52
        +coeff[ 77]*x11    *x31    *x52
        +coeff[ 78]*x12*x22            
        +coeff[ 79]*x12        *x42    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 80]*x12        *x41*x51
        +coeff[ 81]*x13    *x31        
        +coeff[ 82]    *x24*x31        
        +coeff[ 83]    *x23*x32        
        +coeff[ 84]    *x21*x34        
        +coeff[ 85]    *x23*x31*x41    
        +coeff[ 86]    *x21*x33*x41    
        +coeff[ 87]        *x33*x42    
        +coeff[ 88]    *x21*x31*x43    
    ;
    v_l_e_q2ex_1_800                              =v_l_e_q2ex_1_800                              
        +coeff[ 89]        *x32*x43    
        ;

    return v_l_e_q2ex_1_800                              ;
}
float x_e_q2ex_1_700                              (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.8026705E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.80819451E-03, 0.16546129E+00, 0.80928393E-02, 0.68724862E-05,
        -0.32580071E-02, 0.33516053E-03,-0.14238107E-02,-0.37253615E-02,
        -0.29168653E-02, 0.88129054E-05, 0.17899522E-03,-0.14100048E-02,
        -0.37911450E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_q2ex_1_700                              =v_x_e_q2ex_1_700                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        ;

    return v_x_e_q2ex_1_700                              ;
}
float t_e_q2ex_1_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2511970E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.25118972E-03,-0.24558925E-02, 0.51994797E-01, 0.24714218E-02,
         0.17381509E-03,-0.17084538E-03, 0.27453460E-03, 0.70783077E-04,
        -0.19229620E-03,-0.54225500E-03,-0.52146414E-04,-0.71459661E-04,
        -0.16288222E-02,-0.12476867E-02,-0.64090535E-04,-0.56243367E-04,
         0.85062522E-04, 0.16918749E-03, 0.12905219E-03,-0.10087615E-02,
        -0.27487682E-04,-0.11072585E-04, 0.55225162E-04,-0.52455789E-03,
        -0.11859682E-02, 0.84309642E-06,-0.50488047E-05,-0.90723261E-05,
        -0.10164210E-04, 0.10875769E-04,-0.23970793E-04, 0.31562875E-04,
        -0.82227140E-04,-0.65761655E-04,-0.17015516E-04, 0.22673581E-04,
        -0.18951407E-04, 0.29725261E-05, 0.89770892E-05,-0.12527621E-04,
         0.30949311E-05, 0.31061254E-05,-0.59699805E-05,-0.42617976E-05,
         0.12326612E-04,-0.42728302E-05, 0.11971612E-04, 0.65178328E-05,
         0.53764743E-05, 0.78674939E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_700                              =v_t_e_q2ex_1_700                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_700                              =v_t_e_q2ex_1_700                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]*x11            *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x21*x32*x42    
        +coeff[ 25]    *x22            
    ;
    v_t_e_q2ex_1_700                              =v_t_e_q2ex_1_700                              
        +coeff[ 26]        *x31*x41    
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]*x11    *x32    *x51
        +coeff[ 30]*x12*x23            
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x12*x22        *x51
    ;
    v_t_e_q2ex_1_700                              =v_t_e_q2ex_1_700                              
        +coeff[ 35]    *x22        *x53
        +coeff[ 36]*x12*x22        *x52
        +coeff[ 37]*x12                
        +coeff[ 38]*x12*x21            
        +coeff[ 39]    *x22        *x51
        +coeff[ 40]        *x32    *x51
        +coeff[ 41]    *x21    *x41*x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]    *x23*x31        
    ;
    v_t_e_q2ex_1_700                              =v_t_e_q2ex_1_700                              
        +coeff[ 44]    *x22    *x42    
        +coeff[ 45]    *x21    *x43    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]*x11*x21        *x52
        +coeff[ 49]    *x21        *x53
        ;

    return v_t_e_q2ex_1_700                              ;
}
float y_e_q2ex_1_700                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.4335470E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.50046365E-03, 0.21522410E+00, 0.11985659E+00,-0.19878014E-02,
        -0.12614006E-02,-0.67436113E-03,-0.51460991E-03,-0.36049681E-03,
        -0.24209895E-03,-0.77696386E-04,-0.51021496E-04, 0.49162169E-04,
         0.32840384E-04,-0.34635534E-03,-0.42459366E-03,-0.23936915E-03,
        -0.33043125E-04, 0.43983178E-04, 0.10433541E-03,-0.38572671E-03,
        -0.46106376E-04,-0.68830466E-03,-0.22573952E-03,-0.26793787E-03,
        -0.48716140E-06,-0.13137208E-04, 0.22980827E-04, 0.53334938E-04,
         0.84189924E-04,-0.23828667E-03,-0.43559296E-04, 0.35330038E-04,
        -0.11397456E-03,-0.79004225E-04,-0.16160167E-04, 0.48179274E-04,
         0.35912610E-04, 0.31212632E-04, 0.83120285E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_700                              =v_y_e_q2ex_1_700                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x24*x32*x41    
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_1_700                              =v_y_e_q2ex_1_700                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]    *x24*x31*x42    
        +coeff[ 22]    *x24*x33        
        +coeff[ 23]    *x24    *x45    
        +coeff[ 24]    *x21    *x41    
        +coeff[ 25]*x12        *x41    
    ;
    v_y_e_q2ex_1_700                              =v_y_e_q2ex_1_700                              
        +coeff[ 26]            *x41*x52
        +coeff[ 27]        *x31*x42*x51
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]*x11    *x32*x42    
        +coeff[ 32]*x11*x23    *x41    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x11        *x45    
    ;
    v_y_e_q2ex_1_700                              =v_y_e_q2ex_1_700                              
        +coeff[ 35]            *x45*x51
        +coeff[ 36]    *x23*x31*x42    
        +coeff[ 37]    *x21*x31*x43*x51
        +coeff[ 38]    *x22*x32*x41*x51
        ;

    return v_y_e_q2ex_1_700                              ;
}
float p_e_q2ex_1_700                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.6235554E-05;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.12629816E-04,-0.16878258E-01,-0.16784437E-01, 0.16448918E-02,
         0.28389301E-02,-0.13834834E-02,-0.77719719E-03,-0.14244275E-02,
        -0.85191388E-03,-0.17098115E-04,-0.78052876E-03,-0.15035264E-03,
        -0.14951959E-03,-0.25801828E-05,-0.99543933E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_700                              =v_p_e_q2ex_1_700                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_700                              ;
}
float l_e_q2ex_1_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2649341E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25469600E-02,-0.39308397E-02,-0.10218571E-02,-0.43200683E-02,
        -0.51830499E-02,-0.25802053E-03, 0.71741582E-03,-0.22021207E-03,
         0.21457284E-03, 0.62223664E-03, 0.37230601E-03, 0.44297255E-03,
        -0.41492781E-03, 0.66554424E-03,-0.10182319E-02,-0.98044192E-03,
         0.61979733E-03,-0.21532539E-02, 0.48665958E-03, 0.30327696E-03,
         0.39347459E-03, 0.69754309E-03,-0.73128683E-03, 0.71336923E-03,
        -0.19950424E-02,-0.14014114E-03,-0.62435266E-03,-0.64408209E-03,
        -0.10677729E-02, 0.66492724E-03,-0.26164690E-02, 0.17792983E-03,
        -0.78688335E-03, 0.85748929E-04,-0.20039205E-02,-0.15917753E-02,
        -0.80402580E-03, 0.14417680E-02, 0.79353957E-03, 0.18703633E-02,
        -0.37259315E-02,-0.64486274E-02, 0.23981321E-02, 0.39530106E-03,
         0.27338136E-02, 0.10220566E-02,-0.29111523E-04, 0.18441894E-03,
        -0.37098312E-05, 0.81708124E-04, 0.35138302E-04, 0.35242375E-03,
        -0.57585252E-03, 0.80908427E-03, 0.51148771E-03,-0.74226153E-03,
        -0.49043971E-03, 0.19634324E-03, 0.19463689E-03,-0.43175099E-03,
        -0.41817976E-03, 0.11503950E-03, 0.35117156E-03,-0.53795549E-03,
        -0.89744957E-04, 0.36932289E-03, 0.47863380E-03, 0.33990468E-03,
         0.11594255E-03, 0.94585026E-04,-0.62019099E-03, 0.40382412E-03,
        -0.55803073E-03,-0.27593199E-03, 0.86885743E-03,-0.26974562E-03,
         0.48340874E-03,-0.10721288E-02,-0.29954035E-03,-0.12864602E-02,
        -0.65292208E-03, 0.11326837E-02,-0.25158448E-03,-0.36743926E-03,
        -0.31878930E-03, 0.39049951E-03, 0.16651757E-03,-0.22617792E-03,
         0.81156415E-03, 0.32840850E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]*x11*x24    *x41    
        +coeff[  7]    *x21        *x52
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[  8]            *x43*x51
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]*x11    *x32*x41    
        +coeff[ 11]*x12        *x42    
        +coeff[ 12]*x12*x21        *x51
        +coeff[ 13]*x12            *x52
        +coeff[ 14]    *x21*x31*x42*x51
        +coeff[ 15]*x11*x24            
        +coeff[ 16]*x11        *x44    
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 17]*x11    *x32*x41*x51
        +coeff[ 18]*x11        *x43*x51
        +coeff[ 19]*x13*x22            
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]    *x21*x32*x41*x52
        +coeff[ 22]    *x21    *x43*x52
        +coeff[ 23]    *x21*x31    *x54
        +coeff[ 24]*x11    *x32*x43    
        +coeff[ 25]*x11    *x31*x44    
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 26]*x12*x24            
        +coeff[ 27]*x12*x22    *x41*x51
        +coeff[ 28]*x12            *x54
        +coeff[ 29]    *x23*x33    *x51
        +coeff[ 30]    *x23*x31*x41*x52
        +coeff[ 31]    *x21*x31*x43*x52
        +coeff[ 32]        *x33    *x54
        +coeff[ 33]    *x22    *x41*x54
        +coeff[ 34]*x11*x23*x31*x41*x51
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 35]*x11*x23    *x42*x51
        +coeff[ 36]*x12*x21    *x43*x51
        +coeff[ 37]*x12*x22*x31    *x52
        +coeff[ 38]*x13        *x44    
        +coeff[ 39]*x13    *x32*x41*x51
        +coeff[ 40]    *x23*x32*x41*x52
        +coeff[ 41]    *x23*x31*x42*x52
        +coeff[ 42]    *x21*x31*x44*x52
        +coeff[ 43]    *x24        *x54
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 44]    *x21*x32*x41*x54
        +coeff[ 45]            *x44*x54
        +coeff[ 46]    *x21            
        +coeff[ 47]                *x51
        +coeff[ 48]*x11                
        +coeff[ 49]    *x22*x31        
        +coeff[ 50]        *x33        
        +coeff[ 51]    *x21    *x42    
        +coeff[ 52]        *x32    *x51
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 53]        *x31*x41*x51
        +coeff[ 54]*x11*x22            
        +coeff[ 55]*x11        *x42    
        +coeff[ 56]*x11*x21        *x51
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]    *x21*x31*x42    
        +coeff[ 59]            *x44    
        +coeff[ 60]            *x42*x52
        +coeff[ 61]    *x21        *x53
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 62]*x11*x21*x31*x41    
        +coeff[ 63]*x11    *x31*x42    
        +coeff[ 64]*x11*x22        *x51
        +coeff[ 65]*x11    *x32    *x51
        +coeff[ 66]*x11    *x31*x41*x51
        +coeff[ 67]*x11        *x42*x51
        +coeff[ 68]*x12    *x32        
        +coeff[ 69]*x13*x21            
        +coeff[ 70]    *x24*x31        
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 71]    *x22*x33        
        +coeff[ 72]    *x21*x33*x41    
        +coeff[ 73]    *x22    *x43    
        +coeff[ 74]    *x21*x31*x43    
        +coeff[ 75]*x13            *x51
        +coeff[ 76]        *x34    *x51
        +coeff[ 77]    *x22*x31*x41*x51
        +coeff[ 78]        *x32*x42*x51
        +coeff[ 79]        *x31*x43*x51
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 80]            *x44*x51
        +coeff[ 81]    *x21*x31*x41*x52
        +coeff[ 82]        *x32*x41*x52
        +coeff[ 83]        *x31*x42*x52
        +coeff[ 84]    *x22        *x53
        +coeff[ 85]            *x42*x53
        +coeff[ 86]        *x31    *x54
        +coeff[ 87]            *x41*x54
        +coeff[ 88]*x11*x23        *x51
    ;
    v_l_e_q2ex_1_700                              =v_l_e_q2ex_1_700                              
        +coeff[ 89]*x11*x22    *x41*x51
        ;

    return v_l_e_q2ex_1_700                              ;
}
float x_e_q2ex_1_600                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.1467640E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.11535329E-03, 0.16548859E+00, 0.80911517E-02, 0.76277261E-05,
        -0.32560828E-02, 0.33292521E-03,-0.12148805E-02,-0.29509163E-02,
        -0.21877603E-02,-0.33461396E-06,-0.13838579E-02,-0.38201819E-03,
        -0.17903041E-02,-0.15991759E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_q2ex_1_600                              =v_x_e_q2ex_1_600                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_600                              ;
}
float t_e_q2ex_1_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4605978E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.36425201E-04,-0.24587291E-02, 0.52096777E-01, 0.24832995E-02,
         0.17869558E-03,-0.20612399E-03, 0.24791333E-03, 0.34654484E-04,
        -0.19783711E-03,-0.46643350E-03,-0.96734533E-04,-0.64357257E-04,
        -0.15817948E-02,-0.11917523E-02,-0.98772044E-03,-0.84844105E-04,
        -0.58999925E-04, 0.63626219E-04, 0.10999436E-03, 0.13993695E-03,
        -0.11240102E-02, 0.61774968E-04,-0.28133009E-04,-0.51189552E-03,
         0.48084381E-04, 0.10539642E-04,-0.10524299E-04,-0.27406813E-04,
        -0.63951279E-05,-0.25969466E-04, 0.13690994E-04, 0.81212384E-05,
        -0.51650819E-04, 0.66062581E-04, 0.72043040E-04,-0.72895182E-06,
         0.20124899E-05, 0.44038538E-05, 0.32814232E-05,-0.47658564E-05,
         0.13802037E-05,-0.34570237E-05, 0.29866767E-05, 0.34976838E-05,
         0.99954323E-05,-0.10563219E-04, 0.11210582E-04,-0.15414962E-04,
        -0.87455774E-05,-0.19558254E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_600                              =v_t_e_q2ex_1_600                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q2ex_1_600                              =v_t_e_q2ex_1_600                              
        +coeff[ 17]    *x23        *x51
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x21*x32    *x53
        +coeff[ 25]    *x21*x31        
    ;
    v_t_e_q2ex_1_600                              =v_t_e_q2ex_1_600                              
        +coeff[ 26]*x11            *x52
        +coeff[ 27]    *x21*x33        
        +coeff[ 28]    *x22        *x52
        +coeff[ 29]*x11*x22    *x42    
        +coeff[ 30]*x11*x22*x31    *x51
        +coeff[ 31]    *x22        *x53
        +coeff[ 32]*x11*x22*x31*x42    
        +coeff[ 33]    *x23*x31*x41*x51
        +coeff[ 34]    *x21*x31*x43*x51
    ;
    v_t_e_q2ex_1_600                              =v_t_e_q2ex_1_600                              
        +coeff[ 35]*x11    *x31        
        +coeff[ 36]            *x42    
        +coeff[ 37]*x13                
        +coeff[ 38]*x12    *x31        
        +coeff[ 39]*x11*x21*x31        
        +coeff[ 40]    *x22    *x41    
        +coeff[ 41]    *x21    *x41*x51
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]            *x41*x52
    ;
    v_t_e_q2ex_1_600                              =v_t_e_q2ex_1_600                              
        +coeff[ 44]*x11*x22*x31        
        +coeff[ 45]*x11*x21*x32        
        +coeff[ 46]    *x23    *x41    
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]*x11    *x32*x41    
        +coeff[ 49]    *x21*x32*x41    
        ;

    return v_t_e_q2ex_1_600                              ;
}
float y_e_q2ex_1_600                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.8884104E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.98183763E-03, 0.21510123E+00, 0.11981618E+00,-0.19727654E-02,
        -0.12524121E-02,-0.62843657E-03,-0.41723382E-03,-0.34021478E-03,
        -0.22277916E-03,-0.66792956E-04,-0.19786325E-04,-0.30366988E-04,
         0.33402626E-04,-0.49824471E-03,-0.40967120E-06,-0.15234636E-03,
        -0.55935550E-04, 0.48124472E-04, 0.96174001E-04, 0.86409455E-04,
        -0.12774806E-03,-0.41852801E-03,-0.53278520E-03,-0.15559663E-03,
        -0.12669363E-03, 0.74007685E-05, 0.23509392E-04,-0.38389993E-03,
        -0.24329000E-04,-0.94943978E-04,-0.19193041E-04, 0.22538528E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_600                              =v_y_e_q2ex_1_600                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x24*x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_1_600                              =v_y_e_q2ex_1_600                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x24    *x41    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]        *x31*x41    
    ;
    v_y_e_q2ex_1_600                              =v_y_e_q2ex_1_600                              
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]    *x21    *x42*x52
        +coeff[ 31]            *x45*x51
        ;

    return v_y_e_q2ex_1_600                              ;
}
float p_e_q2ex_1_600                              (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.5309311E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.60842744E-04,-0.16912291E-01,-0.16835347E-01, 0.16465165E-02,
         0.28405415E-02,-0.13828872E-02,-0.78097195E-03,-0.14220960E-02,
        -0.85304852E-03,-0.98672454E-05,-0.78153616E-03,-0.14772083E-03,
        -0.14694134E-03, 0.14715861E-05, 0.36826321E-04,-0.10120334E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_600                              =v_p_e_q2ex_1_600                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_600                              ;
}
float l_e_q2ex_1_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2643298E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26380182E-02,-0.37517678E-02,-0.87480847E-03,-0.45663710E-02,
        -0.55727265E-02,-0.16427191E-03,-0.99198613E-03,-0.19778211E-03,
        -0.47611036E-04,-0.21235939E-03, 0.19081726E-03, 0.33773860E-03,
        -0.27809588E-06, 0.43084656E-03, 0.34138976E-03,-0.84027514E-03,
         0.10765750E-03,-0.42027741E-03, 0.66872081E-03,-0.58127096E-03,
         0.11705668E-03,-0.10556448E-02,-0.23485209E-03, 0.25272896E-03,
         0.10209596E-02,-0.39230497E-03, 0.11500005E-05, 0.23338929E-03,
        -0.81991486E-03,-0.60385710E-03, 0.15357189E-02, 0.13066694E-02,
         0.44827818E-03,-0.17176387E-03,-0.14093897E-02,-0.22655571E-03,
         0.56117802E-03,-0.13678479E-03, 0.10693168E-02, 0.49067074E-02,
        -0.14850990E-02, 0.48956682E-03,-0.11619835E-02, 0.12821768E-02,
         0.14391992E-02,-0.70148776E-03, 0.17746050E-02, 0.92426577E-03,
        -0.10846363E-02,-0.37777494E-03, 0.11145418E-02, 0.68870030E-03,
         0.49466745E-03,-0.39769590E-03,-0.11614506E-02, 0.15554274E-03,
         0.34873662E-03, 0.36550339E-03,-0.15871037E-02,-0.12071865E-02,
        -0.25725199E-02, 0.20563358E-02,-0.27394686E-02, 0.16027562E-02,
         0.13132786E-02, 0.85574540E-03,-0.67230710E-03, 0.56306069E-03,
         0.14220251E-02, 0.15940546E-02,-0.46099734E-03, 0.15202966E-02,
        -0.25618321E-02,-0.13519509E-02, 0.11274787E-03,-0.54690583E-04,
         0.27221366E-03, 0.10109676E-03,-0.26763676E-03,-0.41886167E-04,
        -0.19419551E-03,-0.12617618E-03,-0.71839802E-03, 0.19745973E-03,
        -0.10229235E-03,-0.26037375E-03,-0.13697741E-03,-0.19419403E-03,
        -0.31142402E-03,-0.17321252E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x33        
        +coeff[  6]    *x22*x32        
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]*x11        *x41*x51
        +coeff[ 13]    *x21*x33        
        +coeff[ 14]    *x21*x32*x41    
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x22        *x52
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 17]    *x21    *x41*x52
        +coeff[ 18]    *x21        *x53
        +coeff[ 19]*x11    *x32*x41    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11*x21    *x41*x51
        +coeff[ 22]*x12*x21    *x41    
        +coeff[ 23]*x13*x21            
        +coeff[ 24]        *x31*x43*x51
        +coeff[ 25]    *x23        *x52
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]            *x42*x53
        +coeff[ 28]*x11*x22*x32        
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11    *x31*x43    
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x11    *x31    *x53
        +coeff[ 34]*x12*x21*x31*x41    
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]*x12        *x42*x51
        +coeff[ 37]*x13    *x31*x41    
        +coeff[ 38]    *x22*x33*x41    
        +coeff[ 39]    *x22*x32*x42    
        +coeff[ 40]        *x34*x42    
        +coeff[ 41]*x13        *x41*x51
        +coeff[ 42]    *x23    *x41*x52
        +coeff[ 43]    *x22    *x42*x52
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 44]    *x21*x31*x41*x53
        +coeff[ 45]    *x22        *x54
        +coeff[ 46]    *x21    *x41*x54
        +coeff[ 47]        *x31*x41*x54
        +coeff[ 48]*x11*x22*x32    *x51
        +coeff[ 49]*x11*x21*x31*x42*x51
        +coeff[ 50]*x11*x21    *x43*x51
        +coeff[ 51]*x11*x22        *x53
        +coeff[ 52]*x12    *x32    *x52
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 53]*x12*x21        *x53
        +coeff[ 54]    *x24    *x42*x51
        +coeff[ 55]    *x22*x32*x42*x51
        +coeff[ 56]    *x23*x32    *x52
        +coeff[ 57]*x13        *x41*x52
        +coeff[ 58]    *x22*x31*x41*x53
        +coeff[ 59]    *x21*x32    *x54
        +coeff[ 60]*x11*x23*x31*x42    
        +coeff[ 61]*x11*x21*x33*x42    
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 62]*x11*x23*x31*x41*x51
        +coeff[ 63]*x11*x21*x33*x41*x51
        +coeff[ 64]*x11*x21*x31*x43*x51
        +coeff[ 65]*x11*x21*x32    *x53
        +coeff[ 66]*x12*x23*x32        
        +coeff[ 67]*x12    *x31    *x54
        +coeff[ 68]    *x24*x34        
        +coeff[ 69]    *x24*x31*x43    
        +coeff[ 70]    *x23*x33*x41*x51
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 71]    *x21*x34*x42*x51
        +coeff[ 72]        *x33*x43*x52
        +coeff[ 73]        *x32*x42*x54
        +coeff[ 74]    *x21    *x41    
        +coeff[ 75]*x11            *x51
        +coeff[ 76]    *x21*x32        
        +coeff[ 77]    *x22    *x41    
        +coeff[ 78]        *x32*x41    
        +coeff[ 79]    *x21    *x42    
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 80]        *x31*x42    
        +coeff[ 81]        *x31    *x52
        +coeff[ 82]*x11    *x31*x41    
        +coeff[ 83]*x11        *x42    
        +coeff[ 84]*x11*x21        *x51
        +coeff[ 85]*x11    *x31    *x51
        +coeff[ 86]*x11            *x52
        +coeff[ 87]*x12*x21            
        +coeff[ 88]    *x24            
    ;
    v_l_e_q2ex_1_600                              =v_l_e_q2ex_1_600                              
        +coeff[ 89]    *x23*x31        
        ;

    return v_l_e_q2ex_1_600                              ;
}
float x_e_q2ex_1_500                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1700688E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.16938369E-02,-0.32497409E-02, 0.16570494E+00, 0.80872206E-02,
         0.33781989E-03,-0.13711225E-02,-0.37270978E-02,-0.29146385E-02,
         0.45971210E-05,-0.12980177E-02,-0.38093131E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_500                              =v_x_e_q2ex_1_500                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2ex_1_500                              ;
}
float t_e_q2ex_1_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5309770E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.52872754E-03,-0.24544119E-02, 0.52191116E-01, 0.24708521E-02,
         0.17701105E-03,-0.18557769E-03, 0.25464370E-03, 0.62621068E-04,
        -0.19643646E-03,-0.49404497E-03,-0.70943148E-04,-0.72745803E-04,
        -0.16048199E-02,-0.12335586E-02,-0.69457164E-04,-0.53932185E-04,
         0.71982693E-04, 0.19071894E-03, 0.15106413E-03,-0.11967570E-02,
        -0.10198751E-02, 0.18363595E-04,-0.30153913E-04,-0.11729006E-04,
         0.60178339E-04,-0.50743815E-03,-0.73565020E-05, 0.93502977E-05,
         0.13078673E-05,-0.23943352E-04, 0.14852425E-04, 0.13873779E-04,
         0.31272775E-04,-0.87809429E-04,-0.74659802E-04, 0.97490438E-05,
        -0.15651813E-04,-0.49574624E-05, 0.24966324E-04, 0.13739902E-04,
        -0.21594421E-05, 0.37167476E-04,-0.32524815E-04,-0.22525204E-04,
        -0.31297238E-05,-0.12268906E-04, 0.68347190E-05, 0.40586015E-05,
        -0.27824999E-05,-0.35572914E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_500                              =v_t_e_q2ex_1_500                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_500                              =v_t_e_q2ex_1_500                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q2ex_1_500                              =v_t_e_q2ex_1_500                              
        +coeff[ 26]    *x22    *x41    
        +coeff[ 27]    *x22        *x51
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]*x11*x22        *x51
        +coeff[ 31]*x13*x22            
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]*x11*x22*x31*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q2ex_1_500                              =v_t_e_q2ex_1_500                              
        +coeff[ 35]*x11*x23        *x51
        +coeff[ 36]    *x22*x32    *x51
        +coeff[ 37]*x12*x23*x31        
        +coeff[ 38]*x12*x21*x33        
        +coeff[ 39]*x11*x22*x33        
        +coeff[ 40]*x13*x21    *x41*x51
        +coeff[ 41]*x12*x22    *x41*x51
        +coeff[ 42]*x11*x21*x32*x41*x51
        +coeff[ 43]    *x22*x32*x41*x51
    ;
    v_t_e_q2ex_1_500                              =v_t_e_q2ex_1_500                              
        +coeff[ 44]    *x21    *x41    
        +coeff[ 45]*x11*x21*x31        
        +coeff[ 46]        *x32*x41    
        +coeff[ 47]        *x31*x42    
        +coeff[ 48]*x11    *x31    *x51
        +coeff[ 49]    *x22*x32        
        ;

    return v_t_e_q2ex_1_500                              ;
}
float y_e_q2ex_1_500                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.5185310E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.47007829E-03, 0.21508443E+00, 0.11971639E+00,-0.19711070E-02,
        -0.12504894E-02,-0.63991093E-03,-0.48777179E-03,-0.33230995E-03,
        -0.23829288E-03,-0.78190758E-04,-0.22672208E-04, 0.48422455E-04,
         0.46999732E-04,-0.39405216E-03,-0.23816728E-03,-0.38988732E-04,
         0.51392246E-04, 0.99932658E-04, 0.89646026E-04,-0.43487141E-03,
        -0.51040109E-03, 0.13049498E-03,-0.19636349E-03, 0.32496516E-05,
         0.90147587E-05, 0.25581678E-04,-0.38927826E-03, 0.27753918E-04,
        -0.11975720E-03,-0.12330091E-03,-0.17020365E-03, 0.31539086E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_500                              =v_y_e_q2ex_1_500                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q2ex_1_500                              =v_y_e_q2ex_1_500                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x32*x43    
        +coeff[ 22]    *x24*x33        
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42*x51
        +coeff[ 25]            *x41*x52
    ;
    v_y_e_q2ex_1_500                              =v_y_e_q2ex_1_500                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]*x11*x21*x31*x42    
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]        *x31*x43*x52
        ;

    return v_y_e_q2ex_1_500                              ;
}
float p_e_q2ex_1_500                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.2239620E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.18515739E-04,-0.16950984E-01,-0.16913615E-01, 0.16481999E-02,
         0.28441537E-02,-0.13876192E-02,-0.78053190E-03,-0.14291567E-02,
        -0.85623143E-03,-0.16986896E-04,-0.78103552E-03,-0.15004730E-03,
        -0.15018348E-03,-0.24537248E-05,-0.10206950E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_500                              =v_p_e_q2ex_1_500                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_500                              ;
}
float l_e_q2ex_1_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2646297E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25771949E-02,-0.42184941E-02,-0.10637327E-02,-0.42008646E-02,
        -0.53090062E-02,-0.90964907E-03, 0.17863988E-02,-0.47721529E-04,
        -0.23886758E-03, 0.16077068E-03, 0.17340790E-04,-0.27276776E-04,
         0.10370425E-03, 0.77577517E-03, 0.31230264E-03,-0.13578695E-02,
         0.13830521E-03, 0.79644364E-04,-0.25204293E-03, 0.81651623E-03,
        -0.53516543E-03, 0.30821378E-03,-0.39510828E-03,-0.24089463E-03,
         0.86809770E-03,-0.15606754E-02, 0.20283222E-03, 0.82197366E-03,
         0.14485541E-03, 0.72464289E-04, 0.61894336E-03, 0.47778539E-03,
        -0.69212931E-03, 0.10890599E-02, 0.57760038E-03, 0.15081083E-02,
         0.75834955E-03, 0.15659832E-02, 0.36755847E-03,-0.98326046E-03,
         0.50695031E-03, 0.90207340E-03, 0.33863622E-03,-0.19297308E-02,
        -0.47000605E-03, 0.73530921E-03,-0.10013459E-02,-0.44672220E-03,
         0.33678967E-03,-0.73031674E-03,-0.52442367E-03,-0.27049359E-03,
         0.16283601E-02,-0.50959632E-04, 0.24223682E-02, 0.18007780E-02,
        -0.91655544E-04,-0.99421141E-03, 0.27005948E-03, 0.85385039E-03,
        -0.66367874E-03,-0.39103553E-02,-0.10105043E-02,-0.15501847E-02,
         0.12373698E-02, 0.19905616E-02,-0.52633492E-03, 0.21509607E-02,
         0.28641124E-02, 0.50664239E-03,-0.10993835E-02,-0.17530560E-02,
         0.58673572E-03,-0.75287174E-03,-0.45539928E-04, 0.23934957E-03,
        -0.10066282E-03, 0.19282445E-03, 0.18104377E-03,-0.14430890E-03,
        -0.23184600E-03,-0.12051182E-03, 0.19873182E-03,-0.28524423E-03,
        -0.17134387E-03,-0.19106254E-03, 0.25024734E-03,-0.39319604E-03,
        -0.14434291E-03,-0.26119515E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x44    
        +coeff[  6]*x11*x23*x32    *x51
        +coeff[  7]    *x21            
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[  8]            *x41    
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]*x11*x21            
        +coeff[ 13]    *x22    *x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]            *x42*x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 17]                *x53
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]            *x43*x51
        +coeff[ 23]                *x54
        +coeff[ 24]*x11*x22        *x51
        +coeff[ 25]    *x24    *x41    
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x24        *x51
        +coeff[ 28]        *x33*x41*x51
        +coeff[ 29]        *x31*x43*x51
        +coeff[ 30]        *x32*x41*x52
        +coeff[ 31]*x11*x21*x33        
        +coeff[ 32]*x11*x22    *x41*x51
        +coeff[ 33]*x11        *x41*x53
        +coeff[ 34]*x12*x22        *x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]    *x22*x33*x41    
        +coeff[ 37]    *x23*x31*x41*x51
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]    *x21*x31*x41*x53
        +coeff[ 40]        *x32*x41*x53
        +coeff[ 41]*x11*x24*x31        
        +coeff[ 42]*x11*x23*x32        
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 44]*x11*x21*x33    *x51
        +coeff[ 45]*x11    *x34    *x51
        +coeff[ 46]*x11*x21    *x42*x52
        +coeff[ 47]*x12    *x32    *x52
        +coeff[ 48]*x12    *x31    *x53
        +coeff[ 49]*x13*x22*x31        
        +coeff[ 50]*x13*x21*x31*x41    
        +coeff[ 51]    *x23*x33*x41    
        +coeff[ 52]    *x22*x34    *x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 53]    *x21*x33*x42*x51
        +coeff[ 54]    *x21*x33*x41*x52
        +coeff[ 55]    *x21*x32*x42*x52
        +coeff[ 56]*x13            *x53
        +coeff[ 57]    *x22*x32    *x53
        +coeff[ 58]    *x23    *x41*x53
        +coeff[ 59]    *x21*x31*x42*x53
        +coeff[ 60]*x11    *x34*x41*x51
        +coeff[ 61]*x11*x21*x32*x42*x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 62]*x11*x21*x31*x43*x51
        +coeff[ 63]*x11*x21*x31*x41*x53
        +coeff[ 64]*x12*x22*x33        
        +coeff[ 65]*x12*x24    *x41    
        +coeff[ 66]*x12*x22*x32*x41    
        +coeff[ 67]*x12*x23    *x41*x51
        +coeff[ 68]*x12*x21*x31*x42*x51
        +coeff[ 69]*x12*x23        *x52
        +coeff[ 70]*x12*x22    *x41*x52
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 71]*x12    *x31*x41*x53
        +coeff[ 72]    *x23*x34*x41    
        +coeff[ 73]*x13*x21*x31    *x52
        +coeff[ 74]        *x31        
        +coeff[ 75]                *x51
        +coeff[ 76]*x11                
        +coeff[ 77]    *x21*x31        
        +coeff[ 78]                *x52
        +coeff[ 79]*x11            *x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 80]    *x22*x31        
        +coeff[ 81]    *x21    *x42    
        +coeff[ 82]        *x31*x42    
        +coeff[ 83]    *x21*x31    *x51
        +coeff[ 84]        *x32    *x51
        +coeff[ 85]    *x21    *x41*x51
        +coeff[ 86]*x11    *x32        
        +coeff[ 87]*x11        *x41*x51
        +coeff[ 88]*x12            *x51
    ;
    v_l_e_q2ex_1_500                              =v_l_e_q2ex_1_500                              
        +coeff[ 89]    *x23*x31        
        ;

    return v_l_e_q2ex_1_500                              ;
}
float x_e_q2ex_1_450                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.6194547E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.63150929E-03,-0.32460908E-02, 0.16578074E+00, 0.80850953E-02,
         0.33928052E-03,-0.13608916E-02,-0.37224516E-02,-0.29047802E-02,
        -0.88792631E-05,-0.12985823E-02,-0.37907422E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_450                              =v_x_e_q2ex_1_450                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2ex_1_450                              ;
}
float t_e_q2ex_1_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1904548E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.19551424E-03,-0.24546313E-02, 0.52253902E-01, 0.25060815E-02,
         0.17837556E-03,-0.17961203E-03, 0.22256881E-03, 0.49409649E-04,
        -0.18064550E-03,-0.51834778E-03,-0.62205741E-04,-0.66649038E-04,
        -0.15613460E-02,-0.11762071E-02,-0.50462128E-04,-0.50913659E-04,
        -0.11761293E-02,-0.96767716E-03, 0.95128416E-04, 0.86606531E-04,
         0.58665064E-04, 0.35443707E-04,-0.31109375E-04, 0.11715518E-03,
        -0.83138730E-04,-0.50243986E-03,-0.60102408E-04, 0.75499884E-04,
         0.10324943E-03,-0.36635574E-05,-0.77627874E-05,-0.11155112E-04,
        -0.48291063E-05, 0.71913882E-05, 0.10864418E-04, 0.22711964E-04,
         0.10167601E-03,-0.19331672E-04,-0.12438084E-04, 0.90291151E-05,
        -0.24811307E-04, 0.11365179E-04,-0.26324335E-04,-0.47611444E-04,
        -0.40948522E-04,-0.39000006E-04, 0.34264020E-04, 0.53735799E-04,
         0.34749399E-04, 0.69334255E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2ex_1_450                              =v_t_e_q2ex_1_450                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_1_450                              =v_t_e_q2ex_1_450                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23    *x42*x51
        +coeff[ 19]    *x21*x32*x42*x51
        +coeff[ 20]    *x21*x31*x43*x51
        +coeff[ 21]    *x21*x31*x41*x53
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q2ex_1_450                              =v_t_e_q2ex_1_450                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]    *x23*x31*x41*x51
        +coeff[ 29]            *x42    
        +coeff[ 30]*x12*x21            
        +coeff[ 31]*x11            *x52
        +coeff[ 32]            *x41*x52
        +coeff[ 33]*x11*x22*x31        
        +coeff[ 34]*x11*x22        *x51
    ;
    v_t_e_q2ex_1_450                              =v_t_e_q2ex_1_450                              
        +coeff[ 35]*x11*x21    *x41*x51
        +coeff[ 36]    *x21    *x42*x51
        +coeff[ 37]*x11*x21        *x52
        +coeff[ 38]    *x21        *x53
        +coeff[ 39]*x13*x21*x31        
        +coeff[ 40]*x11    *x33*x41    
        +coeff[ 41]    *x22    *x42*x51
        +coeff[ 42]    *x23        *x52
        +coeff[ 43]*x13*x21*x31    *x51
    ;
    v_t_e_q2ex_1_450                              =v_t_e_q2ex_1_450                              
        +coeff[ 44]*x11*x23    *x41*x51
        +coeff[ 45]*x11*x21    *x43*x51
        +coeff[ 46]*x11*x21*x31*x41*x52
        +coeff[ 47]    *x23        *x53
        +coeff[ 48]*x11*x21*x31    *x53
        +coeff[ 49]*x11*x21            
        ;

    return v_t_e_q2ex_1_450                              ;
}
float y_e_q2ex_1_450                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.7942520E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.76902338E-03, 0.21504077E+00, 0.11967082E+00,-0.19716145E-02,
        -0.12493510E-02,-0.74600975E-03,-0.52819948E-03,-0.39864972E-03,
        -0.30535503E-03,-0.84800915E-04,-0.40919182E-04,-0.51021485E-04,
         0.52447322E-04,-0.17299975E-04,-0.42946907E-03,-0.20279044E-03,
         0.87168373E-04, 0.82078404E-04,-0.39101555E-03, 0.10154087E-04,
         0.75677399E-05, 0.17452210E-04, 0.50895302E-04, 0.34373305E-04,
         0.13643896E-04,-0.24834908E-04,-0.12947836E-03, 0.18222629E-04,
        -0.85384068E-04,-0.97344375E-04,-0.37400743E-04,-0.71212278E-04,
         0.57301102E-04,-0.40828818E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_450                              =v_y_e_q2ex_1_450                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x45    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x22    *x41*x51
    ;
    v_y_e_q2ex_1_450                              =v_y_e_q2ex_1_450                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]            *x44    
        +coeff[ 21]            *x41*x52
        +coeff[ 22]        *x31*x42*x51
        +coeff[ 23]        *x32*x41*x51
        +coeff[ 24]    *x21    *x44    
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_y_e_q2ex_1_450                              =v_y_e_q2ex_1_450                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x12    *x31*x42    
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]*x11*x21    *x43*x51
        ;

    return v_y_e_q2ex_1_450                              ;
}
float p_e_q2ex_1_450                              (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.8406176E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.82420956E-04,-0.16981537E-01,-0.16968975E-01, 0.16491528E-02,
         0.28467495E-02,-0.13863441E-02,-0.77849987E-03,-0.14213888E-02,
        -0.85145485E-03,-0.18146187E-04,-0.77570241E-03,-0.14789411E-03,
        -0.14817883E-03,-0.19212295E-06, 0.36981444E-04,-0.10263220E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_450                              =v_p_e_q2ex_1_450                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_450                              ;
}
float l_e_q2ex_1_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2682167E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25024195E-02,-0.38771748E-02,-0.36267581E-03,-0.45896857E-02,
        -0.53425794E-02,-0.22922941E-03, 0.59549417E-03, 0.22629136E-02,
         0.85002300E-03, 0.23447495E-03,-0.44401850E-05,-0.88377739E-04,
        -0.53262211E-04, 0.28900741E-03, 0.52758621E-03,-0.29391158E-03,
        -0.11303138E-03, 0.13645741E-03, 0.13191282E-03, 0.22563653E-03,
         0.48184922E-03,-0.38623062E-03,-0.35892558E-03,-0.44072513E-03,
        -0.51070383E-03, 0.91634918E-03, 0.32080116E-03,-0.98223449E-03,
        -0.73831587E-03, 0.18302613E-04,-0.22778301E-02, 0.16303782E-02,
        -0.68926427E-03, 0.52816438E-04, 0.83352131E-03,-0.64001844E-03,
        -0.20067191E-02,-0.54454751E-03,-0.47896127E-03, 0.48405363E-03,
         0.10175728E-02,-0.36896157E-03,-0.51741325E-03, 0.11843567E-02,
        -0.17473449E-02, 0.60688506E-03, 0.16813452E-02, 0.17079208E-02,
         0.30247825E-02, 0.15366954E-02,-0.37580328E-02,-0.19386266E-02,
        -0.12102411E-02, 0.52973127E-03,-0.56174450E-03, 0.29791775E-02,
         0.12859942E-02,-0.38963030E-03, 0.13835208E-02, 0.17307160E-02,
        -0.59381186E-03, 0.12600493E-02,-0.36399200E-04,-0.17749629E-04,
         0.56821159E-05, 0.10038556E-03,-0.12653206E-03,-0.78090532E-04,
        -0.57247096E-04, 0.10566380E-02,-0.23667103E-03,-0.16968556E-03,
        -0.29374336E-03, 0.12995055E-03, 0.21110805E-04,-0.11314051E-03,
        -0.95192700E-04, 0.16546984E-03, 0.14825705E-03, 0.19186501E-03,
         0.28629569E-03, 0.16733193E-03, 0.18999785E-03, 0.10245608E-03,
        -0.41517464E-03, 0.46145049E-03, 0.48021419E-03, 0.27372982E-03,
         0.26280861E-03,-0.46164053E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11            *x52
        +coeff[  6]*x11    *x31*x42*x51
        +coeff[  7]*x11*x21*x32*x42    
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[  8]*x13    *x31*x42    
        +coeff[  9]                *x51
        +coeff[ 10]*x11                
        +coeff[ 11]    *x21    *x41    
        +coeff[ 12]*x12                
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 17]    *x21    *x41*x51
        +coeff[ 18]            *x42*x51
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]*x11*x21        *x51
        +coeff[ 21]*x12            *x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]        *x34        
        +coeff[ 24]        *x32*x42    
        +coeff[ 25]*x11*x21*x31*x41    
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 26]*x13*x21            
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x22*x32    *x51
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]*x11*x21*x31*x41*x51
        +coeff[ 31]*x11    *x31*x41*x52
        +coeff[ 32]*x11*x21        *x53
        +coeff[ 33]*x12    *x33        
        +coeff[ 34]*x12    *x32    *x51
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 35]*x13    *x31    *x51
        +coeff[ 36]    *x23    *x42*x51
        +coeff[ 37]    *x22        *x54
        +coeff[ 38]*x12*x22    *x42    
        +coeff[ 39]*x12    *x31*x43    
        +coeff[ 40]*x12*x22*x31    *x51
        +coeff[ 41]*x12*x21        *x53
        +coeff[ 42]*x12    *x31    *x53
        +coeff[ 43]    *x24*x32    *x51
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 44]    *x22*x34    *x51
        +coeff[ 45]*x13*x21    *x41*x51
        +coeff[ 46]    *x22*x32*x42*x51
        +coeff[ 47]    *x21*x32*x42*x52
        +coeff[ 48]*x11*x21*x33*x41*x51
        +coeff[ 49]*x11*x22*x31*x41*x52
        +coeff[ 50]*x11    *x33*x41*x52
        +coeff[ 51]*x11    *x32*x42*x52
        +coeff[ 52]*x11    *x32*x41*x53
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 53]*x12    *x33*x41*x51
        +coeff[ 54]*x12*x23        *x52
        +coeff[ 55]    *x23    *x44*x51
        +coeff[ 56]    *x24    *x42*x52
        +coeff[ 57]    *x23    *x43*x52
        +coeff[ 58]    *x22*x33    *x53
        +coeff[ 59]        *x32*x42*x54
        +coeff[ 60]    *x21    *x43*x54
        +coeff[ 61]        *x31*x43*x54
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 62]    *x21            
        +coeff[ 63]    *x21        *x51
        +coeff[ 64]        *x31    *x51
        +coeff[ 65]            *x41*x51
        +coeff[ 66]*x11*x21            
        +coeff[ 67]*x11    *x31        
        +coeff[ 68]        *x33        
        +coeff[ 69]    *x22    *x41    
        +coeff[ 70]        *x32*x41    
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 71]    *x21    *x42    
        +coeff[ 72]        *x31*x42    
        +coeff[ 73]        *x31*x41*x51
        +coeff[ 74]        *x31    *x52
        +coeff[ 75]            *x41*x52
        +coeff[ 76]                *x53
        +coeff[ 77]*x11*x22            
        +coeff[ 78]*x11*x21    *x41    
        +coeff[ 79]*x11        *x42    
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 80]*x11    *x31    *x51
        +coeff[ 81]*x11        *x41*x51
        +coeff[ 82]*x12    *x31        
        +coeff[ 83]*x12        *x41    
        +coeff[ 84]    *x23*x31        
        +coeff[ 85]    *x21*x33        
        +coeff[ 86]    *x22*x31*x41    
        +coeff[ 87]    *x21*x32*x41    
        +coeff[ 88]    *x23        *x51
    ;
    v_l_e_q2ex_1_450                              =v_l_e_q2ex_1_450                              
        +coeff[ 89]    *x22*x31    *x51
        ;

    return v_l_e_q2ex_1_450                              ;
}
float x_e_q2ex_1_449                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.1490167E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.14717676E-02,-0.31824915E-02, 0.16742663E+00, 0.80030896E-02,
         0.32864462E-03,-0.12491504E-02,-0.32803472E-02,-0.22264218E-02,
         0.21971360E-04, 0.18903489E-04,-0.11874774E-04,-0.16828313E-02,
        -0.37604402E-03,-0.17901544E-02,-0.14599072E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_449                              =v_x_e_q2ex_1_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_449                              ;
}
float t_e_q2ex_1_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4830894E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47759403E-03,-0.24202296E-02, 0.52854832E-01, 0.24058353E-02,
         0.17432524E-03,-0.17999001E-03, 0.27365025E-03,-0.13950829E-05,
        -0.89717112E-04, 0.74041985E-04,-0.18225670E-03,-0.69608344E-04,
        -0.17755094E-02,-0.12195260E-02,-0.72072813E-04,-0.49255912E-04,
         0.89047942E-04, 0.19289467E-03, 0.13092070E-03,-0.63247763E-03,
        -0.13629321E-02,-0.10474403E-02, 0.50824940E-06,-0.38460897E-04,
         0.74451164E-04,-0.63669385E-03,-0.65336244E-05,-0.80801065E-05,
        -0.34559587E-04, 0.13385313E-04,-0.90482630E-04,-0.68232745E-04,
         0.18618452E-04,-0.25329422E-04,-0.13898725E-04, 0.17642080E-04,
         0.33297260E-04, 0.11489880E-04,-0.41443320E-04,-0.10985527E-05,
        -0.23527084E-05,-0.40223094E-05, 0.12079772E-04, 0.36523309E-05,
         0.83315817E-05, 0.78407966E-05,-0.17644016E-05, 0.25998356E-05,
        -0.16069600E-04, 0.46195678E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_449                              =v_t_e_q2ex_1_449                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_449                              =v_t_e_q2ex_1_449                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q2ex_1_449                              =v_t_e_q2ex_1_449                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]    *x23*x31    *x51
        +coeff[ 34]*x12*x21        *x52
    ;
    v_t_e_q2ex_1_449                              =v_t_e_q2ex_1_449                              
        +coeff[ 35]*x12*x22    *x41*x51
        +coeff[ 36]*x12*x21*x31    *x52
        +coeff[ 37]    *x21*x33    *x52
        +coeff[ 38]    *x21*x32*x41*x52
        +coeff[ 39]            *x41    
        +coeff[ 40]                *x51
        +coeff[ 41]    *x22            
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_q2ex_1_449                              =v_t_e_q2ex_1_449                              
        +coeff[ 44]    *x21*x31    *x51
        +coeff[ 45]    *x22*x32        
        +coeff[ 46]*x11    *x33        
        +coeff[ 47]*x13        *x41    
        +coeff[ 48]    *x23    *x41    
        +coeff[ 49]*x12    *x31*x41    
        ;

    return v_t_e_q2ex_1_449                              ;
}
float y_e_q2ex_1_449                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.9912077E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.10052426E-02, 0.21460482E+00, 0.13242932E+00,-0.21720240E-02,
        -0.12345224E-02,-0.60700072E-03,-0.40134363E-03,-0.27261476E-03,
        -0.63176412E-04, 0.48132719E-04,-0.32381296E-04,-0.16889666E-03,
        -0.48503411E-04, 0.12333543E-04,-0.56097453E-03,-0.72079834E-04,
        -0.16931209E-03,-0.73142583E-04,-0.98840290E-04, 0.88412715E-04,
        -0.40508219E-03,-0.65456214E-03,-0.25701986E-03, 0.40023056E-04,
        -0.68790694E-04, 0.34473254E-04,-0.30977157E-03,-0.84092790E-05,
        -0.99335339E-05, 0.52223357E-04,-0.54494088E-03,-0.69035646E-04,
        -0.35900655E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_449                              =v_y_e_q2ex_1_449                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]        *x33*x42    
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q2ex_1_449                              =v_y_e_q2ex_1_449                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]    *x22    *x43*x51
        +coeff[ 24]    *x22*x31*x44    
        +coeff[ 25]    *x24*x31*x42    
    ;
    v_y_e_q2ex_1_449                              =v_y_e_q2ex_1_449                              
        +coeff[ 26]        *x31*x42    
        +coeff[ 27]            *x44    
        +coeff[ 28]            *x43*x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x11*x21*x32*x42    
        ;

    return v_y_e_q2ex_1_449                              ;
}
float p_e_q2ex_1_449                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.4826885E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.49840070E-04,-0.18597012E-01,-0.16534008E-01, 0.18055275E-02,
         0.28152876E-02,-0.14074594E-02,-0.87707851E-03,-0.15603027E-02,
        -0.84459479E-03,-0.19994546E-04,-0.19964704E-03,-0.94696775E-03,
        -0.14471854E-03,-0.32130999E-05,-0.10766546E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_449                              =v_p_e_q2ex_1_449                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_449                              ;
}
float l_e_q2ex_1_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2620385E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26599115E-02,-0.40814881E-02,-0.11773614E-02,-0.45456174E-02,
        -0.56422125E-02,-0.32773418E-02, 0.10406970E-03, 0.12255018E-03,
        -0.88800938E-04,-0.12467336E-02, 0.45759505E-04, 0.43247797E-03,
        -0.67784998E-03, 0.22569625E-02,-0.10641360E-02, 0.22556088E-02,
        -0.15411841E-04, 0.35029859E-03, 0.64049516E-03,-0.19073769E-03,
         0.90139773E-03, 0.11692363E-02, 0.10626838E-02,-0.10134376E-02,
         0.83242566E-03,-0.12128309E-02,-0.82982815E-03, 0.34198930E-03,
         0.91349160E-04,-0.24398090E-02, 0.11060843E-02, 0.81173686E-03,
        -0.67470819E-05,-0.10042586E-02, 0.17739489E-02,-0.12309148E-02,
        -0.92980696E-03,-0.11763765E-02,-0.32025269E-04, 0.12979074E-02,
        -0.12458115E-02,-0.90545119E-03, 0.92938065E-03,-0.52520540E-03,
        -0.21466883E-03, 0.52438182E-03, 0.17864651E-02, 0.34344431E-02,
        -0.47954347E-03,-0.93646056E-03,-0.10150115E-02, 0.56955387E-03,
         0.99380454E-03, 0.13942329E-02,-0.22219960E-02, 0.13843251E-02,
         0.15383409E-02, 0.96863153E-03,-0.12158771E-02,-0.21132734E-02,
         0.27627749E-02, 0.24183272E-03,-0.11035252E-03,-0.54949032E-04,
         0.80646605E-04, 0.23412376E-03, 0.16823574E-03,-0.16071007E-03,
        -0.54894254E-03, 0.77723744E-04,-0.38288842E-03,-0.19562215E-03,
        -0.13401012E-03, 0.35694387E-03,-0.65829074E-04, 0.56253403E-03,
        -0.14244972E-03, 0.18068457E-03, 0.77344979E-04, 0.58081107E-04,
        -0.27320546E-03,-0.26764159E-03,-0.35444275E-03,-0.58001489E-03,
        -0.35719294E-03, 0.34225592E-03,-0.28403540E-03, 0.22200184E-03,
         0.19692838E-03,-0.10899464E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x33    *x52
        +coeff[  6]            *x41    
        +coeff[  7]                *x51
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x33        
        +coeff[ 11]*x12    *x31        
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]    *x21*x31    *x52
        +coeff[ 14]*x11    *x32*x41    
        +coeff[ 15]*x11*x21    *x42    
        +coeff[ 16]*x11*x22        *x51
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 17]*x11        *x42*x51
        +coeff[ 18]*x11    *x31    *x52
        +coeff[ 19]*x13    *x31        
        +coeff[ 20]    *x24*x31        
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]*x11*x21*x33        
        +coeff[ 24]*x11*x21*x32*x41    
        +coeff[ 25]*x11    *x33    *x51
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 26]*x12    *x33        
        +coeff[ 27]*x12*x21    *x41*x51
        +coeff[ 28]*x13    *x31    *x51
        +coeff[ 29]    *x23*x31    *x52
        +coeff[ 30]        *x32*x41*x53
        +coeff[ 31]        *x31*x42*x53
        +coeff[ 32]*x11*x23*x32        
        +coeff[ 33]*x11*x24    *x41    
        +coeff[ 34]*x11    *x34*x41    
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 35]*x11*x23    *x42    
        +coeff[ 36]*x11*x21*x32*x42    
        +coeff[ 37]*x11*x21    *x44    
        +coeff[ 38]*x11*x22*x31*x41*x51
        +coeff[ 39]*x11*x22    *x42*x51
        +coeff[ 40]*x11        *x44*x51
        +coeff[ 41]*x11*x22*x31    *x52
        +coeff[ 42]*x12*x23        *x51
        +coeff[ 43]*x12*x21*x32    *x51
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 44]*x12*x21        *x53
        +coeff[ 45]*x13*x22    *x41    
        +coeff[ 46]    *x23*x32*x42    
        +coeff[ 47]    *x22*x33*x42    
        +coeff[ 48]    *x23    *x42*x52
        +coeff[ 49]        *x31*x44*x52
        +coeff[ 50]        *x32*x41*x54
        +coeff[ 51]            *x43*x54
        +coeff[ 52]*x11*x23*x33        
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 53]*x11*x22*x31*x43    
        +coeff[ 54]*x11*x22*x31*x42*x51
        +coeff[ 55]*x12*x23*x31*x41    
        +coeff[ 56]*x12    *x33    *x52
        +coeff[ 57]*x13*x22    *x42    
        +coeff[ 58]*x13    *x32*x41*x51
        +coeff[ 59]    *x22*x34*x41*x51
        +coeff[ 60]    *x23*x33    *x52
        +coeff[ 61]        *x31        
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 62]    *x21*x31        
        +coeff[ 63]    *x21    *x41    
        +coeff[ 64]    *x21        *x51
        +coeff[ 65]        *x31    *x51
        +coeff[ 66]*x11        *x41    
        +coeff[ 67]    *x23            
        +coeff[ 68]        *x32*x41    
        +coeff[ 69]            *x42*x51
        +coeff[ 70]        *x31    *x52
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 71]            *x41*x52
        +coeff[ 72]                *x53
        +coeff[ 73]*x11*x21*x31        
        +coeff[ 74]*x11        *x42    
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11            *x52
        +coeff[ 77]*x12*x21            
        +coeff[ 78]*x12            *x51
        +coeff[ 79]*x13                
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 80]    *x24            
        +coeff[ 81]        *x31*x43    
        +coeff[ 82]        *x33    *x51
        +coeff[ 83]        *x31*x42*x51
        +coeff[ 84]            *x43*x51
        +coeff[ 85]            *x42*x52
        +coeff[ 86]    *x21        *x53
        +coeff[ 87]        *x31    *x53
        +coeff[ 88]            *x41*x53
    ;
    v_l_e_q2ex_1_449                              =v_l_e_q2ex_1_449                              
        +coeff[ 89]                *x54
        ;

    return v_l_e_q2ex_1_449                              ;
}
float x_e_q2ex_1_400                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.3382821E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.35812554E-03,-0.31710062E-02, 0.16758624E+00, 0.79816943E-02,
         0.32588647E-03,-0.10103390E-02,-0.29570872E-02,-0.20462840E-02,
         0.12338138E-05,-0.80699235E-03,-0.12583131E-02,-0.37229716E-03,
        -0.25130461E-02,-0.18363694E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_400                              =v_x_e_q2ex_1_400                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_400                              ;
}
float t_e_q2ex_1_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1232662E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12924611E-03,-0.24153353E-02, 0.53012814E-01, 0.23851565E-02,
         0.17425347E-03,-0.18276823E-03, 0.26319968E-03,-0.13680503E-02,
         0.67838778E-05, 0.72247458E-04,-0.59223972E-03,-0.17153785E-02,
        -0.11821797E-02,-0.71517075E-04,-0.18760540E-03,-0.10462308E-02,
        -0.11389273E-03,-0.71831419E-04,-0.45390727E-04, 0.72365801E-04,
         0.18544498E-03, 0.10432401E-03,-0.64616191E-03,-0.26447244E-04,
        -0.33836026E-04,-0.71443319E-05, 0.11891456E-03, 0.29668328E-04,
         0.72183911E-05,-0.97143266E-05, 0.16428930E-04,-0.81270009E-04,
        -0.57501456E-04, 0.87324779E-05,-0.22482065E-04,-0.21759397E-04,
         0.18485529E-04,-0.14363559E-04,-0.24640991E-04, 0.66498542E-04,
         0.63925851E-04, 0.19661449E-04,-0.21590036E-04, 0.18577428E-04,
         0.25834361E-04,-0.42696320E-04,-0.17545981E-04,-0.13694955E-05,
        -0.29831522E-05,-0.22143565E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32*x42    
    ;
    v_t_e_q2ex_1_400                              =v_t_e_q2ex_1_400                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_q2ex_1_400                              =v_t_e_q2ex_1_400                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2ex_1_400                              =v_t_e_q2ex_1_400                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x11*x21    *x41    
        +coeff[ 29]    *x21*x31    *x52
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11        *x42*x52
    ;
    v_t_e_q2ex_1_400                              =v_t_e_q2ex_1_400                              
        +coeff[ 35]*x13*x22    *x41    
        +coeff[ 36]*x11*x21*x33*x41    
        +coeff[ 37]    *x23*x31*x42    
        +coeff[ 38]    *x23    *x43    
        +coeff[ 39]*x12*x21*x31*x41*x51
        +coeff[ 40]    *x23    *x42*x51
        +coeff[ 41]*x13*x21        *x52
        +coeff[ 42]*x11*x22*x31    *x52
        +coeff[ 43]    *x22    *x42*x52
    ;
    v_t_e_q2ex_1_400                              =v_t_e_q2ex_1_400                              
        +coeff[ 44]*x12*x21        *x53
        +coeff[ 45]    *x21*x32    *x53
        +coeff[ 46]*x11*x21    *x41*x53
        +coeff[ 47]            *x41    
        +coeff[ 48]*x11*x21            
        +coeff[ 49]        *x32        
        ;

    return v_t_e_q2ex_1_400                              ;
}
float y_e_q2ex_1_400                              (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.1055725E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.11200259E-02, 0.21457443E+00, 0.13229512E+00,-0.21676989E-02,
        -0.12230014E-02,-0.60024502E-03,-0.41001244E-03,-0.27216034E-03,
        -0.57387093E-04, 0.55208642E-04,-0.27236187E-04,-0.14628953E-03,
        -0.68057539E-04, 0.15327008E-04,-0.57109341E-03,-0.13307195E-03,
        -0.17603060E-03,-0.79665318E-04,-0.11242309E-03, 0.87745873E-04,
        -0.42701262E-03,-0.62413956E-03,-0.19668631E-04,-0.22734725E-03,
         0.80834769E-04,-0.56122215E-04, 0.61234561E-04,-0.28375440E-03,
         0.11662365E-04, 0.24686591E-04,-0.23158354E-04, 0.24976021E-04,
         0.92350827E-04,-0.54132176E-03,-0.14950431E-04,-0.22125832E-04,
         0.36119174E-04,-0.25587737E-04,-0.72952280E-04, 0.40883930E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_400                              =v_y_e_q2ex_1_400                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]        *x33*x42    
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q2ex_1_400                              =v_y_e_q2ex_1_400                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x24    *x41*x51
        +coeff[ 25]    *x22*x31*x44    
    ;
    v_y_e_q2ex_1_400                              =v_y_e_q2ex_1_400                              
        +coeff[ 26]    *x24*x31*x42    
        +coeff[ 27]        *x31*x42    
        +coeff[ 28]*x11    *x31*x41    
        +coeff[ 29]            *x41*x52
        +coeff[ 30]*x11    *x31*x42    
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x32*x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]*x11        *x41*x52
    ;
    v_y_e_q2ex_1_400                              =v_y_e_q2ex_1_400                              
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]    *x22    *x44    
        +coeff[ 37]        *x32*x44    
        +coeff[ 38]*x11*x23*x31        
        +coeff[ 39]*x11*x21    *x41*x52
        ;

    return v_y_e_q2ex_1_400                              ;
}
float p_e_q2ex_1_400                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.5242478E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.58816142E-04,-0.18606242E-01,-0.16555073E-01, 0.18056221E-02,
         0.28159663E-02,-0.14120037E-02,-0.87893137E-03,-0.15634297E-02,
        -0.84246142E-03,-0.13084320E-04,-0.19952777E-03,-0.95377333E-03,
        -0.14909213E-03,-0.45969596E-05,-0.10850574E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_400                              =v_p_e_q2ex_1_400                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_400                              ;
}
float l_e_q2ex_1_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2638188E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26819564E-02,-0.44158706E-02,-0.10015849E-02,-0.43609892E-02,
        -0.55105393E-02,-0.19680372E-03,-0.29110952E-03,-0.90244394E-04,
        -0.16387753E-03, 0.48353896E-03, 0.51343680E-03,-0.68395231E-04,
         0.29650040E-03,-0.34307304E-03, 0.72458628E-04,-0.14836859E-03,
        -0.98880322E-04,-0.16152317E-03, 0.10664619E-03,-0.14972028E-02,
        -0.35907980E-03,-0.24419959E-03, 0.52033144E-03,-0.33107158E-03,
         0.13223769E-02, 0.99968212E-03,-0.12533843E-03,-0.13733342E-02,
         0.98830671E-03,-0.68574637E-03, 0.60079445E-03, 0.73369837E-03,
         0.13991518E-03,-0.59173262E-03, 0.18256975E-02,-0.22034543E-04,
        -0.11851625E-02, 0.56321541E-03,-0.15412298E-02, 0.17091327E-02,
         0.59566332E-03,-0.33349771E-03,-0.88680262E-03,-0.35842708E-02,
        -0.16965956E-02, 0.25296854E-03, 0.10744188E-02, 0.10712980E-02,
        -0.62420819E-03,-0.50823519E-03, 0.70788595E-03,-0.54370536E-03,
        -0.80326380E-03, 0.11648235E-02,-0.16877295E-02,-0.17656381E-02,
        -0.84019885E-05, 0.73959981E-03,-0.13777763E-02,-0.71037660E-03,
         0.18035970E-02, 0.15036346E-03, 0.24958758E-03, 0.63558567E-04,
         0.22318625E-03,-0.19300207E-03, 0.31840941E-03, 0.79332897E-03,
        -0.33631854E-03,-0.23702973E-03, 0.99438905E-04,-0.11430185E-03,
         0.37338000E-03, 0.13784032E-03, 0.22702900E-03,-0.24665546E-03,
        -0.30979534E-05, 0.23361502E-03,-0.26601306E-03, 0.41117737E-03,
        -0.40978193E-03, 0.16747772E-03, 0.25585201E-03,-0.43206519E-03,
        -0.35205262E-03, 0.11254673E-03, 0.39052722E-03,-0.11174816E-02,
         0.24685971E-03, 0.16027890E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31*x41*x51
        +coeff[  6]        *x31        
        +coeff[  7]            *x41    
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[  8]*x11                
        +coeff[  9]            *x41*x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11    *x32        
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 17]*x11        *x41*x51
        +coeff[ 18]*x11            *x52
        +coeff[ 19]        *x31*x43    
        +coeff[ 20]            *x41*x53
        +coeff[ 21]*x11*x23            
        +coeff[ 22]*x11    *x32    *x51
        +coeff[ 23]*x12        *x41*x51
        +coeff[ 24]        *x34*x41    
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 26]*x13            *x51
        +coeff[ 27]        *x32*x42*x51
        +coeff[ 28]    *x21    *x42*x52
        +coeff[ 29]*x11*x21*x32    *x51
        +coeff[ 30]*x11*x21    *x42*x51
        +coeff[ 31]*x11*x22        *x52
        +coeff[ 32]*x12*x21*x31*x41    
        +coeff[ 33]*x12    *x32*x41    
        +coeff[ 34]*x12    *x31*x41*x51
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 35]        *x34*x42    
        +coeff[ 36]        *x32*x44    
        +coeff[ 37]    *x24        *x52
        +coeff[ 38]        *x32    *x54
        +coeff[ 39]*x11*x24        *x51
        +coeff[ 40]*x11*x23*x31    *x51
        +coeff[ 41]*x13*x23            
        +coeff[ 42]*x13*x22*x31        
        +coeff[ 43]    *x23*x33*x41    
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 44]    *x23*x32*x42    
        +coeff[ 45]    *x21*x33*x43    
        +coeff[ 46]    *x21*x33*x42*x51
        +coeff[ 47]    *x22    *x44*x51
        +coeff[ 48]*x13*x21        *x52
        +coeff[ 49]    *x23*x32    *x52
        +coeff[ 50]    *x24    *x41*x52
        +coeff[ 51]*x11*x24*x32        
        +coeff[ 52]*x12*x24*x31        
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 53]*x12*x21*x32*x41*x51
        +coeff[ 54]*x12    *x33*x41*x51
        +coeff[ 55]*x13*x23    *x41    
        +coeff[ 56]*x13*x21    *x43    
        +coeff[ 57]*x13*x21    *x41*x52
        +coeff[ 58]    *x22*x33    *x53
        +coeff[ 59]        *x32*x43*x53
        +coeff[ 60]        *x34    *x54
        +coeff[ 61]    *x21            
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 62]        *x31    *x51
        +coeff[ 63]*x11        *x41    
        +coeff[ 64]    *x22*x31        
        +coeff[ 65]    *x21*x32        
        +coeff[ 66]        *x33        
        +coeff[ 67]    *x21*x31*x41    
        +coeff[ 68]        *x32*x41    
        +coeff[ 69]    *x22        *x51
        +coeff[ 70]                *x53
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 71]*x11*x21*x31        
        +coeff[ 72]*x11*x21    *x41    
        +coeff[ 73]*x13                
        +coeff[ 74]    *x21*x33        
        +coeff[ 75]        *x34        
        +coeff[ 76]    *x23    *x41    
        +coeff[ 77]    *x22*x31*x41    
        +coeff[ 78]    *x21*x31*x42    
        +coeff[ 79]    *x22*x31    *x51
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 80]        *x33    *x51
        +coeff[ 81]    *x22    *x41*x51
        +coeff[ 82]    *x21*x31*x41*x51
        +coeff[ 83]        *x32*x41*x51
        +coeff[ 84]        *x31*x42*x51
        +coeff[ 85]    *x21        *x53
        +coeff[ 86]*x11*x22*x31        
        +coeff[ 87]*x11*x22        *x51
        +coeff[ 88]*x11*x21    *x41*x51
    ;
    v_l_e_q2ex_1_400                              =v_l_e_q2ex_1_400                              
        +coeff[ 89]*x11    *x31*x41*x51
        ;

    return v_l_e_q2ex_1_400                              ;
}
float x_e_q2ex_1_350                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.3715694E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.37369120E-03,-0.31574941E-02, 0.16805309E+00, 0.79623936E-02,
         0.33468942E-03,-0.10024114E-02,-0.29391090E-02,-0.20305431E-02,
        -0.72370703E-05,-0.79443731E-03,-0.12510527E-02,-0.36838106E-03,
        -0.24959890E-02,-0.18317268E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_350                              =v_x_e_q2ex_1_350                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_350                              ;
}
float t_e_q2ex_1_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1231839E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12348042E-03,-0.24005966E-02, 0.53240996E-01, 0.23824817E-02,
         0.18110481E-03,-0.17348764E-03, 0.30174854E-03,-0.46107480E-05,
        -0.91202004E-04, 0.81791870E-04,-0.18795136E-03,-0.70992304E-04,
        -0.17395789E-02,-0.11842854E-02,-0.83464693E-04,-0.60487866E-04,
         0.87540648E-04, 0.22177376E-03, 0.14781817E-03, 0.16421987E-04,
        -0.62102382E-03,-0.14355828E-02,-0.10933630E-02,-0.28247146E-05,
        -0.47833484E-04,-0.12674317E-04, 0.90402158E-04,-0.68182737E-03,
         0.66508892E-05, 0.33378052E-04,-0.10486098E-04,-0.61191153E-04,
        -0.42739186E-04,-0.33497170E-04,-0.29289140E-04,-0.23261286E-04,
        -0.39625913E-04, 0.29953893E-04,-0.71039726E-05,-0.34876502E-05,
         0.34519728E-05,-0.80225054E-05, 0.45009906E-05, 0.45879765E-05,
         0.11585336E-04,-0.70816859E-05, 0.65384343E-05, 0.15323216E-05,
        -0.79872743E-05,-0.71968934E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_350                              =v_t_e_q2ex_1_350                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_350                              =v_t_e_q2ex_1_350                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x23*x32        
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2ex_1_350                              =v_t_e_q2ex_1_350                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]    *x22    *x41    
        +coeff[ 29]*x11*x22*x31        
        +coeff[ 30]    *x22    *x42    
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x22*x33        
        +coeff[ 34]*x12*x21    *x43    
    ;
    v_t_e_q2ex_1_350                              =v_t_e_q2ex_1_350                              
        +coeff[ 35]*x11*x23    *x41*x51
        +coeff[ 36]*x12*x21*x31    *x52
        +coeff[ 37]    *x23*x31    *x52
        +coeff[ 38]*x11    *x31        
        +coeff[ 39]    *x21*x31        
        +coeff[ 40]    *x21    *x41    
        +coeff[ 41]*x13                
        +coeff[ 42]*x11        *x41*x51
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_t_e_q2ex_1_350                              =v_t_e_q2ex_1_350                              
        +coeff[ 44]*x13*x21            
        +coeff[ 45]*x11*x23            
        +coeff[ 46]*x13    *x31        
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]*x11*x21    *x42    
        +coeff[ 49]*x13            *x51
        ;

    return v_t_e_q2ex_1_350                              ;
}
float y_e_q2ex_1_350                              (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.2091391E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.20325824E-02, 0.21415411E+00, 0.13210613E+00,-0.21590996E-02,
        -0.12135829E-02,-0.77060325E-03,-0.56409877E-03,-0.34966110E-03,
        -0.23044020E-04, 0.56019664E-04, 0.12160801E-03, 0.66490356E-04,
         0.44132841E-04, 0.11095584E-03,-0.46825461E-03, 0.14462936E-03,
        -0.21609617E-03,-0.10247831E-03,-0.93003931E-04, 0.80438702E-04,
        -0.34061295E-03,-0.13455977E-03, 0.37061065E-04,-0.18139687E-03,
        -0.46985585E-03, 0.15516518E-04,-0.89698779E-05, 0.61033352E-04,
        -0.18629116E-03,-0.12463023E-03,-0.31198466E-04,-0.20458689E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_350                              =v_y_e_q2ex_1_350                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]        *x33*x42    
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q2ex_1_350                              =v_y_e_q2ex_1_350                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]    *x22    *x43*x51
        +coeff[ 23]*x11*x23    *x43    
        +coeff[ 24]        *x31*x42    
        +coeff[ 25]            *x41*x52
    ;
    v_y_e_q2ex_1_350                              =v_y_e_q2ex_1_350                              
        +coeff[ 26]            *x43*x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x12    *x31*x42    
        +coeff[ 31]    *x21    *x42*x52
        ;

    return v_y_e_q2ex_1_350                              ;
}
float p_e_q2ex_1_350                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.2050881E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.19693830E-03,-0.18615911E-01,-0.16556725E-01, 0.18037169E-02,
         0.28115471E-02,-0.14141058E-02,-0.88056311E-03,-0.15494159E-02,
        -0.83539635E-03,-0.23368830E-04,-0.19956824E-03,-0.93797198E-03,
        -0.14430449E-03, 0.16669266E-05,-0.11108598E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_350                              =v_p_e_q2ex_1_350                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_350                              ;
}
float l_e_q2ex_1_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2675391E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26697656E-02,-0.58086389E-04,-0.42130444E-02,-0.10693092E-02,
        -0.52267457E-02,-0.56503788E-02,-0.18856715E-03,-0.12550093E-03,
         0.19760113E-03,-0.41236624E-03,-0.70438243E-03, 0.50016440E-03,
        -0.49787509E-03, 0.18456484E-03,-0.53868984E-03, 0.20675990E-03,
         0.64097927E-03, 0.90565285E-04, 0.34183514E-03,-0.93247596E-03,
         0.23338904E-02,-0.82029891E-03, 0.79409662E-03, 0.27892945E-03,
        -0.82912110E-03, 0.19567923E-03,-0.43890291E-03, 0.18010504E-02,
        -0.18832017E-02, 0.91976050E-03, 0.19923642E-02, 0.22469056E-02,
         0.15726929E-02,-0.11449596E-02,-0.22708045E-02, 0.21238003E-02,
        -0.17018946E-02, 0.95033023E-03, 0.29889387E-02, 0.43845800E-03,
         0.23862734E-02,-0.12340625E-02,-0.91000326E-03, 0.33830185E-03,
         0.90783741E-03,-0.18108989E-02,-0.83553593E-03, 0.25900977E-02,
        -0.13243926E-02,-0.38475718E-03,-0.10156637E-02, 0.58180810E-03,
         0.40670403E-03,-0.96665763E-05,-0.35353354E-03, 0.16371915E-03,
         0.12702381E-03, 0.19656633E-03,-0.34006312E-03, 0.50917797E-03,
         0.31558884E-03,-0.47379255E-03, 0.19488316E-03,-0.10691929E-03,
         0.23986807E-03,-0.44767602E-03,-0.61633931E-04, 0.10515519E-02,
         0.19887579E-03, 0.66788815E-03, 0.56220434E-03,-0.10900824E-03,
        -0.15555903E-04, 0.15904372E-03,-0.18477459E-03,-0.34277860E-03,
        -0.17079711E-03,-0.16689752E-03,-0.41316476E-03, 0.40749233E-03,
        -0.71256989E-04, 0.62765839E-03,-0.15361159E-03,-0.27445657E-03,
         0.65374636E-03, 0.17875159E-03, 0.46897805E-03, 0.66733320E-03,
        -0.12449190E-02, 0.36902056E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]                *x51
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]*x13                
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]*x11*x21    *x41*x51
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 17]        *x33*x42    
        +coeff[ 18]        *x33*x41*x51
        +coeff[ 19]    *x22        *x53
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]*x11*x21*x33        
        +coeff[ 22]*x11*x22*x31*x41    
        +coeff[ 23]*x11            *x54
        +coeff[ 24]*x12    *x31    *x52
        +coeff[ 25]*x13    *x32        
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 26]*x11*x22        *x53
        +coeff[ 27]*x12*x22*x32        
        +coeff[ 28]*x12*x22        *x52
        +coeff[ 29]*x12            *x54
        +coeff[ 30]    *x24*x31    *x52
        +coeff[ 31]        *x33*x42*x52
        +coeff[ 32]        *x32*x43*x52
        +coeff[ 33]        *x33*x41*x53
        +coeff[ 34]*x11*x23*x33        
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 35]*x11*x23*x32    *x51
        +coeff[ 36]*x11*x24    *x41*x51
        +coeff[ 37]*x11*x23*x31*x41*x51
        +coeff[ 38]*x11*x21*x33*x41*x51
        +coeff[ 39]*x11*x22    *x43*x51
        +coeff[ 40]*x11*x21*x33    *x52
        +coeff[ 41]*x11*x21*x31    *x54
        +coeff[ 42]*x12*x22*x32*x41    
        +coeff[ 43]*x12    *x33*x42    
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 44]*x13*x21*x33        
        +coeff[ 45]*x13*x21*x31*x41*x51
        +coeff[ 46]*x13    *x31*x41*x52
        +coeff[ 47]    *x23    *x43*x52
        +coeff[ 48]    *x21*x32*x43*x52
        +coeff[ 49]*x13*x21        *x53
        +coeff[ 50]    *x23    *x41*x54
        +coeff[ 51]            *x44*x54
        +coeff[ 52]    *x21            
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 53]*x11                
        +coeff[ 54]                *x52
        +coeff[ 55]*x11            *x51
        +coeff[ 56]    *x22*x31        
        +coeff[ 57]        *x32*x41    
        +coeff[ 58]    *x21    *x42    
        +coeff[ 59]    *x22        *x51
        +coeff[ 60]        *x32    *x51
        +coeff[ 61]    *x21        *x52
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 62]                *x53
        +coeff[ 63]*x11    *x31    *x51
        +coeff[ 64]*x11        *x41*x51
        +coeff[ 65]*x11            *x52
        +coeff[ 66]*x12        *x41    
        +coeff[ 67]    *x22*x31*x41    
        +coeff[ 68]        *x33*x41    
        +coeff[ 69]    *x22    *x42    
        +coeff[ 70]    *x22        *x52
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 71]        *x31    *x53
        +coeff[ 72]            *x41*x53
        +coeff[ 73]*x11*x21    *x42    
        +coeff[ 74]*x11*x21        *x52
        +coeff[ 75]*x12    *x32        
        +coeff[ 76]*x12        *x41*x51
        +coeff[ 77]*x13*x21            
        +coeff[ 78]    *x22*x33        
        +coeff[ 79]    *x21*x34        
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 80]    *x23*x31*x41    
        +coeff[ 81]    *x21*x32*x42    
        +coeff[ 82]*x13            *x51
        +coeff[ 83]        *x34    *x51
        +coeff[ 84]    *x22*x31*x41*x51
        +coeff[ 85]    *x22    *x42*x51
        +coeff[ 86]        *x32*x42*x51
        +coeff[ 87]    *x23        *x52
        +coeff[ 88]    *x22*x31    *x52
    ;
    v_l_e_q2ex_1_350                              =v_l_e_q2ex_1_350                              
        +coeff[ 89]        *x33    *x52
        ;

    return v_l_e_q2ex_1_350                              ;
}
float x_e_q2ex_1_300                              (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.1106775E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.11388264E-02,-0.31354437E-02, 0.16886051E+00, 0.79367245E-02,
         0.32835689E-03,-0.14071891E-02,-0.39948318E-02,-0.28449444E-02,
         0.34712411E-04, 0.20757891E-03,-0.16775294E-02,-0.37828964E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_300                              =v_x_e_q2ex_1_300                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2ex_1_300                              ;
}
float t_e_q2ex_1_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3516573E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.36070042E-03,-0.23974399E-02, 0.53539120E-01, 0.23560068E-02,
         0.17543335E-03,-0.15774112E-03, 0.28278091E-03, 0.15782110E-04,
        -0.95667987E-04, 0.10006883E-03,-0.19360166E-03,-0.17393972E-02,
        -0.12291393E-02,-0.62861283E-04,-0.80205267E-04,-0.57492769E-04,
        -0.61662711E-03,-0.13821174E-02,-0.10477422E-02,-0.37853850E-04,
         0.71415001E-04, 0.17982992E-03, 0.15656890E-03, 0.30733299E-04,
        -0.64567663E-03,-0.17877657E-04,-0.17119901E-04, 0.10165133E-04,
        -0.10104734E-05, 0.19882125E-05,-0.66961211E-05,-0.20788402E-04,
         0.93097529E-04,-0.10211728E-04,-0.61742918E-04, 0.23339699E-04,
        -0.45463308E-04, 0.59201520E-04, 0.21206362E-04, 0.20320664E-04,
        -0.36806498E-05, 0.39370434E-06,-0.20032289E-05, 0.17234865E-05,
        -0.40910313E-05, 0.36500257E-05, 0.54454549E-05, 0.52883479E-05,
        -0.27511687E-05,-0.62187760E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_300                              =v_t_e_q2ex_1_300                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23*x32        
    ;
    v_t_e_q2ex_1_300                              =v_t_e_q2ex_1_300                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21        *x53
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x12*x21*x32    *x51
    ;
    v_t_e_q2ex_1_300                              =v_t_e_q2ex_1_300                              
        +coeff[ 26]    *x23*x32    *x51
        +coeff[ 27]    *x22            
        +coeff[ 28]            *x42    
        +coeff[ 29]    *x22    *x41    
        +coeff[ 30]*x11            *x52
        +coeff[ 31]    *x22    *x42    
        +coeff[ 32]    *x21*x32    *x51
        +coeff[ 33]*x12*x23            
        +coeff[ 34]*x11*x22*x31*x41    
    ;
    v_t_e_q2ex_1_300                              =v_t_e_q2ex_1_300                              
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]    *x21*x31*x43*x51
        +coeff[ 38]*x13*x21        *x52
        +coeff[ 39]*x11    *x31*x41*x53
        +coeff[ 40]*x11*x21            
        +coeff[ 41]        *x32        
        +coeff[ 42]        *x31*x41    
        +coeff[ 43]        *x31    *x51
    ;
    v_t_e_q2ex_1_300                              =v_t_e_q2ex_1_300                              
        +coeff[ 44]*x13                
        +coeff[ 45]    *x22*x31        
        +coeff[ 46]    *x21*x31    *x51
        +coeff[ 47]    *x21    *x41*x51
        +coeff[ 48]            *x41*x52
        +coeff[ 49]*x12    *x31*x41    
        ;

    return v_t_e_q2ex_1_300                              ;
}
float y_e_q2ex_1_300                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.9123285E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.90330472E-03, 0.21405996E+00, 0.13185164E+00,-0.21536921E-02,
        -0.12254223E-02,-0.65008376E-03,-0.41744037E-03,-0.28881748E-03,
        -0.24879453E-04, 0.61816994E-04,-0.12538944E-04,-0.11369820E-03,
         0.10683899E-04, 0.24022086E-05,-0.54007687E-03, 0.17428265E-04,
        -0.15991855E-03,-0.81818602E-04,-0.20499383E-04,-0.28047185E-04,
         0.99498568E-04, 0.84459454E-04,-0.37166078E-03,-0.33876344E-03,
        -0.94578627E-05, 0.24430277E-04,-0.46566690E-03,-0.47265661E-04,
        -0.53688983E-03,-0.14458448E-04,-0.22684810E-03,-0.12996040E-03,
        -0.13238889E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_300                              =v_y_e_q2ex_1_300                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]        *x33*x42    
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q2ex_1_300                              =v_y_e_q2ex_1_300                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x45    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x24    *x41    
        +coeff[ 23]        *x31*x42    
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]            *x41*x52
    ;
    v_y_e_q2ex_1_300                              =v_y_e_q2ex_1_300                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]*x11        *x41*x52
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]*x11*x23*x31        
        ;

    return v_y_e_q2ex_1_300                              ;
}
float p_e_q2ex_1_300                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.5276171E-04;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.50917450E-04,-0.18636337E-01,-0.16590448E-01, 0.18020105E-02,
         0.28128973E-02,-0.14272936E-02,-0.88572933E-03,-0.15481191E-02,
        -0.83873415E-03,-0.61956180E-05,-0.19658911E-03,-0.94821880E-03,
        -0.14575693E-03, 0.89390898E-06, 0.36548601E-04,-0.11277069E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_300                              =v_p_e_q2ex_1_300                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_1_300                              ;
}
float l_e_q2ex_1_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2647417E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25347143E-02,-0.37803957E-02,-0.12645712E-02,-0.42878180E-02,
        -0.51748143E-02, 0.51226729E-03, 0.36473514E-03,-0.41560282E-03,
        -0.12756378E-02,-0.38947098E-03,-0.10141756E-02,-0.73553785E-03,
         0.58081659E-03,-0.90501802E-04, 0.81094557E-04,-0.51250507E-03,
         0.41491361E-03, 0.10894374E-02, 0.37204481E-04, 0.10279897E-02,
         0.11652777E-02, 0.76093979E-03, 0.14516570E-03, 0.43501498E-03,
         0.67080953E-03, 0.49078569E-03,-0.57774014E-03,-0.45436635E-03,
         0.26430472E-03,-0.98313694E-03, 0.55815623E-03, 0.95481164E-03,
        -0.22170646E-02, 0.23814053E-02, 0.62144746E-03,-0.71993208E-03,
        -0.15442779E-02,-0.85065048E-03, 0.16275597E-03,-0.45426225E-03,
         0.17214189E-02,-0.72226604E-03,-0.10687205E-02, 0.15970240E-02,
        -0.21826157E-02, 0.14819766E-02, 0.18399790E-02, 0.11513808E-02,
         0.20473743E-03, 0.16582175E-02,-0.97452430E-03,-0.13818025E-02,
        -0.12600748E-03,-0.89200752E-04, 0.72866892E-05,-0.15136554E-03,
         0.10757245E-03,-0.12700159E-04,-0.44983596E-03,-0.35352277E-03,
        -0.20260007E-03, 0.15033521E-03, 0.57349684E-04,-0.15600950E-03,
        -0.51973306E-03,-0.23767367E-03,-0.65569096E-04, 0.12226227E-03,
         0.72119496E-04, 0.13030958E-03, 0.11827352E-03,-0.43458416E-03,
         0.28000606E-03,-0.43658115E-03,-0.23029884E-03, 0.18417045E-03,
         0.25230509E-03, 0.12445945E-03,-0.34741522E-03,-0.38004247E-03,
        -0.26242450E-03,-0.22547561E-03,-0.20174689E-03, 0.14206124E-03,
         0.18223854E-03,-0.16222913E-03,-0.22549981E-03, 0.35342589E-03,
         0.69100736E-03,-0.35462069E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]    *x21    *x42    
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[  8]*x12*x21*x31*x41    
        +coeff[  9]    *x21*x34*x41    
        +coeff[ 10]    *x24    *x42    
        +coeff[ 11]*x12*x24        *x51
        +coeff[ 12]*x13        *x42*x52
        +coeff[ 13]                *x51
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 17]            *x42*x51
        +coeff[ 18]                *x53
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11    *x31    *x51
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]    *x21    *x41*x52
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]*x11    *x32*x41    
        +coeff[ 25]*x11    *x31*x42    
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 26]*x12    *x31*x41    
        +coeff[ 27]*x13*x21            
        +coeff[ 28]    *x21*x33    *x51
        +coeff[ 29]            *x44*x51
        +coeff[ 30]    *x22        *x53
        +coeff[ 31]*x11    *x34        
        +coeff[ 32]*x11    *x31*x43    
        +coeff[ 33]*x11*x21*x31*x41*x51
        +coeff[ 34]*x11*x21    *x42*x51
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 35]*x12*x21    *x42    
        +coeff[ 36]*x13    *x31    *x51
        +coeff[ 37]*x13        *x41*x51
        +coeff[ 38]    *x24    *x41*x51
        +coeff[ 39]*x12*x24            
        +coeff[ 40]    *x23*x33*x41    
        +coeff[ 41]    *x24    *x43    
        +coeff[ 42]    *x24*x31    *x52
        +coeff[ 43]*x11    *x33*x43    
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 44]*x11*x21*x31*x43*x51
        +coeff[ 45]*x11*x21    *x43*x52
        +coeff[ 46]*x12*x22*x31    *x52
        +coeff[ 47]*x12*x22    *x41*x52
        +coeff[ 48]*x13*x23*x31        
        +coeff[ 49]    *x21*x34*x43    
        +coeff[ 50]    *x21    *x43*x54
        +coeff[ 51]        *x31*x43*x54
        +coeff[ 52]            *x44*x54
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 53]*x11                
        +coeff[ 54]    *x21*x31        
        +coeff[ 55]        *x31    *x51
        +coeff[ 56]                *x52
        +coeff[ 57]*x11        *x41    
        +coeff[ 58]    *x21*x32        
        +coeff[ 59]    *x21*x31*x41    
        +coeff[ 60]        *x31*x42    
        +coeff[ 61]        *x32    *x51
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 62]        *x31    *x52
        +coeff[ 63]            *x41*x52
        +coeff[ 64]*x11    *x32        
        +coeff[ 65]*x11*x21    *x41    
        +coeff[ 66]*x11        *x42    
        +coeff[ 67]*x11            *x52
        +coeff[ 68]*x12        *x41    
        +coeff[ 69]*x12            *x51
        +coeff[ 70]*x13                
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 71]    *x22*x32        
        +coeff[ 72]        *x34        
        +coeff[ 73]    *x22*x31*x41    
        +coeff[ 74]        *x32*x42    
        +coeff[ 75]    *x23        *x51
        +coeff[ 76]    *x21*x31*x41*x51
        +coeff[ 77]            *x43*x51
        +coeff[ 78]    *x22        *x52
        +coeff[ 79]            *x42*x52
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 80]    *x21        *x53
        +coeff[ 81]*x11*x21*x32        
        +coeff[ 82]*x11*x21    *x42    
        +coeff[ 83]*x11*x21*x31    *x51
        +coeff[ 84]*x11*x21        *x52
        +coeff[ 85]*x11        *x41*x52
        +coeff[ 86]*x12*x21*x31        
        +coeff[ 87]*x12*x21        *x51
        +coeff[ 88]    *x21*x34        
    ;
    v_l_e_q2ex_1_300                              =v_l_e_q2ex_1_300                              
        +coeff[ 89]    *x21*x31*x43    
        ;

    return v_l_e_q2ex_1_300                              ;
}
float x_e_q2ex_1_250                              (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.1821974E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.18593192E-02,-0.31089091E-02, 0.16970815E+00, 0.79078153E-02,
         0.32897491E-03,-0.13890712E-02,-0.39995085E-02,-0.28371233E-02,
        -0.89768118E-05, 0.18990827E-03,-0.16672197E-02,-0.36872292E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_250                              =v_x_e_q2ex_1_250                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2ex_1_250                              ;
}
float t_e_q2ex_1_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5855158E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59697527E-03,-0.23896780E-02, 0.53960960E-01, 0.23450961E-02,
         0.17657156E-03,-0.16287489E-03, 0.27346323E-03,-0.11369475E-04,
        -0.84160856E-04, 0.98815923E-04,-0.18623537E-03,-0.53253905E-04,
        -0.17283804E-02,-0.11861834E-02,-0.63957421E-04,-0.45642410E-04,
         0.65403015E-04,-0.61674486E-03,-0.13421159E-02,-0.10066780E-02,
        -0.29604533E-04, 0.18345097E-03, 0.16486696E-03,-0.61962171E-03,
        -0.24280789E-04, 0.32178810E-04, 0.58474750E-04, 0.31632661E-06,
         0.11341116E-04, 0.81926577E-04, 0.10365638E-04,-0.31412354E-04,
        -0.14735499E-03,-0.11446200E-03, 0.27140541E-04, 0.17924267E-04,
        -0.16467076E-04, 0.21541815E-04, 0.11973435E-05, 0.11543557E-05,
         0.54557627E-05, 0.53055578E-05,-0.44445192E-05, 0.82421502E-05,
        -0.31094369E-05,-0.93281033E-05, 0.77126433E-05,-0.46104783E-05,
        -0.61184815E-05,-0.58470046E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_250                              =v_t_e_q2ex_1_250                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_250                              =v_t_e_q2ex_1_250                              
        +coeff[ 17]    *x23*x32        
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]*x12*x21*x32    *x51
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q2ex_1_250                              =v_t_e_q2ex_1_250                              
        +coeff[ 26]    *x23*x31*x41*x51
        +coeff[ 27]*x11        *x41    
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x12*x23            
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]    *x21*x31*x42*x51
    ;
    v_t_e_q2ex_1_250                              =v_t_e_q2ex_1_250                              
        +coeff[ 35]*x11*x23    *x41*x51
        +coeff[ 36]*x12*x22        *x52
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]                *x51
        +coeff[ 39]    *x22            
        +coeff[ 40]*x11*x21    *x41    
        +coeff[ 41]    *x21    *x41*x51
        +coeff[ 42]*x11            *x52
        +coeff[ 43]    *x22*x32        
    ;
    v_t_e_q2ex_1_250                              =v_t_e_q2ex_1_250                              
        +coeff[ 44]    *x21*x33        
        +coeff[ 45]*x11*x22    *x41    
        +coeff[ 46]    *x22*x31*x41    
        +coeff[ 47]    *x22*x31    *x51
        +coeff[ 48]*x11    *x32    *x51
        +coeff[ 49]*x11        *x41*x52
        ;

    return v_t_e_q2ex_1_250                              ;
}
float y_e_q2ex_1_250                              (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.9625409E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.94341359E-03, 0.21361825E+00, 0.13152139E+00,-0.21386943E-02,
        -0.11943023E-02,-0.60376863E-03,-0.41060167E-03,-0.28099338E-03,
        -0.44527318E-04, 0.52464282E-04,-0.40769144E-04,-0.14400481E-03,
        -0.90050293E-04,-0.12826948E-04,-0.52922277E-03,-0.32733995E-04,
        -0.20608306E-03,-0.72975032E-04,-0.12210736E-04, 0.20282399E-04,
         0.84803236E-04,-0.42254213E-03,-0.54953888E-03,-0.23841529E-04,
        -0.25340266E-03,-0.30027318E-03,-0.30502459E-03,-0.29930953E-03,
         0.14094015E-04, 0.81066895E-06,-0.15204681E-04, 0.14220223E-04,
        -0.26086596E-04, 0.20797866E-04,-0.41412064E-03,-0.93571041E-04,
        -0.34697783E-04,-0.91278023E-04,-0.43306834E-04,-0.30914645E-04,
         0.84190440E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_250                              =v_y_e_q2ex_1_250                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]        *x33*x42    
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q2ex_1_250                              =v_y_e_q2ex_1_250                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x24    *x41    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]    *x22*x31*x44    
    ;
    v_y_e_q2ex_1_250                              =v_y_e_q2ex_1_250                              
        +coeff[ 26]    *x22*x32*x45    
        +coeff[ 27]        *x31*x42    
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]            *x42*x51
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]            *x41*x52
        +coeff[ 32]            *x43*x51
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_y_e_q2ex_1_250                              =v_y_e_q2ex_1_250                              
        +coeff[ 35]*x11*x23    *x41    
        +coeff[ 36]    *x21    *x42*x52
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]*x11*x21    *x41*x52
        +coeff[ 39]    *x21*x31*x45    
        +coeff[ 40]    *x24    *x41*x51
        ;

    return v_y_e_q2ex_1_250                              ;
}
float p_e_q2ex_1_250                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1200058E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.11796227E-03,-0.18661812E-01,-0.16617645E-01, 0.17985315E-02,
         0.28095851E-02,-0.14362152E-02,-0.89295977E-03,-0.15392012E-02,
        -0.83068415E-03,-0.87881863E-05,-0.19897017E-03,-0.94295095E-03,
        -0.14657574E-03, 0.62608351E-05,-0.11483527E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_250                              =v_p_e_q2ex_1_250                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_250                              ;
}
float l_e_q2ex_1_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2617699E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25173696E-02,-0.42006802E-02,-0.14481290E-02,-0.46947012E-02,
        -0.52370545E-02,-0.39501261E-04, 0.58900396E-03,-0.12422676E-03,
         0.82205480E-03, 0.58536034E-03, 0.11102393E-03, 0.32035218E-03,
         0.17903778E-05, 0.34178983E-03, 0.35083358E-03,-0.13442273E-03,
        -0.38293737E-03, 0.98572811E-03, 0.20292182E-03, 0.45670196E-03,
         0.47026505E-03, 0.55658526E-03,-0.44308670E-03, 0.22254074E-03,
         0.24068780E-03, 0.17590635E-03,-0.44885129E-03,-0.47059191E-03,
        -0.44049171E-03,-0.11481467E-02,-0.84617629E-03, 0.12021699E-02,
        -0.48143740E-03, 0.27640932E-02,-0.12888647E-03,-0.54400996E-03,
        -0.29142437E-03, 0.53590903E-03, 0.11244257E-02,-0.15540043E-02,
        -0.34529902E-03, 0.38501658E-03,-0.61227073E-03, 0.66730985E-03,
        -0.23064204E-02,-0.14021598E-02, 0.39905272E-03, 0.13830668E-02,
        -0.38392248E-02, 0.13231643E-02,-0.96690736E-03, 0.24404656E-03,
        -0.38021512E-03,-0.19888392E-03, 0.59154958E-04, 0.25996010E-03,
         0.16478942E-03,-0.17248214E-04,-0.21176242E-03,-0.29289885E-03,
         0.52490481E-03,-0.47879104E-03, 0.27050590E-03,-0.18766448E-03,
        -0.18976771E-03,-0.11849582E-03, 0.13178270E-03, 0.18085327E-03,
        -0.16191734E-03, 0.21363652E-03, 0.36485915E-03, 0.27473460E-03,
         0.58616424E-03, 0.27341367E-03, 0.11108082E-02, 0.40763596E-03,
        -0.32111313E-03, 0.30199098E-03,-0.13156045E-03,-0.11609084E-03,
        -0.29680622E-03,-0.28069422E-03, 0.30396946E-03,-0.32050695E-03,
         0.82004175E-03, 0.69724768E-03,-0.26952403E-03,-0.45783672E-03,
         0.26135155E-03, 0.28313103E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11        *x41    
        +coeff[  6]            *x42*x51
        +coeff[  7]*x11*x21    *x41    
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[  8]        *x31*x42*x51
        +coeff[  9]*x11*x21        *x52
        +coeff[ 10]    *x24        *x51
        +coeff[ 11]            *x43    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]                *x53
        +coeff[ 16]*x11    *x32        
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 17]        *x32    *x52
        +coeff[ 18]            *x42*x52
        +coeff[ 19]*x11*x22*x31        
        +coeff[ 20]*x11*x22    *x41    
        +coeff[ 21]*x11    *x32*x41    
        +coeff[ 22]*x11        *x43    
        +coeff[ 23]*x11            *x53
        +coeff[ 24]*x12*x22            
        +coeff[ 25]*x12*x21        *x51
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 26]*x12            *x52
        +coeff[ 27]    *x21    *x43*x51
        +coeff[ 28]    *x23        *x52
        +coeff[ 29]*x11    *x33*x41    
        +coeff[ 30]*x11    *x33    *x51
        +coeff[ 31]*x11*x22    *x41*x51
        +coeff[ 32]*x11    *x32*x41*x51
        +coeff[ 33]*x11*x21    *x42*x51
        +coeff[ 34]*x11*x21    *x41*x52
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 35]*x12        *x43    
        +coeff[ 36]*x12*x22        *x51
        +coeff[ 37]    *x22*x33*x41    
        +coeff[ 38]    *x21*x32*x41*x52
        +coeff[ 39]        *x32    *x54
        +coeff[ 40]*x11*x24        *x51
        +coeff[ 41]*x12*x22*x32        
        +coeff[ 42]    *x21*x33    *x53
        +coeff[ 43]*x11*x24*x31    *x51
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 44]*x11*x23*x32    *x51
        +coeff[ 45]*x11*x21*x32*x41*x52
        +coeff[ 46]*x11*x23        *x53
        +coeff[ 47]*x13*x21*x32    *x51
        +coeff[ 48]*x13*x21    *x42*x51
        +coeff[ 49]*x13*x21        *x53
        +coeff[ 50]        *x33*x42*x53
        +coeff[ 51]*x11                
        +coeff[ 52]    *x21*x31        
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 53]    *x21    *x41    
        +coeff[ 54]            *x41*x51
        +coeff[ 55]                *x52
        +coeff[ 56]    *x23            
        +coeff[ 57]        *x33        
        +coeff[ 58]    *x22    *x41    
        +coeff[ 59]    *x22        *x51
        +coeff[ 60]*x11    *x31*x41    
        +coeff[ 61]*x11*x21        *x51
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 62]*x11    *x31    *x51
        +coeff[ 63]*x11        *x41*x51
        +coeff[ 64]*x11            *x52
        +coeff[ 65]*x12*x21            
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]*x13                
        +coeff[ 69]    *x23*x31        
        +coeff[ 70]    *x21*x33        
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 71]        *x34        
        +coeff[ 72]    *x21*x32*x41    
        +coeff[ 73]        *x33*x41    
        +coeff[ 74]    *x21*x31*x42    
        +coeff[ 75]    *x21    *x43    
        +coeff[ 76]    *x22        *x52
        +coeff[ 77]*x11    *x33        
        +coeff[ 78]*x11*x21    *x42    
        +coeff[ 79]*x11    *x31*x42    
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 80]*x11    *x31    *x52
        +coeff[ 81]*x11        *x41*x52
        +coeff[ 82]*x12    *x32        
        +coeff[ 83]    *x22*x33        
        +coeff[ 84]    *x21*x33*x41    
        +coeff[ 85]    *x21*x32*x42    
        +coeff[ 86]        *x33*x42    
        +coeff[ 87]        *x32*x43    
        +coeff[ 88]    *x23    *x41*x51
    ;
    v_l_e_q2ex_1_250                              =v_l_e_q2ex_1_250                              
        +coeff[ 89]    *x21*x32    *x52
        ;

    return v_l_e_q2ex_1_250                              ;
}
float x_e_q2ex_1_200                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.2196377E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.26569734E-03,-0.30723561E-02, 0.17089079E+00, 0.78534568E-02,
         0.32549715E-03,-0.12242604E-02,-0.32056731E-02,-0.21784264E-02,
         0.91697439E-05,-0.14599773E-04,-0.22729555E-05,-0.16461024E-02,
        -0.36361310E-03,-0.18010653E-02,-0.14468657E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_200                              =v_x_e_q2ex_1_200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_200                              ;
}
float t_e_q2ex_1_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7659504E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.91627684E-04,-0.23680523E-02, 0.54591328E-01, 0.23140754E-02,
         0.17306187E-03,-0.15578128E-03, 0.29161657E-03,-0.14035117E-04,
        -0.80963931E-04, 0.91837268E-04,-0.18059708E-03,-0.66091729E-04,
        -0.17291728E-02,-0.11767057E-02,-0.73154893E-04,-0.48837584E-04,
         0.19455815E-03, 0.12808853E-03,-0.60856756E-03,-0.13663322E-02,
        -0.10429040E-02,-0.18360144E-04,-0.35969031E-04, 0.83039558E-04,
         0.88877518E-04,-0.63064077E-03, 0.94956404E-05,-0.97937600E-05,
         0.82400038E-05,-0.99611079E-05, 0.10189354E-04,-0.65616863E-04,
         0.21627799E-04,-0.38261704E-04,-0.20995496E-04, 0.48491569E-06,
        -0.10636387E-04,-0.10823836E-04, 0.85088359E-05,-0.63670082E-05,
         0.73729443E-05,-0.53037925E-05, 0.10888024E-04, 0.45828633E-05,
         0.78427147E-05, 0.12321029E-04, 0.17564495E-04,-0.16561808E-04,
         0.16229682E-04,-0.70287846E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_200                              =v_t_e_q2ex_1_200                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q2ex_1_200                              =v_t_e_q2ex_1_200                              
        +coeff[ 17]    *x21    *x42*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q2ex_1_200                              =v_t_e_q2ex_1_200                              
        +coeff[ 26]*x11*x21    *x41    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x12*x21        *x51
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]    *x22        *x53
        +coeff[ 33]*x12*x23        *x51
        +coeff[ 34]*x11*x22*x31    *x52
    ;
    v_t_e_q2ex_1_200                              =v_t_e_q2ex_1_200                              
        +coeff[ 35]                *x52
        +coeff[ 36]    *x22        *x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]    *x21*x31*x42    
        +coeff[ 39]*x12    *x31    *x51
        +coeff[ 40]*x11    *x31*x41*x51
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_q2ex_1_200                              =v_t_e_q2ex_1_200                              
        +coeff[ 44]    *x21    *x41*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]*x11*x23*x31        
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q2ex_1_200                              ;
}
float y_e_q2ex_1_200                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.6999175E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.78628429E-04, 0.21314214E+00, 0.13099198E+00,-0.21178597E-02,
        -0.11975940E-02,-0.61253965E-03,-0.42080265E-03,-0.36349168E-03,
        -0.27793180E-03,-0.83380779E-04,-0.53368887E-04,-0.40918353E-05,
         0.16826887E-04,-0.52751671E-03,-0.19517961E-03,-0.90693531E-04,
         0.54338580E-04, 0.92929018E-04, 0.82515973E-04,-0.38622806E-03,
        -0.53789129E-03,-0.22156890E-03, 0.43239306E-04,-0.15200691E-03,
        -0.12065291E-04,-0.15608834E-03,-0.32069441E-03, 0.43176002E-04,
         0.24115725E-04,-0.77871344E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_200                              =v_y_e_q2ex_1_200                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q2ex_1_200                              =v_y_e_q2ex_1_200                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]            *x43*x52
        +coeff[ 23]    *x22*x31*x44    
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]    *x22    *x43    
    ;
    v_y_e_q2ex_1_200                              =v_y_e_q2ex_1_200                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]*x11*x23*x31        
        ;

    return v_y_e_q2ex_1_200                              ;
}
float p_e_q2ex_1_200                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.3945094E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.53053980E-04,-0.18695235E-01,-0.16669337E-01, 0.17942102E-02,
         0.28054970E-02,-0.14605086E-02,-0.90865151E-03,-0.15301614E-02,
        -0.82914019E-03,-0.18806593E-04,-0.19576905E-03,-0.92690520E-03,
        -0.14931121E-03, 0.27208412E-05,-0.11222436E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_200                              =v_p_e_q2ex_1_200                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_200                              ;
}
float l_e_q2ex_1_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2697841E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25929888E-02, 0.20307194E-04,-0.42527239E-02,-0.87988557E-03,
        -0.43015354E-02,-0.52542179E-02,-0.84839347E-04, 0.12649648E-03,
        -0.38770100E-02,-0.76452001E-04,-0.33628236E-04,-0.16409422E-03,
        -0.35006986E-04,-0.10025616E-04, 0.57783851E-03, 0.11183369E-03,
         0.54409058E-03, 0.27120629E-03, 0.33756494E-03, 0.26018104E-04,
         0.10003545E-02, 0.13824012E-02, 0.12409952E-02,-0.50342502E-03,
        -0.11522734E-02,-0.66253141E-03,-0.12224800E-02,-0.21118093E-02,
         0.63050684E-04,-0.10906507E-02,-0.46388074E-03,-0.61853317E-03,
        -0.13412557E-02,-0.32439649E-02,-0.13760861E-02,-0.74241933E-03,
         0.39162714E-03,-0.12137501E-02, 0.28781926E-02, 0.10655688E-03,
         0.28514897E-02, 0.50401693E-03, 0.20817204E-02,-0.10460999E-02,
         0.18436372E-02,-0.62802579E-03,-0.93925704E-03,-0.11373651E-02,
        -0.11517104E-02, 0.13761725E-02,-0.11190657E-02, 0.17450984E-02,
         0.65940124E-03,-0.69874313E-05, 0.19180913E-03,-0.27764647E-03,
         0.33906588E-04,-0.20394674E-03, 0.10518301E-03, 0.64451560E-04,
        -0.11809191E-02, 0.10406671E-03, 0.14633354E-03, 0.69771340E-04,
         0.86413849E-04,-0.12606524E-03, 0.28151914E-03,-0.15610590E-03,
         0.35098518E-03,-0.19842638E-03, 0.54601853E-03,-0.46488552E-03,
         0.10219550E-03,-0.34036225E-03,-0.19219930E-03, 0.18866430E-03,
         0.19657159E-03,-0.12472516E-03, 0.91767579E-03, 0.10095751E-02,
         0.29930365E-03, 0.96621327E-04,-0.25411317E-03,-0.10201093E-03,
         0.25066541E-03, 0.33391578E-03, 0.25422362E-03,-0.20267803E-03,
        -0.15149471E-03,-0.19483556E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22        *x51
        +coeff[  7]*x11*x22*x31*x42    
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[  8]        *x34*x42*x52
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x12                
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]*x12    *x31        
        +coeff[ 15]    *x23        *x51
        +coeff[ 16]    *x21        *x53
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 17]*x11    *x31*x41*x51
        +coeff[ 18]*x13    *x31        
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x34        
        +coeff[ 21]    *x22*x31    *x52
        +coeff[ 22]    *x21*x32    *x52
        +coeff[ 23]    *x21*x31*x41*x52
        +coeff[ 24]*x11*x21    *x42*x51
        +coeff[ 25]*x11*x21*x31    *x52
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 26]*x11        *x42*x52
        +coeff[ 27]*x12*x22*x31        
        +coeff[ 28]*x12*x22        *x51
        +coeff[ 29]*x12        *x42*x51
        +coeff[ 30]*x12    *x31    *x52
        +coeff[ 31]    *x21*x33    *x52
        +coeff[ 32]    *x22*x31*x41*x52
        +coeff[ 33]        *x33*x41*x52
        +coeff[ 34]            *x42*x54
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 35]*x11    *x31*x44    
        +coeff[ 36]*x11*x23        *x52
        +coeff[ 37]    *x22*x33    *x52
        +coeff[ 38]    *x23*x31*x41*x52
        +coeff[ 39]*x11*x24*x31*x41    
        +coeff[ 40]*x11*x22*x32*x42    
        +coeff[ 41]*x11    *x34*x41*x51
        +coeff[ 42]*x11    *x33*x41*x52
        +coeff[ 43]*x11    *x31*x41*x54
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 44]*x12*x24*x31        
        +coeff[ 45]*x12*x23*x31*x41    
        +coeff[ 46]*x12*x24        *x51
        +coeff[ 47]*x12*x21*x33    *x51
        +coeff[ 48]*x13*x22    *x42    
        +coeff[ 49]*x13*x21    *x42*x51
        +coeff[ 50]    *x24*x32    *x52
        +coeff[ 51]*x13        *x42*x52
        +coeff[ 52]    *x24    *x41*x53
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 53]    *x21            
        +coeff[ 54]    *x21*x31        
        +coeff[ 55]    *x21        *x51
        +coeff[ 56]                *x52
        +coeff[ 57]*x11        *x41    
        +coeff[ 58]*x11            *x51
        +coeff[ 59]    *x23            
        +coeff[ 60]    *x21*x32        
        +coeff[ 61]    *x22    *x41    
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 62]    *x21    *x42    
        +coeff[ 63]    *x21*x31    *x51
        +coeff[ 64]        *x32    *x51
        +coeff[ 65]    *x21    *x41*x51
        +coeff[ 66]            *x42*x51
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]*x12            *x51
        +coeff[ 69]    *x23*x31        
        +coeff[ 70]        *x33*x41    
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 71]        *x31*x43    
        +coeff[ 72]    *x22*x31    *x51
        +coeff[ 73]        *x33    *x51
        +coeff[ 74]    *x21    *x42*x51
        +coeff[ 75]        *x31*x42*x51
        +coeff[ 76]    *x22        *x52
        +coeff[ 77]        *x32    *x52
        +coeff[ 78]        *x31*x41*x52
        +coeff[ 79]            *x42*x52
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 80]        *x31    *x53
        +coeff[ 81]            *x41*x53
        +coeff[ 82]*x11*x22*x31        
        +coeff[ 83]*x11*x21*x32        
        +coeff[ 84]*x11    *x33        
        +coeff[ 85]*x11*x22    *x41    
        +coeff[ 86]*x11        *x43    
        +coeff[ 87]*x11*x22        *x51
        +coeff[ 88]*x11        *x41*x52
    ;
    v_l_e_q2ex_1_200                              =v_l_e_q2ex_1_200                              
        +coeff[ 89]*x12        *x42    
        ;

    return v_l_e_q2ex_1_200                              ;
}
float x_e_q2ex_1_175                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1003682E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.10364192E-02,-0.30421631E-02, 0.17177972E+00, 0.78137778E-02,
         0.32853486E-03,-0.12126244E-02,-0.32010453E-02,-0.21882693E-02,
        -0.24433564E-04,-0.12486499E-04, 0.96983549E-05,-0.16127129E-02,
        -0.36806977E-03,-0.17119063E-02,-0.13861299E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_175                              =v_x_e_q2ex_1_175                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_1_175                              ;
}
float t_e_q2ex_1_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3300512E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.34035125E-03,-0.23552359E-02, 0.55044655E-01, 0.22784292E-02,
         0.17346347E-03,-0.15730466E-03, 0.28493811E-03,-0.21849551E-05,
        -0.93800401E-04, 0.82572471E-04,-0.18586380E-03,-0.70812785E-04,
        -0.16947613E-02,-0.11394302E-02,-0.73329196E-04,-0.48870537E-04,
         0.84275940E-04, 0.12491467E-03, 0.11347442E-03,-0.58165443E-03,
        -0.12961640E-02,-0.99888258E-03, 0.26807398E-04,-0.33396856E-04,
        -0.56048329E-05, 0.49173839E-04,-0.60427375E-03, 0.58194992E-05,
         0.37350351E-05,-0.18062849E-04, 0.20397438E-04, 0.18819936E-04,
        -0.86234366E-04,-0.76482225E-04,-0.28721912E-04, 0.42316871E-04,
         0.56132551E-04, 0.53072166E-04, 0.51182724E-04,-0.14585541E-04,
        -0.15604177E-04, 0.85535401E-04, 0.22227414E-05, 0.21233036E-05,
         0.36284482E-05, 0.53041595E-05,-0.51495463E-05, 0.13575331E-05,
        -0.51413790E-05, 0.30113383E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_175                              =v_t_e_q2ex_1_175                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_175                              =v_t_e_q2ex_1_175                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x21*x32    *x53
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q2ex_1_175                              =v_t_e_q2ex_1_175                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]*x12*x21        *x51
        +coeff[ 29]*x11*x21    *x41*x51
        +coeff[ 30]    *x21        *x53
        +coeff[ 31]*x13*x22            
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x11*x22        *x52
    ;
    v_t_e_q2ex_1_175                              =v_t_e_q2ex_1_175                              
        +coeff[ 35]*x11*x23*x31    *x51
        +coeff[ 36]*x11*x23    *x41*x51
        +coeff[ 37]*x11*x21*x32*x41*x51
        +coeff[ 38]*x12*x21    *x42*x51
        +coeff[ 39]    *x22    *x43*x51
        +coeff[ 40]*x12*x22        *x52
        +coeff[ 41]    *x21*x31*x41*x53
        +coeff[ 42]*x11    *x31        
        +coeff[ 43]            *x42    
    ;
    v_t_e_q2ex_1_175                              =v_t_e_q2ex_1_175                              
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x31*x41*x51
        +coeff[ 48]            *x42*x51
        +coeff[ 49]            *x41*x52
        ;

    return v_t_e_q2ex_1_175                              ;
}
float y_e_q2ex_1_175                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.6827273E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.55330817E-03, 0.21271977E+00, 0.13063332E+00,-0.21087362E-02,
        -0.11565035E-02,-0.69945445E-03,-0.43373581E-03,-0.36375664E-03,
        -0.35585804E-03,-0.90808811E-04,-0.51014355E-04, 0.20512232E-04,
        -0.21702383E-04,-0.51857164E-03,-0.17065782E-03,-0.21954920E-03,
        -0.74846306E-04, 0.58338519E-04, 0.90725676E-04,-0.34042125E-03,
        -0.20118114E-03, 0.80842234E-04, 0.97576434E-04,-0.12150771E-04,
         0.51705642E-05, 0.21036674E-04,-0.37775724E-03,-0.34554541E-03,
         0.57504792E-04,-0.22106011E-04,-0.47862530E-04,-0.77753262E-04,
         0.13589850E-04, 0.37974798E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_1_175                              =v_y_e_q2ex_1_175                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x24*x32*x41    
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_1_175                              =v_y_e_q2ex_1_175                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]    *x24    *x41*x51
        +coeff[ 22]    *x22*x31*x44    
        +coeff[ 23]    *x21    *x43    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_y_e_q2ex_1_175                              =v_y_e_q2ex_1_175                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]        *x34*x41    
        +coeff[ 29]*x11        *x43*x51
        +coeff[ 30]        *x31*x42*x52
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x13        *x42    
        +coeff[ 33]*x11*x21    *x43*x51
        ;

    return v_y_e_q2ex_1_175                              ;
}
float p_e_q2ex_1_175                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.9832340E-04;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.87514149E-04,-0.18725103E-01,-0.16704710E-01, 0.17913247E-02,
         0.28014709E-02,-0.14701143E-02,-0.91326266E-03,-0.15152567E-02,
        -0.82333322E-03,-0.22331058E-04,-0.19333653E-03,-0.91219664E-03,
        -0.14536535E-03, 0.12629660E-05,-0.10931849E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_175                              =v_p_e_q2ex_1_175                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_175                              ;
}
float l_e_q2ex_1_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2632241E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25570679E-02,-0.42758835E-02,-0.48875931E-03,-0.45050005E-02,
        -0.54938719E-02,-0.65311824E-03,-0.19583432E-02, 0.14069681E-03,
         0.55284447E-04,-0.18044333E-03,-0.18408163E-03, 0.15750885E-03,
         0.11503455E-03,-0.19549826E-03, 0.36294915E-03, 0.20538797E-03,
        -0.66924898E-04, 0.64283237E-03,-0.14726137E-02, 0.13986799E-03,
        -0.26480795E-03, 0.51077461E-03, 0.22004104E-03, 0.74186083E-03,
        -0.17611211E-03,-0.27417808E-02,-0.29673061E-03,-0.12088895E-02,
        -0.74858207E-03,-0.18206448E-03, 0.52978215E-03,-0.21534743E-03,
         0.75651106E-03, 0.41903625E-03,-0.79660174E-04,-0.20050227E-02,
         0.64055802E-03, 0.17551276E-04,-0.11674726E-02, 0.67428412E-03,
        -0.70747762E-03, 0.20024437E-02, 0.86672639E-03, 0.10160764E-02,
         0.15243778E-02,-0.92918170E-03,-0.16760990E-02, 0.10661059E-02,
         0.25778546E-03, 0.19270587E-02,-0.63325075E-03,-0.86147804E-03,
         0.45001740E-03,-0.50709525E-03,-0.99549571E-03, 0.16078230E-02,
        -0.64929447E-03,-0.13712021E-02, 0.18991431E-02, 0.13668593E-02,
        -0.29414219E-02, 0.56111783E-03, 0.19652667E-02,-0.10493972E-02,
         0.15138157E-02,-0.92887605E-03,-0.20599130E-02, 0.18130176E-02,
         0.31979147E-02,-0.30502717E-02, 0.26188605E-02,-0.44024717E-02,
         0.10598082E-02,-0.20984435E-02,-0.14601921E-02, 0.85393625E-03,
         0.26458621E-02,-0.16107054E-02, 0.66500576E-03,-0.88471739E-03,
         0.14568300E-02, 0.30235882E-04, 0.26412841E-03, 0.20428245E-03,
         0.51172465E-04, 0.11867356E-03, 0.22543331E-03, 0.10769874E-03,
         0.35568629E-03,-0.13188587E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x34        
        +coeff[  6]*x11*x21*x31    *x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[  8]            *x41    
        +coeff[  9]                *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]*x12            *x51
        +coeff[ 16]    *x23        *x51
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x41*x52
        +coeff[ 19]*x11*x23            
        +coeff[ 20]*x11*x22*x31        
        +coeff[ 21]*x11        *x42*x51
        +coeff[ 22]*x11*x21        *x52
        +coeff[ 23]*x13*x21            
        +coeff[ 24]*x13        *x41    
        +coeff[ 25]    *x21*x32    *x52
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]*x11*x22    *x41*x51
        +coeff[ 28]*x11*x21    *x42*x51
        +coeff[ 29]*x11*x21        *x53
        +coeff[ 30]*x12    *x32*x41    
        +coeff[ 31]*x12*x22        *x51
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]    *x23*x33        
        +coeff[ 34]*x13    *x31*x41    
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 35]    *x23*x32*x41    
        +coeff[ 36]    *x22*x32*x42    
        +coeff[ 37]    *x23*x32    *x51
        +coeff[ 38]*x13        *x41*x51
        +coeff[ 39]        *x33*x41*x52
        +coeff[ 40]    *x21*x32    *x53
        +coeff[ 41]    *x21    *x41*x54
        +coeff[ 42]            *x42*x54
        +coeff[ 43]*x11*x23*x31    *x51
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 44]*x11*x21*x33    *x51
        +coeff[ 45]*x11*x22    *x42*x51
        +coeff[ 46]*x11*x23        *x52
        +coeff[ 47]*x11*x21        *x54
        +coeff[ 48]*x11        *x41*x54
        +coeff[ 49]*x12*x21*x32    *x51
        +coeff[ 50]*x12    *x31*x41*x52
        +coeff[ 51]*x12*x21        *x53
        +coeff[ 52]    *x23*x34        
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 53]    *x23    *x43*x51
        +coeff[ 54]*x13*x21        *x52
        +coeff[ 55]    *x21*x34    *x52
        +coeff[ 56]    *x21*x31*x43*x52
        +coeff[ 57]*x11*x24    *x42    
        +coeff[ 58]*x11*x24*x31    *x51
        +coeff[ 59]*x11*x22*x32*x41*x51
        +coeff[ 60]*x11*x22*x31*x42*x51
        +coeff[ 61]*x11    *x31*x41*x54
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 62]*x12*x22    *x42*x51
        +coeff[ 63]*x12*x23        *x52
        +coeff[ 64]*x12*x21*x32    *x52
        +coeff[ 65]*x12        *x42*x53
        +coeff[ 66]*x13*x22*x31*x41    
        +coeff[ 67]    *x23*x32*x43    
        +coeff[ 68]*x13*x22    *x41*x51
        +coeff[ 69]    *x23*x33    *x52
        +coeff[ 70]    *x24*x31*x41*x52
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 71]    *x22*x33*x41*x52
        +coeff[ 72]    *x22*x32*x42*x52
        +coeff[ 73]        *x34*x42*x52
        +coeff[ 74]    *x22    *x44*x52
        +coeff[ 75]*x13*x21        *x53
        +coeff[ 76]    *x23*x32    *x53
        +coeff[ 77]    *x21*x34    *x53
        +coeff[ 78]        *x33*x42*x53
        +coeff[ 79]    *x22*x32    *x54
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 80]    *x21*x33    *x54
        +coeff[ 81]*x11                
        +coeff[ 82]    *x21    *x41    
        +coeff[ 83]    *x21        *x51
        +coeff[ 84]            *x41*x51
        +coeff[ 85]    *x21*x31    *x51
        +coeff[ 86]                *x53
        +coeff[ 87]*x11*x21*x31        
        +coeff[ 88]*x11        *x41*x51
    ;
    v_l_e_q2ex_1_175                              =v_l_e_q2ex_1_175                              
        +coeff[ 89]*x12        *x41    
        ;

    return v_l_e_q2ex_1_175                              ;
}
float x_e_q2ex_1_150                              (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.4174855E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.40141176E-03,-0.30044611E-02, 0.17306788E+00, 0.77684252E-02,
         0.32406600E-03,-0.13877167E-02,-0.39063948E-02,-0.27906776E-02,
        -0.19369105E-04, 0.23683588E-03,-0.16327368E-02,-0.34633811E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_1_150                              =v_x_e_q2ex_1_150                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2ex_1_150                              ;
}
float t_e_q2ex_1_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1351020E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12925237E-03,-0.23390807E-02, 0.55643328E-01, 0.22607530E-02,
         0.16674884E-03,-0.14384105E-03, 0.30532450E-03,-0.28021409E-05,
         0.10771019E-03,-0.57002564E-03,-0.16747712E-02,-0.11596666E-02,
        -0.69073139E-04,-0.17523021E-03,-0.87896449E-04,-0.67870809E-04,
        -0.34490622E-04,-0.13102720E-02,-0.10136068E-02,-0.34201865E-04,
         0.77061013E-04, 0.20394423E-03, 0.15144648E-03,-0.61147904E-03,
        -0.15948526E-04,-0.23024564E-04, 0.35472407E-04, 0.89179066E-05,
        -0.18219323E-04, 0.65388995E-04,-0.95105272E-04,-0.27988606E-04,
        -0.61735889E-04, 0.20856553E-05, 0.27463459E-05,-0.95181611E-06,
        -0.31839297E-05, 0.51489810E-05,-0.28839140E-05,-0.59335539E-05,
         0.81092076E-05,-0.65139329E-05,-0.67946553E-06, 0.78391358E-05,
         0.98900073E-05,-0.43720697E-05, 0.61217697E-05,-0.53456517E-06,
        -0.80081691E-05, 0.68310601E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_t_e_q2ex_1_150                              =v_t_e_q2ex_1_150                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]*x11*x22            
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q2ex_1_150                              =v_t_e_q2ex_1_150                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]*x12*x21*x32    *x51
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q2ex_1_150                              =v_t_e_q2ex_1_150                              
        +coeff[ 26]    *x21*x32    *x53
        +coeff[ 27]    *x21    *x41*x51
        +coeff[ 28]*x11*x21    *x42    
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x13        *x42    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x21            
        +coeff[ 34]        *x32        
    ;
    v_t_e_q2ex_1_150                              =v_t_e_q2ex_1_150                              
        +coeff[ 35]    *x21    *x41    
        +coeff[ 36]            *x41*x51
        +coeff[ 37]*x13                
        +coeff[ 38]*x11        *x41*x51
        +coeff[ 39]*x11            *x52
        +coeff[ 40]*x13*x21            
        +coeff[ 41]*x11*x23            
        +coeff[ 42]*x12*x21*x31        
        +coeff[ 43]    *x23*x31        
    ;
    v_t_e_q2ex_1_150                              =v_t_e_q2ex_1_150                              
        +coeff[ 44]    *x23    *x41    
        +coeff[ 45]    *x22    *x42    
        +coeff[ 46]*x13            *x51
        +coeff[ 47]*x12    *x31    *x51
        +coeff[ 48]    *x22*x31    *x51
        +coeff[ 49]*x11*x21    *x41*x51
        ;

    return v_t_e_q2ex_1_150                              ;
}
float y_e_q2ex_1_150                              (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.7184823E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.17076315E-03, 0.21221411E+00, 0.13014336E+00,-0.20905652E-02,
        -0.11630072E-02,-0.58432377E-03,-0.39412544E-03,-0.28200384E-03,
        -0.44635613E-04, 0.56645440E-04,-0.21854255E-04,-0.16418642E-03,
        -0.22334121E-04,-0.52418350E-03,-0.23098683E-03,-0.31535026E-04,
         0.34708614E-05,-0.87710214E-04,-0.17145822E-03,-0.32631689E-03,
        -0.80158483E-04,-0.51383071E-04, 0.86886073E-04, 0.76608456E-04,
        -0.50707813E-03,-0.38679785E-03,-0.51206432E-03, 0.10833535E-04,
        -0.15449135E-04,-0.96146476E-04,-0.34820609E-04, 0.31146115E-04,
        -0.93821189E-04, 0.35051398E-04,-0.12387746E-04, 0.46721976E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q2ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_150                              =v_y_e_q2ex_1_150                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22*x33        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]        *x33*x44    
    ;
    v_y_e_q2ex_1_150                              =v_y_e_q2ex_1_150                              
        +coeff[ 17]    *x24*x32*x41    
        +coeff[ 18]            *x43    
        +coeff[ 19]        *x31*x42    
        +coeff[ 20]        *x33        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_q2ex_1_150                              =v_y_e_q2ex_1_150                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11        *x43    
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]    *x22    *x44    
        +coeff[ 31]        *x32*x44    
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]        *x32*x41*x52
        +coeff[ 34]            *x42*x53
    ;
    v_y_e_q2ex_1_150                              =v_y_e_q2ex_1_150                              
        +coeff[ 35]*x13*x21    *x41    
        ;

    return v_y_e_q2ex_1_150                              ;
}
float p_e_q2ex_1_150                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.4737346E-05;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.42756587E-05,-0.18761506E-01,-0.16750379E-01, 0.17881069E-02,
         0.27998565E-02,-0.14881444E-02,-0.92049234E-03,-0.15079094E-02,
        -0.81951718E-03,-0.48977531E-05,-0.19112838E-03,-0.92180830E-03,
        -0.14258755E-03, 0.15092188E-05,-0.10887540E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_150                              =v_p_e_q2ex_1_150                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_150                              ;
}
float l_e_q2ex_1_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2607653E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26961458E-02, 0.14048387E-03,-0.44675088E-02,-0.15515545E-02,
        -0.46612080E-02,-0.58544152E-02,-0.50824642E-03, 0.11988899E-03,
         0.13554504E-03, 0.16337966E-03,-0.69153932E-03, 0.33277698E-03,
         0.20518077E-03,-0.26247255E-03,-0.58016909E-03, 0.31131585E-03,
         0.27328552E-03, 0.54046040E-03,-0.29940053E-03, 0.64661173E-03,
        -0.97516796E-03,-0.11537820E-02, 0.44495563E-03,-0.13587570E-02,
         0.30441963E-03,-0.93201065E-03,-0.36446241E-03,-0.16788008E-03,
        -0.23949717E-03,-0.43920279E-03,-0.87351812E-03, 0.29171415E-03,
         0.10035221E-03, 0.82197768E-03,-0.58950676E-03, 0.47278096E-03,
         0.31141592E-02, 0.31720742E-03, 0.83453755E-03,-0.22043299E-02,
        -0.80977177E-03, 0.30085814E-03, 0.33377350E-03, 0.63439767E-03,
         0.10079399E-02, 0.18239574E-02,-0.23910361E-02, 0.73758740E-03,
         0.19770830E-02,-0.11228758E-02,-0.30133496E-02,-0.28491083E-02,
         0.33243690E-02, 0.23226799E-02, 0.32313901E-02, 0.37124255E-03,
         0.13817415E-02, 0.54292806E-04,-0.32267726E-04, 0.49133654E-04,
         0.14310460E-03, 0.11924992E-03,-0.20619018E-03, 0.16531644E-03,
        -0.46433465E-03, 0.81423830E-04, 0.31573349E-03, 0.33602645E-03,
         0.38459289E-03, 0.18078313E-03, 0.61746733E-03, 0.50785451E-03,
        -0.83098497E-03, 0.33131026E-03, 0.54151303E-03, 0.24384478E-03,
        -0.17357149E-03,-0.40929369E-03,-0.45145082E-03,-0.13623682E-03,
        -0.10600321E-03,-0.31431112E-03, 0.24533013E-03, 0.26615891E-04,
        -0.16780510E-03, 0.14148757E-03,-0.22297424E-03, 0.39299284E-03,
        -0.19413685E-03,-0.19934626E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x41*x51
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[  8]                *x52
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]*x11    *x31*x41    
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x13                
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 17]            *x44    
        +coeff[ 18]        *x32*x41*x51
        +coeff[ 19]    *x22    *x43    
        +coeff[ 20]        *x32*x43    
        +coeff[ 21]    *x23*x31    *x51
        +coeff[ 22]    *x22*x31    *x52
        +coeff[ 23]    *x21*x31*x41*x52
        +coeff[ 24]    *x21    *x42*x52
        +coeff[ 25]        *x31*x42*x52
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 26]    *x21        *x54
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]*x11        *x44    
        +coeff[ 29]*x11*x22    *x41*x51
        +coeff[ 30]*x11*x21*x31*x41*x51
        +coeff[ 31]*x11    *x32    *x52
        +coeff[ 32]*x12*x21*x31*x41    
        +coeff[ 33]*x12*x21    *x41*x51
        +coeff[ 34]*x12        *x41*x52
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 35]*x13*x21        *x51
        +coeff[ 36]    *x24    *x41*x51
        +coeff[ 37]*x13            *x52
        +coeff[ 38]*x11*x21*x32    *x52
        +coeff[ 39]*x12*x21    *x42*x51
        +coeff[ 40]*x12*x21    *x41*x52
        +coeff[ 41]*x12*x21        *x53
        +coeff[ 42]*x13    *x33        
        +coeff[ 43]    *x23*x31*x43    
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 44]    *x23*x33    *x51
        +coeff[ 45]*x11    *x33*x41*x52
        +coeff[ 46]*x11    *x31*x41*x54
        +coeff[ 47]*x12    *x34*x41    
        +coeff[ 48]*x12*x22    *x42*x51
        +coeff[ 49]*x12    *x32*x42*x51
        +coeff[ 50]    *x24*x32*x41*x51
        +coeff[ 51]    *x24    *x43*x51
        +coeff[ 52]    *x22*x32*x43*x51
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 53]*x13    *x31*x41*x52
        +coeff[ 54]    *x22*x33*x41*x52
        +coeff[ 55]    *x21    *x44*x53
        +coeff[ 56]    *x22*x32    *x54
        +coeff[ 57]            *x41    
        +coeff[ 58]*x11                
        +coeff[ 59]    *x23            
        +coeff[ 60]    *x21*x32        
        +coeff[ 61]    *x21*x31*x41    
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 62]    *x21    *x42    
        +coeff[ 63]        *x32    *x51
        +coeff[ 64]*x11            *x52
        +coeff[ 65]*x12    *x31        
        +coeff[ 66]    *x23*x31        
        +coeff[ 67]    *x22*x32        
        +coeff[ 68]        *x34        
        +coeff[ 69]    *x21*x32*x41    
        +coeff[ 70]        *x32*x42    
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 71]        *x31*x43    
        +coeff[ 72]    *x22    *x41*x51
        +coeff[ 73]    *x21*x31*x41*x51
        +coeff[ 74]    *x21    *x42*x51
        +coeff[ 75]            *x43*x51
        +coeff[ 76]    *x21*x31    *x52
        +coeff[ 77]        *x32    *x52
        +coeff[ 78]        *x31*x41*x52
        +coeff[ 79]    *x21        *x53
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 80]*x11*x22*x31        
        +coeff[ 81]*x11*x21*x32        
        +coeff[ 82]*x11*x21    *x42    
        +coeff[ 83]*x11    *x31*x41*x51
        +coeff[ 84]*x11        *x42*x51
        +coeff[ 85]*x11        *x41*x52
        +coeff[ 86]*x12*x21*x31        
        +coeff[ 87]*x12*x21        *x51
        +coeff[ 88]*x12        *x41*x51
    ;
    v_l_e_q2ex_1_150                              =v_l_e_q2ex_1_150                              
        +coeff[ 89]*x13*x21            
        ;

    return v_l_e_q2ex_1_150                              ;
}
float x_e_q2ex_1_125                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4073821E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.42113694E-03,-0.29520364E-02, 0.17461799E+00, 0.76908641E-02,
         0.31946937E-03,-0.38439143E-02,-0.27408537E-02,-0.52283186E-03,
         0.21618555E-03,-0.11819735E-02,-0.16088356E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23        *x52
    ;
    v_x_e_q2ex_1_125                              =v_x_e_q2ex_1_125                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        ;

    return v_x_e_q2ex_1_125                              ;
}
float t_e_q2ex_1_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1219883E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12660568E-03,-0.23092269E-02, 0.56484308E-01, 0.21984016E-02,
         0.16707159E-03, 0.81300332E-05,-0.13273461E-03, 0.32239055E-03,
         0.10825067E-03,-0.26436662E-05,-0.56615815E-03,-0.16491456E-02,
        -0.11310881E-02,-0.75655968E-04,-0.77581310E-04,-0.17265420E-03,
        -0.83110317E-04,-0.60146838E-04,-0.13629835E-02,-0.10490771E-02,
        -0.31691663E-04,-0.11378656E-04, 0.70247501E-04, 0.15528107E-03,
         0.17903296E-03,-0.63867623E-03,-0.20322611E-05, 0.11680288E-04,
         0.64452215E-04, 0.30730898E-04, 0.21094043E-04,-0.90710564E-05,
        -0.29184264E-04,-0.21248867E-04,-0.16457619E-04, 0.68860703E-04,
        -0.54860779E-04, 0.95494363E-06,-0.30961719E-05, 0.26339660E-05,
        -0.52543510E-05, 0.40716668E-05, 0.77563845E-05, 0.63805328E-05,
        -0.51454085E-05, 0.84096591E-05,-0.44421895E-05, 0.68277463E-05,
         0.12330948E-04,-0.11462160E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23        *x52
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_t_e_q2ex_1_125                              =v_t_e_q2ex_1_125                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]*x12*x21*x32        
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2ex_1_125                              =v_t_e_q2ex_1_125                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]*x11            *x52
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q2ex_1_125                              =v_t_e_q2ex_1_125                              
        +coeff[ 26]*x12*x21*x32    *x51
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x12*x21    *x42    
        +coeff[ 31]*x11*x23        *x51
        +coeff[ 32]*x12*x21*x31    *x51
        +coeff[ 33]    *x21*x32    *x52
        +coeff[ 34]*x13*x21*x31    *x51
    ;
    v_t_e_q2ex_1_125                              =v_t_e_q2ex_1_125                              
        +coeff[ 35]    *x21*x31*x43*x51
        +coeff[ 36]    *x21    *x42*x53
        +coeff[ 37]                *x51
        +coeff[ 38]    *x22            
        +coeff[ 39]        *x32        
        +coeff[ 40]    *x22*x31        
        +coeff[ 41]        *x31*x42    
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q2ex_1_125                              =v_t_e_q2ex_1_125                              
        +coeff[ 44]*x12        *x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]*x12    *x31    *x51
        +coeff[ 47]*x11    *x31*x41*x51
        +coeff[ 48]*x13*x22            
        +coeff[ 49]*x11*x22*x32        
        ;

    return v_t_e_q2ex_1_125                              ;
}
float y_e_q2ex_1_125                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.4636277E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.48365202E-03, 0.21157067E+00, 0.12940398E+00,-0.20701103E-02,
        -0.11234148E-02,-0.63308934E-03,-0.44199702E-03,-0.27715263E-03,
        -0.58073572E-04, 0.54910557E-04,-0.88612374E-04,-0.11660958E-03,
         0.64615083E-05,-0.47210834E-03,-0.18199558E-03,-0.12401174E-03,
        -0.63743326E-04,-0.17093003E-03,-0.29045402E-03,-0.85962507E-04,
        -0.82519815E-04, 0.90873189E-04,-0.37279160E-03,-0.34605453E-03,
        -0.42708925E-03, 0.34006600E-04,-0.13178700E-04, 0.18867624E-04,
         0.65021792E-04,-0.22095663E-04,-0.68119421E-04,-0.21083670E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_125                              =v_y_e_q2ex_1_125                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22*x33        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]        *x33*x44    
    ;
    v_y_e_q2ex_1_125                              =v_y_e_q2ex_1_125                              
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]        *x33        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x24    *x41    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22    *x43*x51
    ;
    v_y_e_q2ex_1_125                              =v_y_e_q2ex_1_125                              
        +coeff[ 26]*x12        *x41    
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]*x12        *x41*x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x12        *x42*x51
        ;

    return v_y_e_q2ex_1_125                              ;
}
float p_e_q2ex_1_125                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.3588258E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.38786828E-04,-0.18801153E-01,-0.16819766E-01, 0.17830352E-02,
         0.27969684E-02,-0.15174103E-02,-0.93756797E-03,-0.14947857E-02,
        -0.81555743E-03,-0.13344596E-04,-0.90425642E-03,-0.18888990E-03,
        -0.14534626E-03, 0.87609669E-06,-0.11093871E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_125                              =v_p_e_q2ex_1_125                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_125                              ;
}
float l_e_q2ex_1_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2662063E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.25626218E-02,-0.12136634E-03,-0.44747349E-02,-0.79544284E-03,
        -0.43737525E-02,-0.51654871E-02,-0.27016189E-03, 0.22091603E-03,
        -0.23504550E-03, 0.69096999E-03, 0.19670977E-04, 0.16748309E-03,
         0.60124078E-03, 0.22616560E-03, 0.19339369E-03,-0.69589389E-03,
        -0.45574913E-03,-0.54931524E-03,-0.11613179E-03,-0.14534057E-03,
         0.33805359E-03,-0.56624296E-03,-0.37908056E-03,-0.37413422E-03,
         0.16245913E-02,-0.87113143E-03, 0.56701060E-03,-0.87284265E-04,
        -0.10194476E-02,-0.62692480E-03, 0.29352687E-02, 0.80761797E-03,
         0.43612998E-03, 0.10814802E-02,-0.81603887E-03,-0.15791884E-03,
        -0.34400984E-03, 0.34149084E-03,-0.14203828E-03,-0.51189982E-03,
        -0.48666738E-03, 0.35896958E-03,-0.69901138E-03, 0.31919524E-03,
        -0.14467293E-02, 0.37436266E-03,-0.58596546E-03, 0.55016857E-03,
         0.17187590E-02, 0.16494947E-02,-0.69953321E-03, 0.56208140E-03,
        -0.45833492E-03,-0.13618311E-02,-0.84243959E-03,-0.11302702E-02,
         0.95360389E-03, 0.17615675E-03,-0.72865828E-03,-0.40463239E-03,
        -0.66846138E-03,-0.30251953E-02,-0.16974577E-02, 0.57283090E-03,
        -0.76170842E-03, 0.21262672E-02, 0.86099573E-03,-0.14757112E-02,
         0.22020920E-02,-0.11673197E-02,-0.62466576E-03, 0.92815928E-03,
         0.26361973E-02,-0.78713452E-03, 0.74029696E-03,-0.43670964E-03,
        -0.10912983E-03, 0.95886244E-04, 0.11254098E-03, 0.82556624E-04,
         0.15159063E-03,-0.84593565E-04, 0.60880516E-03,-0.22719943E-03,
         0.12109545E-03, 0.38805461E-03,-0.59501221E-03, 0.33307524E-03,
        -0.41799623E-03, 0.60323766E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11            *x51
        +coeff[  7]*x12                
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21*x31    *x51
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]            *x42*x51
        +coeff[ 14]                *x53
        +coeff[ 15]*x11*x21        *x51
        +coeff[ 16]        *x32    *x52
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 17]        *x31*x41*x52
        +coeff[ 18]*x11    *x33        
        +coeff[ 19]*x11*x22    *x41    
        +coeff[ 20]*x11*x21    *x41*x51
        +coeff[ 21]*x12    *x32        
        +coeff[ 22]*x12    *x31    *x51
        +coeff[ 23]*x13        *x41    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x21    *x44    
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 26]*x13            *x51
        +coeff[ 27]    *x24        *x51
        +coeff[ 28]    *x22*x31*x41*x51
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11    *x32*x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11*x22*x31    *x51
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]*x11    *x31*x42*x51
        +coeff[ 37]*x11        *x43*x51
        +coeff[ 38]*x12    *x33        
        +coeff[ 39]*x12    *x31*x41*x51
        +coeff[ 40]*x12        *x41*x52
        +coeff[ 41]*x12            *x53
        +coeff[ 42]    *x23*x33        
        +coeff[ 43]    *x21*x34*x41    
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 44]        *x34*x41*x51
        +coeff[ 45]    *x24        *x52
        +coeff[ 46]*x11*x21*x34        
        +coeff[ 47]*x11    *x34*x41    
        +coeff[ 48]*x11*x23*x31    *x51
        +coeff[ 49]*x11*x23    *x41*x51
        +coeff[ 50]*x11*x21*x31*x42*x51
        +coeff[ 51]*x11    *x31*x42*x52
        +coeff[ 52]*x11*x22        *x53
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 53]*x11*x21    *x41*x53
        +coeff[ 54]*x12    *x33*x41    
        +coeff[ 55]*x12*x21*x31*x41*x51
        +coeff[ 56]*x12    *x32*x41*x51
        +coeff[ 57]*x13*x23            
        +coeff[ 58]*x13*x21*x31    *x51
        +coeff[ 59]*x13    *x32    *x51
        +coeff[ 60]    *x23*x33    *x51
        +coeff[ 61]    *x24*x31*x41*x51
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 62]    *x22*x32*x42*x51
        +coeff[ 63]    *x23    *x43*x51
        +coeff[ 64]        *x31*x44*x52
        +coeff[ 65]    *x22*x31*x41*x53
        +coeff[ 66]*x11*x24*x32        
        +coeff[ 67]*x11*x24*x31*x41    
        +coeff[ 68]*x11    *x33*x42*x51
        +coeff[ 69]*x12    *x33*x42    
        +coeff[ 70]*x12*x24        *x51
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 71]*x12*x22*x31    *x52
        +coeff[ 72]    *x23*x33*x41*x51
        +coeff[ 73]*x13    *x31    *x53
        +coeff[ 74]    *x23*x32    *x53
        +coeff[ 75]            *x44*x54
        +coeff[ 76]                *x51
        +coeff[ 77]    *x21*x31        
        +coeff[ 78]        *x31    *x51
        +coeff[ 79]*x11*x21            
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 80]*x11        *x41    
        +coeff[ 81]        *x33        
        +coeff[ 82]    *x21    *x42    
        +coeff[ 83]*x11*x22            
        +coeff[ 84]*x11    *x32        
        +coeff[ 85]*x11    *x31    *x51
        +coeff[ 86]    *x21*x32*x41    
        +coeff[ 87]        *x33*x41    
        +coeff[ 88]    *x21*x31*x41*x51
    ;
    v_l_e_q2ex_1_125                              =v_l_e_q2ex_1_125                              
        +coeff[ 89]        *x32*x41*x51
        ;

    return v_l_e_q2ex_1_125                              ;
}
float x_e_q2ex_1_100                              (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1291901E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.13407702E-02,-0.28697259E-02, 0.17721033E+00, 0.75808708E-02,
        -0.12875285E-02,-0.37690378E-02,-0.26980590E-02, 0.53015115E-05,
         0.32112841E-03,-0.14568347E-02,-0.35616339E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2ex_1_100                              =v_x_e_q2ex_1_100                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2ex_1_100                              ;
}
float t_e_q2ex_1_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4158283E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.43147098E-03,-0.22747251E-02, 0.57739310E-01, 0.21487167E-02,
         0.16789792E-03, 0.51281563E-05,-0.10677121E-03, 0.33518460E-03,
         0.13234574E-03,-0.59621111E-05,-0.57660346E-03,-0.16412007E-02,
        -0.11273668E-02,-0.57716785E-04,-0.69415408E-04,-0.17733335E-03,
         0.70288086E-04,-0.97373739E-03,-0.74992735E-04,-0.56986497E-04,
         0.19060561E-03, 0.14142702E-03,-0.41745539E-05,-0.59379434E-03,
        -0.12574039E-02, 0.45872468E-04, 0.30224717E-04, 0.68159102E-05,
        -0.44165949E-05,-0.31996977E-04,-0.83116684E-05,-0.91697420E-05,
         0.45359149E-04, 0.16307056E-04,-0.13223820E-04, 0.15391304E-04,
         0.18043229E-05, 0.28654056E-05,-0.22223887E-04,-0.50391477E-05,
        -0.10147980E-04,-0.61597639E-05,-0.73787078E-05,-0.10451329E-04,
        -0.81512499E-05,-0.21252894E-04, 0.77182140E-05,-0.19507926E-04,
         0.52704804E-05,-0.16559825E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23        *x52
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_t_e_q2ex_1_100                              =v_t_e_q2ex_1_100                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]*x12*x21*x32        
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23        *x51
    ;
    v_t_e_q2ex_1_100                              =v_t_e_q2ex_1_100                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11    *x31*x41    
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x21*x32*x42    
        +coeff[ 25]*x12*x21*x32    *x51
    ;
    v_t_e_q2ex_1_100                              =v_t_e_q2ex_1_100                              
        +coeff[ 26]    *x23*x32    *x51
        +coeff[ 27]*x13                
        +coeff[ 28]    *x22*x31        
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]*x11            *x52
        +coeff[ 31]*x12*x21        *x51
        +coeff[ 32]    *x21*x32    *x51
        +coeff[ 33]*x11*x23        *x52
        +coeff[ 34]*x11*x22    *x41*x52
    ;
    v_t_e_q2ex_1_100                              =v_t_e_q2ex_1_100                              
        +coeff[ 35]*x11*x21            
        +coeff[ 36]                *x52
        +coeff[ 37]*x12        *x41    
        +coeff[ 38]*x11*x23            
        +coeff[ 39]*x11*x22*x31        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]*x11    *x31*x42    
        +coeff[ 42]*x13            *x51
        +coeff[ 43]    *x22*x31    *x51
    ;
    v_t_e_q2ex_1_100                              =v_t_e_q2ex_1_100                              
        +coeff[ 44]    *x22    *x41*x51
        +coeff[ 45]*x11*x21        *x52
        +coeff[ 46]*x11            *x53
        +coeff[ 47]*x13*x22            
        +coeff[ 48]*x13*x21*x31        
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q2ex_1_100                              ;
}
float y_e_q2ex_1_100                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1653527E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.15182314E-02, 0.21048772E+00, 0.12839906E+00,-0.20213621E-02,
        -0.10960418E-02,-0.62720815E-03,-0.41038106E-03,-0.26119727E-03,
        -0.47753154E-04, 0.48177564E-04, 0.49756591E-05,-0.11347615E-03,
        -0.32786877E-04,-0.46213935E-03,-0.21794236E-03, 0.31560379E-04,
         0.63324303E-04,-0.15439429E-03,-0.33269665E-03,-0.71139490E-04,
        -0.82019731E-04,-0.38999497E-03,-0.32676657E-03,-0.46820342E-03,
         0.93585586E-04, 0.29209143E-04, 0.65146305E-04,-0.19510451E-04,
        -0.58492184E-04,-0.68032161E-04, 0.30077124E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q2ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q2ex_1_100                              =v_y_e_q2ex_1_100                              
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]        *x31    *x52
        +coeff[ 10]        *x31*x44    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22*x33        
        +coeff[ 15]        *x32*x45    
        +coeff[ 16]        *x33*x44    
    ;
    v_y_e_q2ex_1_100                              =v_y_e_q2ex_1_100                              
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]        *x33        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x24    *x41    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x24*x31    *x51
        +coeff[ 25]    *x22    *x41*x53
    ;
    v_y_e_q2ex_1_100                              =v_y_e_q2ex_1_100                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x21    *x43*x51
        +coeff[ 28]*x11*x21*x31*x42    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x31*x41*x52
        ;

    return v_y_e_q2ex_1_100                              ;
}
float p_e_q2ex_1_100                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1632080E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.15086487E-03,-0.18876217E-01,-0.16914904E-01, 0.17741518E-02,
         0.27869814E-02,-0.15545906E-02,-0.95735630E-03,-0.14758112E-02,
        -0.80859388E-03,-0.14502471E-04,-0.18577898E-03,-0.88677404E-03,
        -0.14268506E-03,-0.20168079E-05,-0.10819080E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_1_100                              =v_p_e_q2ex_1_100                              
        +coeff[  8]            *x43    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x31    *x52
        ;

    return v_p_e_q2ex_1_100                              ;
}
float l_e_q2ex_1_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2667218E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.26989996E-02,-0.44240435E-02,-0.12578344E-02,-0.44051646E-02,
        -0.53479243E-02, 0.35011023E-03,-0.78047269E-04, 0.12191480E-03,
         0.39036636E-03,-0.10451046E-02, 0.13739720E-03, 0.27821265E-03,
        -0.33188643E-03, 0.57724601E-03, 0.74966758E-03,-0.22691954E-03,
        -0.47056173E-03,-0.63919164E-04,-0.15654119E-03,-0.34303419E-03,
        -0.46731107E-03, 0.48939261E-03,-0.14119653E-02,-0.16402246E-02,
         0.17049169E-02,-0.16750767E-02, 0.26360392E-02, 0.99075399E-03,
         0.22554933E-02,-0.24645662E-03, 0.19387483E-03,-0.38402912E-03,
         0.56421594E-03,-0.16407297E-02, 0.49112662E-03, 0.11830400E-02,
        -0.20765287E-02, 0.17588852E-03,-0.95307064E-03,-0.62212121E-03,
        -0.76570164E-03,-0.28014244E-03, 0.26071316E-02, 0.12137268E-02,
         0.27507967E-02, 0.28186222E-02,-0.18743209E-02,-0.32785188E-02,
        -0.34378044E-03, 0.19795171E-03,-0.24718593E-02,-0.16392038E-02,
         0.75007707E-03, 0.20260999E-02, 0.11502706E-02,-0.10469065E-02,
         0.82835025E-03,-0.21807144E-02,-0.17523041E-03,-0.41963212E-03,
         0.26929847E-03,-0.38499020E-04, 0.15202157E-06,-0.16285172E-03,
         0.11101917E-03,-0.48776771E-03, 0.25407501E-03, 0.98689030E-04,
        -0.18879132E-03, 0.16218240E-03, 0.10027962E-02,-0.93442803E-04,
        -0.19007873E-03,-0.17047010E-03, 0.20467932E-03, 0.17586212E-03,
        -0.38433512E-03, 0.20542741E-03, 0.21523199E-03,-0.43280644E-03,
        -0.41347233E-03, 0.28104970E-03, 0.29538333E-03, 0.99967598E-04,
        -0.23170240E-03, 0.14063000E-03,-0.71632583E-03,-0.63485547E-03,
         0.14481162E-03, 0.15861781E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x21*x31        
        +coeff[  7]*x11    *x31        
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]*x11*x21    *x41    
        +coeff[ 10]*x12        *x41    
        +coeff[ 11]*x13                
        +coeff[ 12]        *x33*x41    
        +coeff[ 13]    *x21    *x42*x51
        +coeff[ 14]            *x42*x52
        +coeff[ 15]                *x54
        +coeff[ 16]*x11*x21*x32        
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 17]*x11    *x33        
        +coeff[ 18]*x12*x21        *x51
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x22        *x53
        +coeff[ 21]            *x42*x53
        +coeff[ 22]*x11*x24            
        +coeff[ 23]*x11*x23*x31        
        +coeff[ 24]*x11*x21*x32*x41    
        +coeff[ 25]*x11*x21*x31*x42    
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 26]*x11    *x31*x42*x51
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x11*x21*x31    *x52
        +coeff[ 29]*x11        *x42*x52
        +coeff[ 30]*x12    *x32    *x51
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22*x31*x42*x51
        +coeff[ 33]    *x21*x31*x42*x52
        +coeff[ 34]*x11    *x33    *x52
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 35]*x12*x21    *x41*x52
        +coeff[ 36]    *x21*x34*x42    
        +coeff[ 37]    *x23*x31*x43    
        +coeff[ 38]    *x23*x33    *x51
        +coeff[ 39]    *x21*x33*x42*x51
        +coeff[ 40]    *x23*x31*x41*x52
        +coeff[ 41]        *x34    *x53
        +coeff[ 42]    *x21*x31*x42*x53
        +coeff[ 43]    *x21    *x43*x53
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 44]*x11*x23*x33        
        +coeff[ 45]*x11*x21*x33*x42    
        +coeff[ 46]*x11    *x31*x44*x51
        +coeff[ 47]*x11*x21*x33    *x52
        +coeff[ 48]*x11*x22*x31    *x53
        +coeff[ 49]*x11    *x32*x41*x53
        +coeff[ 50]*x11    *x31*x42*x53
        +coeff[ 51]*x11*x22        *x54
        +coeff[ 52]*x12*x21*x34        
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 53]*x12*x22*x32    *x51
        +coeff[ 54]*x12*x22*x31*x41*x51
        +coeff[ 55]*x12*x21    *x42*x52
        +coeff[ 56]*x13*x23    *x41    
        +coeff[ 57]    *x23*x32*x41*x52
        +coeff[ 58]*x11                
        +coeff[ 59]    *x21        *x51
        +coeff[ 60]            *x41*x51
        +coeff[ 61]*x11        *x41    
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 62]*x11            *x51
        +coeff[ 63]    *x23            
        +coeff[ 64]    *x21*x32        
        +coeff[ 65]    *x21*x31*x41    
        +coeff[ 66]    *x21    *x42    
        +coeff[ 67]        *x31*x42    
        +coeff[ 68]    *x21    *x41*x51
        +coeff[ 69]        *x31*x41*x51
        +coeff[ 70]*x11*x22            
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 71]*x12*x21            
        +coeff[ 72]    *x23*x31        
        +coeff[ 73]    *x22*x32        
        +coeff[ 74]        *x34        
        +coeff[ 75]    *x22    *x42    
        +coeff[ 76]            *x44    
        +coeff[ 77]    *x23        *x51
        +coeff[ 78]        *x32*x41*x51
        +coeff[ 79]            *x43*x51
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 80]    *x21    *x41*x52
        +coeff[ 81]        *x31*x41*x52
        +coeff[ 82]    *x21        *x53
        +coeff[ 83]        *x31    *x53
        +coeff[ 84]*x11*x22*x31        
        +coeff[ 85]*x11*x22    *x41    
        +coeff[ 86]*x11    *x32*x41    
        +coeff[ 87]*x11    *x31*x42    
        +coeff[ 88]*x11*x22        *x51
    ;
    v_l_e_q2ex_1_100                              =v_l_e_q2ex_1_100                              
        +coeff[ 89]*x11*x21    *x41*x51
        ;

    return v_l_e_q2ex_1_100                              ;
}
