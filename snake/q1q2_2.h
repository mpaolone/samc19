float x_e_q1q2_2_1200                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.9190542E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.92120911E-03, 0.16433187E-02, 0.12196649E+00, 0.26558815E-02,
        -0.38468017E-03, 0.88841691E-04,-0.55430009E-03,-0.44599059E-03,
        -0.34757095E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_1200                              =v_x_e_q1q2_2_1200                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_1200                              ;
}
float t_e_q1q2_2_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.4838319E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.44724443E-05,-0.20030981E-02,-0.26802351E-02, 0.23065691E-02,
         0.73834686E-04,-0.18717583E-03,-0.28202249E-03,-0.33363502E-03,
         0.49198337E-06,-0.24012866E-03,-0.15153986E-03,-0.88162553E-04,
        -0.80292305E-03,-0.62733574E-03,-0.16822174E-04,-0.41092362E-03,
        -0.17160273E-04,-0.13446439E-04, 0.49772287E-04, 0.12387684E-04,
         0.13267474E-04,-0.49552071E-03, 0.23124216E-04,-0.45988463E-05,
         0.21481289E-06,-0.90880058E-05, 0.67733413E-05,-0.60540265E-05,
         0.59511495E-06,-0.82182141E-05,-0.22037440E-04, 0.78352286E-05,
         0.23139832E-04, 0.81877806E-05, 0.19215975E-05,-0.62286454E-04,
        -0.23943464E-03,-0.50333820E-04, 0.13344003E-04, 0.98655864E-05,
        -0.19105506E-04, 0.21482621E-04, 0.13394575E-04, 0.18237077E-04,
        -0.20642867E-05,-0.12142646E-05, 0.30032343E-05, 0.35107412E-05,
         0.26692778E-05,-0.24851329E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_1200                              =v_t_e_q1q2_2_1200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_1200                              =v_t_e_q1q2_2_1200                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21*x31    *x52
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x32*x42*x51
        +coeff[ 23]*x11*x21            
        +coeff[ 24]    *x21    *x41    
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_2_1200                              =v_t_e_q1q2_2_1200                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]*x11*x22        *x51
        +coeff[ 31]    *x23        *x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]*x11*x21        *x52
        +coeff[ 34]*x11*x22*x32        
    ;
    v_t_e_q1q2_2_1200                              =v_t_e_q1q2_2_1200                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]    *x21*x33*x41    
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]*x11*x21*x31*x41*x51
        +coeff[ 39]*x12*x21        *x52
        +coeff[ 40]    *x22*x32*x42    
        +coeff[ 41]*x13*x22        *x51
        +coeff[ 42]*x12*x22*x31    *x51
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_q1q2_2_1200                              =v_t_e_q1q2_2_1200                              
        +coeff[ 44]        *x31*x41    
        +coeff[ 45]            *x41*x51
        +coeff[ 46]*x11*x21*x31        
        +coeff[ 47]    *x22*x31        
        +coeff[ 48]*x11*x21    *x41    
        +coeff[ 49]        *x31*x42    
        ;

    return v_t_e_q1q2_2_1200                              ;
}
float y_e_q1q2_2_1200                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.9815762E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.32564665E-04, 0.13641086E+00, 0.90402380E-01,-0.17061024E-02,
        -0.15722964E-02,-0.10393296E-03,-0.12825998E-03, 0.19784360E-03,
         0.31349075E-04, 0.76235738E-04, 0.75359974E-04, 0.66353503E-04,
        -0.22131302E-03, 0.11121771E-03,-0.13789526E-04, 0.38983388E-04,
         0.92012087E-05,-0.17483733E-03,-0.61990162E-04, 0.37780712E-04,
         0.60029915E-05, 0.11411352E-04, 0.16098520E-04,-0.77500961E-04,
        -0.30995587E-04,-0.37462465E-04, 0.25182089E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q1q2_2_1200                              =v_y_e_q1q2_2_1200                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_2_1200                              =v_y_e_q1q2_2_1200                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]*x11*x23    *x41    
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21*x32*x41    
        +coeff[ 22]        *x33    *x51
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_2_1200                              =v_y_e_q1q2_2_1200                              
        +coeff[ 26]*x11*x21*x31*x41*x51
        ;

    return v_y_e_q1q2_2_1200                              ;
}
float p_e_q1q2_2_1200                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.8039888E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.47916154E-04, 0.32423235E-01, 0.66860177E-01,-0.15769140E-02,
         0.61044266E-05,-0.14037059E-02,-0.31578657E-03,-0.37491266E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_1200                              ;
}
float l_e_q1q2_2_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1903433E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19356743E-02,-0.35774671E-02,-0.75133686E-03,-0.25582316E-02,
        -0.30423724E-02,-0.36578302E-03, 0.69836265E-03,-0.13473039E-02,
         0.14466823E-03, 0.39729865E-04, 0.41627369E-04, 0.97856871E-04,
        -0.32819100E-03,-0.12357581E-03,-0.40969136E-03, 0.96894764E-04,
        -0.43316861E-03,-0.46881064E-03,-0.13682189E-02,-0.24579739E-03,
         0.14135292E-03, 0.28343440E-03,-0.27978938E-03,-0.36744424E-03,
        -0.30903905E-03, 0.15581482E-03, 0.65938756E-03,-0.19450437E-02,
        -0.14157878E-02,-0.10809699E-02,-0.75062999E-03,-0.19402268E-03,
         0.20220885E-02, 0.14564042E-02, 0.34106249E-03, 0.55810082E-03,
         0.37946170E-02, 0.49206842E-03,-0.68427622E-03, 0.10216721E-03,
        -0.81831857E-03, 0.32829994E-03,-0.25931390E-02, 0.17605555E-02,
         0.71857753E-03,-0.75934245E-03,-0.19432852E-02, 0.15319763E-02,
         0.48433270E-03,-0.75690082E-03, 0.10955058E-02, 0.73027477E-03,
         0.92389341E-03,-0.47854823E-03,-0.35308159E-03,-0.54582681E-04,
         0.95308374E-03, 0.30366471E-02, 0.12470927E-02,-0.13903151E-02,
        -0.89748926E-03,-0.24150873E-02, 0.13964163E-02,-0.12879516E-02,
         0.13596737E-02, 0.48755948E-03,-0.48013759E-03,-0.21538171E-02,
        -0.99778583E-03,-0.23898720E-02, 0.27557591E-02, 0.52474497E-03,
         0.62096841E-03,-0.21172781E-02,-0.23827762E-02,-0.25187174E-02,
         0.13891454E-02, 0.63266285E-03, 0.58603706E-03,-0.18697921E-02,
        -0.23078099E-02, 0.15291986E-02, 0.57973899E-03, 0.90949377E-03,
        -0.79924408E-04, 0.61851868E-04,-0.14240872E-03, 0.23085462E-03,
        -0.40936671E-03, 0.11149422E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21        *x51
        +coeff[  6]*x11*x23*x32        
        +coeff[  7]*x12    *x32*x41*x51
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[  8]        *x31        
        +coeff[  9]*x11*x21            
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21    *x41*x51
        +coeff[ 15]                *x53
        +coeff[ 16]        *x33*x41    
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 17]            *x44    
        +coeff[ 18]*x11    *x32*x41    
        +coeff[ 19]*x11    *x31*x42    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11*x22        *x51
        +coeff[ 22]*x11    *x31*x41*x51
        +coeff[ 23]*x11*x21        *x52
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]*x12*x22            
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 26]*x12    *x31*x41    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]        *x33*x41*x51
        +coeff[ 30]        *x32*x42*x51
        +coeff[ 31]    *x21    *x43*x51
        +coeff[ 32]        *x31*x43*x51
        +coeff[ 33]    *x22*x31    *x52
        +coeff[ 34]        *x33    *x52
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 35]*x11    *x34        
        +coeff[ 36]*x11*x22*x31*x41    
        +coeff[ 37]*x12*x21*x32        
        +coeff[ 38]*x12*x21    *x42    
        +coeff[ 39]*x12    *x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x24    *x41*x51
        +coeff[ 42]        *x31*x44*x51
        +coeff[ 43]        *x31*x42*x53
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 44]*x11*x24    *x41    
        +coeff[ 45]*x11*x22    *x43    
        +coeff[ 46]*x11*x21*x31*x42*x51
        +coeff[ 47]*x11    *x32*x41*x52
        +coeff[ 48]*x11*x21*x31    *x53
        +coeff[ 49]*x11*x21    *x41*x53
        +coeff[ 50]*x11    *x31*x41*x53
        +coeff[ 51]*x12*x21    *x42*x51
        +coeff[ 52]*x12*x22        *x52
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 53]*x12    *x31    *x53
        +coeff[ 54]*x12            *x54
        +coeff[ 55]    *x24*x33        
        +coeff[ 56]    *x23    *x43*x51
        +coeff[ 57]        *x32*x44*x51
        +coeff[ 58]    *x24    *x41*x52
        +coeff[ 59]        *x33    *x54
        +coeff[ 60]    *x22    *x41*x54
        +coeff[ 61]*x11*x24*x31*x41    
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 62]*x11    *x34*x42    
        +coeff[ 63]*x11    *x33*x42*x51
        +coeff[ 64]*x11    *x33*x41*x52
        +coeff[ 65]*x11    *x33    *x53
        +coeff[ 66]*x11        *x43*x53
        +coeff[ 67]*x12    *x32*x42*x51
        +coeff[ 68]*x12    *x31*x41*x53
        +coeff[ 69]*x13*x22*x31*x41    
        +coeff[ 70]    *x23*x34    *x51
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 71]    *x23*x33*x41*x51
        +coeff[ 72]*x13*x21    *x42*x51
        +coeff[ 73]    *x23*x32*x42*x51
        +coeff[ 74]    *x21*x34*x42*x51
        +coeff[ 75]        *x34*x43*x51
        +coeff[ 76]    *x21*x32*x44*x51
        +coeff[ 77]*x13    *x32    *x52
        +coeff[ 78]    *x24*x31*x41*x52
        +coeff[ 79]    *x23*x32    *x53
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 80]    *x23*x31*x41*x53
        +coeff[ 81]        *x34*x41*x53
        +coeff[ 82]        *x34    *x54
        +coeff[ 83]        *x33*x41*x54
        +coeff[ 84]    *x21        *x51
        +coeff[ 85]        *x31    *x51
        +coeff[ 86]            *x41*x51
        +coeff[ 87]*x11        *x41    
        +coeff[ 88]    *x22*x31        
    ;
    v_l_e_q1q2_2_1200                              =v_l_e_q1q2_2_1200                              
        +coeff[ 89]        *x33        
        ;

    return v_l_e_q1q2_2_1200                              ;
}
float x_e_q1q2_2_1100                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.8157045E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.80203655E-03, 0.12197409E+00, 0.16446083E-02, 0.26540412E-02,
        -0.38273219E-03, 0.88870562E-04,-0.56085753E-03,-0.44712206E-03,
        -0.35301430E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_1100                              =v_x_e_q1q2_2_1100                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_1100                              ;
}
float t_e_q1q2_2_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2424064E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.23073468E-04,-0.20021563E-02,-0.26825215E-02, 0.23097945E-02,
         0.72509669E-04,-0.17839222E-03,-0.26944626E-03,-0.31894364E-03,
         0.47001013E-05,-0.26875248E-03,-0.13313063E-03,-0.80047328E-04,
        -0.84417779E-03,-0.64231042E-03,-0.20604557E-04,-0.50998392E-03,
        -0.41810371E-03,-0.31004358E-04,-0.20437879E-04, 0.66319299E-04,
         0.37694339E-04, 0.12411785E-04,-0.22635343E-03, 0.13668553E-04,
         0.35477668E-04,-0.45850652E-05,-0.52633669E-06,-0.67864318E-06,
         0.25255056E-05,-0.21955073E-04, 0.41235312E-05, 0.87691060E-05,
         0.79289512E-05,-0.68856630E-05, 0.91564143E-05,-0.59308213E-05,
        -0.18277482E-04, 0.12429470E-04,-0.15311831E-04,-0.13408322E-04,
        -0.17777216E-04, 0.10446962E-04,-0.24530500E-04,-0.24148162E-05,
         0.48358670E-05,-0.18738321E-04,-0.10150053E-04,-0.81535964E-05,
        -0.23030712E-04, 0.17380848E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_1100                              =v_t_e_q1q2_2_1100                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x32*x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_2_1100                              =v_t_e_q1q2_2_1100                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]*x13*x23            
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]    *x22            
    ;
    v_t_e_q1q2_2_1100                              =v_t_e_q1q2_2_1100                              
        +coeff[ 26]*x13                
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x22*x31        
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]        *x31*x42    
        +coeff[ 31]*x12*x22            
        +coeff[ 32]*x12    *x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_q1q2_2_1100                              =v_t_e_q1q2_2_1100                              
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x11*x21*x33        
        +coeff[ 38]    *x22*x33        
        +coeff[ 39]*x12*x21    *x42    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]*x11*x21*x31*x41*x51
        +coeff[ 43]*x11*x21    *x42*x51
    ;
    v_t_e_q1q2_2_1100                              =v_t_e_q1q2_2_1100                              
        +coeff[ 44]    *x21*x31*x41*x52
        +coeff[ 45]    *x21    *x42*x52
        +coeff[ 46]*x11*x21        *x53
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]*x12*x21*x31*x41*x51
        +coeff[ 49]*x11*x22    *x42*x51
        ;

    return v_t_e_q1q2_2_1100                              ;
}
float y_e_q1q2_2_1100                              (float *x,int m){
    int ncoeff= 34;
    float avdat=  0.1920137E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
        -0.27001588E-03, 0.13638730E+00, 0.90428002E-01,-0.17130993E-02,
        -0.15654585E-02,-0.12374145E-03, 0.51754265E-04,-0.20472627E-03,
         0.17758725E-03,-0.11270179E-03, 0.12557628E-03, 0.66941138E-04,
         0.75117234E-04, 0.48907794E-04,-0.20226013E-03,-0.20420972E-04,
         0.40084506E-04, 0.31920267E-05,-0.26662716E-04, 0.94394336E-05,
        -0.12867997E-04, 0.35489688E-06, 0.21627744E-04, 0.22255537E-04,
        -0.10710247E-04,-0.78931582E-04,-0.66842847E-04,-0.28614486E-04,
        -0.30929838E-04, 0.41805913E-04,-0.16928059E-04, 0.12786967E-04,
         0.27697324E-04,-0.23869325E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_1100                              =v_y_e_q1q2_2_1100                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_2_1100                              =v_y_e_q1q2_2_1100                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]        *x32*x41*x51
        +coeff[ 24]        *x31*x41*x52
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_2_1100                              =v_y_e_q1q2_2_1100                              
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]            *x43*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]    *x21*x33*x42    
        +coeff[ 31]*x11        *x42*x52
        +coeff[ 32]        *x33    *x52
        +coeff[ 33]*x11*x21*x32*x42    
        ;

    return v_y_e_q1q2_2_1100                              ;
}
float p_e_q1q2_2_1100                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.1687752E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.56820252E-04, 0.32429140E-01, 0.66846415E-01,-0.15776017E-02,
        -0.29528576E-05,-0.14013613E-02,-0.31368871E-03,-0.36923928E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_1100                              ;
}
float l_e_q1q2_2_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1890603E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18139647E-02,-0.76011369E-04,-0.34579234E-02,-0.13460616E-03,
        -0.31335182E-02,-0.53337921E-03,-0.10877341E-02, 0.56187343E-03,
         0.12100536E-02,-0.29014109E-03,-0.87638386E-03,-0.44262520E-03,
         0.48115361E-02, 0.79018425E-03,-0.97007997E-03, 0.64277637E-03,
        -0.17817916E-02, 0.54547307E-03,-0.45298077E-02, 0.23542554E-02,
        -0.95103031E-04, 0.20305035E-04,-0.23474693E-02, 0.27468344E-03,
        -0.22730672E-03,-0.84413005E-04, 0.34150569E-04,-0.23350352E-03,
         0.33440079E-04,-0.23105720E-04, 0.86198811E-03, 0.11749890E-02,
         0.95467884E-04, 0.75288094E-03,-0.58959322E-04,-0.56888629E-03,
        -0.16645128E-04,-0.87084761E-03,-0.22862654E-03, 0.85044821E-03,
        -0.43948370E-03, 0.32241747E-03, 0.33794114E-03, 0.97992575E-04,
         0.67302713E-03, 0.64400776E-03, 0.29192591E-03,-0.93015097E-03,
         0.15180581E-04,-0.78999804E-03,-0.29479343E-03,-0.12303389E-02,
         0.57065324E-03,-0.42837841E-04,-0.46873221E-03, 0.71850355E-03,
        -0.40705493E-03,-0.64203562E-03,-0.55010634E-03, 0.51705947E-03,
        -0.16380525E-02,-0.82860043E-03, 0.11331311E-02, 0.84715424E-03,
         0.58937492E-03,-0.53729367E-03,-0.96139329E-03, 0.73038443E-03,
        -0.43107042E-03, 0.72111440E-03, 0.10508919E-02, 0.50643779E-03,
        -0.62435068E-03, 0.12167119E-02,-0.55500941E-03,-0.47587085E-03,
        -0.99488872E-03, 0.15395080E-02, 0.16179201E-02,-0.14590855E-02,
        -0.13085307E-02, 0.13163725E-02,-0.58305502E-03, 0.97182579E-03,
        -0.68653579E-03, 0.77737641E-03,-0.78939670E-03,-0.19641994E-02,
        -0.79657516E-03, 0.80283993E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[  8]        *x31*x41*x52
        +coeff[  9]    *x21*x31    *x53
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x22*x34        
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]        *x33*x43    
        +coeff[ 16]    *x22    *x42*x52
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 17]*x13*x24            
        +coeff[ 18]    *x24*x33*x41    
        +coeff[ 19]        *x34*x42*x52
        +coeff[ 20]*x11                
        +coeff[ 21]    *x21*x31        
        +coeff[ 22]        *x31*x41    
        +coeff[ 23]                *x52
        +coeff[ 24]*x12                
        +coeff[ 25]    *x22*x31        
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]            *x43    
        +coeff[ 28]    *x22        *x51
        +coeff[ 29]        *x32    *x51
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]*x11    *x32        
        +coeff[ 33]    *x21*x31*x42    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 35]        *x32    *x52
        +coeff[ 36]*x11*x22*x31        
        +coeff[ 37]*x11*x21*x32        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]*x11*x21*x31    *x51
        +coeff[ 40]*x11    *x32    *x51
        +coeff[ 41]*x11            *x53
        +coeff[ 42]*x12*x22            
        +coeff[ 43]*x13*x21            
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 44]        *x32*x43    
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]            *x44*x51
        +coeff[ 47]        *x31*x41*x53
        +coeff[ 48]    *x21        *x54
        +coeff[ 49]        *x31    *x54
        +coeff[ 50]*x11*x21*x33        
        +coeff[ 51]*x11    *x32*x42    
        +coeff[ 52]*x11        *x44    
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 53]*x12    *x33        
        +coeff[ 54]*x12    *x31*x41*x51
        +coeff[ 55]*x13    *x32        
        +coeff[ 56]*x13    *x31*x41    
        +coeff[ 57]*x13        *x42    
        +coeff[ 58]    *x21*x31*x42*x52
        +coeff[ 59]    *x21    *x43*x52
        +coeff[ 60]        *x31*x43*x52
        +coeff[ 61]*x11*x24*x31        
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 62]*x11    *x34    *x51
        +coeff[ 63]*x11*x22*x31    *x52
        +coeff[ 64]*x11*x21*x31*x41*x52
        +coeff[ 65]*x11*x22        *x53
        +coeff[ 66]*x11*x21*x31    *x53
        +coeff[ 67]*x12*x22*x31    *x51
        +coeff[ 68]*x12*x21*x32    *x51
        +coeff[ 69]*x12*x21*x31    *x52
        +coeff[ 70]*x13*x21*x32        
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 71]    *x23    *x43*x51
        +coeff[ 72]        *x32*x44*x51
        +coeff[ 73]    *x22*x31*x42*x52
        +coeff[ 74]*x13            *x53
        +coeff[ 75]        *x34    *x53
        +coeff[ 76]    *x22*x31    *x54
        +coeff[ 77]*x11*x23    *x42*x51
        +coeff[ 78]*x11*x21*x31*x43*x51
        +coeff[ 79]*x11*x22*x32    *x52
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 80]*x11*x22*x31*x41*x52
        +coeff[ 81]*x12*x24    *x41    
        +coeff[ 82]*x12*x22*x32*x41    
        +coeff[ 83]*x12*x21*x33*x41    
        +coeff[ 84]*x12    *x34*x41    
        +coeff[ 85]*x12*x23        *x52
        +coeff[ 86]*x12    *x33    *x52
        +coeff[ 87]*x12*x22    *x41*x52
        +coeff[ 88]*x12*x21    *x42*x52
    ;
    v_l_e_q1q2_2_1100                              =v_l_e_q1q2_2_1100                              
        +coeff[ 89]*x12        *x43*x52
        ;

    return v_l_e_q1q2_2_1100                              ;
}
float x_e_q1q2_2_1000                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.5739197E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.53944404E-03, 0.12195367E+00, 0.16442325E-02, 0.26569536E-02,
        -0.38374559E-03, 0.87279019E-04,-0.54993958E-03,-0.44545115E-03,
        -0.33548439E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_1000                              =v_x_e_q1q2_2_1000                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_1000                              ;
}
float t_e_q1q2_2_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1600937E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.15225238E-04,-0.20045270E-02,-0.26704790E-02, 0.23069466E-02,
         0.73411888E-04,-0.17827390E-03,-0.30041431E-03,-0.33324934E-03,
        -0.89309642E-05,-0.26429311E-03,-0.14420538E-03,-0.91404167E-04,
        -0.82355988E-03,-0.63750235E-03,-0.15933287E-04,-0.39281556E-03,
        -0.19158817E-04,-0.17033992E-04, 0.61899336E-04, 0.31047948E-04,
        -0.18962342E-03,-0.46537234E-03,-0.10490363E-04,-0.10587039E-04,
         0.39748666E-04,-0.17404518E-06, 0.75246003E-05,-0.60299467E-05,
        -0.61034384E-05, 0.19133942E-04,-0.11529478E-04,-0.95901560E-05,
        -0.52561238E-04,-0.30406809E-04,-0.88502929E-05,-0.93938825E-05,
        -0.10865514E-04, 0.15256959E-04, 0.13012326E-04, 0.85033298E-05,
        -0.14479414E-04, 0.26221553E-04, 0.16902606E-04, 0.13342517E-04,
        -0.19348767E-04,-0.86395476E-05, 0.53801014E-05,-0.25360475E-05,
        -0.24092683E-05, 0.32669791E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_1000                              =v_t_e_q1q2_2_1000                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_1000                              =v_t_e_q1q2_2_1000                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x11    *x32    *x52
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x21*x32*x42*x51
        +coeff[ 25]        *x31    *x51
    ;
    v_t_e_q1q2_2_1000                              =v_t_e_q1q2_2_1000                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]*x11    *x33        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x13*x21*x31        
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x12*x21*x31    *x51
    ;
    v_t_e_q1q2_2_1000                              =v_t_e_q1q2_2_1000                              
        +coeff[ 35]*x11*x22    *x41*x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]*x12*x23        *x51
        +coeff[ 40]    *x22*x33    *x51
        +coeff[ 41]*x13*x21    *x41*x51
        +coeff[ 42]*x11*x22    *x42*x51
        +coeff[ 43]*x12*x21        *x53
    ;
    v_t_e_q1q2_2_1000                              =v_t_e_q1q2_2_1000                              
        +coeff[ 44]*x11*x21    *x41*x53
        +coeff[ 45]    *x22    *x41*x53
        +coeff[ 46]*x11*x21*x31        
        +coeff[ 47]            *x41*x52
        +coeff[ 48]*x11*x23            
        +coeff[ 49]*x13    *x31        
        ;

    return v_t_e_q1q2_2_1000                              ;
}
float y_e_q1q2_2_1000                              (float *x,int m){
    int ncoeff= 27;
    float avdat=  0.2183737E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
        -0.18905374E-03, 0.13632119E+00, 0.90420254E-01,-0.17042554E-02,
        -0.15681712E-02,-0.11395061E-03, 0.45903882E-04,-0.19907855E-03,
         0.18971672E-03,-0.12275021E-03, 0.11846740E-03, 0.54750784E-04,
         0.73376286E-04, 0.67924593E-04,-0.21784811E-03,-0.13930566E-04,
         0.46759797E-04, 0.78574622E-05,-0.67839479E-04, 0.52725343E-04,
         0.13497071E-05, 0.11319035E-04,-0.15128115E-04,-0.41667154E-04,
         0.33000044E-04,-0.41114305E-04, 0.25120276E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_1000                              =v_y_e_q1q2_2_1000                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_2_1000                              =v_y_e_q1q2_2_1000                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]*x11*x23    *x41    
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x21*x31*x44    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_2_1000                              =v_y_e_q1q2_2_1000                              
        +coeff[ 26]    *x23*x32*x41    
        ;

    return v_y_e_q1q2_2_1000                              ;
}
float p_e_q1q2_2_1000                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.3779147E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.23683237E-04, 0.32426763E-01, 0.66808522E-01,-0.15782567E-02,
         0.64085625E-05,-0.14061519E-02,-0.31403443E-03,-0.37137666E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_1000                              ;
}
float l_e_q1q2_2_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1872888E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18750309E-02,-0.33670734E-02,-0.97363882E-04,-0.23335423E-02,
        -0.32748792E-02,-0.42223313E-04,-0.62766403E-03,-0.39538406E-03,
        -0.23768641E-03, 0.42279961E-03, 0.66855276E-03,-0.37481423E-03,
         0.11944579E-03, 0.48418806E-03, 0.69227867E-03, 0.41491431E-03,
        -0.49805891E-03, 0.48650059E-03,-0.38065060E-03, 0.46562124E-03,
         0.20842004E-03, 0.26909254E-04,-0.58434886E-03,-0.10220928E-02,
         0.70104271E-03,-0.13099788E-02, 0.23350639E-03,-0.15028928E-02,
         0.52949140E-03, 0.11633405E-02,-0.73133757E-04, 0.63117966E-03,
        -0.20320322E-03,-0.18151022E-03,-0.60466788E-03, 0.57497865E-03,
         0.49512513E-03, 0.12588432E-02,-0.19620892E-03,-0.58069918E-03,
        -0.47908141E-03, 0.58011309E-03,-0.33739849E-03, 0.20877439E-03,
        -0.49134240E-04,-0.82973839E-03,-0.18277260E-02, 0.11370542E-02,
        -0.12385751E-02,-0.13741330E-02,-0.68146351E-03, 0.17075624E-02,
        -0.53223042E-03,-0.12020485E-02,-0.10714489E-02, 0.15139239E-02,
        -0.21440019E-03, 0.14066618E-02, 0.61035395E-03, 0.87151933E-03,
         0.18843470E-03, 0.46377173E-02,-0.48120455E-02,-0.10846537E-02,
        -0.36142964E-02,-0.50061056E-02, 0.13868494E-02,-0.14099073E-04,
        -0.41097929E-03,-0.95157680E-04,-0.11442397E-03, 0.72126473E-04,
         0.14161429E-03, 0.33152004E-03, 0.14345905E-03,-0.87854853E-04,
         0.29019587E-03,-0.25819332E-03, 0.40268703E-03, 0.88804154E-04,
        -0.13530043E-03,-0.90277674E-04, 0.23216713E-03,-0.23179680E-03,
        -0.13005358E-03,-0.13557596E-03, 0.20993380E-03,-0.20055419E-03,
         0.21206438E-03, 0.56640618E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x34        
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[  8]    *x21*x31    *x51
        +coeff[  9]            *x42*x51
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21        *x51
        +coeff[ 12]*x11        *x41*x51
        +coeff[ 13]*x12            *x51
        +coeff[ 14]    *x23*x31        
        +coeff[ 15]    *x21*x33        
        +coeff[ 16]        *x34        
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]        *x33    *x51
        +coeff[ 19]    *x21*x31    *x52
        +coeff[ 20]*x11*x23            
        +coeff[ 21]*x12*x21        *x51
        +coeff[ 22]            *x42*x53
        +coeff[ 23]*x11*x21    *x43    
        +coeff[ 24]*x11*x21        *x53
        +coeff[ 25]*x12    *x32    *x51
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 26]*x13        *x41*x51
        +coeff[ 27]    *x23*x31    *x52
        +coeff[ 28]        *x32    *x54
        +coeff[ 29]*x11*x22    *x43    
        +coeff[ 30]*x11    *x32*x43    
        +coeff[ 31]*x11    *x31*x44    
        +coeff[ 32]*x11    *x34    *x51
        +coeff[ 33]*x11*x23    *x41*x51
        +coeff[ 34]*x11*x22    *x41*x52
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 35]*x11*x21    *x42*x52
        +coeff[ 36]*x11*x21*x31    *x53
        +coeff[ 37]*x11    *x31*x41*x53
        +coeff[ 38]*x12    *x32*x42    
        +coeff[ 39]*x12*x22*x31    *x51
        +coeff[ 40]*x12    *x32    *x52
        +coeff[ 41]*x12*x21        *x53
        +coeff[ 42]*x13    *x33        
        +coeff[ 43]*x13*x22        *x51
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 44]    *x24*x32    *x51
        +coeff[ 45]*x13    *x31*x41*x51
        +coeff[ 46]    *x22*x33*x41*x51
        +coeff[ 47]    *x23    *x42*x52
        +coeff[ 48]    *x21*x32*x42*x52
        +coeff[ 49]*x11*x21*x33*x42    
        +coeff[ 50]*x11    *x34*x41*x51
        +coeff[ 51]*x11*x23    *x42*x51
        +coeff[ 52]*x11    *x34    *x52
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 53]*x11*x21*x32*x41*x52
        +coeff[ 54]*x11*x21    *x42*x53
        +coeff[ 55]*x12*x21*x32*x42    
        +coeff[ 56]*x12    *x33*x41*x51
        +coeff[ 57]*x12    *x32    *x53
        +coeff[ 58]*x12    *x31*x41*x53
        +coeff[ 59]*x13*x23*x31        
        +coeff[ 60]*x13        *x44    
        +coeff[ 61]    *x23*x31*x43*x51
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 62]    *x21*x33*x43*x51
        +coeff[ 63]*x13    *x31    *x53
        +coeff[ 64]    *x23*x31*x41*x53
        +coeff[ 65]    *x21*x32*x42*x53
        +coeff[ 66]    *x21    *x44*x53
        +coeff[ 67]    *x21            
        +coeff[ 68]    *x21*x31        
        +coeff[ 69]                *x52
        +coeff[ 70]*x11*x21            
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 71]*x11    *x31        
        +coeff[ 72]    *x21*x32        
        +coeff[ 73]    *x21*x31*x41    
        +coeff[ 74]        *x31*x42    
        +coeff[ 75]    *x21    *x41*x51
        +coeff[ 76]        *x31*x41*x51
        +coeff[ 77]*x11*x21*x31        
        +coeff[ 78]*x11    *x31    *x51
        +coeff[ 79]*x11            *x52
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 80]*x12*x21            
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]    *x23    *x41    
        +coeff[ 83]        *x33*x41    
        +coeff[ 84]    *x22    *x42    
        +coeff[ 85]    *x21    *x43    
        +coeff[ 86]        *x31*x43    
        +coeff[ 87]    *x23        *x51
        +coeff[ 88]    *x21*x32    *x51
    ;
    v_l_e_q1q2_2_1000                              =v_l_e_q1q2_2_1000                              
        +coeff[ 89]    *x21*x31*x41*x51
        ;

    return v_l_e_q1q2_2_1000                              ;
}
float x_e_q1q2_2_900                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.4081500E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.37981395E-03, 0.12196800E+00, 0.16443672E-02, 0.26569415E-02,
        -0.38063549E-03, 0.88143213E-04,-0.55791688E-03,-0.45071720E-03,
        -0.34907658E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_900                              =v_x_e_q1q2_2_900                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_900                              ;
}
float t_e_q1q2_2_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1494318E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.13729726E-04,-0.20040669E-02,-0.26657397E-02, 0.23080604E-02,
         0.73438954E-04,-0.18290877E-03,-0.28007917E-03,-0.33085584E-03,
         0.56981671E-05,-0.24656372E-03,-0.14314952E-03,-0.83015999E-04,
        -0.80089306E-03,-0.62623696E-03,-0.16605523E-04,-0.44105740E-03,
        -0.19886198E-04,-0.15979222E-04,-0.30577703E-05, 0.68910114E-04,
         0.25661382E-04,-0.22819976E-03,-0.52470079E-03, 0.83392733E-05,
         0.26253783E-04,-0.24746321E-04, 0.58576225E-04, 0.94184213E-06,
        -0.21050409E-05,-0.10767281E-04,-0.58029495E-05, 0.14779442E-04,
        -0.91517823E-05,-0.10314756E-04, 0.72481216E-05, 0.19791096E-05,
        -0.13159147E-05, 0.58053251E-05,-0.47859663E-04,-0.11041005E-04,
        -0.35385954E-04, 0.76468268E-06,-0.14802795E-04, 0.12376878E-04,
        -0.41267949E-05, 0.12768242E-04,-0.18301935E-04, 0.90703097E-05,
         0.17287022E-04,-0.13340805E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_900                              =v_t_e_q1q2_2_900                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_900                              =v_t_e_q1q2_2_900                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x12*x22    *x41*x51
    ;
    v_t_e_q1q2_2_900                              =v_t_e_q1q2_2_900                              
        +coeff[ 26]    *x21*x32*x42*x51
        +coeff[ 27]*x11        *x41    
        +coeff[ 28]                *x52
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]        *x31*x42    
        +coeff[ 31]*x11*x22*x31        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q1q2_2_900                              =v_t_e_q1q2_2_900                              
        +coeff[ 35]*x12*x22*x31        
        +coeff[ 36]*x11*x22*x32        
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]*x11*x22*x31*x41    
        +coeff[ 39]*x11*x21*x32*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x13*x21        *x51
        +coeff[ 42]*x12*x21*x31    *x51
        +coeff[ 43]*x11*x21*x32    *x51
    ;
    v_t_e_q1q2_2_900                              =v_t_e_q1q2_2_900                              
        +coeff[ 44]            *x42*x53
        +coeff[ 45]*x12*x22*x32        
        +coeff[ 46]*x11*x22*x33        
        +coeff[ 47]    *x22*x33*x41    
        +coeff[ 48]*x12*x22*x31    *x51
        +coeff[ 49]*x11*x21*x33    *x51
        ;

    return v_t_e_q1q2_2_900                              ;
}
float y_e_q1q2_2_900                              (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.1801433E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.12899558E-03, 0.13635090E+00, 0.90407237E-01,-0.17139596E-02,
        -0.15608406E-02,-0.93433358E-04,-0.28321443E-04,-0.20855635E-03,
         0.20724471E-03,-0.57971996E-04, 0.11708371E-03, 0.10838686E-03,
         0.68545007E-04, 0.64305248E-04,-0.21469095E-03,-0.32726366E-04,
        -0.36138226E-04, 0.59993796E-04,-0.21975889E-03,-0.10229811E-03,
         0.69169076E-07, 0.14264803E-04, 0.57679790E-04, 0.21781066E-05,
         0.11098876E-04, 0.25764724E-04,-0.15295920E-03, 0.41783358E-04,
         0.17006250E-04, 0.26149657E-04, 0.21112370E-04,-0.61031304E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_900                              =v_y_e_q1q2_2_900                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_900                              =v_y_e_q1q2_2_900                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x24*x33        
        +coeff[ 20]            *x42*x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]        *x32*x41*x51
        +coeff[ 24]*x11        *x42*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q1q2_2_900                              =v_y_e_q1q2_2_900                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]*x12        *x43    
        +coeff[ 29]*x11    *x31*x42*x51
        +coeff[ 30]*x11*x23    *x42    
        +coeff[ 31]    *x22*x32*x41*x51
        ;

    return v_y_e_q1q2_2_900                              ;
}
float p_e_q1q2_2_900                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.6142872E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.38214726E-04, 0.32419514E-01, 0.66826500E-01,-0.15772734E-02,
         0.26287421E-05,-0.14017907E-02,-0.31196180E-03,-0.36965832E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_900                              ;
}
float l_e_q1q2_2_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1855599E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19018091E-02,-0.35489826E-02,-0.54865080E-03,-0.32419674E-02,
        -0.14132122E-03,-0.89983584E-03,-0.19591076E-02,-0.73195650E-03,
        -0.12079041E-03,-0.38653988E-03, 0.30926356E-03,-0.66184253E-03,
         0.17998817E-02, 0.16307482E-03,-0.89515175E-03,-0.50298171E-03,
        -0.39506801E-02, 0.30534077E-03,-0.14723135E-03,-0.15795050E-02,
         0.25014748E-03, 0.30469377E-04,-0.73557213E-03, 0.82173105E-03,
        -0.63647883E-03,-0.25810428E-04,-0.19163285E-03, 0.24445952E-03,
        -0.59006771E-03, 0.58593095E-03, 0.14049618E-03,-0.32090049E-03,
        -0.28151434E-03,-0.64509688E-04, 0.12440392E-03,-0.19808726E-03,
        -0.71160938E-03,-0.16320373E-03, 0.57748175E-03,-0.14930667E-02,
        -0.87669306E-03,-0.13039188E-02, 0.17249392E-02, 0.37449299E-03,
         0.99555135E-03, 0.12416141E-02, 0.13452130E-02,-0.11450254E-02,
         0.37605138E-03, 0.60897780E-03, 0.48281864E-03,-0.23724507E-03,
        -0.20232096E-02, 0.65050356E-03, 0.97950187E-03,-0.16282736E-02,
        -0.55449113E-03, 0.30344492E-03, 0.27248426E-03,-0.44143113E-03,
        -0.45143894E-03, 0.50971814E-03,-0.32744277E-03, 0.72571152E-03,
         0.16033920E-02,-0.99098717E-03,-0.46449740E-03, 0.77177270E-03,
         0.16747287E-02,-0.25243801E-02, 0.12304658E-02,-0.25513242E-02,
         0.56979909E-04, 0.94701670E-03, 0.44767512E-03,-0.46135008E-03,
         0.15688231E-02, 0.18422715E-02, 0.27255821E-02,-0.67122100E-03,
        -0.88079309E-03,-0.92594077E-04,-0.16977559E-02, 0.75381319E-03,
         0.70322450E-03, 0.11109228E-02, 0.12565563E-03,-0.66114859E-04,
        -0.22300138E-03,-0.80960315E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]*x11        *x41*x51
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]*x12*x21    *x41*x51
        +coeff[ 12]    *x24*x31*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]    *x21*x31*x44    
        +coeff[ 16]*x11*x22*x33    *x51
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 17]*x11*x22*x31    *x53
        +coeff[ 18]    *x21    *x41    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]*x11*x21            
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x12*x21            
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 26]*x12    *x31        
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31*x41*x51
        +coeff[ 30]*x12    *x31*x41    
        +coeff[ 31]*x12    *x31    *x51
        +coeff[ 32]*x13*x21            
        +coeff[ 33]    *x23*x32        
        +coeff[ 34]    *x21*x33*x41    
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 35]        *x34*x41    
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x24        *x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x22    *x41*x52
        +coeff[ 40]*x11*x21*x33        
        +coeff[ 41]*x11*x21*x31*x42    
        +coeff[ 42]*x11*x22*x31    *x51
        +coeff[ 43]*x11*x21*x32    *x51
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 44]*x11    *x32*x41*x51
        +coeff[ 45]*x11*x21    *x41*x52
        +coeff[ 46]*x11        *x41*x53
        +coeff[ 47]*x12        *x42*x51
        +coeff[ 48]*x12            *x53
        +coeff[ 49]    *x23*x33        
        +coeff[ 50]    *x21*x32    *x53
        +coeff[ 51]    *x21    *x42*x53
        +coeff[ 52]*x11*x22*x32*x41    
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 53]*x11    *x34*x41    
        +coeff[ 54]*x11*x22*x32    *x51
        +coeff[ 55]*x11    *x31*x43*x51
        +coeff[ 56]*x11    *x32    *x53
        +coeff[ 57]*x11    *x31    *x54
        +coeff[ 58]*x12*x24            
        +coeff[ 59]*x12*x22    *x42    
        +coeff[ 60]*x12        *x44    
        +coeff[ 61]*x12*x22        *x52
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 62]*x13*x22*x31        
        +coeff[ 63]*x13*x22    *x41    
        +coeff[ 64]    *x22*x32*x42*x51
        +coeff[ 65]    *x22    *x44*x51
        +coeff[ 66]*x13        *x41*x52
        +coeff[ 67]            *x43*x54
        +coeff[ 68]*x11*x22*x34        
        +coeff[ 69]*x11*x21*x32*x41*x52
        +coeff[ 70]*x11    *x33*x41*x52
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 71]*x11    *x32*x41*x53
        +coeff[ 72]*x12*x21*x34        
        +coeff[ 73]*x12*x21*x31*x43    
        +coeff[ 74]*x12    *x31*x44    
        +coeff[ 75]*x12    *x31*x43*x51
        +coeff[ 76]*x12        *x44*x51
        +coeff[ 77]*x12*x21*x32    *x52
        +coeff[ 78]*x12*x22    *x41*x52
        +coeff[ 79]*x12    *x32*x41*x52
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 80]*x12        *x43*x52
        +coeff[ 81]*x13*x24            
        +coeff[ 82]*x13*x22*x32        
        +coeff[ 83]*x13*x22    *x42    
        +coeff[ 84]*x13    *x32    *x52
        +coeff[ 85]    *x22*x33*x41*x52
        +coeff[ 86]    *x21            
        +coeff[ 87]*x11                
        +coeff[ 88]    *x21*x31        
    ;
    v_l_e_q1q2_2_900                              =v_l_e_q1q2_2_900                              
        +coeff[ 89]    *x21        *x51
        ;

    return v_l_e_q1q2_2_900                              ;
}
float x_e_q1q2_2_800                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3319825E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.35559275E-04, 0.12199405E+00, 0.16453112E-02, 0.26541194E-02,
        -0.38508672E-03, 0.88565786E-04,-0.58382307E-03,-0.46323592E-03,
        -0.13875980E-03,-0.30891169E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_800                              =v_x_e_q1q2_2_800                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_800                              ;
}
float t_e_q1q2_2_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1941908E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.19557003E-04,-0.20017615E-02,-0.26585252E-02, 0.23083820E-02,
         0.72861832E-04,-0.17842362E-03,-0.27866178E-03,-0.33492246E-03,
        -0.38131075E-05,-0.25766823E-03,-0.13639414E-03,-0.88775043E-04,
        -0.80164062E-03,-0.62224007E-03,-0.17130902E-04,-0.41766101E-03,
        -0.10228105E-04,-0.21060929E-04, 0.58427879E-04, 0.42690022E-04,
        -0.22569706E-03,-0.48800366E-03, 0.85783668E-05,-0.94568247E-06,
        -0.12203087E-04,-0.32617713E-05,-0.72784201E-05,-0.54079300E-06,
        -0.13607819E-04, 0.18565332E-04,-0.12751872E-04,-0.14778140E-04,
        -0.46376834E-04,-0.20815189E-04,-0.24065061E-04, 0.10679893E-04,
         0.87859707E-05, 0.13767431E-04, 0.94136185E-05, 0.20084142E-04,
        -0.82077877E-05,-0.11607227E-04, 0.11753724E-04,-0.52553992E-05,
        -0.13847561E-04, 0.24898110E-04, 0.17350469E-04, 0.11606973E-04,
         0.12824223E-04, 0.13241017E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_800                              =v_t_e_q1q2_2_800                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_800                              =v_t_e_q1q2_2_800                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x21    *x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_t_e_q1q2_2_800                              =v_t_e_q1q2_2_800                              
        +coeff[ 26]    *x21*x31    *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x11*x22*x31        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11    *x33*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1q2_2_800                              =v_t_e_q1q2_2_800                              
        +coeff[ 35]*x11*x21    *x43    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]*x13*x21        *x51
        +coeff[ 38]*x11*x22*x31    *x51
        +coeff[ 39]    *x23*x31    *x51
        +coeff[ 40]    *x22*x32    *x51
        +coeff[ 41]*x11*x22        *x52
        +coeff[ 42]    *x22*x31    *x52
        +coeff[ 43]        *x33    *x52
    ;
    v_t_e_q1q2_2_800                              =v_t_e_q1q2_2_800                              
        +coeff[ 44]*x11*x21        *x53
        +coeff[ 45]*x11*x22*x33        
        +coeff[ 46]*x11*x22*x31*x42    
        +coeff[ 47]    *x23    *x43    
        +coeff[ 48]*x12*x23        *x51
        +coeff[ 49]*x13*x21*x31    *x51
        ;

    return v_t_e_q1q2_2_800                              ;
}
float y_e_q1q2_2_800                              (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.5423427E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.56890782E-03, 0.13635652E+00, 0.90407051E-01,-0.17084025E-02,
        -0.15663302E-02,-0.31699587E-04,-0.34790941E-04,-0.18275874E-03,
         0.22125845E-03,-0.80720725E-04, 0.12657430E-03, 0.73124080E-04,
         0.75964497E-04, 0.66538683E-04,-0.29380785E-03,-0.17775437E-04,
         0.68326149E-05,-0.72767791E-04,-0.27749242E-03, 0.98193168E-05,
         0.14598605E-04, 0.21036187E-04, 0.45098459E-04,-0.20012053E-03,
        -0.14025049E-04,-0.84082938E-04,-0.43941687E-04, 0.21424303E-04,
        -0.43282638E-04,-0.47328969E-04, 0.28059723E-04,-0.69529786E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_800                              =v_y_e_q1q2_2_800                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_2_800                              =v_y_e_q1q2_2_800                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]    *x24*x32*x41    
        +coeff[ 19]        *x33        
        +coeff[ 20]*x11*x21    *x42    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x31*x43*x51
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_2_800                              =v_y_e_q1q2_2_800                              
        +coeff[ 26]*x11*x21*x32*x41    
        +coeff[ 27]    *x21*x32*x41*x51
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]        *x32*x43*x51
        +coeff[ 30]*x13*x21    *x41    
        +coeff[ 31]    *x22*x31*x42*x51
        ;

    return v_y_e_q1q2_2_800                              ;
}
float p_e_q1q2_2_800                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.3338299E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.34632205E-03, 0.32420076E-01, 0.66824943E-01,-0.15775815E-02,
         0.59812587E-05,-0.14044682E-02,-0.31408592E-03,-0.36888503E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_800                              ;
}
float l_e_q1q2_2_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1855368E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18642206E-02,-0.36817284E-02,-0.66200859E-03,-0.21378051E-02,
        -0.30561702E-02, 0.87749527E-03, 0.14006702E-02,-0.11217106E-02,
        -0.68027679E-04,-0.46314348E-04, 0.40040188E-03, 0.45932381E-03,
         0.44562601E-03, 0.91120380E-03,-0.13609634E-03, 0.39727212E-03,
         0.35805273E-03, 0.27344615E-03, 0.20343527E-04,-0.20118708E-03,
        -0.43919726E-03,-0.77543344E-03, 0.20674762E-03, 0.17407806E-03,
        -0.52844244E-03, 0.11025320E-03, 0.10177518E-02,-0.42933525E-03,
        -0.44128599E-03, 0.17899029E-02, 0.69354166E-03, 0.32717912E-02,
        -0.56452124E-03,-0.24595237E-03,-0.44896952E-02,-0.15184125E-02,
         0.72856050E-03, 0.67160459E-03,-0.11692811E-02,-0.63150126E-03,
        -0.10313387E-02,-0.66231663E-03,-0.16737680E-02, 0.14614068E-02,
        -0.20284839E-03, 0.21957018E-03, 0.58194931E-03, 0.14255181E-02,
        -0.86341338E-03, 0.20402663E-02, 0.15337224E-02, 0.19360995E-02,
         0.11134874E-02,-0.85449440E-03,-0.88691694E-03,-0.15951333E-02,
        -0.29703288E-02,-0.20195390E-02,-0.13620459E-02,-0.10161548E-02,
         0.54328884E-02, 0.57190144E-03, 0.87639855E-04,-0.14730210E-03,
         0.18058602E-03,-0.20307998E-03, 0.18917327E-03,-0.24582105E-03,
         0.11273689E-03,-0.21135050E-03,-0.23773330E-03, 0.11995050E-03,
        -0.12409812E-03, 0.11147113E-03, 0.62845596E-04, 0.44609489E-04,
         0.17336236E-03, 0.20573512E-03,-0.23202877E-03, 0.21853847E-03,
         0.65858971E-04,-0.16849619E-03, 0.57856302E-03,-0.11443276E-04,
         0.32376967E-03,-0.59096055E-03, 0.80960331E-03, 0.46989304E-03,
         0.14209717E-03,-0.16602839E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21*x31*x42    
        +coeff[  6]        *x34*x42*x51
        +coeff[  7]    *x22    *x42*x54
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[  8]    *x21        *x51
        +coeff[  9]*x12                
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x24            
        +coeff[ 13]    *x22*x31*x41    
        +coeff[ 14]    *x22*x31    *x51
        +coeff[ 15]        *x32    *x52
        +coeff[ 16]*x11*x21*x32        
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 17]*x11*x21*x31    *x51
        +coeff[ 18]*x11    *x32    *x51
        +coeff[ 19]*x11*x21        *x52
        +coeff[ 20]*x12*x21    *x41    
        +coeff[ 21]*x12    *x31*x41    
        +coeff[ 22]    *x21*x34        
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]            *x42*x53
        +coeff[ 25]*x11    *x33    *x51
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 26]*x11    *x31*x41*x52
        +coeff[ 27]*x12    *x32*x41    
        +coeff[ 28]*x13    *x31*x41    
        +coeff[ 29]    *x22*x32*x42    
        +coeff[ 30]    *x24*x31    *x51
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]        *x33*x42*x51
        +coeff[ 33]        *x33    *x53
        +coeff[ 34]    *x21*x31*x41*x53
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 35]*x11*x24    *x41    
        +coeff[ 36]*x11*x22    *x43    
        +coeff[ 37]*x11*x24        *x51
        +coeff[ 38]*x11    *x33*x41*x51
        +coeff[ 39]*x11*x22        *x53
        +coeff[ 40]*x12    *x32*x42    
        +coeff[ 41]        *x33*x44    
        +coeff[ 42]*x13*x21    *x41*x51
        +coeff[ 43]    *x21*x34*x41*x51
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 44]    *x23    *x43*x51
        +coeff[ 45]    *x21*x33    *x53
        +coeff[ 46]*x11*x22*x34        
        +coeff[ 47]*x11*x22*x31*x42*x51
        +coeff[ 48]*x11*x24        *x52
        +coeff[ 49]*x11*x22    *x41*x53
        +coeff[ 50]*x11*x22        *x54
        +coeff[ 51]*x12*x23*x31*x41    
        +coeff[ 52]*x12*x23    *x42    
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 53]*x12*x23    *x41*x51
        +coeff[ 54]*x12*x22*x31    *x52
        +coeff[ 55]*x13*x22    *x41*x51
        +coeff[ 56]    *x23*x33*x41*x51
        +coeff[ 57]        *x34*x43*x51
        +coeff[ 58]*x13*x22        *x52
        +coeff[ 59]    *x22*x34    *x52
        +coeff[ 60]    *x21*x33*x41*x53
        +coeff[ 61]    *x22    *x43*x53
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 62]    *x21            
        +coeff[ 63]    *x21*x31        
        +coeff[ 64]    *x21    *x41    
        +coeff[ 65]*x11    *x31        
        +coeff[ 66]*x11            *x51
        +coeff[ 67]    *x23            
        +coeff[ 68]    *x22*x31        
        +coeff[ 69]    *x21*x31*x41    
        +coeff[ 70]        *x32*x41    
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 71]    *x21*x31    *x51
        +coeff[ 72]        *x32    *x51
        +coeff[ 73]            *x41*x52
        +coeff[ 74]*x12            *x51
        +coeff[ 75]*x13                
        +coeff[ 76]    *x23*x31        
        +coeff[ 77]        *x34        
        +coeff[ 78]    *x22    *x42    
        +coeff[ 79]        *x32*x41*x51
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 80]    *x21*x31    *x52
        +coeff[ 81]    *x21    *x41*x52
        +coeff[ 82]*x11*x22    *x41    
        +coeff[ 83]*x11    *x32*x41    
        +coeff[ 84]*x11    *x31*x42    
        +coeff[ 85]*x11*x22        *x51
        +coeff[ 86]*x11*x21    *x41*x51
        +coeff[ 87]*x11    *x31*x41*x51
        +coeff[ 88]*x11    *x31    *x52
    ;
    v_l_e_q1q2_2_800                              =v_l_e_q1q2_2_800                              
        +coeff[ 89]*x12*x22            
        ;

    return v_l_e_q1q2_2_800                              ;
}
float x_e_q1q2_2_700                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.6147879E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.61806344E-03, 0.16467064E-02, 0.12199782E+00, 0.26521885E-02,
        -0.38652113E-03, 0.87956207E-04,-0.58142038E-03,-0.46085112E-03,
        -0.13809052E-03,-0.30383727E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_700                              =v_x_e_q1q2_2_700                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_700                              ;
}
float t_e_q1q2_2_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2578095E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.26960677E-04,-0.20031098E-02,-0.26515517E-02, 0.23026585E-02,
         0.70455084E-04,-0.16789277E-03,-0.26169806E-03,-0.32322787E-03,
         0.51057109E-05,-0.27607771E-03,-0.12699782E-03,-0.85422544E-04,
        -0.82884915E-03,-0.64466946E-03,-0.19658515E-04,-0.43513166E-03,
        -0.16923144E-04,-0.17079215E-04,-0.13056258E-04, 0.46660356E-04,
         0.32494994E-04, 0.20322157E-04,-0.23553539E-03,-0.51499519E-03,
        -0.64154960E-05,-0.80040672E-05,-0.46592409E-05, 0.23348923E-05,
        -0.83541663E-05, 0.20652562E-04, 0.78326991E-06, 0.18168739E-04,
        -0.11281771E-04,-0.30912029E-05,-0.47660826E-04,-0.14163082E-04,
        -0.35786808E-04,-0.92254950E-05, 0.14466118E-04,-0.60983789E-05,
         0.96235663E-05, 0.27215356E-04,-0.25766538E-04, 0.18675486E-04,
        -0.15106075E-04, 0.16538965E-04, 0.69937886E-06, 0.20804227E-05,
         0.18656717E-05,-0.24230410E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_700                              =v_t_e_q1q2_2_700                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_700                              =v_t_e_q1q2_2_700                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_2_700                              =v_t_e_q1q2_2_700                              
        +coeff[ 26]*x12            *x51
        +coeff[ 27]                *x53
        +coeff[ 28]*x11*x23            
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]*x11    *x32    *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x12*x23            
        +coeff[ 33]*x11*x22*x32        
        +coeff[ 34]*x11*x22*x31*x41    
    ;
    v_t_e_q1q2_2_700                              =v_t_e_q1q2_2_700                              
        +coeff[ 35]*x11*x21*x32*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x11*x22        *x52
        +coeff[ 38]        *x32*x41*x52
        +coeff[ 39]            *x43*x52
        +coeff[ 40]*x13*x23            
        +coeff[ 41]*x13*x21    *x42    
        +coeff[ 42]*x11*x23    *x42    
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_t_e_q1q2_2_700                              =v_t_e_q1q2_2_700                              
        +coeff[ 44]*x12*x22        *x52
        +coeff[ 45]*x12*x21*x31    *x52
        +coeff[ 46]        *x31        
        +coeff[ 47]*x12                
        +coeff[ 48]    *x22            
        +coeff[ 49]    *x21*x31        
        ;

    return v_t_e_q1q2_2_700                              ;
}
float y_e_q1q2_2_700                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.2288130E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.27254544E-03, 0.13630807E+00, 0.90400457E-01,-0.17066926E-02,
        -0.15648348E-02,-0.48595088E-04,-0.89327521E-04,-0.22161314E-03,
         0.21847730E-03,-0.34957757E-04, 0.14918567E-03, 0.10640768E-03,
         0.73874180E-04, 0.60653674E-04,-0.28088983E-03,-0.39159750E-04,
         0.39821080E-04,-0.27793212E-03,-0.36027006E-04,-0.47152269E-04,
        -0.81158225E-06,-0.53419199E-05,-0.31431298E-05, 0.35558183E-04,
         0.22712076E-04,-0.20886581E-03,-0.33437296E-04, 0.27169634E-04,
        -0.63924184E-04,-0.27256248E-04,-0.55854485E-04, 0.30255474E-04,
         0.24334378E-04, 0.87248176E-04, 0.29853425E-04,-0.39835275E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_700                              =v_y_e_q1q2_2_700                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_2_700                              =v_y_e_q1q2_2_700                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]*x11*x23    *x41    
        +coeff[ 19]    *x22*x33*x42    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x12        *x41    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1q2_2_700                              =v_y_e_q1q2_2_700                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]*x11    *x32*x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x11        *x45    
        +coeff[ 31]*x12*x22    *x41    
        +coeff[ 32]    *x23*x31*x42    
        +coeff[ 33]    *x21*x31*x43*x51
        +coeff[ 34]    *x23    *x42*x51
    ;
    v_y_e_q1q2_2_700                              =v_y_e_q1q2_2_700                              
        +coeff[ 35]    *x21*x33*x41*x51
        ;

    return v_y_e_q1q2_2_700                              ;
}
float p_e_q1q2_2_700                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.1466028E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.16708591E-03, 0.32411050E-01, 0.66803820E-01,-0.15765581E-02,
         0.27569449E-05,-0.14025025E-02,-0.31704287E-03,-0.37700596E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_700                              ;
}
float l_e_q1q2_2_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1865048E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16356473E-02,-0.27599183E-02,-0.34321658E-03,-0.22097074E-02,
        -0.46689061E-05, 0.22960550E-02,-0.92770402E-04,-0.20527327E-03,
        -0.11855010E-02, 0.57206239E-03,-0.19041320E-02,-0.57237097E-02,
         0.33275024E-02,-0.20552929E-02, 0.66417083E-03,-0.19060203E-02,
        -0.24593421E-02,-0.13485490E-03, 0.85393469E-04,-0.17766036E-03,
        -0.30471808E-02, 0.25280967E-03, 0.12649307E-02, 0.14129393E-02,
        -0.60023792E-03,-0.46380208E-03,-0.94395498E-03, 0.39734872E-03,
        -0.32680377E-03,-0.56273106E-03,-0.81665255E-03,-0.35977591E-03,
         0.87662152E-03, 0.31882731E-03,-0.20630872E-02, 0.55816455E-03,
        -0.37527969E-03, 0.40883146E-03,-0.76203840E-03,-0.75078855E-03,
         0.30787501E-02, 0.54947066E-03, 0.70868980E-03,-0.91706665E-03,
        -0.16062224E-02,-0.51120478E-04, 0.32701646E-04,-0.30014265E-03,
        -0.16031768E-02, 0.16967890E-03, 0.10099169E-02, 0.16455449E-02,
        -0.86324126E-03,-0.84988860E-03,-0.53566956E-03, 0.70684351E-03,
         0.62121684E-03,-0.35534060E-03, 0.54547132E-03, 0.45989855E-03,
        -0.95024676E-03,-0.74066530E-03,-0.58060134E-03,-0.62202604E-03,
         0.24416915E-02, 0.27253997E-03,-0.23425401E-02, 0.16279486E-02,
        -0.63061940E-04,-0.15476182E-02, 0.84017776E-03, 0.43969140E-02,
         0.40094201E-02,-0.14816626E-02, 0.17702933E-02,-0.81376947E-03,
        -0.19302419E-02,-0.16551843E-02,-0.59929332E-02, 0.26852412E-02,
         0.10166925E-02, 0.12558185E-02, 0.33983781E-02, 0.17925659E-04,
        -0.51460385E-04, 0.46388450E-04, 0.18256836E-03, 0.75556207E-04,
         0.11969188E-03, 0.10795330E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[  8]            *x44    
        +coeff[  9]*x12    *x31*x41    
        +coeff[ 10]    *x24*x31*x41    
        +coeff[ 11]    *x22*x33*x41    
        +coeff[ 12]    *x22*x32*x42    
        +coeff[ 13]        *x34*x42    
        +coeff[ 14]*x11*x24    *x41    
        +coeff[ 15]*x11    *x32*x43    
        +coeff[ 16]        *x31*x41    
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]            *x43*x51
        +coeff[ 22]        *x31*x41*x52
        +coeff[ 23]*x11*x21*x31*x41    
        +coeff[ 24]*x11    *x31*x42    
        +coeff[ 25]*x12*x22            
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 26]*x12    *x32        
        +coeff[ 27]*x12        *x42    
        +coeff[ 28]*x12*x21        *x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x21*x31*x42*x51
        +coeff[ 31]*x11*x24            
        +coeff[ 32]*x11    *x31*x43    
        +coeff[ 33]*x11        *x44    
        +coeff[ 34]*x11    *x32*x41*x51
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 35]*x11        *x43*x51
        +coeff[ 36]*x12*x21    *x41*x51
        +coeff[ 37]*x13*x22            
        +coeff[ 38]    *x24*x32        
        +coeff[ 39]*x13    *x31*x41    
        +coeff[ 40]    *x22    *x44    
        +coeff[ 41]    *x23    *x42*x51
        +coeff[ 42]    *x21*x32*x41*x52
        +coeff[ 43]    *x21    *x43*x52
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 44]        *x31*x43*x52
        +coeff[ 45]    *x22*x31    *x53
        +coeff[ 46]    *x21*x31    *x54
        +coeff[ 47]*x11*x23*x32        
        +coeff[ 48]*x11*x23*x31*x41    
        +coeff[ 49]*x11*x21*x32    *x52
        +coeff[ 50]*x12    *x34        
        +coeff[ 51]*x12    *x32*x42    
        +coeff[ 52]*x12*x22*x31    *x51
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 53]*x12*x22    *x41*x51
        +coeff[ 54]*x12        *x42*x52
        +coeff[ 55]*x13*x21*x32        
        +coeff[ 56]*x13    *x32*x41    
        +coeff[ 57]*x13*x22        *x51
        +coeff[ 58]    *x23*x33    *x51
        +coeff[ 59]*x13*x21    *x41*x51
        +coeff[ 60]    *x22    *x43*x52
        +coeff[ 61]        *x31*x44*x52
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 62]        *x33    *x54
        +coeff[ 63]        *x32*x41*x54
        +coeff[ 64]*x11*x24*x31*x41    
        +coeff[ 65]*x11*x22*x32*x42    
        +coeff[ 66]*x11*x22*x31*x43    
        +coeff[ 67]*x11*x23*x32    *x51
        +coeff[ 68]*x11*x23        *x53
        +coeff[ 69]*x11*x21*x32    *x53
        +coeff[ 70]*x12*x22*x31    *x52
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 71]    *x24*x33*x41    
        +coeff[ 72]    *x22*x33*x43    
        +coeff[ 73]*x13*x21*x31*x41*x51
        +coeff[ 74]*x13    *x32*x41*x51
        +coeff[ 75]*x13*x21    *x42*x51
        +coeff[ 76]    *x23*x32*x41*x52
        +coeff[ 77]    *x21*x34*x41*x52
        +coeff[ 78]    *x23*x31*x42*x52
        +coeff[ 79]    *x21*x31*x44*x52
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 80]    *x22*x33    *x53
        +coeff[ 81]    *x23*x31    *x54
        +coeff[ 82]    *x21*x32*x41*x54
        +coeff[ 83]            *x41    
        +coeff[ 84]*x11                
        +coeff[ 85]*x11            *x51
        +coeff[ 86]*x12                
        +coeff[ 87]        *x33        
        +coeff[ 88]    *x22    *x41    
    ;
    v_l_e_q1q2_2_700                              =v_l_e_q1q2_2_700                              
        +coeff[ 89]    *x21    *x42    
        ;

    return v_l_e_q1q2_2_700                              ;
}
float x_e_q1q2_2_600                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1130487E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.88342997E-04, 0.16450768E-02, 0.12203269E+00, 0.26537406E-02,
        -0.38556990E-03, 0.86254448E-04,-0.57273667E-03,-0.46021483E-03,
        -0.14288157E-03,-0.30567806E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_600                              =v_x_e_q1q2_2_600                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_600                              ;
}
float t_e_q1q2_2_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5301203E-05;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35209302E-05,-0.20026863E-02,-0.26223219E-02, 0.23120686E-02,
         0.73767180E-04,-0.19336573E-03,-0.28358036E-03,-0.35263112E-03,
         0.31564114E-05,-0.21708167E-03,-0.15958273E-03,-0.93137227E-04,
        -0.79498749E-03,-0.60842751E-03,-0.16687742E-04,-0.41578381E-03,
        -0.24214647E-04,-0.18862451E-04,-0.24476134E-04, 0.11366466E-04,
        -0.71292075E-05,-0.46589665E-03, 0.10895777E-04,-0.74757627E-05,
        -0.12435181E-04, 0.27026720E-05, 0.55982432E-05, 0.82066726E-05,
        -0.56390654E-05,-0.24384806E-04,-0.12287273E-04,-0.13130531E-04,
         0.35814766E-04,-0.47594763E-05, 0.94752577E-05, 0.28421321E-05,
        -0.14454855E-04,-0.12512837E-04,-0.22427515E-03,-0.14679660E-04,
         0.96051263E-05,-0.88220932E-05, 0.77430495E-05, 0.13752180E-04,
         0.39213683E-04, 0.29370269E-04, 0.50579947E-04, 0.19959336E-04,
        -0.15323785E-05, 0.15740893E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_600                              =v_t_e_q1q2_2_600                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_600                              =v_t_e_q1q2_2_600                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x33        
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]*x11*x22*x32        
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_q1q2_2_600                              =v_t_e_q1q2_2_600                              
        +coeff[ 26]*x11*x22    *x41    
        +coeff[ 27]    *x23    *x41    
        +coeff[ 28]*x12    *x31*x41    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]*x13    *x32        
    ;
    v_t_e_q1q2_2_600                              =v_t_e_q1q2_2_600                              
        +coeff[ 35]*x12    *x33        
        +coeff[ 36]*x11*x21*x32*x41    
        +coeff[ 37]*x11    *x33*x41    
        +coeff[ 38]    *x21*x33*x41    
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x11*x22*x31    *x51
        +coeff[ 41]*x11    *x32    *x52
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x21    *x42*x52
    ;
    v_t_e_q1q2_2_600                              =v_t_e_q1q2_2_600                              
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]    *x23*x31*x41*x51
        +coeff[ 46]    *x21*x31*x43*x51
        +coeff[ 47]    *x21*x32    *x53
        +coeff[ 48]*x12                
        +coeff[ 49]    *x22*x31        
        ;

    return v_t_e_q1q2_2_600                              ;
}
float y_e_q1q2_2_600                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.5418992E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.60140679E-03, 0.13628288E+00, 0.90409234E-01,-0.17074634E-02,
        -0.15606601E-02,-0.75820411E-04,-0.91925212E-04, 0.19779890E-03,
        -0.21378579E-04, 0.74325173E-04, 0.63469430E-04,-0.24863845E-03,
         0.14832539E-03, 0.10133181E-03,-0.76022179E-05,-0.88884204E-04,
        -0.15741232E-03,-0.19649776E-03,-0.22991457E-03,-0.56265031E-04,
        -0.30635274E-04, 0.15863807E-03, 0.55378691E-05, 0.39468006E-04,
         0.38881302E-04,-0.81468288E-05,-0.18600120E-04,-0.14440869E-04,
        -0.47266218E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q1q2_2_600                              =v_y_e_q1q2_2_600                              
        +coeff[  8]            *x45    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_2_600                              =v_y_e_q1q2_2_600                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]        *x31*x44*x51
        +coeff[ 21]*x11*x21    *x45    
        +coeff[ 22]        *x31*x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]*x11        *x44    
    ;
    v_y_e_q1q2_2_600                              =v_y_e_q1q2_2_600                              
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]    *x21    *x42*x52
        +coeff[ 28]*x11*x23*x31        
        ;

    return v_y_e_q1q2_2_600                              ;
}
float p_e_q1q2_2_600                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.2772763E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.30570684E-03, 0.32406516E-01, 0.66785291E-01,-0.15739636E-02,
         0.18821293E-04,-0.14059477E-02,-0.31182831E-03,-0.37503423E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]    *x22*x31    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_600                              ;
}
float l_e_q1q2_2_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1856868E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19304660E-02, 0.18280149E-03,-0.34590531E-02,-0.53872180E-03,
        -0.36973946E-02, 0.14088051E-03, 0.29480173E-02, 0.28308494E-04,
        -0.14851149E-02, 0.77277329E-03, 0.45910102E-03, 0.10271724E-02,
        -0.30709940E-02,-0.41511921E-04, 0.34109155E-04,-0.20308176E-03,
        -0.37403117E-03, 0.98027590E-06, 0.36383513E-03, 0.14997982E-04,
         0.34322866E-03, 0.34235785E-03, 0.83333398E-04, 0.44095676E-03,
        -0.52338006E-03, 0.21047860E-03,-0.10308789E-02,-0.13908128E-03,
        -0.46136588E-03, 0.48930396E-03,-0.78311539E-03, 0.11912672E-02,
        -0.59527642E-03,-0.19401455E-02,-0.16248818E-03,-0.61243935E-03,
         0.67777779E-04,-0.79832965E-03,-0.15124077E-02, 0.96815976E-03,
        -0.22485216E-04, 0.11209318E-02, 0.20403258E-03,-0.24896071E-02,
        -0.46376605E-03,-0.10095836E-02,-0.38764087E-03, 0.72437379E-03,
         0.55965676E-03,-0.19707426E-03,-0.27546168E-02, 0.25145750E-03,
         0.69049681E-02, 0.45904820E-03, 0.31797670E-04,-0.15790022E-02,
         0.68042427E-03,-0.42735433E-03, 0.14962257E-02,-0.10532897E-02,
        -0.34035783E-03, 0.10146473E-02, 0.57808915E-03,-0.13428946E-02,
         0.18542659E-02, 0.15894150E-02, 0.86867000E-03,-0.17254418E-03,
         0.86050661E-03,-0.10142904E-02,-0.14488086E-02,-0.12123863E-02,
        -0.51437318E-02, 0.37968806E-02,-0.11278550E-02, 0.11748729E-02,
        -0.26376822E-02, 0.13177049E-02, 0.13679124E-02,-0.12994572E-02,
         0.52705774E-03, 0.28319827E-02,-0.64025616E-03,-0.64777292E-03,
        -0.10519843E-02, 0.10951702E-02,-0.81352756E-03,-0.18209773E-02,
        -0.57102442E-02,-0.11481334E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x32        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]        *x34*x42    
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21        *x51
        +coeff[ 14]                *x52
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x33        
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 17]    *x21*x31*x41    
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]    *x21*x33        
        +coeff[ 21]    *x21*x32*x41    
        +coeff[ 22]    *x21    *x41*x52
        +coeff[ 23]    *x21        *x53
        +coeff[ 24]*x11    *x32*x41    
        +coeff[ 25]*x11        *x43    
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 26]*x11*x21    *x41*x51
        +coeff[ 27]*x12*x21    *x41    
        +coeff[ 28]*x12*x21        *x51
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]        *x31*x43*x51
        +coeff[ 32]    *x23        *x52
        +coeff[ 33]        *x31*x42*x52
        +coeff[ 34]            *x43*x52
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 35]        *x31*x41*x53
        +coeff[ 36]            *x42*x53
        +coeff[ 37]*x11*x22*x32        
        +coeff[ 38]*x11*x22*x31*x41    
        +coeff[ 39]*x11    *x31*x43    
        +coeff[ 40]*x11    *x32*x41*x51
        +coeff[ 41]*x11    *x31*x42*x51
        +coeff[ 42]*x11*x22        *x52
        +coeff[ 43]*x11    *x31*x41*x52
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 44]*x11    *x31    *x53
        +coeff[ 45]*x12*x21*x31*x41    
        +coeff[ 46]*x12    *x31*x41*x51
        +coeff[ 47]*x12        *x42*x51
        +coeff[ 48]*x12    *x31    *x52
        +coeff[ 49]*x13    *x31*x41    
        +coeff[ 50]    *x22*x33*x41    
        +coeff[ 51]*x13        *x42    
        +coeff[ 52]    *x22*x32*x42    
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 53]*x13        *x41*x51
        +coeff[ 54]    *x24        *x52
        +coeff[ 55]    *x23    *x41*x52
        +coeff[ 56]    *x21*x31*x41*x53
        +coeff[ 57]    *x22        *x54
        +coeff[ 58]    *x21    *x41*x54
        +coeff[ 59]*x11*x22*x32    *x51
        +coeff[ 60]*x11*x21*x31*x42*x51
        +coeff[ 61]*x11*x21    *x43*x51
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 62]*x11*x22        *x53
        +coeff[ 63]*x12*x22*x31*x41    
        +coeff[ 64]*x12    *x33*x41    
        +coeff[ 65]*x12    *x32*x42    
        +coeff[ 66]*x12*x21*x32    *x51
        +coeff[ 67]    *x24*x33        
        +coeff[ 68]    *x23*x32    *x52
        +coeff[ 69]    *x22*x32*x41*x52
        +coeff[ 70]        *x32*x43*x52
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 71]    *x21*x32    *x54
        +coeff[ 72]*x11*x23*x31*x42    
        +coeff[ 73]*x11*x21*x33*x42    
        +coeff[ 74]*x11*x23    *x43    
        +coeff[ 75]*x11*x22*x31*x43    
        +coeff[ 76]*x11*x23*x31*x41*x51
        +coeff[ 77]*x11*x21*x33*x41*x51
        +coeff[ 78]*x11*x21*x31*x43*x51
        +coeff[ 79]*x11*x21*x31*x42*x52
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 80]*x11*x21*x32    *x53
        +coeff[ 81]*x11    *x31*x41*x54
        +coeff[ 82]*x12*x23*x32        
        +coeff[ 83]*x12*x22*x33        
        +coeff[ 84]*x12*x22    *x42*x51
        +coeff[ 85]*x13*x23*x31        
        +coeff[ 86]*x13*x21*x33        
        +coeff[ 87]    *x23*x33*x42    
        +coeff[ 88]    *x22*x34*x42    
    ;
    v_l_e_q1q2_2_600                              =v_l_e_q1q2_2_600                              
        +coeff[ 89]    *x21*x34*x43    
        ;

    return v_l_e_q1q2_2_600                              ;
}
float x_e_q1q2_2_500                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1296418E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.12919361E-02, 0.12206688E+00, 0.16469694E-02, 0.26550633E-02,
        -0.38851102E-03, 0.86298765E-04,-0.58408617E-03,-0.46366744E-03,
        -0.14531065E-03,-0.30194593E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_500                              =v_x_e_q1q2_2_500                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_500                              ;
}
float t_e_q1q2_2_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5150908E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.51600251E-04,-0.19998390E-02,-0.26142939E-02, 0.23070106E-02,
         0.72189687E-04,-0.17823311E-03,-0.27811382E-03,-0.32359720E-03,
         0.37577788E-05,-0.23796347E-03,-0.13906804E-03,-0.88400855E-04,
        -0.81027119E-03,-0.63956069E-03,-0.28292014E-04,-0.52763306E-03,
        -0.44088744E-03,-0.32821445E-04,-0.21099899E-04, 0.55675515E-04,
         0.40821447E-04,-0.22037039E-03,-0.11315776E-04, 0.36780166E-04,
        -0.13452142E-04,-0.88253528E-05, 0.73740821E-05, 0.69336702E-05,
        -0.39697948E-05,-0.72082530E-05,-0.63893876E-05, 0.44865278E-05,
         0.61995188E-05, 0.55210171E-05,-0.79144538E-05, 0.90386766E-05,
         0.10442393E-04, 0.14598748E-04, 0.16117683E-04, 0.14750101E-04,
        -0.14411767E-04, 0.14083545E-04, 0.67953970E-05,-0.12901577E-04,
        -0.12436424E-04,-0.11284989E-04, 0.13093334E-04, 0.11227912E-04,
         0.88509778E-05, 0.17689306E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_500                              =v_t_e_q1q2_2_500                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x32*x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_2_500                              =v_t_e_q1q2_2_500                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x33*x42    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22    *x41    
    ;
    v_t_e_q1q2_2_500                              =v_t_e_q1q2_2_500                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x22        *x51
        +coeff[ 28]*x11            *x52
        +coeff[ 29]*x12*x21    *x41    
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x12*x21        *x51
        +coeff[ 33]*x11*x22        *x51
        +coeff[ 34]*x11*x21    *x41*x51
    ;
    v_t_e_q1q2_2_500                              =v_t_e_q1q2_2_500                              
        +coeff[ 35]*x13*x22            
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x11*x22*x32        
        +coeff[ 38]*x13*x21    *x41    
        +coeff[ 39]    *x22*x32*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12    *x31*x42    
        +coeff[ 42]*x12        *x43    
        +coeff[ 43]*x11*x21    *x43    
    ;
    v_t_e_q1q2_2_500                              =v_t_e_q1q2_2_500                              
        +coeff[ 44]*x11*x21*x32    *x51
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]*x12*x21*x33        
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]*x11*x22*x32    *x51
        ;

    return v_t_e_q1q2_2_500                              ;
}
float y_e_q1q2_2_500                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2869052E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.25719960E-03, 0.13632365E+00, 0.90375759E-01,-0.17140075E-02,
        -0.15617316E-02,-0.10681956E-03, 0.24913559E-04,-0.18401901E-03,
         0.18174553E-03,-0.10643966E-03, 0.11736574E-03, 0.76660079E-04,
         0.76176060E-04, 0.66817985E-04,-0.23145443E-03,-0.23510777E-05,
         0.26411351E-04,-0.65166991E-04,-0.63114931E-04, 0.27807964E-04,
         0.42118700E-05, 0.74950385E-05,-0.11574353E-04, 0.42569663E-04,
         0.40278723E-04, 0.28579523E-04,-0.96268581E-04,-0.47920661E-04,
        -0.18058638E-04, 0.20701227E-04,-0.17874174E-04,-0.23764595E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_500                              =v_y_e_q1q2_2_500                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21*x31*x42    
    ;
    v_y_e_q1q2_2_500                              =v_y_e_q1q2_2_500                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x31*x45*x52
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]        *x31*x42*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]*x11*x21    *x43    
    ;
    v_y_e_q1q2_2_500                              =v_y_e_q1q2_2_500                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11*x21    *x42*x51
        +coeff[ 29]    *x22*x31*x43    
        +coeff[ 30]*x12*x21*x31*x41    
        +coeff[ 31]    *x21*x31*x43*x51
        ;

    return v_y_e_q1q2_2_500                              ;
}
float p_e_q1q2_2_500                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.1764189E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.16075683E-03, 0.32386485E-01, 0.66792585E-01,-0.15770834E-02,
         0.45672941E-05,-0.14028898E-02,-0.31173151E-03,-0.37342828E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_500                              ;
}
float l_e_q1q2_2_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1859289E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18444670E-02,-0.34772749E-02,-0.66048105E-03,-0.30636007E-02,
         0.76659402E-03,-0.11902688E-02,-0.40745994E-03,-0.10333750E-02,
        -0.24419665E-03,-0.19506658E-02,-0.39750430E-04,-0.11011541E-02,
         0.11475536E-02, 0.80478925E-03, 0.13037027E-02, 0.58265269E-03,
         0.10231208E-02, 0.18965012E-02, 0.18655106E-02,-0.18342149E-03,
        -0.64428114E-05,-0.16928208E-02, 0.42122709E-04,-0.78163255E-04,
         0.77469466E-03, 0.21566964E-03,-0.25681857E-03,-0.36432190E-03,
         0.31451631E-03,-0.20669978E-03, 0.89409365E-03,-0.12008494E-02,
        -0.18106501E-03,-0.16413459E-02, 0.49796305E-03,-0.55636914E-03,
         0.63063885E-03,-0.16233873E-02, 0.16462388E-03, 0.44213978E-03,
        -0.79279073E-03, 0.24096511E-03, 0.84321114E-03,-0.50045719E-03,
         0.16063746E-02, 0.34355628E-03, 0.77974627E-03, 0.11259005E-02,
        -0.15064969E-03, 0.15497587E-02, 0.35926740E-03,-0.11037291E-02,
         0.94815600E-03, 0.10200326E-02, 0.31689447E-03, 0.82452648E-03,
        -0.14762301E-02, 0.39979169E-03,-0.97445882E-03, 0.81527105E-03,
         0.37393896E-03,-0.13566016E-02, 0.66154398E-03, 0.81312307E-03,
         0.22292853E-03,-0.42980385E-03,-0.71609154E-03, 0.32890419E-03,
        -0.55655523E-03,-0.59014972E-03, 0.47104480E-02, 0.37533548E-02,
         0.15745300E-02,-0.23450959E-02,-0.81631285E-03,-0.43826993E-02,
        -0.13707047E-02,-0.14825498E-02, 0.75108168E-03, 0.59666473E-03,
         0.19933358E-02,-0.12355623E-02, 0.21148378E-02,-0.10209888E-02,
         0.84711536E-03,-0.15874718E-02,-0.73610712E-03,-0.62052108E-03,
        -0.49666014E-04,-0.33290093E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x32        
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]    *x22    *x42    
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]            *x42*x52
        +coeff[ 11]    *x22*x31*x42    
        +coeff[ 12]*x11*x21*x32    *x51
        +coeff[ 13]    *x24*x31*x41    
        +coeff[ 14]    *x22*x33*x41    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]        *x33*x43    
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 17]        *x31*x41*x54
        +coeff[ 18]    *x21*x33*x41*x52
        +coeff[ 19]            *x41    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]        *x31*x41    
        +coeff[ 22]    *x21        *x51
        +coeff[ 23]            *x41*x51
        +coeff[ 24]    *x22    *x41    
        +coeff[ 25]            *x43    
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 26]*x12        *x41    
        +coeff[ 27]    *x23    *x41    
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]            *x43*x51
        +coeff[ 30]*x11*x22        *x51
        +coeff[ 31]*x11*x21*x31    *x51
        +coeff[ 32]*x12            *x52
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x21*x31*x42*x51
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 35]    *x21*x31*x41*x52
        +coeff[ 36]        *x32*x41*x52
        +coeff[ 37]    *x21*x31    *x53
        +coeff[ 38]    *x21    *x41*x53
        +coeff[ 39]*x11*x21*x33        
        +coeff[ 40]*x11*x22    *x41*x51
        +coeff[ 41]*x11        *x41*x53
        +coeff[ 42]*x12*x21*x31    *x51
        +coeff[ 43]*x12*x21    *x41*x51
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 44]*x12    *x31*x41*x51
        +coeff[ 45]*x12        *x42*x51
        +coeff[ 46]    *x21*x34*x41    
        +coeff[ 47]    *x22    *x44    
        +coeff[ 48]    *x21*x34    *x51
        +coeff[ 49]    *x23*x31*x41*x51
        +coeff[ 50]    *x23        *x53
        +coeff[ 51]    *x21*x31*x41*x53
        +coeff[ 52]        *x32*x41*x53
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 53]*x11*x24*x31        
        +coeff[ 54]*x11*x23*x32        
        +coeff[ 55]*x11*x23*x31    *x51
        +coeff[ 56]*x11*x22*x32    *x51
        +coeff[ 57]*x11    *x34    *x51
        +coeff[ 58]*x11*x21    *x42*x52
        +coeff[ 59]*x11*x21*x31    *x53
        +coeff[ 60]*x12*x24            
        +coeff[ 61]*x12*x21*x32*x41    
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 62]*x12*x21    *x43    
        +coeff[ 63]*x12    *x31*x41*x52
        +coeff[ 64]*x12    *x31    *x53
        +coeff[ 65]*x12        *x41*x53
        +coeff[ 66]*x13*x22*x31        
        +coeff[ 67]*x13*x22    *x41    
        +coeff[ 68]*x13*x21*x31*x41    
        +coeff[ 69]*x13*x22        *x51
        +coeff[ 70]    *x21*x34*x41*x51
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 71]    *x21*x33*x42*x51
        +coeff[ 72]    *x21*x33    *x53
        +coeff[ 73]    *x21*x32*x41*x53
        +coeff[ 74]*x11    *x34*x41*x51
        +coeff[ 75]*x11*x21*x32*x42*x51
        +coeff[ 76]*x11*x21*x31*x43*x51
        +coeff[ 77]*x11*x21*x31*x41*x53
        +coeff[ 78]*x11        *x43*x53
        +coeff[ 79]*x12*x22*x33        
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 80]*x12*x24    *x41    
        +coeff[ 81]*x12*x22*x32*x41    
        +coeff[ 82]*x12*x23    *x41*x51
        +coeff[ 83]*x12*x22    *x41*x52
        +coeff[ 84]*x12*x21    *x42*x52
        +coeff[ 85]*x12    *x31*x41*x53
        +coeff[ 86]        *x34*x43*x51
        +coeff[ 87]*x13*x21*x31    *x52
        +coeff[ 88]    *x21            
    ;
    v_l_e_q1q2_2_500                              =v_l_e_q1q2_2_500                              
        +coeff[ 89]        *x31        
        ;

    return v_l_e_q1q2_2_500                              ;
}
float x_e_q1q2_2_450                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.4757632E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.48417732E-03, 0.12205967E+00, 0.16465486E-02, 0.26509527E-02,
        -0.38121745E-03, 0.86819869E-04,-0.55046484E-03,-0.44570593E-03,
        -0.34391973E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_450                              =v_x_e_q1q2_2_450                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_450                              ;
}
float t_e_q1q2_2_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1900950E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18493389E-04,-0.20035226E-02,-0.26012887E-02, 0.23112260E-02,
         0.71008486E-04,-0.17565898E-03,-0.30137249E-03,-0.33905046E-03,
        -0.88683544E-06,-0.25974281E-03,-0.13149921E-03,-0.79822530E-04,
        -0.78392663E-03,-0.59530826E-03,-0.32035925E-04,-0.27228743E-04,
        -0.49823686E-03,-0.39691111E-03,-0.13413944E-04, 0.57233941E-04,
         0.34220284E-04,-0.18080111E-04,-0.19277726E-04,-0.48811376E-04,
        -0.21328317E-03,-0.26660338E-04, 0.70631136E-05, 0.55491654E-06,
         0.43633872E-05,-0.54982165E-05,-0.54405396E-05,-0.16745962E-06,
         0.58333967E-05, 0.33865494E-04, 0.77663690E-05, 0.94367560E-05,
         0.10700325E-04, 0.19264327E-04, 0.12505118E-04,-0.65357976E-05,
        -0.27188877E-04, 0.87670869E-05, 0.94605757E-05,-0.53336485E-05,
        -0.13668509E-04,-0.10911412E-04,-0.42686406E-04,-0.13462855E-04,
         0.95468049E-05, 0.20779346E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_450                              =v_t_e_q1q2_2_450                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x33*x41    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q1q2_2_450                              =v_t_e_q1q2_2_450                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x11*x23    *x41*x51
    ;
    v_t_e_q1q2_2_450                              =v_t_e_q1q2_2_450                              
        +coeff[ 26]*x11*x21            
        +coeff[ 27]        *x31*x41    
        +coeff[ 28]*x13                
        +coeff[ 29]*x12*x21            
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]            *x41*x52
        +coeff[ 32]*x11*x22*x31        
        +coeff[ 33]*x11*x21*x31*x41    
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_q1q2_2_450                              =v_t_e_q1q2_2_450                              
        +coeff[ 35]*x11*x22        *x51
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]*x13*x22            
        +coeff[ 38]*x11*x21*x33        
        +coeff[ 39]*x11*x23    *x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]    *x22    *x42*x51
        +coeff[ 43]*x13            *x52
    ;
    v_t_e_q1q2_2_450                              =v_t_e_q1q2_2_450                              
        +coeff[ 44]    *x23        *x52
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]*x11*x21*x33*x41    
        +coeff[ 47]*x13*x21*x31    *x51
        +coeff[ 48]*x13        *x42*x51
        +coeff[ 49]    *x23    *x42*x51
        ;

    return v_t_e_q1q2_2_450                              ;
}
float y_e_q1q2_2_450                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.5349575E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.51955908E-03, 0.13632922E+00, 0.90359516E-01,-0.17033402E-02,
        -0.15600224E-02,-0.11475600E-03, 0.44543140E-04,-0.21503591E-03,
         0.18842367E-03,-0.10162713E-03, 0.11337904E-03, 0.63359163E-04,
         0.70037284E-04, 0.67813824E-04,-0.22912836E-03,-0.13696744E-05,
        -0.23655612E-04, 0.74989548E-05,-0.29045702E-05, 0.58097303E-05,
         0.19087956E-04, 0.25000334E-05, 0.29890984E-04,-0.15895013E-04,
        -0.16401766E-04,-0.13706534E-04,-0.65477034E-04, 0.14479172E-04,
        -0.45612702E-04,-0.44289645E-04,-0.33206874E-04,-0.20385547E-04,
        -0.27794245E-04,-0.31067935E-04,-0.14876945E-04, 0.84477762E-06,
         0.32326676E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_2_450                              =v_y_e_q1q2_2_450                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_450                              =v_y_e_q1q2_2_450                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]        *x33        
        +coeff[ 19]            *x44    
        +coeff[ 20]    *x21    *x44    
        +coeff[ 21]*x11        *x42*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21    *x41*x51
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]    *x21    *x43*x51
    ;
    v_y_e_q1q2_2_450                              =v_y_e_q1q2_2_450                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x11*x21*x32*x41    
        +coeff[ 31]*x12*x21    *x42    
        +coeff[ 32]*x12    *x31*x42    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]            *x45*x51
    ;
    v_y_e_q1q2_2_450                              =v_y_e_q1q2_2_450                              
        +coeff[ 35]    *x22    *x43*x51
        +coeff[ 36]    *x24    *x41*x51
        ;

    return v_y_e_q1q2_2_450                              ;
}
float p_e_q1q2_2_450                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.2386714E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.23089943E-03, 0.32375641E-01, 0.66787913E-01,-0.15753657E-02,
         0.18356401E-04,-0.14038233E-02,-0.31391863E-03,-0.37335692E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]    *x22*x31    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_450                              ;
}
float l_e_q1q2_2_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1896032E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17393483E-02,-0.32543472E-02,-0.30295708E-03,-0.33237697E-02,
        -0.43153239E-03, 0.67531498E-03,-0.25652317E-03, 0.14709831E-02,
         0.71770314E-03, 0.25950896E-03,-0.29145653E-03, 0.93181717E-03,
        -0.46698627E-03, 0.98727956E-04, 0.66624262E-03,-0.20671671E-02,
         0.36674079E-02,-0.86630828E-03, 0.64844097E-03, 0.39202170E-02,
         0.10452783E-02, 0.16785404E-03, 0.71963543E-04,-0.22633235E-05,
        -0.28189733E-02, 0.25424693E-03, 0.34471066E-03, 0.19236738E-03,
        -0.48488815E-03,-0.56787644E-03,-0.14525549E-04,-0.36507805E-04,
         0.12397832E-03, 0.31888820E-03, 0.21095145E-03, 0.10524589E-03,
        -0.35087805E-03,-0.12092516E-02,-0.29748134E-03, 0.10979171E-02,
        -0.32680674E-03,-0.10929539E-03,-0.89607172E-03, 0.13221885E-03,
         0.36024400E-02, 0.51081693E-03,-0.44000498E-03, 0.90092253E-04,
        -0.23703721E-03,-0.43300880E-03, 0.89381728E-03, 0.11859735E-02,
         0.70588192E-03, 0.45002956E-03, 0.23068094E-02,-0.27057176E-03,
         0.10324974E-02, 0.46765589E-03, 0.34250677E-03,-0.67218457E-03,
        -0.91296178E-03,-0.29130964E-03, 0.24821013E-02, 0.21733339E-02,
         0.64289692E-03, 0.97709533E-03,-0.31589312E-03,-0.27405444E-03,
         0.71850809E-03,-0.22392427E-02,-0.12520947E-02,-0.15025227E-02,
         0.17943309E-02,-0.47248101E-03, 0.68535237E-03,-0.14616470E-02,
         0.46949144E-03,-0.93084422E-03, 0.16504498E-02,-0.71095256E-03,
         0.22430303E-03, 0.66865282E-03, 0.11319997E-02, 0.83173107E-03,
        -0.26916214E-02,-0.20706032E-02,-0.18475660E-02,-0.19811385E-02,
         0.13203418E-02,-0.51878125E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]*x11            *x52
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32*x42    
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]            *x44    
        +coeff[ 10]        *x31*x41*x52
        +coeff[ 11]*x11    *x32*x41*x51
        +coeff[ 12]    *x24*x32        
        +coeff[ 13]    *x22*x34        
        +coeff[ 14]    *x22*x32*x42    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]        *x33*x43    
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 17]*x12*x22    *x42    
        +coeff[ 18]*x13    *x31*x42    
        +coeff[ 19]        *x34*x44    
        +coeff[ 20]*x13*x21    *x42*x51
        +coeff[ 21]                *x51
        +coeff[ 22]*x11                
        +coeff[ 23]    *x21    *x41    
        +coeff[ 24]        *x31*x41    
        +coeff[ 25]            *x41*x51
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x21*x31*x41    
        +coeff[ 28]        *x32*x41    
        +coeff[ 29]    *x21    *x42    
        +coeff[ 30]    *x22        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]    *x21    *x41*x51
        +coeff[ 33]            *x42*x51
        +coeff[ 34]*x11*x21*x31        
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 35]*x12    *x31        
        +coeff[ 36]*x12            *x51
        +coeff[ 37]    *x22    *x42    
        +coeff[ 38]    *x22*x31    *x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]*x11*x21*x32        
        +coeff[ 42]*x12        *x41*x51
        +coeff[ 43]    *x22*x33        
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x21*x31*x43    
        +coeff[ 46]    *x22*x32    *x51
        +coeff[ 47]    *x21    *x42*x52
        +coeff[ 48]            *x43*x52
        +coeff[ 49]            *x42*x53
        +coeff[ 50]*x11    *x31*x42*x51
        +coeff[ 51]*x11    *x31*x41*x52
        +coeff[ 52]*x12    *x32    *x51
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 53]    *x24*x31*x41    
        +coeff[ 54]    *x24    *x42    
        +coeff[ 55]*x13    *x31    *x51
        +coeff[ 56]    *x22*x33    *x51
        +coeff[ 57]    *x23    *x42*x51
        +coeff[ 58]*x13            *x52
        +coeff[ 59]    *x24        *x52
        +coeff[ 60]    *x22        *x54
        +coeff[ 61]*x11*x21*x33*x41    
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 62]*x11*x21*x32*x42    
        +coeff[ 63]*x11*x21*x31*x43    
        +coeff[ 64]*x11*x21    *x44    
        +coeff[ 65]*x11*x21*x31*x41*x52
        +coeff[ 66]*x12    *x33    *x51
        +coeff[ 67]*x12*x21        *x53
        +coeff[ 68]*x12        *x41*x53
        +coeff[ 69]    *x24*x32*x41    
        +coeff[ 70]    *x23*x32*x42    
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 71]    *x22*x32*x43    
        +coeff[ 72]    *x21*x32*x44    
        +coeff[ 73]*x13    *x32    *x51
        +coeff[ 74]    *x24*x32    *x51
        +coeff[ 75]    *x22*x34    *x51
        +coeff[ 76]*x13*x21    *x41*x51
        +coeff[ 77]    *x24*x31*x41*x51
        +coeff[ 78]    *x21*x32*x42*x52
        +coeff[ 79]        *x33*x42*x52
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 80]*x13            *x53
        +coeff[ 81]*x11    *x34*x42    
        +coeff[ 82]*x11*x22*x31*x41*x52
        +coeff[ 83]*x11*x21*x32*x41*x52
        +coeff[ 84]*x11    *x33*x41*x52
        +coeff[ 85]*x11    *x32*x42*x52
        +coeff[ 86]*x11    *x32*x41*x53
        +coeff[ 87]*x12*x21*x32*x42    
        +coeff[ 88]*x12*x21    *x44    
    ;
    v_l_e_q1q2_2_450                              =v_l_e_q1q2_2_450                              
        +coeff[ 89]*x12*x23        *x52
        ;

    return v_l_e_q1q2_2_450                              ;
}
float x_e_q1q2_2_449                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1050832E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.10377462E-02, 0.12258431E+00, 0.16645397E-02, 0.26301481E-02,
        -0.37597443E-03, 0.87118533E-04,-0.60121273E-03,-0.43263051E-03,
        -0.17081139E-03,-0.33292209E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_449                              =v_x_e_q1q2_2_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_449                              ;
}
float t_e_q1q2_2_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6155692E-05;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59645404E-05,-0.19856675E-02,-0.21409877E-02, 0.22858453E-02,
         0.71884620E-04,-0.18001851E-03,-0.31281234E-03,-0.32360939E-03,
         0.19686189E-04,-0.32302720E-03,-0.17413349E-03,-0.84159088E-04,
        -0.91206923E-03,-0.63869380E-03,-0.22220953E-04, 0.67289693E-06,
        -0.13598285E-04,-0.12769588E-04, 0.23403387E-04, 0.57548910E-04,
         0.34405661E-04,-0.53031940E-05,-0.27756023E-03,-0.58032403E-03,
        -0.43920701E-03,-0.14147308E-04,-0.18424242E-05,-0.52924747E-05,
        -0.11710484E-05,-0.86469763E-05,-0.12672140E-04, 0.31875679E-05,
         0.46426999E-05, 0.13940559E-04, 0.15313828E-04,-0.15591420E-04,
         0.23775245E-04,-0.99063000E-05,-0.13982884E-04,-0.45363544E-04,
        -0.35690638E-04, 0.11107141E-04,-0.64059195E-05, 0.10982732E-04,
        -0.84201256E-05,-0.17821389E-04, 0.10207443E-04, 0.59315926E-05,
        -0.18368073E-04, 0.20386075E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_449                              =v_t_e_q1q2_2_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x22*x31        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_449                              =v_t_e_q1q2_2_449                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x31    *x51
    ;
    v_t_e_q1q2_2_449                              =v_t_e_q1q2_2_449                              
        +coeff[ 26]    *x23*x32    *x51
        +coeff[ 27]    *x21*x31        
        +coeff[ 28]*x13                
        +coeff[ 29]*x12*x21            
        +coeff[ 30]*x11    *x32        
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]*x11    *x32*x41    
        +coeff[ 33]    *x21*x31*x42    
        +coeff[ 34]    *x21    *x43    
    ;
    v_t_e_q1q2_2_449                              =v_t_e_q1q2_2_449                              
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]*x13    *x31*x41    
        +coeff[ 39]*x11*x22*x31*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x22        *x51
        +coeff[ 42]*x12    *x32    *x51
        +coeff[ 43]    *x21*x31*x42*x51
    ;
    v_t_e_q1q2_2_449                              =v_t_e_q1q2_2_449                              
        +coeff[ 44]    *x22*x33*x41    
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]*x11*x21*x31*x43    
        +coeff[ 47]*x11*x23*x31    *x51
        +coeff[ 48]*x11*x21*x33    *x51
        +coeff[ 49]    *x22*x33    *x51
        ;

    return v_t_e_q1q2_2_449                              ;
}
float y_e_q1q2_2_449                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.5872610E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.59726991E-03, 0.13595393E+00, 0.10005992E+00,-0.17144845E-02,
         0.14302192E-05,-0.16967067E-02,-0.52634681E-04,-0.42614462E-04,
        -0.19903877E-03, 0.24629716E-03,-0.53415515E-04, 0.13172734E-03,
         0.12067633E-03, 0.65433596E-04, 0.53093769E-04,-0.28497074E-03,
        -0.24302431E-04, 0.35525245E-05,-0.29770657E-03,-0.11886240E-03,
         0.47827318E-04,-0.35254729E-04,-0.60241332E-05, 0.45251112E-04,
         0.29135672E-04,-0.23349599E-03, 0.13053999E-04, 0.28093582E-04,
         0.25344978E-04,-0.32743308E-04,-0.41309271E-04, 0.29131441E-04,
        -0.27964395E-04,-0.54653909E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_449                              =v_y_e_q1q2_2_449                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_449                              =v_y_e_q1q2_2_449                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x24*x31    *x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]            *x44    
        +coeff[ 23]*x11*x21*x31*x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1q2_2_449                              =v_y_e_q1q2_2_449                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]*x11        *x43*x51
        +coeff[ 28]*x11    *x33*x41    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x22    *x41*x51
        +coeff[ 31]        *x33    *x52
        +coeff[ 32]*x11    *x31*x41*x52
        +coeff[ 33]*x11*x23*x31*x41    
        ;

    return v_y_e_q1q2_2_449                              ;
}
float p_e_q1q2_2_449                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3147452E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.31810720E-03, 0.35665244E-01, 0.66459201E-01,-0.15396331E-02,
        -0.15609747E-02,-0.33843011E-03,-0.36089699E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_449                              ;
}
float l_e_q1q2_2_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1840298E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19039429E-02, 0.11618331E-03,-0.34492137E-02,-0.81192044E-03,
        -0.34203937E-02,-0.25721971E-03,-0.15173763E-02,-0.36817871E-03,
        -0.11719617E-02, 0.16552145E-02, 0.90882683E-03, 0.31022285E-02,
        -0.29589483E-03, 0.61160936E-02,-0.10795178E-02, 0.45709279E-02,
        -0.22390392E-03, 0.87993076E-05,-0.24540678E-02, 0.94893301E-04,
        -0.11100479E-02, 0.19047206E-03,-0.53177460E-03, 0.62849512E-03,
         0.21804273E-03, 0.63466352E-04,-0.43777813E-03, 0.20716223E-02,
         0.35449013E-03, 0.15127417E-03, 0.10261493E-02,-0.11082587E-02,
        -0.65601627E-04, 0.28838048E-03, 0.65250386E-03, 0.97568391E-03,
        -0.20992418E-03, 0.75001386E-03, 0.74559281E-03, 0.54362178E-03,
         0.56711258E-03,-0.74825430E-03, 0.83525910E-03,-0.31964146E-03,
        -0.52414823E-03, 0.71241887E-03,-0.68087835E-03,-0.62007003E-03,
         0.98993152E-03, 0.15447409E-02, 0.34484110E-03, 0.58230746E-03,
        -0.18215175E-02, 0.10605231E-02,-0.18357026E-02,-0.15694286E-02,
         0.45030387E-03,-0.91315201E-03, 0.94018970E-03,-0.60110033E-03,
        -0.18012929E-02, 0.17438829E-02, 0.55570014E-04, 0.16495133E-02,
         0.13580087E-02,-0.11982942E-02,-0.87024120E-03, 0.13600804E-03,
        -0.67978457E-04,-0.72936760E-03, 0.96029131E-03,-0.28058097E-02,
        -0.12180769E-02, 0.93862682E-03,-0.56170038E-03,-0.63326443E-03,
         0.62036957E-03,-0.52107952E-03,-0.45991881E-03, 0.44263498E-03,
         0.28422049E-02,-0.16989023E-02,-0.71447052E-03,-0.82350936E-03,
        -0.12937102E-02, 0.46953734E-03, 0.10111097E-02,-0.12177407E-02,
         0.57681446E-03,-0.16752074E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x23*x31        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]    *x24*x31*x41    
        +coeff[ 11]    *x22*x33*x41    
        +coeff[ 12]    *x24    *x42    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]    *x22*x31*x43    
        +coeff[ 16]        *x32*x44    
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 17]            *x41    
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]    *x22*x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]*x12    *x31        
        +coeff[ 24]    *x21*x33        
        +coeff[ 25]        *x34        
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]        *x32*x41*x51
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]            *x42*x52
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]*x11    *x32*x41    
        +coeff[ 32]*x11*x22        *x51
        +coeff[ 33]*x11        *x42*x51
        +coeff[ 34]*x11    *x31    *x52
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 35]*x12    *x31*x41    
        +coeff[ 36]*x13    *x31        
        +coeff[ 37]    *x24*x31        
        +coeff[ 38]    *x22*x32*x41    
        +coeff[ 39]        *x34*x41    
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]    *x21    *x44    
        +coeff[ 42]    *x24        *x51
        +coeff[ 43]    *x21*x32    *x52
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 44]*x11*x21*x33        
        +coeff[ 45]*x11*x21*x32*x41    
        +coeff[ 46]*x11    *x33    *x51
        +coeff[ 47]*x12    *x33        
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x12*x21    *x42    
        +coeff[ 50]*x12*x21    *x41*x51
        +coeff[ 51]*x13    *x31    *x51
        +coeff[ 52]        *x34*x41*x51
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 53]    *x22*x31*x42*x51
        +coeff[ 54]    *x21*x33    *x52
        +coeff[ 55]        *x33*x41*x52
        +coeff[ 56]    *x21*x31*x42*x52
        +coeff[ 57]        *x31*x43*x52
        +coeff[ 58]    *x21*x31    *x54
        +coeff[ 59]*x11*x23*x32        
        +coeff[ 60]*x11*x24    *x41    
        +coeff[ 61]*x11    *x34*x41    
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 62]*x11*x22*x31*x41*x51
        +coeff[ 63]*x11*x22    *x42*x51
        +coeff[ 64]*x11*x21*x31*x42*x51
        +coeff[ 65]*x11        *x44*x51
        +coeff[ 66]*x11*x22*x31    *x52
        +coeff[ 67]*x11*x21    *x42*x52
        +coeff[ 68]*x11*x21*x31    *x53
        +coeff[ 69]*x12*x22*x32        
        +coeff[ 70]*x12    *x34        
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 71]*x12*x22*x31*x41    
        +coeff[ 72]*x12    *x32*x42    
        +coeff[ 73]*x12*x23        *x51
        +coeff[ 74]*x12*x21*x32    *x51
        +coeff[ 75]*x12*x22        *x52
        +coeff[ 76]*x12        *x42*x52
        +coeff[ 77]*x12*x21        *x53
        +coeff[ 78]*x12        *x41*x53
        +coeff[ 79]*x13*x21    *x42    
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 80]    *x22*x33*x42    
        +coeff[ 81]    *x23*x31*x43    
        +coeff[ 82]*x13*x21*x31    *x51
        +coeff[ 83]        *x31*x44*x52
        +coeff[ 84]        *x32*x41*x54
        +coeff[ 85]            *x43*x54
        +coeff[ 86]*x11*x23*x33        
        +coeff[ 87]*x11*x22*x31*x42*x51
        +coeff[ 88]*x11    *x31*x43*x52
    ;
    v_l_e_q1q2_2_449                              =v_l_e_q1q2_2_449                              
        +coeff[ 89]*x12*x23    *x42    
        ;

    return v_l_e_q1q2_2_449                              ;
}
float x_e_q1q2_2_400                              (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.2033635E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.21721916E-03, 0.12262137E+00, 0.16677687E-02, 0.26309392E-02,
        -0.30475348E-03, 0.87739187E-04,-0.69253746E-03,-0.47462276E-03,
        -0.64407963E-04,-0.23970222E-03,-0.27996532E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_400                              =v_x_e_q1q2_2_400                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        ;

    return v_x_e_q1q2_2_400                              ;
}
float t_e_q1q2_2_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1811522E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.17571443E-04,-0.19837986E-02,-0.20637442E-02, 0.22846940E-02,
         0.71273360E-04,-0.18529392E-03,-0.33287588E-03,-0.33275518E-03,
         0.13060509E-05,-0.28606359E-03,-0.18724798E-03,-0.81729180E-04,
        -0.84990961E-03,-0.60256949E-03,-0.14408934E-04, 0.65776396E-04,
        -0.43483809E-03,-0.22768711E-04,-0.10239505E-04, 0.35700868E-04,
         0.12727029E-04,-0.18995741E-04, 0.91492129E-05, 0.15132374E-04,
        -0.27137811E-03,-0.56650309E-03,-0.16292453E-04, 0.19723007E-04,
        -0.18269522E-04, 0.38999337E-05, 0.90235035E-05,-0.31323109E-05,
        -0.34606619E-05,-0.12433831E-04, 0.13804064E-05,-0.77442037E-05,
        -0.53448330E-05,-0.76220749E-05, 0.15243214E-04, 0.11229222E-04,
        -0.67159508E-05,-0.15346764E-04, 0.13206077E-04,-0.39486851E-04,
        -0.80246664E-05,-0.28904175E-04, 0.73566953E-05, 0.13979188E-04,
        -0.15390438E-04, 0.88439610E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_400                              =v_t_e_q1q2_2_400                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_2_400                              =v_t_e_q1q2_2_400                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]*x13*x21*x31        
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x21*x33        
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_t_e_q1q2_2_400                              =v_t_e_q1q2_2_400                              
        +coeff[ 26]*x11        *x42*x52
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]*x11*x21    *x41    
        +coeff[ 30]    *x21    *x41*x51
        +coeff[ 31]*x12*x21*x31        
        +coeff[ 32]*x12    *x32        
        +coeff[ 33]*x11*x21*x32        
        +coeff[ 34]*x12*x21    *x41    
    ;
    v_t_e_q1q2_2_400                              =v_t_e_q1q2_2_400                              
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]*x11    *x31*x42    
        +coeff[ 37]    *x21    *x43    
        +coeff[ 38]    *x21*x32    *x51
        +coeff[ 39]*x11*x21        *x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]*x13*x22            
        +coeff[ 42]*x12*x22    *x41    
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_q1q2_2_400                              =v_t_e_q1q2_2_400                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x11*x23        *x51
        +coeff[ 47]*x11*x22*x31    *x51
        +coeff[ 48]*x12*x21    *x41*x51
        +coeff[ 49]*x11*x22    *x41*x51
        ;

    return v_t_e_q1q2_2_400                              ;
}
float y_e_q1q2_2_400                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.6075938E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.65107335E-03, 0.13596736E+00, 0.10000754E+00,-0.17083618E-02,
        -0.11393429E-04,-0.16794119E-02,-0.63020503E-04,-0.43742180E-04,
        -0.22958449E-03, 0.25497167E-03,-0.23971368E-04, 0.13136609E-03,
         0.12805755E-03, 0.82422026E-04, 0.70727321E-04,-0.29274123E-03,
        -0.19597921E-04,-0.38789120E-04, 0.84978001E-05,-0.28872420E-03,
        -0.92163857E-04,-0.58943479E-05,-0.19282315E-04,-0.46760451E-04,
        -0.20579875E-04,-0.11500421E-04,-0.16815879E-04, 0.10750387E-04,
         0.25584035E-04, 0.11902638E-04,-0.23166006E-03,-0.11684103E-04,
         0.54583546E-04,-0.37937276E-04,-0.28263263E-04, 0.22024922E-04,
         0.28476563E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1q2_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_400                              =v_y_e_q1q2_2_400                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_400                              =v_y_e_q1q2_2_400                              
        +coeff[ 17]        *x31*x42*x51
        +coeff[ 18]*x11*x21    *x43    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]        *x32*x42    
    ;
    v_y_e_q1q2_2_400                              =v_y_e_q1q2_2_400                              
        +coeff[ 26]*x11    *x31*x42    
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]        *x31*x41*x52
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]*x11        *x41*x52
        +coeff[ 32]    *x22    *x44    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_y_e_q1q2_2_400                              =v_y_e_q1q2_2_400                              
        +coeff[ 35]            *x42*x53
        +coeff[ 36]*x11*x21    *x41*x52
        ;

    return v_y_e_q1q2_2_400                              ;
}
float p_e_q1q2_2_400                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3482176E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.36769913E-03, 0.35615295E-01, 0.66444807E-01,-0.15390120E-02,
        -0.15581391E-02,-0.33633932E-03,-0.36162633E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_400                              ;
}
float l_e_q1q2_2_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1853698E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19515349E-02,-0.36768520E-02,-0.62464457E-03,-0.20652607E-02,
        -0.33146723E-02,-0.15191053E-03, 0.96023490E-04,-0.18631644E-02,
        -0.26390271E-03,-0.78604557E-04,-0.15829108E-03,-0.79517900E-04,
         0.42307712E-03,-0.98404496E-04, 0.31795559E-03, 0.19035550E-02,
        -0.64742856E-03, 0.75332512E-03, 0.13257668E-03, 0.32802741E-03,
        -0.84774627E-04,-0.16289903E-03,-0.25123025E-04,-0.15544074E-02,
        -0.46748703E-03, 0.23598537E-03, 0.22781556E-03, 0.13971012E-02,
         0.16859981E-03,-0.36821384E-04,-0.24999068E-02, 0.17548781E-02,
         0.69251878E-03, 0.57733507E-03,-0.15700437E-03,-0.11734946E-02,
        -0.11491496E-02,-0.34683727E-03,-0.64755493E-03,-0.84720121E-03,
        -0.77687262E-03, 0.62614377E-03, 0.75728103E-03, 0.11714695E-02,
        -0.28713513E-03,-0.13576502E-02, 0.58045326E-03,-0.71230368E-03,
        -0.10245602E-02, 0.43776969E-03,-0.11385407E-02, 0.11566307E-02,
        -0.13245066E-02,-0.18782079E-02, 0.20546261E-03, 0.22844940E-03,
        -0.11850806E-02,-0.25341185E-02,-0.43940949E-03, 0.23683840E-02,
        -0.68896799E-03,-0.55407197E-03,-0.13169947E-02,-0.39682351E-02,
         0.23983063E-02, 0.16760831E-02,-0.51383855E-03, 0.58027334E-03,
        -0.44826217E-03, 0.37987233E-03,-0.65062148E-03,-0.10522186E-02,
        -0.53742994E-03, 0.82634157E-03, 0.60668978E-03, 0.10049307E-02,
        -0.13262768E-02, 0.12632008E-02, 0.11939963E-02, 0.29919005E-04,
         0.19607563E-03,-0.65726505E-04, 0.18187042E-03,-0.18181810E-03,
         0.12237439E-03, 0.32541077E-03,-0.16599048E-03,-0.32767633E-03,
         0.33748284E-03,-0.91302936E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11    *x32        
        +coeff[  6]*x11            *x52
        +coeff[  7]        *x32*x42*x51
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[  8]        *x31        
        +coeff[  9]            *x41    
        +coeff[ 10]                *x51
        +coeff[ 11]*x11                
        +coeff[ 12]            *x41*x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]        *x32*x41    
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]                *x53
        +coeff[ 20]*x11*x21        *x51
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]        *x33*x41    
        +coeff[ 23]        *x31*x43    
        +coeff[ 24]            *x41*x53
        +coeff[ 25]*x11*x22    *x41    
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 26]*x11*x21*x31    *x51
        +coeff[ 27]*x11    *x32    *x51
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x21*x33*x41    
        +coeff[ 31]        *x34*x41    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x21    *x44    
        +coeff[ 34]*x13            *x51
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 35]        *x33*x41*x51
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]*x11*x21*x32    *x51
        +coeff[ 41]*x11*x21    *x42*x51
        +coeff[ 42]*x11*x22        *x52
        +coeff[ 43]*x12    *x31*x41*x51
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 44]*x13*x21    *x41    
        +coeff[ 45]        *x32*x44    
        +coeff[ 46]    *x24        *x52
        +coeff[ 47]    *x22*x31    *x53
        +coeff[ 48]        *x32    *x54
        +coeff[ 49]*x11*x24        *x51
        +coeff[ 50]*x11    *x34    *x51
        +coeff[ 51]*x12*x22*x31    *x51
        +coeff[ 52]*x12    *x33    *x51
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 53]*x12    *x32*x41*x51
        +coeff[ 54]*x12    *x31    *x53
        +coeff[ 55]    *x23*x32*x42    
        +coeff[ 56]    *x21*x33*x43    
        +coeff[ 57]    *x21*x32*x44    
        +coeff[ 58]    *x21*x33*x42*x51
        +coeff[ 59]    *x22    *x44*x51
        +coeff[ 60]*x13*x21        *x52
        +coeff[ 61]    *x24*x31    *x52
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 62]    *x23*x32    *x52
        +coeff[ 63]    *x23*x31*x41*x52
        +coeff[ 64]    *x21*x33*x41*x52
        +coeff[ 65]    *x21*x31*x42*x53
        +coeff[ 66]*x11*x24*x32        
        +coeff[ 67]*x11*x21*x32*x43    
        +coeff[ 68]*x11*x23*x31    *x52
        +coeff[ 69]*x11*x21    *x41*x54
        +coeff[ 70]*x12*x24*x31        
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 71]*x12    *x34*x41    
        +coeff[ 72]*x12*x23*x31    *x51
        +coeff[ 73]*x12*x22        *x53
        +coeff[ 74]*x12*x21    *x41*x53
        +coeff[ 75]*x13*x21*x33        
        +coeff[ 76]*x13*x23    *x41    
        +coeff[ 77]    *x22*x34*x41*x51
        +coeff[ 78]        *x34    *x54
        +coeff[ 79]    *x21            
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 80]        *x31    *x51
        +coeff[ 81]                *x52
        +coeff[ 82]*x11*x21            
        +coeff[ 83]*x11            *x51
        +coeff[ 84]    *x23            
        +coeff[ 85]        *x33        
        +coeff[ 86]    *x22        *x51
        +coeff[ 87]*x11*x21*x31        
        +coeff[ 88]*x11*x21    *x41    
    ;
    v_l_e_q1q2_2_400                              =v_l_e_q1q2_2_400                              
        +coeff[ 89]*x12    *x31        
        ;

    return v_l_e_q1q2_2_400                              ;
}
float x_e_q1q2_2_350                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2442391E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.24420666E-03, 0.12276930E+00, 0.16710998E-02, 0.26277204E-02,
        -0.37728890E-03, 0.86515473E-04,-0.59704843E-03,-0.42937783E-03,
        -0.15936902E-03,-0.33328161E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_350                              =v_x_e_q1q2_2_350                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_350                              ;
}
float t_e_q1q2_2_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1311802E-04;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12452959E-04,-0.19787156E-02,-0.19638713E-02, 0.22751077E-02,
         0.71883842E-04,-0.18007077E-03,-0.30689628E-03,-0.32623744E-03,
         0.20332844E-04,-0.31007081E-03,-0.41431626E-05,-0.17661454E-03,
        -0.84577914E-04,-0.87351585E-03,-0.60396414E-03,-0.23081080E-04,
        -0.29595636E-04,-0.20343838E-04, 0.79192658E-04,-0.63292653E-03,
        -0.47941512E-03,-0.14710244E-04, 0.33282791E-04, 0.43740962E-04,
        -0.30414035E-03, 0.28268087E-05,-0.56850280E-06,-0.23393468E-05,
        -0.18850866E-05,-0.49887603E-05, 0.95282604E-06, 0.28127849E-05,
        -0.43495688E-05,-0.73446099E-05, 0.24408102E-04,-0.25137206E-05,
        -0.66990410E-05, 0.55165838E-05, 0.17408684E-04,-0.61160058E-05,
         0.83951118E-05, 0.18045865E-04,-0.26295193E-04,-0.17454768E-04,
        -0.11077791E-04, 0.82436691E-05, 0.21501191E-04,-0.13155404E-04,
        -0.25149060E-04, 0.11993429E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_350                              =v_t_e_q1q2_2_350                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_350                              =v_t_e_q1q2_2_350                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q1q2_2_350                              =v_t_e_q1q2_2_350                              
        +coeff[ 26]        *x31        
        +coeff[ 27]    *x22            
        +coeff[ 28]            *x42    
        +coeff[ 29]*x12*x21            
        +coeff[ 30]    *x22    *x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]*x11            *x52
        +coeff[ 33]*x12*x21*x31        
        +coeff[ 34]*x11*x22*x31        
    ;
    v_t_e_q1q2_2_350                              =v_t_e_q1q2_2_350                              
        +coeff[ 35]*x11    *x33        
        +coeff[ 36]*x12*x21    *x41    
        +coeff[ 37]*x11*x22        *x51
        +coeff[ 38]    *x23        *x51
        +coeff[ 39]*x11*x21    *x41*x51
        +coeff[ 40]*x12*x22    *x41    
        +coeff[ 41]*x12*x21*x31*x41    
        +coeff[ 42]*x11*x22*x31*x41    
        +coeff[ 43]*x11*x22    *x42    
    ;
    v_t_e_q1q2_2_350                              =v_t_e_q1q2_2_350                              
        +coeff[ 44]*x13*x21        *x51
        +coeff[ 45]*x11*x23        *x51
        +coeff[ 46]*x13*x21*x32        
        +coeff[ 47]*x11*x23*x32        
        +coeff[ 48]*x11*x22*x33        
        +coeff[ 49]*x13*x21*x31*x41    
        ;

    return v_t_e_q1q2_2_350                              ;
}
float y_e_q1q2_2_350                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.1391891E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.13484885E-02, 0.13574497E+00, 0.99913634E-01,-0.17115521E-02,
        -0.13460552E-04,-0.16735939E-02,-0.12512182E-03, 0.37575010E-04,
        -0.17650914E-03, 0.21866808E-03,-0.11935148E-03, 0.11703921E-03,
         0.93759219E-04, 0.68102760E-04, 0.72245406E-04,-0.24643267E-03,
        -0.11525645E-04, 0.21999272E-05,-0.62474928E-05, 0.20547420E-05,
        -0.29001689E-04, 0.31761305E-04,-0.89993817E-04,-0.60670591E-04,
        -0.24698491E-04,-0.46293870E-04, 0.52352996E-04, 0.16900933E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_350                              =v_y_e_q1q2_2_350                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_350                              =v_y_e_q1q2_2_350                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x21    *x42    
        +coeff[ 19]        *x33        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x12    *x31*x42    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_2_350                              =v_y_e_q1q2_2_350                              
        +coeff[ 26]    *x22    *x43*x51
        +coeff[ 27]*x11*x23    *x42    
        ;

    return v_y_e_q1q2_2_350                              ;
}
float p_e_q1q2_2_350                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.6251005E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.60885819E-03, 0.35539076E-01, 0.66295274E-01,-0.15356160E-02,
        -0.15539994E-02,-0.33979208E-03,-0.36437190E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_350                              ;
}
float l_e_q1q2_2_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1894277E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18622422E-02,-0.32662530E-02,-0.71527815E-03,-0.25651052E-02,
        -0.33470427E-02,-0.10582362E-02,-0.55852041E-04,-0.85207430E-04,
        -0.17262828E-03,-0.19057890E-03,-0.11820244E-02,-0.55929500E-04,
         0.66673630E-04,-0.72738464E-03, 0.25544755E-03, 0.66364888E-03,
        -0.24478580E-03,-0.15676527E-02, 0.51374693E-03, 0.11393951E-03,
         0.24378824E-02,-0.30667361E-03, 0.93503422E-05, 0.83720614E-03,
         0.14996226E-02,-0.27998004E-02,-0.21108253E-03,-0.70392439E-03,
         0.40263510E-02, 0.28826547E-03,-0.19950492E-02,-0.30351186E-02,
        -0.43491941E-03,-0.12192501E-02, 0.17602090E-02,-0.19175921E-02,
         0.79276436E-03, 0.77509036E-03, 0.51973824E-03, 0.17158076E-02,
        -0.16840373E-02, 0.44762722E-03,-0.25756359E-02, 0.11510699E-02,
        -0.24023030E-02, 0.35313142E-02,-0.95299439E-03,-0.13264952E-02,
         0.18426358E-02,-0.30791550E-02, 0.16130117E-02,-0.26625844E-02,
        -0.27023605E-03, 0.96872123E-03,-0.16564478E-02,-0.24377620E-02,
        -0.63462433E-03, 0.21699111E-02, 0.11399820E-02, 0.17796419E-03,
        -0.55538039E-04, 0.12470588E-03,-0.19953710E-03,-0.94136769E-04,
        -0.10311817E-03, 0.41454634E-04,-0.91600603E-04, 0.12805610E-03,
         0.10732456E-03,-0.92918359E-04, 0.11633681E-03,-0.14780647E-03,
        -0.12885488E-03, 0.30482007E-03,-0.12364100E-03, 0.37994705E-04,
        -0.19342681E-03, 0.54554240E-03, 0.56542736E-03, 0.10113912E-02,
         0.16079489E-03, 0.40911464E-03,-0.17580343E-03,-0.40689073E-03,
         0.53334457E-03,-0.12419675E-03, 0.24730578E-03,-0.54463925E-03,
         0.10854885E-03,-0.27065547E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x12    *x31    *x54
        +coeff[  6]                *x51
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]*x11    *x32        
        +coeff[ 12]*x13                
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]*x11*x21    *x41*x51
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]*x12        *x41*x51
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 17]    *x22*x31    *x52
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]*x11*x24            
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]*x11*x21*x33        
        +coeff[ 22]*x11*x22*x31*x41    
        +coeff[ 23]*x11*x21*x32    *x51
        +coeff[ 24]*x11*x22    *x41*x51
        +coeff[ 25]*x11*x21*x31*x41*x51
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 26]*x11        *x43*x51
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x11*x21*x31    *x52
        +coeff[ 29]*x13    *x32        
        +coeff[ 30]        *x34*x42    
        +coeff[ 31]        *x33*x43    
        +coeff[ 32]    *x21    *x43*x52
        +coeff[ 33]*x11*x22        *x53
        +coeff[ 34]*x12*x22*x32        
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 35]*x12*x22        *x52
        +coeff[ 36]*x12    *x32    *x52
        +coeff[ 37]*x12            *x54
        +coeff[ 38]    *x22*x31*x43*x51
        +coeff[ 39]    *x24*x31    *x52
        +coeff[ 40]    *x22*x32*x41*x52
        +coeff[ 41]    *x22*x31    *x54
        +coeff[ 42]*x11*x23*x33        
        +coeff[ 43]*x11*x24*x31*x41    
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 44]*x11*x24    *x41*x51
        +coeff[ 45]*x11*x21*x33*x41*x51
        +coeff[ 46]*x11*x23    *x42*x51
        +coeff[ 47]*x11*x22    *x41*x53
        +coeff[ 48]*x11*x21*x31*x41*x53
        +coeff[ 49]*x11*x21*x31    *x54
        +coeff[ 50]*x13*x21*x33        
        +coeff[ 51]        *x34*x44    
        +coeff[ 52]    *x24*x33    *x51
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 53]*x13*x22        *x52
        +coeff[ 54]*x13*x21*x31    *x52
        +coeff[ 55]    *x23*x32*x41*x52
        +coeff[ 56]    *x23*x31*x42*x52
        +coeff[ 57]    *x23    *x43*x52
        +coeff[ 58]    *x22    *x44*x52
        +coeff[ 59]    *x21            
        +coeff[ 60]        *x31        
        +coeff[ 61]            *x41    
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 62]                *x52
        +coeff[ 63]*x11            *x51
        +coeff[ 64]    *x21    *x42    
        +coeff[ 65]        *x31*x42    
        +coeff[ 66]            *x43    
        +coeff[ 67]        *x32    *x51
        +coeff[ 68]        *x31*x41*x51
        +coeff[ 69]    *x21        *x52
        +coeff[ 70]            *x41*x52
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 71]*x11    *x31*x41    
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x11        *x41*x51
        +coeff[ 74]*x11            *x52
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]        *x34        
        +coeff[ 78]    *x22*x31*x41    
        +coeff[ 79]        *x32*x42    
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 80]    *x22    *x41*x51
        +coeff[ 81]    *x22        *x52
        +coeff[ 82]    *x21*x31    *x52
        +coeff[ 83]        *x32    *x52
        +coeff[ 84]*x11*x22        *x51
        +coeff[ 85]*x11*x21        *x52
        +coeff[ 86]*x11            *x53
        +coeff[ 87]*x12    *x32        
        +coeff[ 88]*x12*x21    *x41    
    ;
    v_l_e_q1q2_2_350                              =v_l_e_q1q2_2_350                              
        +coeff[ 89]    *x22*x33        
        ;

    return v_l_e_q1q2_2_350                              ;
}
float x_e_q1q2_2_300                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.8146318E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.83718158E-03, 0.12289643E+00, 0.16756107E-02, 0.26232034E-02,
        -0.29092134E-03, 0.87029803E-04,-0.66408631E-03,-0.45420689E-03,
        -0.23355625E-03,-0.29070248E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_300                              =v_x_e_q1q2_2_300                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_2_300                              ;
}
float t_e_q1q2_2_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1724021E-04;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18304616E-04,-0.19740381E-02,-0.18306430E-02, 0.22723922E-02,
         0.73181880E-04,-0.16947385E-03,-0.30958847E-03,-0.31184976E-03,
         0.12424493E-04,-0.31145362E-03,-0.17776343E-03,-0.90637601E-04,
        -0.88594458E-03,-0.64568751E-03,-0.17079290E-04,-0.28763525E-04,
        -0.20608331E-04,-0.60410670E-03,-0.45235283E-03, 0.74069367E-05,
        -0.15810694E-04,-0.76319930E-05, 0.65526787E-04, 0.39906437E-04,
         0.16574078E-04,-0.13267795E-04, 0.13799936E-04,-0.28603824E-03,
         0.23535767E-05,-0.12716840E-05,-0.35649825E-05, 0.23620143E-04,
         0.67059495E-05, 0.48059919E-05, 0.53161675E-05,-0.28657894E-04,
        -0.20978592E-04, 0.99840900E-05, 0.12094128E-04,-0.45897323E-05,
         0.86507480E-05, 0.67323490E-05,-0.45324661E-04, 0.37051661E-04,
         0.55202588E-06,-0.30697687E-04, 0.30317418E-04,-0.15586571E-04,
        -0.11488988E-04, 0.10351615E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_300                              =v_t_e_q1q2_2_300                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_2_300                              =v_t_e_q1q2_2_300                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]    *x22            
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x21*x31*x41*x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]*x12*x23            
    ;
    v_t_e_q1q2_2_300                              =v_t_e_q1q2_2_300                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]    *x23*x32    *x51
        +coeff[ 29]        *x31*x41    
        +coeff[ 30]*x13                
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x11    *x31*x41*x51
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]*x11*x21        *x52
    ;
    v_t_e_q1q2_2_300                              =v_t_e_q1q2_2_300                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x12*x22        *x51
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x22        *x53
        +coeff[ 40]    *x21*x31    *x53
        +coeff[ 41]    *x21    *x41*x53
        +coeff[ 42]*x12*x22*x31*x41    
        +coeff[ 43]*x11*x23*x31*x41    
    ;
    v_t_e_q1q2_2_300                              =v_t_e_q1q2_2_300                              
        +coeff[ 44]*x11*x21*x33*x41    
        +coeff[ 45]*x12*x22    *x42    
        +coeff[ 46]*x11*x21*x32*x42    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]    *x23    *x43    
        +coeff[ 49]    *x21*x32*x43    
        ;

    return v_t_e_q1q2_2_300                              ;
}
float y_e_q1q2_2_300                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.5456901E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.53703168E-03, 0.13575839E+00, 0.99824138E-01,-0.17088705E-02,
         0.86059281E-05,-0.16944511E-02,-0.71039234E-04,-0.14838013E-04,
        -0.17539306E-03, 0.22953990E-03,-0.84184881E-04, 0.12136417E-03,
         0.10430258E-03, 0.74647214E-04, 0.76717755E-04,-0.27861193E-03,
        -0.14624903E-05, 0.46036032E-04, 0.35676963E-04,-0.36170448E-04,
         0.79704932E-07,-0.11973583E-04,-0.17510787E-03,-0.22637089E-03,
        -0.10065482E-04,-0.87163833E-04,-0.67436973E-04, 0.18272667E-04,
        -0.69260983E-04,-0.23091330E-04, 0.29835386E-04,-0.11714828E-04,
         0.17775161E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_300                              =v_y_e_q1q2_2_300                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_300                              =v_y_e_q1q2_2_300                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]*x11*x21    *x43    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_2_300                              =v_y_e_q1q2_2_300                              
        +coeff[ 26]*x11*x23    *x41    
        +coeff[ 27]    *x21    *x42*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x31*x43*x51
        +coeff[ 30]*x13*x21    *x41    
        +coeff[ 31]*x12        *x44    
        +coeff[ 32]    *x21    *x43*x52
        ;

    return v_y_e_q1q2_2_300                              ;
}
float p_e_q1q2_2_300                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2908159E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.28911803E-03, 0.35452396E-01, 0.66259407E-01,-0.15315037E-02,
        -0.15519734E-02,-0.33245725E-03,-0.35456390E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_300                              ;
}
float l_e_q1q2_2_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1865535E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17660949E-02,-0.17043316E-04,-0.29687739E-02,-0.53107861E-03,
        -0.30301595E-02,-0.18842335E-03,-0.51166127E-02,-0.72903378E-03,
        -0.85743045E-03,-0.76266646E-03,-0.11648224E-02,-0.56594022E-03,
        -0.32341920E-03,-0.12720150E-02, 0.33081870E-03,-0.18070544E-02,
        -0.82776305E-03, 0.49118004E-02, 0.56506535E-02, 0.59254153E-03,
        -0.13009885E-02, 0.38590987E-03, 0.18086127E-02,-0.65658861E-02,
        -0.11324550E-03,-0.88839320E-03, 0.29109599E-03, 0.80416350E-04,
         0.29255150E-03, 0.84421015E-04, 0.13996729E-02, 0.69607937E-04,
         0.11853103E-02, 0.74412848E-03,-0.14677898E-03,-0.33662404E-03,
         0.24462029E-03, 0.40944517E-03, 0.31098622E-03, 0.47817730E-03,
        -0.13221761E-02,-0.51008159E-03, 0.11612312E-02,-0.41531460E-03,
        -0.71162113E-03, 0.26539434E-03, 0.34498933E-03,-0.77469199E-03,
        -0.26416907E-02, 0.23518715E-02, 0.66371303E-03,-0.34549125E-03,
        -0.28347553E-03, 0.52495248E-03, 0.59842842E-03,-0.14390356E-02,
        -0.15790587E-02,-0.20110389E-03,-0.84189326E-03, 0.28419221E-03,
         0.56877232E-03,-0.63588511E-03, 0.13008348E-02, 0.10522422E-02,
        -0.13351546E-02, 0.15310918E-02,-0.10654739E-02, 0.67224534E-03,
         0.11407358E-02, 0.26864896E-03,-0.12165506E-02, 0.81307709E-03,
         0.26038671E-02,-0.19566081E-02, 0.94872736E-03,-0.17831151E-02,
         0.57040865E-03, 0.59650833E-03, 0.51887979E-03, 0.37501808E-03,
         0.11256974E-02,-0.10794712E-02, 0.18367638E-04, 0.67422559E-04,
        -0.90257905E-04,-0.15973391E-03, 0.96379423E-04, 0.90323068E-04,
         0.13955458E-03,-0.21694656E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]    *x21*x32*x41    
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[  8]        *x33*x41    
        +coeff[  9]    *x22    *x42    
        +coeff[ 10]        *x31*x43    
        +coeff[ 11]        *x31*x41*x52
        +coeff[ 12]            *x42*x52
        +coeff[ 13]*x12    *x31*x41    
        +coeff[ 14]*x12        *x42    
        +coeff[ 15]*x12*x21*x31*x41    
        +coeff[ 16]    *x24*x32        
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 17]    *x24*x31*x41    
        +coeff[ 18]    *x22*x33*x41    
        +coeff[ 19]        *x34*x42    
        +coeff[ 20]        *x32*x44    
        +coeff[ 21]*x13            *x52
        +coeff[ 22]*x12*x22*x31*x41    
        +coeff[ 23]    *x24*x33*x41    
        +coeff[ 24]*x11                
        +coeff[ 25]        *x31*x41    
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 26]*x11*x21            
        +coeff[ 27]*x11    *x31        
        +coeff[ 28]    *x23            
        +coeff[ 29]                *x53
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]*x12*x21            
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]        *x31    *x53
        +coeff[ 38]*x11    *x32*x41    
        +coeff[ 39]*x11    *x31*x42    
        +coeff[ 40]*x11        *x41*x52
        +coeff[ 41]*x12*x22            
        +coeff[ 42]*x12*x21        *x51
        +coeff[ 43]*x13*x21            
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 44]    *x24        *x51
        +coeff[ 45]    *x21*x33    *x51
        +coeff[ 46]*x11    *x34        
        +coeff[ 47]*x11    *x33*x41    
        +coeff[ 48]*x11    *x31*x43    
        +coeff[ 49]*x11*x21*x31*x41*x51
        +coeff[ 50]*x11*x21    *x42*x51
        +coeff[ 51]*x12*x21    *x42    
        +coeff[ 52]*x12*x22        *x51
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 53]*x12    *x31    *x52
        +coeff[ 54]    *x23*x32*x41    
        +coeff[ 55]    *x21*x33*x42    
        +coeff[ 56]*x13    *x31    *x51
        +coeff[ 57]    *x21*x34    *x51
        +coeff[ 58]*x13        *x41*x51
        +coeff[ 59]    *x24    *x41*x51
        +coeff[ 60]*x11*x23        *x52
        +coeff[ 61]*x11*x21    *x42*x52
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 62]*x11        *x41*x54
        +coeff[ 63]*x12*x22*x32        
        +coeff[ 64]*x12    *x32*x42    
        +coeff[ 65]*x12*x21*x31*x41*x51
        +coeff[ 66]*x12*x21        *x53
        +coeff[ 67]*x13    *x32*x41    
        +coeff[ 68]    *x23*x33*x41    
        +coeff[ 69]    *x24*x31    *x52
        +coeff[ 70]    *x22*x33    *x52
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 71]    *x24        *x53
        +coeff[ 72]*x11    *x33*x43    
        +coeff[ 73]*x11*x21*x31*x43*x51
        +coeff[ 74]*x11*x21    *x43*x52
        +coeff[ 75]*x12*x21    *x42*x52
        +coeff[ 76]*x12        *x43*x52
        +coeff[ 77]*x12    *x31*x41*x53
        +coeff[ 78]*x12*x21        *x54
        +coeff[ 79]*x13*x23*x31        
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 80]    *x23*x32*x42*x51
        +coeff[ 81]    *x21    *x43*x54
        +coeff[ 82]            *x41    
        +coeff[ 83]                *x51
        +coeff[ 84]    *x21        *x51
        +coeff[ 85]        *x31    *x51
        +coeff[ 86]                *x52
        +coeff[ 87]*x11        *x41    
        +coeff[ 88]    *x21*x32        
    ;
    v_l_e_q1q2_2_300                              =v_l_e_q1q2_2_300                              
        +coeff[ 89]    *x22    *x41    
        ;

    return v_l_e_q1q2_2_300                              ;
}
float x_e_q1q2_2_250                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1327793E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.13569294E-02, 0.12315185E+00, 0.16826888E-02, 0.26139563E-02,
        -0.37312711E-03, 0.84704603E-04,-0.59239852E-03,-0.43344763E-03,
        -0.17629053E-03,-0.32630088E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_250                              =v_x_e_q1q2_2_250                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_250                              ;
}
float t_e_q1q2_2_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2116159E-04;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.21711301E-04,-0.19712357E-02,-0.16482009E-02, 0.22675579E-02,
         0.70556358E-04,-0.17048545E-03,-0.31181416E-03,-0.31322436E-03,
         0.13552041E-04,-0.31155790E-03,-0.16049034E-04,-0.16652136E-03,
        -0.75393546E-04,-0.88180962E-03,-0.61770453E-03,-0.16481987E-04,
        -0.17901088E-04,-0.12528598E-04,-0.56913483E-03,-0.42158199E-03,
         0.78478688E-05,-0.11066250E-04,-0.98579576E-05, 0.53747353E-04,
        -0.83649524E-04,-0.26258061E-03,-0.62468920E-04, 0.35310190E-04,
         0.24201057E-04, 0.47802641E-04, 0.63833802E-04, 0.11109867E-05,
        -0.29006812E-05, 0.10496566E-04,-0.45499187E-05,-0.51017396E-05,
         0.76940451E-05, 0.20407548E-04,-0.21484893E-05,-0.35323565E-05,
         0.66766083E-05,-0.14923287E-04, 0.14370321E-04,-0.51967531E-05,
         0.53156773E-05, 0.17089633E-04, 0.11215547E-04,-0.16334487E-04,
        -0.93570770E-05,-0.15801885E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_2_250                              =v_t_e_q1q2_2_250                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_2_250                              =v_t_e_q1q2_2_250                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11*x22    *x41    
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_2_250                              =v_t_e_q1q2_2_250                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]    *x21*x31*x42*x51
        +coeff[ 28]*x12*x21*x31*x41*x51
        +coeff[ 29]    *x21*x32    *x53
        +coeff[ 30]    *x21*x31*x41*x53
        +coeff[ 31]                *x51
        +coeff[ 32]    *x21*x33        
        +coeff[ 33]    *x23        *x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_t_e_q1q2_2_250                              =v_t_e_q1q2_2_250                              
        +coeff[ 35]*x11    *x32    *x51
        +coeff[ 36]*x11*x21    *x41*x51
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]*x12            *x52
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]*x11            *x53
        +coeff[ 41]*x11*x22*x32        
        +coeff[ 42]*x11*x23    *x41    
        +coeff[ 43]*x11*x21    *x43    
    ;
    v_t_e_q1q2_2_250                              =v_t_e_q1q2_2_250                              
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x21*x32*x41*x51
        +coeff[ 46]*x11*x22        *x52
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]*x11*x21    *x41*x52
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q1q2_2_250                              ;
}
float y_e_q1q2_2_250                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.6995637E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.68664737E-03, 0.13556080E+00, 0.99699773E-01,-0.17005248E-02,
        -0.20166122E-04,-0.16735180E-02,-0.71252056E-04,-0.27112654E-04,
        -0.21280244E-03, 0.21773003E-03,-0.43989654E-04, 0.10648293E-03,
         0.10591318E-03, 0.66898101E-04, 0.70040376E-04,-0.27003253E-03,
        -0.35780504E-04,-0.10821364E-04,-0.22553719E-04,-0.25836047E-03,
        -0.10147268E-03,-0.10035668E-05,-0.43267055E-05, 0.12008288E-04,
        -0.88705820E-05, 0.66395914E-04, 0.37803915E-04, 0.33943888E-04,
        -0.19173323E-03,-0.25872490E-04,-0.18186180E-04,-0.63310225E-04,
        -0.32776257E-04,-0.22081758E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_250                              =v_y_e_q1q2_2_250                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_250                              =v_y_e_q1q2_2_250                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]        *x31*x43*x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11                
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11        *x42    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x11*x21    *x42    
    ;
    v_y_e_q1q2_2_250                              =v_y_e_q1q2_2_250                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x42*x52
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]*x11*x21    *x44    
        +coeff[ 32]*x11*x21    *x41*x52
        +coeff[ 33]    *x21*x31*x45    
        ;

    return v_y_e_q1q2_2_250                              ;
}
float p_e_q1q2_2_250                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2672856E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.26170837E-03, 0.35330746E-01, 0.66099688E-01,-0.15255122E-02,
        -0.15487333E-02,-0.33049230E-03,-0.35339949E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_250                              ;
}
float l_e_q1q2_2_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1835191E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16603150E-02,-0.32607431E-02,-0.52054430E-03,-0.32238099E-02,
         0.55596152E-04, 0.26372549E-03, 0.47502955E-03,-0.12134430E-03,
         0.20088565E-03, 0.84691041E-03,-0.68464171E-04, 0.11932697E-02,
         0.47561774E-03,-0.25746200E-03,-0.56988804E-03, 0.49335830E-03,
         0.32779448E-04,-0.18673763E-03,-0.17436832E-03,-0.10050586E-03,
         0.20501210E-03, 0.71178074E-04, 0.71791373E-03,-0.11429053E-02,
         0.77186426E-03, 0.44029104E-04,-0.30760230E-02, 0.80573116E-03,
        -0.13650116E-03, 0.99125107E-04,-0.20508106E-03,-0.90783382E-04,
        -0.33058904E-03, 0.22734090E-04,-0.14176879E-03, 0.44238003E-03,
        -0.56987425E-03, 0.34207161E-03, 0.84804073E-04,-0.70108392E-03,
         0.35586653E-03, 0.25986359E-03, 0.20102892E-03,-0.27701299E-03,
        -0.26639408E-03,-0.58234448E-03,-0.36381467E-03, 0.10737241E-02,
        -0.17563464E-02, 0.20657284E-02,-0.10523708E-03,-0.36018901E-03,
         0.74262155E-03, 0.31432675E-02,-0.26141587E-02, 0.46109656E-03,
        -0.72627835E-03,-0.29779496E-03,-0.59099856E-03, 0.48819452E-03,
         0.11330904E-02, 0.45059313E-03, 0.35314520E-02, 0.24422137E-02,
        -0.87726233E-03, 0.84465614E-03, 0.57989580E-03,-0.21953676E-02,
         0.15330756E-02,-0.13592464E-02, 0.28127837E-04,-0.11024841E-02,
         0.51570986E-03,-0.71701896E-03, 0.90854586E-03,-0.31217521E-02,
         0.70861320E-03, 0.87617303E-03, 0.79191884E-03, 0.11028679E-02,
        -0.27659521E-02, 0.41911416E-02,-0.50186944E-04, 0.60246501E-04,
        -0.82532315E-04,-0.17952145E-03, 0.72165763E-04,-0.39040002E-04,
        -0.14153395E-03, 0.11517332E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]*x11        *x41    
        +coeff[  5]        *x32    *x51
        +coeff[  6]            *x42*x51
        +coeff[  7]*x11*x21    *x41    
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[  8]    *x22*x31*x41    
        +coeff[  9]        *x33*x41    
        +coeff[ 10]    *x22    *x42    
        +coeff[ 11]        *x31*x43    
        +coeff[ 12]        *x31*x42*x51
        +coeff[ 13]        *x31*x41*x52
        +coeff[ 14]                *x54
        +coeff[ 15]*x11*x21        *x52
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 17]*x12        *x42    
        +coeff[ 18]    *x22        *x53
        +coeff[ 19]    *x24*x32        
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]        *x34*x42    
        +coeff[ 22]        *x32*x44    
        +coeff[ 23]        *x32    *x54
        +coeff[ 24]*x12    *x31*x41*x52
        +coeff[ 25]            *x41    
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 26]        *x31*x41    
        +coeff[ 27]                *x52
        +coeff[ 28]*x11*x21            
        +coeff[ 29]*x11    *x31        
        +coeff[ 30]    *x21*x31    *x51
        +coeff[ 31]    *x21        *x52
        +coeff[ 32]*x11    *x32        
        +coeff[ 33]*x12        *x41    
        +coeff[ 34]*x12            *x51
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 35]            *x44    
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]*x11    *x32*x41    
        +coeff[ 39]*x11        *x43    
        +coeff[ 40]*x11            *x53
        +coeff[ 41]*x12*x22            
        +coeff[ 42]*x12*x21        *x51
        +coeff[ 43]*x12            *x52
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 44]    *x21    *x43*x51
        +coeff[ 45]*x11    *x33*x41    
        +coeff[ 46]*x11    *x33    *x51
        +coeff[ 47]*x11*x22    *x41*x51
        +coeff[ 48]*x11    *x32*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        +coeff[ 50]*x11*x21    *x41*x52
        +coeff[ 51]*x12        *x41*x52
        +coeff[ 52]        *x34    *x52
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 53]    *x21*x32*x41*x52
        +coeff[ 54]    *x22    *x42*x52
        +coeff[ 55]*x11*x24*x31        
        +coeff[ 56]*x11    *x31*x44    
        +coeff[ 57]*x11*x24        *x51
        +coeff[ 58]*x11*x21*x31*x41*x52
        +coeff[ 59]*x12*x22*x32        
        +coeff[ 60]*x12    *x32*x42    
        +coeff[ 61]*x13    *x32    *x51
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 62]    *x21*x32*x43*x51
        +coeff[ 63]    *x21*x31*x44*x51
        +coeff[ 64]    *x22*x33    *x52
        +coeff[ 65]    *x22*x31*x41*x53
        +coeff[ 66]*x11*x24*x31    *x51
        +coeff[ 67]*x11*x23*x32    *x51
        +coeff[ 68]*x11    *x34*x41*x51
        +coeff[ 69]*x11*x21*x32*x41*x52
        +coeff[ 70]*x11*x23        *x53
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 71]*x12*x21*x32*x41*x51
        +coeff[ 72]*x12    *x31    *x54
        +coeff[ 73]    *x23*x34*x41    
        +coeff[ 74]*x13*x21*x32    *x51
        +coeff[ 75]*x13*x21    *x42*x51
        +coeff[ 76]*x13    *x31*x41*x52
        +coeff[ 77]*x13*x21        *x53
        +coeff[ 78]        *x34*x41*x53
        +coeff[ 79]    *x22*x31*x42*x53
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 80]    *x21*x32*x41*x54
        +coeff[ 81]    *x22    *x42*x54
        +coeff[ 82]                *x51
        +coeff[ 83]*x11                
        +coeff[ 84]    *x21    *x41    
        +coeff[ 85]*x11            *x51
        +coeff[ 86]    *x22*x31        
        +coeff[ 87]        *x33        
        +coeff[ 88]    *x22    *x41    
    ;
    v_l_e_q1q2_2_250                              =v_l_e_q1q2_2_250                              
        +coeff[ 89]            *x43    
        ;

    return v_l_e_q1q2_2_250                              ;
}
float x_e_q1q2_2_200                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1311175E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.16545353E-03, 0.12346920E+00, 0.16948910E-02, 0.26014750E-02,
        -0.37747607E-03, 0.85671032E-04,-0.58602868E-03,-0.42648651E-03,
        -0.16336964E-03,-0.32867480E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_200                              =v_x_e_q1q2_2_200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_200                              ;
}
float t_e_q1q2_2_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1409574E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13766560E-04,-0.19627924E-02,-0.13604703E-02, 0.22682885E-02,
        -0.30945305E-03,-0.31927772E-03,-0.30499793E-03, 0.71127470E-04,
        -0.17856118E-03,-0.16166471E-03,-0.77470817E-04,-0.88014657E-03,
        -0.60879323E-03,-0.24549727E-04,-0.31370637E-04,-0.17890146E-04,
         0.57422854E-04,-0.59142208E-03,-0.44281140E-03,-0.12848414E-04,
         0.13444360E-04, 0.26143774E-04, 0.29803088E-04, 0.66588304E-05,
        -0.27087508E-03,-0.10731043E-04, 0.16445993E-04,-0.18530736E-04,
        -0.16340018E-04,-0.43672194E-05, 0.16478656E-05,-0.70906754E-05,
         0.67536207E-05,-0.32432069E-05, 0.88882862E-05, 0.70422329E-05,
        -0.11918803E-04, 0.10027458E-04, 0.67231525E-06,-0.11568481E-04,
        -0.19236521E-04, 0.13280913E-04,-0.20452674E-05, 0.27591682E-05,
         0.10469061E-04,-0.67083856E-05, 0.10931033E-04,-0.43706741E-05,
        -0.14798213E-04,-0.99363842E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_2_200                              =v_t_e_q1q2_2_200                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_2_200                              =v_t_e_q1q2_2_200                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x31    *x52
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x11*x21    *x41*x52
    ;
    v_t_e_q1q2_2_200                              =v_t_e_q1q2_2_200                              
        +coeff[ 26]    *x22        *x53
        +coeff[ 27]*x12*x23        *x51
        +coeff[ 28]*x11*x22*x31    *x52
        +coeff[ 29]    *x22            
        +coeff[ 30]                *x52
        +coeff[ 31]    *x22        *x51
        +coeff[ 32]*x12*x22            
        +coeff[ 33]*x12    *x32        
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_q1q2_2_200                              =v_t_e_q1q2_2_200                              
        +coeff[ 35]*x13*x22            
        +coeff[ 36]*x13*x21    *x41    
        +coeff[ 37]*x12*x21*x31*x41    
        +coeff[ 38]*x11*x21*x32*x41    
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x11*x21*x31*x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]    *x23*x31    *x51
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_t_e_q1q2_2_200                              =v_t_e_q1q2_2_200                              
        +coeff[ 44]    *x21*x32*x41*x51
        +coeff[ 45]*x12*x21        *x52
        +coeff[ 46]*x11*x21*x31    *x52
        +coeff[ 47]    *x22*x31    *x52
        +coeff[ 48]    *x21*x32    *x52
        +coeff[ 49]*x11*x23*x31*x41    
        ;

    return v_t_e_q1q2_2_200                              ;
}
float y_e_q1q2_2_200                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.3399333E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.13111821E-03, 0.13539092E+00, 0.99473640E-01,-0.16896597E-02,
        -0.18950108E-04,-0.16676767E-02,-0.67778157E-04,-0.19044497E-04,
        -0.19513827E-03, 0.23020840E-03,-0.66054461E-04, 0.11185170E-03,
         0.94182862E-04, 0.71525137E-04, 0.78418612E-04,-0.27521575E-03,
        -0.59854156E-04,-0.25968889E-05,-0.22727683E-03,-0.90740643E-04,
         0.56571396E-04,-0.98961678E-07,-0.81604867E-05, 0.41959000E-04,
         0.34899651E-04, 0.33675948E-04,-0.13856014E-03, 0.20700343E-04,
        -0.45747598E-04,-0.17512239E-04,-0.32601311E-04,-0.13775346E-04,
        -0.23798184E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_200                              =v_y_e_q1q2_2_200                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x23*x31        
    ;
    v_y_e_q1q2_2_200                              =v_y_e_q1q2_2_200                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]*x12        *x45*x51
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]        *x32*x43    
    ;
    v_y_e_q1q2_2_200                              =v_y_e_q1q2_2_200                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]*x11    *x31*x42*x51
        +coeff[ 30]        *x31*x42*x52
        +coeff[ 31]*x11*x21    *x44    
        +coeff[ 32]*x11*x22*x32*x41    
        ;

    return v_y_e_q1q2_2_200                              ;
}
float p_e_q1q2_2_200                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.4498984E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.29246063E-07, 0.35135705E-01, 0.65927744E-01,-0.15153618E-02,
        -0.15379599E-02,-0.32662295E-03,-0.35378744E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_200                              ;
}
float l_e_q1q2_2_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1890857E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18763366E-02, 0.41239342E-04,-0.34675980E-02,-0.59655105E-03,
        -0.32174098E-02,-0.63016708E-03,-0.12028108E-02,-0.48042691E-03,
         0.30688566E-03,-0.94105781E-03, 0.12717613E-03,-0.63809782E-03,
        -0.13934549E-03, 0.82100177E-03, 0.37454526E-03,-0.13752833E-02,
         0.88362215E-03,-0.87371608E-03,-0.13660267E-02,-0.19914424E-03,
        -0.87954933E-04,-0.98336561E-04, 0.35829964E-03,-0.10910033E-02,
         0.78384340E-03, 0.36581513E-03,-0.59201790E-03,-0.21530395E-03,
        -0.19633016E-03, 0.31162554E-03, 0.96276414E-03, 0.16601036E-02,
         0.46474315E-03,-0.11917512E-02, 0.38810927E-03,-0.12249320E-02,
         0.39594516E-03,-0.12199924E-02, 0.25757258E-02,-0.22629427E-03,
         0.79989090E-03,-0.53253531E-03,-0.39831991E-03,-0.35243898E-03,
        -0.71122125E-03, 0.87864313E-03, 0.61576400E-03,-0.22851741E-03,
        -0.91710512E-03,-0.89373847E-03, 0.18622603E-02, 0.42103857E-04,
         0.36639557E-03, 0.31532564E-02,-0.62319363E-04, 0.22108045E-02,
        -0.89367025E-03,-0.85256918E-03,-0.88754954E-03,-0.10739666E-02,
        -0.11816844E-02,-0.21535133E-02,-0.10763779E-02, 0.15986623E-02,
         0.14859218E-02,-0.12690326E-02,-0.38801658E-03, 0.17164702E-02,
         0.11395572E-02, 0.17407194E-02,-0.15795208E-03, 0.13052032E-03,
        -0.43617826E-03, 0.40833947E-04, 0.12967479E-03,-0.33203312E-03,
         0.16449101E-03, 0.64427346E-04,-0.12979482E-03, 0.25241578E-03,
        -0.10276057E-03, 0.10582765E-03, 0.66547509E-04, 0.39821467E-03,
         0.33135529E-03,-0.33995832E-03,-0.14423947E-03, 0.24479884E-03,
         0.13212727E-03, 0.13317443E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]*x12        *x41    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]*x11*x21*x31    *x52
        +coeff[ 12]*x12*x22        *x51
        +coeff[ 13]    *x24*x31*x41    
        +coeff[ 14]    *x22*x33*x41    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]*x11*x22*x32*x41    
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 17]        *x32*x44*x52
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]*x12                
        +coeff[ 21]    *x21*x31*x41    
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]        *x31*x41*x52
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]*x11    *x33        
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 26]*x11    *x31*x42    
        +coeff[ 27]*x11    *x31*x41*x51
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]*x13    *x31        
        +coeff[ 30]    *x22*x31    *x52
        +coeff[ 31]    *x21*x32    *x52
        +coeff[ 32]*x11    *x32*x41*x51
        +coeff[ 33]*x11*x21    *x42*x51
        +coeff[ 34]*x11        *x43*x51
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 35]*x11        *x42*x52
        +coeff[ 36]*x12*x22    *x41    
        +coeff[ 37]*x12        *x42*x51
        +coeff[ 38]*x12        *x41*x52
        +coeff[ 39]    *x23*x33        
        +coeff[ 40]*x13        *x41*x51
        +coeff[ 41]    *x21*x33    *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]        *x32    *x54
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 44]*x11*x21*x34        
        +coeff[ 45]*x11    *x31*x43*x51
        +coeff[ 46]*x13*x21*x32        
        +coeff[ 47]    *x24*x33        
        +coeff[ 48]    *x23*x32    *x52
        +coeff[ 49]    *x22*x33    *x52
        +coeff[ 50]    *x23*x31*x41*x52
        +coeff[ 51]    *x21*x32    *x54
        +coeff[ 52]*x11*x24*x31*x41    
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 53]*x11*x22*x32*x42    
        +coeff[ 54]*x11    *x34*x42    
        +coeff[ 55]*x11    *x33*x41*x52
        +coeff[ 56]*x11        *x43*x53
        +coeff[ 57]*x11    *x31*x41*x54
        +coeff[ 58]*x12*x24        *x51
        +coeff[ 59]*x12*x21*x33    *x51
        +coeff[ 60]*x12*x22*x31*x41*x51
        +coeff[ 61]*x12        *x41*x54
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 62]*x13*x22    *x42    
        +coeff[ 63]    *x24*x32*x41*x51
        +coeff[ 64]*x13*x21    *x42*x51
        +coeff[ 65]*x13        *x43*x51
        +coeff[ 66]*x13    *x31*x41*x52
        +coeff[ 67]*x13        *x42*x52
        +coeff[ 68]    *x22*x33    *x53
        +coeff[ 69]        *x31*x43*x54
        +coeff[ 70]    *x21            
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 71]    *x21*x31        
        +coeff[ 72]    *x21        *x51
        +coeff[ 73]*x11            *x51
        +coeff[ 74]    *x23            
        +coeff[ 75]    *x21*x32        
        +coeff[ 76]    *x21    *x42    
        +coeff[ 77]    *x21*x31    *x51
        +coeff[ 78]    *x21    *x41*x51
        +coeff[ 79]            *x42*x51
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 80]            *x41*x52
        +coeff[ 81]*x12*x21            
        +coeff[ 82]*x12    *x31        
        +coeff[ 83]*x12            *x51
        +coeff[ 84]        *x34        
        +coeff[ 85]        *x33    *x51
        +coeff[ 86]    *x21    *x42*x51
        +coeff[ 87]        *x31*x42*x51
        +coeff[ 88]    *x22        *x52
    ;
    v_l_e_q1q2_2_200                              =v_l_e_q1q2_2_200                              
        +coeff[ 89]        *x31    *x53
        ;

    return v_l_e_q1q2_2_200                              ;
}
float x_e_q1q2_2_175                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.7022903E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.72649255E-03, 0.12369502E+00, 0.17022467E-02, 0.25934840E-02,
        -0.36937551E-03, 0.86100707E-04,-0.58331178E-03,-0.42131968E-03,
        -0.17417442E-03,-0.32229000E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_175                              =v_x_e_q1q2_2_175                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_175                              ;
}
float t_e_q1q2_2_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5242146E-05;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.45572515E-05,-0.19557110E-02,-0.11536629E-02, 0.22636596E-02,
        -0.32555213E-03,-0.33245859E-03,-0.28018313E-03, 0.71297989E-04,
        -0.18502919E-03,-0.17928379E-03,-0.86200351E-04,-0.84700884E-03,
        -0.58204180E-03,-0.25833860E-04,-0.31750591E-04,-0.21302791E-04,
        -0.51751465E-03,-0.39820370E-03,-0.10168601E-04, 0.10722897E-05,
         0.68867239E-05,-0.15090284E-04, 0.53516223E-05, 0.46212957E-04,
         0.17988972E-04, 0.14758625E-04,-0.24477937E-03,-0.23412138E-04,
         0.29435918E-04, 0.26982058E-04, 0.24432820E-04, 0.45656994E-04,
         0.39035338E-04,-0.20617264E-04, 0.25168604E-05,-0.30921922E-06,
         0.28090890E-05, 0.42392080E-05,-0.43095597E-05, 0.22208874E-05,
        -0.34606155E-05, 0.26754797E-05,-0.44482208E-05, 0.42703437E-05,
         0.55083169E-05, 0.55767882E-05, 0.58548571E-05, 0.63023936E-05,
         0.51705892E-05,-0.48878269E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_2_175                              =v_t_e_q1q2_2_175                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q1q2_2_175                              =v_t_e_q1q2_2_175                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11    *x32        
        +coeff[ 19]*x11            *x52
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x12*x21        *x51
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]*x13*x22            
    ;
    v_t_e_q1q2_2_175                              =v_t_e_q1q2_2_175                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x12*x23        *x51
        +coeff[ 29]*x11*x23*x31    *x51
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]*x11*x21*x32*x41*x51
        +coeff[ 32]*x12*x21    *x42*x51
        +coeff[ 33]*x12*x22        *x52
        +coeff[ 34]*x11    *x31        
    ;
    v_t_e_q1q2_2_175                              =v_t_e_q1q2_2_175                              
        +coeff[ 35]            *x42    
        +coeff[ 36]    *x22*x31        
        +coeff[ 37]    *x22        *x51
        +coeff[ 38]*x11    *x31    *x51
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]            *x42*x51
        +coeff[ 41]            *x41*x52
        +coeff[ 42]    *x22*x31*x41    
        +coeff[ 43]*x11    *x32*x41    
    ;
    v_t_e_q1q2_2_175                              =v_t_e_q1q2_2_175                              
        +coeff[ 44]    *x21*x32*x41    
        +coeff[ 45]    *x22    *x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]*x11    *x31*x41*x51
        +coeff[ 48]*x12            *x52
        +coeff[ 49]        *x32    *x52
        ;

    return v_t_e_q1q2_2_175                              ;
}
float y_e_q1q2_2_175                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.5140042E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.43154624E-03, 0.13520633E+00, 0.99328652E-01,-0.16862674E-02,
        -0.85700804E-05,-0.16489511E-02,-0.72328825E-04,-0.15923437E-04,
        -0.20751829E-03, 0.22499338E-03,-0.48732043E-04, 0.12162268E-03,
         0.64117179E-04, 0.60131893E-04, 0.74437929E-04,-0.26449576E-03,
        -0.16020547E-04, 0.40111754E-04,-0.15860688E-03,-0.48268965E-04,
         0.50106123E-05,-0.94672414E-05,-0.15570497E-04, 0.22256432E-04,
         0.15795877E-04, 0.75135472E-05,-0.25284028E-03, 0.47966645E-04,
        -0.97015451E-04,-0.36685873E-04,-0.36279551E-04, 0.26268630E-04,
         0.27593351E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_175                              =v_y_e_q1q2_2_175                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_175                              =v_y_e_q1q2_2_175                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]*x11*x21    *x43    
        +coeff[ 20]        *x33        
        +coeff[ 21]    *x21    *x43    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]*x11        *x44    
    ;
    v_y_e_q1q2_2_175                              =v_y_e_q1q2_2_175                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]        *x34*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]        *x31*x42*x52
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x22    *x41*x51
        +coeff[ 32]*x11*x21    *x43*x51
        ;

    return v_y_e_q1q2_2_175                              ;
}
float p_e_q1q2_2_175                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1855351E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.14561020E-03, 0.35000909E-01, 0.65770075E-01,-0.15086685E-02,
        -0.15279544E-02,-0.32037508E-03,-0.34628910E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_175                              ;
}
float l_e_q1q2_2_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1839218E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18605571E-02,-0.34655312E-02,-0.40107247E-03,-0.39582360E-02,
        -0.21988917E-03,-0.28786957E-03, 0.81574847E-03, 0.85903256E-03,
         0.12349529E-02,-0.27605591E-02,-0.12435814E-03, 0.12312131E-03,
        -0.39648722E-03, 0.24917128E-02,-0.11190475E-02, 0.99244469E-03,
         0.38821870E-03, 0.13473904E-03, 0.82232284E-04,-0.30018687E-02,
         0.16836237E-03, 0.13779999E-03, 0.10119494E-03, 0.39059715E-03,
        -0.14307578E-03, 0.26186726E-02, 0.60008228E-03,-0.52485822E-04,
         0.61620056E-03, 0.34264970E-03,-0.21763225E-03,-0.13413694E-03,
         0.21681099E-03, 0.68613175E-04,-0.30813580E-02, 0.68735256E-03,
        -0.62665780E-03, 0.32728378E-03, 0.28525770E-03,-0.66490966E-03,
         0.57943666E-03, 0.57044905E-03,-0.13737872E-03,-0.63276693E-03,
         0.14002861E-02,-0.10115220E-02,-0.45089150E-03, 0.30022583E-03,
        -0.40222367E-03,-0.19952536E-02,-0.22765859E-03,-0.37688069E-03,
         0.89820515E-03, 0.11355471E-02, 0.16684534E-02, 0.61109738E-03,
        -0.11882747E-02, 0.77545253E-03, 0.82136877E-03, 0.26120685E-03,
         0.17756775E-02,-0.86431188E-03,-0.75382850E-03, 0.48200681E-03,
         0.50469738E-03,-0.45976730E-03, 0.11475092E-02, 0.58181636E-03,
         0.19139583E-02,-0.10397186E-02,-0.47350660E-03, 0.27528296E-02,
        -0.88541076E-03, 0.18590662E-02, 0.12886757E-02,-0.23149694E-02,
        -0.11631304E-02, 0.74839365E-03, 0.87837141E-03,-0.65226963E-03,
         0.15801690E-02,-0.96542278E-03, 0.13802784E-02,-0.54870284E-03,
        -0.36453395E-03, 0.11704847E-03, 0.75671758E-03,-0.38857834E-04,
        -0.19732574E-02,-0.23368555E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]        *x32    *x51
        +coeff[  5]        *x34        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]*x11*x21*x31    *x51
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]*x12*x22        *x51
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]    *x22*x31*x43    
        +coeff[ 16]*x12    *x33*x41    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 17]        *x31        
        +coeff[ 18]            *x41    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]    *x21        *x51
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]    *x21    *x42    
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 26]            *x44    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x41*x52
        +coeff[ 30]*x11*x22*x31        
        +coeff[ 31]*x11        *x42*x51
        +coeff[ 32]*x12*x21*x31        
        +coeff[ 33]*x13        *x41    
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 35]*x11*x22    *x41*x51
        +coeff[ 36]*x11*x21    *x42*x51
        +coeff[ 37]*x11*x21        *x53
        +coeff[ 38]*x12    *x32*x41    
        +coeff[ 39]*x12        *x43    
        +coeff[ 40]*x12*x21    *x41*x51
        +coeff[ 41]*x12        *x41*x52
        +coeff[ 42]*x13    *x31*x41    
        +coeff[ 43]    *x23*x32*x41    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]    *x21*x34    *x51
        +coeff[ 46]*x13        *x41*x51
        +coeff[ 47]        *x33*x42*x51
        +coeff[ 48]    *x23*x31    *x52
        +coeff[ 49]        *x32*x42*x52
        +coeff[ 50]            *x44*x52
        +coeff[ 51]    *x21*x32    *x53
        +coeff[ 52]            *x42*x54
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 53]*x11*x23*x31    *x51
        +coeff[ 54]*x11*x21*x33    *x51
        +coeff[ 55]*x11*x22*x31*x41*x51
        +coeff[ 56]*x11*x23        *x52
        +coeff[ 57]*x11*x21    *x42*x52
        +coeff[ 58]*x11        *x42*x53
        +coeff[ 59]*x11        *x41*x54
        +coeff[ 60]*x12*x21*x32    *x51
        +coeff[ 61]*x12    *x31*x41*x52
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 62]*x12*x21        *x53
        +coeff[ 63]*x13*x23            
        +coeff[ 64]    *x23*x34        
        +coeff[ 65]*x13        *x43    
        +coeff[ 66]*x13*x21*x31    *x51
        +coeff[ 67]    *x23*x31*x42*x51
        +coeff[ 68]    *x21*x34    *x52
        +coeff[ 69]    *x21*x31*x43*x52
        +coeff[ 70]    *x22    *x41*x54
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 71]*x11*x21*x34*x41    
        +coeff[ 72]*x11*x24    *x42    
        +coeff[ 73]*x11*x24*x31    *x51
        +coeff[ 74]*x11*x22*x32*x41*x51
        +coeff[ 75]*x11*x22*x31*x42*x51
        +coeff[ 76]*x11*x24        *x52
        +coeff[ 77]*x11    *x33*x41*x52
        +coeff[ 78]*x11*x22        *x54
        +coeff[ 79]*x12    *x31*x44    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 80]*x12*x22    *x42*x51
        +coeff[ 81]*x12*x23        *x52
        +coeff[ 82]*x12*x21*x32    *x52
        +coeff[ 83]*x12        *x42*x53
        +coeff[ 84]*x13*x23*x31        
        +coeff[ 85]*x13*x22*x32        
        +coeff[ 86]*x13*x21*x33        
        +coeff[ 87]*x13*x23    *x41    
        +coeff[ 88]*x13*x22*x31*x41    
    ;
    v_l_e_q1q2_2_175                              =v_l_e_q1q2_2_175                              
        +coeff[ 89]*x13*x21*x32*x41    
        ;

    return v_l_e_q1q2_2_175                              ;
}
float x_e_q1q2_2_150                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3126696E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.30196740E-03, 0.12400992E+00, 0.17118441E-02, 0.25859363E-02,
        -0.36980835E-03, 0.87808374E-04,-0.57817675E-03,-0.41732166E-03,
        -0.16244978E-03,-0.32212006E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_150                              =v_x_e_q1q2_2_150                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_150                              ;
}
float t_e_q1q2_2_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1110346E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.11548344E-04,-0.19480295E-02,-0.88611734E-03, 0.22444557E-02,
        -0.30826792E-03,-0.31848595E-03,-0.27667009E-03, 0.70433409E-04,
        -0.18226540E-03,-0.17389197E-03,-0.79318226E-04,-0.84211840E-03,
        -0.59653062E-03,-0.17279024E-04,-0.18512605E-04,-0.63589164E-05,
         0.68287663E-04, 0.47377773E-04,-0.55733725E-03,-0.42967999E-03,
        -0.91548181E-05, 0.64701831E-05,-0.12249986E-04, 0.21194883E-04,
        -0.65341294E-04,-0.26209519E-03,-0.36496494E-04,-0.25557960E-04,
        -0.22387736E-04, 0.34501575E-05, 0.13859164E-05,-0.34554165E-06,
        -0.35064475E-05,-0.16601888E-05, 0.57899565E-05,-0.52929036E-05,
        -0.62981258E-07, 0.58683986E-05, 0.79129150E-05,-0.52278706E-05,
         0.61411838E-05, 0.77462710E-05,-0.45929173E-05,-0.53889876E-05,
         0.66319112E-05, 0.37776754E-05,-0.15277183E-04, 0.92755624E-06,
        -0.12701092E-04, 0.43761761E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_2_150                              =v_t_e_q1q2_2_150                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_2_150                              =v_t_e_q1q2_2_150                              
        +coeff[ 17]    *x21    *x42*x51
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]*x11*x21    *x42    
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_2_150                              =v_t_e_q1q2_2_150                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]*x12*x22*x31    *x51
        +coeff[ 28]*x12*x22    *x41*x51
        +coeff[ 29]*x11*x21            
        +coeff[ 30]        *x32        
        +coeff[ 31]    *x21    *x41    
        +coeff[ 32]            *x41*x51
        +coeff[ 33]*x12        *x41    
        +coeff[ 34]*x13*x21            
    ;
    v_t_e_q1q2_2_150                              =v_t_e_q1q2_2_150                              
        +coeff[ 35]*x11*x23            
        +coeff[ 36]*x12*x21*x31        
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x23    *x41    
        +coeff[ 39]    *x22    *x42    
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x12        *x41*x51
        +coeff[ 42]*x11*x21        *x52
        +coeff[ 43]        *x31*x41*x52
    ;
    v_t_e_q1q2_2_150                              =v_t_e_q1q2_2_150                              
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x13*x21*x31        
        +coeff[ 46]*x11*x22*x32        
        +coeff[ 47]*x13    *x31*x41    
        +coeff[ 48]*x13        *x42    
        +coeff[ 49]*x11*x23        *x51
        ;

    return v_t_e_q1q2_2_150                              ;
}
float y_e_q1q2_2_150                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.3083792E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.94833995E-04, 0.13501765E+00, 0.99141635E-01,-0.16770771E-02,
        -0.72237335E-05,-0.16548694E-02,-0.66463799E-04,-0.58928752E-04,
        -0.21309532E-03, 0.22669969E-03,-0.24042969E-04, 0.12317144E-03,
         0.10602723E-03, 0.63929474E-04, 0.70446382E-04,-0.26603971E-03,
        -0.37283222E-04,-0.20966607E-04,-0.26877562E-03,-0.93523078E-04,
        -0.55296055E-05,-0.38772650E-04,-0.15972617E-04, 0.13742950E-04,
         0.82623328E-05, 0.21319755E-04, 0.29860063E-04, 0.21047186E-04,
        -0.12325099E-04, 0.38170565E-04,-0.21428695E-03, 0.35194880E-04,
         0.39477964E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_150                              =v_y_e_q1q2_2_150                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_150                              =v_y_e_q1q2_2_150                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]        *x32*x42    
        +coeff[ 24]*x11        *x43    
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_y_e_q1q2_2_150                              =v_y_e_q1q2_2_150                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]*x13*x21    *x41    
        +coeff[ 32]    *x22*x32*x41*x51
        ;

    return v_y_e_q1q2_2_150                              ;
}
float p_e_q1q2_2_150                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2589734E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.56222918E-04, 0.34824740E-01, 0.65593950E-01,-0.15026534E-02,
        -0.15240904E-02,-0.32144753E-03,-0.34370404E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_150                              ;
}
float l_e_q1q2_2_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1813485E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19274571E-02, 0.10391256E-03,-0.35851914E-02,-0.11459051E-02,
        -0.35995482E-02,-0.21472457E-03,-0.14783470E-03,-0.78526762E-04,
         0.13863232E-02,-0.58303162E-03, 0.17380167E-03, 0.12175054E-02,
         0.33549734E-02, 0.17032074E-02,-0.11554428E-03, 0.11189011E-02,
         0.19553418E-02,-0.21327313E-02,-0.29919494E-02, 0.11277484E-03,
         0.10524928E-03,-0.16109445E-03, 0.34698410E-03,-0.29013611E-03,
         0.45886401E-04,-0.57697657E-03, 0.30886917E-03, 0.55248593E-03,
         0.51962218E-03, 0.39001324E-03,-0.29193066E-03, 0.28593480E-03,
         0.14694748E-03, 0.87067572E-03,-0.11914900E-02,-0.72571245E-03,
        -0.66476356E-03, 0.44513241E-03,-0.24323445E-02,-0.72865677E-03,
        -0.97967067E-03,-0.12072426E-03, 0.49157860E-03,-0.43205067E-03,
        -0.46497112E-03,-0.36381785E-03,-0.46057432E-03,-0.80880424E-03,
         0.19341881E-02, 0.15438506E-02,-0.32443585E-03, 0.31631844E-03,
        -0.47547903E-03, 0.14241518E-02, 0.45657664E-03, 0.18010361E-02,
         0.79740159E-03,-0.11076342E-02,-0.66375651E-03,-0.35851309E-03,
        -0.94616367E-03,-0.19026479E-02, 0.86957635E-03, 0.17194555E-02,
         0.73551381E-03, 0.23791101E-02, 0.10307515E-02,-0.15745027E-02,
         0.12421262E-02,-0.17881453E-02,-0.64009923E-03,-0.25201796E-03,
         0.15855060E-02,-0.14161766E-02, 0.82521472E-03,-0.66774973E-03,
        -0.34954180E-02,-0.20684120E-02,-0.59963064E-03,-0.24055426E-02,
        -0.22108729E-02, 0.27168877E-02, 0.12098879E-02, 0.40479848E-04,
         0.48882859E-04, 0.38635852E-04, 0.54690776E-04,-0.11121328E-03,
        -0.56837907E-04, 0.63328545E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22        *x51
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]    *x24*x31*x41    
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]        *x33*x41*x52
        +coeff[ 16]*x11    *x33*x41*x52
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 17]    *x24*x33*x41    
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]            *x41*x51
        +coeff[ 20]                *x52
        +coeff[ 21]    *x22    *x41    
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]        *x34        
        +coeff[ 29]            *x44    
        +coeff[ 30]        *x32*x41*x51
        +coeff[ 31]*x13    *x31        
        +coeff[ 32]    *x24*x31        
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]        *x32*x43    
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x32    *x51
        +coeff[ 37]    *x21*x33    *x51
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]    *x21    *x42*x52
        +coeff[ 40]        *x31*x42*x52
        +coeff[ 41]            *x43*x52
        +coeff[ 42]        *x32    *x53
        +coeff[ 43]    *x21    *x41*x53
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 44]    *x21        *x54
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x11*x22    *x41*x51
        +coeff[ 47]*x11*x21*x31*x41*x51
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x12*x21    *x42    
        +coeff[ 50]*x12*x21*x31    *x51
        +coeff[ 51]*x12        *x42*x51
        +coeff[ 52]*x12        *x41*x52
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 53]        *x32*x44    
        +coeff[ 54]*x13*x21        *x51
        +coeff[ 55]    *x24    *x41*x51
        +coeff[ 56]*x11*x21*x32    *x52
        +coeff[ 57]*x12*x22*x31*x41    
        +coeff[ 58]*x12*x21    *x41*x52
        +coeff[ 59]*x13*x21*x32        
        +coeff[ 60]    *x23*x31*x43    
        +coeff[ 61]    *x23    *x44    
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 62]    *x22*x33    *x52
        +coeff[ 63]    *x21    *x44*x52
        +coeff[ 64]    *x23        *x54
        +coeff[ 65]    *x21*x31*x41*x54
        +coeff[ 66]*x11*x22*x34        
        +coeff[ 67]*x11*x22*x33    *x51
        +coeff[ 68]*x11*x22*x31    *x53
        +coeff[ 69]*x11    *x31*x41*x54
        +coeff[ 70]*x12*x22*x33        
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 71]*x12    *x34    *x51
        +coeff[ 72]*x12*x22    *x42*x51
        +coeff[ 73]*x12    *x32*x42*x51
        +coeff[ 74]*x12*x21    *x43*x51
        +coeff[ 75]*x12*x23        *x52
        +coeff[ 76]*x12*x21*x31*x41*x52
        +coeff[ 77]*x12*x21    *x42*x52
        +coeff[ 78]*x13*x22*x32        
        +coeff[ 79]    *x24*x32*x41*x51
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 80]    *x24    *x43*x51
        +coeff[ 81]    *x22*x32*x43*x51
        +coeff[ 82]*x13    *x31*x41*x52
        +coeff[ 83]        *x31        
        +coeff[ 84]            *x41    
        +coeff[ 85]*x11                
        +coeff[ 86]    *x21        *x51
        +coeff[ 87]*x11    *x31        
        +coeff[ 88]*x12                
    ;
    v_l_e_q1q2_2_150                              =v_l_e_q1q2_2_150                              
        +coeff[ 89]    *x23            
        ;

    return v_l_e_q1q2_2_150                              ;
}
float x_e_q1q2_2_125                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3124053E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.32341803E-03, 0.12443934E+00, 0.17270031E-02, 0.25682736E-02,
        -0.36814797E-03, 0.86121239E-04,-0.56797219E-03,-0.41344500E-03,
        -0.16458600E-03,-0.32165088E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_125                              =v_x_e_q1q2_2_125                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_2_125                              ;
}
float t_e_q1q2_2_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1159437E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.11953951E-04,-0.19347282E-02,-0.50880539E-03, 0.22438220E-02,
        -0.30115756E-03,-0.31864012E-03,-0.27400022E-03, 0.68545378E-04,
        -0.17976860E-03,-0.17330548E-03,-0.79863821E-04,-0.82589296E-03,
        -0.58287533E-03,-0.23124569E-04,-0.28620432E-04,-0.20188309E-04,
         0.34281649E-04,-0.54005459E-05,-0.58651814E-03,-0.44429931E-03,
         0.43090768E-04, 0.34173102E-05,-0.11642508E-04,-0.45372758E-05,
         0.18691147E-04,-0.28058415E-03, 0.16898299E-04,-0.71627251E-05,
        -0.22708935E-04, 0.12309381E-04,-0.14426379E-04,-0.24110450E-04,
         0.36137699E-04, 0.38109913E-04, 0.74250437E-06,-0.19634174E-05,
         0.15759333E-05,-0.38595253E-05,-0.24025671E-05, 0.28423010E-05,
         0.22691388E-05, 0.66472007E-05,-0.19860101E-05, 0.58625155E-05,
        -0.30374813E-05, 0.53705585E-05,-0.27281305E-05,-0.85334268E-05,
         0.53148656E-05, 0.10143253E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_2_125                              =v_t_e_q1q2_2_125                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_2_125                              =v_t_e_q1q2_2_125                              
        +coeff[ 17]*x13    *x32        
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]*x13                
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_2_125                              =v_t_e_q1q2_2_125                              
        +coeff[ 26]*x12*x21    *x42    
        +coeff[ 27]*x11*x23        *x51
        +coeff[ 28]*x12*x21*x31    *x51
        +coeff[ 29]*x11*x22    *x41*x51
        +coeff[ 30]*x13*x21*x31    *x51
        +coeff[ 31]    *x22*x33    *x51
        +coeff[ 32]*x12*x21    *x42*x51
        +coeff[ 33]    *x21*x31*x43*x51
        +coeff[ 34]                *x51
    ;
    v_t_e_q1q2_2_125                              =v_t_e_q1q2_2_125                              
        +coeff[ 35]    *x22            
        +coeff[ 36]        *x32        
        +coeff[ 37]    *x22*x31        
        +coeff[ 38]*x11*x21    *x41    
        +coeff[ 39]        *x31*x42    
        +coeff[ 40]*x11    *x31    *x51
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]    *x21*x33        
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q1q2_2_125                              =v_t_e_q1q2_2_125                              
        +coeff[ 44]    *x23    *x41    
        +coeff[ 45]*x11*x21*x31*x41    
        +coeff[ 46]*x12        *x42    
        +coeff[ 47]*x12*x21        *x51
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]    *x22*x31    *x51
        ;

    return v_t_e_q1q2_2_125                              ;
}
float y_e_q1q2_2_125                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2837573E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.29897070E-03, 0.13478534E+00, 0.98824874E-01,-0.16625468E-02,
        -0.86010432E-05,-0.16310496E-02, 0.20336470E-03,-0.11841853E-03,
        -0.15431631E-03, 0.12408137E-03,-0.22341298E-03, 0.84722131E-04,
        -0.21965448E-04, 0.70823742E-04, 0.72141935E-04,-0.91468297E-04,
        -0.25297375E-04, 0.17148783E-05,-0.97505072E-05,-0.17424056E-04,
         0.37016660E-04,-0.78967962E-04,-0.16046835E-04,-0.54391545E-04,
        -0.33456759E-04,-0.16677897E-04, 0.47178586E-04,-0.19468138E-04,
         0.16455726E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31*x42    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q1q2_2_125                              =v_y_e_q1q2_2_125                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22    *x41    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q1q2_2_125                              =v_y_e_q1q2_2_125                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]*x12        *x42*x51
    ;
    v_y_e_q1q2_2_125                              =v_y_e_q1q2_2_125                              
        +coeff[ 26]    *x22    *x43*x51
        +coeff[ 27]    *x23    *x42*x51
        +coeff[ 28]    *x21    *x43*x52
        ;

    return v_y_e_q1q2_2_125                              ;
}
float p_e_q1q2_2_125                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1488805E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.15426376E-03, 0.34557361E-01, 0.65357603E-01,-0.14909308E-02,
        -0.15114871E-02,-0.31270954E-03,-0.33732079E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_125                              ;
}
float l_e_q1q2_2_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1846751E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17642821E-02,-0.79885045E-04,-0.35393850E-02,-0.25857627E-03,
        -0.21090168E-02,-0.30231106E-02, 0.73098141E-03,-0.62831864E-03,
         0.85870345E-03, 0.19638229E-03,-0.17729467E-03, 0.21242001E-03,
        -0.30130634E-03,-0.25097246E-03, 0.30087578E-03, 0.23753531E-03,
         0.43851012E-03,-0.96503191E-03,-0.44768769E-03,-0.47943622E-03,
        -0.19064207E-03,-0.14700183E-03, 0.36958992E-03, 0.18825206E-03,
        -0.46546574E-03,-0.33230233E-03,-0.32852869E-03,-0.40061085E-03,
        -0.54195421E-04, 0.24375666E-03,-0.68195362E-03, 0.30732010E-02,
         0.12778383E-02, 0.11871062E-02,-0.74331177E-03, 0.26219929E-03,
         0.29087477E-03, 0.40512392E-03,-0.18325022E-03,-0.69161603E-03,
        -0.35458515E-03,-0.36471261E-03,-0.73203468E-03,-0.43074595E-03,
        -0.80413965E-03,-0.27617265E-03, 0.34795891E-03, 0.77170046E-03,
         0.36712500E-03, 0.18055611E-02, 0.16237014E-02, 0.48128024E-03,
        -0.72872522E-03,-0.79417916E-03,-0.13917203E-02,-0.14372844E-02,
         0.11965184E-02, 0.32696797E-03, 0.60888188E-03,-0.79656980E-03,
        -0.17764976E-02,-0.13046487E-02, 0.10310602E-02,-0.85438142E-03,
        -0.18989226E-02, 0.13102740E-02,-0.24247325E-02, 0.14735383E-02,
        -0.43403837E-03,-0.22310447E-02,-0.34745582E-02,-0.87558338E-03,
         0.49506972E-03, 0.49354957E-03,-0.56319888E-03, 0.70551457E-03,
        -0.46142252E-03, 0.15574163E-02, 0.57859847E-03,-0.57689619E-03,
         0.79426565E-03,-0.54716633E-03,-0.44618672E-03, 0.12547216E-03,
        -0.99200508E-04, 0.72227260E-04, 0.77914556E-04,-0.80637619E-04,
         0.16774374E-03, 0.87403809E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x12            *x53
        +coeff[  7]*x11*x21*x34        
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[  8]        *x31*x43*x53
        +coeff[  9]            *x41    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]*x12                
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]*x11    *x32        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 17]*x11*x21        *x51
        +coeff[ 18]        *x32    *x52
        +coeff[ 19]        *x31*x41*x52
        +coeff[ 20]*x11    *x33        
        +coeff[ 21]*x11*x22    *x41    
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]*x11    *x31    *x52
        +coeff[ 24]*x12    *x32        
        +coeff[ 25]*x12    *x31*x41    
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 26]*x12    *x31    *x51
        +coeff[ 27]*x13        *x41    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x13            *x51
        +coeff[ 30]    *x21*x33    *x51
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11    *x32*x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11*x22*x31    *x51
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]*x11    *x31*x42*x51
        +coeff[ 37]*x11        *x43*x51
        +coeff[ 38]*x12    *x33        
        +coeff[ 39]*x12*x22        *x51
        +coeff[ 40]*x12    *x31*x41*x51
        +coeff[ 41]*x12        *x41*x52
        +coeff[ 42]    *x23*x33        
        +coeff[ 43]    *x21*x34*x41    
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 44]        *x34*x41*x51
        +coeff[ 45]    *x24        *x52
        +coeff[ 46]    *x23        *x53
        +coeff[ 47]    *x22        *x54
        +coeff[ 48]*x11    *x34*x41    
        +coeff[ 49]*x11*x23*x31    *x51
        +coeff[ 50]*x11*x23    *x41*x51
        +coeff[ 51]*x11*x22*x31*x41*x51
        +coeff[ 52]*x11*x21*x31*x42*x51
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 53]*x11*x22        *x53
        +coeff[ 54]*x11*x21    *x41*x53
        +coeff[ 55]*x12*x21*x31*x41*x51
        +coeff[ 56]*x12    *x32*x41*x51
        +coeff[ 57]*x13*x21    *x42    
        +coeff[ 58]*x13*x22        *x51
        +coeff[ 59]*x13*x21*x31    *x51
        +coeff[ 60]    *x24*x31*x41*x51
        +coeff[ 61]    *x22*x32*x42*x51
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 62]    *x23    *x43*x51
        +coeff[ 63]    *x21*x32*x43*x51
        +coeff[ 64]    *x21    *x44*x52
        +coeff[ 65]    *x21    *x42*x54
        +coeff[ 66]*x11*x24*x31*x41    
        +coeff[ 67]*x11    *x33*x42*x51
        +coeff[ 68]*x11    *x34    *x52
        +coeff[ 69]*x11    *x33*x41*x52
        +coeff[ 70]*x11    *x32*x42*x52
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 71]*x11    *x31*x41*x54
        +coeff[ 72]*x12    *x34*x41    
        +coeff[ 73]*x12*x22*x31    *x52
        +coeff[ 74]*x12    *x32    *x53
        +coeff[ 75]*x13*x22    *x42    
        +coeff[ 76]    *x23*x34    *x51
        +coeff[ 77]    *x23*x33*x41*x51
        +coeff[ 78]*x13*x21        *x53
        +coeff[ 79]*x13    *x31    *x53
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 80]    *x21*x34    *x53
        +coeff[ 81]    *x24    *x41*x53
        +coeff[ 82]            *x44*x54
        +coeff[ 83]    *x21*x31        
        +coeff[ 84]    *x21        *x51
        +coeff[ 85]        *x31    *x51
        +coeff[ 86]            *x41*x51
        +coeff[ 87]                *x52
        +coeff[ 88]*x11        *x41    
    ;
    v_l_e_q1q2_2_125                              =v_l_e_q1q2_2_125                              
        +coeff[ 89]    *x22*x31        
        ;

    return v_l_e_q1q2_2_125                              ;
}
float x_e_q1q2_2_100                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.9197873E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.95353200E-03, 0.12505305E+00, 0.17475701E-02, 0.25446056E-02,
        -0.29651911E-03, 0.84150895E-04,-0.62519353E-03,-0.43650417E-03,
        -0.23951552E-03,-0.27059563E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_2_100                              =v_x_e_q1q2_2_100                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_2_100                              ;
}
float t_e_q1q2_2_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3332354E-06;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13701887E-06,-0.19201903E-02, 0.57483841E-04, 0.22190828E-02,
        -0.17525935E-03,-0.30039749E-03,-0.30936880E-03,-0.28440225E-03,
         0.71309194E-04,-0.16273603E-03,-0.81958693E-04,-0.82326122E-03,
        -0.57993247E-03,-0.72965986E-05,-0.51732326E-03,-0.39527158E-03,
        -0.64699580E-05,-0.24637007E-04,-0.19016177E-04, 0.65625696E-04,
         0.43729397E-04,-0.88820734E-05,-0.13359189E-05,-0.14280639E-04,
        -0.25059684E-03,-0.15688143E-04, 0.34746139E-04, 0.37652098E-04,
        -0.14616937E-04,-0.18267079E-04, 0.10268509E-04, 0.14863092E-05,
         0.48754177E-05,-0.10043836E-04, 0.32499490E-05,-0.26521184E-05,
         0.22866975E-05,-0.22282641E-05,-0.13759348E-04,-0.45784841E-05,
        -0.62295535E-05,-0.61299097E-05,-0.34397550E-06,-0.60724610E-05,
         0.48428292E-05,-0.14618544E-04, 0.37299658E-05, 0.57895377E-05,
        -0.68038648E-05,-0.12188582E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_2_100                              =v_t_e_q1q2_2_100                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32*x42    
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]    *x22*x31        
    ;
    v_t_e_q1q2_2_100                              =v_t_e_q1q2_2_100                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x22*x32        
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x22*x31*x41*x51
    ;
    v_t_e_q1q2_2_100                              =v_t_e_q1q2_2_100                              
        +coeff[ 26]*x12*x21*x32    *x51
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]    *x22*x33    *x51
        +coeff[ 29]*x11*x22    *x41*x52
        +coeff[ 30]*x11*x21            
        +coeff[ 31]                *x52
        +coeff[ 32]*x13                
        +coeff[ 33]*x11    *x32        
        +coeff[ 34]*x12        *x41    
    ;
    v_t_e_q1q2_2_100                              =v_t_e_q1q2_2_100                              
        +coeff[ 35]        *x32*x41    
        +coeff[ 36]*x11*x21        *x51
        +coeff[ 37]    *x22        *x51
        +coeff[ 38]*x11*x23            
        +coeff[ 39]*x11*x22*x31        
        +coeff[ 40]*x13            *x51
        +coeff[ 41]*x12*x21        *x51
        +coeff[ 42]*x12        *x41*x51
        +coeff[ 43]    *x22    *x41*x51
    ;
    v_t_e_q1q2_2_100                              =v_t_e_q1q2_2_100                              
        +coeff[ 44]*x11            *x53
        +coeff[ 45]*x13*x22            
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]*x12*x22*x31        
        +coeff[ 48]*x12*x21*x32        
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q1q2_2_100                              ;
}
float y_e_q1q2_2_100                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1113159E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.10248827E-02, 0.13435028E+00, 0.98429389E-01,-0.16445564E-02,
        -0.92417622E-05,-0.16182460E-02,-0.76666736E-04,-0.33955468E-04,
        -0.17017765E-03, 0.22257994E-03,-0.63932966E-04, 0.14729968E-03,
         0.96731834E-04, 0.83275372E-04, 0.66603716E-04,-0.24310005E-03,
        -0.13733147E-04,-0.31436310E-04, 0.82461602E-05,-0.27304091E-04,
         0.34184013E-04, 0.33958182E-04, 0.75711605E-05,-0.15845311E-03,
        -0.14915304E-04,-0.21005237E-03,-0.53013377E-04, 0.14024386E-04,
        -0.81782200E-04,-0.43270848E-04,-0.34319488E-04, 0.25229790E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_2_100                              =v_y_e_q1q2_2_100                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_2_100                              =v_y_e_q1q2_2_100                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x44*x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x21    *x43*x51
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_2_100                              =v_y_e_q1q2_2_100                              
        +coeff[ 26]*x11*x21*x31*x42    
        +coeff[ 27]*x11    *x32*x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]            *x43*x52
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11    *x31*x43*x51
        ;

    return v_y_e_q1q2_2_100                              ;
}
float p_e_q1q2_2_100                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.4878176E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.44641562E-03, 0.34185581E-01, 0.64966805E-01,-0.14737154E-02,
        -0.14935881E-02,-0.30338592E-03,-0.32820358E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_2_100                              ;
}
float l_e_q1q2_2_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1852373E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18309825E-02,-0.34579565E-02,-0.18340172E-03,-0.49937441E-03,
        -0.26237818E-02,-0.16814327E-02,-0.14022298E-02,-0.32269183E-03,
        -0.10629316E-02, 0.11321023E-02,-0.39300282E-03, 0.26240977E-03,
         0.42381291E-02,-0.17392954E-02,-0.75062294E-03,-0.12873363E-02,
         0.53721354E-02,-0.20110451E-02,-0.18104808E-02, 0.25057219E-03,
        -0.13363443E-03, 0.55816246E-03, 0.76564706E-04,-0.10491012E-02,
        -0.46377886E-05, 0.41394893E-03,-0.20062942E-02,-0.18162042E-04,
        -0.45739536E-03, 0.20811494E-03,-0.78679144E-03,-0.32229966E-03,
         0.23026428E-03,-0.16617698E-02, 0.25337259E-02,-0.47170755E-03,
         0.27059398E-02, 0.14497888E-02, 0.20967473E-02,-0.53951418E-03,
         0.42107695E-03,-0.15003745E-03, 0.17893178E-02, 0.24613030E-02,
        -0.72894682E-03,-0.10512177E-02, 0.99692994E-03, 0.64533914E-03,
        -0.60864812E-03, 0.49181172E-03, 0.93310408E-03, 0.66025637E-03,
        -0.14892737E-02,-0.56097901E-03, 0.35912354E-03, 0.72480412E-03,
        -0.90715790E-03,-0.20896303E-03,-0.14342524E-02,-0.44056983E-03,
         0.21929091E-02, 0.71705802E-03, 0.26968839E-02,-0.20003398E-02,
        -0.35851763E-03,-0.22892260E-02,-0.30028492E-02,-0.52239385E-03,
        -0.50134322E-03,-0.26980208E-02,-0.19281929E-02, 0.64959965E-03,
        -0.10534686E-02, 0.70407573E-03,-0.12959368E-02, 0.84802601E-03,
         0.10983817E-02, 0.56106779E-02,-0.49236580E-03, 0.13453447E-02,
        -0.21563258E-02, 0.38488624E-02,-0.15409344E-02, 0.16116244E-02,
        -0.14936538E-03,-0.74476608E-04,-0.17316232E-03,-0.19529603E-03,
         0.16302211E-03,-0.76273660E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[  8]            *x44    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]    *x24*x31*x41    
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]*x12    *x32*x42    
        +coeff[ 16]    *x22*x34*x42    
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 17]        *x31*x43*x54
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]*x11*x21            
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]            *x41*x52
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]*x12            *x51
        +coeff[ 25]*x13                
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x21*x32        
        +coeff[ 29]*x11    *x33        
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]    *x22        *x53
        +coeff[ 32]*x11*x24            
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x11*x21*x32*x41    
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 35]*x11*x21*x31*x42    
        +coeff[ 36]*x11    *x31*x42*x51
        +coeff[ 37]*x11*x22        *x52
        +coeff[ 38]*x11*x21*x31    *x52
        +coeff[ 39]*x11        *x42*x52
        +coeff[ 40]*x12        *x42*x51
        +coeff[ 41]    *x23*x32*x41    
        +coeff[ 42]    *x23*x31*x41*x51
        +coeff[ 43]    *x21*x33*x41*x51
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 44]    *x22    *x43*x51
        +coeff[ 45]    *x21*x31*x42*x52
        +coeff[ 46]            *x44*x52
        +coeff[ 47]        *x33    *x53
        +coeff[ 48]            *x42*x54
        +coeff[ 49]*x11    *x33    *x52
        +coeff[ 50]*x12*x21    *x41*x52
        +coeff[ 51]*x12        *x42*x52
        +coeff[ 52]    *x21*x34*x42    
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 53]    *x23*x31*x43    
        +coeff[ 54]*x13*x21*x31    *x51
        +coeff[ 55]    *x24*x32    *x51
        +coeff[ 56]    *x23*x33    *x51
        +coeff[ 57]    *x23*x31*x42*x51
        +coeff[ 58]    *x23*x31*x41*x52
        +coeff[ 59]        *x34    *x53
        +coeff[ 60]    *x21*x31*x42*x53
        +coeff[ 61]    *x21    *x43*x53
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 62]*x11*x23*x33        
        +coeff[ 63]*x11*x21*x34*x41    
        +coeff[ 64]*x11*x22    *x44    
        +coeff[ 65]*x11    *x31*x44*x51
        +coeff[ 66]*x11*x21*x33    *x52
        +coeff[ 67]*x11    *x33*x41*x52
        +coeff[ 68]*x11*x22*x31    *x53
        +coeff[ 69]*x11    *x31*x42*x53
        +coeff[ 70]*x11*x22        *x54
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 71]*x12*x21*x34        
        +coeff[ 72]*x12*x21    *x42*x52
        +coeff[ 73]*x12    *x32    *x53
        +coeff[ 74]*x13*x24            
        +coeff[ 75]*x13*x23    *x41    
        +coeff[ 76]*x13*x22    *x42    
        +coeff[ 77]    *x23*x32*x42*x51
        +coeff[ 78]*x13        *x43*x51
        +coeff[ 79]        *x34*x43*x51
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 80]    *x23*x32*x41*x52
        +coeff[ 81]    *x21*x31*x43*x53
        +coeff[ 82]        *x32*x43*x53
        +coeff[ 83]    *x21    *x44*x53
        +coeff[ 84]*x11                
        +coeff[ 85]    *x21    *x41    
        +coeff[ 86]    *x21        *x51
        +coeff[ 87]        *x31    *x51
        +coeff[ 88]            *x41*x51
    ;
    v_l_e_q1q2_2_100                              =v_l_e_q1q2_2_100                              
        +coeff[ 89]                *x52
        ;

    return v_l_e_q1q2_2_100                              ;
}
