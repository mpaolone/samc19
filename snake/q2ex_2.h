float x_e_q2ex_2_1200                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.1533553E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.15375256E-02,-0.46809996E-02, 0.19540760E+00, 0.95509766E-02,
         0.43055948E-03,-0.15196976E-02,-0.38073196E-02,-0.28002076E-02,
        -0.18833778E-04, 0.15531001E-05, 0.17640028E-04,-0.18090701E-02,
        -0.48774359E-03,-0.23771042E-02,-0.21471546E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_1200                              =v_x_e_q2ex_2_1200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_1200                              ;
}
float t_e_q2ex_2_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4382137E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.43948618E-03,-0.25092289E-02, 0.54286122E-01, 0.24938076E-02,
        -0.12707086E-02,-0.13323091E-02,-0.51666505E-03, 0.17933652E-03,
        -0.48112273E-03,-0.51862298E-03,-0.19799625E-03,-0.16645339E-02,
        -0.12824064E-02,-0.11096406E-02,-0.42237716E-04, 0.19694086E-03,
         0.13865676E-03,-0.56996167E-03,-0.12823865E-02, 0.79418887E-05,
        -0.28281122E-04,-0.19486857E-04,-0.15359445E-04, 0.26306578E-04,
        -0.14946324E-04, 0.67783185E-05, 0.45616322E-04, 0.67045497E-04,
         0.16496839E-04,-0.72825414E-05,-0.11391353E-03,-0.97148259E-04,
         0.51277343E-05,-0.71861650E-04, 0.13619753E-04, 0.20929072E-04,
         0.25737761E-04,-0.29585248E-04,-0.11708259E-04, 0.69743528E-05,
         0.31624802E-05, 0.35352384E-05, 0.29527073E-05,-0.35722569E-05,
         0.11726292E-04, 0.91867842E-05, 0.54344587E-05, 0.89285868E-05,
        -0.27308100E-04,-0.13502208E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_1200                              =v_t_e_q2ex_2_1200                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_1200                              =v_t_e_q2ex_2_1200                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x11            *x52
        +coeff[ 23]    *x21        *x53
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_t_e_q2ex_2_1200                              =v_t_e_q2ex_2_1200                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]    *x23*x32*x41    
        +coeff[ 33]    *x22*x32*x42    
        +coeff[ 34]    *x23    *x43    
    ;
    v_t_e_q2ex_2_1200                              =v_t_e_q2ex_2_1200                              
        +coeff[ 35]*x12*x22*x31    *x51
        +coeff[ 36]*x13        *x42*x51
        +coeff[ 37]*x11*x22    *x42*x51
        +coeff[ 38]*x11*x21            
        +coeff[ 39]        *x31*x41    
        +coeff[ 40]*x12*x21            
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]    *x22*x31        
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_t_e_q2ex_2_1200                              =v_t_e_q2ex_2_1200                              
        +coeff[ 44]*x13*x21            
        +coeff[ 45]*x11*x21*x32        
        +coeff[ 46]    *x22*x32        
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]    *x22*x31*x41    
        +coeff[ 49]        *x31*x43    
        ;

    return v_t_e_q2ex_2_1200                              ;
}
float y_e_q2ex_2_1200                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.2128459E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.11401013E-03, 0.20475125E+00, 0.10971928E+00, 0.40659320E-03,
        -0.10125339E-02,-0.10555901E-02,-0.61884150E-03,-0.72380528E-03,
        -0.47940947E-03, 0.13158492E-03,-0.11246760E-03,-0.54772582E-03,
        -0.36760921E-04,-0.58751470E-04, 0.38093811E-04,-0.52798970E-03,
        -0.28272063E-04, 0.65039232E-04,-0.49711874E-03, 0.56621058E-04,
         0.14347586E-04,-0.70611626E-03,-0.58550405E-03, 0.29187699E-04,
        -0.17657501E-03,-0.10499202E-03,-0.81878796E-04,-0.29275156E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_1200                              =v_y_e_q2ex_2_1200                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_1200                              =v_y_e_q2ex_2_1200                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]    *x21*x32*x41    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]*x11*x21*x31*x42    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23    *x41    
    ;
    v_y_e_q2ex_2_1200                              =v_y_e_q2ex_2_1200                              
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]    *x22    *x45    
        ;

    return v_y_e_q2ex_2_1200                              ;
}
float p_e_q2ex_2_1200                              (float *x,int m){
    int ncoeff= 13;
    float avdat=  0.2771079E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.67945830E-05,-0.18530436E-01,-0.19723566E-01, 0.17271549E-02,
         0.29670338E-02,-0.60032937E-03,-0.34313535E-03,-0.57173305E-03,
        -0.33878782E-03,-0.14906075E-03, 0.57631828E-05,-0.15313538E-03,
        -0.32060518E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_1200                              =v_p_e_q2ex_2_1200                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]        *x33    *x52
        +coeff[ 12]        *x32*x41    
        ;

    return v_p_e_q2ex_2_1200                              ;
}
float l_e_q2ex_2_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2975403E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29691411E-02,-0.48946142E-02,-0.13549740E-02,-0.48170551E-02,
        -0.54123364E-02, 0.21713412E-03,-0.87717699E-03, 0.16378455E-04,
         0.79812649E-04,-0.24127946E-03,-0.50713914E-03,-0.28559181E-03,
        -0.11963872E-03,-0.42616372E-03,-0.18481517E-03,-0.12888745E-02,
         0.56107841E-04, 0.30766131E-03,-0.37866391E-03,-0.34419657E-03,
         0.68338471E-03, 0.42531488E-03,-0.44628856E-03, 0.54796576E-03,
         0.39741495E-02,-0.70948538E-03,-0.32672051E-02, 0.10050129E-02,
         0.36178727E-03,-0.18141337E-02, 0.14931331E-02, 0.42980741E-03,
        -0.71329140E-03, 0.95363118E-03, 0.22104404E-03, 0.53564017E-03,
        -0.79676823E-03,-0.13422067E-02,-0.58398972E-03, 0.15832746E-02,
        -0.14383484E-02, 0.57175232E-03,-0.28607575E-02, 0.13041281E-02,
         0.13255215E-02, 0.90525515E-03,-0.10500979E-02, 0.82671177E-03,
        -0.25755172E-02, 0.22664275E-02,-0.16479684E-02,-0.15525285E-02,
         0.36197997E-03, 0.55127550E-03, 0.10695430E-02,-0.53732231E-03,
        -0.46978897E-04,-0.51667259E-04,-0.72271527E-04, 0.25842250E-04,
         0.22268693E-03,-0.27460049E-03, 0.27456557E-03,-0.56506391E-03,
        -0.14898280E-03, 0.12599815E-03, 0.35681346E-03, 0.10201723E-03,
        -0.13960911E-03,-0.39495283E-03,-0.10939855E-03, 0.11129122E-03,
        -0.21700878E-03, 0.34520516E-03, 0.13039133E-03,-0.84737447E-04,
        -0.67651545E-03, 0.35618927E-03,-0.26259292E-03,-0.67272026E-03,
        -0.15978161E-05, 0.42987749E-03, 0.14194906E-02,-0.50135184E-03,
         0.15436079E-03, 0.23538218E-03,-0.10521826E-03,-0.24898539E-03,
        -0.25354707E-03,-0.13322842E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]*x12    *x32*x41*x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[  8]*x11            *x51
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]*x11*x21        *x51
        +coeff[ 12]        *x33*x41    
        +coeff[ 13]            *x44    
        +coeff[ 14]*x11*x21*x32        
        +coeff[ 15]*x11    *x32*x41    
        +coeff[ 16]*x11        *x43    
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 17]*x11*x22        *x51
        +coeff[ 18]*x11    *x31*x41*x51
        +coeff[ 19]*x11*x21        *x52
        +coeff[ 20]*x12    *x31*x41    
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]            *x43*x52
        +coeff[ 23]*x11    *x34        
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x13    *x32        
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 26]        *x31*x44*x51
        +coeff[ 27]*x11*x23*x32        
        +coeff[ 28]*x11*x24    *x41    
        +coeff[ 29]*x11*x21*x31*x42*x51
        +coeff[ 30]*x11    *x32*x41*x52
        +coeff[ 31]*x11*x21*x31    *x53
        +coeff[ 32]*x11*x21    *x41*x53
        +coeff[ 33]*x11    *x31*x41*x53
        +coeff[ 34]*x12*x24            
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 35]*x12*x22        *x52
        +coeff[ 36]    *x21*x34*x42    
        +coeff[ 37]    *x23*x31*x42*x51
        +coeff[ 38]        *x33*x42*x52
        +coeff[ 39]    *x22*x31    *x54
        +coeff[ 40]        *x33    *x54
        +coeff[ 41]    *x22    *x41*x54
        +coeff[ 42]*x11*x24*x31*x41    
        +coeff[ 43]*x11    *x34*x42    
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 44]*x11    *x33*x41*x52
        +coeff[ 45]*x12*x23*x31    *x51
        +coeff[ 46]*x12*x22*x32    *x51
        +coeff[ 47]*x12*x21    *x41*x53
        +coeff[ 48]*x13*x22*x31*x41    
        +coeff[ 49]    *x23*x34    *x51
        +coeff[ 50]    *x21*x34*x42*x51
        +coeff[ 51]        *x34*x43*x51
        +coeff[ 52]*x13    *x32    *x52
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 53]    *x24*x31*x41*x52
        +coeff[ 54]*x13    *x31    *x53
        +coeff[ 55]            *x44*x54
        +coeff[ 56]                *x51
        +coeff[ 57]    *x21        *x51
        +coeff[ 58]        *x31    *x51
        +coeff[ 59]            *x41*x51
        +coeff[ 60]*x11        *x41    
        +coeff[ 61]    *x22*x31        
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 62]        *x33        
        +coeff[ 63]    *x21*x31*x41    
        +coeff[ 64]    *x21    *x42    
        +coeff[ 65]        *x32    *x51
        +coeff[ 66]        *x31    *x52
        +coeff[ 67]                *x53
        +coeff[ 68]*x11*x22            
        +coeff[ 69]*x11    *x31    *x51
        +coeff[ 70]*x11        *x41*x51
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 71]*x11            *x52
        +coeff[ 72]    *x24            
        +coeff[ 73]    *x23*x31        
        +coeff[ 74]    *x23    *x41    
        +coeff[ 75]    *x21*x32*x41    
        +coeff[ 76]    *x21*x31*x42    
        +coeff[ 77]        *x32*x42    
        +coeff[ 78]    *x21    *x43    
        +coeff[ 79]    *x21*x32    *x51
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 80]    *x21*x31*x41*x51
        +coeff[ 81]    *x21    *x42*x51
        +coeff[ 82]        *x31*x42*x51
        +coeff[ 83]            *x43*x51
        +coeff[ 84]        *x32    *x52
        +coeff[ 85]            *x41*x53
        +coeff[ 86]*x11    *x33        
        +coeff[ 87]*x11        *x42*x51
        +coeff[ 88]*x11        *x41*x52
    ;
    v_l_e_q2ex_2_1200                              =v_l_e_q2ex_2_1200                              
        +coeff[ 89]*x12*x21*x31        
        ;

    return v_l_e_q2ex_2_1200                              ;
}
float x_e_q2ex_2_1100                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.1272336E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.12513450E-02,-0.46817213E-02, 0.19546187E+00, 0.95480075E-02,
         0.44216070E-03,-0.15161271E-02,-0.38056737E-02,-0.28299675E-02,
         0.30121220E-04, 0.47976073E-05,-0.15794525E-04,-0.18186227E-02,
        -0.48279227E-03,-0.23562212E-02,-0.20950444E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_1100                              =v_x_e_q2ex_2_1100                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_1100                              ;
}
float t_e_q2ex_2_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3449674E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.33879108E-03,-0.25095707E-02, 0.54306943E-01, 0.25088394E-02,
        -0.12694671E-02,-0.13335227E-02,-0.54495147E-03, 0.18346468E-03,
        -0.47365821E-03,-0.50144654E-03,-0.19287635E-03,-0.51045885E-04,
        -0.17061349E-02,-0.12908054E-02,-0.11176171E-02, 0.21404339E-03,
         0.92562368E-04,-0.55139646E-03,-0.12862450E-02, 0.17830065E-04,
        -0.52823285E-04,-0.27282185E-04, 0.15527794E-04,-0.89879004E-05,
        -0.29280109E-05, 0.75754038E-07, 0.39988768E-05,-0.31795887E-04,
         0.63221391E-05, 0.10971966E-04, 0.39825594E-04, 0.68738467E-04,
         0.67905812E-05,-0.29659475E-04, 0.16378972E-04,-0.20179650E-04,
        -0.18196533E-04,-0.26312315E-04, 0.27135085E-04,-0.19583576E-04,
        -0.26160387E-04, 0.18940304E-04, 0.20978556E-04, 0.50029132E-04,
         0.51455867E-04, 0.29496599E-04, 0.45902449E-04,-0.64145033E-05,
         0.11704911E-04, 0.31044071E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_1100                              =v_t_e_q2ex_2_1100                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_1100                              =v_t_e_q2ex_2_1100                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11    *x32    *x52
        +coeff[ 24]*x11*x21            
        +coeff[ 25]*x13                
    ;
    v_t_e_q2ex_2_1100                              =v_t_e_q2ex_2_1100                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]        *x31*x42    
        +coeff[ 29]    *x22*x31*x41    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x12            *x52
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x11*x21*x33        
    ;
    v_t_e_q2ex_2_1100                              =v_t_e_q2ex_2_1100                              
        +coeff[ 35]    *x22*x33        
        +coeff[ 36]*x12*x21    *x42    
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x11        *x42*x52
        +coeff[ 40]*x11*x21        *x53
        +coeff[ 41]*x13*x23            
        +coeff[ 42]    *x22*x32*x41*x51
        +coeff[ 43]*x11*x22    *x42*x51
    ;
    v_t_e_q2ex_2_1100                              =v_t_e_q2ex_2_1100                              
        +coeff[ 44]    *x23    *x42*x51
        +coeff[ 45]*x11    *x31*x43*x51
        +coeff[ 46]    *x21    *x42*x53
        +coeff[ 47]*x11*x21    *x41    
        +coeff[ 48]*x11*x21        *x51
        +coeff[ 49]*x11    *x31    *x51
        ;

    return v_t_e_q2ex_2_1100                              ;
}
float y_e_q2ex_2_1100                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.1392433E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.25931155E-03, 0.20468412E+00, 0.10973018E+00, 0.39125659E-03,
        -0.10277104E-02,-0.10466606E-02,-0.63113804E-03,-0.75527036E-03,
        -0.47353396E-03, 0.28584534E-04,-0.10864895E-03,-0.46681057E-03,
        -0.46391629E-04,-0.80554157E-04, 0.52016321E-05,-0.53023931E-03,
        -0.49856305E-03,-0.68806068E-04, 0.79488498E-04,-0.15151564E-04,
         0.50191742E-04, 0.17988003E-04,-0.20284505E-03,-0.70031919E-03,
         0.33005017E-05,-0.57000376E-03, 0.13238855E-04,-0.19687612E-03,
        -0.60845439E-04, 0.58250327E-04, 0.10475756E-03, 0.22213343E-03,
         0.11894117E-03,-0.27711227E-03, 0.20942150E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_1100                              =v_y_e_q2ex_2_1100                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_1100                              =v_y_e_q2ex_2_1100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]        *x32*x41*x51
        +coeff[ 22]        *x32*x43    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q2ex_2_1100                              =v_y_e_q2ex_2_1100                              
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]        *x31*x44*x51
        +coeff[ 31]        *x32*x43*x51
        +coeff[ 32]        *x33*x42*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]        *x32*x45    
        ;

    return v_y_e_q2ex_2_1100                              ;
}
float p_e_q2ex_2_1100                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.5044434E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.61445127E-04,-0.18550679E-01,-0.19739056E-01, 0.17290476E-02,
         0.29680093E-02,-0.59785065E-03,-0.36452367E-03,-0.56536536E-03,
        -0.33505354E-03,-0.14964837E-03,-0.14817395E-04,-0.72273705E-03,
         0.76134480E-03,-0.63238062E-04,-0.30710251E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_1100                              =v_p_e_q2ex_2_1100                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x24*x31    *x52
        +coeff[ 13]        *x33        
        +coeff[ 14]        *x32*x41    
        ;

    return v_p_e_q2ex_2_1100                              ;
}
float l_e_q2ex_2_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2965534E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29206290E-02,-0.17739910E-03,-0.51221708E-02,-0.81892463E-03,
        -0.50183283E-02,-0.57824636E-02, 0.25441550E-03,-0.24208377E-03,
        -0.47655012E-04, 0.19872075E-03,-0.68035406E-04, 0.56981685E-03,
        -0.18496779E-04, 0.12710749E-02,-0.21326762E-03, 0.25800295E-03,
        -0.26281780E-03,-0.49135118E-03, 0.13366824E-02,-0.10463943E-02,
        -0.44274994E-03, 0.32727042E-03, 0.35673418E-03, 0.19606194E-03,
        -0.32621634E-03, 0.21884436E-03,-0.56239881E-03,-0.71959785E-03,
         0.25113512E-03, 0.27179200E-03,-0.39565138E-03, 0.50965184E-03,
        -0.40185030E-03, 0.49287169E-02,-0.45816982E-03,-0.58595970E-03,
         0.13492570E-02,-0.68823347E-03, 0.11924466E-02, 0.75963320E-03,
         0.82422089E-03,-0.42422718E-03,-0.48590929E-03, 0.47721920E-03,
         0.11491134E-02, 0.52160502E-03, 0.97670336E-03,-0.57537691E-03,
         0.67108596E-03,-0.80155267E-03,-0.49901381E-03, 0.12396686E-02,
        -0.10632956E-02, 0.48629552E-03,-0.90456958E-03,-0.13862713E-02,
        -0.19824549E-02,-0.56361910E-02,-0.12689847E-02,-0.20350672E-02,
        -0.26732113E-02,-0.60886005E-02,-0.33490364E-02,-0.22080513E-02,
         0.10508207E-02, 0.13605782E-04,-0.11280776E-03,-0.76514763E-04,
        -0.83867642E-04, 0.23464368E-03,-0.58341946E-04,-0.13102182E-03,
         0.18842863E-03, 0.19176037E-03, 0.12326161E-03,-0.62056286E-04,
        -0.17814216E-03, 0.11003054E-03, 0.12024795E-03,-0.15032434E-03,
         0.32236386E-03, 0.10542036E-02, 0.84797217E-03, 0.29076682E-03,
         0.38941271E-03,-0.13303178E-03,-0.31335620E-03,-0.29161887E-03,
        -0.48170704E-03, 0.10134918E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]*x12                
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21*x31    *x51
        +coeff[ 10]        *x32    *x51
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x24            
        +coeff[ 16]    *x23*x31        
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]    *x21*x31*x42    
        +coeff[ 19]*x11*x21*x32        
        +coeff[ 20]*x11    *x32    *x51
        +coeff[ 21]*x11            *x53
        +coeff[ 22]*x12*x22            
        +coeff[ 23]*x12    *x31    *x51
        +coeff[ 24]    *x24*x31        
        +coeff[ 25]    *x22*x32    *x51
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 26]    *x21*x31    *x53
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x24            
        +coeff[ 29]*x12*x21        *x52
        +coeff[ 30]*x12    *x31    *x52
        +coeff[ 31]*x13*x22            
        +coeff[ 32]    *x23*x33        
        +coeff[ 33]    *x22*x31*x41*x52
        +coeff[ 34]        *x33*x41*x52
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 35]    *x21*x31*x42*x52
        +coeff[ 36]    *x21    *x43*x52
        +coeff[ 37]*x11*x24*x31        
        +coeff[ 38]*x11    *x34    *x51
        +coeff[ 39]*x11*x21*x31*x42*x51
        +coeff[ 40]*x11*x22*x31    *x52
        +coeff[ 41]*x11        *x43*x52
        +coeff[ 42]*x11*x22        *x53
        +coeff[ 43]*x12*x21*x31    *x52
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 44]*x13*x21*x32        
        +coeff[ 45]    *x23    *x43*x51
        +coeff[ 46]    *x22*x31*x42*x52
        +coeff[ 47]*x13            *x53
        +coeff[ 48]            *x44*x53
        +coeff[ 49]        *x33    *x54
        +coeff[ 50]*x11*x23*x33        
        +coeff[ 51]*x12*x21*x33*x41    
        +coeff[ 52]*x12*x22*x31*x41*x51
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 53]*x13*x22*x32        
        +coeff[ 54]*x13*x22    *x42    
        +coeff[ 55]    *x23*x33*x42    
        +coeff[ 56]        *x34*x44    
        +coeff[ 57]    *x24*x31*x41*x52
        +coeff[ 58]    *x22*x33*x41*x52
        +coeff[ 59]    *x24    *x42*x52
        +coeff[ 60]        *x34*x42*x52
        +coeff[ 61]        *x33*x43*x52
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 62]        *x33*x42*x53
        +coeff[ 63]        *x32*x43*x53
        +coeff[ 64]    *x23*x31    *x54
        +coeff[ 65]    *x21            
        +coeff[ 66]            *x41    
        +coeff[ 67]*x11                
        +coeff[ 68]        *x31    *x51
        +coeff[ 69]                *x52
        +coeff[ 70]*x11    *x31        
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 71]    *x23            
        +coeff[ 72]        *x33        
        +coeff[ 73]        *x32*x41    
        +coeff[ 74]            *x41*x52
        +coeff[ 75]                *x53
        +coeff[ 76]*x11    *x31*x41    
        +coeff[ 77]*x11        *x41*x51
        +coeff[ 78]*x12*x21            
        +coeff[ 79]    *x22*x31*x41    
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 80]        *x33*x41    
        +coeff[ 81]        *x32*x42    
        +coeff[ 82]        *x31*x43    
        +coeff[ 83]            *x44    
        +coeff[ 84]        *x33    *x51
        +coeff[ 85]    *x22    *x41*x51
        +coeff[ 86]    *x21*x31    *x52
        +coeff[ 87]        *x32    *x52
        +coeff[ 88]    *x21    *x41*x52
    ;
    v_l_e_q2ex_2_1100                              =v_l_e_q2ex_2_1100                              
        +coeff[ 89]        *x31*x41*x52
        ;

    return v_l_e_q2ex_2_1100                              ;
}
float x_e_q2ex_2_1000                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.9019321E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.84701035E-03,-0.46804794E-02, 0.19548753E+00, 0.95347930E-02,
         0.44331755E-03,-0.15200770E-02,-0.37997328E-02,-0.28130997E-02,
        -0.35313340E-04, 0.39745435E-04,-0.55757923E-05,-0.18153774E-02,
        -0.50911604E-03,-0.23429489E-02,-0.21143917E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_1000                              =v_x_e_q2ex_2_1000                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_1000                              ;
}
float t_e_q2ex_2_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2465477E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.23111927E-03,-0.25078373E-02, 0.54332685E-01, 0.25155894E-02,
        -0.12959089E-02,-0.13397932E-02,-0.53645077E-03, 0.18322965E-03,
        -0.47385876E-03,-0.52225904E-03,-0.20656234E-03,-0.16852226E-02,
        -0.12901254E-02,-0.10868782E-02,-0.47873582E-04, 0.22313760E-03,
         0.10948130E-03,-0.50177588E-03,-0.12272623E-02, 0.17146351E-04,
        -0.33689728E-04,-0.25166586E-04,-0.11915452E-04,-0.58690625E-05,
        -0.20344840E-04,-0.88680117E-05,-0.72874036E-05, 0.74780869E-05,
         0.98011124E-05, 0.57513153E-04,-0.29941575E-05,-0.81387196E-04,
        -0.54742497E-04,-0.16224543E-04, 0.21137677E-04, 0.32485899E-04,
        -0.12348187E-04, 0.61880769E-05,-0.13411667E-04, 0.52508862E-04,
         0.62086110E-04, 0.31519219E-04,-0.11844670E-05,-0.44883886E-05,
         0.74528461E-05,-0.34602031E-05, 0.49740797E-05,-0.45990400E-05,
        -0.29724704E-05, 0.44069607E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_1000                              =v_t_e_q2ex_2_1000                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_1000                              =v_t_e_q2ex_2_1000                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x11            *x52
        +coeff[ 23]*x11    *x32    *x52
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11    *x33        
    ;
    v_t_e_q2ex_2_1000                              =v_t_e_q2ex_2_1000                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]*x12*x21        *x51
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]    *x21        *x53
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_q2ex_2_1000                              =v_t_e_q2ex_2_1000                              
        +coeff[ 35]    *x22    *x41*x52
        +coeff[ 36]*x12*x23*x31        
        +coeff[ 37]*x12*x23        *x51
        +coeff[ 38]    *x22*x33    *x51
        +coeff[ 39]    *x23    *x42*x51
        +coeff[ 40]    *x21*x32*x42*x51
        +coeff[ 41]    *x23        *x53
        +coeff[ 42]        *x31        
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_q2ex_2_1000                              =v_t_e_q2ex_2_1000                              
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]    *x22    *x41    
        +coeff[ 46]    *x21    *x41*x51
        +coeff[ 47]            *x41*x52
        +coeff[ 48]*x11*x23            
        +coeff[ 49]*x13    *x31        
        ;

    return v_t_e_q2ex_2_1000                              ;
}
float y_e_q2ex_2_1000                              (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.1767374E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.13367848E-03, 0.20451294E+00, 0.10970072E+00, 0.40728244E-03,
        -0.10228640E-02,-0.93263469E-03,-0.57981542E-03,-0.73327741E-03,
        -0.29973455E-04,-0.75640141E-05,-0.47728914E-03,-0.10380764E-03,
        -0.38779681E-03,-0.28006891E-04,-0.62348765E-04, 0.86744922E-05,
        -0.54041675E-03,-0.54029393E-03,-0.56058716E-05, 0.77072211E-04,
        -0.75898791E-03,-0.61060785E-03,-0.22328213E-03,-0.13572660E-03,
        -0.97403630E-04, 0.97533011E-04, 0.81901242E-04, 0.12946191E-04,
         0.26376394E-04,-0.42870708E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_1000                              =v_y_e_q2ex_2_1000                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q2ex_2_1000                              =v_y_e_q2ex_2_1000                              
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x22*x31*x42    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]*x11*x23    *x41    
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]    *x22    *x45    
    ;
    v_y_e_q2ex_2_1000                              =v_y_e_q2ex_2_1000                              
        +coeff[ 26]    *x24    *x41*x51
        +coeff[ 27]        *x31*x41*x51
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]    *x22    *x43    
        ;

    return v_y_e_q2ex_2_1000                              ;
}
float p_e_q2ex_2_1000                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.6296684E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.58327205E-04,-0.18564377E-01,-0.19748047E-01, 0.17284804E-02,
         0.29673027E-02,-0.59686601E-03,-0.35404993E-03,-0.56592858E-03,
        -0.33408735E-03,-0.15225084E-03,-0.19652767E-04,-0.71504805E-03,
         0.73140021E-03,-0.61351857E-04,-0.30196185E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_1000                              =v_p_e_q2ex_2_1000                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x24*x31    *x52
        +coeff[ 13]        *x33        
        +coeff[ 14]        *x32*x41    
        ;

    return v_p_e_q2ex_2_1000                              ;
}
float l_e_q2ex_2_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2945306E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29531354E-02,-0.50519481E-02,-0.66459522E-03,-0.46112803E-02,
        -0.56854538E-02, 0.95791016E-04, 0.11862478E-03,-0.35231048E-03,
         0.46095427E-03,-0.25923594E-03, 0.11827367E-02, 0.56953030E-03,
         0.64099202E-03,-0.53317327E-03, 0.40140792E-03,-0.56837476E-03,
         0.28038124E-03,-0.36123325E-03,-0.31491881E-03, 0.28894827E-03,
         0.15976841E-02,-0.11106902E-02,-0.10990564E-02,-0.65825350E-03,
        -0.11235815E-02, 0.24429787E-03, 0.21200378E-03,-0.12893700E-02,
         0.97381731E-03, 0.17831384E-02, 0.14410893E-03,-0.10792410E-02,
        -0.73637749E-03, 0.12201082E-02, 0.21628218E-02, 0.75126759E-03,
        -0.17237224E-02, 0.10179133E-03, 0.15855465E-02,-0.14125644E-02,
         0.12949067E-02, 0.39491840E-02,-0.12196435E-02, 0.13570039E-02,
        -0.18145232E-02, 0.13428712E-04, 0.23442772E-03, 0.10219309E-03,
         0.47781132E-03,-0.76051088E-04, 0.89229055E-04,-0.21321986E-03,
         0.41890755E-04,-0.12006614E-03, 0.11191805E-03, 0.20956800E-03,
        -0.19154394E-03, 0.39446427E-03,-0.42455370E-03,-0.30893245E-03,
         0.24541144E-03,-0.23111359E-03,-0.38315524E-04, 0.15577261E-03,
        -0.55695971E-03, 0.29165269E-03, 0.10357349E-02,-0.33808312E-04,
        -0.10987689E-02,-0.31394538E-03,-0.17996737E-04,-0.14826395E-03,
         0.12506137E-03,-0.22280306E-03,-0.21318128E-03, 0.18380691E-03,
        -0.26952272E-03,-0.63744769E-03, 0.11743166E-03,-0.26290570E-03,
         0.50755456E-03, 0.39488226E-03, 0.66013634E-03, 0.41694017E-03,
        -0.11157226E-02,-0.50820172E-03,-0.37310735E-03, 0.11000824E-03,
        -0.50403102E-03,-0.39947793E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q2ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[  8]*x12            *x51
        +coeff[  9]    *x21*x31    *x51
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]    *x21*x33        
        +coeff[ 15]        *x34        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 17]        *x33    *x51
        +coeff[ 18]    *x21*x31    *x52
        +coeff[ 19]*x12*x21        *x51
        +coeff[ 20]    *x22*x32    *x51
        +coeff[ 21]    *x22*x31*x41*x51
        +coeff[ 22]        *x33*x41*x51
        +coeff[ 23]            *x42*x53
        +coeff[ 24]*x11*x21    *x43    
        +coeff[ 25]*x11*x21        *x53
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 26]*x11        *x41*x53
        +coeff[ 27]*x12    *x32    *x51
        +coeff[ 28]*x11*x23    *x42    
        +coeff[ 29]*x11*x22    *x43    
        +coeff[ 30]*x11    *x32*x43    
        +coeff[ 31]*x11*x21    *x44    
        +coeff[ 32]*x11*x21*x32*x41*x51
        +coeff[ 33]*x11*x21    *x42*x52
        +coeff[ 34]*x11    *x31*x41*x53
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 35]    *x23    *x44    
        +coeff[ 36]    *x22*x34    *x51
        +coeff[ 37]*x13    *x31*x41*x51
        +coeff[ 38]*x11*x21*x32*x43    
        +coeff[ 39]*x11*x21*x32*x41*x52
        +coeff[ 40]*x12    *x32    *x53
        +coeff[ 41]    *x23*x31*x43*x51
        +coeff[ 42]    *x21*x33*x43*x51
        +coeff[ 43]    *x23*x32*x41*x52
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 44]    *x23*x31*x41*x53
        +coeff[ 45]*x12                
        +coeff[ 46]    *x22*x31        
        +coeff[ 47]        *x33        
        +coeff[ 48]    *x21*x31*x41    
        +coeff[ 49]        *x31*x42    
        +coeff[ 50]    *x21    *x41*x51
        +coeff[ 51]*x11    *x32        
        +coeff[ 52]*x11        *x42    
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 53]*x12    *x31        
        +coeff[ 54]*x13                
        +coeff[ 55]    *x24            
        +coeff[ 56]    *x23        *x51
        +coeff[ 57]    *x21    *x42*x51
        +coeff[ 58]        *x31*x42*x51
        +coeff[ 59]            *x43*x51
        +coeff[ 60]        *x32    *x52
        +coeff[ 61]    *x21    *x41*x52
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 62]            *x42*x52
        +coeff[ 63]            *x41*x53
        +coeff[ 64]*x11*x22    *x41    
        +coeff[ 65]*x11    *x32*x41    
        +coeff[ 66]*x11    *x31*x42    
        +coeff[ 67]*x11*x22        *x51
        +coeff[ 68]*x11    *x31*x41*x51
        +coeff[ 69]*x11*x21        *x52
        +coeff[ 70]*x11    *x31    *x52
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 71]*x11        *x41*x52
        +coeff[ 72]*x12*x21    *x41    
        +coeff[ 73]*x12    *x31    *x51
        +coeff[ 74]*x12            *x52
        +coeff[ 75]*x13*x21            
        +coeff[ 76]*x13    *x31        
        +coeff[ 77]    *x24*x31        
        +coeff[ 78]    *x21*x34        
        +coeff[ 79]    *x24    *x41    
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 80]    *x23*x31*x41    
        +coeff[ 81]        *x34*x41    
        +coeff[ 82]    *x22*x31*x42    
        +coeff[ 83]    *x22    *x43    
        +coeff[ 84]    *x21*x31*x43    
        +coeff[ 85]        *x32*x43    
        +coeff[ 86]    *x21    *x44    
        +coeff[ 87]*x13            *x51
        +coeff[ 88]    *x24        *x51
    ;
    v_l_e_q2ex_2_1000                              =v_l_e_q2ex_2_1000                              
        +coeff[ 89]    *x23    *x41*x51
        ;

    return v_l_e_q2ex_2_1000                              ;
}
float x_e_q2ex_2_900                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.6306344E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.58550853E-03,-0.46777301E-02, 0.19554935E+00, 0.95351031E-02,
         0.43716881E-03,-0.15155575E-02,-0.38362511E-02,-0.28358561E-02,
         0.39639213E-04,-0.21670006E-04,-0.14466074E-04,-0.18073224E-02,
        -0.48091487E-03,-0.22642221E-02,-0.20433031E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_900                              =v_x_e_q2ex_2_900                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_900                              ;
}
float t_e_q2ex_2_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1710348E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15872998E-03,-0.25045974E-02, 0.54366175E-01, 0.25028614E-02,
        -0.12897509E-02,-0.13483065E-02,-0.51169214E-03, 0.18417340E-03,
        -0.48170754E-03,-0.51494857E-03,-0.19420896E-03,-0.16346001E-02,
        -0.12548206E-02,-0.11344979E-02,-0.47716941E-04, 0.22910317E-03,
         0.13845159E-03,-0.53988356E-03,-0.12902932E-02, 0.10856616E-04,
        -0.37829646E-04,-0.29731042E-04, 0.70671958E-04, 0.94579109E-05,
         0.26172882E-04,-0.25769310E-04,-0.32172877E-05,-0.93021599E-05,
         0.89829791E-05, 0.24552990E-04, 0.16384233E-05,-0.69166148E-04,
        -0.48194619E-04,-0.19021789E-04, 0.15357769E-04,-0.73488918E-04,
        -0.40881321E-04, 0.28646287E-04, 0.71452683E-04, 0.51496954E-04,
        -0.98650821E-06, 0.21742615E-05,-0.36628712E-05,-0.26442933E-05,
        -0.26920845E-05,-0.61280630E-05,-0.68687141E-05,-0.29454031E-05,
         0.34671336E-05,-0.59975164E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_900                              =v_t_e_q2ex_2_900                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_900                              =v_t_e_q2ex_2_900                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x13    *x32        
        +coeff[ 24]    *x23        *x53
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2ex_2_900                              =v_t_e_q2ex_2_900                              
        +coeff[ 26]    *x21*x31    *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x11*x22*x31        
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]*x12        *x41*x51
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x12*x21*x31    *x51
        +coeff[ 34]*x11*x21*x32    *x51
    ;
    v_t_e_q2ex_2_900                              =v_t_e_q2ex_2_900                              
        +coeff[ 35]*x11*x21*x33    *x51
        +coeff[ 36]*x12*x22    *x41*x51
        +coeff[ 37]*x11*x21*x31*x42*x51
        +coeff[ 38]    *x21*x32*x42*x51
        +coeff[ 39]*x11*x21*x31    *x53
        +coeff[ 40]                *x51
        +coeff[ 41]    *x22            
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]        *x31    *x51
    ;
    v_t_e_q2ex_2_900                              =v_t_e_q2ex_2_900                              
        +coeff[ 44]                *x52
        +coeff[ 45]*x13                
        +coeff[ 46]        *x31*x42    
        +coeff[ 47]            *x43    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]*x12*x21*x31        
        ;

    return v_t_e_q2ex_2_900                              ;
}
float y_e_q2ex_2_900                              (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2134690E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.14163351E-03, 0.20453244E+00, 0.10967252E+00, 0.40689300E-03,
        -0.10202265E-02,-0.92139613E-03,-0.70301373E-03,-0.74325525E-03,
        -0.32220891E-04, 0.47682970E-05,-0.45760183E-03,-0.12438257E-03,
        -0.39623067E-03,-0.36756097E-04,-0.66225541E-04, 0.27103774E-05,
        -0.41514190E-03,-0.33126558E-04,-0.55337942E-03,-0.63682889E-03,
        -0.82471270E-04, 0.84795443E-04,-0.20248137E-04,-0.23805692E-03,
        -0.79279553E-05, 0.16551432E-04, 0.54297368E-04, 0.71686212E-04,
         0.14059224E-04, 0.66385983E-04,-0.40931872E-03, 0.41830954E-04,
        -0.64632233E-03, 0.18448427E-04,-0.81810016E-04, 0.30747357E-04,
        -0.79359976E-04, 0.25705878E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_900                              =v_y_e_q2ex_2_900                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_900                              =v_y_e_q2ex_2_900                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x31*x44    
        +coeff[ 21]    *x24    *x43    
        +coeff[ 22]    *x24*x31*x42    
        +coeff[ 23]    *x24*x33        
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]        *x31*x41*x51
    ;
    v_y_e_q2ex_2_900                              =v_y_e_q2ex_2_900                              
        +coeff[ 26]        *x31*x42*x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]*x12        *x43    
        +coeff[ 34]*x11*x23    *x41    
    ;
    v_y_e_q2ex_2_900                              =v_y_e_q2ex_2_900                              
        +coeff[ 35]*x11    *x31*x42*x51
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x11*x23    *x42    
        ;

    return v_y_e_q2ex_2_900                              ;
}
float p_e_q2ex_2_900                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.3720134E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.28582514E-04,-0.18523365E-01,-0.19773657E-01, 0.17292146E-02,
         0.29694391E-02,-0.60173991E-03, 0.27955228E-04,-0.57346327E-03,
        -0.33704366E-03,-0.15461225E-03,-0.14730778E-04,-0.14492583E-05,
        -0.36867324E-03,-0.64621272E-04,-0.31394389E-03,-0.10631711E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_900                              =v_p_e_q2ex_2_900                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]        *x33        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_2_900                              ;
}
float l_e_q2ex_2_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2932891E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29591902E-02,-0.50825714E-02,-0.12459949E-02,-0.44149407E-02,
        -0.54747420E-02, 0.12549809E-03, 0.18685979E-03,-0.57736551E-03,
        -0.15453395E-03,-0.36587351E-03, 0.21339048E-03, 0.12964343E-03,
         0.69999835E-04, 0.61718689E-03,-0.29819660E-03,-0.70869958E-03,
        -0.12168413E-03, 0.10951667E-02,-0.51171333E-03, 0.64067112E-03,
        -0.58931677E-03,-0.29912830E-03, 0.91224484E-03,-0.31784890E-03,
        -0.49271400E-03,-0.55504841E-03,-0.64995699E-03,-0.65447058E-03,
         0.19836670E-02,-0.31691336E-03, 0.11223461E-02,-0.14252847E-03,
         0.68456313E-03,-0.10374537E-02, 0.42471034E-03,-0.87989395E-03,
        -0.21274441E-02,-0.16613498E-02, 0.99165633E-03,-0.16369176E-02,
        -0.73316350E-03, 0.33897042E-03, 0.18141142E-03, 0.31047850E-03,
        -0.99218590E-03,-0.76301605E-03, 0.76857227E-03, 0.17130729E-02,
        -0.24840925E-02, 0.45684236E-03,-0.24186561E-02, 0.10741338E-02,
         0.16663253E-02,-0.15291821E-02, 0.20098151E-02,-0.18846504E-02,
         0.70393371E-03,-0.18547091E-02, 0.79542704E-04, 0.18692694E-04,
        -0.22578056E-03,-0.87200524E-03, 0.44701976E-03,-0.19248511E-03,
        -0.98604774E-04,-0.11259028E-03, 0.12399255E-03,-0.10760625E-03,
         0.16362240E-03,-0.36730626E-03, 0.15811961E-03, 0.87368651E-04,
         0.35820241E-03, 0.20720961E-03, 0.19882258E-03, 0.99096564E-03,
        -0.22804216E-03, 0.26768737E-03,-0.38093750E-03,-0.25060200E-03,
        -0.32486426E-03, 0.19603338E-03,-0.44636693E-03, 0.95400453E-03,
        -0.65863767E-03, 0.50398253E-03, 0.27316131E-03, 0.31214015E-03,
         0.26297977E-03, 0.17431255E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x44    
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[  8]    *x21    *x41    
        +coeff[  9]    *x22        *x51
        +coeff[ 10]        *x32    *x51
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]                *x53
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x12    *x31        
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 17]    *x23*x31        
        +coeff[ 18]    *x21*x31*x42    
        +coeff[ 19]*x11    *x31*x41*x51
        +coeff[ 20]*x12        *x42    
        +coeff[ 21]*x12    *x31    *x51
        +coeff[ 22]*x12            *x52
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x21        *x54
        +coeff[ 25]*x11*x21*x33        
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 26]*x11*x21*x31*x42    
        +coeff[ 27]*x11    *x33    *x51
        +coeff[ 28]*x11    *x31*x42*x51
        +coeff[ 29]*x11    *x31    *x53
        +coeff[ 30]*x11        *x41*x53
        +coeff[ 31]*x12*x21*x32        
        +coeff[ 32]*x12*x21*x31*x41    
        +coeff[ 33]*x12*x21    *x41*x51
        +coeff[ 34]*x12        *x42*x51
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 35]    *x23*x33        
        +coeff[ 36]*x11*x22*x32*x41    
        +coeff[ 37]*x11    *x32*x43    
        +coeff[ 38]*x11*x22*x32    *x51
        +coeff[ 39]*x11    *x31*x43*x51
        +coeff[ 40]*x11    *x32    *x53
        +coeff[ 41]*x11    *x31    *x54
        +coeff[ 42]*x12*x24            
        +coeff[ 43]*x12*x22*x32        
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 44]*x12            *x54
        +coeff[ 45]*x13*x22*x31        
        +coeff[ 46]    *x23        *x54
        +coeff[ 47]*x11*x22*x34        
        +coeff[ 48]*x11*x22*x33    *x51
        +coeff[ 49]*x11*x21*x34    *x51
        +coeff[ 50]*x11    *x31*x44*x51
        +coeff[ 51]*x11    *x33*x41*x52
        +coeff[ 52]*x11*x22*x31    *x53
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 53]*x11    *x32*x41*x53
        +coeff[ 54]*x12*x21*x32    *x52
        +coeff[ 55]*x13*x22*x32        
        +coeff[ 56]*x13    *x32    *x52
        +coeff[ 57]    *x22*x32*x41*x53
        +coeff[ 58]    *x21            
        +coeff[ 59]        *x31        
        +coeff[ 60]*x11                
        +coeff[ 61]    *x21*x31        
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 62]*x11    *x31        
        +coeff[ 63]*x11        *x41    
        +coeff[ 64]*x11            *x51
        +coeff[ 65]        *x33        
        +coeff[ 66]    *x21    *x42    
        +coeff[ 67]    *x21*x31    *x51
        +coeff[ 68]        *x31*x41*x51
        +coeff[ 69]    *x21        *x52
        +coeff[ 70]*x11*x22            
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 71]*x11        *x42    
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x13                
        +coeff[ 74]    *x22*x32        
        +coeff[ 75]    *x21*x33        
        +coeff[ 76]        *x32*x42    
        +coeff[ 77]            *x44    
        +coeff[ 78]    *x22*x31    *x51
        +coeff[ 79]        *x33    *x51
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 80]        *x31*x42*x51
        +coeff[ 81]    *x21*x31    *x52
        +coeff[ 82]*x11    *x33        
        +coeff[ 83]*x11    *x32*x41    
        +coeff[ 84]*x11    *x31*x42    
        +coeff[ 85]*x11        *x43    
        +coeff[ 86]*x11*x21*x31    *x51
        +coeff[ 87]*x11    *x32    *x51
        +coeff[ 88]*x11*x21    *x41*x51
    ;
    v_l_e_q2ex_2_900                              =v_l_e_q2ex_2_900                              
        +coeff[ 89]*x11*x21        *x52
        ;

    return v_l_e_q2ex_2_900                              ;
}
float x_e_q2ex_2_800                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1994170E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.16569840E-04,-0.46754340E-02, 0.19563836E+00, 0.95402375E-02,
         0.43610795E-03,-0.15147972E-02,-0.38400001E-02,-0.28532620E-02,
         0.15460246E-04,-0.16712895E-04,-0.62866875E-05,-0.18039815E-02,
        -0.49751136E-03,-0.22651122E-02,-0.20206501E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_800                              =v_x_e_q2ex_2_800                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_800                              ;
}
float t_e_q2ex_2_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1893774E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.17178872E-04,-0.25049101E-02, 0.54410532E-01, 0.24922295E-02,
        -0.12728822E-02,-0.13537979E-02,-0.53622707E-03, 0.18333578E-03,
        -0.47402683E-03,-0.50510914E-03,-0.20139007E-03,-0.16531134E-02,
        -0.12532049E-02,-0.11352302E-02,-0.51275238E-04, 0.17801058E-03,
         0.16455962E-03,-0.54993149E-03,-0.12784397E-02, 0.15519561E-04,
        -0.24357319E-04,-0.32878736E-04,-0.15607714E-05,-0.28529023E-05,
        -0.22057762E-04,-0.20459825E-04,-0.11127085E-04, 0.53409167E-04,
         0.76279532E-04,-0.77640827E-04,-0.26189144E-04,-0.47546517E-04,
         0.24132954E-04, 0.27962105E-04, 0.26111022E-04, 0.24589224E-04,
         0.30606814E-04, 0.44763870E-04, 0.79477650E-04,-0.23976667E-04,
        -0.29788327E-04,-0.24532483E-05,-0.30280057E-05,-0.58179749E-05,
         0.30328033E-05, 0.37587356E-05,-0.10743153E-04,-0.57337616E-05,
         0.53164431E-05, 0.40369641E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_800                              =v_t_e_q2ex_2_800                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_800                              =v_t_e_q2ex_2_800                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21    *x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_t_e_q2ex_2_800                              =v_t_e_q2ex_2_800                              
        +coeff[ 26]*x11            *x52
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11    *x33*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x13*x21        *x51
        +coeff[ 33]    *x23*x31    *x51
        +coeff[ 34]    *x22*x31*x41*x51
    ;
    v_t_e_q2ex_2_800                              =v_t_e_q2ex_2_800                              
        +coeff[ 35]*x11*x22*x33        
        +coeff[ 36]    *x23    *x43    
        +coeff[ 37]*x13*x21*x31    *x51
        +coeff[ 38]    *x23*x31*x41*x51
        +coeff[ 39]*x11*x22*x31    *x52
        +coeff[ 40]*x11*x21*x31    *x53
        +coeff[ 41]    *x22            
        +coeff[ 42]*x12*x21            
        +coeff[ 43]*x12        *x41    
    ;
    v_t_e_q2ex_2_800                              =v_t_e_q2ex_2_800                              
        +coeff[ 44]            *x43    
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]    *x21*x31    *x51
        +coeff[ 47]        *x31*x41*x51
        +coeff[ 48]    *x23*x31        
        +coeff[ 49]*x11*x21*x32        
        ;

    return v_t_e_q2ex_2_800                              ;
}
float y_e_q2ex_2_800                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.9640701E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.10020779E-02, 0.20452030E+00, 0.10964146E+00, 0.40220394E-03,
        -0.10244703E-02,-0.10064847E-02,-0.59641717E-03,-0.73752750E-03,
         0.99849502E-04, 0.76214543E-04,-0.49370556E-03,-0.11158604E-03,
        -0.54393109E-03,-0.35677531E-04,-0.58588303E-04, 0.14244014E-04,
        -0.54732955E-03,-0.54251554E-03, 0.81870976E-05, 0.81459126E-04,
        -0.13015786E-03,-0.94020434E-04, 0.68659552E-04, 0.83372368E-04,
         0.17411969E-04, 0.66871413E-04,-0.74993807E-03,-0.62383845E-03,
        -0.19744260E-03,-0.64116546E-04, 0.26993683E-04,-0.29013556E-03,
         0.65383385E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_800                              =v_y_e_q2ex_2_800                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_q2ex_2_800                              =v_y_e_q2ex_2_800                              
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]*x11*x23    *x41    
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]    *x24    *x41*x51
        +coeff[ 23]        *x33*x44    
        +coeff[ 24]*x11*x21    *x42    
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_q2ex_2_800                              =v_y_e_q2ex_2_800                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x21*x32*x41    
        +coeff[ 30]            *x45*x51
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x41*x51
        ;

    return v_y_e_q2ex_2_800                              ;
}
float p_e_q2ex_2_800                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.4646080E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.51006991E-04,-0.18545100E-01,-0.19804567E-01, 0.17305952E-02,
         0.29706920E-02,-0.60087722E-03, 0.18634464E-04,-0.57676691E-03,
        -0.33880735E-03,-0.15593518E-03,-0.81623020E-05, 0.49828095E-05,
        -0.36149178E-03,-0.63541636E-04,-0.31869891E-03,-0.10766523E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_800                              =v_p_e_q2ex_2_800                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]        *x33        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]        *x31    *x52
        ;

    return v_p_e_q2ex_2_800                              ;
}
float l_e_q2ex_2_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2925965E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29674694E-02,-0.53958534E-02,-0.10699468E-02,-0.43707206E-02,
        -0.54963296E-02, 0.19886013E-03,-0.71351198E-04,-0.11059231E-03,
         0.25675414E-03, 0.24114808E-04, 0.54237380E-03, 0.10820468E-02,
         0.56038531E-04, 0.32366870E-03, 0.35375383E-03, 0.16602104E-03,
        -0.64592646E-03,-0.21493316E-03, 0.10846305E-02, 0.10523438E-03,
         0.81768609E-03, 0.25269514E-03, 0.89703139E-03,-0.47581989E-03,
         0.15639026E-02, 0.16068537E-02,-0.87214878E-03,-0.13012965E-02,
        -0.40334457E-03,-0.30663670E-02,-0.16316551E-02, 0.80697931E-03,
         0.73414668E-03,-0.56219468E-03,-0.94366918E-03,-0.84424176E-03,
         0.13155071E-02, 0.15795284E-02,-0.74496301E-03, 0.33423705E-02,
        -0.23630655E-02,-0.18623970E-02,-0.55418414E-03,-0.33309977E-03,
         0.63644827E-03, 0.11223353E-02, 0.87369722E-03, 0.18212089E-03,
         0.31332118E-02, 0.35186089E-02,-0.13752394E-03, 0.77402492E-05,
        -0.50845874E-05,-0.31556410E-03, 0.15989336E-03,-0.10277893E-03,
         0.83548261E-03,-0.70149843E-04, 0.18466024E-03, 0.35850337E-03,
         0.22591471E-03, 0.23950428E-03, 0.17129593E-03,-0.21193734E-03,
        -0.46757158E-03,-0.18662612E-03, 0.59616583E-03,-0.23136781E-04,
         0.33041515E-03,-0.69597876E-03,-0.19572880E-03,-0.19685479E-03,
         0.17918232E-03,-0.15346077E-03, 0.23013307E-03,-0.17671799E-03,
         0.18847919E-03, 0.38424486E-03,-0.64266118E-03,-0.21166851E-03,
         0.80730976E-03,-0.63257478E-03, 0.86439948E-03,-0.10865130E-02,
        -0.37025081E-03,-0.30312655E-03,-0.21285156E-03,-0.11845070E-02,
         0.49889012E-03,-0.57187642E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x21        *x51
        +coeff[  7]*x12                
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[  8]            *x42*x51
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]    *x24            
        +coeff[ 11]    *x22*x31*x41    
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]*x11*x21*x32        
        +coeff[ 14]*x11*x21*x31    *x51
        +coeff[ 15]*x11    *x32    *x51
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 17]    *x24        *x51
        +coeff[ 18]    *x22    *x41*x52
        +coeff[ 19]        *x31*x41*x53
        +coeff[ 20]*x11*x21*x31*x42    
        +coeff[ 21]*x11    *x33    *x51
        +coeff[ 22]*x11    *x31*x41*x52
        +coeff[ 23]*x13    *x31*x41    
        +coeff[ 24]    *x22*x32*x42    
        +coeff[ 25]    *x23*x31*x41*x51
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 26]        *x32*x43*x51
        +coeff[ 27]    *x22*x31*x41*x52
        +coeff[ 28]        *x33    *x53
        +coeff[ 29]    *x21*x31*x41*x53
        +coeff[ 30]*x11*x24    *x41    
        +coeff[ 31]*x11*x22    *x43    
        +coeff[ 32]*x11*x24        *x51
        +coeff[ 33]*x11*x22        *x53
        +coeff[ 34]*x12    *x32*x42    
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 35]*x12*x21    *x43    
        +coeff[ 36]    *x21*x34*x41*x51
        +coeff[ 37]        *x34*x42*x51
        +coeff[ 38]    *x23    *x43*x51
        +coeff[ 39]    *x22*x31*x43*x51
        +coeff[ 40]    *x22*x32*x41*x52
        +coeff[ 41]        *x33*x42*x52
        +coeff[ 42]        *x32*x41*x54
        +coeff[ 43]*x12    *x34*x41    
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 44]*x12*x21*x33    *x51
        +coeff[ 45]*x12    *x31*x42*x52
        +coeff[ 46]    *x24*x33    *x51
        +coeff[ 47]    *x21*x34*x42*x51
        +coeff[ 48]    *x21*x33*x41*x53
        +coeff[ 49]    *x22*x31*x41*x54
        +coeff[ 50]    *x21*x31        
        +coeff[ 51]    *x21    *x41    
        +coeff[ 52]            *x41*x51
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 53]*x11    *x31        
        +coeff[ 54]*x11            *x51
        +coeff[ 55]    *x21*x32        
        +coeff[ 56]        *x31*x41*x51
        +coeff[ 57]                *x53
        +coeff[ 58]    *x23*x31        
        +coeff[ 59]    *x21    *x43    
        +coeff[ 60]    *x22    *x41*x51
        +coeff[ 61]            *x43*x51
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 62]        *x32    *x52
        +coeff[ 63]    *x21    *x41*x52
        +coeff[ 64]        *x31*x41*x52
        +coeff[ 65]            *x42*x52
        +coeff[ 66]*x11*x22    *x41    
        +coeff[ 67]*x11    *x32*x41    
        +coeff[ 68]*x11    *x31*x42    
        +coeff[ 69]*x11*x22        *x51
        +coeff[ 70]*x11    *x31*x41*x51
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 71]*x11*x21        *x52
        +coeff[ 72]*x11    *x31    *x52
        +coeff[ 73]*x12*x22            
        +coeff[ 74]*x12        *x42    
        +coeff[ 75]*x12        *x41*x51
        +coeff[ 76]*x13    *x31        
        +coeff[ 77]    *x24*x31        
        +coeff[ 78]    *x23*x32        
        +coeff[ 79]    *x22*x33        
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 80]    *x21*x34        
        +coeff[ 81]    *x23*x31*x41    
        +coeff[ 82]    *x21*x33*x41    
        +coeff[ 83]    *x22*x31*x42    
        +coeff[ 84]    *x22    *x43    
        +coeff[ 85]    *x21*x31*x43    
        +coeff[ 86]        *x32*x43    
        +coeff[ 87]    *x22*x31*x41*x51
        +coeff[ 88]    *x22    *x42*x51
    ;
    v_l_e_q2ex_2_800                              =v_l_e_q2ex_2_800                              
        +coeff[ 89]        *x31*x43*x51
        ;

    return v_l_e_q2ex_2_800                              ;
}
float x_e_q2ex_2_700                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.9476317E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.95399731E-03, 0.19572163E+00, 0.95339194E-02, 0.67325450E-05,
        -0.46804142E-02, 0.43566461E-03,-0.15035522E-02,-0.38228058E-02,
        -0.28031992E-02,-0.17349557E-04, 0.30426045E-05,-0.61430933E-05,
        -0.17949402E-02,-0.49228582E-03,-0.22877981E-02,-0.20945254E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x23            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_q2ex_2_700                              =v_x_e_q2ex_2_700                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]*x12*x21*x32        
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x12*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23*x31*x41    
        +coeff[ 15]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_700                              ;
}
float t_e_q2ex_2_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2593192E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.26127824E-03,-0.25057916E-02, 0.54451548E-01, 0.24954171E-02,
        -0.12592176E-02,-0.13284194E-02,-0.55834639E-03, 0.17925043E-03,
        -0.45469237E-03,-0.48883987E-03,-0.19728554E-03,-0.16934206E-02,
        -0.13048376E-02,-0.11349424E-02,-0.54974917E-04, 0.19631056E-03,
         0.13730803E-03,-0.55059732E-03,-0.12894772E-02,-0.17091019E-04,
        -0.31562653E-04,-0.27324071E-04, 0.60996994E-04, 0.77762408E-04,
         0.47601939E-05, 0.19479805E-05,-0.19254896E-04,-0.96113063E-05,
        -0.66539928E-05,-0.13133385E-04,-0.20182833E-04, 0.11572294E-04,
        -0.24390842E-04, 0.33045482E-04,-0.74784075E-04,-0.59802122E-04,
        -0.62153987E-06,-0.83017694E-05,-0.52809382E-05,-0.22274469E-05,
         0.84336152E-05,-0.31321351E-05,-0.46717905E-05, 0.33244587E-05,
         0.35347414E-05, 0.30124193E-05, 0.45552219E-05,-0.33533620E-05,
         0.16510261E-04, 0.77860177E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_700                              =v_t_e_q2ex_2_700                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_700                              =v_t_e_q2ex_2_700                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11    *x32    *x52
        +coeff[ 25]        *x31*x41    
    ;
    v_t_e_q2ex_2_700                              =v_t_e_q2ex_2_700                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]*x12            *x51
        +coeff[ 29]*x11            *x52
        +coeff[ 30]    *x22*x31*x41    
        +coeff[ 31]*x11    *x32    *x51
        +coeff[ 32]*x12*x23            
        +coeff[ 33]*x11*x23    *x41    
        +coeff[ 34]*x11*x22*x31*x41    
    ;
    v_t_e_q2ex_2_700                              =v_t_e_q2ex_2_700                              
        +coeff[ 35]*x11*x22    *x42    
        +coeff[ 36]                *x51
        +coeff[ 37]    *x21*x31        
        +coeff[ 38]    *x21    *x41    
        +coeff[ 39]                *x52
        +coeff[ 40]*x12*x21            
        +coeff[ 41]            *x43    
        +coeff[ 42]    *x22        *x51
        +coeff[ 43]        *x32    *x51
    ;
    v_t_e_q2ex_2_700                              =v_t_e_q2ex_2_700                              
        +coeff[ 44]    *x21    *x41*x51
        +coeff[ 45]            *x41*x52
        +coeff[ 46]                *x53
        +coeff[ 47]*x11*x23            
        +coeff[ 48]*x12*x21*x31        
        +coeff[ 49]*x12*x21    *x41    
        ;

    return v_t_e_q2ex_2_700                              ;
}
float y_e_q2ex_2_700                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.4276862E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.49071788E-03, 0.20438133E+00, 0.10959391E+00, 0.37850847E-03,
        -0.10199517E-02,-0.96942758E-03,-0.68821886E-03,-0.73452340E-03,
         0.60424492E-04,-0.17285884E-04,-0.45131260E-03,-0.12846403E-03,
        -0.47398635E-03,-0.41082589E-04,-0.64158594E-04,-0.43699736E-03,
        -0.11048754E-03, 0.83880521E-04,-0.51783310E-03,-0.73390506E-03,
         0.84509404E-04,-0.59011618E-05, 0.19266869E-04,-0.22393033E-03,
        -0.12776347E-04, 0.18472654E-04,-0.14884472E-04, 0.37745420E-04,
         0.63208085E-04, 0.76691933E-04,-0.18544296E-03,-0.76878880E-03,
        -0.77322591E-04,-0.15008614E-04, 0.81916929E-04, 0.10923095E-03,
         0.29164081E-04,-0.19850807E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_700                              =v_y_e_q2ex_2_700                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x23    *x41    
    ;
    v_y_e_q2ex_2_700                              =v_y_e_q2ex_2_700                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x32*x43    
        +coeff[ 21]    *x22*x33*x42    
        +coeff[ 22]    *x22*x34*x41    
        +coeff[ 23]    *x24*x33        
        +coeff[ 24]*x12        *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q2ex_2_700                              =v_y_e_q2ex_2_700                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]        *x31*x42*x51
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]        *x32*x41*x51
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]*x11        *x45    
        +coeff[ 34]            *x45*x51
    ;
    v_y_e_q2ex_2_700                              =v_y_e_q2ex_2_700                              
        +coeff[ 35]        *x31*x44*x51
        +coeff[ 36]    *x21*x31*x43*x51
        +coeff[ 37]    *x22    *x45    
        ;

    return v_y_e_q2ex_2_700                              ;
}
float p_e_q2ex_2_700                              (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.1130032E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.18372586E-04,-0.18565083E-01,-0.19839568E-01, 0.17314251E-02,
         0.29713544E-02,-0.60196774E-03, 0.27863793E-04,-0.57463237E-03,
        -0.33759847E-03,-0.15527895E-03,-0.17991280E-04, 0.86618702E-05,
        -0.85005404E-05,-0.36970637E-03,-0.64746753E-04,-0.31024544E-03,
        -0.10666709E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_700                              =v_p_e_q2ex_2_700                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x24*x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]        *x31    *x52
        ;

    return v_p_e_q2ex_2_700                              ;
}
float l_e_q2ex_2_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2931843E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.28375499E-02,-0.47214199E-02,-0.11416426E-02,-0.45751762E-02,
        -0.54989639E-02,-0.27219788E-03, 0.71722653E-03, 0.15874561E-04,
        -0.24736868E-03, 0.21563837E-03, 0.57269889E-03, 0.60975639E-03,
         0.35553126E-03, 0.54483430E-03,-0.37053257E-03, 0.40763777E-03,
        -0.10026098E-02, 0.60952711E-03, 0.32491668E-03, 0.80183090E-04,
         0.18254966E-02,-0.15451686E-02, 0.64005191E-03, 0.18270459E-03,
        -0.19386278E-02,-0.13350867E-03,-0.61724643E-03,-0.62264787E-03,
        -0.82807208E-03, 0.54967741E-03,-0.16899781E-02,-0.25102680E-02,
         0.14371304E-03,-0.14213114E-02,-0.76562969E-03,-0.38206927E-03,
        -0.14618005E-02,-0.12038478E-03, 0.10452046E-02, 0.73910126E-03,
         0.88890316E-03,-0.35013556E-02, 0.74131975E-04, 0.10296809E-02,
        -0.25380861E-02,-0.46470016E-02,-0.79559343E-03, 0.34922609E-03,
        -0.86836862E-05, 0.30075625E-03,-0.18097785E-03,-0.20943278E-03,
         0.43693089E-03, 0.66947110E-03, 0.52390777E-03,-0.70365396E-03,
         0.13842680E-03, 0.20647419E-03,-0.26806915E-03, 0.31567022E-03,
         0.34457393E-03, 0.20027206E-03,-0.51732996E-03,-0.88114430E-04,
         0.36984985E-03, 0.48832939E-03, 0.34314726E-03, 0.19925799E-03,
         0.21292240E-03,-0.55494549E-03, 0.44988349E-03,-0.50179218E-03,
        -0.30817610E-03, 0.83546585E-03,-0.27646546E-03, 0.38094094E-03,
         0.70149760E-03,-0.80427236E-03,-0.77084202E-03, 0.10732813E-02,
        -0.34813111E-03, 0.59325277E-04,-0.45553182E-03, 0.36944193E-03,
         0.14815874E-03,-0.21306044E-03, 0.23349098E-03, 0.31877487E-03,
         0.35088052E-03, 0.17786844E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]*x11*x24    *x41    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]            *x43*x51
        +coeff[ 10]        *x31*x41*x52
        +coeff[ 11]*x11*x21*x31*x41    
        +coeff[ 12]*x11    *x32*x41    
        +coeff[ 13]*x12        *x42    
        +coeff[ 14]*x12*x21        *x51
        +coeff[ 15]*x12            *x52
        +coeff[ 16]*x11*x24            
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 17]*x11        *x44    
        +coeff[ 18]*x13*x22            
        +coeff[ 19]    *x23    *x42*x51
        +coeff[ 20]    *x21*x32*x41*x52
        +coeff[ 21]    *x21    *x43*x52
        +coeff[ 22]    *x21*x31    *x54
        +coeff[ 23]*x11*x23*x32        
        +coeff[ 24]*x11    *x32*x43    
        +coeff[ 25]*x11    *x31*x44    
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 26]*x12*x24            
        +coeff[ 27]*x12*x22    *x41*x51
        +coeff[ 28]*x12            *x54
        +coeff[ 29]*x13*x21*x32        
        +coeff[ 30]    *x21*x31*x44*x51
        +coeff[ 31]    *x23*x31*x41*x52
        +coeff[ 32]    *x21*x31*x43*x52
        +coeff[ 33]    *x21    *x43*x53
        +coeff[ 34]        *x33    *x54
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 35]        *x32*x41*x54
        +coeff[ 36]*x11    *x32*x41*x53
        +coeff[ 37]*x11        *x43*x53
        +coeff[ 38]*x12*x22*x31    *x52
        +coeff[ 39]*x13        *x44    
        +coeff[ 40]    *x21*x33*x44    
        +coeff[ 41]*x13*x21*x31*x41*x51
        +coeff[ 42]*x13    *x32*x41*x51
        +coeff[ 43]*x13        *x43*x51
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 44]    *x23*x32*x41*x52
        +coeff[ 45]    *x23*x31*x42*x52
        +coeff[ 46]*x13*x21        *x53
        +coeff[ 47]    *x24        *x54
        +coeff[ 48]*x11                
        +coeff[ 49]    *x21    *x42    
        +coeff[ 50]    *x22        *x51
        +coeff[ 51]    *x21*x31    *x51
        +coeff[ 52]        *x31*x41*x51
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 53]            *x42*x51
        +coeff[ 54]*x11*x22            
        +coeff[ 55]*x11        *x42    
        +coeff[ 56]*x12    *x31        
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]            *x44    
        +coeff[ 59]    *x21    *x42*x51
        +coeff[ 60]    *x21    *x41*x52
        +coeff[ 61]            *x42*x52
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 62]*x11    *x31*x42    
        +coeff[ 63]*x11*x22        *x51
        +coeff[ 64]*x11    *x32    *x51
        +coeff[ 65]*x11    *x31*x41*x51
        +coeff[ 66]*x11        *x42*x51
        +coeff[ 67]*x12    *x32        
        +coeff[ 68]*x12    *x31*x41    
        +coeff[ 69]    *x24*x31        
        +coeff[ 70]    *x22*x33        
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 71]    *x21*x33*x41    
        +coeff[ 72]    *x22    *x43    
        +coeff[ 73]    *x21*x31*x43    
        +coeff[ 74]*x13            *x51
        +coeff[ 75]    *x23*x31    *x51
        +coeff[ 76]    *x21*x33    *x51
        +coeff[ 77]    *x22*x31*x41*x51
        +coeff[ 78]            *x44*x51
        +coeff[ 79]    *x21*x31*x41*x52
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 80]        *x31*x42*x52
        +coeff[ 81]            *x43*x52
        +coeff[ 82]    *x21*x31    *x53
        +coeff[ 83]    *x21    *x41*x53
        +coeff[ 84]        *x31    *x54
        +coeff[ 85]            *x41*x54
        +coeff[ 86]*x11*x23*x31        
        +coeff[ 87]*x11*x21*x32*x41    
        +coeff[ 88]*x11*x23        *x51
    ;
    v_l_e_q2ex_2_700                              =v_l_e_q2ex_2_700                              
        +coeff[ 89]*x11*x21*x31*x41*x51
        ;

    return v_l_e_q2ex_2_700                              ;
}
float x_e_q2ex_2_600                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.1733812E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.13642412E-03,-0.46719774E-02, 0.19590139E+00, 0.95285121E-02,
         0.43504359E-03,-0.15169324E-02,-0.38184761E-02,-0.28189817E-02,
         0.13966485E-04,-0.62294453E-06,-0.10052847E-04,-0.17970559E-02,
        -0.49488532E-03,-0.23352201E-02,-0.20810037E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_600                              =v_x_e_q2ex_2_600                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_600                              ;
}
float t_e_q2ex_2_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4763135E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.37568734E-04,-0.25095600E-02, 0.54558001E-01, 0.25234257E-02,
        -0.12856157E-02,-0.13677804E-02,-0.48069868E-03, 0.18433045E-03,
        -0.49186696E-03,-0.53553842E-03,-0.20318788E-03,-0.16440471E-02,
        -0.12453853E-02,-0.11183413E-02,-0.49953705E-04, 0.16525827E-03,
         0.11596616E-03,-0.54213038E-03,-0.12307316E-02, 0.34735185E-04,
        -0.50501247E-04,-0.35459194E-04,-0.11223308E-04,-0.11418684E-04,
         0.77316317E-05,-0.19719571E-04, 0.55708184E-04,-0.23501647E-04,
         0.13609269E-04, 0.78292805E-05,-0.56207260E-04, 0.78770718E-04,
         0.45211731E-04, 0.48699014E-04,-0.20750228E-05,-0.72133497E-06,
         0.21565827E-05, 0.48290185E-05, 0.34086306E-05,-0.47985027E-05,
        -0.12450165E-04, 0.20034252E-05,-0.37299972E-05, 0.32863179E-05,
        -0.76784208E-05, 0.35831201E-05, 0.10651253E-04,-0.11293499E-04,
        -0.16335991E-04,-0.98073233E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_600                              =v_t_e_q2ex_2_600                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_600                              =v_t_e_q2ex_2_600                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x11*x22*x32        
        +coeff[ 23]*x11    *x32    *x52
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]    *x21*x33        
    ;
    v_t_e_q2ex_2_600                              =v_t_e_q2ex_2_600                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x11*x22*x31    *x51
        +coeff[ 29]    *x22        *x53
        +coeff[ 30]*x11*x22*x31*x42    
        +coeff[ 31]    *x21*x31*x43*x51
        +coeff[ 32]    *x23        *x53
        +coeff[ 33]    *x21    *x42*x53
        +coeff[ 34]    *x22            
    ;
    v_t_e_q2ex_2_600                              =v_t_e_q2ex_2_600                              
        +coeff[ 35]*x11    *x31        
        +coeff[ 36]            *x42    
        +coeff[ 37]*x13                
        +coeff[ 38]*x12    *x31        
        +coeff[ 39]*x11*x21*x31        
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]    *x22    *x41    
        +coeff[ 42]    *x21    *x41*x51
        +coeff[ 43]        *x31*x41*x51
    ;
    v_t_e_q2ex_2_600                              =v_t_e_q2ex_2_600                              
        +coeff[ 44]*x11            *x52
        +coeff[ 45]            *x41*x52
        +coeff[ 46]*x11*x22*x31        
        +coeff[ 47]*x11*x21*x32        
        +coeff[ 48]*x11*x21*x31*x41    
        +coeff[ 49]*x11    *x32*x41    
        ;

    return v_t_e_q2ex_2_600                              ;
}
float y_e_q2ex_2_600                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.8540390E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.94250566E-03, 0.20425583E+00, 0.10953006E+00, 0.41146786E-03,
        -0.10033675E-02,-0.10673341E-02,-0.61399094E-03,-0.75043435E-03,
        -0.46996234E-03, 0.55331784E-04,-0.10965217E-03,-0.48410695E-03,
        -0.11100024E-04,-0.58646350E-04, 0.33976554E-04,-0.53809275E-03,
        -0.17575410E-03,-0.41145504E-04,-0.60516206E-03,-0.46358356E-03,
        -0.19031379E-03,-0.12132175E-03,-0.28057731E-03,-0.89517147E-04,
         0.66254361E-05, 0.64639178E-04, 0.66309149E-04,-0.53673942E-03,
        -0.21908145E-04,-0.88501904E-04,-0.17949644E-04, 0.20904763E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_600                              =v_y_e_q2ex_2_600                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]    *x24*x32*x41    
    ;
    v_y_e_q2ex_2_600                              =v_y_e_q2ex_2_600                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]    *x22    *x45    
        +coeff[ 23]    *x24*x31*x42    
        +coeff[ 24]        *x31*x41    
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q2ex_2_600                              =v_y_e_q2ex_2_600                              
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]    *x21    *x42*x52
        +coeff[ 31]            *x45*x51
        ;

    return v_y_e_q2ex_2_600                              ;
}
float p_e_q2ex_2_600                              (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.6277129E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.71604925E-04,-0.18600756E-01,-0.19889351E-01, 0.17334345E-02,
         0.29734224E-02,-0.60078979E-03, 0.22230213E-04,-0.57653082E-03,
        -0.34126616E-03,-0.15484837E-03,-0.12356923E-04,-0.80760874E-05,
         0.25812399E-05,-0.36463371E-03,-0.62616797E-04,-0.31295605E-03,
        -0.10293946E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_600                              =v_p_e_q2ex_2_600                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x24*x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]        *x31    *x52
        ;

    return v_p_e_q2ex_2_600                              ;
}
float l_e_q2ex_2_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2929622E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29238083E-02,-0.45373258E-02,-0.97660930E-03,-0.47809775E-02,
        -0.56840461E-02, 0.22562945E-04,-0.13692868E-03, 0.17861613E-02,
        -0.34668367E-05,-0.98993223E-05, 0.19343720E-03, 0.17667937E-04,
         0.34323067E-03, 0.18693532E-04, 0.28627008E-03,-0.66125696E-03,
         0.43093934E-03, 0.32777339E-03, 0.65482984E-03,-0.40354210E-03,
        -0.65501378E-03, 0.18685579E-03,-0.10674223E-02,-0.26316277E-03,
         0.26557071E-03,-0.14450151E-02, 0.10079899E-02,-0.38524417E-03,
         0.36907120E-03,-0.88028301E-03,-0.21928845E-02, 0.12853214E-02,
         0.30052851E-03,-0.15999250E-03,-0.14830211E-02,-0.17995002E-03,
         0.75049332E-03,-0.16913417E-03, 0.32244481E-02,-0.12546086E-02,
        -0.10596417E-02, 0.17642574E-02,-0.53053111E-03, 0.10012400E-02,
        -0.15046911E-02, 0.16123042E-02,-0.11588601E-03,-0.93529769E-03,
        -0.94285735E-03,-0.47043199E-02, 0.19833520E-03, 0.26055826E-02,
        -0.17238297E-02, 0.26667435E-02,-0.21971788E-02, 0.22026263E-02,
         0.83585625E-03,-0.84305974E-03,-0.47185487E-03, 0.13264708E-03,
         0.14353120E-03,-0.45543603E-03,-0.96067335E-04,-0.44265529E-04,
        -0.31001118E-03, 0.14530093E-03, 0.27641858E-03,-0.10877442E-03,
        -0.25571595E-03,-0.18177991E-03, 0.15431223E-03,-0.16899339E-03,
        -0.13090152E-02, 0.11153044E-02,-0.16629949E-04,-0.90471975E-03,
         0.22442856E-03, 0.34013251E-03,-0.35963728E-03,-0.31677965E-03,
         0.18145115E-03,-0.39384446E-04, 0.12328950E-03,-0.20218386E-03,
         0.32710956E-03,-0.37519526E-03, 0.94635703E-03,-0.94173657E-03,
         0.18084962E-03,-0.83077268E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]        *x33        
        +coeff[  7]    *x24*x32        
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[  8]    *x21        *x51
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]                *x53
        +coeff[ 14]*x11        *x41*x51
        +coeff[ 15]    *x24            
        +coeff[ 16]    *x21*x33        
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 17]    *x21*x32*x41    
        +coeff[ 18]    *x22        *x52
        +coeff[ 19]    *x21    *x41*x52
        +coeff[ 20]*x11    *x32*x41    
        +coeff[ 21]*x11        *x43    
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]*x12*x21    *x41    
        +coeff[ 24]*x13*x21            
        +coeff[ 25]        *x32*x42*x51
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 26]        *x31*x43*x51
        +coeff[ 27]    *x23        *x52
        +coeff[ 28]            *x42*x53
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x11    *x31    *x53
        +coeff[ 34]*x12*x21*x31*x41    
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]*x12        *x42*x51
        +coeff[ 37]    *x22*x33*x41    
        +coeff[ 38]    *x22*x32*x42    
        +coeff[ 39]    *x23    *x41*x52
        +coeff[ 40]    *x22        *x54
        +coeff[ 41]    *x21    *x41*x54
        +coeff[ 42]*x11*x21*x31*x42*x51
        +coeff[ 43]*x11*x21    *x43*x51
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 44]    *x24    *x42*x51
        +coeff[ 45]        *x34*x42*x51
        +coeff[ 46]    *x23*x32    *x52
        +coeff[ 47]    *x22*x31*x41*x53
        +coeff[ 48]    *x21*x32    *x54
        +coeff[ 49]*x11*x23*x31*x42    
        +coeff[ 50]*x11*x22*x32*x42    
        +coeff[ 51]*x11*x21*x33*x42    
        +coeff[ 52]*x11*x23    *x43    
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 53]*x11*x22*x31*x43    
        +coeff[ 54]*x11*x23*x31*x41*x51
        +coeff[ 55]*x11*x21*x33*x41*x51
        +coeff[ 56]*x11*x21*x32    *x53
        +coeff[ 57]*x12*x23*x32        
        +coeff[ 58]*x12*x22        *x53
        +coeff[ 59]    *x21    *x41    
        +coeff[ 60]    *x21*x32        
        +coeff[ 61]    *x22    *x41    
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 62]        *x32*x41    
        +coeff[ 63]    *x21    *x42    
        +coeff[ 64]        *x31*x42    
        +coeff[ 65]*x11*x21*x31        
        +coeff[ 66]*x11*x21    *x41    
        +coeff[ 67]*x11*x21        *x51
        +coeff[ 68]*x11    *x31    *x51
        +coeff[ 69]*x12*x21            
        +coeff[ 70]*x12    *x31        
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 71]    *x23*x31        
        +coeff[ 72]    *x22*x32        
        +coeff[ 73]    *x22*x31*x41    
        +coeff[ 74]        *x33*x41    
        +coeff[ 75]        *x32*x42    
        +coeff[ 76]    *x21*x31*x41*x51
        +coeff[ 77]    *x21        *x53
        +coeff[ 78]*x11    *x33        
        +coeff[ 79]*x11    *x32    *x51
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 80]*x11        *x41*x52
        +coeff[ 81]*x11            *x53
        +coeff[ 82]*x12    *x32        
        +coeff[ 83]*x12*x21        *x51
        +coeff[ 84]    *x23*x32        
        +coeff[ 85]    *x22*x33        
        +coeff[ 86]    *x24    *x41    
        +coeff[ 87]    *x22*x32*x41    
        +coeff[ 88]*x13            *x51
    ;
    v_l_e_q2ex_2_600                              =v_l_e_q2ex_2_600                              
        +coeff[ 89]    *x22*x31*x41*x51
        ;

    return v_l_e_q2ex_2_600                              ;
}
float x_e_q2ex_2_500                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.2008740E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.20006204E-02,-0.46697590E-02, 0.19610041E+00, 0.95206974E-02,
         0.44354954E-03,-0.14981063E-02,-0.38208247E-02,-0.28128233E-02,
         0.39334682E-05,-0.38920425E-05, 0.53496037E-05,-0.17886392E-02,
        -0.49516448E-03,-0.23084420E-02,-0.20918436E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_500                              =v_x_e_q2ex_2_500                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_500                              ;
}
float t_e_q2ex_2_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5521032E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54947153E-03,-0.25048715E-02, 0.54656535E-01, 0.25006048E-02,
        -0.12810248E-02,-0.13412560E-02,-0.51184202E-03, 0.18292168E-03,
        -0.47087768E-03,-0.50893391E-03,-0.20215583E-03,-0.16682027E-02,
        -0.12895016E-02,-0.11481817E-02,-0.57037192E-04, 0.17745885E-03,
         0.14677955E-03,-0.53731503E-03,-0.13007113E-02, 0.31747953E-04,
        -0.36027854E-04,-0.25171976E-04,-0.12190221E-04,-0.91391630E-05,
        -0.19995965E-04,-0.73518086E-05, 0.97859429E-05,-0.12910818E-04,
        -0.35798014E-05,-0.53071144E-05, 0.28827666E-04, 0.14862383E-04,
         0.74821524E-04, 0.15286185E-04, 0.30476305E-04,-0.82476749E-04,
        -0.67020010E-04, 0.10523429E-04,-0.16426975E-04, 0.30470079E-04,
        -0.32668700E-04,-0.17312230E-04, 0.37994494E-04,-0.22699145E-04,
         0.23714876E-04, 0.31482748E-04, 0.64601234E-04,-0.11838193E-04,
         0.40524283E-05,-0.29432210E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_500                              =v_t_e_q2ex_2_500                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q2ex_2_500                              =v_t_e_q2ex_2_500                              
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]*x11            *x52
        +coeff[ 23]    *x21    *x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22    *x41    
    ;
    v_t_e_q2ex_2_500                              =v_t_e_q2ex_2_500                              
        +coeff[ 26]    *x22        *x51
        +coeff[ 27]*x12*x21    *x41    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11        *x43    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11*x22        *x51
        +coeff[ 32]    *x21*x32    *x51
        +coeff[ 33]*x13*x22            
        +coeff[ 34]*x11*x23*x31        
    ;
    v_t_e_q2ex_2_500                              =v_t_e_q2ex_2_500                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x11*x23        *x51
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]*x12*x23        *x51
        +coeff[ 40]*x12*x21*x32    *x51
        +coeff[ 41]*x13*x21    *x41*x51
        +coeff[ 42]*x12*x22    *x41*x51
        +coeff[ 43]    *x22*x32*x41*x51
    ;
    v_t_e_q2ex_2_500                              =v_t_e_q2ex_2_500                              
        +coeff[ 44]    *x23    *x42*x51
        +coeff[ 45]    *x23        *x53
        +coeff[ 46]    *x21*x31*x41*x53
        +coeff[ 47]*x11*x21*x31        
        +coeff[ 48]        *x32*x41    
        +coeff[ 49]*x11    *x31    *x51
        ;

    return v_t_e_q2ex_2_500                              ;
}
float y_e_q2ex_2_500                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.5028390E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.45682892E-03, 0.20419708E+00, 0.10943018E+00, 0.41498253E-03,
        -0.10011715E-02,-0.12205320E-02,-0.79766725E-03,-0.85284287E-03,
        -0.22525250E-04, 0.93087219E-04,-0.57481369E-03,-0.12910130E-03,
        -0.46802897E-03,-0.19037101E-04,-0.58346610E-04, 0.55321205E-04,
        -0.42402960E-03,-0.28848266E-04, 0.70946509E-04,-0.36685981E-03,
        -0.22812094E-05, 0.29604162E-05, 0.11671557E-05, 0.70961585E-04,
         0.12326321E-04, 0.43975015E-04,-0.12067300E-03, 0.45963789E-04,
        -0.73745810E-04,-0.10498138E-03,-0.11402951E-03, 0.31448151E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_500                              =v_y_e_q2ex_2_500                              
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_500                              =v_y_e_q2ex_2_500                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]        *x31*x41    
        +coeff[ 22]            *x43*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]            *x44*x51
        +coeff[ 25]        *x33*x42    
    ;
    v_y_e_q2ex_2_500                              =v_y_e_q2ex_2_500                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11*x21*x31*x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]    *x22*x31*x43    
        ;

    return v_y_e_q2ex_2_500                              ;
}
float p_e_q2ex_2_500                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.2914870E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.24711859E-04,-0.18683167E-01,-0.19975202E-01, 0.17353391E-02,
         0.29771510E-02,-0.60296053E-03, 0.26169890E-04,-0.36778240E-03,
        -0.57178503E-03,-0.33887691E-03,-0.14922184E-03,-0.44157696E-05,
        -0.15666516E-03,-0.31407870E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_500                              =v_p_e_q2ex_2_500                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_500                              ;
}
float l_e_q2ex_2_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2931077E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.28923615E-02,-0.50178207E-02,-0.14237677E-02,-0.44181347E-02,
        -0.54116333E-02, 0.19025117E-03,-0.15184615E-02, 0.67384892E-04,
         0.17342760E-02,-0.71217342E-04, 0.23472925E-03,-0.24096067E-04,
        -0.65348278E-04, 0.10237653E-03,-0.18963426E-03,-0.12172532E-03,
         0.42371408E-03, 0.18171227E-03,-0.51412923E-03, 0.82859083E-03,
        -0.39188453E-03,-0.41363543E-03,-0.22744371E-03, 0.84603339E-03,
         0.93148870E-03,-0.39225807E-02, 0.45437264E-03,-0.64614398E-03,
         0.10593289E-02, 0.68795454E-03, 0.59469341E-03, 0.21682694E-02,
         0.87832747E-03, 0.17893618E-02, 0.18672079E-03,-0.12397508E-02,
         0.10704268E-02, 0.94140298E-03, 0.25332565E-03,-0.19314549E-02,
        -0.49682806E-03, 0.73931983E-03,-0.10525906E-02,-0.62229508E-03,
        -0.53112663E-03,-0.80926932E-03,-0.55702304E-03, 0.19596156E-02,
        -0.32878784E-03, 0.27962381E-02, 0.80347998E-03, 0.17933416E-02,
        -0.81069935E-04, 0.92781534E-04, 0.26426304E-02,-0.59039565E-03,
        -0.42153029E-02,-0.26214139E-02, 0.24315966E-02, 0.34450463E-02,
         0.10860983E-02,-0.26454022E-02,-0.14662473E-03,-0.72163891E-03,
        -0.81763137E-04,-0.99386561E-04, 0.45338619E-03,-0.78090350E-04,
         0.12579588E-03,-0.13904976E-03, 0.98830868E-04, 0.50857063E-04,
        -0.26709947E-03,-0.26540953E-03,-0.20626909E-03, 0.10864212E-03,
        -0.10189653E-03, 0.24220003E-03,-0.39133875E-03,-0.13834392E-03,
        -0.26798464E-03,-0.45632321E-03, 0.26707732E-03, 0.29980662E-03,
         0.26255386E-03,-0.83319224E-04, 0.26908785E-03, 0.22906918E-03,
        -0.37736632E-03, 0.19932337E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[  8]*x11*x21*x32    *x53
        +coeff[  9]        *x31        
        +coeff[ 10]                *x51
        +coeff[ 11]    *x21    *x41    
        +coeff[ 12]    *x21        *x51
        +coeff[ 13]            *x41*x51
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 17]*x12    *x31        
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43*x51
        +coeff[ 22]                *x54
        +coeff[ 23]*x11*x22        *x51
        +coeff[ 24]    *x24        *x51
        +coeff[ 25]    *x22*x31*x41*x51
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 26]*x11*x21*x33        
        +coeff[ 27]*x11*x22    *x41*x51
        +coeff[ 28]*x11        *x41*x53
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x12*x22        *x51
        +coeff[ 31]*x12    *x31*x41*x51
        +coeff[ 32]    *x22*x33*x41    
        +coeff[ 33]    *x23*x31*x41*x51
        +coeff[ 34]    *x23        *x53
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 35]    *x21*x31*x41*x53
        +coeff[ 36]        *x32*x41*x53
        +coeff[ 37]*x11*x24*x31        
        +coeff[ 38]*x11*x23*x32        
        +coeff[ 39]*x11*x22*x32    *x51
        +coeff[ 40]*x11*x21*x33    *x51
        +coeff[ 41]*x11    *x34    *x51
        +coeff[ 42]*x11*x21    *x42*x52
        +coeff[ 43]*x12    *x32    *x52
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 44]*x12        *x41*x53
        +coeff[ 45]*x13*x22*x31        
        +coeff[ 46]*x13*x21*x31*x41    
        +coeff[ 47]    *x24*x31*x41*x51
        +coeff[ 48]    *x21*x32*x43*x51
        +coeff[ 49]    *x21*x33*x41*x52
        +coeff[ 50]        *x34*x41*x52
        +coeff[ 51]    *x21*x32*x42*x52
        +coeff[ 52]*x13            *x53
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 53]    *x23    *x41*x53
        +coeff[ 54]    *x22*x31*x41*x53
        +coeff[ 55]*x11    *x34*x41*x51
        +coeff[ 56]*x11*x21*x32*x42*x51
        +coeff[ 57]*x11*x21*x31*x43*x51
        +coeff[ 58]*x12*x23    *x41*x51
        +coeff[ 59]*x12*x21*x31*x42*x51
        +coeff[ 60]*x12*x21    *x42*x52
        +coeff[ 61]*x12    *x31*x41*x53
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 62]    *x23*x33*x42    
        +coeff[ 63]*x13*x21*x31    *x52
        +coeff[ 64]    *x21            
        +coeff[ 65]*x11                
        +coeff[ 66]    *x21*x31        
        +coeff[ 67]        *x31    *x51
        +coeff[ 68]                *x52
        +coeff[ 69]*x11            *x51
        +coeff[ 70]    *x23            
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 71]    *x21*x32        
        +coeff[ 72]    *x21    *x42    
        +coeff[ 73]    *x21*x31    *x51
        +coeff[ 74]    *x21    *x41*x51
        +coeff[ 75]            *x42*x51
        +coeff[ 76]            *x41*x52
        +coeff[ 77]*x11    *x32        
        +coeff[ 78]*x11        *x41*x51
        +coeff[ 79]*x12            *x51
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 80]    *x23*x31        
        +coeff[ 81]    *x21*x33        
        +coeff[ 82]        *x34        
        +coeff[ 83]    *x21    *x43    
        +coeff[ 84]    *x23        *x51
        +coeff[ 85]    *x22*x31    *x51
        +coeff[ 86]        *x33    *x51
        +coeff[ 87]    *x22    *x41*x51
        +coeff[ 88]        *x32*x41*x51
    ;
    v_l_e_q2ex_2_500                              =v_l_e_q2ex_2_500                              
        +coeff[ 89]        *x32    *x52
        ;

    return v_l_e_q2ex_2_500                              ;
}
float x_e_q2ex_2_450                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.7295724E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.74398762E-03,-0.46675359E-02, 0.19622868E+00, 0.95178317E-02,
         0.44269496E-03,-0.15069435E-02,-0.38761266E-02,-0.28613443E-02,
        -0.40557363E-04,-0.46522164E-05,-0.17864330E-02,-0.49496238E-03,
        -0.21385492E-02,-0.19449984E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_450                              =v_x_e_q2ex_2_450                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_450                              ;
}
float t_e_q2ex_2_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1971085E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.20168240E-03,-0.25078787E-02, 0.54721717E-01, 0.25041453E-02,
        -0.13163931E-02,-0.13541548E-02,-0.53503120E-03, 0.18398504E-03,
        -0.46508294E-03,-0.50042145E-03,-0.18555706E-03,-0.16230784E-02,
        -0.12309925E-02,-0.66823173E-04, 0.21449858E-03, 0.17646170E-03,
        -0.12787397E-02,-0.10912057E-02, 0.16920811E-04,-0.17025686E-04,
        -0.21707319E-04,-0.12027363E-04,-0.33581862E-04,-0.53015491E-03,
         0.38222315E-05, 0.44157255E-05,-0.82736224E-05,-0.52389323E-05,
         0.11393021E-04, 0.56079698E-04, 0.26413467E-04,-0.20392174E-04,
         0.49178952E-05, 0.26932059E-04, 0.97553211E-05,-0.73973439E-04,
        -0.24605546E-04,-0.51418003E-04, 0.12272735E-04,-0.27334303E-04,
         0.12441174E-04,-0.48114220E-04,-0.45548684E-04, 0.25362384E-04,
        -0.43734326E-04, 0.33017903E-04, 0.37810696E-04, 0.34564924E-04,
        -0.49499013E-04, 0.71380409E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_450                              =v_t_e_q2ex_2_450                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_450                              =v_t_e_q2ex_2_450                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x11            *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]        *x31*x41    
        +coeff[ 25]*x13                
    ;
    v_t_e_q2ex_2_450                              =v_t_e_q2ex_2_450                              
        +coeff[ 26]*x12*x21            
        +coeff[ 27]            *x41*x52
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x21    *x41*x51
        +coeff[ 31]*x11*x21        *x52
        +coeff[ 32]    *x21        *x53
        +coeff[ 33]*x13*x22            
        +coeff[ 34]*x13*x21*x31        
    ;
    v_t_e_q2ex_2_450                              =v_t_e_q2ex_2_450                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]*x11    *x33*x41    
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]*x13*x22*x31        
        +coeff[ 41]*x13*x21*x31    *x51
        +coeff[ 42]*x11*x23    *x41*x51
        +coeff[ 43]    *x23    *x42*x51
    ;
    v_t_e_q2ex_2_450                              =v_t_e_q2ex_2_450                              
        +coeff[ 44]*x11*x21    *x43*x51
        +coeff[ 45]*x11*x21*x31*x41*x52
        +coeff[ 46]    *x23        *x53
        +coeff[ 47]*x11*x21*x31    *x53
        +coeff[ 48]    *x21    *x42*x53
        +coeff[ 49]*x11*x21            
        ;

    return v_t_e_q2ex_2_450                              ;
}
float y_e_q2ex_2_450                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.7423265E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.71839627E-03, 0.20410612E+00, 0.10932693E+00, 0.41721849E-03,
        -0.10033698E-02,-0.10255395E-02,-0.60095510E-03,-0.72695053E-03,
        -0.50265994E-03, 0.96735057E-04,-0.11433329E-03,-0.53891784E-03,
        -0.41455369E-04,-0.66489330E-04,-0.14094063E-04,-0.55080710E-03,
        -0.53701195E-03,-0.53587528E-05, 0.64153195E-04, 0.93939889E-05,
         0.66189081E-04, 0.60232131E-04, 0.44838704E-04, 0.22955946E-04,
        -0.22229962E-04,-0.73749328E-03,-0.58454939E-03, 0.17205180E-04,
        -0.19517275E-03,-0.94360526E-04,-0.46981881E-04,-0.34884732E-04,
         0.20883492E-04,-0.81601516E-04, 0.18061399E-04,-0.29981908E-03,
        -0.38660033E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_450                              =v_y_e_q2ex_2_450                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_450                              =v_y_e_q2ex_2_450                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]        *x32*x41*x51
        +coeff[ 23]        *x32*x43    
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q2ex_2_450                              =v_y_e_q2ex_2_450                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x11*x21*x32*x41    
        +coeff[ 31]*x12    *x31*x42    
        +coeff[ 32]    *x21    *x42*x52
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x12        *x44    
    ;
    v_y_e_q2ex_2_450                              =v_y_e_q2ex_2_450                              
        +coeff[ 35]    *x22    *x45    
        +coeff[ 36]*x11*x21    *x43*x51
        ;

    return v_y_e_q2ex_2_450                              ;
}
float p_e_q2ex_2_450                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.9425959E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.92152506E-04,-0.18720485E-01,-0.20031372E-01, 0.17360098E-02,
         0.29793978E-02,-0.59807423E-03,-0.35791160E-03,-0.56979858E-03,
        -0.33517394E-03,-0.15050736E-03,-0.18678955E-04,-0.71539928E-03,
         0.73522999E-03,-0.62100095E-04,-0.30487910E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_450                              =v_p_e_q2ex_2_450                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]    *x22*x31    *x52
        +coeff[ 12]    *x24*x31    *x52
        +coeff[ 13]        *x33        
        +coeff[ 14]        *x32*x41    
        ;

    return v_p_e_q2ex_2_450                              ;
}
float l_e_q2ex_2_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2969771E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.27887814E-02,-0.47629899E-02,-0.46216426E-03,-0.47072642E-02,
        -0.54614427E-02,-0.59417536E-03,-0.23019791E-03,-0.15085076E-02,
        -0.16201191E-02, 0.20736165E-03, 0.15272091E-04,-0.18091859E-03,
         0.18858517E-05,-0.48773552E-04, 0.33944848E-03, 0.39664804E-03,
        -0.28365784E-03,-0.16497137E-03, 0.22699905E-03, 0.23157909E-03,
         0.39529442E-03,-0.35217009E-03,-0.38421626E-03,-0.42510554E-03,
        -0.27823981E-03, 0.17560645E-02, 0.54384780E-03,-0.10826057E-03,
         0.97218732E-03,-0.94747345E-03, 0.12574191E-02, 0.97373303E-03,
         0.33861492E-03,-0.20537262E-02, 0.20632022E-02,-0.10833401E-02,
         0.78311446E-03, 0.43472653E-03,-0.18723166E-02,-0.92490902E-03,
         0.12462537E-02,-0.35746896E-03, 0.59430971E-03, 0.11123334E-02,
        -0.50112844E-03,-0.22363910E-02, 0.10335104E-02,-0.59657323E-03,
        -0.12468118E-02,-0.86073887E-05, 0.46317945E-02, 0.25133602E-02,
        -0.38221704E-02,-0.23755603E-02, 0.76104462E-03,-0.98706211E-03,
         0.14088855E-02, 0.29202863E-02, 0.11740851E-02, 0.15019793E-02,
        -0.14477756E-02, 0.84815576E-03,-0.69920941E-04,-0.13089916E-03,
        -0.50329544E-06, 0.84184525E-04,-0.71022245E-04, 0.13637703E-03,
         0.10256192E-02,-0.19807354E-03,-0.21814433E-03,-0.28487018E-03,
         0.17360145E-03,-0.25777697E-04,-0.12069807E-03,-0.96557633E-04,
         0.11001259E-03, 0.12442123E-03, 0.20384492E-03, 0.59384666E-03,
         0.18650059E-03, 0.84355248E-04,-0.47852093E-03, 0.53170597E-03,
         0.41776954E-03, 0.52751444E-03, 0.30963754E-03, 0.13297313E-03,
        -0.53034618E-03, 0.49216545E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]*x11            *x52
        +coeff[  7]    *x22*x34    *x51
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[  8]*x11    *x32*x43*x51
        +coeff[  9]                *x51
        +coeff[ 10]*x11                
        +coeff[ 11]    *x21    *x41    
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]*x12                
        +coeff[ 14]    *x23            
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]            *x43    
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]            *x42*x51
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]*x11*x21        *x51
        +coeff[ 21]*x12            *x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]        *x34        
        +coeff[ 24]        *x32*x42    
        +coeff[ 25]*x11*x21*x32        
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 26]*x11*x21    *x42    
        +coeff[ 27]*x11    *x31*x42    
        +coeff[ 28]*x13*x21            
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]    *x21*x32*x41*x51
        +coeff[ 31]    *x21*x31*x42*x51
        +coeff[ 32]    *x21*x32    *x52
        +coeff[ 33]*x11*x21*x31*x41*x51
        +coeff[ 34]*x11    *x31*x41*x52
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 35]*x11*x21        *x53
        +coeff[ 36]*x12    *x32    *x51
        +coeff[ 37]*x12    *x31*x41*x51
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x22        *x54
        +coeff[ 40]*x11*x21*x31*x41*x52
        +coeff[ 41]*x12*x22    *x42    
        +coeff[ 42]*x12    *x31*x43    
        +coeff[ 43]*x12*x22*x31    *x51
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 44]*x12    *x31    *x53
        +coeff[ 45]*x13*x21*x32        
        +coeff[ 46]*x13    *x31*x42    
        +coeff[ 47]*x13    *x32    *x51
        +coeff[ 48]    *x24*x31*x41*x51
        +coeff[ 49]*x13            *x53
        +coeff[ 50]*x11*x21*x33*x41*x51
        +coeff[ 51]*x11*x21*x32*x42*x51
        +coeff[ 52]*x11    *x33*x41*x52
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 53]*x11    *x32*x42*x52
        +coeff[ 54]*x11*x23        *x53
        +coeff[ 55]*x12*x23        *x52
        +coeff[ 56]*x12*x21    *x42*x52
        +coeff[ 57]    *x23    *x44*x51
        +coeff[ 58]    *x24    *x42*x52
        +coeff[ 59]    *x22*x33    *x53
        +coeff[ 60]*x13        *x41*x53
        +coeff[ 61]    *x23*x31    *x54
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 62]    *x21            
        +coeff[ 63]    *x21*x31        
        +coeff[ 64]        *x31    *x51
        +coeff[ 65]            *x41*x51
        +coeff[ 66]*x11    *x31        
        +coeff[ 67]*x11            *x51
        +coeff[ 68]    *x22    *x41    
        +coeff[ 69]        *x32*x41    
        +coeff[ 70]    *x21    *x42    
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 71]        *x31*x42    
        +coeff[ 72]        *x31*x41*x51
        +coeff[ 73]        *x31    *x52
        +coeff[ 74]            *x41*x52
        +coeff[ 75]                *x53
        +coeff[ 76]*x11*x22            
        +coeff[ 77]*x11*x21    *x41    
        +coeff[ 78]*x11        *x42    
        +coeff[ 79]*x11        *x41*x51
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 80]*x12    *x31        
        +coeff[ 81]*x12        *x41    
        +coeff[ 82]    *x23*x31        
        +coeff[ 83]    *x21*x33        
        +coeff[ 84]    *x22*x31*x41    
        +coeff[ 85]    *x21*x32*x41    
        +coeff[ 86]    *x21*x31*x42    
        +coeff[ 87]    *x23        *x51
        +coeff[ 88]    *x22*x31    *x51
    ;
    v_l_e_q2ex_2_450                              =v_l_e_q2ex_2_450                              
        +coeff[ 89]    *x22        *x52
        ;

    return v_l_e_q2ex_2_450                              ;
}
float x_e_q2ex_2_449                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.1769749E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.17474504E-02,-0.45835092E-02, 0.19810589E+00, 0.94070509E-02,
         0.42713838E-03,-0.12419607E-02,-0.37909334E-02,-0.26229806E-02,
         0.57307921E-05, 0.14579537E-04,-0.10939949E-02,-0.16059275E-02,
        -0.48604596E-03,-0.33695272E-02,-0.24578252E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_449                              =v_x_e_q2ex_2_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_449                              ;
}
float t_e_q2ex_2_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5008409E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.49501267E-03,-0.24694088E-02, 0.55333715E-01, 0.24265554E-02,
        -0.14276874E-02,-0.13293434E-02,-0.65422093E-03, 0.17973715E-03,
        -0.47429561E-03,-0.62853959E-03,-0.18732624E-03,-0.52824438E-04,
        -0.18532739E-02,-0.12773962E-02, 0.22333852E-03, 0.13878921E-03,
        -0.14986354E-02,-0.11902074E-02, 0.40605414E-05,-0.37492344E-04,
        -0.21608603E-04,-0.54267994E-05,-0.67984621E-03,-0.66722914E-05,
        -0.23900800E-04,-0.82360602E-05, 0.57471763E-04, 0.88262546E-04,
        -0.29810861E-04, 0.13784239E-04,-0.83632985E-04,-0.62433886E-04,
         0.19765434E-04,-0.26641181E-04,-0.15099040E-04, 0.47672253E-04,
         0.16978562E-04, 0.34806864E-04,-0.48968348E-04,-0.12359354E-05,
        -0.24291014E-05,-0.38740177E-05, 0.13351492E-04, 0.36585218E-05,
         0.88212000E-05, 0.11754594E-04, 0.69040402E-05,-0.63572311E-05,
        -0.21178312E-04,-0.17564515E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_449                              =v_t_e_q2ex_2_449                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_449                              =v_t_e_q2ex_2_449                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x22*x31        
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2ex_2_449                              =v_t_e_q2ex_2_449                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]    *x23*x31    *x51
        +coeff[ 34]*x12*x21        *x52
    ;
    v_t_e_q2ex_2_449                              =v_t_e_q2ex_2_449                              
        +coeff[ 35]*x13*x22    *x41    
        +coeff[ 36]*x12*x22    *x41*x51
        +coeff[ 37]*x12*x21*x31    *x52
        +coeff[ 38]    *x21*x32*x41*x52
        +coeff[ 39]            *x41    
        +coeff[ 40]                *x51
        +coeff[ 41]    *x22            
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_q2ex_2_449                              =v_t_e_q2ex_2_449                              
        +coeff[ 44]    *x21*x31    *x51
        +coeff[ 45]*x11*x22*x31        
        +coeff[ 46]    *x22*x32        
        +coeff[ 47]*x11    *x33        
        +coeff[ 48]*x11*x22    *x41    
        +coeff[ 49]    *x23    *x41    
        ;

    return v_t_e_q2ex_2_449                              ;
}
float y_e_q2ex_2_449                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.9585285E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.97150245E-03, 0.20390742E+00, 0.12113007E+00, 0.38789192E-03,
        -0.11231127E-02,-0.90845255E-03,-0.67219860E-03,-0.78900391E-03,
        -0.56209491E-03,-0.14785303E-03,-0.67753408E-05,-0.39050617E-03,
        -0.50996608E-04,-0.72727482E-04, 0.15119897E-04,-0.55697118E-03,
        -0.81816004E-04, 0.84128704E-04,-0.55518001E-03,-0.82136074E-03,
        -0.53192780E-04, 0.19329872E-03, 0.17230006E-03,-0.11355992E-03,
        -0.82654487E-05,-0.12576602E-04, 0.10243116E-03, 0.62794294E-04,
         0.74179727E-04, 0.18185672E-04,-0.48670670E-03,-0.95318805E-03,
        -0.19741403E-03,-0.68752954E-04, 0.44270495E-04,-0.33147004E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_449                              =v_y_e_q2ex_2_449                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_449                              =v_y_e_q2ex_2_449                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x31*x44    
        +coeff[ 21]    *x24    *x43    
        +coeff[ 22]    *x24*x31*x42    
        +coeff[ 23]    *x24*x33        
        +coeff[ 24]            *x44    
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q2ex_2_449                              =v_y_e_q2ex_2_449                              
        +coeff[ 26]        *x31*x42*x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]        *x32*x41*x51
        +coeff[ 29]        *x31*x44    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]            *x45*x51
    ;
    v_y_e_q2ex_2_449                              =v_y_e_q2ex_2_449                              
        +coeff[ 35]*x11*x21*x32*x42    
        ;

    return v_y_e_q2ex_2_449                              ;
}
float p_e_q2ex_2_449                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.6030013E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.61911291E-04,-0.20510335E-01,-0.19573675E-01, 0.19010971E-02,
         0.29468378E-02,-0.61033887E-03, 0.17578599E-04,-0.40058701E-03,
        -0.61623595E-03,-0.33234499E-03,-0.14529831E-03, 0.67614055E-05,
        -0.17389348E-03,-0.38130576E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_449                              =v_p_e_q2ex_2_449                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_449                              ;
}
float l_e_q2ex_2_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2912464E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29642419E-02,-0.49155182E-02,-0.12912859E-02,-0.47535747E-02,
        -0.57428605E-02,-0.12886467E-04,-0.32760701E-02, 0.10333375E-03,
         0.10695274E-03,-0.12493790E-02, 0.50039340E-04, 0.43600932E-03,
        -0.67373476E-03, 0.22576265E-02,-0.10723252E-02, 0.22657402E-02,
        -0.14644184E-04, 0.34607013E-03, 0.64635510E-03,-0.19412788E-03,
         0.90073317E-03, 0.11641773E-02, 0.10795365E-02, 0.10885661E-02,
        -0.12157797E-02,-0.83750871E-03, 0.32370162E-03, 0.80520287E-04,
        -0.24385401E-02, 0.11012716E-02, 0.76590903E-03,-0.20298845E-04,
        -0.10138216E-02, 0.17893085E-02,-0.12268140E-02,-0.93470979E-03,
        -0.11835145E-02,-0.79837206E-04, 0.12667954E-02,-0.12379881E-02,
        -0.91838418E-03, 0.93771657E-03,-0.54043351E-03,-0.23090337E-03,
         0.52937795E-03, 0.17874334E-02, 0.34620394E-02,-0.47439162E-03,
        -0.92212629E-03,-0.10079789E-02, 0.57523965E-03, 0.14318010E-02,
        -0.22595164E-02, 0.13761470E-02, 0.15385069E-02, 0.98866876E-03,
        -0.12082942E-02,-0.20924385E-02, 0.27699582E-02, 0.23862554E-03,
        -0.11056606E-03,-0.55707391E-04, 0.83450388E-04, 0.22016319E-03,
         0.16794671E-03,-0.41375108E-04,-0.15882846E-03,-0.55308919E-03,
         0.87200111E-04,-0.38103762E-03,-0.19546515E-03,-0.13629405E-03,
         0.11827411E-03,-0.11319148E-03,-0.67026354E-04, 0.57463982E-03,
        -0.13777822E-03, 0.17787782E-03, 0.77486955E-04, 0.58687201E-04,
        -0.25948236E-03,-0.27554834E-03,-0.35124351E-03,-0.55764266E-03,
        -0.35819868E-03, 0.34343428E-03,-0.28367451E-03, 0.24213891E-03,
         0.19591811E-03,-0.10900193E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x21*x33    *x52
        +coeff[  7]            *x41    
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[  8]                *x51
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x33        
        +coeff[ 11]*x12    *x31        
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]    *x21*x31    *x52
        +coeff[ 14]*x11    *x32*x41    
        +coeff[ 15]*x11*x21    *x42    
        +coeff[ 16]*x11*x22        *x51
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 17]*x11        *x42*x51
        +coeff[ 18]*x11    *x31    *x52
        +coeff[ 19]*x13    *x31        
        +coeff[ 20]    *x24*x31        
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]*x11*x21*x32*x41    
        +coeff[ 24]*x11    *x33    *x51
        +coeff[ 25]*x12    *x33        
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 26]*x12*x21    *x41*x51
        +coeff[ 27]*x13    *x31    *x51
        +coeff[ 28]    *x23*x31    *x52
        +coeff[ 29]        *x32*x41*x53
        +coeff[ 30]        *x31*x42*x53
        +coeff[ 31]*x11*x23*x32        
        +coeff[ 32]*x11*x24    *x41    
        +coeff[ 33]*x11    *x34*x41    
        +coeff[ 34]*x11*x23    *x42    
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 35]*x11*x21*x32*x42    
        +coeff[ 36]*x11*x21    *x44    
        +coeff[ 37]*x11*x22*x31*x41*x51
        +coeff[ 38]*x11*x22    *x42*x51
        +coeff[ 39]*x11        *x44*x51
        +coeff[ 40]*x11*x22*x31    *x52
        +coeff[ 41]*x12*x23        *x51
        +coeff[ 42]*x12*x21*x32    *x51
        +coeff[ 43]*x12*x21        *x53
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 44]*x13*x22    *x41    
        +coeff[ 45]    *x23*x32*x42    
        +coeff[ 46]    *x22*x33*x42    
        +coeff[ 47]    *x23    *x42*x52
        +coeff[ 48]        *x31*x44*x52
        +coeff[ 49]        *x32*x41*x54
        +coeff[ 50]            *x43*x54
        +coeff[ 51]*x11*x22*x31*x43    
        +coeff[ 52]*x11*x22*x31*x42*x51
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 53]*x12*x23*x31*x41    
        +coeff[ 54]*x12    *x33    *x52
        +coeff[ 55]*x13*x22    *x42    
        +coeff[ 56]*x13    *x32*x41*x51
        +coeff[ 57]    *x22*x34*x41*x51
        +coeff[ 58]    *x23*x33    *x52
        +coeff[ 59]        *x31        
        +coeff[ 60]    *x21*x31        
        +coeff[ 61]    *x21    *x41    
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 62]    *x21        *x51
        +coeff[ 63]        *x31    *x51
        +coeff[ 64]*x11        *x41    
        +coeff[ 65]*x12                
        +coeff[ 66]    *x23            
        +coeff[ 67]        *x32*x41    
        +coeff[ 68]            *x42*x51
        +coeff[ 69]        *x31    *x52
        +coeff[ 70]            *x41*x52
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 71]                *x53
        +coeff[ 72]*x11*x21*x31        
        +coeff[ 73]*x11*x21    *x41    
        +coeff[ 74]*x11        *x42    
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11            *x52
        +coeff[ 77]*x12*x21            
        +coeff[ 78]*x12            *x51
        +coeff[ 79]*x13                
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 80]    *x24            
        +coeff[ 81]        *x31*x43    
        +coeff[ 82]        *x33    *x51
        +coeff[ 83]        *x31*x42*x51
        +coeff[ 84]            *x43*x51
        +coeff[ 85]            *x42*x52
        +coeff[ 86]    *x21        *x53
        +coeff[ 87]        *x31    *x53
        +coeff[ 88]            *x41*x53
    ;
    v_l_e_q2ex_2_449                              =v_l_e_q2ex_2_449                              
        +coeff[ 89]                *x54
        ;

    return v_l_e_q2ex_2_449                              ;
}
float x_e_q2ex_2_400                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.4099648E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.43356972E-03,-0.45681479E-02, 0.19849283E+00, 0.93780980E-02,
         0.42517306E-03,-0.12515502E-02,-0.38255537E-02,-0.26349653E-02,
         0.15615222E-04, 0.14663329E-05,-0.10417259E-02,-0.16364134E-02,
        -0.47991221E-03,-0.32615506E-02,-0.23833050E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_400                              =v_x_e_q2ex_2_400                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_400                              ;
}
float t_e_q2ex_2_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1287317E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.13407062E-03,-0.24640337E-02, 0.55495139E-01, 0.24192932E-02,
        -0.14392631E-02,-0.13310731E-02,-0.61340968E-03, 0.17964249E-03,
        -0.47768815E-03,-0.65055996E-03,-0.19029937E-03,-0.17917708E-02,
        -0.12420462E-02,-0.55061770E-04, 0.18775031E-03, 0.83582105E-04,
        -0.14979675E-02,-0.11846069E-02, 0.20197307E-04,-0.37756981E-04,
        -0.17261691E-04,-0.73595356E-05, 0.89076915E-04, 0.26484780E-04,
        -0.68792282E-03, 0.13208720E-03,-0.23629558E-04,-0.22729479E-04,
        -0.10251478E-04, 0.26607393E-04,-0.12478986E-04, 0.30816642E-04,
         0.17091441E-04,-0.72348616E-04,-0.51104980E-04, 0.93818780E-05,
         0.12991470E-04,-0.25472100E-04,-0.30048395E-04, 0.19272486E-04,
         0.11334592E-03, 0.17519375E-04,-0.21805314E-04, 0.26168476E-04,
        -0.19035513E-04,-0.12532915E-05,-0.24478277E-05,-0.23735538E-05,
         0.32575681E-05,-0.31185880E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_400                              =v_t_e_q2ex_2_400                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_400                              =v_t_e_q2ex_2_400                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x11            *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x21        *x53
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x23    *x42*x51
    ;
    v_t_e_q2ex_2_400                              =v_t_e_q2ex_2_400                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x21*x31    *x52
        +coeff[ 31]*x11*x21*x33        
        +coeff[ 32]*x12*x22    *x41    
        +coeff[ 33]*x11*x22*x31*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q2ex_2_400                              =v_t_e_q2ex_2_400                              
        +coeff[ 35]*x11*x23        *x51
        +coeff[ 36]*x11*x22*x31    *x51
        +coeff[ 37]*x11        *x42*x52
        +coeff[ 38]*x13*x22    *x41    
        +coeff[ 39]*x11*x21*x33*x41    
        +coeff[ 40]    *x23*x31*x41*x51
        +coeff[ 41]*x13*x21        *x52
        +coeff[ 42]*x11*x22*x31    *x52
        +coeff[ 43]    *x22    *x42*x52
    ;
    v_t_e_q2ex_2_400                              =v_t_e_q2ex_2_400                              
        +coeff[ 44]*x11*x21    *x41*x53
        +coeff[ 45]            *x41    
        +coeff[ 46]*x11*x21            
        +coeff[ 47]        *x32        
        +coeff[ 48]*x11        *x41    
        +coeff[ 49]                *x52
        ;

    return v_t_e_q2ex_2_400                              ;
}
float y_e_q2ex_2_400                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1019856E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.10816586E-02, 0.20388491E+00, 0.12099223E+00, 0.41849268E-03,
        -0.11041850E-02,-0.10988025E-02,-0.68063446E-03,-0.80347172E-03,
        -0.55431994E-03,-0.15105064E-03,-0.11801712E-04,-0.42878321E-03,
        -0.44625962E-04,-0.57694760E-04, 0.12558517E-04,-0.57452044E-03,
        -0.92351816E-04, 0.67022404E-04,-0.34286914E-03,-0.77664538E-03,
        -0.68905305E-04,-0.40899150E-03,-0.21266310E-03, 0.21017158E-04,
         0.10727468E-04,-0.30327861E-04,-0.19651692E-04, 0.39620205E-04,
         0.26847150E-04, 0.24086969E-04,-0.63186872E-03,-0.25984939E-03,
         0.70968701E-04,-0.74134055E-04, 0.12051850E-04, 0.38250397E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_400                              =v_y_e_q2ex_2_400                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_400                              =v_y_e_q2ex_2_400                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x31*x44    
        +coeff[ 21]    *x24    *x43    
        +coeff[ 22]    *x24*x31*x42    
        +coeff[ 23]    *x24*x33        
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_y_e_q2ex_2_400                              =v_y_e_q2ex_2_400                              
        +coeff[ 26]        *x32*x42    
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]        *x32*x41*x51
        +coeff[ 29]        *x31*x44    
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x33        
        +coeff[ 32]    *x22    *x44    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x11        *x45    
    ;
    v_y_e_q2ex_2_400                              =v_y_e_q2ex_2_400                              
        +coeff[ 35]*x11*x21    *x41*x52
        ;

    return v_y_e_q2ex_2_400                              ;
}
float p_e_q2ex_2_400                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.6638523E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.73400151E-04,-0.20520380E-01,-0.19588772E-01, 0.19008217E-02,
         0.29470322E-02,-0.63081709E-03, 0.26432608E-04,-0.39847381E-03,
        -0.61807700E-03,-0.33221534E-03,-0.15002582E-03, 0.77573954E-04,
         0.82962997E-05,-0.17568396E-03,-0.40568164E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_400                              =v_p_e_q2ex_2_400                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22*x32*x41    
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        ;

    return v_p_e_q2ex_2_400                              ;
}
float l_e_q2ex_2_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2931416E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.30035691E-02,-0.52970350E-02,-0.13069325E-02,-0.45671640E-02,
        -0.54587135E-02, 0.59285242E-03,-0.10397244E-03,-0.29473542E-03,
        -0.89909212E-04,-0.16665556E-03, 0.46549697E-03,-0.70889306E-04,
         0.36360594E-03, 0.71519535E-04,-0.15009807E-03,-0.12940641E-03,
        -0.16484591E-03, 0.10512805E-03,-0.15664817E-02,-0.31709275E-03,
        -0.25331299E-03, 0.38870855E-03,-0.32038061E-03, 0.13222304E-02,
         0.99554239E-03,-0.25719879E-03,-0.13715148E-02,-0.94842789E-03,
         0.33462064E-04,-0.66484755E-03, 0.68407680E-03, 0.74708520E-03,
        -0.59268961E-03, 0.14683949E-02,-0.32978455E-03,-0.97583764E-03,
         0.17636552E-03,-0.11535794E-02, 0.39297281E-03, 0.44374692E-03,
        -0.32991738E-03,-0.89563860E-03, 0.62335929E-03, 0.13296248E-03,
         0.60588028E-03, 0.12833630E-02,-0.60503930E-03, 0.67620154E-03,
        -0.20845477E-02,-0.53568528E-03,-0.77731261E-03, 0.11653629E-02,
         0.53679751E-03, 0.12337446E-02,-0.17293819E-02,-0.17687243E-02,
         0.12604030E-04, 0.78264362E-03,-0.14215086E-02, 0.99753903E-03,
         0.13931443E-02,-0.68147056E-04, 0.26236248E-03,-0.12676470E-03,
         0.63674583E-04, 0.21918480E-03, 0.32786286E-03, 0.58005942E-03,
        -0.33806701E-03, 0.16564161E-03,-0.25613821E-03, 0.16041884E-03,
        -0.11744967E-03, 0.35140425E-03, 0.14221895E-03, 0.23877768E-03,
         0.74077565E-04, 0.21034294E-03,-0.37482864E-03,-0.14990603E-03,
        -0.22466980E-03, 0.39584909E-03,-0.39054541E-03, 0.13395578E-03,
        -0.58429834E-03,-0.30600539E-03,-0.66901004E-03, 0.46398491E-03,
         0.17767490E-03, 0.41402274E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]        *x31*x41*x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[  8]            *x41    
        +coeff[  9]*x11                
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11    *x32        
        +coeff[ 15]*x11*x21        *x51
        +coeff[ 16]*x11        *x41*x51
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 17]*x11            *x52
        +coeff[ 18]        *x31*x43    
        +coeff[ 19]            *x41*x53
        +coeff[ 20]*x11*x23            
        +coeff[ 21]*x11    *x32    *x51
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]*x13            *x51
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 26]        *x32*x42*x51
        +coeff[ 27]    *x21*x32    *x52
        +coeff[ 28]    *x21    *x42*x52
        +coeff[ 29]*x11*x21*x32    *x51
        +coeff[ 30]*x11*x21    *x42*x51
        +coeff[ 31]*x11*x22        *x52
        +coeff[ 32]*x12    *x32*x41    
        +coeff[ 33]*x12    *x31*x41*x51
        +coeff[ 34]        *x34*x42    
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 35]        *x32*x44    
        +coeff[ 36]    *x24        *x52
        +coeff[ 37]        *x32    *x54
        +coeff[ 38]*x11*x24        *x51
        +coeff[ 39]*x11*x23*x31    *x51
        +coeff[ 40]*x13*x23            
        +coeff[ 41]*x13*x22*x31        
        +coeff[ 42]    *x23*x34        
        +coeff[ 43]    *x21*x32*x44    
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 44]    *x21*x33*x42*x51
        +coeff[ 45]    *x22    *x44*x51
        +coeff[ 46]*x13*x21        *x52
        +coeff[ 47]    *x24    *x41*x52
        +coeff[ 48]    *x23*x31*x41*x52
        +coeff[ 49]*x11*x24*x32        
        +coeff[ 50]*x12*x24*x31        
        +coeff[ 51]*x12*x21*x31*x43    
        +coeff[ 52]*x12*x21    *x44    
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 53]*x12*x22*x31*x41*x51
        +coeff[ 54]*x12    *x33*x41*x51
        +coeff[ 55]*x13*x23    *x41    
        +coeff[ 56]*x13*x21    *x43    
        +coeff[ 57]*x13*x21    *x41*x52
        +coeff[ 58]    *x22*x33    *x53
        +coeff[ 59]        *x31*x44*x53
        +coeff[ 60]        *x34    *x54
        +coeff[ 61]                *x51
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 62]        *x31    *x51
        +coeff[ 63]                *x52
        +coeff[ 64]*x11        *x41    
        +coeff[ 65]    *x22*x31        
        +coeff[ 66]        *x33        
        +coeff[ 67]    *x21*x31*x41    
        +coeff[ 68]        *x32*x41    
        +coeff[ 69]    *x21    *x42    
        +coeff[ 70]    *x22        *x51
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 71]                *x53
        +coeff[ 72]*x11*x21*x31        
        +coeff[ 73]*x11*x21    *x41    
        +coeff[ 74]*x13                
        +coeff[ 75]    *x21*x33        
        +coeff[ 76]    *x23    *x41    
        +coeff[ 77]    *x22*x31*x41    
        +coeff[ 78]    *x21*x31*x42    
        +coeff[ 79]    *x21    *x43    
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 80]            *x44    
        +coeff[ 81]    *x22*x31    *x51
        +coeff[ 82]        *x33    *x51
        +coeff[ 83]    *x22    *x41*x51
        +coeff[ 84]        *x32*x41*x51
        +coeff[ 85]    *x21    *x42*x51
        +coeff[ 86]        *x31*x42*x51
        +coeff[ 87]    *x22        *x52
        +coeff[ 88]    *x21        *x53
    ;
    v_l_e_q2ex_2_400                              =v_l_e_q2ex_2_400                              
        +coeff[ 89]*x11*x22*x31        
        ;

    return v_l_e_q2ex_2_400                              ;
}
float x_e_q2ex_2_350                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.4425339E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.44531465E-03,-0.45509706E-02, 0.19909064E+00, 0.93510961E-02,
         0.43729850E-03,-0.12398477E-02,-0.38023358E-02,-0.26149058E-02,
         0.13410573E-04,-0.96765498E-05,-0.10273190E-02,-0.16257182E-02,
        -0.47524858E-03,-0.32400000E-02,-0.23772570E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_350                              =v_x_e_q2ex_2_350                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_350                              ;
}
float t_e_q2ex_2_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1269498E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12720402E-03,-0.24489474E-02, 0.55731621E-01, 0.24296229E-02,
        -0.13967933E-02,-0.13187558E-02,-0.64375933E-03, 0.18236344E-03,
        -0.47048775E-03,-0.63058967E-03,-0.19412414E-03,-0.18200686E-02,
        -0.12464519E-02,-0.54678319E-04, 0.25338962E-03, 0.10774632E-03,
        -0.15738208E-02,-0.12373377E-02, 0.43278334E-04,-0.49397586E-04,
        -0.33844306E-04,-0.13524139E-04, 0.16544591E-04,-0.72694069E-03,
        -0.36444551E-04, 0.68156951E-05, 0.32591153E-04,-0.11511832E-04,
         0.89208326E-04,-0.54520518E-04,-0.36281726E-04, 0.10039334E-04,
        -0.32959237E-04,-0.34899149E-04, 0.27439271E-04,-0.31113173E-04,
        -0.25574693E-04, 0.87589826E-04,-0.30629228E-04, 0.25991851E-04,
        -0.74753534E-05,-0.16997615E-05, 0.34575069E-05,-0.82708275E-05,
         0.48169354E-05, 0.98717937E-05,-0.73581587E-05, 0.72658800E-05,
        -0.83124251E-05, 0.67218457E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_350                              =v_t_e_q2ex_2_350                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_350                              =v_t_e_q2ex_2_350                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x11            *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22    *x41    
    ;
    v_t_e_q2ex_2_350                              =v_t_e_q2ex_2_350                              
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]*x11*x22*x33        
        +coeff[ 33]*x12*x21    *x43    
        +coeff[ 34]*x12*x23        *x51
    ;
    v_t_e_q2ex_2_350                              =v_t_e_q2ex_2_350                              
        +coeff[ 35]*x12*x21*x32    *x51
        +coeff[ 36]*x11*x23    *x41*x51
        +coeff[ 37]    *x23    *x42*x51
        +coeff[ 38]*x12*x21*x31    *x52
        +coeff[ 39]    *x23*x31    *x52
        +coeff[ 40]*x11    *x31        
        +coeff[ 41]    *x21*x31        
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]*x13                
    ;
    v_t_e_q2ex_2_350                              =v_t_e_q2ex_2_350                              
        +coeff[ 44]*x11        *x41*x51
        +coeff[ 45]*x13*x21            
        +coeff[ 46]*x11*x23            
        +coeff[ 47]*x13    *x31        
        +coeff[ 48]*x12*x21*x31        
        +coeff[ 49]*x11*x21*x31*x41    
        ;

    return v_t_e_q2ex_2_350                              ;
}
float y_e_q2ex_2_350                              (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.1965546E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.19115603E-02, 0.20346934E+00, 0.12080023E+00, 0.43178009E-03,
        -0.10982275E-02,-0.12249273E-02,-0.89598249E-03,-0.90958143E-03,
        -0.73047128E-03,-0.17912121E-03,-0.32782140E-04,-0.44570703E-03,
        -0.13030179E-04,-0.66123081E-04, 0.10347135E-03,-0.45633773E-03,
        -0.74383432E-04,-0.35946211E-03,-0.12788492E-03, 0.74684409E-04,
        -0.16651693E-03, 0.31023057E-04, 0.54695498E-04, 0.13951365E-03,
         0.10566650E-03,-0.10518201E-03, 0.56368099E-04,-0.74216114E-04,
        -0.29137784E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_350                              =v_y_e_q2ex_2_350                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_350                              =v_y_e_q2ex_2_350                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]    *x24*x31    *x51
        +coeff[ 20]*x11*x23    *x43    
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]        *x32*x43    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q2ex_2_350                              =v_y_e_q2ex_2_350                              
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x12    *x31*x42    
        ;

    return v_y_e_q2ex_2_350                              ;
}
float p_e_q2ex_2_350                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.2274481E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.21872287E-03,-0.20525251E-01,-0.19589815E-01, 0.18986168E-02,
         0.29420543E-02,-0.61306177E-03, 0.14434816E-04,-0.39980814E-03,
        -0.61275874E-03,-0.32725581E-03,-0.14482944E-03, 0.77672839E-05,
        -0.17388533E-03,-0.37941357E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_350                              =v_p_e_q2ex_2_350                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_350                              ;
}
float l_e_q2ex_2_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2970166E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29445288E-02,-0.55421628E-04,-0.50476487E-02,-0.11321662E-02,
        -0.54631475E-02,-0.57583041E-02, 0.15771198E-02,-0.14124233E-03,
         0.17962727E-03, 0.11775574E-03, 0.47670201E-04, 0.17670396E-03,
        -0.53437671E-03, 0.18977576E-03, 0.69703441E-03,-0.12315710E-02,
        -0.15895189E-03, 0.78390597E-03,-0.71093184E-03, 0.98914257E-03,
        -0.10301190E-02, 0.83143049E-03, 0.28251001E-03,-0.84471802E-03,
         0.20440533E-03,-0.26814657E-03,-0.11274199E-02, 0.18510903E-02,
        -0.19823059E-02, 0.97200059E-03, 0.11974402E-02, 0.20701708E-02,
         0.26433035E-02, 0.19540268E-02, 0.10922430E-02, 0.21040780E-02,
        -0.21900630E-02, 0.18376531E-02, 0.36198553E-02, 0.13273482E-02,
         0.17465438E-02,-0.26374969E-02,-0.11409817E-02,-0.46384055E-03,
        -0.91992755E-03,-0.82014123E-03,-0.18722131E-02, 0.20948469E-02,
         0.33809402E-03,-0.99566450E-05,-0.33501783E-03,-0.35594878E-05,
        -0.11792868E-04,-0.54708746E-03, 0.30024390E-03,-0.43838267E-03,
         0.10662440E-02, 0.22778405E-03, 0.63781155E-03, 0.58306265E-03,
        -0.11928864E-03, 0.27215571E-03,-0.11477430E-03,-0.49707628E-05,
         0.14120129E-03, 0.49272523E-03,-0.19903005E-03, 0.25156082E-03,
        -0.35525052E-03,-0.19029016E-03,-0.10287061E-03, 0.97981677E-03,
         0.71699225E-03,-0.15213333E-03, 0.32119689E-03, 0.75111503E-03,
         0.35139630E-04,-0.13003153E-02, 0.34260409E-03,-0.23921160E-03,
        -0.31437704E-03, 0.90777321E-03,-0.22109454E-03,-0.14637469E-02,
         0.16025991E-03, 0.10017032E-02, 0.14696376E-02,-0.35890547E-03,
        -0.19275381E-03, 0.43808241E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41*x51
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x23            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x13                
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]*x11*x21    *x41*x51
        +coeff[ 14]*x12    *x31*x41    
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]        *x33*x42    
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 17]    *x22*x32    *x51
        +coeff[ 18]    *x22        *x53
        +coeff[ 19]*x11*x23*x31        
        +coeff[ 20]*x11*x21*x33        
        +coeff[ 21]*x11*x22*x31*x41    
        +coeff[ 22]*x11            *x54
        +coeff[ 23]*x12    *x31    *x52
        +coeff[ 24]*x13    *x32        
        +coeff[ 25]    *x21    *x43*x52
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 26]*x11*x22        *x53
        +coeff[ 27]*x12*x22*x32        
        +coeff[ 28]*x12*x22        *x52
        +coeff[ 29]*x12            *x54
        +coeff[ 30]    *x23*x34        
        +coeff[ 31]    *x24*x31    *x52
        +coeff[ 32]        *x33*x42*x52
        +coeff[ 33]        *x32*x43*x52
        +coeff[ 34]    *x22    *x42*x53
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 35]*x11*x23*x32    *x51
        +coeff[ 36]*x11*x24    *x41*x51
        +coeff[ 37]*x11*x23*x31*x41*x51
        +coeff[ 38]*x11*x21*x33*x41*x51
        +coeff[ 39]*x11*x22    *x43*x51
        +coeff[ 40]*x11*x21*x33    *x52
        +coeff[ 41]*x11*x21*x31    *x54
        +coeff[ 42]*x12*x22*x32*x41    
        +coeff[ 43]*x12    *x32*x43    
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 44]*x13*x21*x31*x41*x51
        +coeff[ 45]*x13    *x31*x41*x52
        +coeff[ 46]    *x23*x32*x41*x52
        +coeff[ 47]    *x23    *x43*x52
        +coeff[ 48]    *x22    *x44*x52
        +coeff[ 49]*x11                
        +coeff[ 50]                *x52
        +coeff[ 51]*x11            *x51
        +coeff[ 52]    *x22*x31        
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 53]*x11    *x31    *x51
        +coeff[ 54]*x11        *x41*x51
        +coeff[ 55]*x11            *x52
        +coeff[ 56]    *x22*x31*x41    
        +coeff[ 57]        *x33*x41    
        +coeff[ 58]    *x22    *x42    
        +coeff[ 59]    *x22        *x52
        +coeff[ 60]        *x32    *x52
        +coeff[ 61]            *x42*x52
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]            *x41*x53
        +coeff[ 64]*x11*x21    *x42    
        +coeff[ 65]*x11*x22        *x51
        +coeff[ 66]*x11*x21        *x52
        +coeff[ 67]*x11            *x53
        +coeff[ 68]*x12    *x32        
        +coeff[ 69]*x12        *x41*x51
        +coeff[ 70]    *x22*x33        
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 71]    *x22*x32*x41    
        +coeff[ 72]    *x22*x31*x42    
        +coeff[ 73]*x13            *x51
        +coeff[ 74]    *x21*x33    *x51
        +coeff[ 75]    *x21*x32*x41*x51
        +coeff[ 76]    *x21    *x43*x51
        +coeff[ 77]    *x22*x31    *x52
        +coeff[ 78]        *x33    *x52
        +coeff[ 79]    *x21    *x42*x52
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 80]    *x21    *x41*x53
        +coeff[ 81]*x11*x21*x32*x41    
        +coeff[ 82]*x11*x21    *x43    
        +coeff[ 83]*x11*x21*x31*x41*x51
        +coeff[ 84]*x11    *x32*x41*x51
        +coeff[ 85]*x11    *x31*x42*x51
        +coeff[ 86]*x11*x21*x31    *x52
        +coeff[ 87]*x11*x21    *x41*x52
        +coeff[ 88]*x11*x21        *x53
    ;
    v_l_e_q2ex_2_350                              =v_l_e_q2ex_2_350                              
        +coeff[ 89]*x11    *x31    *x53
        ;

    return v_l_e_q2ex_2_350                              ;
}
float x_e_q2ex_2_300                              (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.1311432E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.13495641E-02,-0.45225564E-02, 0.20013344E+00, 0.93155717E-02,
         0.42848694E-03,-0.17655675E-02,-0.51743430E-02,-0.36738268E-02,
        -0.97043603E-05, 0.46934412E-04, 0.27126248E-03,-0.21716533E-02,
        -0.48888713E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_300                              =v_x_e_q2ex_2_300                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        ;

    return v_x_e_q2ex_2_300                              ;
}
float t_e_q2ex_2_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3672894E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.37662979E-03,-0.24487569E-02, 0.56037609E-01, 0.23963246E-02,
        -0.14194497E-02,-0.13065536E-02,-0.63832058E-03, 0.18084473E-03,
        -0.45836842E-03,-0.62921859E-03,-0.19362071E-03,-0.18172668E-02,
        -0.12866121E-02,-0.48195550E-04, 0.21364339E-03, 0.16817522E-03,
        -0.69270632E-03,-0.11881487E-02,-0.10975701E-04,-0.56140289E-04,
        -0.36969730E-04, 0.10002851E-03,-0.30640947E-05,-0.73828464E-05,
        -0.15185605E-02, 0.63289910E-04, 0.11698849E-04, 0.31943028E-06,
        -0.19704263E-04,-0.25784888E-06,-0.66121038E-05,-0.31598236E-04,
        -0.11636535E-04, 0.26289521E-04,-0.18243991E-04, 0.20890522E-04,
         0.58568476E-04,-0.40332156E-05, 0.35457306E-05, 0.56707668E-05,
         0.47216818E-05,-0.62327863E-05, 0.15478370E-04, 0.31859106E-05,
        -0.90761832E-05, 0.90895783E-05, 0.96686663E-05,-0.44911071E-05,
         0.69399002E-05,-0.42532224E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_300                              =v_t_e_q2ex_2_300                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q2ex_2_300                              =v_t_e_q2ex_2_300                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21        *x53
        +coeff[ 23]*x13    *x32        
        +coeff[ 24]    *x21*x32*x42    
        +coeff[ 25]    *x23        *x53
    ;
    v_t_e_q2ex_2_300                              =v_t_e_q2ex_2_300                              
        +coeff[ 26]    *x22            
        +coeff[ 27]            *x42    
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x22    *x41    
        +coeff[ 30]*x11            *x52
        +coeff[ 31]    *x22    *x42    
        +coeff[ 32]*x12*x23            
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x22*x31*x43    
    ;
    v_t_e_q2ex_2_300                              =v_t_e_q2ex_2_300                              
        +coeff[ 35]    *x22*x31*x42*x51
        +coeff[ 36]    *x21*x31*x43*x51
        +coeff[ 37]*x11        *x41    
        +coeff[ 38]    *x22*x31        
        +coeff[ 39]    *x21*x31    *x51
        +coeff[ 40]    *x21    *x41*x51
        +coeff[ 41]*x12    *x31*x41    
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_q2ex_2_300                              =v_t_e_q2ex_2_300                              
        +coeff[ 44]*x11    *x31*x42    
        +coeff[ 45]*x12        *x41*x51
        +coeff[ 46]*x11    *x31*x41*x51
        +coeff[ 47]            *x43*x51
        +coeff[ 48]*x11*x21        *x52
        +coeff[ 49]    *x21*x31    *x52
        ;

    return v_t_e_q2ex_2_300                              ;
}
float y_e_q2ex_2_300                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.8775601E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.86969044E-03, 0.20336185E+00, 0.12052596E+00, 0.42257612E-03,
        -0.10931849E-02,-0.10751269E-02,-0.65767555E-03,-0.76328067E-03,
        -0.57509117E-03,-0.14182225E-03, 0.48210764E-04,-0.47947789E-03,
        -0.21527596E-04,-0.60222501E-04, 0.20858090E-04,-0.57864212E-03,
        -0.14242126E-04,-0.45018914E-03,-0.79761885E-05, 0.67136258E-04,
        -0.42554435E-04, 0.64639651E-04,-0.76975964E-03,-0.41690004E-04,
        -0.70346094E-03,-0.12437271E-04, 0.26242378E-04,-0.25642867E-03,
        -0.11385949E-03,-0.12246694E-03,-0.26784712E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_300                              =v_y_e_q2ex_2_300                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_300                              =v_y_e_q2ex_2_300                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]        *x31*x44    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]*x11        *x41*x52
    ;
    v_y_e_q2ex_2_300                              =v_y_e_q2ex_2_300                              
        +coeff[ 26]*x11*x21*x31*x42    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x22    *x45    
        ;

    return v_y_e_q2ex_2_300                              ;
}
float p_e_q2ex_2_300                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.6373487E-04;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.61761988E-04,-0.20541031E-01,-0.19623656E-01, 0.18968694E-02,
         0.29437384E-02,-0.62063214E-03, 0.23744127E-04,-0.41067271E-03,
        -0.61454490E-03,-0.33097481E-03,-0.14480671E-03, 0.19078130E-04,
        -0.17491318E-03,-0.38844446E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_300                              =v_p_e_q2ex_2_300                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_300                              ;
}
float l_e_q2ex_2_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2946281E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.28786010E-02,-0.47025154E-02,-0.14037343E-02,-0.44560139E-02,
        -0.54443493E-02, 0.59284200E-03,-0.90322061E-03,-0.54127322E-04,
         0.43268697E-03,-0.15692549E-02, 0.47457920E-03,-0.28469765E-04,
         0.32677350E-03,-0.38140028E-03, 0.46375589E-03, 0.10953472E-02,
         0.10002988E-02, 0.11662489E-02, 0.76231302E-03, 0.41160019E-03,
        -0.57492248E-03,-0.45219361E-03,-0.25569758E-03, 0.25599165E-03,
        -0.96688356E-03, 0.73492026E-03,-0.15646237E-02,-0.16962816E-02,
         0.24629135E-02, 0.58245508E-03,-0.12412287E-02,-0.72689861E-03,
        -0.15555676E-02,-0.86440420E-03, 0.14479525E-03, 0.13876577E-03,
         0.15377090E-02,-0.74243441E-03,-0.10514123E-02, 0.46586752E-03,
        -0.23645456E-02, 0.15240794E-02, 0.17883288E-02, 0.13104747E-02,
         0.17011930E-03, 0.12199883E-02, 0.15188619E-02,-0.73820906E-03,
        -0.89984831E-04, 0.30353476E-05,-0.14108332E-03, 0.84316736E-04,
        -0.30684582E-03,-0.21698164E-03, 0.60767162E-03, 0.68524350E-04,
        -0.13256460E-03,-0.44940862E-04, 0.20706166E-03,-0.25496841E-03,
         0.16671213E-03,-0.25024411E-03, 0.25430002E-03, 0.20632822E-03,
        -0.73635112E-03, 0.79337240E-03, 0.17374253E-03, 0.22476954E-03,
         0.13064612E-03,-0.30591735E-03,-0.34900779E-04,-0.59538230E-03,
        -0.24264531E-03,-0.27287699E-03,-0.15624033E-03,-0.23109493E-03,
         0.68856702E-04, 0.66535291E-03,-0.19378580E-03, 0.44895810E-03,
        -0.57112476E-04, 0.14487948E-03, 0.17500336E-03,-0.15204711E-03,
        -0.51514589E-03,-0.22340698E-03, 0.37224611E-03, 0.15995193E-03,
         0.19505771E-03,-0.54372847E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[  8]    *x21*x34*x41    
        +coeff[  9]    *x24    *x42    
        +coeff[ 10]*x13            *x54
        +coeff[ 11]    *x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 17]*x11    *x31    *x51
        +coeff[ 18]*x11        *x41*x51
        +coeff[ 19]        *x31    *x53
        +coeff[ 20]*x12    *x31*x41    
        +coeff[ 21]*x13*x21            
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x21*x33    *x51
        +coeff[ 24]            *x44*x51
        +coeff[ 25]    *x22        *x53
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 26]*x11    *x32*x42    
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]*x11*x21*x31*x41*x51
        +coeff[ 29]*x11*x21    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41    
        +coeff[ 31]*x12*x21    *x42    
        +coeff[ 32]*x13    *x31    *x51
        +coeff[ 33]*x13        *x41*x51
        +coeff[ 34]    *x24    *x41*x51
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 35]*x12*x24            
        +coeff[ 36]    *x23*x33*x41    
        +coeff[ 37]    *x24    *x43    
        +coeff[ 38]    *x24*x31    *x52
        +coeff[ 39]*x11*x22*x34        
        +coeff[ 40]*x11*x21*x31*x43*x51
        +coeff[ 41]*x11*x21    *x43*x52
        +coeff[ 42]*x12*x22*x31    *x52
        +coeff[ 43]*x12*x22    *x41*x52
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 44]*x13*x23*x31        
        +coeff[ 45]*x13    *x32*x42    
        +coeff[ 46]    *x21*x34*x43    
        +coeff[ 47]    *x21    *x43*x54
        +coeff[ 48]*x11                
        +coeff[ 49]    *x21*x31        
        +coeff[ 50]        *x31    *x51
        +coeff[ 51]*x11    *x31        
        +coeff[ 52]    *x21*x31*x41    
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 53]        *x31*x42    
        +coeff[ 54]        *x32    *x51
        +coeff[ 55]        *x31    *x52
        +coeff[ 56]            *x41*x52
        +coeff[ 57]*x11*x22            
        +coeff[ 58]*x11    *x32        
        +coeff[ 59]*x11*x21    *x41    
        +coeff[ 60]*x11        *x42    
        +coeff[ 61]    *x22*x32        
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 62]        *x34        
        +coeff[ 63]    *x23    *x41    
        +coeff[ 64]    *x21*x32*x41    
        +coeff[ 65]    *x22    *x42    
        +coeff[ 66]    *x23        *x51
        +coeff[ 67]    *x21*x31*x41*x51
        +coeff[ 68]            *x43*x51
        +coeff[ 69]    *x22        *x52
        +coeff[ 70]        *x32    *x52
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 71]        *x31*x41*x52
        +coeff[ 72]            *x42*x52
        +coeff[ 73]    *x21        *x53
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]*x11*x21*x32        
        +coeff[ 76]*x11    *x33        
        +coeff[ 77]*x11    *x32*x41    
        +coeff[ 78]*x11*x21    *x42    
        +coeff[ 79]*x11    *x31*x42    
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 80]*x11        *x43    
        +coeff[ 81]*x11*x21*x31    *x51
        +coeff[ 82]*x11*x21        *x52
        +coeff[ 83]*x11        *x41*x52
        +coeff[ 84]*x12*x22            
        +coeff[ 85]*x12*x21*x31        
        +coeff[ 86]*x12*x21        *x51
        +coeff[ 87]*x12            *x52
        +coeff[ 88]    *x21*x34        
    ;
    v_l_e_q2ex_2_300                              =v_l_e_q2ex_2_300                              
        +coeff[ 89]        *x34    *x51
        ;

    return v_l_e_q2ex_2_300                              ;
}
float x_e_q2ex_2_250                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.2161550E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.22057160E-02,-0.44903299E-02, 0.20096540E+00, 0.92700338E-02,
         0.42911610E-03,-0.12257237E-02,-0.37545944E-02,-0.25904237E-02,
         0.41607222E-04, 0.22643501E-05,-0.10512087E-02,-0.16122531E-02,
        -0.48365662E-03,-0.32479451E-02,-0.23629456E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_250                              =v_x_e_q2ex_2_250                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_250                              ;
}
float t_e_q2ex_2_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6086504E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62029855E-03,-0.24373317E-02, 0.56476135E-01, 0.23627630E-02,
        -0.14316658E-02,-0.13099345E-02,-0.64081006E-03, 0.17604895E-03,
        -0.46851145E-03,-0.62638626E-03,-0.19465676E-03,-0.18094876E-02,
        -0.12470842E-02,-0.36974328E-04, 0.24356529E-03, 0.17314601E-03,
        -0.14796613E-02,-0.11472764E-02, 0.78208386E-05,-0.30329700E-04,
        -0.18441609E-04, 0.10220759E-03, 0.33249116E-05,-0.66639786E-03,
         0.23053439E-04,-0.21823234E-05,-0.51637397E-06, 0.12606751E-04,
        -0.21197269E-04, 0.33579487E-04, 0.99830932E-05,-0.28736775E-04,
         0.12719905E-04,-0.13989318E-03,-0.10927010E-03, 0.27718592E-04,
        -0.16901484E-04, 0.12240216E-05, 0.16125829E-05,-0.26306316E-05,
         0.55559976E-05,-0.46326313E-05, 0.90711037E-05,-0.31502323E-05,
        -0.86797463E-05, 0.12654877E-04,-0.42813663E-05,-0.46793011E-05,
         0.71223831E-05, 0.70331416E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_250                              =v_t_e_q2ex_2_250                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_250                              =v_t_e_q2ex_2_250                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x23        *x53
        +coeff[ 25]*x11        *x41    
    ;
    v_t_e_q2ex_2_250                              =v_t_e_q2ex_2_250                              
        +coeff[ 26]*x13                
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]*x12*x23            
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11*x23    *x41    
        +coeff[ 33]*x11*x22*x31*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q2ex_2_250                              =v_t_e_q2ex_2_250                              
        +coeff[ 35]    *x21*x31*x42*x51
        +coeff[ 36]*x12*x22        *x52
        +coeff[ 37]                *x51
        +coeff[ 38]    *x22            
        +coeff[ 39]    *x22    *x41    
        +coeff[ 40]    *x21    *x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]    *x22*x32        
        +coeff[ 43]    *x21*x33        
    ;
    v_t_e_q2ex_2_250                              =v_t_e_q2ex_2_250                              
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]    *x22*x31*x41    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x22*x31    *x51
        +coeff[ 48]*x11*x21    *x41*x51
        +coeff[ 49]*x11            *x53
        ;

    return v_t_e_q2ex_2_250                              ;
}
float y_e_q2ex_2_250                              (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.8894044E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.87155995E-03, 0.20289534E+00, 0.12019359E+00, 0.43886673E-03,
        -0.10878952E-02,-0.10524365E-02,-0.67203405E-03,-0.80032035E-03,
        -0.56960917E-03,-0.14776271E-03, 0.19843486E-04,-0.46385638E-03,
        -0.51985447E-04,-0.71273877E-04,-0.57576403E-05,-0.56304020E-03,
        -0.48027854E-04,-0.41934129E-03,-0.70072082E-03,-0.26358780E-03,
        -0.53525972E-03,-0.18740681E-03,-0.59016485E-04,-0.31741289E-03,
         0.12733099E-04, 0.29804380E-06,-0.14178009E-04,-0.12288373E-04,
        -0.47069257E-05, 0.18078108E-04, 0.39276976E-04, 0.62847743E-04,
         0.66344110E-04,-0.53483050E-03,-0.30958850E-04,-0.49600396E-04,
        -0.27546903E-04,-0.38909981E-04,-0.22966960E-03,-0.27106365E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_250                              =v_y_e_q2ex_2_250                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_250                              =v_y_e_q2ex_2_250                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x22*x31*x44    
        +coeff[ 21]    *x24    *x43    
        +coeff[ 22]    *x24*x31*x42    
        +coeff[ 23]    *x22*x32*x45    
        +coeff[ 24]*x11        *x42    
        +coeff[ 25]            *x42*x51
    ;
    v_y_e_q2ex_2_250                              =v_y_e_q2ex_2_250                              
        +coeff[ 26]        *x31*x41*x51
        +coeff[ 27]        *x31    *x52
        +coeff[ 28]            *x43*x51
        +coeff[ 29]*x11*x21    *x42    
        +coeff[ 30]        *x31*x42*x51
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x21    *x42*x52
    ;
    v_y_e_q2ex_2_250                              =v_y_e_q2ex_2_250                              
        +coeff[ 35]*x11*x23*x31        
        +coeff[ 36]*x12*x22    *x41    
        +coeff[ 37]*x11*x21    *x41*x52
        +coeff[ 38]    *x22    *x45    
        +coeff[ 39]    *x21*x31*x45    
        ;

    return v_y_e_q2ex_2_250                              ;
}
float p_e_q2ex_2_250                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1325229E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.13030303E-03,-0.20567330E-01,-0.19640731E-01, 0.18930723E-02,
         0.29405570E-02,-0.63983456E-03, 0.26544765E-04,-0.40656290E-03,
        -0.60650043E-03,-0.32600996E-03,-0.14472895E-03, 0.70921262E-04,
         0.15475456E-04,-0.17031950E-03,-0.40329533E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_250                              =v_p_e_q2ex_2_250                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22*x32*x41    
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        ;

    return v_p_e_q2ex_2_250                              ;
}
float l_e_q2ex_2_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2921690E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.28025920E-02,-0.50487118E-02,-0.13605603E-02,-0.47787107E-02,
        -0.53329053E-02,-0.90909998E-04, 0.62444271E-03,-0.12632771E-03,
         0.82843867E-03,-0.56571403E-03, 0.12999099E-03, 0.32189375E-03,
        -0.69956400E-05, 0.36832879E-03, 0.41131189E-03,-0.13527978E-03,
        -0.38857717E-03, 0.99841668E-03, 0.20068068E-03, 0.60503982E-03,
         0.35157046E-03, 0.22394040E-03, 0.16939387E-03,-0.44730026E-03,
        -0.47032500E-03,-0.32136752E-03,-0.11561371E-02,-0.82001311E-03,
         0.11654387E-02,-0.46019873E-03, 0.28302441E-02,-0.12791419E-03,
        -0.55620197E-03,-0.30222282E-03, 0.65803190E-03, 0.11208849E-02,
        -0.15614886E-02,-0.36566373E-03, 0.42601701E-03, 0.79449808E-03,
         0.80516515E-03,-0.58608066E-03, 0.63656201E-03,-0.22530670E-02,
        -0.13290871E-02, 0.38567503E-03, 0.13477736E-02,-0.39422745E-02,
         0.13670825E-02,-0.96973986E-03, 0.24453807E-03,-0.38716124E-03,
        -0.19786703E-03, 0.60015856E-04, 0.25475031E-03,-0.17953939E-03,
         0.14262777E-03,-0.15313753E-04,-0.20760427E-03,-0.37571017E-03,
         0.52585237E-03,-0.49740990E-03, 0.26603506E-03,-0.18574444E-03,
        -0.18885071E-03,-0.10618618E-03, 0.12717814E-03, 0.18437032E-03,
        -0.16393834E-03, 0.22236923E-03, 0.37024869E-03, 0.60171413E-03,
         0.11215933E-02, 0.39990430E-03,-0.31540266E-03, 0.38799539E-03,
         0.46114565E-03,-0.24173011E-03,-0.23843904E-03,-0.25407624E-03,
         0.29445073E-03, 0.21098668E-03,-0.16033229E-03,-0.32563263E-03,
         0.71479171E-03, 0.70848363E-03,-0.28430609E-03,-0.47406167E-03,
         0.16567990E-03, 0.26624236E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]            *x42*x51
        +coeff[  7]*x11*x21    *x41    
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[  8]        *x31*x42*x51
        +coeff[  9]*x11        *x43    
        +coeff[ 10]    *x24        *x51
        +coeff[ 11]            *x43    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]                *x53
        +coeff[ 16]*x11    *x32        
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 17]        *x32    *x52
        +coeff[ 18]            *x42*x52
        +coeff[ 19]*x11*x21        *x52
        +coeff[ 20]*x11            *x53
        +coeff[ 21]*x12*x22            
        +coeff[ 22]*x12*x21        *x51
        +coeff[ 23]*x12            *x52
        +coeff[ 24]    *x21    *x43*x51
        +coeff[ 25]    *x23        *x52
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 26]*x11    *x33*x41    
        +coeff[ 27]*x11    *x33    *x51
        +coeff[ 28]*x11*x22    *x41*x51
        +coeff[ 29]*x11    *x32*x41*x51
        +coeff[ 30]*x11*x21    *x42*x51
        +coeff[ 31]*x11*x21    *x41*x52
        +coeff[ 32]*x12        *x43    
        +coeff[ 33]*x12*x22        *x51
        +coeff[ 34]    *x22*x33*x41    
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 35]    *x21*x32*x41*x52
        +coeff[ 36]        *x32    *x54
        +coeff[ 37]*x11    *x31*x44    
        +coeff[ 38]*x12*x22*x32        
        +coeff[ 39]*x13*x22*x31        
        +coeff[ 40]*x13*x22    *x41    
        +coeff[ 41]    *x21*x33    *x53
        +coeff[ 42]*x11*x24*x31    *x51
        +coeff[ 43]*x11*x23*x32    *x51
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 44]*x11*x21*x32*x41*x52
        +coeff[ 45]*x11*x23        *x53
        +coeff[ 46]*x13*x21*x32    *x51
        +coeff[ 47]*x13*x21    *x42*x51
        +coeff[ 48]*x13*x21        *x53
        +coeff[ 49]        *x33*x42*x53
        +coeff[ 50]*x11                
        +coeff[ 51]    *x21*x31        
        +coeff[ 52]    *x21    *x41    
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 53]            *x41*x51
        +coeff[ 54]                *x52
        +coeff[ 55]*x11            *x51
        +coeff[ 56]    *x23            
        +coeff[ 57]        *x33        
        +coeff[ 58]    *x22    *x41    
        +coeff[ 59]    *x22        *x51
        +coeff[ 60]*x11    *x31*x41    
        +coeff[ 61]*x11*x21        *x51
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 62]*x11    *x31    *x51
        +coeff[ 63]*x11        *x41*x51
        +coeff[ 64]*x11            *x52
        +coeff[ 65]*x12*x21            
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]*x13                
        +coeff[ 69]    *x23*x31        
        +coeff[ 70]    *x21*x33        
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 71]    *x21*x32*x41    
        +coeff[ 72]    *x21*x31*x42    
        +coeff[ 73]    *x21    *x43    
        +coeff[ 74]    *x22        *x52
        +coeff[ 75]*x11    *x33        
        +coeff[ 76]*x11    *x32*x41    
        +coeff[ 77]*x11*x22        *x51
        +coeff[ 78]*x11    *x31    *x52
        +coeff[ 79]*x11        *x41*x52
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 80]*x12    *x32        
        +coeff[ 81]*x13*x21            
        +coeff[ 82]*x13    *x31        
        +coeff[ 83]    *x22*x33        
        +coeff[ 84]    *x21*x33*x41    
        +coeff[ 85]    *x21*x32*x42    
        +coeff[ 86]        *x33*x42    
        +coeff[ 87]        *x32*x43    
        +coeff[ 88]*x13            *x51
    ;
    v_l_e_q2ex_2_250                              =v_l_e_q2ex_2_250                              
        +coeff[ 89]    *x23    *x41*x51
        ;

    return v_l_e_q2ex_2_250                              ;
}
float x_e_q2ex_2_200                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.2644357E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.31935680E-03,-0.44427249E-02, 0.20258322E+00, 0.92007285E-02,
         0.42452960E-03,-0.12314335E-02,-0.37426804E-02,-0.25903075E-02,
         0.91074862E-05,-0.28502776E-04,-0.10088234E-02,-0.16023034E-02,
        -0.46794320E-03,-0.32614756E-02,-0.23703624E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_200                              =v_x_e_q2ex_2_200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_200                              ;
}
float t_e_q2ex_2_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.8064480E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.96382697E-04,-0.24130936E-02, 0.57127953E-01, 0.23296759E-02,
        -0.14211183E-02,-0.13249377E-02,-0.62817620E-03, 0.17931937E-03,
        -0.47018417E-03,-0.62700722E-03,-0.18987092E-03,-0.18080029E-02,
        -0.12371808E-02,-0.58870206E-04, 0.22979148E-03, 0.14138024E-03,
        -0.66980702E-03,-0.11833567E-02,-0.13854105E-04,-0.46242840E-04,
        -0.35129535E-04, 0.10328257E-03, 0.31952602E-05,-0.14949014E-02,
        -0.35144330E-05, 0.17466764E-05,-0.25104422E-04, 0.10349968E-04,
        -0.10692529E-04, 0.11053153E-04, 0.51508334E-04,-0.40932107E-04,
        -0.21413212E-04, 0.39113002E-06, 0.16737788E-05, 0.20898649E-05,
         0.59812319E-05, 0.92387372E-05,-0.75516823E-05,-0.10248510E-04,
        -0.65551640E-05,-0.55656892E-05,-0.57539710E-05, 0.11119665E-04,
         0.89233054E-05, 0.13138115E-04, 0.19061608E-04,-0.17401264E-04,
         0.17226872E-04,-0.21393431E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_200                              =v_t_e_q2ex_2_200                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q2ex_2_200                              =v_t_e_q2ex_2_200                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x22            
        +coeff[ 25]    *x21*x31        
    ;
    v_t_e_q2ex_2_200                              =v_t_e_q2ex_2_200                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]*x11            *x52
        +coeff[ 29]*x12*x21        *x51
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]*x12*x23        *x51
        +coeff[ 32]*x11*x22*x31    *x52
        +coeff[ 33]        *x32        
        +coeff[ 34]                *x52
    ;
    v_t_e_q2ex_2_200                              =v_t_e_q2ex_2_200                              
        +coeff[ 35]                *x53
        +coeff[ 36]*x12*x22            
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x22*x32        
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]*x12    *x31    *x51
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]*x11        *x42*x51
        +coeff[ 43]    *x21*x31    *x52
    ;
    v_t_e_q2ex_2_200                              =v_t_e_q2ex_2_200                              
        +coeff[ 44]    *x21    *x41*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]*x11*x23*x31        
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q2ex_2_200                              ;
}
float y_e_q2ex_2_200                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.9178304E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
        -0.48404178E-04, 0.20239437E+00, 0.11963587E+00, 0.44615270E-03,
        -0.10622953E-02,-0.10779691E-02,-0.67876349E-03,-0.78635907E-03,
        -0.55686146E-03,-0.14290377E-03, 0.38983853E-04,-0.47011563E-03,
        -0.19757546E-04,-0.66748871E-04, 0.16140550E-04,-0.55674801E-03,
        -0.67938519E-04,-0.39307689E-03,-0.71793178E-03,-0.24910993E-03,
        -0.12283419E-03,-0.25773316E-03,-0.19271254E-03,-0.10403842E-03,
        -0.17675021E-03,-0.10772598E-04, 0.61362742E-04, 0.63616564E-04,
        -0.50694117E-03, 0.39269067E-04, 0.23231005E-04,-0.45713678E-04,
         0.84636427E-04,-0.20173698E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_200                              =v_y_e_q2ex_2_200                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2ex_2_200                              =v_y_e_q2ex_2_200                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]    *x22*x31*x44    
        +coeff[ 22]    *x24    *x43    
        +coeff[ 23]    *x24*x31*x42    
        +coeff[ 24]*x13*x23    *x41    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_y_e_q2ex_2_200                              =v_y_e_q2ex_2_200                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]*x11    *x31*x43    
        +coeff[ 30]*x12        *x41*x51
        +coeff[ 31]        *x31*x42*x52
        +coeff[ 32]*x13*x21    *x41    
        +coeff[ 33]    *x22    *x45    
        ;

    return v_y_e_q2ex_2_200                              ;
}
float p_e_q2ex_2_200                              (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.3862460E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.53812884E-04,-0.20589832E-01,-0.19691357E-01, 0.18886137E-02,
         0.29353669E-02,-0.63741766E-03, 0.17340451E-04,-0.42013722E-03,
        -0.60801604E-03,-0.32644923E-03,-0.14914085E-03, 0.11228174E-04,
        -0.17350228E-03,-0.37982917E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_200                              =v_p_e_q2ex_2_200                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_200                              ;
}
float l_e_q2ex_2_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3010110E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29357222E-02,-0.51319385E-02,-0.10773318E-02,-0.45795017E-02,
        -0.53137136E-02, 0.43390311E-04,-0.94972033E-03,-0.41496730E-02,
        -0.29478950E-04,-0.53756703E-05, 0.14338680E-03,-0.16755503E-03,
        -0.44339988E-04,-0.11183286E-04, 0.27768727E-03, 0.11015958E-03,
         0.53156225E-03, 0.30613632E-03, 0.33483823E-03,-0.26042844E-03,
         0.13427333E-02,-0.15381110E-03,-0.64168050E-03,-0.94215735E-03,
        -0.11017502E-02,-0.58317697E-03,-0.71298995E-03, 0.27272608E-02,
        -0.41889518E-04, 0.25064396E-02, 0.20523241E-02, 0.50979457E-03,
         0.18322959E-02,-0.10942670E-02,-0.36367698E-03,-0.12262654E-02,
        -0.11578256E-02,-0.13880350E-02, 0.20413958E-02,-0.90949243E-03,
         0.13289899E-02,-0.12094759E-02, 0.15721543E-02,-0.25027257E-02,
        -0.34549121E-04, 0.18504405E-03,-0.20945400E-03,-0.25945043E-03,
         0.17231129E-03,-0.39625829E-03,-0.12072462E-02,-0.47831654E-04,
         0.30706398E-03,-0.55787132E-04,-0.67029154E-03, 0.53379923E-03,
        -0.21323744E-03, 0.10997367E-03,-0.16719814E-03, 0.37343975E-03,
        -0.19071580E-03, 0.44818036E-03,-0.32230094E-03,-0.25423360E-03,
        -0.19180782E-03, 0.49589260E-03, 0.19530080E-03, 0.23088303E-03,
        -0.22916537E-03,-0.17570730E-03, 0.25945777E-03, 0.33187223E-03,
         0.26045862E-03, 0.15937607E-03,-0.17832010E-03,-0.13118694E-03,
         0.52521634E-03, 0.10294284E-02,-0.77371439E-03,-0.58716408E-03,
         0.81917387E-04, 0.44220898E-03,-0.28755781E-03,-0.51201147E-03,
         0.60815748E-03, 0.31897606E-03, 0.71862550E-03,-0.59525156E-03,
         0.36079332E-03, 0.33306525E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x22*x31*x42    
        +coeff[  6]*x12*x24        *x51
        +coeff[  7]        *x32*x42*x54
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[  8]            *x41    
        +coeff[  9]                *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x12                
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]*x12    *x31        
        +coeff[ 15]        *x31*x41*x52
        +coeff[ 16]    *x21        *x53
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 17]*x11    *x31*x41*x51
        +coeff[ 18]*x13    *x31        
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x32    *x52
        +coeff[ 21]    *x21*x31*x41*x52
        +coeff[ 22]*x11*x21*x31    *x52
        +coeff[ 23]*x11        *x42*x52
        +coeff[ 24]*x12        *x42*x51
        +coeff[ 25]    *x21*x33    *x52
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 26]*x11    *x31*x44    
        +coeff[ 27]    *x23*x31*x41*x52
        +coeff[ 28]*x11*x24*x31*x41    
        +coeff[ 29]*x11*x22*x32*x42    
        +coeff[ 30]*x11*x21*x33*x41*x51
        +coeff[ 31]*x11    *x34*x41*x51
        +coeff[ 32]*x11    *x33*x41*x52
        +coeff[ 33]*x12*x22*x33        
        +coeff[ 34]*x12*x23*x31*x41    
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 35]*x12*x21*x33    *x51
        +coeff[ 36]*x13*x22    *x42    
        +coeff[ 37]*x13*x21*x31*x41*x51
        +coeff[ 38]    *x22*x34*x41*x51
        +coeff[ 39]*x13    *x31*x41*x52
        +coeff[ 40]*x13        *x42*x52
        +coeff[ 41]    *x22*x31*x43*x52
        +coeff[ 42]    *x22*x33    *x53
        +coeff[ 43]        *x33*x41*x54
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 44]    *x21            
        +coeff[ 45]    *x21*x31        
        +coeff[ 46]    *x21        *x51
        +coeff[ 47]*x11        *x41    
        +coeff[ 48]    *x23            
        +coeff[ 49]    *x22*x31        
        +coeff[ 50]    *x21*x32        
        +coeff[ 51]        *x33        
        +coeff[ 52]    *x21    *x42    
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 53]    *x22        *x51
        +coeff[ 54]    *x21*x31    *x51
        +coeff[ 55]        *x32    *x51
        +coeff[ 56]    *x21    *x41*x51
        +coeff[ 57]            *x42*x51
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x12            *x51
        +coeff[ 60]    *x23*x31        
        +coeff[ 61]        *x33*x41    
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 62]        *x31*x43    
        +coeff[ 63]        *x33    *x51
        +coeff[ 64]    *x21    *x42*x51
        +coeff[ 65]        *x31*x42*x51
        +coeff[ 66]            *x42*x52
        +coeff[ 67]            *x41*x53
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]*x11*x21*x32        
        +coeff[ 70]*x11    *x33        
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 71]*x11*x22    *x41    
        +coeff[ 72]*x11        *x43    
        +coeff[ 73]*x11*x21        *x52
        +coeff[ 74]*x12        *x42    
        +coeff[ 75]*x12        *x41*x51
        +coeff[ 76]    *x24*x31        
        +coeff[ 77]    *x21*x34        
        +coeff[ 78]    *x23*x31*x41    
        +coeff[ 79]    *x23    *x42    
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 80]*x13            *x51
        +coeff[ 81]    *x23*x31    *x51
        +coeff[ 82]    *x22*x32    *x51
        +coeff[ 83]    *x21*x32*x41*x51
        +coeff[ 84]    *x22*x31    *x52
        +coeff[ 85]    *x22    *x41*x52
        +coeff[ 86]    *x21*x31    *x53
        +coeff[ 87]        *x32    *x53
        +coeff[ 88]    *x21    *x41*x53
    ;
    v_l_e_q2ex_2_200                              =v_l_e_q2ex_2_200                              
        +coeff[ 89]            *x42*x53
        ;

    return v_l_e_q2ex_2_200                              ;
}
float x_e_q2ex_2_175                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1194725E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.12334804E-02,-0.44037672E-02, 0.20389065E+00, 0.91462554E-02,
         0.42875062E-03,-0.15104274E-02,-0.41430341E-02,-0.28233845E-02,
        -0.32296350E-04,-0.17586524E-04, 0.13290083E-04,-0.20900797E-02,
        -0.47378626E-03,-0.22347057E-02,-0.18024711E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_175                              =v_x_e_q2ex_2_175                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_175                              ;
}
float t_e_q2ex_2_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3422010E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35284445E-03,-0.24024798E-02, 0.57593841E-01, 0.23115145E-02,
        -0.14218879E-02,-0.13339582E-02,-0.60391630E-03, 0.17876028E-03,
        -0.47592525E-03,-0.63377235E-03,-0.19141792E-03,-0.17808477E-02,
        -0.12029178E-02,-0.53626278E-04, 0.16617739E-03, 0.10665877E-03,
        -0.14397517E-02,-0.11491505E-02, 0.44245615E-04,-0.40771458E-04,
        -0.22713213E-04,-0.60396196E-05,-0.65109989E-03, 0.50297433E-04,
         0.47449571E-05,-0.22258315E-04, 0.65486449E-04,-0.17076651E-04,
         0.27337994E-04, 0.18866976E-04,-0.78304292E-04,-0.70295617E-04,
        -0.29982424E-04, 0.41919437E-04,-0.30768409E-04, 0.59851100E-04,
         0.48476322E-04, 0.62556195E-04,-0.16467353E-04,-0.15249931E-04,
         0.71264505E-04, 0.23991606E-05, 0.29229245E-05, 0.21217315E-05,
         0.38484091E-05, 0.55444575E-05,-0.54091765E-05, 0.82935924E-06,
        -0.55545929E-05, 0.31204734E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_175                              =v_t_e_q2ex_2_175                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_175                              =v_t_e_q2ex_2_175                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x11            *x52
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]*x12*x23        *x51
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2ex_2_175                              =v_t_e_q2ex_2_175                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]*x11*x21    *x41*x51
        +coeff[ 28]    *x21        *x53
        +coeff[ 29]*x13*x22            
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x11*x23*x31    *x51
        +coeff[ 34]*x12*x21*x32    *x51
    ;
    v_t_e_q2ex_2_175                              =v_t_e_q2ex_2_175                              
        +coeff[ 35]*x11*x23    *x41*x51
        +coeff[ 36]*x11*x21*x32*x41*x51
        +coeff[ 37]    *x23    *x42*x51
        +coeff[ 38]    *x22    *x43*x51
        +coeff[ 39]*x12*x22        *x52
        +coeff[ 40]    *x21*x31*x41*x53
        +coeff[ 41]*x11    *x31        
        +coeff[ 42]    *x21    *x41    
        +coeff[ 43]            *x42    
    ;
    v_t_e_q2ex_2_175                              =v_t_e_q2ex_2_175                              
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x31*x41*x51
        +coeff[ 48]            *x42*x51
        +coeff[ 49]            *x41*x52
        ;

    return v_t_e_q2ex_2_175                              ;
}
float y_e_q2ex_2_175                              (float *x,int m){
    int ncoeff= 42;
    float avdat= -0.6235946E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 43]={
         0.50097465E-03, 0.20191781E+00, 0.11924945E+00, 0.45844764E-03,
        -0.10696293E-02,-0.96739875E-03,-0.66080736E-03,-0.64340187E-03,
        -0.53714728E-03,-0.12859266E-03,-0.52286930E-04,-0.34792454E-03,
        -0.39821738E-04,-0.80275327E-04,-0.17858334E-04,-0.55697729E-03,
         0.86624095E-04,-0.47177088E-03,-0.74507552E-03,-0.50375296E-03,
        -0.69091046E-04,-0.13879916E-04,-0.46619778E-04,-0.44076846E-03,
        -0.58560374E-04, 0.10327656E-03, 0.58687008E-04, 0.75320895E-04,
        -0.13826023E-03,-0.33092778E-03,-0.19040513E-03,-0.66491467E-03,
        -0.19038421E-03,-0.25460683E-03,-0.16157632E-04,-0.20357520E-04,
        -0.43713622E-04,-0.76083001E-04, 0.33845525E-04, 0.12295885E-04,
         0.34986555E-04, 0.32360116E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_175                              =v_y_e_q2ex_2_175                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]        *x33        
        +coeff[ 10]            *x45    
        +coeff[ 11]            *x43    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q2ex_2_175                              =v_y_e_q2ex_2_175                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x31*x44    
        +coeff[ 20]    *x24    *x43    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x31*x42*x52
        +coeff[ 23]    *x22*x32*x45    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_q2ex_2_175                              =v_y_e_q2ex_2_175                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]        *x32*x41*x51
        +coeff[ 28]        *x31*x44    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]        *x32*x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]        *x33*x42    
        +coeff[ 33]    *x22*x33        
        +coeff[ 34]    *x21    *x45    
    ;
    v_y_e_q2ex_2_175                              =v_y_e_q2ex_2_175                              
        +coeff[ 35]*x11        *x43*x51
        +coeff[ 36]        *x31*x42*x52
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]            *x45*x51
        +coeff[ 39]*x13        *x42    
        +coeff[ 40]*x11*x21    *x43*x51
        +coeff[ 41]*x11    *x31*x43*x51
        ;

    return v_y_e_q2ex_2_175                              ;
}
float p_e_q2ex_2_175                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.1067353E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.94429874E-04,-0.20614246E-01,-0.19720225E-01, 0.18849041E-02,
         0.29306053E-02,-0.64095482E-03, 0.14517969E-04,-0.41821337E-03,
        -0.60156430E-03,-0.32516048E-03,-0.14591243E-03, 0.49205610E-05,
        -0.17267087E-03,-0.36973841E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_175                              =v_p_e_q2ex_2_175                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_175                              ;
}
float l_e_q2ex_2_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2943792E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.29110310E-02,-0.51529096E-02,-0.76382310E-03,-0.48150257E-02,
        -0.58442610E-02,-0.25457335E-04,-0.61623589E-03,-0.14073863E-02,
         0.13725882E-03, 0.58868180E-04,-0.11411262E-03, 0.94846437E-04,
         0.19452647E-03,-0.12356241E-03, 0.32206182E-03,-0.84307692E-04,
         0.59388834E-03,-0.66638626E-04,-0.19159042E-02, 0.48950542E-03,
         0.11178722E-03, 0.75528654E-03,-0.25734450E-02,-0.27255403E-03,
        -0.12484251E-02,-0.73600805E-03,-0.22027186E-03, 0.53109368E-03,
         0.78494784E-04, 0.80068759E-03,-0.12728978E-03,-0.45231618E-02,
         0.94127361E-04,-0.11756442E-02, 0.62426989E-03,-0.69275138E-03,
         0.94644952E-03, 0.15259309E-02,-0.93071029E-03,-0.44243361E-03,
         0.11312622E-02, 0.18909451E-02, 0.75684342E-03,-0.78872021E-03,
         0.43677425E-03,-0.39163610E-03,-0.54941420E-03,-0.98723208E-03,
         0.16237727E-02,-0.13719932E-02, 0.19187037E-02, 0.18229943E-02,
        -0.23366204E-02, 0.62811835E-03, 0.13241720E-02,-0.98873989E-03,
         0.15060292E-02,-0.20583961E-02, 0.36853468E-02, 0.33672566E-02,
        -0.16342899E-02, 0.56541325E-02,-0.32992661E-02,-0.15687033E-02,
         0.89031237E-03, 0.26057537E-02,-0.16879435E-02, 0.10517053E-02,
         0.12784934E-03, 0.66972693E-03, 0.19527889E-03,-0.23165761E-04,
        -0.11671731E-03, 0.13854986E-03, 0.14453974E-03, 0.15372506E-03,
         0.11318435E-03, 0.36472417E-03,-0.14532373E-03,-0.16296770E-03,
        -0.34026057E-03, 0.79338008E-03, 0.88049291E-03,-0.73958351E-03,
         0.54964441E-03, 0.24000114E-03,-0.27445567E-03, 0.12109331E-03,
         0.31657622E-03,-0.13142679E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]        *x34        
        +coeff[  7]*x11*x23        *x52
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[  8]        *x31        
        +coeff[  9]            *x41    
        +coeff[ 10]                *x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x23        *x51
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 17]    *x21    *x41*x52
        +coeff[ 18]*x11*x21*x31    *x51
        +coeff[ 19]*x11        *x42*x51
        +coeff[ 20]*x12*x21*x31        
        +coeff[ 21]*x13*x21            
        +coeff[ 22]    *x21*x32    *x52
        +coeff[ 23]        *x33    *x52
        +coeff[ 24]*x11*x22    *x41*x51
        +coeff[ 25]*x11*x21    *x42*x51
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 26]*x11*x21        *x53
        +coeff[ 27]*x12    *x32*x41    
        +coeff[ 28]*x12*x22        *x51
        +coeff[ 29]*x12*x21    *x41*x51
        +coeff[ 30]*x13    *x31*x41    
        +coeff[ 31]    *x23*x32*x41    
        +coeff[ 32]    *x23*x32    *x51
        +coeff[ 33]*x13        *x41*x51
        +coeff[ 34]    *x23*x31    *x52
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 35]    *x21*x32    *x53
        +coeff[ 36]*x11*x23*x31    *x51
        +coeff[ 37]*x11*x21*x33    *x51
        +coeff[ 38]*x11*x22    *x42*x51
        +coeff[ 39]*x11*x22*x31    *x52
        +coeff[ 40]*x11*x21        *x54
        +coeff[ 41]*x12*x21*x32    *x51
        +coeff[ 42]*x12        *x42*x52
        +coeff[ 43]*x12*x21        *x53
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 44]    *x23*x34        
        +coeff[ 45]*x13    *x32*x41    
        +coeff[ 46]    *x23    *x43*x51
        +coeff[ 47]*x13*x21        *x52
        +coeff[ 48]    *x21*x34    *x52
        +coeff[ 49]*x11*x24    *x42    
        +coeff[ 50]*x11*x24*x31    *x51
        +coeff[ 51]*x11*x22*x32*x41*x51
        +coeff[ 52]*x11*x22*x31*x42*x51
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 53]*x11    *x31*x41*x54
        +coeff[ 54]*x12*x22    *x42*x51
        +coeff[ 55]*x12*x23        *x52
        +coeff[ 56]*x12*x21*x32    *x52
        +coeff[ 57]*x13*x22*x31*x41    
        +coeff[ 58]    *x23*x32*x43    
        +coeff[ 59]*x13*x22    *x41*x51
        +coeff[ 60]    *x23*x33    *x52
        +coeff[ 61]    *x23*x32*x41*x52
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 62]    *x21*x34*x41*x52
        +coeff[ 63]        *x34*x42*x52
        +coeff[ 64]*x13*x21        *x53
        +coeff[ 65]    *x23*x32    *x53
        +coeff[ 66]    *x21*x34    *x53
        +coeff[ 67]        *x33*x42*x53
        +coeff[ 68]*x11                
        +coeff[ 69]    *x21    *x41    
        +coeff[ 70]    *x21        *x51
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 71]            *x41*x51
        +coeff[ 72]    *x22        *x51
        +coeff[ 73]    *x21*x31    *x51
        +coeff[ 74]        *x31*x41*x51
        +coeff[ 75]                *x53
        +coeff[ 76]*x11*x21*x31        
        +coeff[ 77]*x11        *x41*x51
        +coeff[ 78]*x12        *x41    
        +coeff[ 79]*x13                
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 80]    *x23    *x41    
        +coeff[ 81]    *x21*x32*x41    
        +coeff[ 82]        *x32*x42    
        +coeff[ 83]    *x21    *x43    
        +coeff[ 84]        *x31*x43    
        +coeff[ 85]            *x44    
        +coeff[ 86]    *x22*x31    *x51
        +coeff[ 87]        *x33    *x51
        +coeff[ 88]        *x32*x41*x51
    ;
    v_l_e_q2ex_2_175                              =v_l_e_q2ex_2_175                              
        +coeff[ 89]    *x22        *x52
        ;

    return v_l_e_q2ex_2_175                              ;
}
float x_e_q2ex_2_150                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.4955917E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.47663841E-03,-0.43557268E-02, 0.20544341E+00, 0.90786554E-02,
         0.42325858E-03,-0.14935371E-02,-0.40899841E-02,-0.27674872E-02,
         0.35422963E-04,-0.27222002E-04,-0.71939530E-05,-0.20734209E-02,
        -0.44918002E-03,-0.22533818E-02,-0.18627278E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_150                              =v_x_e_q2ex_2_150                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]*x12*x21            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_q2ex_2_150                              ;
}
float t_e_q2ex_2_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1397989E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13384603E-03,-0.23824868E-02, 0.58211263E-01, 0.22738876E-02,
        -0.14066353E-02,-0.13110040E-02,-0.59444126E-03, 0.17036690E-03,
        -0.46810403E-03,-0.62793627E-03,-0.18066712E-03,-0.17637692E-02,
        -0.12278407E-02,-0.52073945E-04, 0.24101243E-03, 0.16711545E-03,
        -0.14420392E-02,-0.11509076E-02,-0.20065418E-04,-0.35476678E-04,
        -0.10292527E-04, 0.97469703E-04, 0.10378617E-04,-0.65534498E-03,
         0.22295388E-04,-0.29775585E-04, 0.91195870E-05,-0.19729883E-04,
         0.31465941E-04,-0.88939400E-04,-0.25792880E-04,-0.55933371E-04,
         0.69239752E-06, 0.44414787E-05, 0.21828500E-05, 0.54314723E-05,
        -0.92612311E-06,-0.33372430E-05,-0.61125888E-05, 0.83951190E-05,
        -0.72419980E-05, 0.60058665E-05,-0.59622080E-05, 0.64803726E-05,
         0.37773455E-06,-0.11074743E-04, 0.74105892E-05,-0.92076161E-05,
         0.63031043E-05,-0.53256194E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_150                              =v_t_e_q2ex_2_150                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_150                              =v_t_e_q2ex_2_150                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]    *x21*x33*x41    
        +coeff[ 24]    *x23        *x53
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2ex_2_150                              =v_t_e_q2ex_2_150                              
        +coeff[ 26]    *x21    *x41*x51
        +coeff[ 27]*x11*x21    *x42    
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x13        *x42    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x21            
        +coeff[ 33]    *x21*x31        
        +coeff[ 34]        *x32        
    ;
    v_t_e_q2ex_2_150                              =v_t_e_q2ex_2_150                              
        +coeff[ 35]    *x21    *x41    
        +coeff[ 36]            *x41*x51
        +coeff[ 37]*x11        *x41*x51
        +coeff[ 38]*x11            *x52
        +coeff[ 39]*x13*x21            
        +coeff[ 40]*x11*x23            
        +coeff[ 41]*x11*x21*x32        
        +coeff[ 42]    *x22    *x42    
        +coeff[ 43]*x13            *x51
    ;
    v_t_e_q2ex_2_150                              =v_t_e_q2ex_2_150                              
        +coeff[ 44]*x12    *x31    *x51
        +coeff[ 45]    *x22*x31    *x51
        +coeff[ 46]*x11*x21    *x41*x51
        +coeff[ 47]    *x22    *x41*x51
        +coeff[ 48]*x11        *x42*x51
        +coeff[ 49]        *x31*x41*x52
        ;

    return v_t_e_q2ex_2_150                              ;
}
float y_e_q2ex_2_150                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.7388571E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.16742964E-03, 0.20141412E+00, 0.11875850E+00, 0.47233651E-03,
        -0.10439769E-02,-0.96807833E-03,-0.63128438E-03,-0.76145580E-03,
        -0.54895261E-03,-0.10508949E-05,-0.27103882E-03,-0.40014953E-03,
        -0.13541229E-03,-0.34124296E-04,-0.73573319E-04,-0.18494307E-04,
        -0.57508785E-03,-0.90691166E-04,-0.36221896E-04,-0.85022196E-03,
        -0.48152645E-03,-0.70641469E-03,-0.83588020E-04,-0.81519102E-05,
        -0.11012127E-04, 0.99065701E-05, 0.70453869E-04,-0.14542445E-04,
         0.67687339E-04,-0.27263924E-03,-0.89302244E-04,-0.90034933E-04,
         0.12975169E-03, 0.13388242E-03, 0.42237516E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_150                              =v_y_e_q2ex_2_150                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]    *x22*x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x33        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_150                              =v_y_e_q2ex_2_150                              
        +coeff[ 17]    *x24*x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31*x42    
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22    *x45    
        +coeff[ 23]            *x42*x51
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]*x11        *x43    
    ;
    v_y_e_q2ex_2_150                              =v_y_e_q2ex_2_150                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]        *x31*x44*x51
        +coeff[ 33]        *x32*x43*x51
        +coeff[ 34]*x13*x21    *x41    
        ;

    return v_y_e_q2ex_2_150                              ;
}
float p_e_q2ex_2_150                              (float *x,int m){
    int ncoeff= 13;
    float avdat=  0.3293168E-05;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.67216411E-05,-0.20644344E-01,-0.19760218E-01, 0.18816678E-02,
         0.29290856E-02,-0.65141829E-03,-0.41026392E-03,-0.60203730E-03,
        -0.32476205E-03,-0.14328437E-03, 0.18334636E-04,-0.16887295E-03,
        -0.38001896E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_150                              =v_p_e_q2ex_2_150                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]        *x33    *x52
        +coeff[ 12]        *x32*x41    
        ;

    return v_p_e_q2ex_2_150                              ;
}
float l_e_q2ex_2_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2926830E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.30456078E-02, 0.14719526E-03,-0.53733606E-02,-0.16619159E-02,
        -0.48799980E-02,-0.60446542E-02, 0.23662968E-03,-0.44641513E-03,
         0.19381205E-03, 0.14094134E-03,-0.66999590E-03, 0.48354969E-03,
         0.16214403E-03,-0.35828821E-03,-0.56060485E-03, 0.18164582E-03,
         0.29417485E-03, 0.55179710E-03, 0.91389357E-03,-0.10546318E-02,
        -0.11852852E-02,-0.19536947E-02,-0.23384220E-02,-0.18334623E-03,
        -0.23114368E-03,-0.92922780E-03, 0.18234510E-02, 0.76405809E-03,
        -0.43059999E-03, 0.45225117E-03, 0.26423877E-03, 0.85319247E-03,
        -0.21355851E-02,-0.79716666E-03, 0.32057837E-03, 0.31017140E-03,
         0.46759178E-02, 0.56301020E-02, 0.10467906E-02,-0.76818891E-03,
         0.29170301E-02, 0.31755440E-04, 0.50110649E-03, 0.59099408E-03,
        -0.98804594E-03,-0.31808573E-02,-0.15238668E-02,-0.50404505E-03,
         0.18015323E-02, 0.20950795E-02, 0.33607553E-02, 0.32029644E-03,
         0.14495682E-02,-0.47181009E-04,-0.11377707E-03, 0.42905045E-04,
        -0.49814214E-04, 0.28275041E-03,-0.35413416E-03, 0.29800710E-03,
         0.32805238E-03, 0.38837097E-03, 0.16115259E-03, 0.58980059E-03,
         0.49113354E-03, 0.27479921E-03,-0.29471968E-03, 0.37799895E-03,
        -0.16905802E-03,-0.42964550E-03,-0.45743480E-03,-0.12827075E-03,
        -0.10709305E-03,-0.29411929E-03, 0.24822928E-03, 0.63183375E-04,
        -0.14443640E-03, 0.29570164E-03,-0.20909245E-03, 0.27414027E-03,
         0.34779415E-03,-0.18855039E-03,-0.19657575E-03, 0.15795133E-03,
        -0.16018828E-03,-0.24449744E-03,-0.69523667E-03, 0.34621288E-03,
         0.13312377E-02, 0.76166756E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[  8]            *x41*x51
        +coeff[  9]                *x52
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]*x11    *x31*x41    
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x13                
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 17]            *x44    
        +coeff[ 18]    *x22    *x43    
        +coeff[ 19]        *x32*x43    
        +coeff[ 20]    *x23*x31    *x51
        +coeff[ 21]    *x21*x31*x41*x52
        +coeff[ 22]    *x21    *x42*x52
        +coeff[ 23]*x11*x22    *x42    
        +coeff[ 24]*x11        *x44    
        +coeff[ 25]*x11*x21*x31*x41*x51
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 26]*x11    *x31*x41*x52
        +coeff[ 27]*x12*x21    *x41*x51
        +coeff[ 28]*x12        *x41*x52
        +coeff[ 29]*x13*x21        *x51
        +coeff[ 30]*x13            *x52
        +coeff[ 31]*x11*x21*x32    *x52
        +coeff[ 32]*x12*x21    *x42*x51
        +coeff[ 33]*x12*x21    *x41*x52
        +coeff[ 34]*x12*x21        *x53
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 35]*x13    *x33        
        +coeff[ 36]    *x23*x32*x42    
        +coeff[ 37]    *x23*x31*x43    
        +coeff[ 38]    *x23*x33    *x51
        +coeff[ 39]    *x23*x32    *x52
        +coeff[ 40]    *x21    *x44*x52
        +coeff[ 41]        *x31*x44*x52
        +coeff[ 42]        *x34    *x53
        +coeff[ 43]    *x22*x31    *x54
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 44]        *x31*x42*x54
        +coeff[ 45]*x11    *x31*x41*x54
        +coeff[ 46]*x12*x22*x32    *x51
        +coeff[ 47]*x12*x22*x31*x41*x51
        +coeff[ 48]*x12*x22    *x42*x51
        +coeff[ 49]*x13    *x31*x41*x52
        +coeff[ 50]    *x22*x33*x41*x52
        +coeff[ 51]    *x21    *x44*x53
        +coeff[ 52]    *x22*x32    *x54
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 53]*x11                
        +coeff[ 54]*x12                
        +coeff[ 55]    *x23            
        +coeff[ 56]    *x21*x31*x41    
        +coeff[ 57]        *x32*x41    
        +coeff[ 58]*x11            *x52
        +coeff[ 59]    *x23*x31        
        +coeff[ 60]    *x22*x32        
        +coeff[ 61]        *x34        
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 62]    *x21*x32*x41    
        +coeff[ 63]        *x32*x42    
        +coeff[ 64]        *x31*x43    
        +coeff[ 65]    *x22    *x41*x51
        +coeff[ 66]        *x32*x41*x51
        +coeff[ 67]    *x21    *x42*x51
        +coeff[ 68]    *x21*x31    *x52
        +coeff[ 69]        *x32    *x52
        +coeff[ 70]        *x31*x41*x52
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 71]    *x21        *x53
        +coeff[ 72]*x11*x22*x31        
        +coeff[ 73]*x11*x21*x32        
        +coeff[ 74]*x11*x21    *x42    
        +coeff[ 75]*x11    *x31*x41*x51
        +coeff[ 76]*x11        *x42*x51
        +coeff[ 77]*x11        *x41*x52
        +coeff[ 78]*x12*x21*x31        
        +coeff[ 79]*x12        *x42    
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 80]*x12*x21        *x51
        +coeff[ 81]*x12        *x41*x51
        +coeff[ 82]*x13*x21            
        +coeff[ 83]    *x24*x31        
        +coeff[ 84]*x13        *x41    
        +coeff[ 85]    *x24    *x41    
        +coeff[ 86]    *x23*x31*x41    
        +coeff[ 87]    *x22*x32*x41    
        +coeff[ 88]    *x21*x33*x41    
    ;
    v_l_e_q2ex_2_150                              =v_l_e_q2ex_2_150                              
        +coeff[ 89]    *x21*x32*x42    
        ;

    return v_l_e_q2ex_2_150                              ;
}
float x_e_q2ex_2_125                              (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.4789355E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.49494242E-03,-0.42890222E-02, 0.20770226E+00, 0.89773573E-02,
         0.41575445E-03,-0.49876678E-02,-0.35420498E-02,-0.33797266E-05,
         0.29329906E-03,-0.16979445E-02,-0.20914178E-02,-0.46031125E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23        *x52
    ;
    v_x_e_q2ex_2_125                              =v_x_e_q2ex_2_125                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2ex_2_125                              ;
}
float t_e_q2ex_2_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1288034E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13363514E-03,-0.23554261E-02, 0.59077561E-01, 0.22111814E-02,
        -0.13915066E-02,-0.13189254E-02,-0.59191626E-03, 0.16799448E-03,
        -0.46435493E-03,-0.61602012E-03,-0.17162478E-03,-0.17410087E-02,
        -0.11986949E-02,-0.62239749E-04, 0.19137975E-03, 0.17992643E-03,
        -0.15026331E-02,-0.11960123E-02, 0.11828914E-04,-0.49852286E-04,
        -0.33574666E-04,-0.96942786E-05,-0.68384613E-03, 0.22213044E-05,
        -0.19466643E-04,-0.11727874E-04, 0.82129292E-04, 0.34409659E-04,
         0.19509529E-04,-0.96197946E-05,-0.30160714E-04,-0.23201837E-04,
        -0.17446802E-04, 0.35965400E-04, 0.79592923E-04,-0.50042254E-05,
        -0.56541685E-04, 0.10255217E-05,-0.33925910E-05, 0.28693089E-05,
        -0.54596821E-05, 0.39948450E-05, 0.78128551E-05, 0.65764775E-05,
        -0.57314219E-05, 0.81163435E-05, 0.33289965E-04,-0.42005790E-05,
         0.62570316E-05, 0.14298420E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_125                              =v_t_e_q2ex_2_125                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x41*x51
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q2ex_2_125                              =v_t_e_q2ex_2_125                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x23*x32    *x51
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]*x13                
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2ex_2_125                              =v_t_e_q2ex_2_125                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x12*x21    *x42    
        +coeff[ 29]*x11*x23        *x51
        +coeff[ 30]*x12*x21*x31    *x51
        +coeff[ 31]    *x21*x32    *x52
        +coeff[ 32]*x13*x21*x31    *x51
        +coeff[ 33]    *x23    *x42*x51
        +coeff[ 34]    *x21*x31*x43*x51
    ;
    v_t_e_q2ex_2_125                              =v_t_e_q2ex_2_125                              
        +coeff[ 35]    *x23        *x53
        +coeff[ 36]    *x21    *x42*x53
        +coeff[ 37]                *x51
        +coeff[ 38]    *x22            
        +coeff[ 39]        *x32        
        +coeff[ 40]    *x22*x31        
        +coeff[ 41]        *x31*x42    
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q2ex_2_125                              =v_t_e_q2ex_2_125                              
        +coeff[ 44]*x12        *x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x23        *x51
        +coeff[ 47]*x12    *x31    *x51
        +coeff[ 48]*x11            *x53
        +coeff[ 49]*x13*x22            
        ;

    return v_t_e_q2ex_2_125                              ;
}
float y_e_q2ex_2_125                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.4412349E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.45951674E-03, 0.20076165E+00, 0.11799827E+00, 0.52017864E-03,
        -0.10211311E-02,-0.10821186E-02,-0.68119977E-03,-0.76498627E-03,
        -0.56494703E-03, 0.10283598E-03,-0.23806911E-03,-0.51685417E-03,
        -0.13957647E-03,-0.34693410E-04,-0.61997642E-04,-0.52173488E-03,
        -0.56102126E-04,-0.31416566E-04, 0.68250090E-04,-0.44494140E-03,
        -0.91214992E-04,-0.11087739E-03, 0.81466191E-04,-0.11894632E-04,
        -0.67135325E-03,-0.13610857E-04,-0.51503361E-03,-0.22523258E-04,
        -0.19463499E-04,-0.26435006E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q2ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_125                              =v_y_e_q2ex_2_125                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]    *x22*x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x33        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x23    *x41    
    ;
    v_y_e_q2ex_2_125                              =v_y_e_q2ex_2_125                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]    *x22*x34*x41    
        +coeff[ 22]    *x22    *x41*x53
        +coeff[ 23]*x12        *x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]        *x33*x42    
    ;
    v_y_e_q2ex_2_125                              =v_y_e_q2ex_2_125                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x12        *x42*x51
        +coeff[ 29]    *x22    *x45    
        ;

    return v_y_e_q2ex_2_125                              ;
}
float p_e_q2ex_2_125                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.4054507E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.43820059E-04,-0.20673955E-01,-0.19822620E-01, 0.18751515E-02,
         0.29245834E-02,-0.66657725E-03, 0.14915297E-04,-0.43192261E-03,
        -0.59918873E-03,-0.32445139E-03,-0.14525182E-03, 0.12890153E-04,
        -0.17214782E-03,-0.37318037E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q2ex_2_125                              =v_p_e_q2ex_2_125                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]        *x33    *x52
        +coeff[ 13]        *x32*x41    
        ;

    return v_p_e_q2ex_2_125                              ;
}
float l_e_q2ex_2_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2990039E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.28686731E-02,-0.11907235E-03,-0.54118559E-02,-0.86866098E-03,
        -0.46086316E-02,-0.52990676E-02, 0.25158960E-03,-0.19811463E-03,
         0.22764638E-03,-0.90988382E-03, 0.67277142E-03,-0.19058758E-04,
        -0.40557314E-03, 0.17124576E-03, 0.58012619E-03, 0.97942670E-04,
         0.32113254E-03,-0.69599040E-03, 0.13623279E-03,-0.54860220E-03,
        -0.57307282E-03,-0.45696946E-03,-0.27610332E-03,-0.87370026E-04,
         0.36572351E-03,-0.57954830E-03,-0.36554792E-03, 0.16846093E-02,
        -0.92097197E-03, 0.44713353E-03,-0.10769359E-02, 0.10601657E-02,
        -0.74608758E-03,-0.12246043E-03,-0.15835151E-03,-0.45957314E-03,
        -0.46039955E-03,-0.70118735E-03, 0.65433048E-03,-0.18714730E-02,
         0.67129557E-04, 0.11872931E-02, 0.14007046E-02, 0.63375419E-03,
        -0.93767385E-03,-0.12132031E-02, 0.28555153E-03,-0.84214454E-03,
        -0.10977644E-02, 0.10216935E-02,-0.65473805E-03,-0.24277968E-02,
        -0.17583200E-02, 0.60649391E-03,-0.79195661E-03, 0.20697238E-02,
         0.44824029E-02, 0.31056199E-02,-0.12079311E-02, 0.10075928E-02,
         0.24034034E-02,-0.78770588E-03, 0.56410325E-03,-0.89545560E-03,
        -0.10456319E-03, 0.82921564E-04, 0.15321553E-03, 0.10400474E-03,
        -0.84731269E-04, 0.19510192E-03,-0.87309803E-04, 0.15541106E-03,
         0.18212295E-03, 0.22529597E-03,-0.87951499E-04,-0.96777419E-03,
         0.37730730E-03,-0.37304705E-03, 0.10145424E-02, 0.25911481E-03,
        -0.18048643E-03, 0.31123968E-03,-0.13766857E-03,-0.28184601E-03,
        -0.21801610E-03, 0.22896501E-03, 0.16081380E-03,-0.13700061E-03,
         0.14107530E-02, 0.98152296E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]*x11            *x51
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[  8]*x12                
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]            *x42*x51
        +coeff[ 16]                *x53
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 17]*x11*x21        *x51
        +coeff[ 18]*x12            *x51
        +coeff[ 19]        *x32    *x52
        +coeff[ 20]        *x31*x41*x52
        +coeff[ 21]*x11*x21*x32        
        +coeff[ 22]*x11    *x33        
        +coeff[ 23]*x11*x22    *x41    
        +coeff[ 24]*x11*x21    *x41*x51
        +coeff[ 25]*x12    *x32        
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 26]*x12    *x31    *x51
        +coeff[ 27]        *x33*x42    
        +coeff[ 28]    *x21    *x44    
        +coeff[ 29]*x13            *x51
        +coeff[ 30]    *x22*x31*x41*x51
        +coeff[ 31]*x11*x23        *x51
        +coeff[ 32]*x11*x22*x31    *x51
        +coeff[ 33]*x11    *x33    *x51
        +coeff[ 34]*x12    *x33        
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]*x12        *x41*x52
        +coeff[ 37]    *x23*x33        
        +coeff[ 38]    *x21*x34*x41    
        +coeff[ 39]        *x34*x41*x51
        +coeff[ 40]    *x24        *x52
        +coeff[ 41]*x11*x23*x31    *x51
        +coeff[ 42]*x11*x23    *x41*x51
        +coeff[ 43]*x11*x22*x31*x41*x51
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 44]*x11*x21*x31*x42*x51
        +coeff[ 45]*x11*x21    *x41*x53
        +coeff[ 46]*x11    *x31    *x54
        +coeff[ 47]*x12    *x33*x41    
        +coeff[ 48]*x12*x21*x31*x41*x51
        +coeff[ 49]*x12    *x32*x41*x51
        +coeff[ 50]    *x23*x33    *x51
        +coeff[ 51]    *x24*x31*x41*x51
        +coeff[ 52]    *x22*x32*x42*x51
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 53]    *x23    *x43*x51
        +coeff[ 54]        *x31*x44*x52
        +coeff[ 55]    *x22*x31*x41*x53
        +coeff[ 56]*x11    *x33*x42*x51
        +coeff[ 57]*x11    *x32*x43*x51
        +coeff[ 58]*x12    *x33*x42    
        +coeff[ 59]*x12*x22*x31    *x52
        +coeff[ 60]    *x23*x33*x41*x51
        +coeff[ 61]*x13    *x31    *x53
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 62]    *x21*x34    *x53
        +coeff[ 63]            *x44*x54
        +coeff[ 64]                *x51
        +coeff[ 65]*x11                
        +coeff[ 66]    *x21*x31        
        +coeff[ 67]        *x31    *x51
        +coeff[ 68]        *x33        
        +coeff[ 69]    *x21    *x42    
        +coeff[ 70]*x11*x22            
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 71]*x11    *x32        
        +coeff[ 72]*x11    *x31*x41    
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x13                
        +coeff[ 75]    *x21*x32*x41    
        +coeff[ 76]        *x33*x41    
        +coeff[ 77]    *x21*x31*x41*x51
        +coeff[ 78]        *x32*x41*x51
        +coeff[ 79]    *x22        *x52
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 80]    *x21*x31    *x52
        +coeff[ 81]            *x42*x52
        +coeff[ 82]            *x41*x53
        +coeff[ 83]*x11*x21        *x52
        +coeff[ 84]*x11            *x53
        +coeff[ 85]*x12*x21    *x41    
        +coeff[ 86]*x13*x21            
        +coeff[ 87]*x13        *x41    
        +coeff[ 88]    *x23*x31*x41    
    ;
    v_l_e_q2ex_2_125                              =v_l_e_q2ex_2_125                              
        +coeff[ 89]    *x23    *x42    
        ;

    return v_l_e_q2ex_2_125                              ;
}
float x_e_q2ex_2_100                              (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.1532902E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.15911879E-02,-0.41834204E-02, 0.21092910E+00, 0.88245878E-02,
         0.41876786E-03,-0.16768009E-02,-0.48863138E-02,-0.34923530E-02,
         0.17195485E-04, 0.25276080E-03,-0.20343601E-02,-0.45779109E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2ex_2_100                              =v_x_e_q2ex_2_100                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2ex_2_100                              ;
}
float t_e_q2ex_2_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4319609E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.44814893E-03,-0.23254359E-02, 0.60373597E-01, 0.21619014E-02,
        -0.13802884E-02,-0.13007108E-02,-0.60633291E-03, 0.17341410E-03,
        -0.45369405E-03,-0.60850161E-03,-0.17880854E-03,-0.17450041E-02,
        -0.12044008E-02,-0.19299356E-04,-0.11213538E-02,-0.30782117E-04,
        -0.16782084E-04, 0.23170844E-03, 0.14036879E-03,-0.64455535E-05,
        -0.63877349E-03,-0.13974232E-02, 0.49257742E-04, 0.33633820E-04,
         0.76298356E-05,-0.53924446E-05,-0.10605199E-04,-0.86013861E-05,
        -0.19758165E-04, 0.61208528E-04, 0.33771750E-04, 0.34903544E-04,
         0.18091359E-04, 0.15851874E-04, 0.18180403E-05, 0.54161078E-05,
        -0.23086377E-04,-0.78385538E-05,-0.98689352E-05, 0.29139619E-04,
        -0.10140605E-04,-0.83676596E-05,-0.22724505E-04, 0.81325652E-05,
        -0.19837264E-04, 0.59154008E-05,-0.47889596E-04,-0.85462481E-04,
        -0.89617160E-05,-0.62079169E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q2ex_2_100                              =v_t_e_q2ex_2_100                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q2ex_2_100                              =v_t_e_q2ex_2_100                              
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x12*x21*x32    *x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x13                
        +coeff[ 25]    *x22*x31        
    ;
    v_t_e_q2ex_2_100                              =v_t_e_q2ex_2_100                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x11*x22    *x41    
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x22*x32*x41    
        +coeff[ 31]    *x23    *x42*x51
        +coeff[ 32]*x11*x23        *x52
        +coeff[ 33]*x11*x21            
        +coeff[ 34]                *x52
    ;
    v_t_e_q2ex_2_100                              =v_t_e_q2ex_2_100                              
        +coeff[ 35]*x12        *x41    
        +coeff[ 36]*x11*x23            
        +coeff[ 37]*x13            *x51
        +coeff[ 38]*x12*x21        *x51
        +coeff[ 39]    *x23        *x51
        +coeff[ 40]    *x22*x31    *x51
        +coeff[ 41]    *x22    *x41*x51
        +coeff[ 42]*x11*x21        *x52
        +coeff[ 43]*x11            *x53
    ;
    v_t_e_q2ex_2_100                              =v_t_e_q2ex_2_100                              
        +coeff[ 44]*x13*x22            
        +coeff[ 45]*x13*x21*x31        
        +coeff[ 46]*x11*x22*x32        
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]*x12    *x32*x41    
        +coeff[ 49]*x11*x22    *x42    
        ;

    return v_t_e_q2ex_2_100                              ;
}
float y_e_q2ex_2_100                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1552961E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.14253185E-02, 0.19962415E+00, 0.11695901E+00, 0.53929543E-03,
        -0.99376915E-03,-0.11122481E-02,-0.65601565E-03,-0.78834692E-03,
        -0.53541258E-03, 0.11670060E-03,-0.24643642E-03,-0.48542157E-03,
        -0.12542351E-03,-0.46300072E-04,-0.50595299E-04, 0.38485909E-05,
        -0.53901126E-03,-0.70082147E-04,-0.36295987E-03,-0.63312257E-03,
        -0.20955062E-03,-0.66876135E-04, 0.12677818E-03,-0.20487825E-04,
        -0.15079412E-04, 0.31051342E-04, 0.56211738E-04, 0.78559096E-04,
         0.65053828E-04,-0.66977192E-03,-0.17690274E-04,-0.52140102E-04,
        -0.65041786E-04, 0.26640257E-04,-0.25797146E-03, 0.28634689E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2ex_2_100                              =v_y_e_q2ex_2_100                              
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]    *x22*x33        
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x33        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11*x21    *x43    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q2ex_2_100                              =v_y_e_q2ex_2_100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x31*x44    
        +coeff[ 21]    *x24    *x43    
        +coeff[ 22]    *x24*x31*x42    
        +coeff[ 23]    *x24*x32*x41    
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_q2ex_2_100                              =v_y_e_q2ex_2_100                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]        *x31*x44    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x21    *x43*x51
        +coeff[ 31]            *x43*x52
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]    *x21*x31*x41*x52
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q2ex_2_100                              =v_y_e_q2ex_2_100                              
        +coeff[ 35]*x11    *x31*x43*x51
        ;

    return v_y_e_q2ex_2_100                              ;
}
float p_e_q2ex_2_100                              (float *x,int m){
    int ncoeff= 13;
    float avdat=  0.1821501E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
        -0.16826825E-03,-0.20736616E-01,-0.19903695E-01, 0.18654178E-02,
         0.29143926E-02,-0.68493665E-03,-0.42936328E-03,-0.59540250E-03,
        -0.32558179E-03,-0.14211499E-03, 0.16497297E-04,-0.16992098E-03,
        -0.36977109E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q2ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2ex_2_100                              =v_p_e_q2ex_2_100                              
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x34*x41    
        +coeff[ 11]        *x33    *x52
        +coeff[ 12]        *x32*x41    
        ;

    return v_p_e_q2ex_2_100                              ;
}
float l_e_q2ex_2_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3007323E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.30210912E-02,-0.53996393E-02,-0.12011658E-02,-0.45948233E-02,
        -0.54631685E-02, 0.42567210E-03,-0.29780983E-03,-0.36908535E-03,
         0.22748466E-03, 0.28405740E-03,-0.89200906E-03, 0.12929826E-03,
         0.28339538E-03,-0.40291491E-03, 0.56369550E-03, 0.77343179E-03,
        -0.23059080E-03,-0.46068561E-03, 0.10210079E-03,-0.13625287E-03,
        -0.21395892E-03, 0.51596441E-03,-0.13912941E-02,-0.18903783E-02,
         0.79607678E-03,-0.60305255E-03, 0.87962369E-03, 0.20733376E-02,
        -0.22600056E-03, 0.11856825E-03, 0.56399888E-03,-0.12428516E-03,
         0.47741825E-03, 0.13180505E-02,-0.21514259E-02,-0.10836311E-04,
         0.83929801E-03,-0.81788958E-03,-0.56261844E-04,-0.53952105E-03,
         0.10645414E-02, 0.12035221E-02, 0.31029957E-02,-0.10868929E-03,
        -0.28761204E-02,-0.22055481E-02,-0.15131095E-02, 0.71172923E-03,
         0.86571620E-03,-0.84901485E-03, 0.15371912E-02, 0.39141480E-04,
        -0.17949131E-03,-0.41096538E-03, 0.35090995E-03, 0.41246465E-04,
        -0.23405555E-03, 0.90079746E-04,-0.17701811E-03, 0.14337695E-03,
        -0.55648183E-03, 0.15132445E-03, 0.11002866E-03, 0.22104202E-03,
         0.10039974E-02,-0.17875842E-03,-0.21705656E-03, 0.18563928E-03,
        -0.38304742E-03, 0.20672528E-03, 0.18028119E-03,-0.45253951E-03,
        -0.33037510E-03, 0.28926996E-03, 0.27255906E-03, 0.21256560E-03,
        -0.23648307E-03, 0.14801137E-03,-0.68002252E-03,-0.60519332E-03,
         0.14195780E-03, 0.15688709E-03, 0.24587862E-03,-0.15290482E-03,
         0.21379211E-03,-0.23143290E-03,-0.19917257E-03,-0.27252256E-03,
         0.20014535E-03, 0.32077299E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x21*x31        
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[  8]            *x42*x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x12        *x41    
        +coeff[ 12]*x13                
        +coeff[ 13]        *x33*x41    
        +coeff[ 14]    *x21    *x42*x51
        +coeff[ 15]            *x42*x52
        +coeff[ 16]                *x54
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 17]*x11*x21*x32        
        +coeff[ 18]*x11    *x33        
        +coeff[ 19]*x12*x21        *x51
        +coeff[ 20]    *x23        *x52
        +coeff[ 21]        *x31*x41*x53
        +coeff[ 22]*x11*x24            
        +coeff[ 23]*x11*x23*x31        
        +coeff[ 24]*x11*x21*x32*x41    
        +coeff[ 25]*x11*x21*x31*x42    
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 26]*x11*x22        *x52
        +coeff[ 27]*x11*x21*x31    *x52
        +coeff[ 28]*x11        *x42*x52
        +coeff[ 29]*x11        *x41*x53
        +coeff[ 30]*x12    *x32    *x51
        +coeff[ 31]*x13        *x41*x51
        +coeff[ 32]    *x22*x31*x42*x51
        +coeff[ 33]*x12*x21    *x41*x52
        +coeff[ 34]    *x21*x34*x42    
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 35]    *x23*x31*x43    
        +coeff[ 36]    *x24*x32    *x51
        +coeff[ 37]    *x23*x33    *x51
        +coeff[ 38]    *x21*x33*x42*x51
        +coeff[ 39]        *x34    *x53
        +coeff[ 40]    *x21*x31*x42*x53
        +coeff[ 41]        *x32*x42*x53
        +coeff[ 42]*x11*x23*x33        
        +coeff[ 43]*x11    *x33*x42*x51
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 44]*x11*x21*x33    *x52
        +coeff[ 45]*x11    *x31*x42*x53
        +coeff[ 46]*x11*x22        *x54
        +coeff[ 47]*x12*x21*x34        
        +coeff[ 48]*x13*x23    *x41    
        +coeff[ 49]    *x23*x34*x41    
        +coeff[ 50]*x13    *x31*x42*x51
        +coeff[ 51]    *x21            
        +coeff[ 52]*x11                
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 53]    *x21        *x51
        +coeff[ 54]            *x41*x51
        +coeff[ 55]*x11    *x31        
        +coeff[ 56]*x11        *x41    
        +coeff[ 57]*x11            *x51
        +coeff[ 58]    *x23            
        +coeff[ 59]    *x21*x32        
        +coeff[ 60]    *x21*x31*x41    
        +coeff[ 61]    *x21    *x42    
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 62]        *x31*x42    
        +coeff[ 63]        *x31*x41*x51
        +coeff[ 64]*x11*x22            
        +coeff[ 65]*x12*x21            
        +coeff[ 66]    *x22*x32        
        +coeff[ 67]    *x22    *x42    
        +coeff[ 68]            *x44    
        +coeff[ 69]    *x23        *x51
        +coeff[ 70]        *x32*x41*x51
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 71]            *x43*x51
        +coeff[ 72]    *x21    *x41*x52
        +coeff[ 73]        *x31*x41*x52
        +coeff[ 74]    *x21        *x53
        +coeff[ 75]        *x31    *x53
        +coeff[ 76]*x11*x22*x31        
        +coeff[ 77]*x11*x22    *x41    
        +coeff[ 78]*x11    *x32*x41    
        +coeff[ 79]*x11    *x31*x42    
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 80]*x11*x22        *x51
        +coeff[ 81]*x11*x21    *x41*x51
        +coeff[ 82]*x11    *x31    *x52
        +coeff[ 83]*x11            *x53
        +coeff[ 84]*x12*x21*x31        
        +coeff[ 85]*x12    *x31    *x51
        +coeff[ 86]*x12        *x41*x51
        +coeff[ 87]    *x24*x31        
        +coeff[ 88]    *x22*x33        
    ;
    v_l_e_q2ex_2_100                              =v_l_e_q2ex_2_100                              
        +coeff[ 89]*x13        *x41    
        ;

    return v_l_e_q2ex_2_100                              ;
}
