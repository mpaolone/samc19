float x_e_q1en_2_1200                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.5412094E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.55107643E-03, 0.37395954E-02, 0.11696028E+00, 0.26560281E-03,
         0.10445822E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_1200                              ;
}
float t_e_q1en_2_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1566957E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15954013E-03,-0.10726290E-02, 0.30736744E-01, 0.11484658E-02,
        -0.18680628E-03, 0.42127758E-04,-0.61031285E-04,-0.10295346E-04,
        -0.44918805E-04, 0.13592554E-03, 0.10736797E-03, 0.11472701E-03,
         0.94961360E-04,-0.53633947E-04, 0.44442284E-04, 0.16235104E-04,
         0.51780808E-04, 0.12833414E-05,-0.17264962E-05, 0.15033223E-04,
        -0.17560638E-04,-0.76994029E-05,-0.68385486E-06, 0.23820658E-05,
        -0.58484111E-05, 0.26478049E-05,-0.22144727E-05,-0.25437473E-05,
        -0.41354560E-05, 0.37650111E-05, 0.77032500E-05,-0.38473131E-05,
        -0.28705174E-05, 0.53536955E-05, 0.41163457E-05, 0.12476392E-04,
         0.91452848E-05, 0.64773039E-05, 0.56600616E-05, 0.50983417E-05,
         0.58839746E-05,-0.60675889E-05,-0.64560691E-05, 0.45150614E-05,
        -0.60171833E-05,-0.62495847E-05, 0.27521464E-05,-0.35644032E-05,
         0.57964266E-05, 0.44093995E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_1200                              =v_t_e_q1en_2_1200                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_1200                              =v_t_e_q1en_2_1200                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11            *x52
        +coeff[ 19]*x12*x22*x31    *x51
        +coeff[ 20]    *x22*x33    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q1en_2_1200                              =v_t_e_q1en_2_1200                              
        +coeff[ 26]*x12        *x42    
        +coeff[ 27]*x12*x21        *x51
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x22*x31    *x51
        +coeff[ 31]*x11*x21    *x41*x51
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q1en_2_1200                              =v_t_e_q1en_2_1200                              
        +coeff[ 35]*x11*x22*x32        
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x12*x22        *x51
        +coeff[ 38]    *x21*x33    *x51
        +coeff[ 39]    *x23    *x41*x51
        +coeff[ 40]*x11*x21*x31*x41*x51
        +coeff[ 41]    *x21*x32*x41*x51
        +coeff[ 42]    *x21*x31*x42*x51
        +coeff[ 43]*x12*x21        *x52
    ;
    v_t_e_q1en_2_1200                              =v_t_e_q1en_2_1200                              
        +coeff[ 44]*x11*x22        *x52
        +coeff[ 45]*x11*x21*x31    *x52
        +coeff[ 46]        *x31*x42*x52
        +coeff[ 47]    *x22        *x53
        +coeff[ 48]*x12*x23*x31        
        +coeff[ 49]*x12*x22*x32        
        ;

    return v_t_e_q1en_2_1200                              ;
}
float y_e_q1en_2_1200                              (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.3084310E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.33966306E-03, 0.65371938E-01, 0.57591077E-01,-0.13931957E-03,
        -0.14772407E-03,-0.59145332E-04,-0.10114043E-03,-0.17132585E-04,
        -0.66000401E-04,-0.96849202E-04, 0.28846518E-04,-0.19344126E-04,
        -0.12194848E-04, 0.63384118E-05, 0.51467428E-05, 0.91561978E-05,
         0.15808164E-04,-0.30669671E-04,-0.91013579E-04,-0.62113693E-04,
        -0.79618067E-05,-0.11514619E-04,-0.13310422E-04, 0.79673828E-05,
        -0.84410985E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_1200                              =v_y_e_q1en_2_1200                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]    *x24*x33        
        +coeff[ 11]*x11*x21    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x42*x52
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1en_2_1200                              =v_y_e_q1en_2_1200                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x24*x31        
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]*x11    *x33*x41    
        +coeff[ 21]    *x21*x31*x41*x52
        +coeff[ 22]    *x21*x33*x42    
        +coeff[ 23]*x12        *x42*x51
        +coeff[ 24]*x11    *x34*x41    
        ;

    return v_y_e_q1en_2_1200                              ;
}
float p_e_q1en_2_1200                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.1244873E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.14667770E-03, 0.15196322E-01, 0.45401480E-01,-0.61912031E-03,
        -0.62564132E-03,-0.26391121E-03,-0.24119328E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_1200                              ;
}
float l_e_q1en_2_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1502722E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14284562E-02,-0.31716574E-02,-0.51414699E-03,-0.83222828E-03,
         0.93171705E-03,-0.70812157E-03, 0.67423080E-03,-0.93980704E-03,
         0.25204306E-02, 0.60157931E-05, 0.16604718E-04,-0.35463230E-03,
        -0.12966216E-03, 0.11884925E-03,-0.20256673E-03,-0.74486609E-03,
         0.68041141E-03,-0.47213121E-04,-0.30815549E-03, 0.21472586E-03,
         0.32719411E-03,-0.27906740E-03,-0.14000334E-02, 0.10673059E-03,
        -0.32501537E-03, 0.57512627E-03, 0.11996482E-03, 0.83289493E-03,
         0.17110379E-03,-0.52336598E-03,-0.48265010E-04, 0.30021899E-03,
        -0.49469352E-04,-0.34807331E-03, 0.11154310E-02, 0.73456962E-03,
        -0.60787954E-03, 0.11481434E-02,-0.86735556E-03,-0.48564246E-03,
         0.89353503E-03,-0.53422496E-03,-0.54883549E-03, 0.19860601E-02,
        -0.13213611E-02, 0.20864849E-03, 0.13060764E-02,-0.14112184E-03,
         0.40571860E-03,-0.49371656E-03, 0.61103003E-03, 0.30703787E-03,
         0.63546264E-03,-0.80997800E-03,-0.75295859E-03, 0.46394125E-03,
         0.70065504E-03,-0.12416840E-02,-0.94124622E-03,-0.48833509E-03,
         0.55496063E-03, 0.84904127E-03,-0.49956160E-03,-0.27616241E-03,
         0.36940462E-03,-0.99129952E-03,-0.27152817E-03, 0.17854606E-02,
         0.16754769E-02,-0.17074227E-02, 0.39053714E-03,-0.35627556E-03,
        -0.41136236E-03, 0.54627610E-03,-0.44674592E-03, 0.11350684E-02,
        -0.15559273E-02,-0.78840402E-03, 0.34767346E-03,-0.46022120E-03,
        -0.10491187E-02, 0.13093305E-02, 0.12638485E-02, 0.87209267E-03,
        -0.88862982E-03,-0.85293461E-03, 0.74443500E-03, 0.54646813E-03,
        -0.51761308E-03, 0.40083240E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]    *x21*x33*x41    
        +coeff[  5]*x11*x21*x32    *x51
        +coeff[  6]*x11    *x31*x41*x53
        +coeff[  7]*x12    *x32*x41*x51
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[  8]*x12*x23*x31    *x51
        +coeff[  9]            *x41    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11    *x32        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 17]*x13                
        +coeff[ 18]    *x24            
        +coeff[ 19]    *x23*x31        
        +coeff[ 20]        *x31*x43    
        +coeff[ 21]            *x44    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22        *x52
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]*x11*x21*x32        
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 26]*x11    *x33        
        +coeff[ 27]*x11*x21    *x42    
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x11    *x31    *x52
        +coeff[ 31]*x12    *x31*x41    
        +coeff[ 32]*x12*x21        *x51
        +coeff[ 33]    *x21*x33    *x51
        +coeff[ 34]    *x21*x31*x42*x51
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 35]        *x31*x43*x51
        +coeff[ 36]        *x31*x41*x53
        +coeff[ 37]*x11    *x34        
        +coeff[ 38]*x11    *x31*x43    
        +coeff[ 39]*x11*x22    *x41*x51
        +coeff[ 40]*x11    *x31*x42*x51
        +coeff[ 41]*x12    *x32*x41    
        +coeff[ 42]*x12*x21    *x41*x51
        +coeff[ 43]    *x24    *x41*x51
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 44]    *x21*x32*x42*x51
        +coeff[ 45]*x13            *x52
        +coeff[ 46]    *x21*x31*x42*x52
        +coeff[ 47]            *x44*x52
        +coeff[ 48]        *x33    *x53
        +coeff[ 49]    *x21*x31*x41*x53
        +coeff[ 50]    *x21    *x42*x53
        +coeff[ 51]            *x43*x53
        +coeff[ 52]*x11*x23*x32        
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 53]*x11*x21*x34        
        +coeff[ 54]*x11    *x34*x41    
        +coeff[ 55]*x11    *x33*x42    
        +coeff[ 56]*x11    *x32*x43    
        +coeff[ 57]*x11*x21    *x44    
        +coeff[ 58]*x11*x22*x31    *x52
        +coeff[ 59]*x12*x21*x31*x42    
        +coeff[ 60]*x12*x23        *x51
        +coeff[ 61]*x12*x22        *x52
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 62]*x12    *x31    *x53
        +coeff[ 63]*x12            *x54
        +coeff[ 64]        *x34*x43    
        +coeff[ 65]    *x23*x33    *x51
        +coeff[ 66]    *x22*x34    *x51
        +coeff[ 67]    *x22*x32*x42*x51
        +coeff[ 68]    *x21*x33*x42*x51
        +coeff[ 69]    *x21*x31*x44*x51
        +coeff[ 70]    *x21*x34    *x52
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 71]    *x21    *x43*x53
        +coeff[ 72]            *x44*x53
        +coeff[ 73]    *x22*x31    *x54
        +coeff[ 74]        *x31*x42*x54
        +coeff[ 75]*x11    *x32*x43*x51
        +coeff[ 76]*x11    *x31*x44*x51
        +coeff[ 77]*x11    *x32*x41*x53
        +coeff[ 78]*x12*x24    *x41    
        +coeff[ 79]*x12*x23    *x42    
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 80]*x12*x24        *x51
        +coeff[ 81]*x12*x23    *x41*x51
        +coeff[ 82]*x12*x21*x32*x41*x51
        +coeff[ 83]*x12*x22        *x53
        +coeff[ 84]*x12*x21*x31    *x53
        +coeff[ 85]    *x23*x33*x42    
        +coeff[ 86]    *x23*x34    *x51
        +coeff[ 87]    *x24*x31*x41*x52
        +coeff[ 88]    *x21*x33    *x54
    ;
    v_l_e_q1en_2_1200                              =v_l_e_q1en_2_1200                              
        +coeff[ 89]        *x31        
        ;

    return v_l_e_q1en_2_1200                              ;
}
float x_e_q1en_2_1100                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.4333792E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60064E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.42651562E-03, 0.37413253E-02, 0.11697071E+00, 0.26514745E-03,
         0.10144118E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_1100                              ;
}
float t_e_q1en_2_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1081293E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60064E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.10644984E-03,-0.10722539E-02, 0.30734999E-01, 0.11489019E-02,
        -0.18025780E-03, 0.40885505E-04,-0.55559889E-04, 0.26274211E-05,
        -0.41732652E-04, 0.12848251E-03, 0.10124301E-03, 0.99639765E-04,
         0.95187097E-04,-0.39570412E-04, 0.16265039E-05, 0.51638770E-04,
        -0.97095281E-05, 0.16976792E-05,-0.83663572E-05, 0.28539600E-04,
         0.17126220E-04,-0.13280543E-04, 0.49265964E-05,-0.91603988E-05,
         0.12200931E-04, 0.25693953E-06,-0.17080852E-05,-0.47321155E-05,
         0.33537212E-05,-0.64273314E-06, 0.34665814E-06, 0.21512781E-05,
         0.59577992E-05,-0.76249501E-06, 0.11353674E-04, 0.82720189E-05,
         0.23050934E-05,-0.88052284E-05,-0.63495991E-05, 0.64190554E-05,
        -0.49915420E-05, 0.57596658E-05, 0.78161665E-05,-0.71011732E-05,
         0.55979317E-05,-0.59272093E-05,-0.42028946E-05,-0.16806202E-04,
         0.14754067E-04,-0.76533252E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1en_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_1100                              =v_t_e_q1en_2_1100                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_1100                              =v_t_e_q1en_2_1100                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]*x11*x22*x31*x41    
        +coeff[ 21]    *x21    *x42*x52
        +coeff[ 22]*x13*x23            
        +coeff[ 23]*x12*x21*x31*x41*x51
        +coeff[ 24]*x11*x22    *x42*x51
        +coeff[ 25]        *x31*x41    
    ;
    v_t_e_q1en_2_1100                              =v_t_e_q1en_2_1100                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11*x21        *x51
        +coeff[ 28]    *x21    *x41*x51
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x11    *x33        
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x23        *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1en_2_1100                              =v_t_e_q1en_2_1100                              
        +coeff[ 35]*x11*x21*x32    *x51
        +coeff[ 36]    *x21*x33    *x51
        +coeff[ 37]    *x23    *x41*x51
        +coeff[ 38]*x11*x21*x31*x41*x51
        +coeff[ 39]*x11*x21*x31    *x52
        +coeff[ 40]*x11        *x42*x52
        +coeff[ 41]*x13*x21*x32        
        +coeff[ 42]*x12*x22*x32        
        +coeff[ 43]*x11*x22*x33        
    ;
    v_t_e_q1en_2_1100                              =v_t_e_q1en_2_1100                              
        +coeff[ 44]    *x22*x33*x41    
        +coeff[ 45]*x13*x21    *x42    
        +coeff[ 46]*x13*x21    *x41*x51
        +coeff[ 47]    *x23*x31*x41*x51
        +coeff[ 48]    *x21*x33*x41*x51
        +coeff[ 49]    *x22*x31*x42*x51
        ;

    return v_t_e_q1en_2_1100                              ;
}
float y_e_q1en_2_1100                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.9924583E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60064E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.10089122E-02, 0.65372646E-01, 0.57610936E-01,-0.14023886E-03,
        -0.14863235E-03,-0.90612732E-04,-0.61063532E-04,-0.15212065E-04,
        -0.66412809E-04,-0.54820989E-05,-0.15241973E-04,-0.79732687E-04,
        -0.57402380E-04,-0.58217687E-04, 0.23588469E-04, 0.42039010E-04,
        -0.38039647E-06,-0.70201636E-05, 0.13452081E-04,-0.83907571E-05,
         0.84360399E-05, 0.10985519E-04,-0.16491216E-04,-0.30459720E-04,
        -0.55189039E-05, 0.75616449E-05,-0.19581405E-04, 0.14739097E-04,
        -0.15732307E-04,-0.12588332E-04, 0.12924774E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_1100                              =v_y_e_q1en_2_1100                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x24    *x41    
        +coeff[ 12]    *x22*x32*x41    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22    *x41*x52
        +coeff[ 15]        *x33*x42*x52
        +coeff[ 16]        *x31*x41    
    ;
    v_y_e_q1en_2_1100                              =v_y_e_q1en_2_1100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x21*x31*x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x21    *x45    
        +coeff[ 25]*x12*x21    *x42    
    ;
    v_y_e_q1en_2_1100                              =v_y_e_q1en_2_1100                              
        +coeff[ 26]*x12*x22    *x41    
        +coeff[ 27]*x13    *x31*x41    
        +coeff[ 28]*x11    *x31*x41*x52
        +coeff[ 29]        *x31*x43*x52
        +coeff[ 30]*x11*x21*x33*x41    
        ;

    return v_y_e_q1en_2_1100                              ;
}
float p_e_q1en_2_1100                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.4257638E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60064E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.44113101E-03, 0.15199332E-01, 0.45405671E-01,-0.61956549E-03,
        -0.62509533E-03,-0.26629181E-03,-0.24088215E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_1100                              ;
}
float l_e_q1en_2_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1479918E-02;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60064E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14827497E-02,-0.34692707E-02,-0.84956677E-03,-0.15299660E-03,
         0.26993171E-03,-0.16303723E-03,-0.86755533E-03,-0.18637545E-02,
         0.22525529E-02, 0.18438428E-02, 0.15514999E-03,-0.51768202E-04,
        -0.21220061E-04, 0.14943318E-04,-0.17877061E-03,-0.98887438E-04,
        -0.95291754E-04,-0.22661018E-03, 0.38714954E-03, 0.73825722E-04,
         0.26322747E-03, 0.37273261E-03,-0.10065659E-02,-0.20337828E-03,
         0.26484774E-03, 0.78007183E-03,-0.24711070E-03,-0.15111541E-03,
        -0.16098235E-03, 0.76555240E-03, 0.70936739E-03, 0.63122256E-03,
         0.55388425E-03,-0.52834855E-03,-0.26747785E-03, 0.13769825E-02,
        -0.97675913E-03,-0.11765010E-03,-0.81374351E-03, 0.44972874E-03,
         0.12919516E-02, 0.30825328E-03, 0.10439935E-02,-0.12145272E-02,
        -0.69887785E-03, 0.53830538E-03,-0.34163258E-03,-0.30965373E-03,
        -0.10400764E-02, 0.28088316E-03, 0.92796341E-03, 0.57436532E-03,
         0.44875627E-03,-0.12844990E-02, 0.39137641E-03,-0.17845641E-03,
        -0.66998106E-03,-0.11429462E-02,-0.11877702E-02, 0.39240267E-03,
        -0.52514730E-03,-0.46479708E-03,-0.35202637E-03, 0.90998056E-03,
         0.92812220E-03, 0.39172822E-03, 0.46699829E-03,-0.20367764E-02,
         0.13230407E-02,-0.14316926E-02,-0.28479769E-03, 0.22499040E-02,
        -0.32897387E-03, 0.74005662E-03,-0.38609721E-03,-0.57395385E-03,
        -0.45966628E-03,-0.84969861E-03,-0.11783985E-02,-0.14049476E-02,
         0.10091811E-02,-0.91164652E-03,-0.13920865E-02,-0.12354944E-03,
         0.10113434E-02, 0.89091313E-03,-0.14541552E-02, 0.76768233E-03,
         0.96558366E-03,-0.95929747E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x12                
        +coeff[  4]*x12*x22            
        +coeff[  5]    *x21*x33*x41    
        +coeff[  6]        *x33*x41*x52
        +coeff[  7]    *x22    *x42*x52
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[  8]*x11*x23    *x42*x51
        +coeff[  9]*x13*x22*x32        
        +coeff[ 10]        *x31        
        +coeff[ 11]*x11                
        +coeff[ 12]                *x52
        +coeff[ 13]*x11*x21            
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 17]    *x22*x31        
        +coeff[ 18]    *x21    *x42    
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]        *x34        
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 26]        *x32*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]*x11*x22*x31        
        +coeff[ 30]*x11    *x32    *x51
        +coeff[ 31]*x11    *x31*x41*x51
        +coeff[ 32]*x11    *x31    *x52
        +coeff[ 33]*x12        *x42    
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 35]    *x21*x32*x41*x51
        +coeff[ 36]    *x21*x31    *x53
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]*x11*x21*x33        
        +coeff[ 39]*x11*x23        *x51
        +coeff[ 40]*x11*x21*x31*x41*x51
        +coeff[ 41]*x11*x21    *x41*x52
        +coeff[ 42]*x11*x21        *x53
        +coeff[ 43]*x12*x21    *x42    
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 44]*x12*x21    *x41*x51
        +coeff[ 45]*x12*x21        *x52
        +coeff[ 46]*x12    *x31    *x52
        +coeff[ 47]    *x22*x34        
        +coeff[ 48]    *x23*x31*x42    
        +coeff[ 49]    *x22*x33    *x51
        +coeff[ 50]    *x22*x31*x41*x52
        +coeff[ 51]            *x44*x52
        +coeff[ 52]    *x22        *x54
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 53]*x11*x24*x31        
        +coeff[ 54]*x11*x21*x33*x41    
        +coeff[ 55]*x11*x21*x32*x42    
        +coeff[ 56]*x11*x22*x31*x41*x51
        +coeff[ 57]*x11    *x33*x41*x51
        +coeff[ 58]*x11    *x32*x42*x51
        +coeff[ 59]*x11        *x44*x51
        +coeff[ 60]*x11    *x33    *x52
        +coeff[ 61]*x11*x21    *x42*x52
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 62]*x11*x22        *x53
        +coeff[ 63]*x12*x22    *x42    
        +coeff[ 64]    *x21*x33*x43    
        +coeff[ 65]*x13*x21*x31    *x51
        +coeff[ 66]    *x24    *x42*x51
        +coeff[ 67]    *x23*x31*x42*x51
        +coeff[ 68]    *x21*x33*x42*x51
        +coeff[ 69]    *x21*x32*x43*x51
        +coeff[ 70]    *x23    *x42*x52
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 71]    *x22*x31*x42*x52
        +coeff[ 72]*x13            *x53
        +coeff[ 73]    *x23*x31    *x53
        +coeff[ 74]    *x23        *x54
        +coeff[ 75]    *x22*x31    *x54
        +coeff[ 76]*x11*x22*x34        
        +coeff[ 77]*x11*x21*x31*x44    
        +coeff[ 78]*x11*x24    *x41*x51
        +coeff[ 79]*x11*x21*x33*x41*x51
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 80]*x11*x22    *x43*x51
        +coeff[ 81]*x11*x22*x32    *x52
        +coeff[ 82]*x11*x22*x31*x41*x52
        +coeff[ 83]*x11    *x33*x41*x52
        +coeff[ 84]*x11*x21*x31*x42*x52
        +coeff[ 85]*x11    *x31*x43*x52
        +coeff[ 86]*x11*x21    *x42*x53
        +coeff[ 87]*x12*x23*x32        
        +coeff[ 88]*x12*x23    *x42    
    ;
    v_l_e_q1en_2_1100                              =v_l_e_q1en_2_1100                              
        +coeff[ 89]*x12*x21*x32*x42    
        ;

    return v_l_e_q1en_2_1100                              ;
}
float x_e_q1en_2_1000                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.5821612E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.56006288E-03, 0.37388112E-02, 0.11694275E+00, 0.27332688E-03,
         0.10705629E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_1000                              ;
}
float t_e_q1en_2_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1486917E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.14318510E-03,-0.10715636E-02, 0.30734900E-01, 0.11499394E-02,
        -0.18097372E-03, 0.41864321E-04,-0.69358452E-04,-0.81314938E-05,
        -0.47779908E-04, 0.13689813E-03, 0.10512237E-03, 0.11230292E-03,
         0.96590644E-04,-0.46466612E-04, 0.13902943E-05, 0.62936764E-04,
        -0.33250965E-05, 0.27867432E-04, 0.17261187E-04,-0.90815611E-05,
        -0.11508865E-05,-0.10873757E-04,-0.12628683E-05, 0.11039887E-05,
         0.41802073E-05,-0.53089839E-05,-0.20104048E-05,-0.64353676E-05,
         0.40163127E-05,-0.26411178E-05, 0.86044901E-06, 0.23317934E-05,
         0.32292553E-05, 0.35995049E-05,-0.19381052E-05, 0.25318818E-05,
         0.84412541E-05, 0.19399504E-04, 0.64803244E-05,-0.79848060E-05,
        -0.30457195E-05,-0.65196618E-05,-0.25634995E-05,-0.41964640E-05,
         0.63986790E-05, 0.64940850E-05, 0.91685924E-05,-0.69963830E-05,
        -0.46388168E-05, 0.51342672E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1en_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_1000                              =v_t_e_q1en_2_1000                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1en_2_1000                              =v_t_e_q1en_2_1000                              
        +coeff[ 17]    *x23*x32        
        +coeff[ 18]*x11*x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41*x51
        +coeff[ 20]                *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x22    *x41    
        +coeff[ 24]    *x21*x31    *x51
        +coeff[ 25]*x11*x23            
    ;
    v_t_e_q1en_2_1000                              =v_t_e_q1en_2_1000                              
        +coeff[ 26]*x12*x21*x31        
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]*x11*x21*x31*x41    
        +coeff[ 30]*x12        *x42    
        +coeff[ 31]    *x22    *x42    
        +coeff[ 32]    *x21*x31*x42    
        +coeff[ 33]*x11*x22        *x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_t_e_q1en_2_1000                              =v_t_e_q1en_2_1000                              
        +coeff[ 35]*x12*x22*x31        
        +coeff[ 36]*x11*x22*x32        
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]*x11    *x32*x42    
        +coeff[ 39]    *x21*x33    *x51
        +coeff[ 40]*x11*x22    *x41*x51
        +coeff[ 41]    *x21*x31*x42*x51
        +coeff[ 42]    *x21    *x43*x51
        +coeff[ 43]*x11    *x32    *x52
    ;
    v_t_e_q1en_2_1000                              =v_t_e_q1en_2_1000                              
        +coeff[ 44]    *x21*x32    *x52
        +coeff[ 45]*x13*x23            
        +coeff[ 46]*x13*x22*x31        
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]*x12*x23    *x41    
        +coeff[ 49]*x11*x22*x32*x41    
        ;

    return v_t_e_q1en_2_1000                              ;
}
float y_e_q1en_2_1000                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.5106081E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.49507996E-03, 0.65364495E-01, 0.57599206E-01,-0.14160281E-03,
        -0.14778352E-03,-0.48088659E-04,-0.64329724E-04,-0.11466222E-04,
        -0.83289233E-04, 0.16157559E-04, 0.33084336E-05,-0.14155219E-04,
        -0.72237184E-04,-0.71428236E-04, 0.10162081E-04, 0.65628756E-05,
         0.43546743E-06, 0.74618993E-05, 0.69237458E-05, 0.95428159E-05,
         0.10945696E-04,-0.84140765E-05, 0.10558210E-04,-0.87959510E-04,
        -0.63970059E-04,-0.27405145E-04,-0.15944213E-04, 0.70242254E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_1000                              =v_y_e_q1en_2_1000                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x24*x33        
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]    *x24    *x41    
        +coeff[ 13]    *x22*x32*x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]    *x21    *x43    
    ;
    v_y_e_q1en_2_1000                              =v_y_e_q1en_2_1000                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]*x11*x21    *x41*x51
        +coeff[ 22]    *x21    *x41*x52
        +coeff[ 23]    *x24*x31        
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23    *x41    
    ;
    v_y_e_q1en_2_1000                              =v_y_e_q1en_2_1000                              
        +coeff[ 26]    *x23    *x43    
        +coeff[ 27]    *x24    *x42    
        ;

    return v_y_e_q1en_2_1000                              ;
}
float p_e_q1en_2_1000                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2148880E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.20570852E-03, 0.15196859E-01, 0.45396920E-01,-0.61913871E-03,
        -0.62565901E-03,-0.26202711E-03,-0.23965453E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_1000                              ;
}
float l_e_q1en_2_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1466896E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14978171E-02,-0.33728620E-02,-0.39451182E-03,-0.11181858E-02,
         0.25203131E-03, 0.24741405E-03,-0.62371961E-04, 0.76416676E-03,
        -0.15668618E-03,-0.42119113E-03,-0.18144981E-03,-0.12286718E-03,
        -0.25112400E-03,-0.42028353E-03, 0.19599273E-03, 0.10032870E-03,
         0.23279074E-03, 0.19247692E-03,-0.15893512E-03,-0.37706990E-03,
         0.24631116E-03,-0.47967158E-03,-0.34555292E-03, 0.14101320E-03,
         0.16919794E-02,-0.12347614E-02,-0.33659246E-03,-0.36369232E-03,
         0.10254711E-02, 0.38316564E-04,-0.11736114E-02, 0.60199774E-04,
         0.11775634E-02, 0.82711413E-04, 0.50865428E-03, 0.91853424E-03,
        -0.74995088E-03,-0.47137175E-03,-0.13040425E-03,-0.37444619E-03,
         0.33086049E-03, 0.50484849E-03,-0.10127169E-02, 0.12234719E-03,
         0.62415109E-03,-0.51465072E-03, 0.33510308E-03,-0.27787872E-03,
         0.56683959E-03,-0.33120948E-03, 0.61351230E-03, 0.32161927E-03,
         0.77903195E-03,-0.87858870E-03, 0.17821670E-02, 0.12103482E-03,
        -0.83371875E-03, 0.93553698E-03, 0.69979770E-03,-0.88944926E-03,
         0.14927308E-02,-0.16371104E-02, 0.49328344E-03,-0.46157319E-03,
         0.85858122E-03, 0.56087557E-03,-0.38885846E-03,-0.54025761E-03,
        -0.10575722E-02,-0.29251052E-02, 0.19510370E-02,-0.12582473E-02,
        -0.44379605E-03, 0.40456950E-03, 0.82120107E-03, 0.96749095E-03,
        -0.50743675E-03,-0.27238097E-03, 0.14636385E-02,-0.11449795E-02,
        -0.32391280E-02, 0.91455673E-03, 0.47411813E-03, 0.89133927E-03,
        -0.72516670E-03,-0.56551519E-03,-0.67062717E-03,-0.43001072E-03,
         0.53410284E-03, 0.95863524E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22*x31        
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[  8]        *x33        
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x21*x31    *x51
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]*x11    *x32        
        +coeff[ 14]*x11        *x42    
        +coeff[ 15]*x11*x21        *x51
        +coeff[ 16]*x12            *x51
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 17]*x13                
        +coeff[ 18]    *x22*x32        
        +coeff[ 19]        *x33    *x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]            *x43*x51
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]*x11*x23            
        +coeff[ 24]*x11    *x31*x42    
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 26]*x11        *x42*x51
        +coeff[ 27]*x11    *x31    *x52
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]    *x24*x31        
        +coeff[ 31]    *x24    *x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x21    *x44    
        +coeff[ 34]*x13            *x51
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 35]        *x33    *x52
        +coeff[ 36]    *x21    *x42*x52
        +coeff[ 37]        *x31    *x54
        +coeff[ 38]*x11*x24            
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x12*x21*x32        
        +coeff[ 41]    *x22*x33*x41    
        +coeff[ 42]    *x21*x34*x41    
        +coeff[ 43]    *x23    *x43    
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 44]    *x21*x32*x43    
        +coeff[ 45]*x13    *x31    *x51
        +coeff[ 46]*x13        *x41*x51
        +coeff[ 47]    *x24    *x41*x51
        +coeff[ 48]    *x21*x32*x41*x52
        +coeff[ 49]    *x23        *x53
        +coeff[ 50]            *x43*x53
        +coeff[ 51]*x11*x23*x32        
        +coeff[ 52]*x11*x22*x32*x41    
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 53]*x11    *x31*x44    
        +coeff[ 54]*x11*x22*x32    *x51
        +coeff[ 55]*x11    *x34    *x51
        +coeff[ 56]*x11*x23    *x41*x51
        +coeff[ 57]*x11    *x33*x41*x51
        +coeff[ 58]*x11*x21    *x43*x51
        +coeff[ 59]*x11*x22        *x53
        +coeff[ 60]*x11    *x31*x41*x53
        +coeff[ 61]*x11        *x41*x54
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 62]*x12*x23*x31        
        +coeff[ 63]*x12*x21*x31*x42    
        +coeff[ 64]*x12    *x32*x41*x51
        +coeff[ 65]*x12    *x31*x41*x52
        +coeff[ 66]*x12        *x41*x53
        +coeff[ 67]*x13    *x32*x41    
        +coeff[ 68]*x13    *x31*x42    
        +coeff[ 69]    *x23*x32*x42    
        +coeff[ 70]    *x21*x32*x44    
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 71]*x13    *x32    *x51
        +coeff[ 72]    *x22*x31*x43*x51
        +coeff[ 73]    *x23        *x54
        +coeff[ 74]*x11*x22*x34        
        +coeff[ 75]*x11*x23*x32*x41    
        +coeff[ 76]*x11*x22*x33*x41    
        +coeff[ 77]*x11*x23    *x43    
        +coeff[ 78]*x11*x24*x31    *x51
        +coeff[ 79]*x11*x22*x32*x41*x51
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 80]*x11*x22*x31*x42*x51
        +coeff[ 81]*x11*x21*x32*x42*x51
        +coeff[ 82]*x11    *x33*x42*x51
        +coeff[ 83]*x11    *x31*x44*x51
        +coeff[ 84]*x11*x22*x32    *x52
        +coeff[ 85]*x11*x21    *x43*x52
        +coeff[ 86]*x12*x24    *x41    
        +coeff[ 87]*x12    *x34    *x51
        +coeff[ 88]*x12    *x32*x41*x52
    ;
    v_l_e_q1en_2_1000                              =v_l_e_q1en_2_1000                              
        +coeff[ 89]*x12*x21    *x42*x52
        ;

    return v_l_e_q1en_2_1000                              ;
}
float x_e_q1en_2_900                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.4124310E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.40310845E-03, 0.37387975E-02, 0.11696008E+00, 0.26981474E-03,
         0.10355865E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_900                              ;
}
float t_e_q1en_2_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.9908264E-04;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.96746830E-04,-0.10725659E-02, 0.30742718E-01, 0.11478999E-02,
        -0.18287348E-03, 0.41785908E-04,-0.61929444E-04,-0.11697799E-05,
        -0.46365589E-04, 0.13914364E-03, 0.10422597E-03, 0.10544451E-03,
         0.90462512E-04,-0.46113088E-04, 0.36686477E-04, 0.11730545E-04,
         0.55183900E-04,-0.92717682E-05, 0.17562523E-05,-0.69143521E-05,
         0.76272995E-05,-0.27624210E-06,-0.63386366E-07,-0.27618447E-06,
        -0.19144957E-05, 0.96308349E-05, 0.36634021E-05,-0.67782784E-08,
         0.43875943E-05,-0.13068333E-04, 0.32735802E-05,-0.26397441E-06,
         0.74657059E-05, 0.22158990E-05,-0.32717360E-05, 0.36675151E-05,
        -0.43997052E-05,-0.90440481E-05, 0.92511855E-05, 0.87007447E-05,
        -0.78579360E-05, 0.27396702E-05, 0.36055005E-05, 0.63167076E-05,
         0.73278165E-05, 0.43377436E-05, 0.47217409E-05,-0.97473721E-05,
         0.56887334E-05,-0.46180003E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_900                              =v_t_e_q1en_2_900                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_900                              =v_t_e_q1en_2_900                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]*x11*x21*x32    *x51
        +coeff[ 21]*x11*x21            
        +coeff[ 22]*x11    *x31        
        +coeff[ 23]        *x31*x42    
        +coeff[ 24]    *x21*x31    *x51
        +coeff[ 25]*x11*x22*x31        
    ;
    v_t_e_q1en_2_900                              =v_t_e_q1en_2_900                              
        +coeff[ 26]*x11*x21*x32        
        +coeff[ 27]    *x23    *x41    
        +coeff[ 28]    *x21        *x53
        +coeff[ 29]*x13*x21*x31        
        +coeff[ 30]*x12*x22*x31        
        +coeff[ 31]*x13    *x32        
        +coeff[ 32]*x11*x22*x32        
        +coeff[ 33]*x11*x21*x33        
        +coeff[ 34]*x12*x22    *x41    
    ;
    v_t_e_q1en_2_900                              =v_t_e_q1en_2_900                              
        +coeff[ 35]*x13    *x31*x41    
        +coeff[ 36]*x11*x21*x32*x41    
        +coeff[ 37]*x12*x21    *x42    
        +coeff[ 38]*x11*x22    *x42    
        +coeff[ 39]*x11*x21*x31*x42    
        +coeff[ 40]    *x22*x31*x42    
        +coeff[ 41]    *x22*x32    *x51
        +coeff[ 42]*x12*x21    *x41*x51
        +coeff[ 43]*x12*x21        *x52
    ;
    v_t_e_q1en_2_900                              =v_t_e_q1en_2_900                              
        +coeff[ 44]*x11*x21*x31    *x52
        +coeff[ 45]    *x22*x31    *x52
        +coeff[ 46]*x12*x23*x31        
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]    *x23*x32*x41    
        +coeff[ 49]*x12    *x32*x42    
        ;

    return v_t_e_q1en_2_900                              ;
}
float y_e_q1en_2_900                              (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.8301459E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.79256977E-03, 0.65374970E-01, 0.57609655E-01,-0.13918697E-03,
        -0.14904044E-03,-0.55772791E-04,-0.47213398E-04, 0.31406159E-05,
        -0.79550322E-04,-0.98555603E-04,-0.89596331E-04,-0.50603703E-05,
        -0.12399713E-04,-0.13687711E-04,-0.72830881E-04, 0.85483753E-05,
        -0.73367482E-05, 0.57326656E-05, 0.46000823E-05, 0.10674384E-04,
        -0.19421772E-04, 0.21248434E-04,-0.98131795E-05,-0.88811612E-05,
        -0.35932455E-04, 0.10092923E-04,-0.96774402E-05, 0.91826369E-05,
        -0.10918312E-04,-0.67835681E-05, 0.13869821E-04,-0.18027709E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_900                              =v_y_e_q1en_2_900                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x24    *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]    *x21*x31*x41    
    ;
    v_y_e_q1en_2_900                              =v_y_e_q1en_2_900                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]*x11    *x31*x41*x51
        +coeff[ 23]        *x32*x42*x51
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x12        *x43    
    ;
    v_y_e_q1en_2_900                              =v_y_e_q1en_2_900                              
        +coeff[ 26]    *x21*x31*x44    
        +coeff[ 27]*x11    *x31*x42*x51
        +coeff[ 28]*x11    *x32*x41*x51
        +coeff[ 29]    *x24    *x42    
        +coeff[ 30]        *x33*x42*x51
        +coeff[ 31]*x11*x22*x32*x41    
        ;

    return v_y_e_q1en_2_900                              ;
}
float p_e_q1en_2_900                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.3538814E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.32822747E-03, 0.15197139E-01, 0.45406595E-01,-0.61845529E-03,
        -0.62562380E-03,-0.26293343E-03,-0.23845422E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_900                              ;
}
float l_e_q1en_2_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1448897E-02;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14779589E-02,-0.34378264E-02,-0.91812439E-03,-0.26254871E-03,
        -0.29033824E-03, 0.70940459E-03,-0.91603363E-03,-0.17514009E-02,
         0.13918163E-02,-0.23434288E-04,-0.21057476E-03, 0.41846503E-03,
         0.35294931E-03,-0.63201552E-03,-0.22974487E-03,-0.53538504E-03,
         0.79553248E-03,-0.14143487E-03, 0.75121736E-03, 0.43791565E-03,
         0.57787320E-03,-0.27500436E-03, 0.54344133E-03, 0.46479548E-03,
         0.76830729E-04,-0.33028005E-03, 0.43758933E-03,-0.94530504E-03,
         0.10995445E-02, 0.58842037E-03,-0.55561606E-04, 0.83704898E-03,
         0.46326930E-03,-0.66249224E-03,-0.56649325E-03, 0.42395020E-03,
         0.43790488E-03,-0.74755732E-03, 0.32751207E-03,-0.16663402E-02,
        -0.35085377E-04, 0.43070887E-03,-0.65327965E-03,-0.56677830E-03,
         0.13943615E-02, 0.83188259E-03, 0.51644020E-03,-0.21719193E-03,
        -0.54482819E-03,-0.64437429E-03, 0.66643988E-03,-0.81856613E-03,
        -0.79957693E-03,-0.59753354E-03, 0.70529711E-03, 0.21762527E-02,
        -0.18343172E-02, 0.26152754E-03,-0.18510759E-02, 0.66968327E-03,
         0.11079322E-02, 0.29433690E-03, 0.23545753E-02,-0.55081886E-03,
        -0.37432098E-03,-0.70678809E-03, 0.73274801E-03,-0.12522520E-02,
         0.17761810E-02,-0.36052701E-04,-0.21390173E-03,-0.99886252E-04,
         0.13275759E-03,-0.59604823E-04, 0.12437184E-03, 0.16729414E-03,
         0.15857388E-03,-0.80841419E-04,-0.70982729E-04,-0.17810838E-03,
         0.14578983E-03,-0.44307671E-03,-0.10249573E-03, 0.62033687E-04,
        -0.33603143E-03,-0.68813941E-04,-0.16669209E-03,-0.34119893E-03,
         0.98512159E-04, 0.14852108E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x31    *x51
        +coeff[  4]        *x31*x43    
        +coeff[  5]*x11*x22*x32    *x51
        +coeff[  6]*x12    *x32*x42    
        +coeff[  7]*x12*x22*x31*x41*x51
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[  8]*x13    *x31*x42*x51
        +coeff[  9]            *x41*x51
        +coeff[ 10]*x11*x22            
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]*x11    *x31    *x51
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]*x12            *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x21*x32*x41    
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 17]    *x21*x31*x42    
        +coeff[ 18]        *x32*x41*x51
        +coeff[ 19]    *x21*x31    *x52
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]        *x34*x41    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x24        *x51
        +coeff[ 24]    *x21*x31*x42*x51
        +coeff[ 25]        *x32*x41*x52
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 26]*x11*x22*x32        
        +coeff[ 27]*x11    *x33    *x51
        +coeff[ 28]*x11        *x41*x53
        +coeff[ 29]*x12*x22*x31        
        +coeff[ 30]*x12    *x33        
        +coeff[ 31]*x12            *x53
        +coeff[ 32]    *x23*x33        
        +coeff[ 33]    *x21*x34*x41    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 35]        *x33*x42*x51
        +coeff[ 36]    *x22*x32    *x52
        +coeff[ 37]        *x32*x41*x53
        +coeff[ 38]*x11*x23*x32        
        +coeff[ 39]*x11*x22*x32*x41    
        +coeff[ 40]*x11    *x34*x41    
        +coeff[ 41]*x11*x22    *x43    
        +coeff[ 42]*x11*x22*x31*x41*x51
        +coeff[ 43]*x11*x22    *x41*x52
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 44]*x11    *x32*x41*x52
        +coeff[ 45]*x11    *x31    *x54
        +coeff[ 46]*x12*x22*x32        
        +coeff[ 47]*x12*x23        *x51
        +coeff[ 48]*x12*x21*x31*x41*x51
        +coeff[ 49]*x13*x22*x31        
        +coeff[ 50]    *x22*x32*x43    
        +coeff[ 51]*x13    *x31    *x52
        +coeff[ 52]*x13        *x41*x52
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 53]*x11*x23*x33        
        +coeff[ 54]*x11*x24    *x42    
        +coeff[ 55]*x11    *x32*x43*x51
        +coeff[ 56]*x11    *x32*x41*x53
        +coeff[ 57]*x12*x22*x33        
        +coeff[ 58]*x12*x21*x32*x42    
        +coeff[ 59]*x12    *x33*x42    
        +coeff[ 60]*x12*x21*x31*x42*x51
        +coeff[ 61]*x12*x23        *x52
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 62]*x12*x21*x32    *x52
        +coeff[ 63]*x12*x22        *x53
        +coeff[ 64]*x12*x21        *x54
        +coeff[ 65]*x13        *x43*x51
        +coeff[ 66]    *x22*x33*x41*x52
        +coeff[ 67]    *x21*x31*x44*x52
        +coeff[ 68]    *x23*x32    *x53
        +coeff[ 69]        *x31        
        +coeff[ 70]    *x21*x31        
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 71]        *x32        
        +coeff[ 72]    *x21    *x41    
        +coeff[ 73]    *x21        *x51
        +coeff[ 74]*x11    *x31        
        +coeff[ 75]*x11        *x41    
        +coeff[ 76]    *x23            
        +coeff[ 77]    *x21*x32        
        +coeff[ 78]    *x22    *x41    
        +coeff[ 79]    *x21*x31*x41    
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 80]    *x21    *x42    
        +coeff[ 81]    *x22        *x51
        +coeff[ 82]    *x21*x31    *x51
        +coeff[ 83]        *x32    *x51
        +coeff[ 84]    *x21        *x52
        +coeff[ 85]*x11*x21    *x41    
        +coeff[ 86]*x11        *x42    
        +coeff[ 87]*x12    *x31        
        +coeff[ 88]*x13                
    ;
    v_l_e_q1en_2_900                              =v_l_e_q1en_2_900                              
        +coeff[ 89]    *x22*x31    *x51
        ;

    return v_l_e_q1en_2_900                              ;
}
float x_e_q1en_2_800                              (float *x,int m){
    int ncoeff=  5;
    float avdat=  0.6436545E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
        -0.64296724E-03, 0.37389705E-02, 0.11694422E+00, 0.26945220E-03,
         0.11139848E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_800                              ;
}
float t_e_q1en_2_800                              (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.1800827E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.17995099E-03,-0.10704650E-02, 0.30740162E-01, 0.11491560E-02,
        -0.17993101E-03, 0.42214120E-04,-0.53160795E-04,-0.41513435E-05,
        -0.44821049E-04, 0.13347832E-03, 0.10401291E-03, 0.10579112E-03,
         0.91825641E-04,-0.40067851E-04, 0.28949371E-05, 0.41404324E-04,
        -0.12577438E-04,-0.13094369E-05,-0.45155216E-05, 0.26585642E-04,
         0.14770915E-04, 0.19663703E-04,-0.93836425E-05,-0.96984922E-05,
        -0.73639859E-06,-0.17514004E-05, 0.35669057E-05,-0.11942215E-04,
         0.61684091E-06, 0.30920048E-05, 0.80237542E-05, 0.88734150E-05,
         0.59784261E-05,-0.85457705E-05, 0.37184127E-05,-0.62598629E-05,
         0.34750817E-05, 0.10564194E-04,-0.89893610E-05,-0.40404675E-05,
        -0.45714310E-05,-0.74698946E-05, 0.12588336E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_800                              =v_t_e_q1en_2_800                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_800                              =v_t_e_q1en_2_800                              
        +coeff[ 17]*x11    *x32        
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]*x11*x22*x31*x41    
        +coeff[ 21]*x11*x22    *x42    
        +coeff[ 22]    *x22*x32    *x51
        +coeff[ 23]    *x21*x33*x41*x51
        +coeff[ 24]    *x22            
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q1en_2_800                              =v_t_e_q1en_2_800                              
        +coeff[ 26]*x12*x21        *x51
        +coeff[ 27]*x11*x21    *x41*x51
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x22*x32        
        +coeff[ 31]*x11    *x32*x42    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]    *x21*x32*x41*x51
        +coeff[ 34]    *x21    *x43*x51
    ;
    v_t_e_q1en_2_800                              =v_t_e_q1en_2_800                              
        +coeff[ 35]    *x21*x32    *x52
        +coeff[ 36]*x11*x22*x33        
        +coeff[ 37]*x11*x22*x32    *x51
        +coeff[ 38]*x12*x22    *x41*x51
        +coeff[ 39]*x13*x21        *x52
        +coeff[ 40]*x12*x22        *x52
        +coeff[ 41]*x11*x22        *x53
        +coeff[ 42]*x11*x21    *x41*x53
        ;

    return v_t_e_q1en_2_800                              ;
}
float y_e_q1en_2_800                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.5294327E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.51468622E-03, 0.65347157E-01, 0.57612818E-01,-0.14077406E-03,
        -0.15066232E-03,-0.95561372E-04,-0.57430694E-04,-0.17770595E-04,
        -0.86349588E-04,-0.82603132E-04,-0.66052344E-05, 0.86558966E-05,
        -0.16401422E-04,-0.65633307E-04,-0.55248191E-04,-0.40636940E-04,
         0.65185795E-05, 0.58025662E-05,-0.85502861E-05, 0.10922559E-04,
         0.10842267E-04,-0.20083422E-04,-0.89062087E-05, 0.90169524E-05,
        -0.21579415E-04,-0.23490222E-04, 0.16522534E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_800                              =v_y_e_q1en_2_800                              
        +coeff[  8]    *x24*x31*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]*x11*x21    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]    *x24    *x41    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x34*x41    
        +coeff[ 16]            *x41*x52
    ;
    v_y_e_q1en_2_800                              =v_y_e_q1en_2_800                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]*x11*x21*x31*x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22    *x43    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]        *x32*x42*x51
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23    *x41    
    ;
    v_y_e_q1en_2_800                              =v_y_e_q1en_2_800                              
        +coeff[ 26]        *x33*x42*x51
        ;

    return v_y_e_q1en_2_800                              ;
}
float p_e_q1en_2_800                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.6706772E-04;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.58398244E-04, 0.15195553E-01, 0.45382213E-01,-0.61899843E-03,
        -0.62474090E-03,-0.26536736E-03,-0.23979088E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_800                              ;
}
float l_e_q1en_2_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1452953E-02;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14229795E-02,-0.35736300E-02,-0.32656480E-04,-0.96809212E-03,
        -0.72522619E-03,-0.15666677E-02,-0.12868145E-02,-0.52929332E-04,
        -0.26009974E-06, 0.10243437E-04, 0.32642181E-03,-0.24329804E-03,
         0.16403706E-03,-0.89942238E-04, 0.30029099E-03, 0.11779112E-03,
        -0.45172081E-04,-0.10071131E-02, 0.33790217E-03, 0.19861679E-02,
        -0.90058654E-03,-0.24950810E-03,-0.59874513E-03,-0.49628009E-03,
         0.88976769E-04, 0.31845190E-03, 0.58501342E-03,-0.51296997E-03,
         0.10165699E-02,-0.35823716E-03, 0.91198599E-04, 0.13347076E-03,
        -0.45294879E-03,-0.10129414E-02, 0.41011686E-03,-0.82388276E-03,
         0.98206894E-03, 0.48080253E-03, 0.62325667E-03, 0.14483070E-02,
        -0.68342051E-03,-0.12851738E-02,-0.17942006E-02, 0.67052274E-03,
        -0.65154301E-04,-0.56536432E-03, 0.33324247E-03,-0.84505056E-03,
         0.41173535E-03, 0.34675142E-03, 0.46041029E-03, 0.15253751E-02,
         0.77142473E-03, 0.59245026E-03,-0.91092032E-03,-0.20657144E-03,
        -0.11417249E-02,-0.16611547E-03, 0.56909972E-04, 0.71396003E-03,
        -0.72591321E-03, 0.68081293E-03,-0.10277181E-02, 0.45324321E-03,
         0.32321890E-03,-0.85438357E-03,-0.44557743E-04,-0.84261410E-03,
         0.15149243E-02,-0.14213136E-02,-0.99323515E-03,-0.45753384E-03,
         0.43502610E-03,-0.92061679E-03, 0.88746398E-03, 0.52595575E-03,
         0.34172193E-03,-0.13335969E-02, 0.28351618E-02,-0.85135119E-03,
         0.12275057E-03,-0.96811760E-04,-0.14952515E-03, 0.90385125E-04,
         0.14766201E-03, 0.11624449E-03, 0.19726933E-03, 0.20676870E-03,
         0.12120928E-03,-0.12473193E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11*x21*x32        
        +coeff[  5]    *x23*x32    *x51
        +coeff[  6]    *x22*x32    *x54
        +coeff[  7]            *x41    
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[  8]                *x52
        +coeff[  9]*x11*x21            
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]    *x24            
        +coeff[ 15]        *x34        
        +coeff[ 16]        *x33*x41    
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 17]    *x21*x31    *x52
        +coeff[ 18]        *x32    *x52
        +coeff[ 19]*x11*x21*x31*x41    
        +coeff[ 20]*x11*x22        *x51
        +coeff[ 21]*x11            *x53
        +coeff[ 22]*x12*x21        *x51
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]*x11*x21*x33        
        +coeff[ 25]*x11*x21*x31*x42    
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 26]*x11    *x31*x41*x52
        +coeff[ 27]*x11        *x42*x52
        +coeff[ 28]*x12        *x42*x51
        +coeff[ 29]    *x22*x34        
        +coeff[ 30]*x13    *x31*x41    
        +coeff[ 31]*x13        *x42    
        +coeff[ 32]        *x33*x43    
        +coeff[ 33]    *x22*x32*x41*x51
        +coeff[ 34]    *x22    *x43*x51
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 35]    *x23*x31    *x52
        +coeff[ 36]    *x21*x32    *x53
        +coeff[ 37]    *x21*x31    *x54
        +coeff[ 38]*x11*x22*x33        
        +coeff[ 39]*x11*x21*x34        
        +coeff[ 40]*x11*x24    *x41    
        +coeff[ 41]*x11*x23*x31*x41    
        +coeff[ 42]*x11*x21*x33*x41    
        +coeff[ 43]*x11*x22    *x43    
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 44]*x11*x23    *x41*x51
        +coeff[ 45]*x11*x21*x32*x41*x51
        +coeff[ 46]*x11    *x32*x42*x51
        +coeff[ 47]*x11    *x31*x43*x51
        +coeff[ 48]*x11*x21*x31    *x53
        +coeff[ 49]*x12*x21*x33        
        +coeff[ 50]*x12*x21*x31*x42    
        +coeff[ 51]*x12*x23        *x51
        +coeff[ 52]*x13*x22        *x51
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 53]*x13    *x31*x41*x51
        +coeff[ 54]    *x22*x33*x41*x51
        +coeff[ 55]    *x24    *x42*x51
        +coeff[ 56]    *x21*x33*x42*x51
        +coeff[ 57]    *x24        *x53
        +coeff[ 58]    *x23*x31    *x53
        +coeff[ 59]*x11*x22*x31*x43    
        +coeff[ 60]*x11*x21*x32*x43    
        +coeff[ 61]*x11*x22    *x44    
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 62]*x11*x22*x32*x41*x51
        +coeff[ 63]*x11*x23    *x41*x52
        +coeff[ 64]*x11    *x33    *x53
        +coeff[ 65]*x12*x24    *x41    
        +coeff[ 66]*x12    *x34*x41    
        +coeff[ 67]*x12*x21*x32*x42    
        +coeff[ 68]*x12*x22    *x41*x52
        +coeff[ 69]*x12    *x32*x41*x52
        +coeff[ 70]*x12        *x42*x53
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 71]*x13*x24            
        +coeff[ 72]*x13*x22*x32        
        +coeff[ 73]*x13    *x33*x41    
        +coeff[ 74]    *x21*x34*x43    
        +coeff[ 75]*x13*x21*x31*x41*x51
        +coeff[ 76]        *x33*x44*x51
        +coeff[ 77]    *x23*x32*x41*x52
        +coeff[ 78]    *x23*x31    *x54
        +coeff[ 79]    *x21*x33    *x54
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 80]    *x21            
        +coeff[ 81]    *x21        *x51
        +coeff[ 82]*x11    *x31        
        +coeff[ 83]    *x21*x32        
        +coeff[ 84]        *x32*x41    
        +coeff[ 85]        *x31*x42    
        +coeff[ 86]    *x21*x31    *x51
        +coeff[ 87]    *x21    *x41*x51
        +coeff[ 88]        *x31*x41*x51
    ;
    v_l_e_q1en_2_800                              =v_l_e_q1en_2_800                              
        +coeff[ 89]        *x31    *x52
        ;

    return v_l_e_q1en_2_800                              ;
}
float x_e_q1en_2_700                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.1021301E-02;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.10293861E-02, 0.37419770E-02, 0.11696075E+00, 0.26592970E-03,
         0.10260724E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_700                              ;
}
float t_e_q1en_2_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2637720E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.26615267E-03,-0.10716973E-02, 0.30749653E-01, 0.11466820E-02,
        -0.17537776E-03, 0.41591418E-04,-0.60533752E-04,-0.81378166E-05,
        -0.45944300E-04, 0.13969021E-03, 0.10657428E-03, 0.10832993E-03,
         0.93083130E-04,-0.39157982E-04, 0.28069371E-05, 0.49258568E-04,
        -0.12968683E-04,-0.94572897E-07,-0.19115250E-05, 0.90116346E-05,
         0.24904472E-04, 0.14839251E-04, 0.14588577E-04, 0.10029074E-04,
        -0.18016783E-04, 0.19556890E-05,-0.14572952E-07,-0.14420914E-05,
        -0.38353728E-05, 0.17676400E-05,-0.53375147E-05, 0.62456179E-05,
         0.30308388E-05, 0.59769709E-05, 0.48522074E-05, 0.65328250E-05,
        -0.62473177E-05,-0.34542288E-05,-0.79715855E-05, 0.38187582E-05,
         0.68896516E-05,-0.63102625E-05,-0.72962034E-05, 0.22197310E-05,
         0.52189062E-05, 0.34701832E-05, 0.56721651E-05,-0.68897889E-05,
        -0.54107700E-05,-0.84139710E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1en_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_700                              =v_t_e_q1en_2_700                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_700                              =v_t_e_q1en_2_700                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]*x11*x22*x32        
        +coeff[ 20]    *x23*x32        
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]*x11*x22*x31*x41    
        +coeff[ 23]*x11*x22    *x42    
        +coeff[ 24]    *x23*x31*x41*x51
        +coeff[ 25]*x11*x21            
    ;
    v_t_e_q1en_2_700                              =v_t_e_q1en_2_700                              
        +coeff[ 26]*x11        *x41    
        +coeff[ 27]                *x52
        +coeff[ 28]    *x22    *x41    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]*x11*x23            
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]*x12*x21        *x51
        +coeff[ 33]    *x23        *x51
        +coeff[ 34]*x11*x21    *x41*x51
    ;
    v_t_e_q1en_2_700                              =v_t_e_q1en_2_700                              
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]*x12*x23            
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]*x13*x21    *x41    
        +coeff[ 39]*x12    *x32*x41    
        +coeff[ 40]*x12*x21    *x42    
        +coeff[ 41]*x13*x21        *x51
        +coeff[ 42]*x12*x22        *x51
        +coeff[ 43]    *x22*x32    *x51
    ;
    v_t_e_q1en_2_700                              =v_t_e_q1en_2_700                              
        +coeff[ 44]    *x22    *x42*x51
        +coeff[ 45]*x12    *x31    *x52
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]*x12*x22*x31*x41    
        +coeff[ 48]*x12*x21*x32*x41    
        +coeff[ 49]*x11*x22*x32*x41    
        ;

    return v_t_e_q1en_2_700                              ;
}
float y_e_q1en_2_700                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.1180967E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.96329168E-04, 0.65324880E-01, 0.57602905E-01,-0.14239780E-03,
        -0.14811882E-03,-0.52349878E-04,-0.68100504E-04,-0.52782771E-05,
        -0.82818930E-04,-0.89176494E-04,-0.96162672E-04,-0.61528631E-05,
        -0.15948952E-04, 0.98067549E-05,-0.45692228E-04,-0.52736672E-04,
        -0.57688630E-04, 0.16765562E-04, 0.74324439E-05,-0.37005379E-05,
        -0.79321635E-05, 0.25063805E-05, 0.62863846E-05, 0.48423749E-05,
         0.75923667E-05, 0.95578534E-05,-0.21169282E-04, 0.26258878E-04,
        -0.29021825E-04,-0.23876832E-04,-0.77769155E-05, 0.85120791E-05,
         0.17251201E-04, 0.15275764E-04,-0.93587460E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_700                              =v_y_e_q1en_2_700                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]    *x24    *x41    
        +coeff[ 15]    *x24    *x43    
        +coeff[ 16]*x11*x24    *x42    
    ;
    v_y_e_q1en_2_700                              =v_y_e_q1en_2_700                              
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]            *x42*x51
        +coeff[ 20]        *x31*x43    
        +coeff[ 21]*x12        *x41    
        +coeff[ 22]    *x21*x31*x42    
        +coeff[ 23]        *x31    *x52
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q1en_2_700                              =v_y_e_q1en_2_700                              
        +coeff[ 26]*x11    *x31*x43    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]            *x45*x51
        +coeff[ 32]*x12*x22    *x41    
        +coeff[ 33]*x13    *x31*x41    
        +coeff[ 34]*x11*x23    *x42    
        ;

    return v_y_e_q1en_2_700                              ;
}
float p_e_q1en_2_700                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.4050380E-04;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.53630934E-04, 0.15190811E-01, 0.45369972E-01,-0.61775616E-03,
        -0.62270189E-03,-0.26533322E-03,-0.24436665E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_700                              ;
}
float l_e_q1en_2_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1451423E-02;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.13777829E-02,-0.30792197E-02,-0.11126640E-02, 0.11355040E-02,
        -0.27682044E-03, 0.74046745E-03,-0.54417900E-03,-0.42369071E-03,
        -0.15307490E-03, 0.93275984E-03,-0.19104517E-03, 0.17031746E-03,
         0.99109557E-05,-0.15613396E-03, 0.10594735E-03,-0.25912965E-03,
        -0.83942577E-05,-0.19687755E-03, 0.44331999E-03, 0.37718483E-03,
        -0.52094017E-03, 0.58837155E-04,-0.13250501E-03,-0.10154444E-03,
         0.16298241E-03,-0.17813365E-02, 0.38205594E-03,-0.60726382E-03,
        -0.14658457E-03, 0.23979304E-03, 0.98113099E-03,-0.11937385E-02,
         0.32955696E-03, 0.60078112E-03,-0.46346145E-03, 0.77343202E-03,
        -0.56939089E-03, 0.33632532E-03, 0.34919055E-03,-0.29579861E-03,
        -0.85418444E-03,-0.29043030E-03, 0.30307009E-03,-0.41291022E-03,
        -0.83181976E-05,-0.77731919E-03, 0.58532605E-03,-0.29818353E-03,
         0.52460824E-03, 0.47859913E-03, 0.49043773E-03, 0.37002147E-03,
         0.83674316E-03, 0.82370191E-03, 0.22596426E-02, 0.69235230E-03,
        -0.56463794E-03, 0.63468079E-03, 0.16675087E-02, 0.61655696E-03,
        -0.12934164E-02,-0.86054514E-03,-0.73454931E-03,-0.54464798E-03,
        -0.85274200E-03, 0.76986011E-03,-0.63853798E-03, 0.34720288E-03,
         0.36136265E-03, 0.79420925E-03,-0.50842157E-03,-0.78481280E-04,
         0.21822103E-02,-0.37353125E-03,-0.13365218E-02,-0.11414870E-02,
         0.10625188E-02, 0.93004102E-03,-0.46904469E-03,-0.10589608E-02,
         0.46044716E-03,-0.12766166E-02, 0.12197314E-02, 0.10058410E-02,
        -0.39291327E-03,-0.63898991E-03,-0.83549210E-03,-0.96343446E-03,
        -0.25820927E-03,-0.87348197E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x11*x22    *x41    
        +coeff[  4]*x11*x21    *x42    
        +coeff[  5]*x12        *x42    
        +coeff[  6]*x12*x24            
        +coeff[  7]*x12    *x31*x43    
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[  8]    *x23*x33*x41    
        +coeff[  9]*x11*x22    *x43*x51
        +coeff[ 10]    *x21    *x41    
        +coeff[ 11]            *x41*x51
        +coeff[ 12]                *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31    *x52
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11*x21*x31        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]*x13                
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x21    *x41*x52
        +coeff[ 24]*x11    *x31*x42    
        +coeff[ 25]*x11*x22        *x51
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 26]*x11            *x53
        +coeff[ 27]*x12*x21        *x51
        +coeff[ 28]*x13    *x31        
        +coeff[ 29]    *x23*x32        
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]        *x31*x44    
        +coeff[ 33]    *x22    *x42*x51
        +coeff[ 34]            *x44*x51
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]        *x33    *x52
        +coeff[ 37]*x11        *x44    
        +coeff[ 38]*x11    *x33    *x51
        +coeff[ 39]*x11*x21        *x53
        +coeff[ 40]*x11    *x31    *x53
        +coeff[ 41]*x12*x23            
        +coeff[ 42]*x12    *x33        
        +coeff[ 43]*x12*x21*x31*x41    
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 44]*x12        *x42*x51
        +coeff[ 45]    *x23*x33        
        +coeff[ 46]*x13        *x42    
        +coeff[ 47]    *x23*x32    *x51
        +coeff[ 48]    *x23    *x42*x51
        +coeff[ 49]    *x23*x31    *x52
        +coeff[ 50]    *x21*x32*x41*x52
        +coeff[ 51]    *x21*x32    *x53
        +coeff[ 52]    *x21    *x41*x54
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 53]*x11    *x33*x42    
        +coeff[ 54]*x11*x24        *x51
        +coeff[ 55]*x11*x21*x32    *x52
        +coeff[ 56]*x11*x21    *x42*x52
        +coeff[ 57]*x12*x23*x31        
        +coeff[ 58]*x12*x21*x32*x41    
        +coeff[ 59]*x12*x21*x32    *x51
        +coeff[ 60]*x12*x21    *x41*x52
        +coeff[ 61]*x12        *x42*x52
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 62]*x13*x22    *x41    
        +coeff[ 63]*x13    *x31*x42    
        +coeff[ 64]    *x23    *x44    
        +coeff[ 65]    *x22*x31*x44    
        +coeff[ 66]*x13*x22        *x51
        +coeff[ 67]*x13*x21    *x41*x51
        +coeff[ 68]    *x21*x32*x43*x51
        +coeff[ 69]        *x33*x41*x53
        +coeff[ 70]        *x31*x43*x53
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 71]*x11*x23*x32*x41    
        +coeff[ 72]*x11*x23*x32    *x51
        +coeff[ 73]*x11*x21*x34    *x51
        +coeff[ 74]*x11*x22*x32*x41*x51
        +coeff[ 75]*x11*x21*x32    *x53
        +coeff[ 76]*x11    *x33    *x53
        +coeff[ 77]*x11*x22    *x41*x53
        +coeff[ 78]*x12*x24*x31        
        +coeff[ 79]*x12*x22*x32*x41    
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 80]*x12    *x32*x43    
        +coeff[ 81]*x12*x23    *x41*x51
        +coeff[ 82]*x12    *x32*x42*x51
        +coeff[ 83]*x12*x21    *x43*x51
        +coeff[ 84]*x13*x23*x31        
        +coeff[ 85]*x13*x21*x33        
        +coeff[ 86]*x13*x21*x32*x41    
        +coeff[ 87]    *x23*x34*x41    
        +coeff[ 88]        *x34*x44    
    ;
    v_l_e_q1en_2_700                              =v_l_e_q1en_2_700                              
        +coeff[ 89]    *x24*x32*x41*x51
        ;

    return v_l_e_q1en_2_700                              ;
}
float x_e_q1en_2_600                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.2578292E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30015E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.24533833E-03, 0.37396080E-02, 0.11696398E+00, 0.26862061E-03,
         0.12207682E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_600                              ;
}
float t_e_q1en_2_600                              (float *x,int m){
    int ncoeff= 49;
    float avdat= -0.6876481E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30015E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 50]={
         0.65432665E-04,-0.10728801E-02, 0.30764291E-01, 0.11495521E-02,
        -0.18767575E-03, 0.42140382E-04,-0.57715886E-04,-0.18436755E-04,
        -0.48248981E-04, 0.12919916E-03, 0.11262213E-03, 0.12048360E-03,
         0.95300296E-04,-0.51965300E-04, 0.41112733E-04, 0.20599220E-04,
         0.45108642E-04, 0.27209555E-05, 0.21786911E-05,-0.17910674E-04,
         0.44463154E-05,-0.70254960E-05,-0.13682092E-05, 0.37064474E-05,
         0.14717308E-05,-0.94440438E-05,-0.41347579E-06,-0.44631124E-05,
         0.52784380E-05, 0.13481190E-04, 0.49252299E-05, 0.67253623E-05,
        -0.42236147E-05, 0.88790039E-05,-0.10703594E-04, 0.32629559E-05,
        -0.11959073E-04, 0.10711467E-04,-0.17881846E-04, 0.17935849E-04,
         0.95917094E-05,-0.52729170E-05, 0.61739429E-05, 0.67390183E-05,
         0.62095919E-05, 0.92226182E-05,-0.14251415E-04, 0.60335424E-05,
         0.45611555E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_600                              =v_t_e_q1en_2_600                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_600                              =v_t_e_q1en_2_600                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x31*x41*x51
        +coeff[ 20]    *x21*x31        
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]    *x21*x33        
    ;
    v_t_e_q1en_2_600                              =v_t_e_q1en_2_600                              
        +coeff[ 26]*x12        *x42    
        +coeff[ 27]*x13        *x42    
        +coeff[ 28]*x12*x21    *x42    
        +coeff[ 29]*x11*x22    *x42    
        +coeff[ 30]*x11*x22*x31    *x51
        +coeff[ 31]*x11*x21*x31*x41*x51
        +coeff[ 32]*x11    *x32    *x52
        +coeff[ 33]    *x21    *x42*x52
        +coeff[ 34]*x12*x22*x32        
    ;
    v_t_e_q1en_2_600                              =v_t_e_q1en_2_600                              
        +coeff[ 35]*x13*x22    *x41    
        +coeff[ 36]*x13*x21*x31*x41    
        +coeff[ 37]*x12*x22    *x42    
        +coeff[ 38]    *x23*x31*x42    
        +coeff[ 39]    *x21*x33*x42    
        +coeff[ 40]*x11*x21*x31*x43    
        +coeff[ 41]*x12*x22*x31    *x51
        +coeff[ 42]*x11*x22*x32    *x51
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_q1en_2_600                              =v_t_e_q1en_2_600                              
        +coeff[ 44]*x13*x21    *x41*x51
        +coeff[ 45]    *x21*x33*x41*x51
        +coeff[ 46]    *x23    *x42*x51
        +coeff[ 47]    *x22*x31*x41*x52
        +coeff[ 48]    *x23        *x53
        ;

    return v_t_e_q1en_2_600                              ;
}
float y_e_q1en_2_600                              (float *x,int m){
    int ncoeff= 25;
    float avdat= -0.2166879E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30015E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
         0.23514211E-03, 0.65335117E-01, 0.57622246E-01,-0.14270689E-03,
        -0.14367049E-03,-0.10344544E-03,-0.61284489E-04,-0.10795343E-03,
        -0.69986767E-04,-0.16235505E-04,-0.21853717E-04, 0.89900783E-06,
        -0.64833133E-04,-0.60222774E-04,-0.33527638E-05, 0.26081460E-06,
        -0.51691909E-05, 0.18337689E-04, 0.70205851E-05,-0.10660744E-04,
        -0.24433119E-04,-0.22328515E-04, 0.26875021E-04, 0.35451794E-04,
        -0.11535305E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x32*x41    
    ;
    v_y_e_q1en_2_600                              =v_y_e_q1en_2_600                              
        +coeff[  8]    *x24*x31*x42    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]    *x24    *x41    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22    *x41*x51
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]*x12*x21    *x41    
    ;
    v_y_e_q1en_2_600                              =v_y_e_q1en_2_600                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]        *x33*x41*x51
        +coeff[ 19]*x11    *x31*x42*x51
        +coeff[ 20]*x11*x23*x31        
        +coeff[ 21]        *x31*x44*x51
        +coeff[ 22]    *x22    *x43*x51
        +coeff[ 23]    *x22*x31*x42*x51
        +coeff[ 24]    *x22    *x45    
        ;

    return v_y_e_q1en_2_600                              ;
}
float p_e_q1en_2_600                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1522461E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30015E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.16501988E-03, 0.15189718E-01, 0.45367707E-01,-0.61863539E-03,
        -0.62482926E-03,-0.26598395E-03,-0.24533379E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_600                              ;
}
float l_e_q1en_2_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1478540E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30015E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14729039E-02,-0.30337430E-02,-0.34338085E-03,-0.11905079E-02,
        -0.28333630E-03,-0.85930224E-03,-0.53353270E-03, 0.89425279E-03,
        -0.36217112E-04, 0.99746954E-04, 0.15210986E-03,-0.43056021E-03,
        -0.28712687E-03,-0.64004044E-03,-0.17526245E-02,-0.13587617E-03,
        -0.15320218E-02, 0.94468711E-03, 0.37206139E-03, 0.16119705E-03,
        -0.62983454E-03,-0.15330044E-04,-0.28620868E-02, 0.27229867E-02,
         0.44083537E-03, 0.47232624E-03,-0.45993261E-03, 0.41690675E-03,
        -0.11932129E-03, 0.14623185E-02,-0.30862240E-03, 0.69950789E-03,
        -0.84685750E-03, 0.14711693E-02,-0.61570579E-03, 0.12030968E-02,
         0.63680287E-03,-0.57074445E-03, 0.35571181E-02, 0.49051817E-03,
         0.10745960E-02,-0.70307712E-03, 0.10580188E-02, 0.69600489E-03,
         0.11617824E-02, 0.16985143E-02, 0.38808410E-03,-0.16735012E-02,
         0.32892305E-03,-0.93237398E-03, 0.31022940E-03, 0.39196422E-03,
        -0.66166307E-03, 0.86737258E-04,-0.94574183E-03,-0.24737357E-03,
        -0.10722022E-02,-0.27051012E-03, 0.14868320E-02, 0.72477228E-03,
        -0.82448591E-03,-0.21080328E-02, 0.13752590E-02,-0.82267355E-03,
         0.43225882E-03, 0.16884964E-02, 0.97987405E-03,-0.11121487E-02,
         0.31408391E-03, 0.25517109E-02, 0.61557919E-03, 0.16085624E-02,
         0.86342223E-03, 0.32068109E-02,-0.16974305E-02,-0.13086309E-04,
        -0.14672450E-02,-0.16887020E-02,-0.11769186E-02,-0.11393013E-02,
        -0.15074050E-02,-0.45699682E-03, 0.84001821E-03,-0.44576297E-03,
         0.40092054E-03,-0.13590013E-02, 0.10585390E-02, 0.13247633E-02,
         0.42511150E-03,-0.32525144E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11    *x33        
        +coeff[  5]    *x22*x33*x41    
        +coeff[  6]        *x33    *x53
        +coeff[  7]    *x24*x31    *x53
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[  8]        *x31        
        +coeff[  9]    *x21        *x51
        +coeff[ 10]*x11*x21        *x51
        +coeff[ 11]*x11    *x31    *x51
        +coeff[ 12]    *x24            
        +coeff[ 13]    *x22    *x42    
        +coeff[ 14]        *x31*x41*x52
        +coeff[ 15]            *x41*x53
        +coeff[ 16]*x11*x21    *x41*x51
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 17]*x11    *x31*x41*x51
        +coeff[ 18]*x12        *x42    
        +coeff[ 19]        *x32*x43    
        +coeff[ 20]    *x21*x31*x41*x52
        +coeff[ 21]*x11*x21    *x43    
        +coeff[ 22]*x11*x22    *x41*x51
        +coeff[ 23]*x11    *x31*x42*x51
        +coeff[ 24]*x12*x21*x31*x41    
        +coeff[ 25]*x12*x21    *x42    
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 26]*x12*x21*x31    *x51
        +coeff[ 27]*x12*x21        *x52
        +coeff[ 28]    *x24*x32        
        +coeff[ 29]    *x22*x32*x42    
        +coeff[ 30]        *x34*x42    
        +coeff[ 31]        *x33*x43    
        +coeff[ 32]    *x21*x33*x41*x51
        +coeff[ 33]    *x22    *x42*x52
        +coeff[ 34]    *x21*x31*x42*x52
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 35]        *x31*x43*x52
        +coeff[ 36]    *x21*x31*x41*x53
        +coeff[ 37]    *x22        *x54
        +coeff[ 38]        *x31*x41*x54
        +coeff[ 39]*x11*x22*x31*x42    
        +coeff[ 40]*x11*x23*x31    *x51
        +coeff[ 41]*x11*x21*x33    *x51
        +coeff[ 42]*x11*x23    *x41*x51
        +coeff[ 43]*x11    *x33*x41*x51
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 44]*x11*x21    *x43*x51
        +coeff[ 45]*x11*x21*x31*x41*x52
        +coeff[ 46]*x11        *x43*x52
        +coeff[ 47]*x11    *x31*x41*x53
        +coeff[ 48]*x12*x21*x33        
        +coeff[ 49]*x12        *x42*x52
        +coeff[ 50]*x12            *x54
        +coeff[ 51]*x13*x21*x32        
        +coeff[ 52]    *x24*x33        
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 53]    *x23*x34        
        +coeff[ 54]*x13*x21*x31*x41    
        +coeff[ 55]*x13    *x32*x41    
        +coeff[ 56]    *x23*x33*x41    
        +coeff[ 57]*x13*x21    *x42    
        +coeff[ 58]    *x22*x33*x42    
        +coeff[ 59]    *x23*x31*x43    
        +coeff[ 60]    *x22*x31*x44    
        +coeff[ 61]    *x24*x31*x41*x51
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 62]    *x22*x33*x41*x51
        +coeff[ 63]    *x24    *x42*x51
        +coeff[ 64]    *x22*x32*x42*x51
        +coeff[ 65]    *x21*x33*x42*x51
        +coeff[ 66]    *x22*x31*x43*x51
        +coeff[ 67]    *x21*x31*x44*x51
        +coeff[ 68]    *x24*x31    *x52
        +coeff[ 69]    *x21*x33*x41*x52
        +coeff[ 70]    *x22*x32    *x53
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 71]*x11*x21*x33*x42    
        +coeff[ 72]*x11*x21*x32*x43    
        +coeff[ 73]*x11*x24    *x41*x51
        +coeff[ 74]*x11    *x31*x44*x51
        +coeff[ 75]*x11*x23*x31    *x52
        +coeff[ 76]*x11*x21*x33    *x52
        +coeff[ 77]*x11*x21*x31*x42*x52
        +coeff[ 78]*x11    *x31*x42*x53
        +coeff[ 79]*x12*x23*x32        
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 80]*x12*x21*x33*x41    
        +coeff[ 81]*x12    *x34    *x51
        +coeff[ 82]*x12*x23        *x52
        +coeff[ 83]*x12    *x31*x41*x53
        +coeff[ 84]*x12        *x42*x53
        +coeff[ 85]*x12*x21        *x54
        +coeff[ 86]*x13*x22    *x41*x51
        +coeff[ 87]*x13*x21*x31    *x52
        +coeff[ 88]*x13        *x41*x53
    ;
    v_l_e_q1en_2_600                              =v_l_e_q1en_2_600                              
        +coeff[ 89]        *x31*x43*x54
        ;

    return v_l_e_q1en_2_600                              ;
}
float x_e_q1en_2_500                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.1126426E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.11313724E-02, 0.37394981E-02, 0.11697330E+00, 0.27237943E-03,
         0.10647457E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_500                              ;
}
float t_e_q1en_2_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2889289E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.28986990E-03,-0.10716314E-02, 0.30770658E-01, 0.11497085E-02,
        -0.18351933E-03, 0.41140564E-04,-0.64803753E-04,-0.44052440E-05,
        -0.47348352E-04, 0.13810082E-03, 0.11050828E-03, 0.10145109E-03,
         0.97054311E-04,-0.43114262E-04,-0.19907577E-05, 0.38689304E-04,
         0.50530463E-04, 0.22940328E-05, 0.19661411E-04,-0.26402486E-04,
        -0.98290657E-05, 0.26584746E-05,-0.93483777E-05, 0.50146027E-05,
         0.30330105E-06,-0.48122106E-05,-0.20025925E-05,-0.17761494E-05,
         0.71872737E-05,-0.32009664E-05,-0.40378795E-05, 0.56154172E-05,
         0.61915302E-05, 0.13235502E-04,-0.88077668E-05,-0.27020835E-05,
         0.70508499E-05, 0.69165580E-05,-0.51282818E-05, 0.81640028E-05,
        -0.66260304E-05,-0.46286514E-05, 0.85111969E-05, 0.51701136E-05,
         0.78497351E-05,-0.34708289E-05, 0.99517529E-05, 0.82983724E-05,
         0.52503060E-05,-0.11186323E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_500                              =v_t_e_q1en_2_500                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_500                              =v_t_e_q1en_2_500                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11*x22*x31*x41    
        +coeff[ 19]*x11*x22*x31*x41*x51
        +coeff[ 20]    *x23*x31*x41*x51
        +coeff[ 21]*x12*x21            
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]*x11            *x52
        +coeff[ 25]*x11*x22*x31        
    ;
    v_t_e_q1en_2_500                              =v_t_e_q1en_2_500                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]*x11    *x32    *x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x21    *x41*x52
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x12*x21*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x11*x21*x32    *x51
    ;
    v_t_e_q1en_2_500                              =v_t_e_q1en_2_500                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]    *x21*x33    *x51
        +coeff[ 37]    *x23    *x41*x51
        +coeff[ 38]    *x21    *x43*x51
        +coeff[ 39]*x11    *x31*x41*x52
        +coeff[ 40]*x11        *x42*x52
        +coeff[ 41]    *x21*x31    *x53
        +coeff[ 42]*x11*x22*x33        
        +coeff[ 43]*x12*x23    *x41    
    ;
    v_t_e_q1en_2_500                              =v_t_e_q1en_2_500                              
        +coeff[ 44]*x11*x22*x31*x42    
        +coeff[ 45]    *x21*x33*x42    
        +coeff[ 46]    *x21*x32*x43    
        +coeff[ 47]*x13    *x31*x41*x51
        +coeff[ 48]    *x22*x31*x42*x51
        +coeff[ 49]*x11    *x32*x42*x51
        ;

    return v_t_e_q1en_2_500                              ;
}
float y_e_q1en_2_500                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2796909E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.25002108E-03, 0.65353326E-01, 0.57601795E-01,-0.14301810E-03,
        -0.14833096E-03,-0.54683529E-04,-0.38911425E-04,-0.42606216E-05,
        -0.89872206E-04,-0.91509515E-04,-0.85879736E-04,-0.23135101E-05,
        -0.10894913E-04,-0.14218136E-04,-0.11014000E-05,-0.76618620E-04,
         0.27631056E-05,-0.30853905E-05, 0.11756882E-04,-0.36711911E-05,
         0.66673219E-05, 0.13597366E-04, 0.11153220E-04,-0.32850596E-04,
        -0.37621136E-04, 0.57068628E-05, 0.18872322E-04,-0.14851855E-04,
         0.83519672E-05,-0.12529656E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_500                              =v_y_e_q1en_2_500                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x21    *x41    
    ;
    v_y_e_q1en_2_500                              =v_y_e_q1en_2_500                              
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]            *x44    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]        *x31*x45    
    ;
    v_y_e_q1en_2_500                              =v_y_e_q1en_2_500                              
        +coeff[ 26]            *x43*x52
        +coeff[ 27]*x12*x22    *x41    
        +coeff[ 28]*x12        *x42*x51
        +coeff[ 29]*x11*x21*x33*x41    
        ;

    return v_y_e_q1en_2_500                              ;
}
float p_e_q1en_2_500                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2513965E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.23137005E-03, 0.15183117E-01, 0.45377582E-01,-0.61775878E-03,
        -0.62389008E-03,-0.26753786E-03,-0.24528621E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_500                              ;
}
float l_e_q1en_2_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1455062E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.13772007E-02,-0.36205519E-02,-0.99091085E-04,-0.85605308E-03,
        -0.15154229E-02, 0.49619738E-03,-0.43761721E-02,-0.52762269E-04,
        -0.36141826E-03, 0.42337495E-04, 0.52045239E-03, 0.47979498E-03,
        -0.29327412E-03,-0.17642882E-04,-0.10991185E-03, 0.87205146E-04,
         0.86480583E-03,-0.46744573E-03, 0.53419196E-03, 0.62953983E-03,
         0.13110547E-02,-0.25355737E-03, 0.11452849E-02, 0.21313119E-03,
         0.88955107E-03, 0.47852684E-03, 0.40642006E-03, 0.30725292E-03,
        -0.14265182E-02,-0.32854037E-03,-0.47893045E-03, 0.13873692E-02,
         0.17416386E-02, 0.42037273E-03, 0.24330916E-03, 0.65022684E-03,
        -0.44896948E-03,-0.10518017E-02, 0.46608536E-03,-0.15466387E-03,
        -0.10442754E-02,-0.23803175E-03,-0.74384443E-03,-0.19211592E-02,
        -0.12309100E-02, 0.65830769E-03, 0.50213240E-03, 0.14828042E-02,
         0.70132711E-03,-0.47176579E-03,-0.69723994E-03,-0.10094255E-02,
        -0.73887652E-03, 0.17776940E-02, 0.13056560E-02,-0.43759894E-03,
         0.42670796E-03, 0.62289799E-03,-0.13790280E-02,-0.28595533E-02,
        -0.40022685E-03, 0.62534132E-03,-0.49906957E-03, 0.12007442E-03,
         0.57552755E-03, 0.62469143E-03, 0.67053700E-03,-0.12646227E-02,
        -0.11942906E-02, 0.45210554E-03,-0.45218301E-03, 0.11307795E-02,
        -0.57855446E-03,-0.11447171E-02,-0.82845229E-03, 0.44503331E-03,
        -0.15649593E-02, 0.58617227E-03, 0.47672566E-03, 0.43148876E-03,
         0.10701173E-03,-0.99111545E-04,-0.16282136E-03, 0.82611543E-04,
        -0.18873815E-03, 0.76901269E-04,-0.13312590E-03,-0.12736909E-03,
         0.16153476E-03,-0.25595672E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]    *x22        *x54
        +coeff[  5]    *x22*x32*x43    
        +coeff[  6]*x11*x23*x31*x41*x51
        +coeff[  7]    *x21            
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[  8]            *x41    
        +coeff[  9]        *x31    *x51
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]                *x53
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]*x11    *x31    *x51
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x32*x42    
        +coeff[ 19]        *x33    *x51
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43*x51
        +coeff[ 22]    *x22        *x52
        +coeff[ 23]    *x21    *x41*x52
        +coeff[ 24]*x11*x22        *x51
        +coeff[ 25]*x11*x21*x31    *x51
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 26]*x12*x22            
        +coeff[ 27]*x12        *x41*x51
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]        *x33*x42    
        +coeff[ 30]        *x32*x43    
        +coeff[ 31]*x11*x21*x32    *x51
        +coeff[ 32]*x11*x21*x31*x41*x51
        +coeff[ 33]*x11*x21    *x42*x51
        +coeff[ 34]*x12*x21        *x52
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 35]*x13    *x32        
        +coeff[ 36]    *x22*x34        
        +coeff[ 37]        *x34*x42    
        +coeff[ 38]    *x23    *x43    
        +coeff[ 39]        *x33*x43    
        +coeff[ 40]    *x24    *x41*x51
        +coeff[ 41]            *x44*x52
        +coeff[ 42]        *x33    *x53
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 44]*x11*x21*x33    *x51
        +coeff[ 45]*x11    *x34    *x51
        +coeff[ 46]*x11*x21*x32*x41*x51
        +coeff[ 47]*x11*x21*x31*x41*x52
        +coeff[ 48]*x12*x22*x31*x41    
        +coeff[ 49]*x12    *x31*x42*x51
        +coeff[ 50]*x12    *x32    *x52
        +coeff[ 51]*x12        *x41*x53
        +coeff[ 52]*x13*x21*x31*x41    
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 53]    *x21*x32*x44    
        +coeff[ 54]    *x23*x31*x42*x51
        +coeff[ 55]        *x33*x42*x52
        +coeff[ 56]*x11*x23*x33        
        +coeff[ 57]*x11*x23*x31*x42    
        +coeff[ 58]*x11*x22*x32*x41*x51
        +coeff[ 59]*x11*x21*x32*x42*x51
        +coeff[ 60]*x11*x22*x32    *x52
        +coeff[ 61]*x11    *x31*x43*x52
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 62]*x11*x23        *x53
        +coeff[ 63]*x11        *x43*x53
        +coeff[ 64]*x11        *x42*x54
        +coeff[ 65]*x12*x22*x33        
        +coeff[ 66]*x12*x24    *x41    
        +coeff[ 67]*x12*x23*x31    *x51
        +coeff[ 68]*x12    *x32*x42*x51
        +coeff[ 69]*x12        *x44*x51
        +coeff[ 70]*x12        *x43*x52
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 71]*x12*x21*x31    *x53
        +coeff[ 72]*x13*x22*x31*x41    
        +coeff[ 73]    *x23*x32*x42*x51
        +coeff[ 74]*x13    *x32    *x52
        +coeff[ 75]    *x24*x31*x41*x52
        +coeff[ 76]    *x22*x33*x41*x52
        +coeff[ 77]*x13        *x41*x53
        +coeff[ 78]    *x23    *x42*x53
        +coeff[ 79]        *x34    *x54
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 80]                *x51
        +coeff[ 81]*x11                
        +coeff[ 82]*x11            *x51
        +coeff[ 83]    *x23            
        +coeff[ 84]    *x22*x31        
        +coeff[ 85]        *x33        
        +coeff[ 86]    *x22    *x41    
        +coeff[ 87]    *x21    *x42    
        +coeff[ 88]        *x31*x42    
    ;
    v_l_e_q1en_2_500                              =v_l_e_q1en_2_500                              
        +coeff[ 89]    *x21*x31    *x51
        ;

    return v_l_e_q1en_2_500                              ;
}
float x_e_q1en_2_450                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.6848519E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.69024129E-03, 0.37390993E-02, 0.11697274E+00, 0.26689601E-03,
         0.10762998E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_450                              ;
}
float t_e_q1en_2_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1771660E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.17844088E-03,-0.10720107E-02, 0.30776154E-01, 0.11496646E-02,
        -0.18030814E-03, 0.42013085E-04,-0.63278327E-04,-0.11178054E-04,
        -0.43747088E-04, 0.13267955E-03, 0.11171602E-03, 0.11187163E-03,
         0.96292482E-04,-0.39707993E-04, 0.12692557E-04, 0.55285902E-04,
         0.27820729E-05,-0.70570986E-05,-0.90475414E-05, 0.22442520E-04,
        -0.12662668E-04, 0.19199138E-05, 0.23358755E-05, 0.29826737E-06,
         0.50108001E-05,-0.82451070E-05,-0.45578653E-06, 0.18153214E-05,
         0.81443868E-05, 0.12654266E-05, 0.30502322E-05,-0.34480404E-05,
         0.16047479E-05,-0.14986080E-05,-0.24684477E-05, 0.24841675E-05,
        -0.14662413E-05,-0.89033774E-05, 0.15977941E-04, 0.72167372E-05,
         0.33633848E-05,-0.53422964E-05,-0.37239838E-05,-0.52432692E-05,
        -0.51642041E-05,-0.60433258E-05, 0.19823405E-04, 0.63519692E-05,
         0.11019252E-04,-0.16027476E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1en_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_450                              =v_t_e_q1en_2_450                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22*x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1en_2_450                              =v_t_e_q1en_2_450                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11*x21        *x52
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x23*x31*x41*x51
        +coeff[ 21]*x11*x21            
        +coeff[ 22]    *x22            
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]*x11        *x41    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_q1en_2_450                              =v_t_e_q1en_2_450                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]*x11    *x31*x41    
        +coeff[ 29]    *x22        *x51
        +coeff[ 30]*x11*x22*x31        
        +coeff[ 31]*x13        *x41    
        +coeff[ 32]        *x33*x41    
        +coeff[ 33]*x11        *x43    
        +coeff[ 34]*x11    *x31*x41*x51
    ;
    v_t_e_q1en_2_450                              =v_t_e_q1en_2_450                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]*x12*x21*x32        
        +coeff[ 37]*x11    *x33*x41    
        +coeff[ 38]*x11*x22    *x42    
        +coeff[ 39]*x11*x21*x31*x42    
        +coeff[ 40]    *x21*x33    *x51
        +coeff[ 41]*x12*x21        *x52
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]*x11        *x42*x52
    ;
    v_t_e_q1en_2_450                              =v_t_e_q1en_2_450                              
        +coeff[ 44]*x11    *x32*x43    
        +coeff[ 45]*x11*x21*x32*x41*x51
        +coeff[ 46]    *x21*x33*x41*x51
        +coeff[ 47]*x11*x22    *x42*x51
        +coeff[ 48]*x11*x21*x31*x42*x51
        +coeff[ 49]    *x21*x31*x43*x51
        ;

    return v_t_e_q1en_2_450                              ;
}
float y_e_q1en_2_450                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.8490028E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.85999935E-04, 0.65355390E-01, 0.57610977E-01,-0.14127493E-03,
        -0.14547590E-03,-0.78104007E-04,-0.62340259E-04,-0.12847569E-04,
        -0.73356154E-04,-0.10811593E-04,-0.14815981E-04,-0.27268487E-04,
        -0.79845377E-04,-0.71855713E-04,-0.70747214E-04, 0.49109035E-05,
         0.73139054E-05,-0.55646874E-05, 0.73261567E-05, 0.13008139E-04,
        -0.78685598E-05,-0.10122657E-04,-0.18344519E-04, 0.10244829E-04,
        -0.27916229E-04,-0.13987151E-04, 0.21435884E-04, 0.13553720E-04,
         0.11465822E-04, 0.53101999E-05,-0.15177096E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_450                              =v_y_e_q1en_2_450                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]    *x24    *x41    
        +coeff[ 13]    *x22*x32*x41    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]            *x41*x52
    ;
    v_y_e_q1en_2_450                              =v_y_e_q1en_2_450                              
        +coeff[ 17]    *x21*x31*x42    
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]        *x31*x44    
        +coeff[ 20]*x11*x21    *x41*x51
        +coeff[ 21]*x11    *x31*x41*x51
        +coeff[ 22]*x11    *x31*x43    
        +coeff[ 23]*x11        *x41*x52
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x12    *x31*x42    
    ;
    v_y_e_q1en_2_450                              =v_y_e_q1en_2_450                              
        +coeff[ 26]*x11*x21*x33        
        +coeff[ 27]    *x23    *x43    
        +coeff[ 28]        *x31*x44*x51
        +coeff[ 29]*x13        *x42    
        +coeff[ 30]    *x21    *x43*x52
        ;

    return v_y_e_q1en_2_450                              ;
}
float p_e_q1en_2_450                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2567733E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.27663411E-04, 0.15179575E-01, 0.45373242E-01,-0.61638391E-03,
        -0.62302157E-03,-0.26489471E-03,-0.24307505E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_450                              ;
}
float l_e_q1en_2_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1501569E-02;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14303075E-02,-0.33110587E-02,-0.60131878E-03,-0.81243063E-03,
         0.26935979E-03,-0.49069535E-03, 0.32393893E-03, 0.82954846E-03,
         0.14101723E-03, 0.19411834E-03,-0.30985635E-03,-0.27052472E-04,
         0.55430530E-04,-0.14511604E-03,-0.49518637E-03, 0.10790957E-03,
         0.20218524E-03, 0.11107500E-02, 0.28517024E-03, 0.16753539E-03,
         0.63521153E-03,-0.30919228E-03,-0.36283678E-03, 0.43721785E-03,
        -0.33095661E-04, 0.10461338E-02,-0.63333981E-04,-0.64818573E-03,
        -0.16343834E-03,-0.16802666E-03, 0.17494352E-03,-0.44742200E-03,
         0.13589811E-02,-0.32673139E-03,-0.50515332E-03,-0.63015713E-03,
         0.45033259E-03, 0.18702130E-02, 0.12544615E-02, 0.32929287E-03,
        -0.54987287E-03, 0.20492766E-02,-0.12122863E-02,-0.12859471E-03,
        -0.21542549E-03, 0.86213090E-03, 0.27956199E-03,-0.24199633E-03,
        -0.11927481E-02,-0.79909968E-03, 0.14981334E-02, 0.99095295E-03,
         0.55439625E-03,-0.27758896E-03, 0.15036812E-02,-0.70404931E-03,
         0.57251757E-03,-0.11099142E-02,-0.30646610E-03, 0.13936426E-02,
        -0.10771928E-02,-0.65247208E-03,-0.95986453E-03,-0.10203151E-02,
        -0.45945327E-03, 0.16171493E-02,-0.10635891E-02,-0.27910538E-03,
        -0.14001444E-02,-0.16023687E-02, 0.38127028E-03,-0.14907527E-02,
        -0.72678219E-03,-0.72576926E-03,-0.20897868E-02, 0.64222212E-03,
         0.85143908E-03, 0.55985065E-03, 0.63234626E-03, 0.93551836E-03,
        -0.10576694E-02,-0.84677513E-03, 0.99446846E-03, 0.84057152E-04,
        -0.42601034E-04,-0.63502841E-04, 0.66957749E-04,-0.90068061E-04,
         0.10437737E-03, 0.42635696E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11*x21*x31        
        +coeff[  5]*x11            *x52
        +coeff[  6]*x11    *x31*x42    
        +coeff[  7]*x11    *x31*x42*x51
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[  8]*x11                
        +coeff[  9]            *x41*x51
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]*x12                
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]*x11*x21        *x51
        +coeff[ 19]*x12    *x31        
        +coeff[ 20]        *x33*x41    
        +coeff[ 21]            *x44    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x21    *x41*x52
        +coeff[ 24]*x11*x23            
        +coeff[ 25]*x11*x22*x31        
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 26]*x11*x21    *x42    
        +coeff[ 27]*x12    *x31*x41    
        +coeff[ 28]*x12            *x52
        +coeff[ 29]*x13        *x41    
        +coeff[ 30]    *x24    *x41    
        +coeff[ 31]        *x31*x43*x51
        +coeff[ 32]    *x21    *x42*x52
        +coeff[ 33]    *x21        *x54
        +coeff[ 34]            *x41*x54
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 35]*x11*x21    *x43    
        +coeff[ 36]*x11*x21*x31*x41*x51
        +coeff[ 37]*x11    *x32*x41*x51
        +coeff[ 38]*x11    *x31*x41*x52
        +coeff[ 39]*x12    *x31*x41*x51
        +coeff[ 40]*x13*x21    *x41    
        +coeff[ 41]    *x23*x32*x41    
        +coeff[ 42]    *x21*x34*x41    
        +coeff[ 43]*x13        *x42    
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 44]*x13    *x31    *x51
        +coeff[ 45]    *x22*x33    *x51
        +coeff[ 46]    *x23    *x42*x51
        +coeff[ 47]    *x22    *x43*x51
        +coeff[ 48]    *x23    *x41*x52
        +coeff[ 49]*x11*x24*x31        
        +coeff[ 50]*x11*x21*x33    *x51
        +coeff[ 51]*x11*x22    *x41*x52
        +coeff[ 52]*x11    *x31*x42*x52
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 53]*x11*x21*x31    *x53
        +coeff[ 54]*x12    *x31*x43    
        +coeff[ 55]*x12*x22    *x41*x51
        +coeff[ 56]*x13*x21    *x42    
        +coeff[ 57]*x13*x21*x31    *x51
        +coeff[ 58]    *x23*x33    *x51
        +coeff[ 59]    *x22*x33*x41*x51
        +coeff[ 60]    *x22*x31*x43*x51
        +coeff[ 61]*x13        *x41*x52
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 62]    *x24    *x41*x52
        +coeff[ 63]    *x23    *x42*x52
        +coeff[ 64]        *x33*x42*x52
        +coeff[ 65]    *x22    *x41*x54
        +coeff[ 66]*x11*x23*x32*x41    
        +coeff[ 67]*x11*x22*x33*x41    
        +coeff[ 68]*x11    *x34*x41*x51
        +coeff[ 69]*x11*x21*x31*x43*x51
        +coeff[ 70]*x11    *x34    *x52
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 71]*x11    *x33*x41*x52
        +coeff[ 72]*x11*x22    *x42*x52
        +coeff[ 73]*x11*x21*x31*x42*x52
        +coeff[ 74]*x11    *x32*x41*x53
        +coeff[ 75]*x12*x21*x33*x41    
        +coeff[ 76]*x12    *x32*x41*x52
        +coeff[ 77]*x12    *x31*x42*x52
        +coeff[ 78]    *x21*x33*x44    
        +coeff[ 79]*x13*x21    *x42*x51
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 80]        *x33*x43*x52
        +coeff[ 81]*x13*x21        *x53
        +coeff[ 82]    *x22*x31*x41*x54
        +coeff[ 83]    *x21            
        +coeff[ 84]        *x31        
        +coeff[ 85]    *x21*x31        
        +coeff[ 86]        *x32        
        +coeff[ 87]    *x21    *x41    
        +coeff[ 88]*x11        *x41    
    ;
    v_l_e_q1en_2_450                              =v_l_e_q1en_2_450                              
        +coeff[ 89]*x11            *x51
        ;

    return v_l_e_q1en_2_450                              ;
}
float x_e_q1en_2_449                              (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.6367635E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59985E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.64206432E-03, 0.37432928E-02,-0.37744286E-06, 0.11703607E+00,
         0.26067835E-03, 0.11350073E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_449                              ;
}
float t_e_q1en_2_449                              (float *x,int m){
    int ncoeff= 45;
    float avdat= -0.1789593E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59985E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 46]={
         0.18018851E-03,-0.10631358E-02, 0.31006396E-01, 0.11371188E-02,
        -0.18184888E-03,-0.67609581E-04, 0.41099887E-04,-0.12353185E-04,
        -0.44060431E-04, 0.14948036E-03, 0.11208562E-03, 0.14012374E-03,
         0.11337038E-03,-0.50339815E-04, 0.20184185E-04, 0.64316861E-04,
        -0.93740491E-05, 0.23095979E-05, 0.12766716E-04,-0.76639417E-05,
         0.30009225E-04, 0.75173757E-05, 0.85504496E-06,-0.17815913E-05,
         0.22812378E-05, 0.25584561E-06,-0.33363465E-05, 0.24739527E-05,
        -0.20477983E-05,-0.24731462E-05,-0.40661594E-05,-0.38849944E-05,
         0.40101168E-05, 0.11260745E-04, 0.52759501E-05, 0.46741297E-05,
        -0.59889512E-05,-0.72149683E-05,-0.14627363E-04,-0.61508154E-05,
        -0.13233995E-04, 0.99871759E-05, 0.10579188E-04, 0.30843694E-05,
         0.12852618E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]*x11            *x51
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_449                              =v_t_e_q1en_2_449                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22*x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_449                              =v_t_e_q1en_2_449                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x23*x32        
        +coeff[ 21]*x12*x22        *x51
        +coeff[ 22]                *x51
        +coeff[ 23]    *x22            
        +coeff[ 24]    *x21    *x41    
        +coeff[ 25]        *x31*x41    
    ;
    v_t_e_q1en_2_449                              =v_t_e_q1en_2_449                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11*x21        *x51
        +coeff[ 28]    *x21*x31    *x51
        +coeff[ 29]                *x53
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_t_e_q1en_2_449                              =v_t_e_q1en_2_449                              
        +coeff[ 35]    *x23*x31*x42    
        +coeff[ 36]*x11*x21*x32*x42    
        +coeff[ 37]    *x22*x31*x43    
        +coeff[ 38]*x12*x23        *x51
        +coeff[ 39]*x11*x21*x33    *x51
        +coeff[ 40]    *x23*x31*x41*x51
        +coeff[ 41]*x12*x21    *x41*x52
        +coeff[ 42]*x12*x21        *x53
        +coeff[ 43]*x12        *x41*x53
    ;
    v_t_e_q1en_2_449                              =v_t_e_q1en_2_449                              
        +coeff[ 44]    *x21*x31*x41*x53
        ;

    return v_t_e_q1en_2_449                              ;
}
float y_e_q1en_2_449                              (float *x,int m){
    int ncoeff= 21;
    float avdat=  0.5019499E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59985E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
        -0.48989907E-03, 0.65363638E-01, 0.63971899E-01,-0.14002866E-03,
        -0.15738787E-03,-0.90066576E-04,-0.58974238E-04,-0.11980735E-04,
        -0.82724677E-04, 0.58774040E-05,-0.36554877E-04,-0.11073184E-04,
        -0.14187860E-04,-0.75742639E-04,-0.12266853E-04, 0.51460133E-05,
         0.51676266E-05,-0.76831137E-04,-0.10165147E-03,-0.20990159E-04,
         0.10039998E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_449                              =v_y_e_q1en_2_449                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]    *x24*x33        
        +coeff[ 11]*x11*x21    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q1en_2_449                              =v_y_e_q1en_2_449                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]*x11*x22    *x41*x51
        +coeff[ 20]*x13        *x41*x51
        ;

    return v_y_e_q1en_2_449                              ;
}
float p_e_q1en_2_449                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.9762120E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59985E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.91256778E-04, 0.16726779E-01, 0.45275521E-01,-0.68001583E-03,
        -0.61929214E-03,-0.29398897E-03,-0.24771664E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_449                              ;
}
float l_e_q1en_2_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1442606E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59985E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14490400E-02,-0.33636263E-02,-0.11750610E-02, 0.30355083E-03,
         0.84699743E-03,-0.14972440E-02, 0.12715721E-02, 0.12284378E-03,
         0.19094259E-03,-0.16597951E-03, 0.11067623E-03, 0.23888334E-03,
        -0.33212660E-03,-0.17650136E-03,-0.12790004E-03, 0.57321903E-03,
        -0.47538881E-03, 0.30959101E-03,-0.43776003E-03,-0.28442719E-03,
        -0.46676895E-03, 0.22864931E-02, 0.72012231E-03,-0.16530315E-03,
         0.50753425E-03, 0.12226084E-02,-0.36338871E-03,-0.13860931E-02,
         0.60308579E-03,-0.80067699E-03, 0.52345305E-03, 0.68077212E-03,
         0.42941110E-03,-0.20598054E-03,-0.67494420E-03, 0.55464427E-03,
        -0.60988701E-03,-0.97632332E-03,-0.18184211E-02,-0.10811205E-02,
         0.18695733E-02, 0.86346362E-03,-0.11483165E-02, 0.26628346E-03,
        -0.32874441E-03,-0.30633147E-03,-0.98910695E-03,-0.57522894E-03,
         0.10938226E-02,-0.61630044E-03, 0.86084066E-03,-0.48438588E-03,
        -0.86230977E-03, 0.19909085E-02,-0.25262919E-02,-0.10956927E-02,
        -0.20046388E-02, 0.11945873E-02, 0.12254693E-02,-0.12523168E-02,
        -0.12369136E-02, 0.70053828E-03,-0.40035954E-03,-0.77157305E-03,
        -0.87612501E-03, 0.11284131E-02,-0.66167879E-03, 0.79686835E-03,
         0.67464262E-03,-0.42742101E-03, 0.66867814E-03,-0.12202427E-02,
         0.40700118E-03, 0.92552515E-03,-0.26460338E-03, 0.44041572E-03,
         0.41934018E-03, 0.14037461E-02,-0.90424839E-03,-0.27055049E-03,
         0.18926424E-02,-0.71918720E-03, 0.89631457E-03, 0.14708111E-02,
        -0.84513296E-04, 0.50205192E-04,-0.10246936E-03,-0.20894881E-03,
         0.13081756E-03, 0.11439028E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]    *x24        *x51
        +coeff[  4]*x12*x23        *x51
        +coeff[  5]*x12    *x33    *x51
        +coeff[  6]*x12*x22    *x41*x52
        +coeff[  7]    *x21            
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[  8]    *x21*x31        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]                *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]*x12    *x31        
        +coeff[ 16]    *x21*x33        
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 17]            *x42*x52
        +coeff[ 18]    *x21        *x53
        +coeff[ 19]                *x54
        +coeff[ 20]*x11*x21*x32        
        +coeff[ 21]*x11*x21    *x42    
        +coeff[ 22]*x11*x22        *x51
        +coeff[ 23]*x11        *x42*x51
        +coeff[ 24]*x12    *x31*x41    
        +coeff[ 25]*x12    *x31    *x51
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]*x11*x21*x31*x42    
        +coeff[ 28]*x11    *x31*x41*x52
        +coeff[ 29]*x12    *x33        
        +coeff[ 30]*x12*x21*x31*x41    
        +coeff[ 31]    *x23*x32*x41    
        +coeff[ 32]    *x23*x31*x42    
        +coeff[ 33]    *x21*x32*x43    
        +coeff[ 34]    *x24*x31    *x51
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 35]    *x21*x33*x41*x51
        +coeff[ 36]        *x31*x44*x51
        +coeff[ 37]*x11*x23    *x42    
        +coeff[ 38]*x11*x21    *x44    
        +coeff[ 39]*x11*x24        *x51
        +coeff[ 40]*x11*x22    *x42*x51
        +coeff[ 41]*x11    *x32*x42*x51
        +coeff[ 42]*x11        *x44*x51
        +coeff[ 43]*x11    *x31*x41*x53
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 44]*x12    *x33*x41    
        +coeff[ 45]*x12*x21    *x43    
        +coeff[ 46]*x12    *x31*x42*x51
        +coeff[ 47]*x12*x21*x31    *x52
        +coeff[ 48]    *x22*x33*x42    
        +coeff[ 49]    *x23*x31*x43    
        +coeff[ 50]    *x23    *x41*x53
        +coeff[ 51]    *x22*x31    *x54
        +coeff[ 52]*x11*x24*x31*x41    
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 53]*x11*x23*x31*x42    
        +coeff[ 54]*x11*x22*x32*x42    
        +coeff[ 55]*x11*x22*x33    *x51
        +coeff[ 56]*x11*x23*x31*x41*x51
        +coeff[ 57]*x11*x22    *x42*x52
        +coeff[ 58]*x11    *x32*x42*x52
        +coeff[ 59]*x11        *x44*x52
        +coeff[ 60]*x12*x24        *x51
        +coeff[ 61]*x12*x23        *x52
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 62]*x12    *x33    *x52
        +coeff[ 63]*x12    *x32*x41*x52
        +coeff[ 64]*x12*x21    *x42*x52
        +coeff[ 65]*x12*x22        *x53
        +coeff[ 66]*x12    *x32    *x53
        +coeff[ 67]*x12    *x31    *x54
        +coeff[ 68]*x13*x22*x32        
        +coeff[ 69]    *x24*x34        
        +coeff[ 70]*x13*x21*x32*x41    
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 71]    *x24*x33*x41    
        +coeff[ 72]*x13*x22    *x42    
        +coeff[ 73]    *x24*x31*x43    
        +coeff[ 74]*x13*x23        *x51
        +coeff[ 75]*x13    *x33    *x51
        +coeff[ 76]    *x23*x34    *x51
        +coeff[ 77]*x13*x21*x31*x41*x51
        +coeff[ 78]    *x24*x31*x42*x51
        +coeff[ 79]*x13        *x43*x51
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 80]    *x22*x31*x44*x51
        +coeff[ 81]*x13*x22        *x52
        +coeff[ 82]*x13        *x42*x52
        +coeff[ 83]    *x22*x31*x42*x53
        +coeff[ 84]        *x31        
        +coeff[ 85]                *x51
        +coeff[ 86]*x11*x21            
        +coeff[ 87]    *x22*x31        
        +coeff[ 88]        *x33        
    ;
    v_l_e_q1en_2_449                              =v_l_e_q1en_2_449                              
        +coeff[ 89]        *x32*x41    
        ;

    return v_l_e_q1en_2_449                              ;
}
float x_e_q1en_2_400                              (float *x,int m){
    int ncoeff=  5;
    float avdat=  0.2483086E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
        -0.24750753E-03, 0.37413263E-02, 0.11704981E+00, 0.26586975E-03,
         0.11600396E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_400                              ;
}
float t_e_q1en_2_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.8045811E-04;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.80216545E-04,-0.10606640E-02, 0.31045709E-01, 0.11323936E-02,
        -0.18436296E-03, 0.42015836E-04,-0.69900096E-04,-0.97373859E-05,
        -0.40829920E-04, 0.15525828E-03, 0.11327361E-03, 0.13974712E-03,
         0.10478332E-03,-0.55305874E-04, 0.17291176E-05, 0.40671544E-04,
         0.68532274E-04,-0.10922686E-04, 0.20559396E-05, 0.83922923E-05,
         0.73511510E-05, 0.19659377E-04, 0.13884163E-04, 0.11451541E-04,
         0.21963046E-05, 0.78184684E-06,-0.12638439E-04,-0.18811755E-05,
        -0.43847144E-05,-0.40114423E-05,-0.26945470E-05,-0.38190356E-05,
         0.61007131E-05,-0.17189275E-05, 0.23983387E-05, 0.49999517E-05,
         0.35132423E-05, 0.59188483E-07, 0.57032926E-05, 0.53106496E-05,
        -0.65144645E-05,-0.45840357E-05,-0.65234062E-05, 0.10601639E-04,
         0.14137233E-04,-0.38617741E-05,-0.54234674E-05,-0.55147066E-05,
         0.18261186E-04,-0.11140865E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_400                              =v_t_e_q1en_2_400                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_400                              =v_t_e_q1en_2_400                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]*x11*x22*x31*x41    
        +coeff[ 22]*x11*x22    *x42    
        +coeff[ 23]*x11*x21*x32    *x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q1en_2_400                              =v_t_e_q1en_2_400                              
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]*x12    *x32        
        +coeff[ 28]*x11*x21*x32        
        +coeff[ 29]*x11*x22        *x51
        +coeff[ 30]*x11*x21    *x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]*x11*x21        *x52
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]        *x31    *x53
    ;
    v_t_e_q1en_2_400                              =v_t_e_q1en_2_400                              
        +coeff[ 35]*x11*x22*x32        
        +coeff[ 36]*x12*x22    *x41    
        +coeff[ 37]*x13    *x31    *x51
        +coeff[ 38]*x11*x22*x31    *x51
        +coeff[ 39]*x11*x22    *x41*x51
        +coeff[ 40]*x11*x21    *x42*x51
        +coeff[ 41]*x13            *x52
        +coeff[ 42]    *x21    *x42*x52
        +coeff[ 43]*x13*x22*x31        
    ;
    v_t_e_q1en_2_400                              =v_t_e_q1en_2_400                              
        +coeff[ 44]*x11*x22*x33        
        +coeff[ 45]*x11*x23    *x42    
        +coeff[ 46]*x12*x22*x31    *x51
        +coeff[ 47]    *x23*x32    *x51
        +coeff[ 48]*x12*x21*x31*x41*x51
        +coeff[ 49]    *x23*x31*x41*x51
        ;

    return v_t_e_q1en_2_400                              ;
}
float y_e_q1en_2_400                              (float *x,int m){
    int ncoeff= 25;
    float avdat= -0.4486151E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
         0.46657733E-03, 0.65335281E-01, 0.63975915E-01,-0.13898029E-03,
        -0.15740326E-03,-0.99764417E-04,-0.44799814E-04,-0.45967972E-05,
        -0.11274748E-03,-0.69299880E-04,-0.41198163E-06,-0.18357578E-04,
        -0.15039403E-04,-0.16544709E-04,-0.88617657E-04,-0.97466887E-04,
        -0.36303331E-05, 0.72351299E-05,-0.60586735E-05,-0.97948614E-05,
         0.74443733E-05,-0.27125185E-04,-0.14037461E-04, 0.80832424E-05,
         0.18501783E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_400                              =v_y_e_q1en_2_400                              
        +coeff[  8]    *x22*x32*x41    
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31*x44    
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x24    *x41    
        +coeff[ 15]    *x24*x31*x42    
        +coeff[ 16]*x11        *x42    
    ;
    v_y_e_q1en_2_400                              =v_y_e_q1en_2_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]*x11    *x31*x42    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x31*x41*x52
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]*x11*x21*x31*x41*x51
        +coeff[ 23]        *x33    *x52
        +coeff[ 24]        *x32*x43*x51
        ;

    return v_y_e_q1en_2_400                              ;
}
float p_e_q1en_2_400                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3027229E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.31424445E-03, 0.16705530E-01, 0.45242019E-01,-0.68045559E-03,
        -0.61796041E-03,-0.29680596E-03,-0.24868676E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_400                              ;
}
float l_e_q1en_2_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1457454E-02;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14951708E-02,-0.35756326E-02,-0.14241623E-03,-0.78753685E-03,
         0.59457042E-03, 0.58510655E-03,-0.40884354E-03, 0.98486897E-03,
        -0.24593456E-02,-0.23542180E-03, 0.51179383E-03,-0.27299722E-03,
        -0.36512494E-04,-0.16726383E-03, 0.13058673E-03, 0.80272628E-04,
        -0.12501010E-03,-0.24099487E-04,-0.66879475E-04, 0.29463827E-03,
         0.31884335E-03, 0.20593197E-03,-0.23223106E-03,-0.58102550E-03,
         0.25758048E-03, 0.14317119E-03, 0.22179288E-03,-0.35717952E-03,
         0.10605931E-02,-0.89011778E-03,-0.57516684E-03,-0.23568550E-02,
        -0.43622247E-03,-0.12005221E-02, 0.84421318E-03,-0.86403656E-03,
         0.27630755E-03,-0.37322400E-03, 0.97074959E-03,-0.63374959E-03,
        -0.55289501E-03,-0.33682730E-03, 0.14503184E-03, 0.31302025E-03,
        -0.52206579E-03,-0.37019656E-03,-0.52430999E-03, 0.10025763E-02,
         0.52817905E-03, 0.78742235E-03,-0.81271422E-03, 0.93651115E-03,
        -0.60308084E-03, 0.22923465E-03,-0.54577948E-03, 0.90504350E-03,
         0.24008857E-03,-0.94407116E-03,-0.34700075E-03, 0.51766133E-03,
        -0.84262213E-03, 0.34557126E-03, 0.61542547E-03,-0.66578801E-03,
        -0.83881483E-03, 0.55715267E-03, 0.19258339E-02,-0.16391989E-02,
         0.27565800E-02,-0.39604522E-03, 0.86140400E-03,-0.14105880E-02,
         0.92338142E-03, 0.16527781E-02, 0.63473481E-03,-0.63830969E-03,
         0.11527095E-02, 0.13989545E-02,-0.56185637E-03, 0.45211698E-03,
        -0.89604431E-03,-0.71606610E-03, 0.11746204E-02,-0.72783732E-03,
         0.66482072E-03,-0.14504798E-03, 0.70729887E-03,-0.15354577E-02,
         0.52957464E-03, 0.13344733E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11    *x31*x41    
        +coeff[  5]        *x31*x43*x51
        +coeff[  6]        *x31*x42*x52
        +coeff[  7]*x11*x22*x32    *x51
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[  8]        *x32*x44*x51
        +coeff[  9]        *x32        
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]*x11    *x32        
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 17]*x11*x21        *x51
        +coeff[ 18]*x11            *x52
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x22        *x52
        +coeff[ 21]    *x21    *x41*x52
        +coeff[ 22]            *x42*x52
        +coeff[ 23]*x11*x23            
        +coeff[ 24]*x11*x21*x31    *x51
        +coeff[ 25]*x11        *x42*x51
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 26]*x12*x21    *x41    
        +coeff[ 27]*x12        *x42    
        +coeff[ 28]        *x34*x41    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]        *x33*x41*x51
        +coeff[ 31]    *x22    *x42*x51
        +coeff[ 32]    *x21*x32    *x52
        +coeff[ 33]    *x22    *x41*x52
        +coeff[ 34]    *x21*x31    *x53
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 35]*x11*x23*x31        
        +coeff[ 36]*x11*x21*x32*x41    
        +coeff[ 37]*x11*x22    *x41*x51
        +coeff[ 38]*x11*x22        *x52
        +coeff[ 39]*x11    *x31*x41*x52
        +coeff[ 40]*x12    *x32*x41    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]*x12    *x31*x41*x51
        +coeff[ 43]*x13*x21*x31        
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 44]*x13*x21    *x41    
        +coeff[ 45]    *x21*x34*x41    
        +coeff[ 46]    *x23*x31*x42    
        +coeff[ 47]    *x21*x31*x42*x52
        +coeff[ 48]*x11*x22*x32*x41    
        +coeff[ 49]*x11*x23    *x41*x51
        +coeff[ 50]*x11*x21*x32*x41*x51
        +coeff[ 51]*x11        *x43*x52
        +coeff[ 52]*x11        *x42*x53
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 53]*x11    *x31    *x54
        +coeff[ 54]*x11        *x41*x54
        +coeff[ 55]*x12*x21*x33        
        +coeff[ 56]*x12    *x34        
        +coeff[ 57]*x12*x21*x31*x42    
        +coeff[ 58]*x12    *x31*x43    
        +coeff[ 59]*x12    *x31*x42*x51
        +coeff[ 60]*x12*x21*x31    *x52
        +coeff[ 61]*x13    *x33        
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 62]    *x23*x34        
        +coeff[ 63]        *x34*x43    
        +coeff[ 64]    *x23*x33    *x51
        +coeff[ 65]    *x24    *x42*x51
        +coeff[ 66]        *x34*x42*x51
        +coeff[ 67]    *x21*x32*x43*x51
        +coeff[ 68]    *x22    *x44*x51
        +coeff[ 69]*x13*x21        *x52
        +coeff[ 70]    *x24    *x41*x52
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 71]    *x23*x31*x41*x52
        +coeff[ 72]    *x21*x33*x41*x52
        +coeff[ 73]    *x22    *x43*x52
        +coeff[ 74]    *x24        *x53
        +coeff[ 75]    *x22*x32    *x53
        +coeff[ 76]    *x21*x32*x41*x53
        +coeff[ 77]*x11*x23*x33        
        +coeff[ 78]*x11*x24    *x42    
        +coeff[ 79]*x11*x23    *x43    
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 80]*x11*x22*x31*x43    
        +coeff[ 81]*x11*x22*x32    *x52
        +coeff[ 82]*x11*x21*x32*x41*x52
        +coeff[ 83]*x11*x23        *x53
        +coeff[ 84]*x11*x21    *x42*x53
        +coeff[ 85]*x12*x23*x32        
        +coeff[ 86]*x12*x24    *x41    
        +coeff[ 87]*x12*x21*x32*x42    
        +coeff[ 88]*x12*x21    *x44    
    ;
    v_l_e_q1en_2_400                              =v_l_e_q1en_2_400                              
        +coeff[ 89]*x12*x22*x31*x41*x51
        ;

    return v_l_e_q1en_2_400                              ;
}
float x_e_q1en_2_350                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.7917899E-04;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30029E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.92210219E-04, 0.37429384E-02, 0.11703490E+00, 0.26628884E-03,
         0.11365164E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_350                              ;
}
float t_e_q1en_2_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1293140E-04;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30029E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.16424778E-04,-0.10598844E-02, 0.31086767E-01, 0.11336304E-02,
        -0.18016835E-03, 0.40904386E-04,-0.62798375E-04,-0.74532340E-05,
        -0.46678641E-04, 0.14849343E-03, 0.10847704E-03, 0.12784411E-03,
         0.97805387E-04,-0.49481474E-04,-0.54337370E-05, 0.33799413E-04,
         0.69293339E-04,-0.90185313E-05, 0.13821942E-05, 0.24399869E-04,
         0.16498454E-04,-0.26741202E-04,-0.16329215E-04,-0.54934617E-06,
        -0.20743464E-05,-0.27548635E-05, 0.20410603E-05,-0.21592383E-06,
        -0.21760420E-05,-0.47273147E-05, 0.21776093E-05, 0.61093363E-06,
         0.67526375E-05, 0.29211976E-05, 0.10801026E-04,-0.45723223E-05,
         0.30176973E-05, 0.10050239E-04, 0.89964424E-05,-0.10955728E-04,
         0.99933432E-05,-0.25428983E-05, 0.40479813E-05, 0.45094844E-05,
         0.71298236E-05,-0.38574631E-05, 0.61115029E-05, 0.87560684E-05,
        -0.50079966E-05, 0.89595087E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1en_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_350                              =v_t_e_q1en_2_350                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_350                              =v_t_e_q1en_2_350                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]*x11*x22*x31*x41    
        +coeff[ 20]*x11*x22    *x42    
        +coeff[ 21]*x11*x23    *x41*x51
        +coeff[ 22]    *x21*x32*x41*x52
        +coeff[ 23]        *x31        
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_t_e_q1en_2_350                              =v_t_e_q1en_2_350                              
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]    *x22    *x41    
        +coeff[ 28]*x11            *x52
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x12    *x31*x41    
        +coeff[ 31]*x11*x22        *x51
        +coeff[ 32]    *x23        *x51
        +coeff[ 33]    *x21*x32    *x51
        +coeff[ 34]*x11*x21    *x41*x51
    ;
    v_t_e_q1en_2_350                              =v_t_e_q1en_2_350                              
        +coeff[ 35]    *x21    *x42*x51
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]*x13*x21*x31        
        +coeff[ 38]*x11*x23*x31        
        +coeff[ 39]*x11*x21*x33        
        +coeff[ 40]*x11    *x31*x43    
        +coeff[ 41]*x13*x21        *x51
        +coeff[ 42]    *x21*x31*x42*x51
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_t_e_q1en_2_350                              =v_t_e_q1en_2_350                              
        +coeff[ 44]    *x21    *x42*x52
        +coeff[ 45]*x11*x23*x32        
        +coeff[ 46]*x12*x21*x31*x42    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]*x11*x23*x31    *x51
        +coeff[ 49]*x11*x22*x32    *x51
        ;

    return v_t_e_q1en_2_350                              ;
}
float y_e_q1en_2_350                              (float *x,int m){
    int ncoeff= 21;
    float avdat=  0.3559203E-03;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30029E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
        -0.34689921E-03, 0.65306447E-01, 0.63964605E-01,-0.13102594E-03,
        -0.16011973E-03,-0.96809097E-04,-0.11146753E-03,-0.18230616E-04,
        -0.13780191E-03,-0.11844021E-03,-0.13759456E-04,-0.44211729E-05,
        -0.93315975E-04,-0.35963338E-04,-0.11316066E-04, 0.55368764E-05,
        -0.11988142E-04, 0.11655316E-04,-0.92002620E-05,-0.20566560E-04,
         0.12885249E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_350                              =v_y_e_q1en_2_350                              
        +coeff[  8]    *x22*x33*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]    *x24    *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]        *x32*x41*x51
    ;
    v_y_e_q1en_2_350                              =v_y_e_q1en_2_350                              
        +coeff[ 17]        *x33*x42    
        +coeff[ 18]    *x21    *x42*x52
        +coeff[ 19]*x11*x23*x31        
        +coeff[ 20]    *x22*x32*x42    
        ;

    return v_y_e_q1en_2_350                              ;
}
float p_e_q1en_2_350                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2411898E-03;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30029E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.23533106E-03, 0.16675303E-01, 0.45191221E-01,-0.67961629E-03,
        -0.61750802E-03,-0.29369266E-03,-0.24745864E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_350                              ;
}
float l_e_q1en_2_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1498452E-02;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30029E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14500044E-02,-0.31788545E-02,-0.17916787E-03,-0.12946387E-02,
         0.11878243E-02,-0.23110888E-02,-0.16142097E-02,-0.81014447E-03,
         0.10890899E-03,-0.19543630E-03,-0.17659370E-03, 0.54666502E-05,
        -0.11048941E-03,-0.31327910E-03, 0.14339156E-03, 0.21821879E-03,
         0.82726305E-03,-0.89685120E-04,-0.38508304E-04, 0.48607361E-03,
         0.21834929E-03, 0.74775267E-03, 0.27880137E-03,-0.74194593E-03,
        -0.47728492E-03,-0.79802051E-03,-0.70629787E-04, 0.25771707E-03,
        -0.40039868E-03,-0.18814798E-03,-0.31513179E-03,-0.97606004E-04,
         0.35684305E-03,-0.50946930E-03,-0.11766057E-02, 0.14712480E-02,
         0.10665808E-02,-0.10907116E-02, 0.11133470E-02,-0.61982952E-03,
         0.12523659E-02,-0.11957354E-02, 0.35268857E-03,-0.27422555E-03,
         0.31271364E-03,-0.26310698E-03,-0.48563207E-03,-0.10669519E-02,
         0.29136350E-04,-0.15839640E-03, 0.69843908E-03,-0.31784692E-03,
         0.36149082E-03, 0.15764150E-02, 0.68833044E-03, 0.24778585E-03,
         0.10793208E-02,-0.11673875E-02, 0.29437486E-03,-0.80443575E-03,
         0.10722759E-02,-0.73667691E-03,-0.99137868E-03, 0.68979152E-03,
        -0.46535917E-02, 0.24563384E-02, 0.58389013E-03, 0.32621485E-02,
        -0.89286850E-03,-0.16871258E-02, 0.65011287E-03, 0.88005756E-04,
        -0.11837328E-03,-0.21928146E-03,-0.14302350E-03, 0.10027441E-03,
        -0.97018034E-04,-0.80072037E-04, 0.43657135E-04, 0.88754045E-04,
         0.12031326E-03, 0.95636031E-04,-0.87144777E-04,-0.13297939E-03,
        -0.26618462E-03, 0.13101818E-03,-0.15075710E-03, 0.26691746E-03,
         0.27085090E-03, 0.41725134E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x13*x22*x31        
        +coeff[  5]    *x22*x32*x41*x52
        +coeff[  6]*x11*x24    *x41*x51
        +coeff[  7]*x12    *x31    *x54
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[  8]            *x41    
        +coeff[  9]                *x51
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]    *x21    *x41    
        +coeff[ 12]    *x21        *x51
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]*x11*x21        *x51
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]*x13                
        +coeff[ 21]        *x32*x42    
        +coeff[ 22]    *x21    *x41*x52
        +coeff[ 23]*x11*x22*x31        
        +coeff[ 24]    *x21*x32*x42    
        +coeff[ 25]        *x31*x43*x51
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 26]*x11*x24            
        +coeff[ 27]*x11*x21*x32    *x51
        +coeff[ 28]*x11    *x32*x41*x51
        +coeff[ 29]*x11*x22        *x52
        +coeff[ 30]*x11*x21    *x41*x52
        +coeff[ 31]*x12*x21*x32        
        +coeff[ 32]*x12    *x31*x42    
        +coeff[ 33]*x12    *x31*x41*x51
        +coeff[ 34]        *x34*x42    
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 35]    *x22*x32*x41*x51
        +coeff[ 36]    *x22*x31*x42*x51
        +coeff[ 37]        *x33*x42*x51
        +coeff[ 38]    *x22    *x42*x52
        +coeff[ 39]    *x22*x31    *x53
        +coeff[ 40]*x11*x24*x31        
        +coeff[ 41]*x11*x22*x33        
        +coeff[ 42]*x11*x21    *x44    
        +coeff[ 43]*x11    *x33*x41*x51
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 44]*x11    *x33    *x52
        +coeff[ 45]*x11*x21        *x54
        +coeff[ 46]*x12*x22    *x41*x51
        +coeff[ 47]*x12*x22        *x52
        +coeff[ 48]*x12    *x32    *x52
        +coeff[ 49]*x12        *x41*x53
        +coeff[ 50]*x12            *x54
        +coeff[ 51]    *x23*x34        
        +coeff[ 52]*x13*x21*x31*x41    
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 53]    *x21*x34*x42    
        +coeff[ 54]    *x24*x31    *x52
        +coeff[ 55]*x13        *x41*x52
        +coeff[ 56]*x11*x23*x32    *x51
        +coeff[ 57]*x11*x21*x32*x42*x51
        +coeff[ 58]*x11    *x33*x41*x52
        +coeff[ 59]*x11    *x32*x42*x52
        +coeff[ 60]*x12    *x32*x41*x52
        +coeff[ 61]*x12*x21    *x42*x52
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 62]*x12        *x43*x52
        +coeff[ 63]*x12        *x41*x54
        +coeff[ 64]    *x24*x31*x42*x51
        +coeff[ 65]    *x22*x33*x42*x51
        +coeff[ 66]    *x24    *x43*x51
        +coeff[ 67]    *x22*x31*x44*x51
        +coeff[ 68]*x13    *x31*x41*x52
        +coeff[ 69]    *x22*x32*x41*x53
        +coeff[ 70]    *x21*x32*x42*x53
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 71]    *x21            
        +coeff[ 72]*x11                
        +coeff[ 73]                *x52
        +coeff[ 74]    *x21*x32        
        +coeff[ 75]    *x22        *x51
        +coeff[ 76]    *x21    *x41*x51
        +coeff[ 77]            *x42*x51
        +coeff[ 78]            *x41*x52
        +coeff[ 79]                *x53
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 80]*x11    *x32        
        +coeff[ 81]*x11        *x42    
        +coeff[ 82]*x11    *x31    *x51
        +coeff[ 83]*x12        *x41    
        +coeff[ 84]    *x22*x32        
        +coeff[ 85]        *x34        
        +coeff[ 86]    *x22    *x42    
        +coeff[ 87]    *x21*x31*x42    
        +coeff[ 88]            *x44    
    ;
    v_l_e_q1en_2_350                              =v_l_e_q1en_2_350                              
        +coeff[ 89]    *x22*x31    *x51
        ;

    return v_l_e_q1en_2_350                              ;
}
float x_e_q1en_2_300                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.1069197E-02;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.10600777E-02, 0.37408932E-02, 0.11706728E+00, 0.26964900E-03,
         0.10243680E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_300                              ;
}
float t_e_q1en_2_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2881824E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.28496599E-03,-0.10564164E-02, 0.31160004E-01, 0.11361869E-02,
        -0.17949879E-03, 0.40678409E-04,-0.65537737E-04,-0.62064432E-05,
        -0.46424953E-04, 0.15502049E-03, 0.10077078E-03, 0.13690630E-03,
         0.10286376E-03,-0.53232212E-04,-0.31945367E-05, 0.34207660E-04,
         0.66455308E-04,-0.19402421E-05, 0.21152811E-04, 0.65068907E-05,
        -0.99702438E-05,-0.93710505E-05, 0.41190674E-05, 0.11800004E-05,
        -0.81475127E-05,-0.14004504E-05,-0.87656354E-05, 0.34255004E-05,
         0.38483809E-05,-0.31670079E-05, 0.74434947E-05, 0.18736850E-04,
        -0.57235425E-05, 0.64034148E-05, 0.43839059E-05,-0.35349087E-05,
        -0.49377381E-05, 0.65936802E-05,-0.49979121E-05,-0.12509371E-04,
         0.11705059E-04, 0.62082017E-05,-0.74801546E-05,-0.56038634E-05,
         0.20761639E-04,-0.69457788E-05, 0.44301050E-05, 0.11030492E-04,
         0.33433428E-05,-0.49862620E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_300                              =v_t_e_q1en_2_300                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_300                              =v_t_e_q1en_2_300                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11*x22*x31*x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]    *x21*x31*x41*x53
        +coeff[ 22]    *x22            
        +coeff[ 23]            *x42    
        +coeff[ 24]*x11*x22            
        +coeff[ 25]            *x41*x52
    ;
    v_t_e_q1en_2_300                              =v_t_e_q1en_2_300                              
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]*x12*x22*x31        
        +coeff[ 30]*x13    *x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x21*x31*x42    
        +coeff[ 33]*x11    *x32*x42    
        +coeff[ 34]*x12*x22        *x51
    ;
    v_t_e_q1en_2_300                              =v_t_e_q1en_2_300                              
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]*x11*x21*x31*x41*x51
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]*x11        *x42*x52
        +coeff[ 39]*x12*x22*x31*x41    
        +coeff[ 40]*x11*x23*x31*x41    
        +coeff[ 41]    *x22*x33*x41    
        +coeff[ 42]*x11*x21*x31*x43    
        +coeff[ 43]*x12*x21*x32    *x51
    ;
    v_t_e_q1en_2_300                              =v_t_e_q1en_2_300                              
        +coeff[ 44]*x11*x22*x31*x41*x51
        +coeff[ 45]*x11    *x31*x43*x51
        +coeff[ 46]*x13*x21        *x52
        +coeff[ 47]    *x21*x32*x41*x52
        +coeff[ 48]*x11        *x43*x52
        +coeff[ 49]    *x21    *x43*x52
        ;

    return v_t_e_q1en_2_300                              ;
}
float y_e_q1en_2_300                              (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.6955451E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.66021708E-03, 0.65329589E-01, 0.63948743E-01,-0.14157896E-03,
        -0.15719373E-03,-0.76812212E-04,-0.65488603E-04,-0.11398487E-04,
        -0.80103986E-04,-0.10004832E-03,-0.97714488E-04,-0.13156807E-04,
         0.55835187E-06,-0.16374977E-04,-0.59184858E-04, 0.30049248E-04,
        -0.39698934E-05,-0.29837145E-05, 0.64036512E-05, 0.78041076E-05,
        -0.28589839E-04,-0.23403523E-04, 0.63059902E-05,-0.82085380E-05,
        -0.14072651E-04, 0.10602226E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_300                              =v_y_e_q1en_2_300                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x24    *x41    
        +coeff[ 15]    *x22    *x43*x51
        +coeff[ 16]            *x42*x51
    ;
    v_y_e_q1en_2_300                              =v_y_e_q1en_2_300                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]    *x23    *x43    
        +coeff[ 23]        *x31*x41*x53
        +coeff[ 24]    *x22    *x45    
        +coeff[ 25]    *x21*x31*x45    
        ;

    return v_y_e_q1en_2_300                              ;
}
float p_e_q1en_2_300                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.4313549E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.41217537E-03, 0.16632851E-01, 0.45180473E-01,-0.67668426E-03,
        -0.61510247E-03,-0.29444709E-03,-0.24563898E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_300                              ;
}
float l_e_q1en_2_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1458261E-02;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14187347E-02,-0.30868354E-02,-0.10024766E-02,-0.63217070E-03,
         0.73676946E-03,-0.64389133E-05, 0.29111395E-02, 0.31838434E-04,
         0.16858525E-03,-0.20640367E-03,-0.93141589E-05,-0.26492612E-03,
        -0.10091144E-03, 0.10772303E-03,-0.20896693E-03, 0.17904498E-03,
         0.65242674E-03,-0.24434767E-03, 0.36979909E-03, 0.83597394E-03,
         0.39983084E-03,-0.23730112E-03,-0.35853550E-03,-0.83286257E-03,
        -0.27834490E-03, 0.40475937E-03, 0.11930263E-03, 0.99735707E-03,
        -0.65024418E-04, 0.20473904E-02, 0.61782193E-03,-0.13524096E-03,
         0.23869459E-03,-0.62504224E-03, 0.12152352E-03,-0.68947428E-03,
         0.14105955E-02,-0.41546804E-03,-0.73865475E-03, 0.12050886E-03,
        -0.98105718E-03,-0.12767983E-02,-0.39384092E-03,-0.83467510E-03,
         0.70925785E-03,-0.65198954E-03,-0.34770329E-03,-0.22425798E-03,
         0.94087509E-03,-0.48827854E-03,-0.47043682E-03, 0.41288079E-03,
         0.57001977E-03,-0.11771711E-02,-0.26432441E-02, 0.14216640E-02,
        -0.24997309E-03, 0.71056257E-03,-0.18155443E-02, 0.19060407E-02,
        -0.13721065E-02,-0.79061871E-03,-0.68035617E-03,-0.40665272E-03,
        -0.81504305E-03, 0.13315948E-03, 0.66841149E-03,-0.45950903E-03,
         0.53250510E-03, 0.11413707E-02, 0.16082586E-02,-0.44255020E-03,
        -0.89846930E-03, 0.28502883E-02,-0.20551495E-04,-0.89902409E-04,
        -0.45890323E-04, 0.11495518E-03, 0.10447721E-03,-0.75872376E-04,
         0.90732050E-04, 0.19584764E-03,-0.15566885E-03,-0.27851693E-03,
         0.82611034E-04,-0.43060875E-03, 0.82210863E-04,-0.10660643E-03,
         0.10037395E-03,-0.24216920E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]    *x21*x32*x41    
        +coeff[  4]        *x31    *x53
        +coeff[  5]    *x24        *x51
        +coeff[  6]*x11*x23*x31*x41*x51
        +coeff[  7]    *x21            
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[  8]    *x21    *x41    
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]        *x31    *x51
        +coeff[ 12]            *x41*x51
        +coeff[ 13]*x11*x21            
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]                *x53
        +coeff[ 16]*x11    *x31    *x51
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 17]    *x24            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]            *x43*x51
        +coeff[ 21]    *x21    *x41*x52
        +coeff[ 22]*x11    *x31*x42    
        +coeff[ 23]*x11    *x31*x41*x51
        +coeff[ 24]*x12*x21*x31        
        +coeff[ 25]*x12*x21        *x51
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 26]*x13    *x31        
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]        *x31*x44    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x11    *x33*x41    
        +coeff[ 31]*x11    *x31*x43    
        +coeff[ 32]*x11*x23        *x51
        +coeff[ 33]*x11    *x31*x41*x52
        +coeff[ 34]*x11            *x54
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 35]*x12*x21*x31*x41    
        +coeff[ 36]    *x21*x34*x41    
        +coeff[ 37]    *x24    *x42    
        +coeff[ 38]*x13    *x31    *x51
        +coeff[ 39]*x13        *x41*x51
        +coeff[ 40]    *x23*x31*x41*x51
        +coeff[ 41]    *x23    *x41*x52
        +coeff[ 42]    *x22*x31    *x53
        +coeff[ 43]    *x21    *x41*x54
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 44]*x11*x21*x33    *x51
        +coeff[ 45]*x11*x21*x31*x42*x51
        +coeff[ 46]*x11    *x33    *x52
        +coeff[ 47]*x11        *x43*x52
        +coeff[ 48]*x11    *x31*x41*x53
        +coeff[ 49]*x12*x21*x32    *x51
        +coeff[ 50]    *x24*x33        
        +coeff[ 51]*x13*x21*x31*x41    
        +coeff[ 52]    *x23*x33*x41    
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 53]    *x21*x32*x43*x51
        +coeff[ 54]    *x22    *x44*x51
        +coeff[ 55]    *x22*x32*x41*x52
        +coeff[ 56]    *x22    *x43*x52
        +coeff[ 57]    *x21*x32*x41*x53
        +coeff[ 58]*x11*x21*x31*x43*x51
        +coeff[ 59]*x11*x21    *x43*x52
        +coeff[ 60]*x11*x21*x31    *x54
        +coeff[ 61]*x11*x21    *x41*x54
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 62]*x12    *x32*x42*x51
        +coeff[ 63]*x12    *x31*x43*x51
        +coeff[ 64]*x12*x21    *x42*x52
        +coeff[ 65]*x13*x23*x31        
        +coeff[ 66]    *x22*x34*x42    
        +coeff[ 67]    *x24    *x43*x51
        +coeff[ 68]*x13*x22        *x52
        +coeff[ 69]*x13*x21*x31    *x52
        +coeff[ 70]    *x23*x33    *x52
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 71]    *x21*x33*x42*x52
        +coeff[ 72]    *x21*x33    *x54
        +coeff[ 73]    *x23    *x41*x54
        +coeff[ 74]        *x31        
        +coeff[ 75]*x11                
        +coeff[ 76]        *x32        
        +coeff[ 77]*x11    *x31        
        +coeff[ 78]    *x23            
        +coeff[ 79]    *x22*x31        
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 80]    *x21*x32        
        +coeff[ 81]        *x33        
        +coeff[ 82]    *x22    *x41    
        +coeff[ 83]        *x31*x42    
        +coeff[ 84]            *x43    
        +coeff[ 85]    *x22        *x51
        +coeff[ 86]    *x21        *x52
        +coeff[ 87]            *x41*x52
        +coeff[ 88]*x11    *x32        
    ;
    v_l_e_q1en_2_300                              =v_l_e_q1en_2_300                              
        +coeff[ 89]*x11*x21    *x41    
        ;

    return v_l_e_q1en_2_300                              ;
}
float x_e_q1en_2_250                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.8043235E-03;
    float xmin[10]={
        -0.39989E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.82022540E-03, 0.37443978E-02, 0.11708169E+00, 0.26446217E-03,
         0.11676442E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_250                              ;
}
float t_e_q1en_2_250                              (float *x,int m){
    int ncoeff= 48;
    float avdat= -0.2092747E-03;
    float xmin[10]={
        -0.39989E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 49]={
         0.21336833E-03,-0.10539681E-02, 0.31247335E-01, 0.11302747E-02,
        -0.17294769E-03, 0.39598144E-04,-0.58688969E-04,-0.60664738E-05,
        -0.40737548E-04, 0.13985393E-03, 0.98262652E-04, 0.13154809E-03,
         0.97646887E-04,-0.46227255E-04, 0.12746226E-04, 0.66790453E-04,
        -0.97278516E-05, 0.15987555E-05,-0.21566634E-05, 0.85715201E-05,
         0.27561266E-04, 0.40549057E-07,-0.35818598E-07, 0.24796959E-05,
         0.48039684E-07,-0.14909965E-05, 0.52359833E-05,-0.13614424E-05,
         0.40966279E-05,-0.13881928E-04,-0.30330277E-05, 0.37236960E-05,
         0.70818492E-05, 0.73804163E-05,-0.27992203E-05,-0.52887044E-05,
        -0.38735088E-05, 0.64671967E-05,-0.84986395E-05,-0.82844472E-05,
         0.76683127E-05, 0.26579582E-05,-0.75177550E-05, 0.79343490E-05,
        -0.38368808E-05,-0.83814903E-05, 0.70439582E-05, 0.15584197E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_250                              =v_t_e_q1en_2_250                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22*x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_250                              =v_t_e_q1en_2_250                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11*x22    *x41    
        +coeff[ 19]*x11*x22*x32        
        +coeff[ 20]    *x23*x32        
        +coeff[ 21]                *x51
        +coeff[ 22]*x12*x21            
        +coeff[ 23]*x11    *x31*x41    
        +coeff[ 24]    *x21*x31    *x51
        +coeff[ 25]        *x31    *x52
    ;
    v_t_e_q1en_2_250                              =v_t_e_q1en_2_250                              
        +coeff[ 26]*x12*x21    *x41    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]*x11*x21    *x41*x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x22        *x52
        +coeff[ 31]*x11            *x53
        +coeff[ 32]*x12*x21*x32        
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x11*x21    *x43    
    ;
    v_t_e_q1en_2_250                              =v_t_e_q1en_2_250                              
        +coeff[ 35]*x11*x21*x31*x41*x51
        +coeff[ 36]*x11*x21    *x42*x51
        +coeff[ 37]    *x21*x31*x42*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]    *x21    *x42*x52
        +coeff[ 41]*x12            *x53
        +coeff[ 42]*x11*x22*x32*x41    
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_q1en_2_250                              =v_t_e_q1en_2_250                              
        +coeff[ 44]    *x23*x31    *x52
        +coeff[ 45]*x12*x21    *x41*x52
        +coeff[ 46]*x11*x21*x31*x41*x52
        +coeff[ 47]    *x21*x31*x41*x53
        ;

    return v_t_e_q1en_2_250                              ;
}
float y_e_q1en_2_250                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.1365824E-04;
    float xmin[10]={
        -0.39989E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.31431173E-04, 0.65284528E-01, 0.63939966E-01,-0.13778544E-03,
        -0.15560804E-03,-0.81183724E-04,-0.55954672E-04,-0.74949226E-05,
         0.42992106E-05,-0.11390948E-04,-0.14466091E-04,-0.69529313E-04,
        -0.71968971E-04,-0.10025001E-03,-0.84113366E-04,-0.14728968E-04,
         0.84980894E-06, 0.52330256E-05,-0.72251287E-05,-0.37020495E-05,
         0.70263186E-05,-0.10462137E-04, 0.12374070E-04,-0.36083198E-04,
         0.15608788E-04,-0.12690640E-04, 0.20017827E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_250                              =v_y_e_q1en_2_250                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x22*x31*x42    
        +coeff[ 12]    *x24    *x41    
        +coeff[ 13]    *x22*x32*x41    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x32*x41    
        +coeff[ 16]*x11*x21            
    ;
    v_y_e_q1en_2_250                              =v_y_e_q1en_2_250                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x44    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x21*x31*x41*x52
        +coeff[ 25]*x11*x21    *x41*x52
    ;
    v_y_e_q1en_2_250                              =v_y_e_q1en_2_250                              
        +coeff[ 26]*x11*x21*x32*x42    
        ;

    return v_y_e_q1en_2_250                              ;
}
float p_e_q1en_2_250                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2671862E-04;
    float xmin[10]={
        -0.39989E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.12714280E-04, 0.16578784E-01, 0.45107257E-01,-0.67424116E-03,
        -0.61500003E-03,-0.29475626E-03,-0.24462942E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_250                              ;
}
float l_e_q1en_2_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1455766E-02;
    float xmin[10]={
        -0.39989E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.13484254E-02,-0.31438200E-02,-0.13994418E-03,-0.80402882E-03,
         0.26192059E-03, 0.43146746E-03, 0.33950442E-06,-0.49218669E-03,
         0.23498903E-03, 0.10390397E-02,-0.12950422E-02,-0.88081328E-03,
        -0.16112364E-03,-0.12790374E-03, 0.44505563E-04, 0.28387090E-03,
         0.38240204E-03,-0.53134911E-04, 0.20022267E-03,-0.47304219E-03,
         0.14935799E-03,-0.31873747E-03, 0.37162798E-03,-0.79987431E-03,
         0.39335078E-03,-0.32199870E-03, 0.17880683E-03,-0.29144817E-03,
        -0.89574070E-03,-0.27278028E-03, 0.18984838E-04, 0.11101516E-03,
        -0.19625371E-03,-0.21993861E-03,-0.41365754E-03, 0.20146048E-03,
        -0.22792850E-03,-0.16950062E-02, 0.41291185E-03, 0.68459820E-04,
         0.71417255E-03, 0.11743866E-02,-0.46058724E-03,-0.64138754E-03,
        -0.13145094E-02,-0.37475221E-03, 0.11880853E-02,-0.30340708E-03,
         0.83021924E-03,-0.12866837E-02, 0.10144445E-02,-0.25430025E-03,
        -0.36959862E-03,-0.87824749E-03,-0.73532178E-03, 0.11466627E-02,
         0.82713779E-03,-0.74442301E-03,-0.48542063E-03, 0.93451742E-03,
         0.18728440E-04,-0.70864038E-03,-0.90470549E-03, 0.20911415E-03,
         0.26476115E-03,-0.73303282E-03,-0.19042023E-02, 0.72858168E-03,
         0.34116421E-03, 0.17185857E-02, 0.16369000E-02, 0.77166432E-03,
         0.89627277E-03,-0.50388434E-03, 0.10551764E-02,-0.60872512E-03,
        -0.11500058E-02, 0.19598857E-02, 0.65228913E-03,-0.45912113E-03,
        -0.83232945E-03, 0.80033275E-03,-0.86488895E-03, 0.16247913E-02,
        -0.60305418E-03,-0.73678886E-04, 0.43122065E-04,-0.12532157E-03,
         0.52896423E-04, 0.87784691E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x42*x51
        +coeff[  6]*x11*x21    *x41    
        +coeff[  7]*x11        *x43    
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[  8]    *x21*x33    *x52
        +coeff[  9]*x12*x21        *x53
        +coeff[ 10]    *x22    *x43*x53
        +coeff[ 11]    *x22*x32    *x54
        +coeff[ 12]            *x41    
        +coeff[ 13]                *x51
        +coeff[ 14]            *x41*x51
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]            *x43    
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x21        *x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 26]        *x31*x42*x51
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11    *x31*x41*x51
        +coeff[ 29]*x11        *x41*x52
        +coeff[ 30]*x11            *x53
        +coeff[ 31]*x12*x22            
        +coeff[ 32]    *x21*x33    *x51
        +coeff[ 33]    *x21    *x43*x51
        +coeff[ 34]    *x23        *x52
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 35]        *x31*x41*x53
        +coeff[ 36]*x11    *x34        
        +coeff[ 37]*x11*x21*x32*x41    
        +coeff[ 38]*x11*x22    *x41*x51
        +coeff[ 39]*x11*x21*x31    *x52
        +coeff[ 40]*x11*x21        *x53
        +coeff[ 41]*x12*x22    *x41    
        +coeff[ 42]*x12        *x43    
        +coeff[ 43]*x12    *x31*x41*x51
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 44]    *x21*x33*x42    
        +coeff[ 45]    *x22*x31*x43    
        +coeff[ 46]    *x21*x31*x44    
        +coeff[ 47]    *x23*x31    *x52
        +coeff[ 48]    *x22    *x42*x52
        +coeff[ 49]    *x21*x31*x42*x52
        +coeff[ 50]    *x21*x31*x41*x53
        +coeff[ 51]    *x22        *x54
        +coeff[ 52]*x11*x24        *x51
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 53]*x11*x21*x32*x41*x51
        +coeff[ 54]*x11*x21*x31*x42*x51
        +coeff[ 55]*x11    *x31*x41*x53
        +coeff[ 56]*x12*x22*x32        
        +coeff[ 57]*x12*x23        *x51
        +coeff[ 58]*x12*x22        *x52
        +coeff[ 59]    *x22*x33*x42    
        +coeff[ 60]    *x24    *x43    
        +coeff[ 61]    *x23*x31*x43    
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 62]    *x24    *x42*x51
        +coeff[ 63]*x13*x21        *x52
        +coeff[ 64]*x13    *x31    *x52
        +coeff[ 65]    *x24*x31    *x52
        +coeff[ 66]    *x24    *x41*x52
        +coeff[ 67]    *x21*x31*x43*x52
        +coeff[ 68]*x13            *x53
        +coeff[ 69]    *x22    *x41*x54
        +coeff[ 70]*x11*x21*x34*x41    
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 71]*x11    *x31*x44*x51
        +coeff[ 72]*x11*x21*x31*x42*x52
        +coeff[ 73]*x11    *x33    *x53
        +coeff[ 74]*x12*x22    *x42*x51
        +coeff[ 75]*x12    *x32*x42*x51
        +coeff[ 76]*x12*x22    *x41*x52
        +coeff[ 77]*x12    *x31*x41*x53
        +coeff[ 78]*x12    *x31    *x54
        +coeff[ 79]    *x24*x34        
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 80]    *x21*x34*x43    
        +coeff[ 81]    *x23*x32*x42*x51
        +coeff[ 82]    *x22*x32*x43*x51
        +coeff[ 83]    *x23*x32*x41*x52
        +coeff[ 84]    *x23    *x41*x54
        +coeff[ 85]        *x31        
        +coeff[ 86]*x11                
        +coeff[ 87]    *x21*x31        
        +coeff[ 88]    *x21    *x41    
    ;
    v_l_e_q1en_2_250                              =v_l_e_q1en_2_250                              
        +coeff[ 89]                *x52
        ;

    return v_l_e_q1en_2_250                              ;
}
float x_e_q1en_2_200                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.6222612E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.65398664E-03, 0.37459056E-02, 0.11709418E+00, 0.25814719E-03,
         0.11203776E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_200                              ;
}
float t_e_q1en_2_200                              (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.1755712E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.18391247E-03,-0.10490956E-02, 0.31386353E-01, 0.11268453E-02,
        -0.17877306E-03, 0.42095686E-04,-0.68214809E-04,-0.29618755E-05,
        -0.43870255E-04, 0.14311899E-03, 0.10381817E-03, 0.12726654E-03,
         0.10649693E-03,-0.51521398E-04, 0.12800065E-05, 0.72841991E-04,
        -0.86475193E-05, 0.19987060E-05, 0.35839301E-04, 0.21338539E-04,
         0.13012690E-04,-0.87174458E-05, 0.38839644E-05, 0.39158190E-05,
         0.55028895E-05,-0.21769404E-05, 0.15605970E-06,-0.64333944E-05,
         0.35002197E-05,-0.21799542E-05,-0.24709948E-05,-0.96264148E-05,
         0.53966471E-06, 0.54282068E-05,-0.84702415E-05, 0.28821817E-05,
        -0.42461761E-05, 0.37396971E-05,-0.81884182E-05, 0.68443405E-05,
        -0.43064583E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_200                              =v_t_e_q1en_2_200                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_200                              =v_t_e_q1en_2_200                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]*x11*x22*x31*x41    
        +coeff[ 20]*x11*x22    *x42    
        +coeff[ 21]*x11*x21*x31*x42    
        +coeff[ 22]    *x22    *x43    
        +coeff[ 23]    *x22        *x53
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]*x12*x21            
    ;
    v_t_e_q1en_2_200                              =v_t_e_q1en_2_200                              
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]    *x21*x33        
        +coeff[ 28]*x12    *x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]    *x21    *x41*x52
        +coeff[ 31]*x12*x21    *x41*x51
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x21    *x41*x53
        +coeff[ 34]*x12*x21*x31*x42    
    ;
    v_t_e_q1en_2_200                              =v_t_e_q1en_2_200                              
        +coeff[ 35]*x11*x21*x32*x42    
        +coeff[ 36]        *x33*x43    
        +coeff[ 37]*x11*x23        *x52
        +coeff[ 38]*x11*x22*x31    *x52
        +coeff[ 39]    *x22*x31*x41*x52
        +coeff[ 40]*x11    *x32    *x53
        ;

    return v_t_e_q1en_2_200                              ;
}
float y_e_q1en_2_200                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.4270943E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.41584595E-03, 0.65303691E-01, 0.63908629E-01,-0.13858099E-03,
        -0.15662453E-03,-0.79391684E-04,-0.52317137E-04,-0.68490840E-05,
        -0.24053394E-04,-0.12687296E-04,-0.17277434E-04,-0.66176617E-04,
        -0.96011820E-04,-0.89267291E-04,-0.46349171E-04,-0.95680291E-04,
         0.39226329E-05, 0.56671961E-05, 0.18144046E-04, 0.48028892E-05,
        -0.50213430E-05, 0.91246002E-05, 0.54202096E-05,-0.31812047E-04,
        -0.16038755E-04,-0.11336409E-04, 0.12524835E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_200                              =v_y_e_q1en_2_200                              
        +coeff[  8]        *x33*x42    
        +coeff[  9]*x11*x21    *x41    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x24    *x41    
        +coeff[ 12]    *x22*x32*x41    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x22*x33        
        +coeff[ 15]    *x22*x31*x44    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q1en_2_200                              =v_y_e_q1en_2_200                              
        +coeff[ 17]    *x21    *x42    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]*x11    *x31*x41    
        +coeff[ 20]    *x21    *x41*x51
        +coeff[ 21]            *x41*x52
        +coeff[ 22]*x12        *x42    
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x11    *x31*x42*x51
    ;
    v_y_e_q1en_2_200                              =v_y_e_q1en_2_200                              
        +coeff[ 26]    *x22*x31    *x52
        ;

    return v_y_e_q1en_2_200                              ;
}
float p_e_q1en_2_200                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2277025E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.22521615E-03, 0.16492154E-01, 0.45057368E-01,-0.67071681E-03,
        -0.61201304E-03,-0.28876390E-03,-0.24145430E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_200                              ;
}
float l_e_q1en_2_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1472271E-02;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14147329E-02, 0.83675586E-04,-0.32309580E-02,-0.88251522E-03,
         0.59778395E-03,-0.75305119E-03,-0.10876664E-02,-0.10847838E-03,
         0.90277324E-04,-0.21939949E-03,-0.24769845E-03,-0.38102063E-03,
        -0.52198844E-04,-0.92947143E-04, 0.28451498E-05, 0.52419666E-03,
         0.14422034E-03,-0.36662820E-03,-0.27554308E-03,-0.34992208E-03,
         0.74141333E-03, 0.11297211E-03, 0.57356938E-03, 0.20678462E-03,
         0.92797639E-03, 0.39367352E-03,-0.33544691E-03,-0.16637943E-02,
        -0.24642880E-03, 0.22939953E-03, 0.14362037E-03,-0.14936028E-02,
         0.98492543E-03, 0.11336483E-02,-0.10815088E-02, 0.41264150E-03,
        -0.47435128E-03, 0.82662964E-03, 0.14559053E-02, 0.43397804E-03,
         0.25628289E-03,-0.71169500E-03, 0.68856718E-03, 0.86747756E-03,
        -0.63431676E-03, 0.36059163E-03, 0.10857778E-02, 0.91190898E-03,
         0.17891858E-02,-0.10158651E-02,-0.91192487E-03,-0.27287059E-03,
        -0.11077748E-02,-0.95596741E-03, 0.47614315E-03,-0.10686206E-02,
        -0.97741734E-03, 0.72173460E-03,-0.56097965E-03,-0.11925101E-02,
         0.43870890E-03, 0.38362635E-03, 0.14537179E-02,-0.11876919E-02,
         0.89285400E-03,-0.25252732E-02, 0.29429677E-02, 0.84720762E-03,
         0.17651335E-02,-0.40817031E-03, 0.31705440E-04,-0.15324986E-03,
        -0.13948031E-02,-0.80959243E-03,-0.18708272E-02, 0.72279136E-03,
        -0.44651693E-03,-0.90354175E-03, 0.22363773E-03,-0.73103711E-03,
         0.69384644E-03, 0.80504501E-03,-0.26831566E-03,-0.18284800E-02,
        -0.77447144E-03, 0.10772434E-02, 0.17581243E-02, 0.69860776E-03,
         0.10450204E-02,-0.85142959E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]            *x42    
        +coeff[  4]    *x23    *x43    
        +coeff[  5]        *x32*x42*x52
        +coeff[  6]*x11*x22*x31*x42    
        +coeff[  7]    *x21            
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[  8]        *x31        
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x33        
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]*x11*x21*x31        
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 17]*x12*x21            
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]        *x33    *x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21    *x41*x52
        +coeff[ 22]    *x21        *x53
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]*x11*x21*x31*x41    
        +coeff[ 25]*x11        *x43    
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 26]*x11    *x31*x41*x51
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]*x12*x22            
        +coeff[ 29]*x13    *x31        
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23        *x52
        +coeff[ 32]    *x22*x31    *x52
        +coeff[ 33]    *x21    *x42*x52
        +coeff[ 34]*x11*x21*x31*x42    
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 35]*x11*x21    *x43    
        +coeff[ 36]*x11*x21*x31    *x52
        +coeff[ 37]*x12*x23            
        +coeff[ 38]*x12*x21*x31*x41    
        +coeff[ 39]*x12    *x32*x41    
        +coeff[ 40]*x12*x21*x31    *x51
        +coeff[ 41]*x12        *x42*x51
        +coeff[ 42]*x13    *x31*x41    
        +coeff[ 43]    *x21*x31*x43*x51
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 44]    *x21*x31*x42*x52
        +coeff[ 45]*x11*x21    *x44    
        +coeff[ 46]*x11    *x31*x43*x51
        +coeff[ 47]*x11    *x31*x41*x53
        +coeff[ 48]*x11        *x41*x54
        +coeff[ 49]*x12*x22    *x41*x51
        +coeff[ 50]*x12*x21*x31*x41*x51
        +coeff[ 51]*x13*x21*x32        
        +coeff[ 52]*x13*x21*x31*x41    
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 53]    *x21*x33*x43    
        +coeff[ 54]    *x23*x33    *x51
        +coeff[ 55]*x13    *x31*x41*x51
        +coeff[ 56]    *x22*x33    *x52
        +coeff[ 57]    *x23*x31*x41*x52
        +coeff[ 58]    *x22*x32*x41*x52
        +coeff[ 59]    *x23    *x42*x52
        +coeff[ 60]    *x23*x31    *x53
        +coeff[ 61]            *x44*x53
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 62]    *x23        *x54
        +coeff[ 63]    *x21    *x42*x54
        +coeff[ 64]*x11*x24*x31*x41    
        +coeff[ 65]*x11*x22*x33*x41    
        +coeff[ 66]*x11*x21*x33*x42    
        +coeff[ 67]*x11*x24        *x52
        +coeff[ 68]*x11    *x33*x41*x52
        +coeff[ 69]*x11        *x44*x52
        +coeff[ 70]*x11    *x31*x41*x54
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 71]*x12*x22*x33        
        +coeff[ 72]*x12*x23*x31*x41    
        +coeff[ 73]*x12*x22*x31*x42    
        +coeff[ 74]*x12*x21*x33    *x51
        +coeff[ 75]*x12    *x32*x42*x51
        +coeff[ 76]*x13*x24            
        +coeff[ 77]*x13*x21*x33        
        +coeff[ 78]*x13    *x34        
        +coeff[ 79]*x13*x23    *x41    
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 80]*x13*x21*x32*x41    
        +coeff[ 81]    *x22*x33*x43    
        +coeff[ 82]    *x24    *x43*x51
        +coeff[ 83]*x13    *x31*x41*x52
        +coeff[ 84]    *x24*x31*x41*x52
        +coeff[ 85]    *x23*x32*x41*x52
        +coeff[ 86]    *x23*x31*x42*x52
        +coeff[ 87]        *x33*x43*x52
        +coeff[ 88]    *x24    *x41*x53
    ;
    v_l_e_q1en_2_200                              =v_l_e_q1en_2_200                              
        +coeff[ 89]    *x21    *x44*x53
        ;

    return v_l_e_q1en_2_200                              ;
}
float x_e_q1en_2_175                              (float *x,int m){
    int ncoeff=  6;
    float avdat=  0.8164253E-03;
    float xmin[10]={
        -0.40000E-02,-0.60055E-01,-0.59975E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30005E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
        -0.81028847E-03, 0.11712781E+00,-0.10163837E-04, 0.37537226E-02,
         0.26134832E-03, 0.11079793E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_175                              ;
}
float t_e_q1en_2_175                              (float *x,int m){
    int ncoeff= 48;
    float avdat=  0.2100065E-03;
    float xmin[10]={
        -0.40000E-02,-0.60055E-01,-0.59975E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30005E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 49]={
        -0.20840585E-03,-0.10503061E-02, 0.31490885E-01, 0.11235361E-02,
        -0.18148827E-03, 0.38427796E-04,-0.66333560E-04,-0.14787115E-04,
        -0.44701999E-04, 0.14932021E-03, 0.11635833E-03, 0.13569147E-03,
         0.10881115E-03,-0.51618685E-04, 0.35428893E-04, 0.19897308E-04,
         0.63116131E-04,-0.15033300E-04, 0.47866974E-05,-0.87347298E-05,
         0.54865391E-05,-0.74784516E-05,-0.74363311E-05, 0.20745456E-05,
         0.32566479E-06, 0.11307646E-05, 0.47860162E-05, 0.76559027E-05,
         0.33192596E-05, 0.38005642E-05, 0.38177654E-05,-0.33200342E-05,
         0.47507729E-05,-0.75770931E-05, 0.32926023E-05,-0.56260392E-05,
         0.41127664E-05,-0.56810395E-05,-0.53721851E-05,-0.50522995E-05,
        -0.74896880E-05,-0.15404019E-04,-0.59988088E-05,-0.50497315E-05,
         0.87400476E-05, 0.81953194E-05, 0.81594981E-05,-0.52594373E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_175                              =v_t_e_q1en_2_175                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_175                              =v_t_e_q1en_2_175                              
        +coeff[ 17]*x11*x22        *x52
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x12*x22*x31        
        +coeff[ 21]*x11*x22*x31    *x51
        +coeff[ 22]*x12*x21*x31*x41*x51
        +coeff[ 23]    *x21*x31        
        +coeff[ 24]            *x42    
        +coeff[ 25]        *x31    *x51
    ;
    v_t_e_q1en_2_175                              =v_t_e_q1en_2_175                              
        +coeff[ 26]*x13                
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]    *x22        *x51
        +coeff[ 29]*x12*x21    *x41    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]*x11            *x53
        +coeff[ 33]*x13    *x32        
        +coeff[ 34]*x11*x22    *x41*x51
    ;
    v_t_e_q1en_2_175                              =v_t_e_q1en_2_175                              
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]    *x22    *x41*x52
        +coeff[ 37]*x12*x22*x31*x41    
        +coeff[ 38]    *x23*x32*x41    
        +coeff[ 39]    *x21*x33*x42    
        +coeff[ 40]    *x23*x31*x41*x51
        +coeff[ 41]*x11*x21*x31*x42*x51
        +coeff[ 42]*x11    *x32*x42*x51
        +coeff[ 43]    *x22*x32    *x52
    ;
    v_t_e_q1en_2_175                              =v_t_e_q1en_2_175                              
        +coeff[ 44]*x11*x21*x31*x41*x52
        +coeff[ 45]    *x22    *x42*x52
        +coeff[ 46]*x11*x21*x31    *x53
        +coeff[ 47]    *x21*x32    *x53
        ;

    return v_t_e_q1en_2_175                              ;
}
float y_e_q1en_2_175                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.7930918E-04;
    float xmin[10]={
        -0.40000E-02,-0.60055E-01,-0.59975E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30005E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.60442981E-04, 0.65230966E-01, 0.63894495E-01,-0.13556427E-03,
        -0.16106293E-03,-0.75001240E-04,-0.44227832E-04,-0.81762455E-05,
        -0.96165604E-04,-0.10559874E-03,-0.90427995E-04,-0.13341596E-04,
        -0.17351889E-05,-0.88339279E-04,-0.22954939E-04, 0.40620312E-05,
         0.58260944E-05, 0.10906663E-04,-0.73350593E-05,-0.27650800E-04,
         0.96523345E-05, 0.14395206E-04,-0.19514175E-04, 0.12798023E-04,
         0.14910791E-04,-0.15755430E-04, 0.16439266E-04, 0.15844596E-04,
        -0.13598693E-04, 0.14258313E-04,-0.14060793E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_175                              =v_y_e_q1en_2_175                              
        +coeff[  8]    *x22*x31*x42    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]    *x24    *x41    
        +coeff[ 14]*x13*x21    *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]        *x31    *x52
    ;
    v_y_e_q1en_2_175                              =v_y_e_q1en_2_175                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]        *x31*x43*x51
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]            *x43*x52
        +coeff[ 21]*x12    *x31*x42    
        +coeff[ 22]*x11*x23*x31        
        +coeff[ 23]        *x31*x44*x51
        +coeff[ 24]*x13        *x42    
        +coeff[ 25]*x11        *x42*x52
    ;
    v_y_e_q1en_2_175                              =v_y_e_q1en_2_175                              
        +coeff[ 26]    *x22    *x43*x51
        +coeff[ 27]*x11*x21    *x41*x52
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]*x11*x21    *x43*x51
        +coeff[ 30]    *x21*x33*x41*x51
        ;

    return v_y_e_q1en_2_175                              ;
}
float p_e_q1en_2_175                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2717722E-04;
    float xmin[10]={
        -0.40000E-02,-0.60055E-01,-0.59975E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30005E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.11361646E-04, 0.16432002E-01, 0.44959664E-01,-0.66709914E-03,
        -0.60668535E-03,-0.29400035E-03,-0.24575199E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_175                              ;
}
float l_e_q1en_2_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1457649E-02;
    float xmin[10]={
        -0.40000E-02,-0.60055E-01,-0.59975E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30005E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14983342E-02,-0.34519609E-02,-0.12416316E-02,-0.76313416E-04,
        -0.10152342E-02,-0.49231085E-03,-0.76525105E-03,-0.16764835E-02,
         0.48670021E-03, 0.55150694E-03, 0.16143722E-02,-0.26467415E-02,
         0.14907897E-03, 0.24348959E-03, 0.13314551E-03, 0.25818130E-03,
         0.24232153E-03,-0.46129586E-04, 0.27177529E-03,-0.21467700E-03,
         0.33454684E-03,-0.69317408E-04,-0.19138370E-03,-0.53416850E-04,
        -0.50945011E-04, 0.48709408E-03, 0.42842669E-03,-0.32322737E-03,
        -0.18677781E-02, 0.22389497E-03,-0.22054241E-03,-0.73581847E-03,
         0.69572433E-03, 0.96721551E-03,-0.10191215E-02,-0.48792592E-03,
         0.51811797E-03,-0.75820705E-03,-0.87699143E-03,-0.14153942E-02,
        -0.24791881E-02, 0.28146725E-03, 0.39465795E-03, 0.37522145E-03,
         0.38571449E-03,-0.39436424E-03, 0.28081381E-03, 0.98764198E-03,
        -0.33227119E-03,-0.40509593E-03, 0.37599815E-03, 0.13162299E-02,
        -0.67209697E-03, 0.65633963E-03,-0.36736421E-03, 0.56832231E-03,
         0.48086920E-03,-0.35767179E-03, 0.97252283E-03, 0.31200529E-03,
        -0.45727653E-03, 0.13817583E-02, 0.77559886E-03,-0.61966473E-03,
        -0.35112884E-03,-0.67179411E-03,-0.14825617E-02, 0.11454044E-02,
        -0.46342885E-03,-0.38533809E-03,-0.10814364E-02, 0.33041876E-03,
         0.47066194E-03, 0.16827579E-02,-0.41581897E-03, 0.15166744E-02,
        -0.43441294E-03, 0.78737992E-03,-0.40309227E-03, 0.10932795E-02,
         0.11792334E-02, 0.14750983E-02,-0.11343303E-02, 0.13704005E-02,
         0.13750194E-02,-0.52267645E-03, 0.70732855E-03, 0.92016120E-03,
        -0.60330273E-03,-0.61474775E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x33*x41    
        +coeff[  4]*x11*x21*x31    *x51
        +coeff[  5]*x12    *x31*x41    
        +coeff[  6]*x13        *x41    
        +coeff[  7]*x11*x23        *x52
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[  8]*x11*x22    *x41*x52
        +coeff[  9]*x12*x23*x31        
        +coeff[ 10]*x13*x23            
        +coeff[ 11]*x12    *x31*x43*x51
        +coeff[ 12]        *x31        
        +coeff[ 13]    *x21    *x41    
        +coeff[ 14]    *x21        *x51
        +coeff[ 15]        *x31    *x51
        +coeff[ 16]*x11        *x41    
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21    *x42    
        +coeff[ 19]        *x31*x42    
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]*x11*x21*x31        
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]        *x32*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]*x11*x23            
        +coeff[ 32]*x11*x21*x32        
        +coeff[ 33]        *x33*x41*x51
        +coeff[ 34]    *x21    *x42*x52
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 35]        *x31*x41*x53
        +coeff[ 36]*x11*x23    *x41    
        +coeff[ 37]*x11*x21*x32*x41    
        +coeff[ 38]*x11*x21*x31*x42    
        +coeff[ 39]*x11*x21    *x42*x51
        +coeff[ 40]*x11    *x31*x42*x51
        +coeff[ 41]*x11    *x31*x41*x52
        +coeff[ 42]*x11*x21        *x53
        +coeff[ 43]*x11    *x31    *x53
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 44]*x12*x22    *x41    
        +coeff[ 45]*x12        *x43    
        +coeff[ 46]*x12*x22        *x51
        +coeff[ 47]*x12    *x31*x41*x51
        +coeff[ 48]*x12    *x31    *x52
        +coeff[ 49]*x13*x21    *x41    
        +coeff[ 50]    *x22*x31*x43    
        +coeff[ 51]    *x24*x31    *x51
        +coeff[ 52]*x13        *x41*x51
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 53]    *x22*x31*x42*x51
        +coeff[ 54]    *x21*x31*x42*x52
        +coeff[ 55]    *x22*x31    *x53
        +coeff[ 56]    *x21    *x41*x54
        +coeff[ 57]*x11*x21*x31*x43    
        +coeff[ 58]*x11*x21*x33    *x51
        +coeff[ 59]*x11*x23    *x41*x51
        +coeff[ 60]*x11    *x31*x41*x53
        +coeff[ 61]*x11*x21        *x54
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 62]*x12*x21*x32    *x51
        +coeff[ 63]*x12*x21    *x41*x52
        +coeff[ 64]*x12*x21        *x53
        +coeff[ 65]*x13*x21*x32        
        +coeff[ 66]    *x24*x32*x41    
        +coeff[ 67]    *x22*x34*x41    
        +coeff[ 68]*x13    *x31*x42    
        +coeff[ 69]    *x22    *x44*x51
        +coeff[ 70]*x13*x21        *x52
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 71]*x13    *x31    *x52
        +coeff[ 72]*x13        *x41*x52
        +coeff[ 73]    *x21    *x44*x52
        +coeff[ 74]    *x21*x32    *x54
        +coeff[ 75]*x11*x21*x34*x41    
        +coeff[ 76]*x11*x21*x34    *x51
        +coeff[ 77]*x11*x24    *x41*x51
        +coeff[ 78]*x11*x22*x32*x41*x51
        +coeff[ 79]*x11    *x34*x41*x51
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 80]*x11    *x33*x42*x51
        +coeff[ 81]*x11    *x31*x44*x51
        +coeff[ 82]*x11*x23*x31    *x52
        +coeff[ 83]*x11*x21*x31*x42*x52
        +coeff[ 84]*x11*x21    *x42*x53
        +coeff[ 85]*x12*x23*x31*x41    
        +coeff[ 86]*x12*x22*x31*x42    
        +coeff[ 87]*x12*x22    *x43    
        +coeff[ 88]*x12*x21    *x44    
    ;
    v_l_e_q1en_2_175                              =v_l_e_q1en_2_175                              
        +coeff[ 89]*x12*x23*x31    *x51
        ;

    return v_l_e_q1en_2_175                              ;
}
float x_e_q1en_2_150                              (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.5602173E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.57072192E-03, 0.37490888E-02, 0.11717336E+00, 0.26084360E-03,
         0.11477840E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_150                              ;
}
float t_e_q1en_2_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1486410E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15080710E-03,-0.10400334E-02, 0.31626441E-01, 0.11132931E-02,
        -0.17874208E-03, 0.39188362E-04,-0.68379406E-04,-0.76074166E-05,
        -0.39860130E-04, 0.14255148E-03, 0.10448229E-03, 0.13737309E-03,
         0.10706353E-03,-0.52518721E-04, 0.35535420E-04, 0.14580334E-04,
         0.71561233E-04,-0.12092812E-04, 0.29232394E-05,-0.23666112E-06,
        -0.37004310E-06, 0.13574015E-05,-0.27453470E-06, 0.30866283E-05,
        -0.18404148E-05, 0.14073584E-05,-0.31305041E-06, 0.37835982E-05,
        -0.14014543E-04, 0.62220233E-05, 0.46870573E-05,-0.35653031E-06,
         0.54005582E-05, 0.77074174E-05, 0.36139154E-05, 0.62131662E-05,
         0.11453283E-04, 0.65450681E-05,-0.45625970E-05,-0.47035114E-05,
        -0.76585993E-05, 0.21260723E-04, 0.60012862E-05, 0.41848953E-05,
        -0.93411500E-05,-0.73314463E-05, 0.65616073E-05, 0.12762946E-04,
        -0.62722111E-05,-0.66221392E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_150                              =v_t_e_q1en_2_150                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1en_2_150                              =v_t_e_q1en_2_150                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]        *x31        
        +coeff[ 20]    *x21*x31        
        +coeff[ 21]        *x32        
        +coeff[ 22]    *x21    *x41    
        +coeff[ 23]    *x22*x31        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_q1en_2_150                              =v_t_e_q1en_2_150                              
        +coeff[ 26]        *x31*x41*x51
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]*x11*x21*x31*x41    
        +coeff[ 29]*x12*x21        *x51
        +coeff[ 30]    *x21        *x53
        +coeff[ 31]*x13    *x32        
        +coeff[ 32]*x12*x21*x32        
        +coeff[ 33]*x11*x22*x32        
        +coeff[ 34]*x13    *x31*x41    
    ;
    v_t_e_q1en_2_150                              =v_t_e_q1en_2_150                              
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]*x11        *x42*x52
        +coeff[ 40]    *x21    *x42*x52
        +coeff[ 41]*x13*x21*x31*x41    
        +coeff[ 42]*x12*x22*x31*x41    
        +coeff[ 43]    *x23    *x43    
    ;
    v_t_e_q1en_2_150                              =v_t_e_q1en_2_150                              
        +coeff[ 44]*x12*x21*x32    *x51
        +coeff[ 45]*x12*x21*x31*x41*x51
        +coeff[ 46]*x11*x22    *x42*x51
        +coeff[ 47]*x11*x21*x31*x41*x52
        +coeff[ 48]*x11*x21    *x42*x52
        +coeff[ 49]    *x21*x31*x42*x52
        ;

    return v_t_e_q1en_2_150                              ;
}
float y_e_q1en_2_150                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.3216474E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.31740000E-03, 0.65264709E-01, 0.63894026E-01,-0.13978021E-03,
        -0.15562696E-03,-0.80784688E-04,-0.77017066E-04,-0.20388721E-04,
        -0.23853647E-05,-0.11147503E-03,-0.16053747E-04,-0.13330495E-04,
        -0.74223841E-04,-0.87709508E-04,-0.76649130E-05,-0.85325983E-05,
        -0.77433024E-05, 0.80868404E-05, 0.95886817E-05,-0.19797390E-04,
        -0.40528368E-04, 0.81577809E-05,-0.30294701E-04,-0.67855076E-05,
         0.23973829E-04, 0.54995917E-05,-0.55972146E-05,-0.19880725E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_150                              =v_y_e_q1en_2_150                              
        +coeff[  8]        *x33*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]        *x33        
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]    *x22*x31*x42    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]            *x41*x52
    ;
    v_y_e_q1en_2_150                              =v_y_e_q1en_2_150                              
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22    *x43    
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]*x11    *x32*x42    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]*x11    *x31    *x52
        +coeff[ 24]            *x43*x52
        +coeff[ 25]*x11        *x45    
    ;
    v_y_e_q1en_2_150                              =v_y_e_q1en_2_150                              
        +coeff[ 26]            *x42*x53
        +coeff[ 27]        *x32*x45    
        ;

    return v_y_e_q1en_2_150                              ;
}
float p_e_q1en_2_150                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.1933266E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.18875377E-03, 0.16354809E-01, 0.44917628E-01,-0.66653575E-03,
        -0.60764374E-03,-0.29012101E-03,-0.24364593E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_150                              ;
}
float l_e_q1en_2_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1433260E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.15233792E-02,-0.34462807E-02, 0.47976820E-04,-0.45347269E-03,
        -0.12512717E-02,-0.62858831E-03, 0.48473343E-03, 0.84612577E-03,
         0.79146173E-03, 0.13665544E-02, 0.13143496E-03, 0.10313062E-03,
         0.22834624E-03, 0.10398881E-02,-0.95395308E-05,-0.33987605E-03,
        -0.25243894E-03,-0.41704156E-03, 0.57716261E-04,-0.26250057E-03,
        -0.10737192E-02,-0.30975620E-03, 0.38207386E-03, 0.41434518E-03,
         0.52575592E-03, 0.31386866E-03,-0.89609181E-03,-0.33904560E-03,
         0.38821821E-04,-0.82412153E-03, 0.36871026E-03, 0.39292354E-03,
        -0.12684916E-02, 0.61971002E-03, 0.91884326E-03,-0.63134113E-03,
        -0.48798960E-03, 0.46377227E-03,-0.78956009E-03,-0.42821854E-03,
        -0.26852259E-03,-0.14235107E-03, 0.71399892E-03, 0.44727357E-03,
         0.49421616E-03, 0.91158540E-03,-0.26356126E-03,-0.35327196E-03,
        -0.61880745E-03,-0.73865597E-03, 0.40081737E-03, 0.67585916E-03,
        -0.13241644E-02, 0.44396927E-03, 0.61421900E-03, 0.50436716E-04,
         0.48112802E-03, 0.39023173E-03,-0.20617043E-04,-0.52615441E-03,
         0.14265982E-02,-0.53123437E-03, 0.49946242E-03,-0.59604866E-03,
        -0.90610079E-03, 0.12070450E-02, 0.73881284E-03, 0.88044477E-03,
         0.63681981E-03,-0.13988685E-02, 0.12951667E-02, 0.59764931E-03,
         0.94485516E-03,-0.59694209E-03, 0.14295283E-02,-0.10226220E-02,
        -0.10726461E-02, 0.67932933E-03, 0.71311410E-03, 0.14172276E-02,
         0.75923163E-03,-0.22833071E-03,-0.11301060E-03,-0.58126225E-04,
        -0.41462907E-04,-0.67628956E-04, 0.24946075E-03,-0.70838512E-04,
         0.69950431E-04,-0.70297770E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x32*x42    
        +coeff[  6]            *x41*x53
        +coeff[  7]*x11    *x31*x41*x52
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[  8]        *x33*x43    
        +coeff[  9]        *x32*x44    
        +coeff[ 10]                *x51
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x22    *x41    
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21    *x41*x51
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]*x11    *x32        
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]*x11*x21        *x51
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]*x13                
        +coeff[ 23]    *x21*x32*x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]    *x21    *x41*x52
        +coeff[ 28]*x11*x22    *x41    
        +coeff[ 29]    *x23*x32        
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x23        *x52
        +coeff[ 32]    *x21*x31*x41*x52
        +coeff[ 33]    *x21    *x42*x52
        +coeff[ 34]*x11*x23        *x51
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]*x11*x22    *x41*x51
        +coeff[ 37]*x11        *x43*x51
        +coeff[ 38]*x11*x22        *x52
        +coeff[ 39]*x12*x21*x32        
        +coeff[ 40]*x12    *x32*x41    
        +coeff[ 41]*x12        *x42*x51
        +coeff[ 42]*x13*x21        *x51
        +coeff[ 43]*x13    *x31    *x51
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 44]*x13        *x41*x51
        +coeff[ 45]        *x34*x41*x51
        +coeff[ 46]    *x23    *x42*x51
        +coeff[ 47]    *x22    *x41*x53
        +coeff[ 48]    *x21*x31    *x54
        +coeff[ 49]*x11*x22*x32*x41    
        +coeff[ 50]*x11*x22*x32    *x51
        +coeff[ 51]*x11*x21*x32*x41*x51
        +coeff[ 52]*x11    *x31*x42*x52
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 53]*x11    *x31    *x54
        +coeff[ 54]*x12*x22*x32        
        +coeff[ 55]*x13    *x33        
        +coeff[ 56]    *x24*x32*x41    
        +coeff[ 57]*x13    *x31*x42    
        +coeff[ 58]    *x23*x31*x43    
        +coeff[ 59]    *x23    *x44    
        +coeff[ 60]    *x23*x31*x41*x52
        +coeff[ 61]    *x22*x31*x42*x52
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 62]    *x22    *x43*x52
        +coeff[ 63]    *x21*x32    *x54
        +coeff[ 64]*x11*x21*x34    *x51
        +coeff[ 65]*x11*x21*x32*x42*x51
        +coeff[ 66]*x11*x22*x32    *x52
        +coeff[ 67]*x11*x23    *x41*x52
        +coeff[ 68]*x11*x21*x31*x42*x52
        +coeff[ 69]*x11    *x31*x41*x54
        +coeff[ 70]*x12*x22    *x42*x51
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 71]*x12*x21    *x43*x51
        +coeff[ 72]*x12*x21    *x42*x52
        +coeff[ 73]*x13*x21*x32*x41    
        +coeff[ 74]    *x23*x32*x43    
        +coeff[ 75]    *x21*x34*x43    
        +coeff[ 76]    *x21*x34*x42*x51
        +coeff[ 77]*x13*x22        *x52
        +coeff[ 78]    *x23*x33    *x52
        +coeff[ 79]*x13    *x31*x41*x52
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 80]    *x23*x32    *x53
        +coeff[ 81]    *x21            
        +coeff[ 82]*x11                
        +coeff[ 83]*x11*x21            
        +coeff[ 84]*x11            *x51
        +coeff[ 85]*x12                
        +coeff[ 86]    *x23            
        +coeff[ 87]        *x33        
        +coeff[ 88]        *x31*x42    
    ;
    v_l_e_q1en_2_150                              =v_l_e_q1en_2_150                              
        +coeff[ 89]    *x21*x31    *x51
        ;

    return v_l_e_q1en_2_150                              ;
}
float x_e_q1en_2_125                              (float *x,int m){
    int ncoeff=  5;
    float avdat=  0.2592420E-03;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
        -0.26034430E-03, 0.37503631E-02, 0.11720549E+00, 0.25885372E-03,
         0.11916302E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_125                              ;
}
float t_e_q1en_2_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.7237809E-04;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.73247160E-04,-0.10330693E-02, 0.31810146E-01, 0.11089853E-02,
        -0.17982112E-03, 0.38914823E-04,-0.61120001E-04,-0.40611822E-05,
        -0.44291006E-04, 0.14139974E-03, 0.10457545E-03, 0.12422299E-03,
         0.10116692E-03,-0.44647721E-04, 0.21087105E-05, 0.63178268E-04,
        -0.37403508E-05,-0.82872029E-05, 0.11030201E-05, 0.32013179E-05,
         0.65410923E-05, 0.31572417E-04, 0.22795164E-04, 0.13070418E-04,
         0.16766120E-05,-0.55083101E-05,-0.31459701E-05, 0.38133621E-05,
         0.36319477E-05,-0.46890686E-05,-0.34597631E-05,-0.19363117E-05,
         0.33563008E-05,-0.56092563E-05, 0.52664227E-05,-0.78755365E-05,
         0.95535097E-05,-0.57256407E-05, 0.48335232E-05, 0.64561623E-05,
         0.72359185E-05,-0.99787521E-05, 0.49066234E-05, 0.72632492E-05,
         0.60647285E-05, 0.63032467E-05,-0.98882028E-05,-0.37868465E-05,
         0.50843641E-05,-0.48343381E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_125                              =v_t_e_q1en_2_125                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]    *x22            
    ;
    v_t_e_q1en_2_125                              =v_t_e_q1en_2_125                              
        +coeff[ 17]*x11*x22            
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x23*x32        
        +coeff[ 22]*x11*x22*x31*x41    
        +coeff[ 23]*x11*x22    *x42    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]        *x31*x41*x51
    ;
    v_t_e_q1en_2_125                              =v_t_e_q1en_2_125                              
        +coeff[ 26]*x11            *x52
        +coeff[ 27]*x11*x21*x31*x41    
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]            *x43*x51
        +coeff[ 32]*x12*x22    *x41    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11*x21*x32    *x51
    ;
    v_t_e_q1en_2_125                              =v_t_e_q1en_2_125                              
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]*x12*x21    *x41*x51
        +coeff[ 37]    *x21*x32*x41*x51
        +coeff[ 38]    *x21*x31*x42*x51
        +coeff[ 39]        *x31*x43*x51
        +coeff[ 40]    *x23        *x52
        +coeff[ 41]    *x21*x32    *x52
        +coeff[ 42]    *x22        *x53
        +coeff[ 43]*x13*x22*x31        
    ;
    v_t_e_q1en_2_125                              =v_t_e_q1en_2_125                              
        +coeff[ 44]*x12*x22*x32        
        +coeff[ 45]    *x23*x32*x41    
        +coeff[ 46]*x11*x22*x31*x42    
        +coeff[ 47]*x12*x21    *x43    
        +coeff[ 48]*x13*x22        *x51
        +coeff[ 49]*x13*x21*x31    *x51
        ;

    return v_t_e_q1en_2_125                              ;
}
float y_e_q1en_2_125                              (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.3697363E-04;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.13087563E-04, 0.65235607E-01, 0.63840739E-01,-0.13063448E-03,
        -0.15846004E-03,-0.77662422E-04,-0.44977383E-04,-0.13452313E-04,
         0.74771815E-05,-0.12388957E-04,-0.19201574E-04,-0.76648757E-04,
        -0.77757213E-04,-0.76762582E-04,-0.87338318E-04,-0.15614538E-04,
         0.95956184E-05, 0.48193579E-05,-0.33273016E-05, 0.79381871E-05,
        -0.16922100E-04,-0.41548255E-05, 0.11250503E-04,-0.26969095E-04,
        -0.18251121E-04,-0.95594996E-05, 0.13333502E-04,-0.28697463E-04,
         0.20871499E-04,-0.14380507E-04, 0.72640200E-05,-0.18121116E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_125                              =v_y_e_q1en_2_125                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]        *x33        
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x22*x31*x42    
        +coeff[ 12]    *x24    *x41    
        +coeff[ 13]    *x22*x32*x41    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x23    *x41    
        +coeff[ 16]    *x21    *x41    
    ;
    v_y_e_q1en_2_125                              =v_y_e_q1en_2_125                              
        +coeff[ 17]            *x42*x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x21*x32*x41    
        +coeff[ 21]    *x23        *x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]            *x44*x51
        +coeff[ 25]*x12        *x41*x51
    ;
    v_y_e_q1en_2_125                              =v_y_e_q1en_2_125                              
        +coeff[ 26]        *x32*x42*x51
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]            *x43*x52
        +coeff[ 29]*x11    *x31*x41*x52
        +coeff[ 30]*x12        *x44    
        +coeff[ 31]*x11*x22*x31*x42    
        ;

    return v_y_e_q1en_2_125                              ;
}
float p_e_q1en_2_125                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.7364320E-05;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.19043482E-04, 0.16236005E-01, 0.44814527E-01,-0.65982278E-03,
        -0.60052535E-03,-0.28574155E-03,-0.24038331E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_125                              ;
}
float l_e_q1en_2_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1463920E-02;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14439018E-02,-0.17183063E-03,-0.35160207E-02,-0.18055829E-03,
        -0.89431251E-03, 0.23638383E-03,-0.39000509E-03, 0.45975606E-03,
         0.95863821E-03, 0.11971167E-02, 0.82096440E-03,-0.13545819E-03,
         0.83300358E-04,-0.11063506E-03, 0.68764744E-03,-0.12409450E-03,
        -0.46642142E-03, 0.94278570E-04, 0.52989059E-03, 0.10099160E-02,
        -0.31367937E-03,-0.29977057E-02, 0.42486284E-03, 0.13273294E-03,
         0.24637824E-03, 0.33771090E-03,-0.22726913E-03,-0.14144930E-03,
         0.52726839E-03,-0.59023954E-03,-0.11503956E-02, 0.23142018E-03,
        -0.26859558E-03,-0.27782213E-04,-0.71563316E-03,-0.39126146E-04,
        -0.92442773E-04,-0.28232386E-03, 0.22587468E-03, 0.73324621E-03,
        -0.31623230E-03,-0.75290934E-03,-0.53946837E-03, 0.37531916E-03,
        -0.18645880E-03,-0.89427480E-03,-0.42139002E-03, 0.27498341E-03,
         0.45362936E-03,-0.12890721E-02,-0.39965817E-03, 0.19502110E-02,
         0.13010907E-02,-0.99620223E-03, 0.61831606E-03, 0.14189461E-02,
        -0.52203174E-03, 0.70036243E-03,-0.40451591E-03,-0.44883756E-03,
         0.79005363E-03, 0.11647826E-02,-0.80403418E-03, 0.47073225E-03,
         0.74525783E-03, 0.71336713E-03, 0.15327060E-02, 0.48510978E-03,
        -0.81936672E-03,-0.65348204E-03, 0.54315152E-03, 0.44462187E-03,
        -0.84983080E-03, 0.49858965E-03, 0.75646222E-03,-0.12231979E-02,
        -0.48274771E-04, 0.10196373E-02,-0.46389116E-03, 0.63942389E-04,
        -0.52401848E-03, 0.56343997E-03,-0.85047213E-03, 0.86649490E-03,
         0.11632744E-02, 0.55984175E-03,-0.10422044E-02,-0.94968046E-03,
         0.12778785E-02,-0.52413548E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q1en_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x32        
        +coeff[  6]        *x32    *x52
        +coeff[  7]*x11*x21*x31*x41    
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[  8]    *x23    *x41*x51
        +coeff[  9]*x11    *x31*x42*x52
        +coeff[ 10]*x12*x22    *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x12                
        +coeff[ 13]    *x23            
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21    *x41*x51
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 17]*x11    *x32        
        +coeff[ 18]*x12            *x51
        +coeff[ 19]    *x21*x33        
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]        *x32*x41*x51
        +coeff[ 23]        *x31*x42*x51
        +coeff[ 24]            *x43*x51
        +coeff[ 25]    *x22        *x52
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]*x11    *x32*x41    
        +coeff[ 28]*x11    *x31*x41*x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x12*x21*x31        
        +coeff[ 31]*x12*x21    *x41    
        +coeff[ 32]*x12    *x31    *x51
        +coeff[ 33]*x13        *x41    
        +coeff[ 34]        *x34*x41    
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 35]    *x21*x33    *x51
        +coeff[ 36]        *x34    *x51
        +coeff[ 37]            *x44*x51
        +coeff[ 38]    *x21*x31    *x53
        +coeff[ 39]*x11*x22*x31*x41    
        +coeff[ 40]*x11    *x33*x41    
        +coeff[ 41]*x11*x22*x31    *x51
        +coeff[ 42]*x11*x21        *x53
        +coeff[ 43]*x12*x21*x32        
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 44]*x12    *x33        
        +coeff[ 45]*x12*x22        *x51
        +coeff[ 46]*x12    *x32    *x51
        +coeff[ 47]*x12        *x41*x52
        +coeff[ 48]*x13*x21*x31        
        +coeff[ 49]    *x23*x33        
        +coeff[ 50]*x13*x21        *x51
        +coeff[ 51]    *x23*x31*x41*x51
        +coeff[ 52]    *x21*x33*x41*x51
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 53]        *x34*x41*x51
        +coeff[ 54]    *x22*x31*x42*x51
        +coeff[ 55]    *x21*x31*x43*x51
        +coeff[ 56]            *x44*x52
        +coeff[ 57]    *x23        *x53
        +coeff[ 58]    *x22    *x41*x53
        +coeff[ 59]*x11*x22*x32*x41    
        +coeff[ 60]*x11*x21*x33*x41    
        +coeff[ 61]*x11    *x34*x41    
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 62]*x11*x24        *x51
        +coeff[ 63]*x11*x22*x32    *x51
        +coeff[ 64]*x11*x22*x31*x41*x51
        +coeff[ 65]*x11*x23        *x52
        +coeff[ 66]*x12*x23*x31        
        +coeff[ 67]*x12        *x42*x52
        +coeff[ 68]*x13*x21*x31*x41    
        +coeff[ 69]*x13    *x32*x41    
        +coeff[ 70]*x13*x22        *x51
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 71]    *x24*x32    *x51
        +coeff[ 72]*x13    *x31*x41*x51
        +coeff[ 73]        *x34*x41*x52
        +coeff[ 74]    *x23*x31    *x53
        +coeff[ 75]    *x21*x33    *x53
        +coeff[ 76]*x11*x23*x31*x42    
        +coeff[ 77]*x11*x22*x33    *x51
        +coeff[ 78]*x11    *x32*x43*x51
        +coeff[ 79]*x11*x23        *x53
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 80]*x11    *x31*x42*x53
        +coeff[ 81]*x12*x21*x33*x41    
        +coeff[ 82]*x12*x22*x31*x41*x51
        +coeff[ 83]*x12*x21*x32*x41*x51
        +coeff[ 84]*x12*x22    *x42*x51
        +coeff[ 85]*x12*x21    *x42*x52
        +coeff[ 86]*x12        *x43*x52
        +coeff[ 87]*x13*x21*x31*x42    
        +coeff[ 88]*x13*x23        *x51
    ;
    v_l_e_q1en_2_125                              =v_l_e_q1en_2_125                              
        +coeff[ 89]        *x33*x43*x52
        ;

    return v_l_e_q1en_2_125                              ;
}
float x_e_q1en_2_100                              (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.8301238E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.82504418E-03, 0.37508651E-02, 0.41238332E-06, 0.11729320E+00,
         0.25612322E-03, 0.11702509E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_x_e_q1en_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        ;

    return v_x_e_q1en_2_100                              ;
}
float t_e_q1en_2_100                              (float *x,int m){
    int ncoeff= 45;
    float avdat= -0.2238642E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 46]={
         0.22281264E-03,-0.10230906E-02, 0.32095272E-01, 0.10967673E-02,
        -0.17324598E-03, 0.39422328E-04,-0.65261251E-04,-0.50493113E-05,
        -0.43875236E-04, 0.14257814E-03, 0.99764133E-04, 0.13090941E-03,
         0.10524145E-03,-0.48574009E-04, 0.10840752E-05, 0.64336913E-04,
        -0.75347330E-05, 0.15814652E-05,-0.54795360E-05, 0.29920455E-04,
         0.24945808E-04,-0.58543935E-06,-0.18801554E-05,-0.10689422E-05,
        -0.19418478E-05, 0.79590500E-05,-0.96583371E-05, 0.66994467E-05,
         0.11349907E-04, 0.19753390E-05, 0.47995654E-05, 0.55635801E-05,
        -0.15404004E-05, 0.28952393E-05,-0.82827146E-05,-0.79153860E-05,
         0.54677162E-05, 0.11827654E-04, 0.50448280E-05,-0.86935097E-05,
        -0.47853250E-05, 0.11539672E-04,-0.47156486E-05,-0.67620977E-05,
         0.11431358E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1en_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1en_2_100                              =v_t_e_q1en_2_100                              
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x33*x41    
        +coeff[ 16]*x11*x22            
    ;
    v_t_e_q1en_2_100                              =v_t_e_q1en_2_100                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]*x11*x22*x31*x41    
        +coeff[ 21]        *x31        
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q1en_2_100                              =v_t_e_q1en_2_100                              
        +coeff[ 26]*x11*x21    *x41*x51
        +coeff[ 27]*x11*x21*x33        
        +coeff[ 28]*x11*x22    *x42    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]*x11*x22*x31    *x51
        +coeff[ 31]    *x21*x33    *x51
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]*x11*x21*x31*x41*x51
    ;
    v_t_e_q1en_2_100                              =v_t_e_q1en_2_100                              
        +coeff[ 35]    *x21    *x43*x51
        +coeff[ 36]    *x21    *x41*x53
        +coeff[ 37]*x11*x23*x31*x41    
        +coeff[ 38]*x12*x22    *x42    
        +coeff[ 39]*x11*x21*x31*x43    
        +coeff[ 40]    *x22*x33    *x51
        +coeff[ 41]*x11*x23    *x41*x51
        +coeff[ 42]*x11*x21    *x42*x52
        +coeff[ 43]    *x23        *x53
    ;
    v_t_e_q1en_2_100                              =v_t_e_q1en_2_100                              
        +coeff[ 44]*x11*x21    *x41*x53
        ;

    return v_t_e_q1en_2_100                              ;
}
float y_e_q1en_2_100                              (float *x,int m){
    int ncoeff= 21;
    float avdat= -0.6419193E-04;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
         0.41662966E-04, 0.65205373E-01, 0.63811503E-01,-0.13357843E-03,
        -0.15151814E-03,-0.12641989E-03,-0.92416041E-04,-0.23978039E-05,
        -0.10560347E-03,-0.10743058E-03,-0.74498748E-04,-0.13037687E-04,
        -0.13366571E-04,-0.10876041E-04, 0.78284747E-05,-0.70167202E-05,
         0.20135356E-04,-0.35877536E-04,-0.22352557E-04, 0.77799841E-05,
        -0.17386514E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        +coeff[  7]        *x32*x41    
    ;
    v_y_e_q1en_2_100                              =v_y_e_q1en_2_100                              
        +coeff[  8]    *x24*x31*x42    
        +coeff[  9]    *x24*x32*x41    
        +coeff[ 10]    *x24*x33        
        +coeff[ 11]*x11*x21    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x12*x21    *x41    
        +coeff[ 16]            *x44*x51
    ;
    v_y_e_q1en_2_100                              =v_y_e_q1en_2_100                              
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11        *x41*x52
        +coeff[ 20]*x11*x21*x31*x42    
        ;

    return v_y_e_q1en_2_100                              ;
}
float p_e_q1en_2_100                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1345052E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.12152339E-03, 0.16072182E-01, 0.44661440E-01,-0.65371464E-03,
        -0.59401034E-03,-0.28700571E-03,-0.23994855E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1en_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1en_2_100                              ;
}
float l_e_q1en_2_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1487758E-02;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.14981903E-02,-0.34043274E-02,-0.29766775E-03, 0.11632766E-04,
        -0.10690816E-02,-0.86993352E-03, 0.10627517E-02, 0.67490136E-03,
         0.69858972E-03, 0.22801201E-04, 0.73007024E-04,-0.26753559E-03,
        -0.10980214E-03,-0.22069518E-03,-0.32614707E-03, 0.29421246E-03,
        -0.75290108E-03,-0.25237608E-03, 0.35113093E-03, 0.36997354E-03,
         0.64109924E-03,-0.32453661E-03,-0.96307689E-03, 0.20669709E-03,
         0.44049499E-04, 0.28405714E-03,-0.27451024E-03,-0.29029237E-03,
        -0.69596781E-03,-0.10470868E-02, 0.90131076E-03,-0.40435116E-02,
         0.79861260E-03,-0.58966019E-03, 0.23715274E-03,-0.27297816E-03,
         0.26131034E-03, 0.77880098E-03, 0.35213513E-03,-0.59613673E-03,
        -0.51370531E-03,-0.84007578E-03, 0.13821539E-02,-0.45038946E-03,
        -0.28802064E-03,-0.58634672E-03,-0.49780431E-03, 0.84515422E-03,
         0.55145967E-03,-0.10135669E-02,-0.65582909E-03,-0.70587907E-03,
        -0.48681861E-03, 0.33795298E-03, 0.61890425E-03, 0.30931487E-03,
        -0.52092155E-03, 0.17966942E-02, 0.21727866E-03, 0.86100219E-04,
         0.14928899E-02,-0.57702442E-03, 0.10768760E-02, 0.16902430E-02,
         0.92822034E-03,-0.15893388E-02,-0.57303574E-03, 0.80200465E-03,
        -0.15518327E-02, 0.35395520E-02,-0.70469576E-03,-0.62745641E-03,
         0.60718058E-03,-0.10762907E-02,-0.20111187E-02, 0.80697169E-03,
        -0.39016426E-03, 0.45349184E-03,-0.75463683E-03, 0.50266035E-03,
        -0.14168903E-02,-0.52135278E-04,-0.39802220E-04, 0.56789856E-03,
        -0.52788226E-04, 0.52893360E-04, 0.15308024E-03,-0.11643183E-03,
         0.72487188E-03, 0.22205448E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11        *x42*x52
        +coeff[  6]*x13        *x42    
        +coeff[  7]        *x33    *x53
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[  8]*x11*x22    *x41*x53
        +coeff[  9]    *x21            
        +coeff[ 10]*x11                
        +coeff[ 11]        *x31    *x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11        *x42    
        +coeff[ 14]        *x33*x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]            *x43*x51
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 17]            *x41*x53
        +coeff[ 18]*x11    *x33        
        +coeff[ 19]*x11*x21*x31*x41    
        +coeff[ 20]*x11*x21    *x42    
        +coeff[ 21]*x11*x21*x31    *x51
        +coeff[ 22]    *x24*x31        
        +coeff[ 23]    *x23*x31    *x51
        +coeff[ 24]        *x33*x41*x51
        +coeff[ 25]            *x44*x51
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 26]    *x22        *x53
        +coeff[ 27]        *x32    *x53
        +coeff[ 28]*x11    *x32*x42    
        +coeff[ 29]*x11*x21    *x42*x51
        +coeff[ 30]*x11    *x31*x42*x51
        +coeff[ 31]*x11*x21    *x41*x52
        +coeff[ 32]*x12*x21    *x42    
        +coeff[ 33]*x12    *x31*x41*x51
        +coeff[ 34]    *x23*x33        
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 35]    *x22    *x43*x51
        +coeff[ 36]    *x23    *x41*x52
        +coeff[ 37]        *x31*x43*x52
        +coeff[ 38]            *x43*x53
        +coeff[ 39]*x11*x23    *x42    
        +coeff[ 40]*x11*x21*x32*x42    
        +coeff[ 41]*x11*x22*x31*x41*x51
        +coeff[ 42]*x11*x21    *x43*x51
        +coeff[ 43]*x11*x21    *x41*x53
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 44]*x12        *x43*x51
        +coeff[ 45]    *x21*x34*x42    
        +coeff[ 46]    *x21*x32*x44    
        +coeff[ 47]*x13*x21*x31    *x51
        +coeff[ 48]    *x24*x32    *x51
        +coeff[ 49]    *x23*x33    *x51
        +coeff[ 50]*x13*x21    *x41*x51
        +coeff[ 51]    *x21*x32*x43*x51
        +coeff[ 52]*x13            *x53
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 53]    *x23    *x41*x53
        +coeff[ 54]    *x21*x31*x42*x53
        +coeff[ 55]    *x21*x32    *x54
        +coeff[ 56]    *x21*x31*x41*x54
        +coeff[ 57]*x11*x24*x31*x41    
        +coeff[ 58]*x11*x23*x32*x41    
        +coeff[ 59]*x11*x22*x33*x41    
        +coeff[ 60]*x11    *x34*x42    
        +coeff[ 61]*x11*x22*x31*x43    
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 62]*x11*x21*x32*x43    
        +coeff[ 63]*x11*x23    *x42*x51
        +coeff[ 64]*x11*x22*x31*x42*x51
        +coeff[ 65]*x11    *x33*x42*x51
        +coeff[ 66]*x11    *x33*x41*x52
        +coeff[ 67]*x11*x21    *x43*x52
        +coeff[ 68]*x11    *x31*x42*x53
        +coeff[ 69]*x11*x21    *x41*x54
        +coeff[ 70]*x12*x21    *x42*x52
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 71]*x12    *x31*x42*x52
        +coeff[ 72]*x13*x23    *x41    
        +coeff[ 73]*x13*x22*x31*x41    
        +coeff[ 74]*x13    *x32*x42    
        +coeff[ 75]    *x23*x33*x42    
        +coeff[ 76]*x13*x23        *x51
        +coeff[ 77]*x13    *x33    *x51
        +coeff[ 78]    *x23*x31*x43*x51
        +coeff[ 79]*x13    *x32    *x52
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 80]        *x31*x43*x54
        +coeff[ 81]        *x31        
        +coeff[ 82]    *x21        *x51
        +coeff[ 83]            *x41*x51
        +coeff[ 84]                *x52
        +coeff[ 85]*x11*x21            
        +coeff[ 86]*x11            *x51
        +coeff[ 87]    *x23            
        +coeff[ 88]    *x22*x31        
    ;
    v_l_e_q1en_2_100                              =v_l_e_q1en_2_100                              
        +coeff[ 89]    *x21*x32        
        ;

    return v_l_e_q1en_2_100                              ;
}
