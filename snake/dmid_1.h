float x_e_dmid_1_400                              (float *x,int m){
    int ncoeff= 24;
    float avdat= -0.4402281E+01;
    float xmin[10]={
        -0.39990E-02,-0.59985E-01,-0.79951E-01,-0.40009E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59941E-01, 0.79995E-01, 0.39939E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.86180936E-03, 0.14680205E+00, 0.71226116E-02, 0.33681181E-05,
        -0.51695751E-02, 0.52414165E-03, 0.12164271E-02, 0.40234128E-03,
        -0.78704422E-02,-0.58006463E-02,-0.51712450E-04,-0.56155186E-04,
        -0.93414608E-04, 0.43012387E-05,-0.19432639E-02,-0.58065960E-02,
        -0.42131953E-02, 0.32688843E-03,-0.16030586E-03,-0.30203979E-02,
        -0.45350788E-03,-0.91886177E-04, 0.15799858E-03,-0.22198132E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_dmid_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]                *x51
        +coeff[  6]    *x22            
        +coeff[  7]*x11            *x51
    ;
    v_x_e_dmid_1_400                              =v_x_e_dmid_1_400                              
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]*x12*x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]    *x21*x31*x41*x52
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_x_e_dmid_1_400                              =v_x_e_dmid_1_400                              
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x21        *x54
        +coeff[ 21]*x11*x21            
        +coeff[ 22]        *x31*x41    
        +coeff[ 23]*x11*x22            
        ;

    return v_x_e_dmid_1_400                              ;
}
float t_e_dmid_1_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2746047E+01;
    float xmin[10]={
        -0.39990E-02,-0.59985E-01,-0.79951E-01,-0.40009E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59941E-01, 0.79995E-01, 0.39939E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18171513E-01,-0.99471211E-02, 0.15007430E+00, 0.30032391E-01,
         0.76816075E-01,-0.13336575E-01, 0.21554658E-01, 0.56041767E-05,
        -0.86186510E-02, 0.10490974E-01, 0.71341973E-02,-0.56957016E-02,
        -0.15034231E-02,-0.16991274E-01,-0.54727341E-04,-0.13798869E-01,
        -0.10763389E-01,-0.90280204E-03,-0.16096629E-02,-0.67653414E-02,
        -0.19478692E-01,-0.20382262E-02, 0.22202067E-02,-0.28055493E-03,
        -0.16806542E-02,-0.28691604E-03,-0.11492817E-01,-0.78752832E-02,
        -0.46571251E-02, 0.64793408E-05, 0.49999246E-04,-0.24795994E-04,
        -0.26439843E-03, 0.12577722E-03,-0.39316167E-03,-0.45353323E-02,
        -0.15452623E-03, 0.11398273E-03, 0.10245154E-03, 0.28606139E-04,
        -0.28220451E-03,-0.50949855E-02,-0.84737604E-02, 0.27175576E-04,
        -0.61617597E-04, 0.41962587E-04,-0.18868996E-03,-0.11820772E-01,
        -0.45744283E-03, 0.88596016E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_dmid_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]            *x42    
        +coeff[  6]    *x21        *x51
        +coeff[  7]*x13*x21            
    ;
    v_t_e_dmid_1_400                              =v_t_e_dmid_1_400                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x23            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]*x12*x21*x31*x41    
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_dmid_1_400                              =v_t_e_dmid_1_400                              
        +coeff[ 17]                *x52
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x21*x31*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x21        *x52
    ;
    v_t_e_dmid_1_400                              =v_t_e_dmid_1_400                              
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x23*x32        
        +coeff[ 29]    *x22*x33*x41    
        +coeff[ 30]        *x33*x43    
        +coeff[ 31]    *x22*x32    *x52
        +coeff[ 32]    *x22*x31*x41*x52
        +coeff[ 33]*x12                
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_dmid_1_400                              =v_t_e_dmid_1_400                              
        +coeff[ 35]    *x22*x32        
        +coeff[ 36]    *x21    *x42*x51
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]*x11*x21        *x51
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]    *x22        *x52
        +coeff[ 41]    *x21*x33*x41    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]            *x41    
    ;
    v_t_e_dmid_1_400                              =v_t_e_dmid_1_400                              
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]    *x23    *x41    
        +coeff[ 46]    *x21*x31*x41*x51
        +coeff[ 47]    *x21*x32*x42    
        +coeff[ 48]    *x23    *x42*x51
        +coeff[ 49]        *x31        
        ;

    return v_t_e_dmid_1_400                              ;
}
float y_e_dmid_1_400                              (float *x,int m){
    int ncoeff= 72;
    float avdat= -0.8697484E-03;
    float xmin[10]={
        -0.39990E-02,-0.59985E-01,-0.79951E-01,-0.40009E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59941E-01, 0.79995E-01, 0.39939E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 73]={
         0.73883909E-03, 0.16360340E+00, 0.39729077E-01, 0.19113157E-01,
         0.10517292E-01,-0.44183913E-02,-0.97179683E-02,-0.47375904E-02,
        -0.19414411E-02,-0.78800181E-02,-0.53366101E-02,-0.11300540E-02,
        -0.79941482E-03,-0.69704089E-04,-0.12969329E-03,-0.99804706E-03,
        -0.39860923E-02,-0.12356566E-02, 0.38960623E-03, 0.73653943E-03,
         0.16430787E-03, 0.68951456E-04,-0.78919821E-03,-0.32071190E-03,
        -0.31375850E-03, 0.15012181E-03,-0.17215220E-03,-0.41216491E-02,
        -0.87610621E-03,-0.41562608E-02,-0.12320630E-01,-0.18339697E-02,
        -0.14090784E-01,-0.22361216E-02,-0.55567396E-03,-0.14175300E-04,
         0.43362516E-03, 0.17560823E-03,-0.59481936E-04,-0.75176773E-04,
        -0.92270307E-03,-0.31965070E-02,-0.34003353E-02,-0.76014404E-02,
        -0.17359608E-02, 0.25887144E-03, 0.57748024E-03, 0.38792845E-03,
         0.95220277E-03, 0.86143496E-04, 0.68702712E-03, 0.16163349E-03,
        -0.19492937E-02, 0.15047284E-03,-0.11896715E-03, 0.41540181E-04,
         0.29115131E-05, 0.74194745E-05,-0.16528273E-04,-0.87400040E-05,
        -0.10161516E-04, 0.33584598E-04, 0.19116214E-04, 0.29655971E-03,
         0.58887988E-04, 0.40636273E-03, 0.46808753E-04,-0.47494137E-04,
         0.15873084E-03, 0.59476242E-04, 0.78798439E-04, 0.47924568E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dmid_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21    *x41    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[  8]    *x21*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x31*x44    
        +coeff[ 15]    *x22*x33        
        +coeff[ 16]            *x43    
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]*x11        *x41    
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 26]    *x21*x31    *x51
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x24*x31        
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x31*x44    
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 35]                *x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x43    
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31*x42    
        +coeff[ 43]    *x22*x33*x42    
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 44]    *x22*x34*x41    
        +coeff[ 45]    *x21    *x43    
        +coeff[ 46]    *x21*x31*x42    
        +coeff[ 47]    *x21*x32*x41    
        +coeff[ 48]        *x31*x42*x51
        +coeff[ 49]    *x21*x33        
        +coeff[ 50]        *x32*x41*x51
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x22    *x43    
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 53]    *x23    *x41*x51
        +coeff[ 54]*x11*x23*x31        
        +coeff[ 55]*x11*x23*x31*x42    
        +coeff[ 56]    *x21            
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]*x12    *x31        
        +coeff[ 61]    *x21    *x41*x52
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 62]    *x21*x31    *x52
        +coeff[ 63]*x11*x21    *x43    
        +coeff[ 64]            *x41*x53
        +coeff[ 65]*x11*x21*x31*x42    
        +coeff[ 66]        *x31    *x53
        +coeff[ 67]*x11*x23    *x41    
        +coeff[ 68]*x11*x21*x32*x41    
        +coeff[ 69]    *x23*x31    *x51
        +coeff[ 70]    *x22    *x41*x52
    ;
    v_y_e_dmid_1_400                              =v_y_e_dmid_1_400                              
        +coeff[ 71]    *x22*x31    *x52
        ;

    return v_y_e_dmid_1_400                              ;
}
float p_e_dmid_1_400                              (float *x,int m){
    int ncoeff= 34;
    float avdat=  0.3803684E-05;
    float xmin[10]={
        -0.39990E-02,-0.59985E-01,-0.79951E-01,-0.40009E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59941E-01, 0.79995E-01, 0.39939E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.13195395E-05,-0.62044781E-01,-0.22050919E-01,-0.11058253E-01,
        -0.29617690E-01, 0.86743813E-02, 0.13968676E-01,-0.11798187E-01,
         0.16201919E-03,-0.77032582E-02, 0.11396430E-02,-0.29761165E-02,
         0.13895631E-04, 0.51459373E-03,-0.17802009E-02,-0.65214215E-02,
        -0.34456905E-02, 0.66179235E-03,-0.59928716E-03,-0.82994770E-03,
         0.86102518E-04, 0.60188829E-03, 0.16451039E-02, 0.18396105E-02,
         0.78845304E-03,-0.55924396E-04,-0.97602233E-03,-0.42928141E-02,
         0.43507852E-03, 0.73022203E-03, 0.74879592E-03, 0.50252263E-03,
        -0.37842564E-03, 0.35159479E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x35 = x34*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dmid_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dmid_1_400                              =v_p_e_dmid_1_400                              
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x23*x31    *x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dmid_1_400                              =v_p_e_dmid_1_400                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x35        
        +coeff[ 21]        *x34*x41    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]        *x32*x43    
        +coeff[ 24]        *x31*x44    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_p_e_dmid_1_400                              =v_p_e_dmid_1_400                              
        +coeff[ 26]        *x33        
        +coeff[ 27]        *x32*x41    
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x23    *x41    
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]    *x22    *x41*x51
        +coeff[ 33]    *x25*x31        
        ;

    return v_p_e_dmid_1_400                              ;
}
float l_e_dmid_1_400                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.5046447E-02;
    float xmin[10]={
        -0.39990E-02,-0.59985E-01,-0.79951E-01,-0.40009E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59941E-01, 0.79995E-01, 0.39939E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.50576800E-02,-0.36613345E-01, 0.12694561E-02,-0.11216428E-01,
        -0.11430929E-01,-0.17675514E-02, 0.12048008E-02, 0.26343674E-04,
         0.19152305E-03, 0.89937657E-05, 0.17560880E-04,-0.40353565E-02,
        -0.11834988E-01, 0.75392710E-03,-0.78878709E-03,-0.40850951E-04,
         0.13872286E-02,-0.99183861E-04, 0.17395455E-02, 0.12484313E-02,
         0.54039917E-03, 0.80724736E-03, 0.92662714E-03, 0.44411074E-03,
        -0.98369355E-05, 0.43335320E-04, 0.10734513E-03,-0.22349348E-04,
         0.73292766E-04, 0.71514776E-03, 0.53807100E-04, 0.29803198E-03,
         0.14481093E-02, 0.10390020E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dmid_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]            *x42    
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dmid_1_400                              =v_l_e_dmid_1_400                              
        +coeff[  8]    *x24*x32        
        +coeff[  9]    *x22*x34        
        +coeff[ 10]        *x34*x42    
        +coeff[ 11]        *x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]*x11*x21            
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]                *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_dmid_1_400                              =v_l_e_dmid_1_400                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]    *x21    *x42    
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]    *x23*x32        
        +coeff[ 24]    *x21*x34        
        +coeff[ 25]    *x23*x34        
    ;
    v_l_e_dmid_1_400                              =v_l_e_dmid_1_400                              
        +coeff[ 26]    *x21        *x52
        +coeff[ 27]*x12                
        +coeff[ 28]    *x23            
        +coeff[ 29]    *x21*x32        
        +coeff[ 30]*x11*x22            
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]    *x23*x31*x41    
        +coeff[ 33]    *x23    *x42    
        ;

    return v_l_e_dmid_1_400                              ;
}
