float x_e_dmid_4_400                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.8378930E+00;
    float xmin[10]={
        -0.39990E-02,-0.58509E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.59348E-01, 0.79934E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.33623877E-02,-0.11781833E-01, 0.44940863E-01, 0.28215465E+00,
         0.23020718E-01, 0.17028006E-01,-0.16046935E-02,-0.12831707E-02,
        -0.36290307E-02,-0.19886337E-01,-0.14332587E-01,-0.63529653E-04,
         0.41253606E-04,-0.38161941E-02, 0.50871109E-03,-0.30190221E-02,
         0.60814439E-03,-0.73457779E-02,-0.92195283E-03, 0.76727761E-03,
        -0.83087903E-03, 0.12300224E-02,-0.43765158E-05, 0.11558412E-03,
         0.52074238E-03,-0.52350463E-03,-0.10703078E-01,-0.79331109E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dmid_4_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_x_e_dmid_4_400                              =v_x_e_dmid_4_400                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]    *x23            
    ;
    v_x_e_dmid_4_400                              =v_x_e_dmid_4_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21    *x41    
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]*x12*x21*x31        
        +coeff[ 23]            *x41    
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]*x11*x22            
    ;
    v_x_e_dmid_4_400                              =v_x_e_dmid_4_400                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        ;

    return v_x_e_dmid_4_400                              ;
}
float t_e_dmid_4_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.8412728E+00;
    float xmin[10]={
        -0.39990E-02,-0.58509E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.59348E-01, 0.79934E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.10090864E-02,-0.69631569E-01, 0.35961490E-01, 0.52593444E-02,
         0.31870079E-05, 0.18011201E-02,-0.12206440E-02,-0.71013346E-03,
        -0.14770791E-02,-0.28440118E-03,-0.12401912E-02,-0.26180371E-03,
        -0.76338724E-03, 0.12424785E-02, 0.13443544E-02,-0.68598051E-05,
        -0.10822280E-03,-0.92899354E-05, 0.10332211E-02, 0.53057884E-05,
        -0.45188848E-03, 0.19813128E-03, 0.79622283E-03, 0.17780370E-03,
         0.26675611E-03, 0.62152241E-04,-0.13798992E-03, 0.28820985E-02,
         0.18919250E-02, 0.27320935E-04,-0.64321743E-04,-0.95299154E-04,
         0.39783030E-04, 0.51059447E-04,-0.19587483E-03,-0.51919813E-03,
         0.25273992E-02, 0.18819420E-02, 0.11189944E-04,-0.46032801E-04,
         0.13963801E-03, 0.89186680E-04, 0.22739314E-04,-0.13202766E-03,
        -0.22364732E-03,-0.32652789E-03,-0.40649353E-04,-0.21506933E-05,
         0.12402829E-02, 0.55348282E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dmid_4_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x21        *x51
        +coeff[  7]                *x52
    ;
    v_t_e_dmid_4_400                              =v_t_e_dmid_4_400                              
        +coeff[  8]            *x42    
        +coeff[  9]*x11*x21            
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11            *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]*x12    *x32        
        +coeff[ 16]    *x22*x32        
    ;
    v_t_e_dmid_4_400                              =v_t_e_dmid_4_400                              
        +coeff[ 17]*x12*x21*x32        
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]*x12                
        +coeff[ 20]        *x32        
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x21*x32        
        +coeff[ 23]    *x22        *x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]            *x41    
    ;
    v_t_e_dmid_4_400                              =v_t_e_dmid_4_400                              
        +coeff[ 26]            *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]        *x31        
        +coeff[ 30]    *x21*x31        
        +coeff[ 31]    *x21    *x41    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_dmid_4_400                              =v_t_e_dmid_4_400                              
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x23*x32    *x51
        +coeff[ 39]    *x23    *x42*x51
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]*x11        *x42    
        +coeff[ 42]                *x53
        +coeff[ 43]    *x22*x31*x41    
    ;
    v_t_e_dmid_4_400                              =v_t_e_dmid_4_400                              
        +coeff[ 44]    *x21*x32    *x51
        +coeff[ 45]    *x21    *x42*x51
        +coeff[ 46]    *x22        *x52
        +coeff[ 47]*x13    *x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]*x11    *x32        
        ;

    return v_t_e_dmid_4_400                              ;
}
float y_e_dmid_4_400                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6074443E-03;
    float xmin[10]={
        -0.39990E-02,-0.58509E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.59348E-01, 0.79934E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.22521303E-02, 0.13860205E+00,-0.52780129E-01,-0.51119667E-01,
        -0.20384884E-01, 0.40971830E-01, 0.25274070E-01,-0.24184156E-01,
        -0.13011783E-01, 0.10509671E-02,-0.84864406E-03, 0.18491796E-02,
        -0.41551939E-02,-0.23919253E-02, 0.74443291E-03,-0.14850194E-01,
        -0.10187463E-01,-0.26460676E-02,-0.19717729E-02, 0.80388167E-03,
        -0.15514405E-02,-0.24697264E-02, 0.51041407E-03,-0.72150081E-02,
        -0.25115439E-02, 0.12209346E-02, 0.60212886E-03, 0.35913372E-02,
        -0.19159346E-02, 0.65221696E-03, 0.11330183E-03, 0.17412226E-02,
        -0.15949588E-04,-0.85734326E-03,-0.37495127E-04, 0.48400488E-03,
         0.23942141E-03, 0.84397820E-04, 0.36829368E-02, 0.65517179E-02,
         0.43536560E-02, 0.94796618E-03,-0.10405115E-01,-0.16141968E-01,
        -0.10986950E-01,-0.16964948E-02, 0.19664837E-02, 0.53893145E-04,
        -0.78566103E-04, 0.18779470E-02, 0.14563077E-02, 0.35708179E-03,
         0.39799185E-03, 0.23488460E-03,-0.23754823E-02,-0.28594917E-04,
        -0.22411719E-03,-0.27652868E-03,-0.71638606E-04,-0.89718073E-04,
         0.41474454E-04, 0.59993844E-03, 0.72902802E-03, 0.21810198E-03,
        -0.15639170E-03,-0.32335876E-02,-0.40250183E-02,-0.23542240E-02,
        -0.75579929E-03, 0.12287764E-03, 0.32377598E-03,-0.42241580E-04,
        -0.70309208E-04,-0.27463864E-04,-0.43225828E-04,-0.25746680E-04,
         0.37121594E-04,-0.43580494E-04,-0.27403030E-04, 0.10861165E-05,
         0.59014751E-03, 0.15540794E-03, 0.91002323E-03, 0.34685858E-03,
        -0.11966878E-03, 0.14105286E-03, 0.16190857E-03, 0.43693589E-03,
         0.11716839E-03,-0.77710167E-04,-0.74516549E-02,-0.70730988E-02,
        -0.28893829E-02,-0.27529551E-02,-0.44479406E-04, 0.22292582E-03,
         0.12188629E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dmid_4_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21            
        +coeff[ 10]                *x51
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]            *x43*x51
        +coeff[ 20]            *x45    
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]    *x22            
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x23    *x41    
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]        *x31*x41    
        +coeff[ 30]            *x44    
        +coeff[ 31]    *x23*x31        
        +coeff[ 32]        *x34        
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11                
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 35]            *x42    
        +coeff[ 36]        *x32        
        +coeff[ 37]    *x21        *x51
        +coeff[ 38]    *x21    *x43    
        +coeff[ 39]    *x21*x31*x42    
        +coeff[ 40]    *x21*x32*x41    
        +coeff[ 41]    *x21*x33        
        +coeff[ 42]    *x22    *x43    
        +coeff[ 43]    *x22*x31*x42    
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x24*x31        
        +coeff[ 46]    *x24    *x43    
        +coeff[ 47]                *x52
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]        *x31*x42*x51
        +coeff[ 50]        *x32*x41*x51
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x21    *x41*x52
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 53]    *x21*x31    *x52
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]*x11*x21            
        +coeff[ 56]    *x21    *x42    
        +coeff[ 57]    *x21*x31*x41    
        +coeff[ 58]    *x23            
        +coeff[ 59]    *x21*x32        
        +coeff[ 60]    *x22        *x51
        +coeff[ 61]    *x22    *x42    
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 62]    *x22*x31*x41    
        +coeff[ 63]    *x22*x32        
        +coeff[ 64]*x11*x22    *x41    
        +coeff[ 65]        *x31*x44    
        +coeff[ 66]        *x32*x43    
        +coeff[ 67]        *x33*x42    
        +coeff[ 68]        *x34*x41    
        +coeff[ 69]        *x31    *x53
        +coeff[ 70]    *x23    *x41*x51
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 71]            *x43*x53
        +coeff[ 72]        *x31*x41*x51
        +coeff[ 73]        *x32    *x51
        +coeff[ 74]*x12        *x41    
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]    *x24            
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]*x11*x21    *x41*x51
        +coeff[ 79]            *x44*x51
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 80]*x11*x21    *x43    
        +coeff[ 81]            *x41*x53
        +coeff[ 82]*x11*x21*x31*x42    
        +coeff[ 83]*x11*x21*x32*x41    
        +coeff[ 84]*x11*x23*x31        
        +coeff[ 85]    *x23*x31    *x51
        +coeff[ 86]    *x22    *x41*x52
        +coeff[ 87]    *x23*x31*x42    
        +coeff[ 88]    *x22*x31    *x52
    ;
    v_y_e_dmid_4_400                              =v_y_e_dmid_4_400                              
        +coeff[ 89]    *x22    *x43*x51
        +coeff[ 90]    *x22*x31*x44    
        +coeff[ 91]    *x22*x32*x43    
        +coeff[ 92]    *x22*x33*x42    
        +coeff[ 93]    *x24    *x45    
        +coeff[ 94]            *x42*x51
        +coeff[ 95]        *x31*x43    
        +coeff[ 96]        *x32*x42    
        ;

    return v_y_e_dmid_4_400                              ;
}
float p_e_dmid_4_400                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.3490611E-04;
    float xmin[10]={
        -0.39990E-02,-0.58509E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.59348E-01, 0.79934E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.12551750E-03,-0.27702780E-01,-0.39617028E-02,-0.30244091E-02,
        -0.13472354E-01, 0.42430824E-02, 0.71399845E-02, 0.26591329E-03,
        -0.28035147E-02,-0.44127172E-02,-0.18397419E-02,-0.14626094E-03,
         0.48213077E-03,-0.98985783E-03, 0.12024419E-03,-0.32569054E-02,
        -0.17999988E-02, 0.17667815E-04,-0.13946789E-05, 0.32942753E-04,
         0.32184969E-04, 0.89384637E-04,-0.49028825E-03,-0.21618651E-02,
         0.20954954E-03, 0.95893082E-03,-0.36111387E-03, 0.59248932E-03,
        -0.68760878E-05, 0.12501821E-03, 0.10481408E-03, 0.37815382E-04,
         0.12374496E-03,-0.28640387E-03, 0.22885964E-03,-0.21282773E-03,
         0.45835459E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dmid_4_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x21            
    ;
    v_p_e_dmid_4_400                              =v_p_e_dmid_4_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]                *x51
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dmid_4_400                              =v_p_e_dmid_4_400                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33    *x52
        +coeff[ 20]        *x32*x41*x52
        +coeff[ 21]    *x22            
        +coeff[ 22]        *x33        
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_dmid_4_400                              =v_p_e_dmid_4_400                              
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x25*x31        
        +coeff[ 28]        *x31    *x54
        +coeff[ 29]        *x31*x41    
        +coeff[ 30]            *x42    
        +coeff[ 31]    *x21        *x51
        +coeff[ 32]*x11*x21*x31        
        +coeff[ 33]        *x31    *x52
        +coeff[ 34]    *x21    *x43    
    ;
    v_p_e_dmid_4_400                              =v_p_e_dmid_4_400                              
        +coeff[ 35]    *x22    *x41*x51
        +coeff[ 36]        *x34        
        ;

    return v_p_e_dmid_4_400                              ;
}
float l_e_dmid_4_400                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1023287E-01;
    float xmin[10]={
        -0.39990E-02,-0.58509E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.59348E-01, 0.79934E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.83585251E-02,-0.25425580E+00,-0.12441491E-01, 0.96977865E-02,
        -0.20177623E-01,-0.14106405E-01,-0.10810776E-01,-0.95767025E-02,
         0.14356797E-02, 0.14057752E-01, 0.10514588E-01, 0.86625827E-04,
         0.39541672E-04, 0.33452273E-02,-0.24569125E-03,-0.47279904E-02,
        -0.63796842E-03, 0.56044059E-02,-0.15114872E-02, 0.39999542E-03,
        -0.58389094E-03,-0.16178681E-03, 0.13817573E-04, 0.19774616E-02,
         0.63325779E-03,-0.42395903E-04, 0.21480367E-03,-0.35887709E-03,
         0.85976435E-03, 0.10280805E-02, 0.43328956E-03, 0.92451693E-02,
         0.67453063E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dmid_4_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dmid_4_400                              =v_l_e_dmid_4_400                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]    *x21*x34        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_dmid_4_400                              =v_l_e_dmid_4_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]            *x41    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]    *x23            
        +coeff[ 22]        *x33        
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]    *x23*x31        
    ;
    v_l_e_dmid_4_400                              =v_l_e_dmid_4_400                              
        +coeff[ 26]        *x31        
        +coeff[ 27]    *x21*x31        
        +coeff[ 28]        *x32    *x51
        +coeff[ 29]            *x42*x51
        +coeff[ 30]*x11*x22            
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        ;

    return v_l_e_dmid_4_400                              ;
}
