float x_e_dent_1200                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5099728E+01;
    float xmin[10]={
        -0.39996E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
        -0.38238679E-03, 0.43414235E-02,-0.27360766E-04,-0.12127826E+00,
        -0.58270213E-02, 0.35255654E-02, 0.31969806E-02, 0.24863635E-02,
         0.65192167E-03,-0.34037698E-03,-0.28882580E-03, 0.91495214E-03,
         0.12050014E-02, 0.38280679E-03, 0.36043461E-03,-0.14934107E-03,
        -0.17516303E-03, 0.24446410E-02, 0.19947202E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_1200                                =v_x_e_dent_1200                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_x_e_dent_1200                                =v_x_e_dent_1200                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_1200                                ;
}
float t_e_dent_1200                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3649234E+01;
    float xmin[10]={
        -0.39996E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.73860712E-01,-0.35547964E-01, 0.77074248E+00, 0.21458717E+00,
         0.37309475E-01, 0.59188432E-02,-0.18297903E-01, 0.29732227E-01,
         0.46187703E-01, 0.17592592E-01, 0.18885631E-01,-0.77335872E-02,
        -0.13573620E-01,-0.12728484E-01,-0.94407648E-02, 0.24662165E-02,
        -0.67072785E-02,-0.21551592E-01, 0.66571124E-02, 0.94549978E-04,
        -0.75403252E-03, 0.46817176E-02,-0.29384869E-02,-0.23349507E-01,
        -0.31259820E-01,-0.24620539E-01, 0.85048654E-04,-0.26884757E-03,
         0.43860925E-03, 0.26583413E-02,-0.27914438E-03, 0.38279325E-02,
        -0.72453660E-02, 0.42033390E-03, 0.60397689E-03, 0.80338574E-03,
        -0.11711596E-02, 0.35195118E-02, 0.28791949E-02,-0.11553644E-02,
        -0.19621171E-01,-0.16596802E-01, 0.20903347E-02,-0.74291154E-03,
        -0.73216489E-03,-0.18799056E-02,-0.17243270E-02, 0.42614414E-03,
        -0.85697910E-02,-0.94326097E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_1200                                =v_t_e_dent_1200                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_1200                                =v_t_e_dent_1200                                
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]*x12*x22*x31*x41    
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_1200                                =v_t_e_dent_1200                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]*x12                
        +coeff[ 29]        *x32        
        +coeff[ 30]                *x52
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]    *x22*x32        
        +coeff[ 33]*x12*x21            
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_dent_1200                                =v_t_e_dent_1200                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]    *x21*x31*x43    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x11    *x31*x41    
    ;
    v_t_e_dent_1200                                =v_t_e_dent_1200                                
        +coeff[ 44]*x11        *x42    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_dent_1200                                ;
}
float y_e_dent_1200                                (float *x,int m){
    int ncoeff= 65;
    float avdat= -0.1177112E-03;
    float xmin[10]={
        -0.39996E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 66]={
         0.52880987E-04, 0.12913805E+00, 0.38386051E-01, 0.11850383E-01,
         0.56269267E-02, 0.40497426E-02, 0.16507056E-04, 0.36834318E-02,
        -0.26156958E-02,-0.16355704E-02,-0.26038024E-02,-0.15075495E-02,
        -0.54584967E-03,-0.66899718E-03,-0.25327054E-04,-0.85683650E-05,
        -0.14670937E-03,-0.14495158E-02,-0.50788429E-06,-0.13438880E-03,
        -0.32843024E-03,-0.23175453E-03,-0.41834268E-03, 0.38770458E-03,
         0.12894321E-03,-0.24962982E-02,-0.11691800E-02,-0.85800001E-03,
        -0.59026876E-03,-0.90783363E-03, 0.52407784E-04, 0.93627068E-05,
        -0.36595293E-03, 0.88343177E-05, 0.36462789E-04, 0.25669719E-04,
         0.28815001E-03,-0.86767053E-04, 0.18041034E-03,-0.14875594E-02,
        -0.18260853E-02, 0.13568283E-04,-0.56810072E-05,-0.92403003E-04,
        -0.41334941E-04, 0.48819042E-04, 0.39145729E-04,-0.37543217E-03,
         0.28429109E-04,-0.16164946E-02,-0.49764517E-03,-0.21805545E-05,
        -0.67404173E-04, 0.12585323E-03,-0.33010307E-04,-0.19821247E-04,
        -0.26013449E-03, 0.11357673E-03, 0.15333797E-03, 0.47349131E-04,
         0.70154048E-04,-0.36507125E-04, 0.39928920E-04, 0.18595338E-03,
         0.53184191E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x33    *x52
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 17]            *x43    
        +coeff[ 18]*x11        *x43    
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]        *x31    *x52
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]    *x22*x32*x43    
        +coeff[ 30]    *x24*x32*x41    
        +coeff[ 31]    *x24*x33        
        +coeff[ 32]    *x22*x32*x45    
        +coeff[ 33]*x11*x21    *x41    
        +coeff[ 34]*x11        *x41*x51
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x22*x32*x41    
        +coeff[ 41]            *x45*x51
        +coeff[ 42]                *x51
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]            *x41*x53
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]        *x31    *x53
        +coeff[ 49]    *x22*x31*x44    
        +coeff[ 50]    *x22*x33*x42    
        +coeff[ 51]    *x21            
        +coeff[ 52]    *x21*x31*x42    
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 53]            *x43*x51
        +coeff[ 54]    *x21*x33        
        +coeff[ 55]*x11*x22*x31        
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x41*x51
        +coeff[ 61]*x11*x23*x31        
    ;
    v_y_e_dent_1200                                =v_y_e_dent_1200                                
        +coeff[ 62]    *x23*x31    *x51
        +coeff[ 63]        *x32*x45    
        +coeff[ 64]    *x22*x32*x41*x51
        ;

    return v_y_e_dent_1200                                ;
}
float p_e_dent_1200                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.1244826E-04;
    float xmin[10]={
        -0.39996E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.20474708E-04,-0.62589064E-01,-0.53623609E-01,-0.16695902E-01,
        -0.25734454E-01, 0.68635968E-02, 0.11813010E-01,-0.17214889E-01,
         0.75886335E-03,-0.87272422E-02, 0.10726812E-02, 0.53755662E-05,
         0.78435184E-03,-0.33990487E-02,-0.21806515E-02, 0.10681325E-02,
         0.24208976E-02,-0.46737787E-04, 0.34932826E-04,-0.30922570E-03,
        -0.16946448E-02, 0.39840653E-03,-0.49176213E-03, 0.84233948E-03,
        -0.65390568E-03,-0.91546902E-03,-0.98901211E-04,-0.45097110E-03,
         0.58659480E-03,-0.51748159E-03,-0.13471470E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_1200                                =v_p_e_dent_1200                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_1200                                =v_p_e_dent_1200                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_1200                                =v_p_e_dent_1200                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11        *x41*x51
        ;

    return v_p_e_dent_1200                                ;
}
float l_e_dent_1200                                (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.5482578E-02;
    float xmin[10]={
        -0.39996E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.56246365E-02, 0.23441468E+00,-0.83207581E-02,-0.17674824E-01,
         0.11257091E-01,-0.63011064E-02, 0.14911173E-02,-0.23848354E-03,
        -0.19079854E-02,-0.61492980E-02, 0.12032413E-02,-0.54912991E-02,
        -0.47908560E-02,-0.15039319E-02, 0.80284639E-03,-0.14166428E-02,
        -0.20050236E-02,-0.14205147E-02,-0.85634622E-03, 0.21238855E-03,
         0.20812465E-03,-0.39342162E-03, 0.87483169E-03,-0.40122881E-03,
        -0.21918808E-03, 0.10866628E-02,-0.70845312E-03,-0.65368100E-03,
         0.34254065E-03, 0.67697244E-03,-0.60682362E-02,-0.77264867E-03,
        -0.49324948E-02, 0.49221318E-03,-0.10846980E-02, 0.78158075E-03,
        -0.66427991E-03, 0.66405500E-03,-0.77043351E-03, 0.43126088E-03,
        -0.61636926E-02,-0.43417374E-02,-0.48766547E-03,-0.46389294E-03,
         0.11483465E-02, 0.11077635E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_1200                                =v_l_e_dent_1200                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dent_1200                                =v_l_e_dent_1200                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]                *x51
        +coeff[ 20]        *x31        
        +coeff[ 21]        *x31*x42    
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_dent_1200                                =v_l_e_dent_1200                                
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]*x11    *x32*x41    
        +coeff[ 28]*x11        *x43    
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x21    *x44    
        +coeff[ 34]        *x33*x41*x51
    ;
    v_l_e_dent_1200                                =v_l_e_dent_1200                                
        +coeff[ 35]    *x24*x32        
        +coeff[ 36]        *x31*x44*x51
        +coeff[ 37]*x11*x23*x32        
        +coeff[ 38]*x11    *x32*x42*x51
        +coeff[ 39]*x12*x24            
        +coeff[ 40]    *x21*x34*x42    
        +coeff[ 41]    *x21*x33*x43    
        +coeff[ 42]        *x33    *x54
        +coeff[ 43]            *x43*x54
    ;
    v_l_e_dent_1200                                =v_l_e_dent_1200                                
        +coeff[ 44]*x11    *x34*x42    
        +coeff[ 45]*x11    *x33*x41*x52
        ;

    return v_l_e_dent_1200                                ;
}
float x_e_dent_1100                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5099694E+01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
        -0.36562336E-03, 0.43431246E-02,-0.12133693E+00,-0.58259191E-02,
         0.35308010E-02, 0.32006260E-02, 0.25149677E-02, 0.70231385E-03,
        -0.34953564E-03,-0.30420654E-03, 0.30817807E-03, 0.90403087E-03,
         0.11708332E-02, 0.37249841E-03,-0.15244167E-03,-0.18765897E-03,
         0.24108221E-02, 0.19298001E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_1100                                =v_x_e_dent_1100                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_1100                                =v_x_e_dent_1100                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_1100                                ;
}
float t_e_dent_1100                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3648810E+01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.73696390E-01,-0.35391774E-01, 0.77105337E+00, 0.21459806E+00,
         0.37052277E-01, 0.60353256E-02,-0.18380268E-01, 0.29441390E-01,
         0.46390381E-01, 0.17415103E-01, 0.18687315E-01,-0.77198287E-02,
        -0.13094408E-01,-0.12926596E-01,-0.99036284E-02, 0.25023934E-02,
        -0.64462894E-02,-0.28758196E-02,-0.21058535E-01, 0.75256261E-02,
         0.14450730E-03, 0.68752334E-03, 0.43212939E-02,-0.23313757E-01,
        -0.31860001E-01,-0.24563126E-01, 0.11532255E-03, 0.72301154E-05,
         0.43971874E-03, 0.23174789E-02,-0.24412193E-03, 0.36390037E-02,
        -0.67456602E-02,-0.17014517E-01,-0.89000969E-03, 0.54486963E-03,
         0.70997549E-03,-0.81641698E-03, 0.39066910E-02, 0.31259093E-02,
        -0.10849743E-02,-0.14840647E-03,-0.85133510E-02,-0.19700328E-01,
         0.28686880E-03, 0.27056152E-03,-0.38254479E-03,-0.77911100E-03,
        -0.61744830E-03, 0.10937836E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_1100                                =v_t_e_dent_1100                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_1100                                =v_t_e_dent_1100                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_1100                                =v_t_e_dent_1100                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]*x12                
        +coeff[ 29]        *x32        
        +coeff[ 30]                *x52
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]    *x22*x32        
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]*x11        *x42    
    ;
    v_t_e_dent_1100                                =v_t_e_dent_1100                                
        +coeff[ 35]*x11*x21        *x51
        +coeff[ 36]        *x32    *x51
        +coeff[ 37]*x11*x23            
        +coeff[ 38]    *x21*x31*x41*x51
        +coeff[ 39]    *x21    *x42*x51
        +coeff[ 40]    *x22        *x52
        +coeff[ 41]*x13    *x31*x41    
        +coeff[ 42]    *x21*x33*x41    
        +coeff[ 43]    *x21*x32*x42    
    ;
    v_t_e_dent_1100                                =v_t_e_dent_1100                                
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]*x12*x21            
        +coeff[ 46]*x11    *x32        
        +coeff[ 47]*x11    *x31*x41    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_dent_1100                                ;
}
float y_e_dent_1100                                (float *x,int m){
    int ncoeff= 67;
    float avdat=  0.1200311E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 68]={
        -0.19916172E-03, 0.12903745E+00, 0.38328599E-01, 0.11836409E-01,
         0.56340746E-02, 0.40580002E-02, 0.19984811E-04, 0.36896523E-02,
        -0.26628424E-02,-0.16228609E-02,-0.25777495E-02,-0.15604571E-02,
        -0.54464256E-03,-0.67186460E-03, 0.36187081E-04, 0.99084464E-05,
        -0.15216067E-02,-0.22653631E-03,-0.13044478E-03,-0.14427112E-03,
        -0.32299588E-03,-0.42947897E-03, 0.39112143E-03, 0.12497771E-04,
         0.12712330E-03,-0.23324233E-02,-0.11806350E-02,-0.86835591E-03,
        -0.85745816E-03,-0.20024912E-02,-0.23810295E-04,-0.96921989E-03,
         0.90199628E-05,-0.19019007E-03, 0.24207584E-04, 0.44110951E-04,
         0.39034301E-04, 0.29231200E-04, 0.30806140E-03, 0.25865904E-04,
         0.20134915E-03,-0.12092674E-02,-0.15603240E-04,-0.15181698E-02,
         0.30572712E-05,-0.19699263E-02,-0.69086454E-05, 0.16116390E-03,
        -0.96139425E-04,-0.50726929E-04, 0.43859898E-04,-0.38976318E-03,
         0.67404842E-04,-0.25922477E-05,-0.67841080E-04,-0.33736978E-04,
        -0.21525217E-04, 0.10428687E-03, 0.33118369E-04, 0.14390280E-03,
         0.26820548E-04, 0.59142436E-04,-0.34799334E-04, 0.28550008E-04,
        -0.10213442E-03,-0.74699979E-04,-0.80799451E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x33    *x52
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]    *x22*x32*x43    
        +coeff[ 30]        *x34*x43    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]    *x24*x32*x41    
        +coeff[ 33]    *x22*x34*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 35]    *x22*x32*x45    
        +coeff[ 36]*x11        *x41*x51
        +coeff[ 37]*x11    *x31    *x51
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x22    *x41*x51
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]        *x33*x42    
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 44]            *x45*x51
        +coeff[ 45]    *x22*x31*x44    
        +coeff[ 46]                *x51
        +coeff[ 47]            *x43*x51
        +coeff[ 48]    *x21*x32*x41    
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]    *x22*x33        
        +coeff[ 52]    *x23    *x41*x51
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 53]    *x21            
        +coeff[ 54]    *x21*x31*x42    
        +coeff[ 55]    *x21*x33        
        +coeff[ 56]*x11*x22*x31        
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]            *x41*x53
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]        *x31    *x53
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_dent_1100                                =v_y_e_dent_1100                                
        +coeff[ 62]*x11*x23*x31        
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]    *x22    *x43*x51
        +coeff[ 65]    *x22*x31*x42*x51
        +coeff[ 66]    *x24    *x41*x51
        ;

    return v_y_e_dent_1100                                ;
}
float p_e_dent_1100                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3333024E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.36319462E-03,-0.62572293E-01,-0.53672146E-01,-0.16694158E-01,
        -0.25720567E-01, 0.68705468E-02, 0.11812772E-01,-0.17292930E-01,
         0.78061561E-03,-0.87968074E-02, 0.10684306E-02,-0.56648146E-05,
         0.78735128E-03,-0.33900933E-02,-0.21757332E-02, 0.10584525E-02,
         0.25093199E-02,-0.70285241E-04, 0.67453657E-04,-0.31464951E-03,
        -0.16863837E-02, 0.40298607E-03,-0.51160477E-03, 0.81106037E-03,
        -0.66301884E-03,-0.90930052E-03, 0.25856692E-04,-0.18401821E-04,
         0.56869764E-03,-0.45240682E-03,-0.51718549E-03,-0.13422818E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_1100                                =v_p_e_dent_1100                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_1100                                =v_p_e_dent_1100                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_1100                                =v_p_e_dent_1100                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        +coeff[ 31]*x11        *x41*x51
        ;

    return v_p_e_dent_1100                                ;
}
float l_e_dent_1100                                (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.5550732E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.55060452E-02, 0.23486800E+00,-0.85542072E-02,-0.17325865E-01,
         0.11357435E-01,-0.59515540E-02, 0.30385188E-02, 0.11137818E-05,
        -0.15819003E-02,-0.58458713E-02, 0.13664106E-02,-0.44103130E-02,
        -0.53537497E-02, 0.26732570E-03, 0.36526671E-04, 0.67973015E-03,
        -0.21830772E-02,-0.30643835E-02,-0.11634288E-02, 0.68371894E-03,
        -0.58944093E-03,-0.12945278E-03, 0.38716404E-03, 0.20492430E-03,
         0.51186437E-03,-0.58812262E-02,-0.16062991E-02,-0.38069431E-02,
        -0.18009875E-02, 0.42880094E-03,-0.34234682E-03,-0.29725824E-02,
        -0.38000068E-03,-0.48267949E-03,-0.63858286E-03,-0.49313752E-03,
         0.97199512E-03,-0.39050977E-02,-0.25915848E-02, 0.20847963E-02,
        -0.51652954E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_1100                                =v_l_e_dent_1100                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dent_1100                                =v_l_e_dent_1100                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x12                
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x13                
        +coeff[ 24]*x11    *x32    *x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dent_1100                                =v_l_e_dent_1100                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x21*x31*x42*x51
        +coeff[ 29]*x13    *x32        
        +coeff[ 30]    *x23*x33        
        +coeff[ 31]    *x24*x31*x41    
        +coeff[ 32]*x13        *x42    
        +coeff[ 33]*x11*x24*x31        
        +coeff[ 34]*x11*x23*x32        
    ;
    v_l_e_dent_1100                                =v_l_e_dent_1100                                
        +coeff[ 35]*x11        *x43*x52
        +coeff[ 36]*x12*x21*x31*x41*x51
        +coeff[ 37]    *x23*x32*x42    
        +coeff[ 38]    *x23    *x44    
        +coeff[ 39]    *x21*x33*x42*x51
        +coeff[ 40]*x13            *x53
        ;

    return v_l_e_dent_1100                                ;
}
float x_e_dent_1000                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5100174E+01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
         0.98921766E-04, 0.43422617E-02,-0.12133852E+00,-0.58150482E-02,
         0.35300555E-02, 0.31894178E-02, 0.24823025E-02, 0.69985032E-03,
        -0.35095910E-03,-0.29535967E-03, 0.31242205E-03, 0.89548528E-03,
         0.11809032E-02, 0.38766622E-03,-0.15002863E-03,-0.16988098E-03,
         0.24100265E-02, 0.19762660E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_1000                                =v_x_e_dent_1000                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_1000                                =v_x_e_dent_1000                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_1000                                ;
}
float t_e_dent_1000                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3651985E+01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.76733470E-01,-0.35516512E-01, 0.77115667E+00, 0.21460415E+00,
         0.37400257E-01, 0.60269763E-02,-0.18234551E-01, 0.29291827E-01,
         0.46611473E-01, 0.17382709E-01, 0.18522007E-01,-0.76660071E-02,
        -0.13674283E-01,-0.12805757E-01,-0.10061388E-01, 0.25238947E-02,
        -0.65026693E-02,-0.29718960E-02,-0.20895209E-01, 0.67452472E-02,
        -0.23106451E-04, 0.12119185E-02, 0.44420105E-02,-0.23426544E-01,
        -0.31460579E-01,-0.24781898E-01, 0.10759593E-03, 0.23342764E-02,
        -0.31800725E-03, 0.37085419E-02,-0.67017884E-02,-0.16680267E-01,
         0.42454750E-03, 0.56633912E-03, 0.75402512E-03,-0.11633547E-02,
         0.35933352E-02, 0.30849746E-02,-0.10143860E-02,-0.77392384E-02,
        -0.18902678E-01, 0.68060134E-03, 0.15359661E-02, 0.20585468E-03,
        -0.81754028E-03,-0.72199001E-03,-0.81229024E-03,-0.43951924E-03,
        -0.59549973E-04,-0.92446411E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_dent_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_1000                                =v_t_e_dent_1000                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_1000                                =v_t_e_dent_1000                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_1000                                =v_t_e_dent_1000                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x32        
        +coeff[ 28]                *x52
        +coeff[ 29]        *x31*x41*x51
        +coeff[ 30]    *x22*x32        
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x12                
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_dent_1000                                =v_t_e_dent_1000                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21*x33*x41    
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]*x12*x21*x32    *x51
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_dent_1000                                =v_t_e_dent_1000                                
        +coeff[ 44]*x11    *x31*x41    
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]*x11    *x32    *x52
        +coeff[ 48]*x11    *x31        
        +coeff[ 49]*x11*x21*x31        
        ;

    return v_t_e_dent_1000                                ;
}
float y_e_dent_1000                                (float *x,int m){
    int ncoeff= 62;
    float avdat=  0.1617147E-04;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 63]={
         0.98110731E-05, 0.12885520E+00, 0.38258426E-01, 0.11833785E-01,
         0.56364876E-02, 0.40596602E-02, 0.13808749E-04, 0.37008647E-02,
        -0.26370657E-02,-0.16230221E-02,-0.25825975E-02,-0.15641841E-02,
        -0.55482110E-03,-0.67129533E-03, 0.68533232E-05,-0.26234400E-06,
        -0.14952131E-02,-0.22651351E-03,-0.13516522E-03,-0.14722023E-03,
        -0.32096510E-03,-0.42378248E-03, 0.37818617E-03, 0.10234254E-04,
        -0.23592918E-02,-0.11833198E-02,-0.86327799E-03,-0.69093803E-03,
        -0.16721137E-02,-0.84891700E-03, 0.74376308E-04,-0.15550105E-03,
         0.22146962E-04,-0.14279834E-03, 0.37734830E-04, 0.26682805E-04,
         0.29287324E-03, 0.12289430E-03,-0.30551048E-04, 0.19931578E-03,
        -0.13554186E-02,-0.16061515E-02,-0.39208989E-03,-0.52342722E-06,
        -0.18701911E-02, 0.15735834E-03,-0.96880503E-04,-0.43777778E-04,
         0.46924753E-04, 0.38918275E-04, 0.83415332E-04, 0.22775228E-05,
        -0.63525345E-04,-0.31237891E-04,-0.15987533E-04, 0.10992427E-03,
         0.14943944E-03, 0.25045470E-04, 0.45674391E-04,-0.37182883E-04,
         0.26856595E-04,-0.67804700E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x33    *x52
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[ 26]    *x24*x31        
        +coeff[ 27]    *x22    *x45    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]    *x22*x33*x42    
        +coeff[ 30]    *x24*x32*x41    
        +coeff[ 31]    *x22*x34*x41    
        +coeff[ 32]    *x24*x33        
        +coeff[ 33]    *x22*x32*x45    
        +coeff[ 34]*x11        *x41*x51
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x22    *x41*x51
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]            *x45*x51
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[ 44]    *x22*x31*x44    
        +coeff[ 45]            *x43*x51
        +coeff[ 46]    *x21*x32*x41    
        +coeff[ 47]*x11*x22    *x41    
        +coeff[ 48]        *x33    *x51
        +coeff[ 49]            *x41*x53
        +coeff[ 50]    *x23    *x41*x51
        +coeff[ 51]                *x51
        +coeff[ 52]    *x21*x31*x42    
    ;
    v_y_e_dent_1000                                =v_y_e_dent_1000                                
        +coeff[ 53]    *x21*x33        
        +coeff[ 54]*x11*x22*x31        
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]        *x31    *x53
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]*x11*x23*x31        
        +coeff[ 60]    *x23*x31    *x51
        +coeff[ 61]    *x22    *x43*x51
        ;

    return v_y_e_dent_1000                                ;
}
float p_e_dent_1000                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2803481E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.26600107E-03,-0.62707201E-01,-0.53753182E-01,-0.16716955E-01,
        -0.25727611E-01, 0.68701366E-02, 0.11811933E-01,-0.17222736E-01,
         0.75996364E-03,-0.87212399E-02, 0.10739047E-02,-0.16489763E-04,
         0.79671812E-03,-0.33639574E-02,-0.21565626E-02, 0.10654199E-02,
         0.24470279E-02,-0.61201747E-04, 0.40120998E-04,-0.30176481E-03,
        -0.16742219E-02, 0.39572650E-03,-0.49547409E-03, 0.82156173E-03,
        -0.64621604E-03,-0.89245330E-03,-0.10874953E-03,-0.44429800E-03,
         0.58766565E-03,-0.50407893E-03,-0.13778252E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_1000                                =v_p_e_dent_1000                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_1000                                =v_p_e_dent_1000                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_1000                                =v_p_e_dent_1000                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11        *x41*x51
        ;

    return v_p_e_dent_1000                                ;
}
float l_e_dent_1000                                (float *x,int m){
    int ncoeff= 49;
    float avdat= -0.4608574E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 50]={
         0.46283016E-02, 0.23497657E+00,-0.81712240E-02,-0.17898943E-01,
         0.11526869E-01,-0.61672549E-02, 0.39012503E-03,-0.53320202E-03,
        -0.12373939E-02,-0.55931136E-02, 0.13397204E-02,-0.34775077E-02,
        -0.61558485E-02,-0.18221674E-02,-0.23714463E-03, 0.49909687E-03,
         0.12575091E-03,-0.24343138E-02,-0.24880336E-02,-0.12250503E-02,
        -0.14565344E-03,-0.19713619E-03, 0.15367300E-02, 0.44586859E-03,
        -0.49366162E-03,-0.47570866E-03, 0.29384150E-03, 0.61434589E-03,
        -0.61449845E-03, 0.84451656E-03,-0.62363566E-03,-0.53781355E-02,
        -0.41433927E-04,-0.63439268E-02, 0.35152023E-03, 0.64004067E-04,
        -0.13333495E-02,-0.10014765E-02, 0.93679706E-03, 0.66933379E-03,
        -0.32005884E-03,-0.38576458E-03, 0.14413102E-02, 0.68059174E-03,
         0.81088516E-03, 0.95181528E-03,-0.30164805E-02,-0.87984791E-02,
        -0.23078748E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_1000                                =v_l_e_dent_1000                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_1000                                =v_l_e_dent_1000                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31    *x51
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x32        
    ;
    v_l_e_dent_1000                                =v_l_e_dent_1000                                
        +coeff[ 26]*x12            *x51
        +coeff[ 27]    *x24            
        +coeff[ 28]        *x34        
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]*x13            *x51
    ;
    v_l_e_dent_1000                                =v_l_e_dent_1000                                
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]*x11*x22*x32        
        +coeff[ 40]*x11*x21*x33        
        +coeff[ 41]*x13    *x31*x41    
        +coeff[ 42]    *x22*x33*x41    
        +coeff[ 43]*x11    *x31*x41*x53
    ;
    v_l_e_dent_1000                                =v_l_e_dent_1000                                
        +coeff[ 44]*x12*x23        *x51
        +coeff[ 45]*x12*x21*x31*x41*x51
        +coeff[ 46]    *x23*x33*x41    
        +coeff[ 47]    *x23*x32*x42    
        +coeff[ 48]    *x23    *x44    
        ;

    return v_l_e_dent_1000                                ;
}
float x_e_dent_900                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5100375E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
         0.34941491E-03, 0.43402677E-02,-0.12146112E+00,-0.58181649E-02,
         0.35290774E-02, 0.32265787E-02, 0.25121139E-02, 0.65154611E-03,
        -0.34581500E-03,-0.30011326E-03, 0.31606140E-03, 0.91669743E-03,
         0.11902644E-02, 0.37295991E-03,-0.15682298E-03,-0.17595357E-03,
         0.23225751E-02, 0.19108680E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_900                                =v_x_e_dent_900                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_900                                =v_x_e_dent_900                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_900                                ;
}
float t_e_dent_900                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3653939E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.79073943E-01,-0.35556573E-01, 0.77195740E+00, 0.21515457E+00,
         0.37487388E-01, 0.59741605E-02,-0.18376308E-01, 0.29525200E-01,
         0.46557967E-01, 0.17372154E-01, 0.18631179E-01,-0.76384894E-02,
        -0.13663034E-01,-0.13014110E-01,-0.96379369E-02, 0.25114745E-02,
        -0.64904671E-02,-0.31211372E-02,-0.20858189E-01, 0.68739946E-02,
         0.24872041E-04,-0.84929605E-03, 0.44667833E-02,-0.22678120E-01,
        -0.23917515E-01,-0.10023969E-03,-0.19170705E-03, 0.43483905E-03,
         0.26829031E-02,-0.31767986E-03, 0.60956134E-03, 0.74497418E-03,
         0.39009866E-02,-0.70720776E-02,-0.30693909E-01,-0.17102119E-01,
        -0.89903135E-03, 0.38106921E-02, 0.31109322E-02,-0.10339246E-02,
        -0.84931413E-02,-0.20009352E-01, 0.78828621E-03, 0.12197316E-02,
         0.14999347E-02,-0.67412690E-03,-0.67542505E-03,-0.18643099E-02,
        -0.15757392E-02,-0.46411736E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_dent_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_900                                =v_t_e_dent_900                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_900                                =v_t_e_dent_900                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_900                                =v_t_e_dent_900                                
        +coeff[ 26]        *x33*x41*x51
        +coeff[ 27]*x12                
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]        *x31*x41*x51
        +coeff[ 33]    *x22*x32        
        +coeff[ 34]    *x23*x31*x41    
    ;
    v_t_e_dent_900                                =v_t_e_dent_900                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]*x12*x21        *x52
        +coeff[ 43]*x12*x21*x32    *x51
    ;
    v_t_e_dent_900                                =v_t_e_dent_900                                
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]*x11    *x31*x41    
        +coeff[ 46]*x11        *x42    
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]*x12*x21        *x51
        ;

    return v_t_e_dent_900                                ;
}
float y_e_dent_900                                (float *x,int m){
    int ncoeff= 57;
    float avdat=  0.3139624E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 58]={
         0.77142931E-05, 0.12872529E+00, 0.38151577E-01, 0.11851739E-01,
         0.56376364E-02, 0.37143517E-02, 0.40720464E-02,-0.25902465E-02,
        -0.15844248E-02,-0.22869266E-02,-0.13956217E-02,-0.54331368E-03,
        -0.67209493E-03,-0.18066695E-03,-0.14059860E-05,-0.13080637E-02,
        -0.13330762E-03,-0.14746051E-03,-0.30156970E-03,-0.22669278E-03,
        -0.42409924E-03, 0.37798382E-03, 0.10435922E-04,-0.19462547E-02,
        -0.33480288E-02,-0.11206047E-02,-0.20127406E-02,-0.85440022E-03,
        -0.42805160E-03, 0.35041281E-04, 0.27948294E-04, 0.30451626E-03,
         0.12567535E-03,-0.57904046E-04, 0.20081681E-03,-0.14484355E-04,
         0.38549488E-05, 0.16103195E-04, 0.15770606E-03,-0.99246034E-04,
        -0.46999521E-04,-0.46782312E-03, 0.45918681E-04,-0.49000001E-03,
        -0.26829200E-03,-0.72677110E-04,-0.34736491E-04,-0.15235742E-04,
         0.10538020E-03,-0.74483702E-04, 0.31296404E-04, 0.14873107E-03,
         0.27067619E-04, 0.53090702E-04, 0.66986802E-04,-0.35562844E-04,
         0.29950037E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21    *x41    
        +coeff[  6]    *x21*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11        *x41    
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[ 17]*x11    *x31        
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11        *x41*x51
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]        *x31*x42*x51
        +coeff[ 32]    *x23*x31        
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]        *x32*x41*x51
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[ 35]            *x45*x51
        +coeff[ 36]                *x51
        +coeff[ 37]    *x21    *x43    
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x21*x32*x41    
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x31*x44    
        +coeff[ 42]        *x33    *x51
        +coeff[ 43]        *x32*x43    
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[ 44]        *x33*x42    
        +coeff[ 45]    *x21*x31*x42    
        +coeff[ 46]    *x21*x33        
        +coeff[ 47]*x11*x22*x31        
        +coeff[ 48]*x11*x21    *x43    
        +coeff[ 49]        *x34*x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]*x11*x21*x31*x42    
        +coeff[ 52]        *x31    *x53
    ;
    v_y_e_dent_900                                =v_y_e_dent_900                                
        +coeff[ 53]*x11*x21*x32*x41    
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]*x11*x23*x31        
        +coeff[ 56]    *x23*x31    *x51
        ;

    return v_y_e_dent_900                                ;
}
float p_e_dent_900                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1358759E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.10919980E-03,-0.62903747E-01,-0.53881057E-01,-0.16718158E-01,
        -0.25764596E-01, 0.68669422E-02, 0.11823844E-01,-0.17269624E-01,
         0.79569535E-03,-0.87493183E-02, 0.10730721E-02,-0.95042160E-05,
         0.79040317E-03,-0.33100280E-02,-0.21445048E-02, 0.10652248E-02,
         0.24720167E-02, 0.43974516E-04, 0.67057132E-04,-0.16841271E-02,
         0.39851823E-03,-0.51139179E-03, 0.85036963E-03,-0.64004265E-03,
        -0.93316421E-03,-0.15765936E-03,-0.60784980E-03, 0.57131052E-03,
        -0.51072927E-03,-0.13039111E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_900                                =v_p_e_dent_900                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_900                                =v_p_e_dent_900                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]    *x21    *x41*x51
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x52
    ;
    v_p_e_dent_900                                =v_p_e_dent_900                                
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11        *x41*x51
        ;

    return v_p_e_dent_900                                ;
}
float l_e_dent_900                                (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.4179876E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.40836218E-02, 0.23416480E+00,-0.84008509E-02,-0.17346893E-01,
         0.11115825E-01,-0.57148719E-02, 0.29834703E-03, 0.20969164E-03,
        -0.18159018E-02,-0.56480924E-02, 0.13499769E-02,-0.34608238E-02,
        -0.73626952E-03,-0.16553319E-02,-0.19981824E-02, 0.32390610E-03,
        -0.96128776E-03, 0.17890906E-03,-0.23495214E-03,-0.86499233E-03,
        -0.86034863E-03,-0.24656882E-03,-0.30815683E-03,-0.78825856E-03,
         0.99258439E-03, 0.41197197E-03,-0.41619784E-03,-0.29105146E-02,
        -0.50855838E-02,-0.49349088E-02,-0.62461048E-02,-0.44517335E-02,
        -0.54593047E-03, 0.35512864E-03, 0.47520956E-03, 0.62592287E-03,
         0.14483866E-02,-0.58044208E-03,-0.15797728E-02, 0.72892691E-03,
        -0.87990016E-02,-0.14118343E-01,-0.96805589E-02, 0.10817848E-02,
        -0.92638144E-03, 0.12416111E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_900                                =v_l_e_dent_900                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21        *x52
    ;
    v_l_e_dent_900                                =v_l_e_dent_900                                
        +coeff[ 17]                *x51
        +coeff[ 18]        *x31    *x51
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x21*x32        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]    *x21*x31*x42    
        +coeff[ 24]*x11*x22        *x51
        +coeff[ 25]*x11        *x42*x51
    ;
    v_l_e_dent_900                                =v_l_e_dent_900                                
        +coeff[ 26]*x12        *x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]*x12            *x53
        +coeff[ 34]    *x23*x33        
    ;
    v_l_e_dent_900                                =v_l_e_dent_900                                
        +coeff[ 35]    *x21*x32    *x53
        +coeff[ 36]*x11*x22*x31*x42    
        +coeff[ 37]*x11    *x31*x44    
        +coeff[ 38]*x11*x24        *x51
        +coeff[ 39]*x12*x22*x32        
        +coeff[ 40]    *x23*x33*x41    
        +coeff[ 41]    *x23*x32*x42    
        +coeff[ 42]    *x23*x31*x43    
        +coeff[ 43]*x13*x22        *x51
    ;
    v_l_e_dent_900                                =v_l_e_dent_900                                
        +coeff[ 44]*x11*x22*x33    *x51
        +coeff[ 45]*x11*x21*x32*x42*x51
        ;

    return v_l_e_dent_900                                ;
}
float x_e_dent_800                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5100723E+01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
         0.65031351E-03, 0.43395725E-02,-0.12146135E+00,-0.58180070E-02,
         0.35327284E-02, 0.32460841E-02, 0.25470015E-02, 0.61613566E-03,
        -0.34323224E-03,-0.29409130E-03, 0.30832773E-03, 0.92786888E-03,
         0.12094297E-02, 0.38479338E-03,-0.15449194E-03,-0.17926095E-03,
         0.22849210E-02, 0.18322056E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_800                                =v_x_e_dent_800                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_800                                =v_x_e_dent_800                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_800                                ;
}
float t_e_dent_800                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3655573E+01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.80311269E-01,-0.35516437E-01, 0.77223438E+00, 0.21512195E+00,
         0.36983047E-01, 0.59323879E-02,-0.18315969E-01, 0.29176779E-01,
         0.46458855E-01, 0.17168859E-01, 0.18908098E-01,-0.77125346E-02,
        -0.13135687E-01,-0.13190142E-01,-0.99114543E-02, 0.24641820E-02,
        -0.65176934E-02,-0.29448092E-02,-0.20765917E-01, 0.73325760E-02,
         0.30204532E-03, 0.32752872E-04, 0.46866634E-02,-0.23199828E-01,
        -0.31545397E-01,-0.23946922E-01,-0.89177320E-05,-0.38062962E-03,
         0.23116472E-02,-0.30488960E-03, 0.38991547E-02,-0.68392111E-02,
        -0.17142450E-01, 0.41703638E-03, 0.51410345E-03, 0.69461495E-03,
        -0.10542524E-02, 0.40434902E-02, 0.33128427E-02,-0.10971637E-02,
        -0.86945510E-02,-0.19498527E-01, 0.26139311E-03, 0.29384633E-03,
        -0.78458025E-03,-0.78880257E-03,-0.47278739E-03, 0.12622059E-02,
         0.86208005E-04,-0.10490965E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_800                                =v_t_e_dent_800                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_800                                =v_t_e_dent_800                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_800                                =v_t_e_dent_800                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x12                
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_dent_800                                =v_t_e_dent_800                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_dent_800                                =v_t_e_dent_800                                
        +coeff[ 44]*x11    *x31*x41    
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]    *x22*x31*x41*x51
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_dent_800                                ;
}
float y_e_dent_800                                (float *x,int m){
    int ncoeff= 58;
    float avdat=  0.7583873E-03;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 59]={
        -0.77381008E-03, 0.12859555E+00, 0.38062204E-01, 0.11871978E-01,
         0.56455927E-02, 0.40762280E-02, 0.12515804E-04, 0.37201769E-02,
        -0.26289255E-02,-0.16067682E-02,-0.25796785E-02,-0.15632163E-02,
        -0.67157572E-03, 0.18631838E-04, 0.21081676E-05,-0.15086491E-02,
        -0.53599058E-03,-0.13380441E-03,-0.14611079E-03,-0.32286014E-03,
        -0.21175806E-03,-0.42580525E-03, 0.38006649E-03, 0.24225757E-04,
        -0.25336442E-02,-0.11929905E-02,-0.87916770E-03,-0.74162288E-03,
        -0.16071146E-02, 0.77043900E-04, 0.51304229E-04, 0.24196022E-04,
         0.35000518E-04, 0.26762720E-04, 0.29628340E-03, 0.12277199E-03,
        -0.82399041E-04, 0.17724361E-03,-0.13386315E-02,-0.17191874E-02,
        -0.42235377E-03, 0.29533014E-04,-0.17569660E-02,-0.10692116E-03,
        -0.45147710E-04, 0.45745837E-04, 0.39511870E-04,-0.53471519E-03,
        -0.73364383E-04, 0.10788719E-03,-0.36919246E-04,-0.19657922E-04,
         0.92644084E-04, 0.10946469E-03, 0.22905047E-04, 0.52105304E-04,
        -0.35199140E-04, 0.53472402E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21    *x41*x51
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[ 17]*x11        *x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[ 26]    *x24*x31        
        +coeff[ 27]    *x22    *x45    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]    *x24*x32*x41    
        +coeff[ 30]    *x24*x33        
        +coeff[ 31]    *x22*x32*x45    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[ 35]    *x23*x31        
        +coeff[ 36]    *x22    *x41*x51
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x22*x32*x41    
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x22*x31*x44    
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]            *x41*x53
        +coeff[ 47]    *x22*x33*x42    
        +coeff[ 48]    *x21*x31*x42    
        +coeff[ 49]            *x43*x51
        +coeff[ 50]    *x21*x33        
        +coeff[ 51]*x11*x22*x31        
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_dent_800                                =v_y_e_dent_800                                
        +coeff[ 53]*x11*x21*x31*x42    
        +coeff[ 54]        *x31    *x53
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]*x11*x23*x31        
        +coeff[ 57]    *x22*x32*x41*x51
        ;

    return v_y_e_dent_800                                ;
}
float p_e_dent_800                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.9323418E-04;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.96061747E-04,-0.62831923E-01,-0.54007344E-01,-0.16730173E-01,
        -0.25759520E-01, 0.68679247E-02, 0.11824694E-01,-0.17319195E-01,
         0.80293405E-03,-0.87728556E-02, 0.10731588E-02,-0.57506240E-05,
         0.78786962E-03,-0.33872949E-02,-0.21627126E-02, 0.10587503E-02,
         0.25393739E-02,-0.20703677E-04, 0.47964862E-04,-0.30952072E-03,
        -0.17147211E-02, 0.40008430E-03,-0.51442569E-03, 0.79192081E-03,
        -0.65279409E-03,-0.90719259E-03,-0.10523205E-03,-0.44681199E-03,
         0.57752879E-03,-0.49265393E-03,-0.12705689E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_800                                =v_p_e_dent_800                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_800                                =v_p_e_dent_800                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_800                                =v_p_e_dent_800                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11        *x41*x51
        ;

    return v_p_e_dent_800                                ;
}
float l_e_dent_800                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.3503442E-02;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.35312593E-02, 0.23503773E+00,-0.83415601E-02,-0.17652497E-01,
         0.11199959E-01,-0.59176651E-02, 0.16525264E-02,-0.20856198E-03,
        -0.16450163E-02,-0.57449243E-02, 0.12298648E-02,-0.62284861E-02,
        -0.52555040E-02,-0.14998114E-02,-0.10626847E-02, 0.72755356E-03,
        -0.18941305E-02,-0.22689824E-02, 0.86808228E-04,-0.16104357E-02,
        -0.44013006E-02,-0.12023591E-03, 0.12425042E-02, 0.49233093E-03,
        -0.20539500E-03, 0.20479933E-03,-0.61271829E-04, 0.45781338E-03,
        -0.27933486E-02, 0.42068292E-03,-0.47139396E-03, 0.90647471E-03,
         0.94686053E-03, 0.55793661E-03, 0.15037636E-02, 0.16226727E-02,
        -0.11600131E-02, 0.45569599E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_800                                =v_l_e_dent_800                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dent_800                                =v_l_e_dent_800                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]                *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]*x12                
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11*x21*x31        
    ;
    v_l_e_dent_800                                =v_l_e_dent_800                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]*x11*x21*x31    *x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x22*x32    *x51
        +coeff[ 30]        *x33*x41*x51
        +coeff[ 31]        *x32*x42*x51
        +coeff[ 32]    *x21        *x54
        +coeff[ 33]    *x24*x32        
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_dent_800                                =v_l_e_dent_800                                
        +coeff[ 35]    *x21*x33*x41*x51
        +coeff[ 36]    *x21*x31*x41*x53
        +coeff[ 37]*x11*x21*x34        
        ;

    return v_l_e_dent_800                                ;
}
float x_e_dent_700                                (float *x,int m){
    int ncoeff= 20;
    float avdat= -0.5100277E+01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
         0.19913328E-03,-0.28243026E-04,-0.12158217E+00,-0.58149919E-02,
        -0.20455320E-05, 0.43416573E-02, 0.35395140E-02, 0.31748202E-02,
         0.24598979E-02, 0.71358378E-03,-0.34505871E-03,-0.29908124E-03,
         0.87550050E-03, 0.11599609E-02, 0.37469607E-03, 0.36065138E-03,
        -0.15492183E-03,-0.17069260E-03, 0.24558262E-02, 0.20227253E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_dent_700                                =v_x_e_dent_700                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]*x11            *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]            *x42    
    ;
    v_x_e_dent_700                                =v_x_e_dent_700                                
        +coeff[ 17]*x12    *x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        ;

    return v_x_e_dent_700                                ;
}
float t_e_dent_700                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3652239E+01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.77048905E-01,-0.35490293E-01, 0.77316076E+00, 0.21601078E+00,
         0.37041720E-01, 0.18622188E-01,-0.18229114E-01, 0.29308958E-01,
         0.47178071E-01, 0.59787929E-02, 0.17179817E-01,-0.79058055E-02,
        -0.13075908E-01,-0.12623286E-01,-0.10544701E-01, 0.24771560E-02,
        -0.61531602E-02,-0.29478911E-02,-0.21107772E-01, 0.74616848E-02,
        -0.15723363E-02, 0.44917613E-02,-0.22828272E-01,-0.32138892E-01,
        -0.25009943E-01, 0.83323699E-04,-0.16902207E-03, 0.42586317E-03,
         0.26335246E-02,-0.29482879E-03, 0.38915437E-02,-0.74008522E-02,
        -0.16831409E-01,-0.72456745E-03, 0.55150402E-03, 0.82810578E-03,
        -0.13168774E-02, 0.35868855E-02, 0.28932136E-02,-0.11722476E-02,
        -0.56203022E-04,-0.86407112E-02,-0.19485561E-01, 0.11894021E-03,
         0.23158739E-03,-0.57570555E-03,-0.15744775E-02,-0.13505955E-02,
         0.11525069E-02, 0.33872985E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_700                                =v_t_e_dent_700                                
        +coeff[  8]    *x23            
        +coeff[  9]                *x51
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_700                                =v_t_e_dent_700                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_700                                =v_t_e_dent_700                                
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]*x12                
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x11        *x42    
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_dent_700                                =v_t_e_dent_700                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]*x13    *x31*x41    
        +coeff[ 41]    *x21*x33*x41    
        +coeff[ 42]    *x21*x32*x42    
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_dent_700                                =v_t_e_dent_700                                
        +coeff[ 44]*x12*x21            
        +coeff[ 45]*x11    *x31*x41    
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]    *x21*x32    *x51
        +coeff[ 49]*x11*x23    *x41    
        ;

    return v_t_e_dent_700                                ;
}
float y_e_dent_700                                (float *x,int m){
    int ncoeff= 59;
    float avdat= -0.2210756E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 60]={
         0.25750592E-03, 0.12830690E+00, 0.37910752E-01, 0.11862030E-01,
         0.56527071E-02, 0.40883180E-02, 0.12946966E-04, 0.37362815E-02,
        -0.25800506E-02,-0.15792302E-02,-0.22998385E-02,-0.14111116E-02,
        -0.54519722E-03,-0.67318458E-03,-0.18094429E-03, 0.55314308E-05,
        -0.13143497E-02,-0.13219830E-03,-0.14668205E-03,-0.30693033E-03,
        -0.22706587E-03,-0.43125136E-03, 0.37877570E-03, 0.28713766E-04,
         0.12256479E-03,-0.19551660E-02,-0.33631115E-02,-0.11410499E-02,
        -0.20241477E-02,-0.86277322E-03,-0.43133131E-03, 0.36217538E-04,
         0.25961986E-04, 0.29045070E-03,-0.19641169E-04, 0.20267403E-03,
        -0.58196169E-05, 0.35379403E-05,-0.77763369E-04,-0.11256033E-03,
        -0.35230587E-04,-0.40582825E-04,-0.46692041E-03, 0.46279627E-04,
        -0.46779649E-03,-0.24275527E-03, 0.36418827E-04,-0.84737358E-05,
         0.14377653E-03,-0.19479337E-04, 0.84208237E-04,-0.64067528E-04,
         0.91016569E-04, 0.24125835E-04, 0.59033526E-04,-0.34103181E-04,
        -0.13013007E-04, 0.25741618E-04,-0.60688075E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x33    *x52
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[ 17]*x11        *x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x43    
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x24*x31        
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x22    *x41*x51
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x45*x51
        +coeff[ 37]                *x51
        +coeff[ 38]    *x21*x31*x42    
        +coeff[ 39]    *x21*x32*x41    
        +coeff[ 40]    *x21*x33        
        +coeff[ 41]*x11*x22    *x41    
        +coeff[ 42]        *x31*x44    
        +coeff[ 43]        *x33    *x51
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[ 44]        *x32*x43    
        +coeff[ 45]        *x33*x42    
        +coeff[ 46]            *x41*x53
        +coeff[ 47]*x12        *x41    
        +coeff[ 48]            *x43*x51
        +coeff[ 49]*x11*x22*x31        
        +coeff[ 50]*x11*x21    *x43    
        +coeff[ 51]        *x34*x41    
        +coeff[ 52]*x11*x21*x31*x42    
    ;
    v_y_e_dent_700                                =v_y_e_dent_700                                
        +coeff[ 53]        *x31    *x53
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]*x11*x23*x31        
        +coeff[ 56]*x11        *x45    
        +coeff[ 57]    *x23*x31    *x51
        +coeff[ 58]    *x24    *x41*x51
        ;

    return v_y_e_dent_700                                ;
}
float p_e_dent_700                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.6662287E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.45434528E-04,-0.62942304E-01,-0.54180790E-01,-0.16767101E-01,
        -0.25819972E-01, 0.68846494E-02, 0.11829446E-01,-0.17336933E-01,
         0.84111182E-03,-0.88254083E-02, 0.10756879E-02, 0.14001260E-06,
         0.78322523E-03,-0.33668550E-02,-0.21533428E-02, 0.10533294E-02,
         0.25071267E-02,-0.42137515E-04, 0.27978922E-04,-0.30989246E-03,
        -0.16931038E-02, 0.40075730E-03,-0.50621317E-03, 0.83781622E-03,
        -0.63592830E-03,-0.90729905E-03,-0.13212618E-03,-0.43173425E-03,
         0.59598149E-03,-0.52913692E-03,-0.12857383E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_700                                =v_p_e_dent_700                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_700                                =v_p_e_dent_700                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_700                                =v_p_e_dent_700                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11        *x41*x51
        ;

    return v_p_e_dent_700                                ;
}
float l_e_dent_700                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.4380466E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.42366562E-02, 0.23471317E+00,-0.83783623E-02,-0.17103253E-01,
         0.11051681E-01,-0.57959980E-02,-0.67693670E-03,-0.15703029E-02,
        -0.52432586E-02, 0.11593361E-02,-0.58975378E-02,-0.43045464E-02,
        -0.20043803E-02, 0.74011437E-03,-0.15694228E-02,-0.87410974E-03,
         0.18006944E-03,-0.93478465E-03,-0.18487109E-02, 0.94716880E-03,
         0.39097993E-03, 0.38256348E-03, 0.44274438E-03, 0.42196998E-03,
         0.72047312E-03, 0.50707301E-03,-0.57318746E-02,-0.46749651E-02,
        -0.12474888E-02,-0.97784598E-03,-0.38937185E-03,-0.74760086E-03,
         0.72968542E-03, 0.65251306E-03,-0.59039111E-03,-0.55668398E-03,
         0.64736989E-03,-0.59851119E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_dent_700                                =v_l_e_dent_700                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_700                                =v_l_e_dent_700                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]        *x31*x41*x52
        +coeff[ 24]*x11*x21*x31*x41    
        +coeff[ 25]*x12        *x42    
    ;
    v_l_e_dent_700                                =v_l_e_dent_700                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x22*x31*x41*x51
        +coeff[ 29]*x11*x24            
        +coeff[ 30]*x11    *x31*x43    
        +coeff[ 31]        *x34*x42    
        +coeff[ 32]    *x21*x32*x41*x52
        +coeff[ 33]    *x23        *x53
        +coeff[ 34]*x12*x24            
    ;
    v_l_e_dent_700                                =v_l_e_dent_700                                
        +coeff[ 35]*x12*x21        *x53
        +coeff[ 36]*x13*x21*x32        
        +coeff[ 37]*x13*x22        *x51
        ;

    return v_l_e_dent_700                                ;
}
float x_e_dent_600                                (float *x,int m){
    int ncoeff= 21;
    float avdat= -0.5100733E+01;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
         0.70048496E-03,-0.24987603E-04,-0.12181153E+00,-0.58112536E-02,
        -0.10578197E-04, 0.43417569E-02, 0.35602076E-02, 0.30028373E-02,
         0.25976356E-02, 0.58593863E-03,-0.34572385E-03,-0.29886674E-03,
         0.93001808E-03, 0.12505022E-02, 0.38192284E-03, 0.34824907E-03,
        -0.15519207E-03,-0.16678084E-03, 0.24298953E-02, 0.19927935E-02,
         0.52199775E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_dent_600                                =v_x_e_dent_600                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]*x11            *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]            *x42    
    ;
    v_x_e_dent_600                                =v_x_e_dent_600                                
        +coeff[ 17]*x12    *x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x21*x31*x43    
        ;

    return v_x_e_dent_600                                ;
}
float t_e_dent_600                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3656389E+01;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.81434280E-01,-0.35448134E-01, 0.77463114E+00, 0.21648180E+00,
         0.37114605E-01, 0.60032392E-02,-0.18408142E-01, 0.28972598E-01,
         0.46870343E-01, 0.16926117E-01, 0.18748783E-01,-0.77616242E-02,
        -0.13664422E-01,-0.13322782E-01,-0.92503764E-02, 0.24882213E-02,
        -0.68873619E-02,-0.29911371E-02,-0.20980477E-01, 0.74076108E-02,
        -0.40382572E-03, 0.20778560E-03, 0.43752445E-02,-0.23075083E-01,
        -0.31269722E-01,-0.24205139E-01,-0.20676980E-04, 0.34050066E-04,
         0.22407530E-02,-0.24202222E-03, 0.37299974E-02,-0.66995695E-02,
        -0.16720194E-01, 0.39969778E-03, 0.53950795E-03, 0.73424465E-03,
        -0.11984027E-02, 0.37043984E-02, 0.28194936E-02,-0.12647957E-02,
        -0.82761943E-02,-0.18550109E-01, 0.35834764E-04, 0.30228149E-03,
        -0.75648766E-03,-0.67827612E-03, 0.36371898E-03,-0.52164559E-03,
         0.11778777E-02,-0.30956359E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_600                                =v_t_e_dent_600                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_600                                =v_t_e_dent_600                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_600                                =v_t_e_dent_600                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x12                
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_dent_600                                =v_t_e_dent_600                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_dent_600                                =v_t_e_dent_600                                
        +coeff[ 44]*x11    *x31*x41    
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]*x11*x21    *x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]    *x21*x32    *x51
        +coeff[ 49]*x13    *x32        
        ;

    return v_t_e_dent_600                                ;
}
float y_e_dent_600                                (float *x,int m){
    int ncoeff= 59;
    float avdat= -0.5715454E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 60]={
         0.62430400E-03, 0.12797132E+00, 0.37725743E-01, 0.11887530E-01,
         0.56669265E-02, 0.41025546E-02, 0.21062102E-04, 0.37510595E-02,
        -0.25859638E-02,-0.15990449E-02,-0.22865357E-02,-0.13975633E-02,
        -0.67342335E-03,-0.20754020E-03,-0.10213240E-05,-0.12877163E-02,
        -0.53570152E-03,-0.13520927E-03,-0.14636184E-03,-0.30797563E-03,
        -0.21476709E-03,-0.42625997E-03, 0.38811873E-03, 0.59585277E-05,
        -0.19444061E-02,-0.33351786E-02,-0.11326354E-02,-0.20247581E-02,
        -0.85293053E-03,-0.42526482E-03, 0.37131515E-04, 0.26014659E-04,
        -0.10413175E-03, 0.28146716E-03, 0.12392639E-03,-0.64214102E-04,
         0.19779349E-03, 0.17057655E-04, 0.50799558E-05,-0.42961954E-04,
        -0.50836924E-03, 0.42746466E-04,-0.50575577E-03,-0.24569919E-03,
        -0.27462008E-05,-0.63713611E-04,-0.67295332E-05, 0.11839446E-03,
        -0.34451557E-04,-0.17878183E-04, 0.12511459E-03,-0.68285408E-04,
         0.30904186E-04, 0.16653510E-03, 0.23789811E-04, 0.48160895E-04,
         0.39940260E-04,-0.42632044E-04,-0.26835662E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21    *x41*x51
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[ 17]*x11        *x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x24*x31        
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x11        *x41*x51
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x23*x31        
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[ 35]    *x22    *x41*x51
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]            *x45*x51
        +coeff[ 38]                *x51
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]        *x31*x44    
        +coeff[ 41]        *x33    *x51
        +coeff[ 42]        *x32*x43    
        +coeff[ 43]        *x33*x42    
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[ 44]*x12        *x41    
        +coeff[ 45]    *x21*x31*x42    
        +coeff[ 46]*x12    *x31        
        +coeff[ 47]            *x43*x51
        +coeff[ 48]    *x21*x33        
        +coeff[ 49]*x11*x22*x31        
        +coeff[ 50]*x11*x21    *x43    
        +coeff[ 51]        *x34*x41    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_dent_600                                =v_y_e_dent_600                                
        +coeff[ 53]*x11*x21*x31*x42    
        +coeff[ 54]        *x31    *x53
        +coeff[ 55]*x11*x21*x32*x41    
        +coeff[ 56]    *x23    *x41*x51
        +coeff[ 57]*x11*x23*x31        
        +coeff[ 58]*x12*x22    *x41    
        ;

    return v_y_e_dent_600                                ;
}
float p_e_dent_600                                (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.2834649E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.30890424E-03,-0.63237168E-01,-0.54415274E-01,-0.16796431E-01,
        -0.25828898E-01, 0.68889763E-02, 0.11844564E-01,-0.17332355E-01,
         0.81397366E-03,-0.88083688E-02, 0.10744692E-02, 0.83806486E-06,
         0.78520854E-03,-0.33219045E-02,-0.21471183E-02, 0.10621240E-02,
         0.24919054E-02, 0.82245147E-04, 0.49118935E-04,-0.17112785E-02,
         0.40994611E-03,-0.51516847E-03, 0.77478384E-03,-0.64210437E-03,
        -0.93658979E-03,-0.12572338E-03,-0.61984110E-03, 0.58273494E-03,
        -0.53451542E-03,-0.12539241E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_600                                =v_p_e_dent_600                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_600                                =v_p_e_dent_600                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]    *x21    *x41*x51
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x52
    ;
    v_p_e_dent_600                                =v_p_e_dent_600                                
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11        *x41*x51
        ;

    return v_p_e_dent_600                                ;
}
float l_e_dent_600                                (float *x,int m){
    int ncoeff= 47;
    float avdat= -0.3495570E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 48]={
         0.34053926E-02, 0.23501039E+00,-0.83395727E-02,-0.17534889E-01,
         0.11327669E-01,-0.59437966E-02, 0.15601328E-02,-0.24421458E-03,
        -0.15819536E-02,-0.58581056E-02, 0.13228813E-02,-0.72779409E-02,
        -0.44382019E-02,-0.14182507E-02, 0.13492220E-03, 0.64285728E-03,
        -0.13005703E-02,-0.16455869E-02,-0.14079817E-02,-0.12994549E-03,
         0.38513318E-02,-0.72275012E-04,-0.12979984E-03, 0.48466699E-03,
        -0.20802107E-03,-0.12274479E-02, 0.19891320E-02, 0.41698676E-03,
         0.26783842E-03,-0.26293788E-02,-0.84180920E-03, 0.72366581E-03,
        -0.29232539E-02,-0.76088915E-03,-0.20478624E-02, 0.84229209E-03,
         0.64122718E-03,-0.26342848E-02,-0.31340788E-02,-0.54800413E-02,
        -0.30219280E-02, 0.12531782E-02,-0.91428054E-03,-0.11420628E-02,
         0.27370406E-03, 0.37050326E-02,-0.12695782E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_600                                =v_l_e_dent_600                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dent_600                                =v_l_e_dent_600                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22*x32*x42    
        +coeff[ 21]        *x31        
        +coeff[ 22]*x11    *x31        
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_dent_600                                =v_l_e_dent_600                                
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x41*x52
        +coeff[ 28]*x12*x21*x31        
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x32    *x52
        +coeff[ 31]            *x42*x53
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x12    *x31*x41*x51
        +coeff[ 34]    *x21*x33*x41*x51
    ;
    v_l_e_dent_600                                =v_l_e_dent_600                                
        +coeff[ 35]    *x23    *x42*x51
        +coeff[ 36]*x12*x22    *x42    
        +coeff[ 37]    *x23*x33*x41    
        +coeff[ 38]    *x21*x34*x42    
        +coeff[ 39]    *x23*x31*x43    
        +coeff[ 40]    *x23    *x44    
        +coeff[ 41]        *x31*x43*x53
        +coeff[ 42]    *x23        *x54
        +coeff[ 43]*x11*x24*x32        
    ;
    v_l_e_dent_600                                =v_l_e_dent_600                                
        +coeff[ 44]*x11*x22*x32*x42    
        +coeff[ 45]*x11*x22*x31*x43    
        +coeff[ 46]*x12*x23*x31*x41    
        ;

    return v_l_e_dent_600                                ;
}
float x_e_dent_500                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5099492E+01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
        -0.59356220E-03, 0.43349988E-02,-0.25481699E-04,-0.12192576E+00,
        -0.58027138E-02, 0.35657568E-02, 0.32312556E-02, 0.25018153E-02,
         0.64649433E-03,-0.35146988E-03,-0.29641774E-03, 0.89710118E-03,
         0.11894802E-02, 0.38471381E-03, 0.34406461E-03,-0.14489434E-03,
        -0.17292410E-03, 0.23651866E-02, 0.19520245E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_500                                =v_x_e_dent_500                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_x_e_dent_500                                =v_x_e_dent_500                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_500                                ;
}
float t_e_dent_500                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3647593E+01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.72366223E-01,-0.35435539E-01, 0.77593756E+00, 0.21732897E+00,
         0.36986388E-01, 0.60196575E-02,-0.18436655E-01, 0.28716048E-01,
         0.47514088E-01, 0.16839968E-01, 0.18798206E-01,-0.78022918E-02,
        -0.13681177E-01,-0.13052946E-01,-0.96999006E-02, 0.25550609E-02,
        -0.65022670E-02,-0.29659199E-02,-0.21014372E-01, 0.73459246E-02,
         0.90090936E-03, 0.44211526E-02,-0.23507735E-01,-0.31373695E-01,
        -0.24639435E-01, 0.17558144E-03,-0.28004523E-03, 0.43912066E-03,
         0.22644375E-02,-0.29607638E-03, 0.67342177E-03, 0.39063236E-02,
        -0.68473839E-02,-0.17301599E-01, 0.33370208E-03, 0.69958012E-03,
        -0.12416650E-02, 0.40587108E-02, 0.34382117E-02,-0.10694296E-02,
        -0.80684628E-02,-0.19564999E-01, 0.75225631E-03,-0.76964451E-03,
        -0.73713146E-03,-0.18018774E-03,-0.73984772E-03, 0.90231997E-03,
         0.31824032E-03, 0.53870014E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_500                                =v_t_e_dent_500                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_500                                =v_t_e_dent_500                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_500                                =v_t_e_dent_500                                
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]*x12                
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]    *x22*x32        
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]*x12*x21            
    ;
    v_t_e_dent_500                                =v_t_e_dent_500                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x11    *x31*x41    
    ;
    v_t_e_dent_500                                =v_t_e_dent_500                                
        +coeff[ 44]*x11        *x42    
        +coeff[ 45]*x11            *x52
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x11*x23*x31        
        +coeff[ 49]*x13*x21    *x42    
        ;

    return v_t_e_dent_500                                ;
}
float y_e_dent_500                                (float *x,int m){
    int ncoeff= 59;
    float avdat= -0.3987878E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 60]={
         0.36858520E-03, 0.12757120E+00, 0.37439156E-01, 0.11902868E-01,
         0.56767915E-02, 0.37819105E-02, 0.41195359E-02,-0.25801957E-02,
        -0.15840684E-02,-0.22618752E-02,-0.13874150E-02,-0.54195436E-03,
        -0.67458145E-03,-0.19135927E-03, 0.25401578E-05,-0.14662261E-03,
        -0.12982570E-02,-0.21525701E-03, 0.39443325E-05,-0.13692162E-03,
        -0.30165416E-03,-0.42722214E-03, 0.38168457E-03, 0.10480304E-06,
         0.12207290E-03,-0.19831902E-02,-0.34001633E-02,-0.11488260E-02,
        -0.20344099E-02,-0.85761398E-03,-0.43574246E-03, 0.38150152E-04,
         0.27967286E-04, 0.28311860E-03,-0.98688879E-05, 0.20064358E-03,
        -0.63719704E-05, 0.10508817E-04, 0.13438928E-03,-0.10569922E-03,
        -0.46170207E-04,-0.50808978E-03, 0.44788907E-04,-0.52304886E-03,
        -0.28473311E-03,-0.24785220E-05, 0.59961931E-05,-0.81761456E-04,
        -0.34866596E-04,-0.20264964E-04, 0.13587113E-03,-0.81701583E-04,
         0.34099743E-04, 0.17767771E-03, 0.25001405E-04, 0.62558058E-04,
         0.54003864E-04,-0.38868515E-04,-0.69716778E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21    *x41    
        +coeff[  6]    *x21*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]*x11        *x43    
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x43    
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x24*x31        
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x22    *x41*x51
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x45*x51
        +coeff[ 37]    *x21    *x43    
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x21*x32*x41    
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x31*x44    
        +coeff[ 42]        *x33    *x51
        +coeff[ 43]        *x32*x43    
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[ 44]        *x33*x42    
        +coeff[ 45]                *x51
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x31*x42    
        +coeff[ 48]    *x21*x33        
        +coeff[ 49]*x11*x22*x31        
        +coeff[ 50]*x11*x21    *x43    
        +coeff[ 51]        *x34*x41    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_dent_500                                =v_y_e_dent_500                                
        +coeff[ 53]*x11*x21*x31*x42    
        +coeff[ 54]        *x31    *x53
        +coeff[ 55]*x11*x21*x32*x41    
        +coeff[ 56]    *x23    *x41*x51
        +coeff[ 57]*x11*x23*x31        
        +coeff[ 58]    *x24    *x41*x51
        ;

    return v_y_e_dent_500                                ;
}
float p_e_dent_500                                (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.1206183E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.10780547E-03,-0.63224301E-01,-0.54767661E-01,-0.16826723E-01,
        -0.25926715E-01, 0.69015673E-02, 0.11864522E-01,-0.17368250E-01,
         0.77371480E-03,-0.88509852E-02, 0.10751252E-02,-0.94950892E-05,
         0.79225958E-03,-0.33681197E-02,-0.21593552E-02, 0.10634771E-02,
         0.24733271E-02,-0.39631184E-04, 0.66662913E-04,-0.30279049E-03,
        -0.16840539E-02, 0.39803443E-03,-0.53214870E-03, 0.79969090E-03,
        -0.65580895E-03,-0.91868063E-03, 0.79885740E-05, 0.12638934E-04,
         0.56625425E-03,-0.46450598E-03,-0.50768972E-03,-0.13006983E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_500                                =v_p_e_dent_500                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_500                                =v_p_e_dent_500                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_500                                =v_p_e_dent_500                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        +coeff[ 31]*x11        *x41*x51
        ;

    return v_p_e_dent_500                                ;
}
float l_e_dent_500                                (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.5917114E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.58796359E-02, 0.23533840E+00,-0.84502436E-02,-0.17676480E-01,
         0.11209420E-01,-0.59062955E-02, 0.13366946E-02, 0.16005630E-03,
        -0.18402130E-02,-0.57656271E-02, 0.13700817E-02,-0.59688962E-02,
        -0.46641966E-02,-0.23099887E-02,-0.17833094E-02, 0.69949852E-03,
         0.24880908E-03,-0.10273677E-02,-0.18696171E-02,-0.58909116E-03,
         0.19998128E-03, 0.73237490E-03, 0.48740304E-03,-0.27663351E-03,
         0.49057353E-03, 0.18836347E-03,-0.56059397E-03, 0.10202102E-02,
         0.85376989E-03, 0.50407520E-03,-0.19204692E-03,-0.52715815E-02,
        -0.44366205E-02,-0.64115139E-03, 0.82211150E-03,-0.12551629E-02,
         0.60703058E-03, 0.53232600E-03,-0.49709686E-03,-0.11614105E-02,
        -0.84731134E-03, 0.22047870E-02, 0.26827801E-02,-0.16762153E-02,
        -0.78980741E-03, 0.63190685E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_500                                =v_l_e_dent_500                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_500                                =v_l_e_dent_500                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_dent_500                                =v_l_e_dent_500                                
        +coeff[ 26]*x12        *x41    
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x24        *x51
    ;
    v_l_e_dent_500                                =v_l_e_dent_500                                
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]*x12        *x43    
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]*x11*x21*x33    *x51
        +coeff[ 39]*x11*x21    *x42*x52
        +coeff[ 40]*x13*x21*x31*x41    
        +coeff[ 41]    *x22*x34    *x51
        +coeff[ 42]    *x21*x33*x41*x52
        +coeff[ 43]    *x21*x31*x43*x52
    ;
    v_l_e_dent_500                                =v_l_e_dent_500                                
        +coeff[ 44]*x11    *x34    *x52
        +coeff[ 45]*x12*x22*x33        
        ;

    return v_l_e_dent_500                                ;
}
float x_e_dent_450                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5100293E+01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
         0.21278298E-03, 0.43345564E-02,-0.12206709E+00,-0.58040232E-02,
         0.35717129E-02, 0.32773786E-02, 0.25556961E-02, 0.64783590E-03,
        -0.35052525E-03,-0.29509072E-03, 0.30235486E-03, 0.90916402E-03,
         0.11937764E-02, 0.38171571E-03,-0.14125484E-03,-0.18227246E-03,
         0.22178667E-02, 0.18079050E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_450                                =v_x_e_dent_450                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]            *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_450                                =v_x_e_dent_450                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_450                                ;
}
float t_e_dent_450                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3653322E+01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.78171276E-01,-0.35388891E-01, 0.77711523E+00, 0.21800838E+00,
         0.37077416E-01, 0.59918119E-02,-0.18489307E-01, 0.28596794E-01,
         0.47727693E-01, 0.16622182E-01, 0.18747384E-01,-0.77937841E-02,
        -0.14129717E-01,-0.13214796E-01,-0.10158408E-01, 0.25362936E-02,
        -0.63371886E-02,-0.29646850E-02,-0.21023026E-01, 0.73725227E-02,
         0.10128213E-02, 0.45302822E-02,-0.23239981E-01,-0.30926537E-01,
        -0.24164883E-01, 0.17609200E-03, 0.14546228E-03, 0.22247906E-02,
        -0.22830974E-03, 0.65208011E-03, 0.36536450E-02,-0.67129224E-02,
         0.38933268E-03, 0.72203134E-03,-0.96053770E-03, 0.39342768E-02,
         0.32506308E-02,-0.12674342E-02,-0.19409571E-01,-0.16551584E-01,
         0.27268101E-03, 0.14731612E-03,-0.77947427E-03,-0.72358444E-03,
        -0.19264505E-03, 0.22571646E-03,-0.59200829E-03, 0.92303392E-03,
        -0.29201948E-03,-0.80615273E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_450                                =v_t_e_dent_450                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_450                                =v_t_e_dent_450                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_450                                =v_t_e_dent_450                                
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]        *x32        
        +coeff[ 28]                *x52
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]*x12                
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_dent_450                                =v_t_e_dent_450                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x21    *x42*x51
        +coeff[ 37]    *x22        *x52
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x23*x32    *x51
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]*x11        *x42    
    ;
    v_t_e_dent_450                                =v_t_e_dent_450                                
        +coeff[ 44]*x11            *x52
        +coeff[ 45]*x11*x22*x31        
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x13    *x32        
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dent_450                                ;
}
float y_e_dent_450                                (float *x,int m){
    int ncoeff= 55;
    float avdat= -0.3663244E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 56]={
         0.35034298E-03, 0.12727976E+00, 0.37251059E-01, 0.11908892E-01,
         0.56811692E-02, 0.41328608E-02, 0.13283389E-04, 0.37996061E-02,
        -0.25880050E-02,-0.15925842E-02,-0.22761011E-02,-0.13991566E-02,
        -0.53869683E-03,-0.67608332E-03,-0.42554681E-03,-0.18127027E-03,
        -0.13115484E-02,-0.30335959E-03,-0.13471817E-03,-0.14745003E-03,
        -0.21502895E-03, 0.37890923E-03, 0.61494335E-04,-0.19654795E-02,
        -0.33957309E-02,-0.11625912E-02,-0.20199432E-02,-0.85557776E-03,
        -0.43316875E-03, 0.36795231E-04, 0.28454891E-04, 0.29307778E-03,
         0.12089054E-03,-0.21421632E-04, 0.18609523E-03,-0.23512152E-04,
         0.15584861E-03,-0.10679222E-03,-0.49593862E-04,-0.49493735E-03,
         0.45141463E-04,-0.52419794E-03,-0.28575672E-03, 0.40087431E-04,
         0.26548238E-04,-0.20022576E-05,-0.82078535E-04,-0.36094640E-04,
        -0.21780854E-04,-0.75702177E-04, 0.56597317E-04, 0.43686759E-04,
        -0.24017429E-04,-0.79316058E-04, 0.57190326E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11        *x41*x51
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]        *x31*x42*x51
        +coeff[ 32]    *x23*x31        
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]        *x32*x41*x51
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[ 35]            *x45*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x31*x44    
        +coeff[ 40]        *x33    *x51
        +coeff[ 41]        *x32*x43    
        +coeff[ 42]        *x33*x42    
        +coeff[ 43]            *x41*x53
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[ 44]        *x31    *x53
        +coeff[ 45]                *x51
        +coeff[ 46]    *x21*x31*x42    
        +coeff[ 47]    *x21*x33        
        +coeff[ 48]*x11*x22*x31        
        +coeff[ 49]        *x34*x41    
        +coeff[ 50]*x11*x21*x31*x42    
        +coeff[ 51]    *x23    *x41*x51
        +coeff[ 52]*x11*x23*x31        
    ;
    v_y_e_dent_450                                =v_y_e_dent_450                                
        +coeff[ 53]    *x24    *x41*x51
        +coeff[ 54]    *x22*x32*x41*x51
        ;

    return v_y_e_dent_450                                ;
}
float p_e_dent_450                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.2635041E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.25772149E-03,-0.63357398E-01,-0.55002891E-01,-0.16851721E-01,
        -0.25970344E-01, 0.69020563E-02, 0.11877011E-01,-0.17416807E-01,
         0.80419850E-03,-0.88723544E-02, 0.10751287E-02,-0.11060654E-04,
         0.79533621E-03,-0.33583797E-02,-0.21536190E-02, 0.10609478E-02,
         0.25186129E-02,-0.49969327E-04, 0.65360735E-04,-0.30747443E-03,
        -0.16760349E-02, 0.41145214E-03,-0.54827286E-03, 0.77269558E-03,
        -0.65413309E-03,-0.91130822E-03,-0.76459519E-05, 0.56920352E-03,
        -0.44090996E-03,-0.50435809E-03,-0.12998002E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_450                                =v_p_e_dent_450                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_450                                =v_p_e_dent_450                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_450                                =v_p_e_dent_450                                
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11        *x41*x51
        ;

    return v_p_e_dent_450                                ;
}
float l_e_dent_450                                (float *x,int m){
    int ncoeff= 43;
    float avdat= -0.4409448E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
         0.42890180E-02, 0.23587911E+00,-0.82983114E-02,-0.17452614E-01,
         0.11114973E-01,-0.59693567E-02, 0.17746270E-02, 0.38776526E-03,
        -0.14730658E-02,-0.62499973E-02, 0.12322890E-02,-0.50214333E-02,
        -0.37298047E-02,-0.31754894E-02,-0.37407913E-03, 0.84185554E-03,
        -0.62375721E-02,-0.12265112E-02, 0.14854960E-03,-0.22217857E-02,
        -0.98492415E-03,-0.10381051E-02,-0.74008436E-04,-0.12576373E-02,
         0.69977436E-03, 0.40117206E-03, 0.23258124E-03,-0.33583213E-03,
        -0.20488814E-03, 0.47597746E-03, 0.39466165E-03, 0.76460012E-03,
        -0.30595262E-03, 0.62914647E-03, 0.15172061E-02, 0.92404149E-03,
         0.68309810E-03,-0.60970220E-03,-0.10459006E-02, 0.10565394E-02,
        -0.12187947E-02, 0.13016104E-02,-0.12356457E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dent_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_450                                =v_l_e_dent_450                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x23*x34        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x31*x41    
    ;
    v_l_e_dent_450                                =v_l_e_dent_450                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]                *x51
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23*x32        
        +coeff[ 22]*x12                
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_l_e_dent_450                                =v_l_e_dent_450                                
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]*x11    *x31*x42    
        +coeff[ 31]    *x21*x32    *x52
        +coeff[ 32]*x11*x22    *x41*x51
        +coeff[ 33]*x11    *x31*x42*x51
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_dent_450                                =v_l_e_dent_450                                
        +coeff[ 35]*x11*x21*x32*x42    
        +coeff[ 36]*x12    *x31*x43    
        +coeff[ 37]*x13    *x32    *x51
        +coeff[ 38]    *x22*x34    *x51
        +coeff[ 39]        *x34*x42*x51
        +coeff[ 40]*x11*x23*x31*x41*x51
        +coeff[ 41]*x11*x22*x31*x41*x52
        +coeff[ 42]*x12*x21*x32*x42    
        ;

    return v_l_e_dent_450                                ;
}
float x_e_dent_449                                (float *x,int m){
    int ncoeff= 20;
    float avdat= -0.5099966E+01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
        -0.29939724E-03, 0.42680688E-02,-0.12314987E+00,-0.56932997E-02,
         0.36553356E-02, 0.34812063E-02, 0.24803237E-02, 0.85620029E-03,
        -0.33753840E-03,-0.30430878E-03, 0.23405171E-04,-0.31981392E-04,
         0.34408967E-03, 0.88955410E-03, 0.14074506E-02,-0.14127480E-03,
         0.35288333E-03,-0.17175241E-03, 0.26543031E-02, 0.19272129E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_449                                =v_x_e_dent_449                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x23        *x52
        +coeff[ 11]                *x51
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dent_449                                =v_x_e_dent_449                                
        +coeff[ 17]*x12    *x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        ;

    return v_x_e_dent_449                                ;
}
float t_e_dent_449                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3651805E+01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.75435072E-01,-0.35037030E-01, 0.78521883E+00, 0.22230822E+00,
         0.36047604E-01, 0.60250657E-02,-0.18297061E-01, 0.29539254E-01,
         0.48732881E-01, 0.19849680E-01, 0.18593816E-01,-0.78428676E-02,
        -0.21562634E-01,-0.14049112E-01,-0.95648132E-02, 0.24473879E-02,
        -0.95649967E-02,-0.28375196E-02,-0.21575579E-01, 0.78125978E-02,
        -0.21086856E-03, 0.43456834E-02,-0.26284885E-01,-0.25482052E-02,
        -0.30828414E-01,-0.22815341E-01,-0.11232488E-03, 0.15332815E-03,
        -0.33548332E-03, 0.41629997E-03, 0.31581647E-02, 0.65892807E-03,
         0.88449725E-03, 0.40335804E-02,-0.87640593E-02,-0.10345521E-02,
        -0.11723808E-02,-0.22785610E-02, 0.40271222E-02, 0.29190546E-02,
        -0.94818426E-02,-0.10194684E-01, 0.31184519E-03,-0.28916617E-03,
         0.25490663E-03,-0.15029812E-03,-0.65079797E-03,-0.64207055E-03,
        -0.10730810E-02, 0.12801586E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_dent_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_449                                =v_t_e_dent_449                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_449                                =v_t_e_dent_449                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]        *x32*x42    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_449                                =v_t_e_dent_449                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x33*x41*x51
        +coeff[ 28]    *x22*x32    *x52
        +coeff[ 29]*x12                
        +coeff[ 30]        *x32        
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]        *x32    *x51
        +coeff[ 33]        *x31*x41*x51
        +coeff[ 34]    *x22*x32        
    ;
    v_t_e_dent_449                                =v_t_e_dent_449                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]*x11*x23            
        +coeff[ 37]        *x31*x43    
        +coeff[ 38]    *x21*x31*x41*x51
        +coeff[ 39]    *x21    *x42*x51
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]    *x21*x31*x43    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]                *x52
    ;
    v_t_e_dent_449                                =v_t_e_dent_449                                
        +coeff[ 44]*x12*x21            
        +coeff[ 45]    *x22*x31        
        +coeff[ 46]*x11    *x31*x41    
        +coeff[ 47]*x11        *x42    
        +coeff[ 48]        *x33*x41    
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_dent_449                                ;
}
float y_e_dent_449                                (float *x,int m){
    int ncoeff= 64;
    float avdat= -0.8183771E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 65]={
         0.82350220E-03, 0.12890138E+00, 0.42185191E-01, 0.11790148E-01,
         0.62069367E-02, 0.45474800E-02, 0.19806654E-04, 0.37016026E-02,
        -0.26232342E-02,-0.17997852E-02,-0.25942212E-02,-0.16967462E-02,
        -0.66923432E-03,-0.46719372E-03,-0.12004826E-03,-0.13300888E-02,
        -0.41665952E-03,-0.55100996E-03,-0.26433784E-03,-0.12885439E-03,
        -0.15845230E-03, 0.39808566E-03, 0.72114117E-05, 0.14604689E-03,
        -0.31900543E-02,-0.11648276E-02,-0.22952228E-02,-0.94051776E-03,
        -0.36378822E-03,-0.13057895E-02, 0.84053660E-04,-0.38696264E-04,
        -0.13704645E-03, 0.38648719E-04, 0.30494377E-04,-0.12238747E-03,
         0.32199526E-03,-0.15125867E-04, 0.24467317E-03,-0.16902060E-02,
        -0.53204520E-03,-0.18231672E-05,-0.46552603E-04,-0.44786939E-04,
         0.60202390E-04,-0.49475068E-03,-0.12074027E-02, 0.75013697E-04,
         0.56190896E-04,-0.45769068E-03,-0.67680361E-04, 0.10430945E-03,
        -0.32161851E-03,-0.21329317E-04,-0.28359852E-03, 0.10849620E-03,
        -0.14040183E-03, 0.15655914E-03, 0.25202884E-04, 0.65342741E-04,
         0.54924854E-04,-0.39025217E-04, 0.36415655E-04,-0.57762125E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x45    
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 17]    *x21    *x41*x51
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]    *x22*x31*x44    
        +coeff[ 30]    *x24    *x43    
        +coeff[ 31]    *x24*x33        
        +coeff[ 32]    *x24    *x45    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]*x11    *x31    *x51
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21*x33        
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]        *x32*x43    
        +coeff[ 46]    *x22*x32*x43    
        +coeff[ 47]        *x34*x43    
        +coeff[ 48]            *x43*x53
        +coeff[ 49]    *x22*x33*x42    
        +coeff[ 50]    *x21*x31*x42    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]        *x31*x44    
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 53]*x11*x22*x31        
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x41*x51
        +coeff[ 61]*x11*x23*x31        
    ;
    v_y_e_dent_449                                =v_y_e_dent_449                                
        +coeff[ 62]    *x23*x31    *x51
        +coeff[ 63]    *x24    *x41*x51
        ;

    return v_y_e_dent_449                                ;
}
float p_e_dent_449                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.1045996E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.11001787E-03,-0.69240965E-01,-0.53128585E-01,-0.18805638E-01,
        -0.26066160E-01, 0.75564575E-02, 0.11730725E-01,-0.17660975E-01,
         0.88536995E-03,-0.99992529E-02, 0.10538519E-02,-0.37708114E-05,
         0.86029846E-03,-0.37083898E-02,-0.21491956E-02, 0.10453678E-02,
         0.25213638E-02,-0.78354926E-04, 0.71516020E-04,-0.41497417E-03,
        -0.20396423E-02, 0.42851130E-03,-0.58668503E-03, 0.84144977E-03,
        -0.64864638E-03,-0.90638147E-03, 0.20922240E-04,-0.12169668E-04,
         0.62541914E-03,-0.49582601E-03,-0.57080883E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_449                                =v_p_e_dent_449                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_449                                =v_p_e_dent_449                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_449                                =v_p_e_dent_449                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        ;

    return v_p_e_dent_449                                ;
}
float l_e_dent_449                                (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.4961212E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.53021344E-02, 0.23822616E+00,-0.81877848E-02,-0.17853277E-01,
         0.10583614E-01,-0.58626919E-02,-0.78508107E-04,-0.19840163E-02,
        -0.61015999E-02, 0.12760673E-02,-0.49333610E-02,-0.52443999E-02,
        -0.19195878E-02, 0.55513991E-03,-0.13161964E-02, 0.49201306E-03,
         0.10636324E-03,-0.24136726E-02, 0.88179251E-03,-0.70936829E-02,
        -0.42238403E-02, 0.96313648E-04, 0.49352885E-03,-0.18083358E-02,
         0.71742130E-03, 0.52582094E-03,-0.83969889E-03,-0.19763997E-03,
         0.29395302E-03,-0.61278214E-03,-0.55802747E-03, 0.32482453E-03,
         0.39472984E-03,-0.72989415E-03, 0.74704131E-03,-0.48519070E-02,
         0.33170549E-03, 0.71388995E-03, 0.64772210E-03, 0.67906495E-03,
        -0.23270946E-02,-0.58000616E-03,-0.34928692E-02,-0.60005073E-03,
        -0.84652839E-03, 0.91344217E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_dent_449                                =v_l_e_dent_449                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23        *x52
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_449                                =v_l_e_dent_449                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]            *x41    
        +coeff[ 22]    *x21*x31        
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_l_e_dent_449                                =v_l_e_dent_449                                
        +coeff[ 26]    *x21        *x52
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x12    *x31        
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]*x11*x22        *x51
        +coeff[ 33]    *x22*x33        
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_l_e_dent_449                                =v_l_e_dent_449                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]        *x34    *x51
        +coeff[ 37]*x11*x21*x32*x41    
        +coeff[ 38]    *x24*x31*x41    
        +coeff[ 39]    *x21*x33*x41*x51
        +coeff[ 40]    *x24*x32*x41    
        +coeff[ 41]    *x23*x32*x42    
        +coeff[ 42]    *x21*x32*x44    
        +coeff[ 43]    *x23*x31    *x53
    ;
    v_l_e_dent_449                                =v_l_e_dent_449                                
        +coeff[ 44]    *x21*x32    *x54
        +coeff[ 45]*x11*x22*x31*x43    
        ;

    return v_l_e_dent_449                                ;
}
float x_e_dent_400                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5101468E+01;
    float xmin[10]={
        -0.39997E-02,-0.60022E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59995E-01, 0.30032E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
         0.14030044E-02, 0.42556939E-02,-0.12366445E+00,-0.56791031E-02,
         0.36718545E-02, 0.35164764E-02, 0.24802962E-02, 0.77005842E-03,
        -0.33622212E-03,-0.29209949E-03, 0.30222410E-03,-0.20022590E-04,
         0.92295889E-03, 0.14471969E-02,-0.14686173E-03, 0.38417059E-03,
        -0.19219726E-03, 0.25427530E-02, 0.18792113E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_400                                =v_x_e_dent_400                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42    
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_x_e_dent_400                                =v_x_e_dent_400                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_400                                ;
}
float t_e_dent_400                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3662347E+01;
    float xmin[10]={
        -0.39997E-02,-0.60022E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59995E-01, 0.30032E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.87288134E-01,-0.34936018E-01, 0.78783762E+00, 0.22411835E+00,
         0.35692375E-01, 0.59387917E-02,-0.18304586E-01, 0.29444279E-01,
         0.49884297E-01, 0.19051969E-01, 0.18755665E-01,-0.79069780E-02,
        -0.14665062E-01,-0.12624234E-01,-0.11977454E-01, 0.24692644E-02,
        -0.82907490E-02,-0.28753383E-02,-0.25985513E-01,-0.21145938E-01,
         0.75879497E-02, 0.45410842E-02,-0.89918952E-02,-0.34291409E-01,
        -0.23901591E-01, 0.21875123E-03,-0.17580368E-03, 0.43285772E-03,
         0.31154552E-02,-0.30208481E-03, 0.41412222E-02, 0.65311725E-03,
         0.89583523E-03,-0.12960612E-02, 0.43015871E-02, 0.29976293E-02,
        -0.10915484E-02,-0.23682088E-01,-0.18220387E-01,-0.49423288E-04,
         0.29584809E-03,-0.68675616E-03,-0.45634821E-03,-0.12753289E-02,
        -0.12122499E-02, 0.16008435E-02, 0.45512791E-03,-0.11679944E-01,
        -0.89172722E-03,-0.47159175E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_400                                =v_t_e_dent_400                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_400                                =v_t_e_dent_400                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_400                                =v_t_e_dent_400                                
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]*x12                
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]        *x32    *x51
        +coeff[ 33]*x11*x23            
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dent_400                                =v_t_e_dent_400                                
        +coeff[ 35]    *x21    *x42*x51
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x21*x31*x43    
        +coeff[ 39]    *x23*x32    *x51
        +coeff[ 40]*x12*x21            
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]*x11        *x42    
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_dent_400                                =v_t_e_dent_400                                
        +coeff[ 44]        *x31*x43    
        +coeff[ 45]    *x21*x32    *x51
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22    *x42*x51
        +coeff[ 49]*x11        *x42*x52
        ;

    return v_t_e_dent_400                                ;
}
float y_e_dent_400                                (float *x,int m){
    int ncoeff= 62;
    float avdat= -0.6968484E-03;
    float xmin[10]={
        -0.39997E-02,-0.60022E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59995E-01, 0.30032E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 63]={
         0.69568405E-03, 0.12875798E+00, 0.42010721E-01, 0.11769987E-01,
         0.62086275E-02, 0.45755268E-02, 0.11975701E-04, 0.37343046E-02,
        -0.26526982E-02,-0.17960015E-02,-0.27776607E-02,-0.18183753E-02,
        -0.66452444E-03,-0.46566283E-03,-0.16200484E-04,-0.15815040E-03,
        -0.14376353E-02,-0.42356091E-03,-0.54623326E-03,-0.24643395E-03,
         0.27456783E-05,-0.13005735E-03, 0.40428914E-03, 0.58299258E-04,
         0.14961851E-03,-0.28773174E-02,-0.12102635E-02,-0.21601650E-02,
        -0.99182921E-03,-0.39956061E-03,-0.19115900E-02, 0.31339761E-03,
         0.36701564E-04,-0.43948737E-03, 0.35401921E-04, 0.29716901E-04,
         0.31618634E-03, 0.15161546E-04, 0.23443530E-03,-0.16447204E-02,
        -0.56831917E-03,-0.15165287E-04, 0.17234878E-03,-0.12174646E-03,
        -0.47425623E-04, 0.59265803E-04,-0.12199399E-03,-0.16696509E-02,
        -0.58557914E-03,-0.86761924E-04,-0.40678802E-04,-0.24311052E-04,
        -0.12877937E-03,-0.61624261E-04, 0.30781539E-04, 0.47836784E-04,
         0.27201128E-04, 0.42311913E-04,-0.25579566E-04,-0.10040756E-03,
        -0.67608678E-04,-0.60881150E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[ 17]        *x33        
        +coeff[ 18]    *x21    *x41*x51
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x24*x31        
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x31*x44    
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24*x33        
        +coeff[ 33]    *x24    *x45    
        +coeff[ 34]*x11        *x41*x51
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]            *x43*x51
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]        *x32*x43    
        +coeff[ 47]    *x22*x32*x43    
        +coeff[ 48]    *x22*x33*x42    
        +coeff[ 49]    *x21*x31*x42    
        +coeff[ 50]    *x21*x33        
        +coeff[ 51]*x11*x22*x31        
        +coeff[ 52]        *x33*x42    
    ;
    v_y_e_dent_400                                =v_y_e_dent_400                                
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]        *x31    *x53
        +coeff[ 57]    *x23    *x41*x51
        +coeff[ 58]*x11*x23*x31        
        +coeff[ 59]    *x22    *x43*x51
        +coeff[ 60]    *x22*x31*x42*x51
        +coeff[ 61]    *x24    *x41*x51
        ;

    return v_y_e_dent_400                                ;
}
float p_e_dent_400                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.3950945E-04;
    float xmin[10]={
        -0.39997E-02,-0.60022E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59995E-01, 0.30032E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.45667261E-04,-0.69278449E-01,-0.53170789E-01,-0.18838249E-01,
        -0.26130440E-01, 0.75524705E-02, 0.11725488E-01,-0.17815212E-01,
         0.90133830E-03,-0.10060394E-01, 0.10413895E-02, 0.16026266E-05,
         0.85119245E-03,-0.37013208E-02,-0.21376556E-02, 0.10587757E-02,
         0.26013246E-02,-0.29392859E-04, 0.93225804E-04,-0.40111624E-03,
        -0.20682777E-02, 0.43587908E-03,-0.60858246E-03, 0.86266495E-03,
        -0.65701886E-03,-0.87194564E-03, 0.16161341E-04,-0.28043532E-04,
         0.62984484E-03,-0.47961835E-03,-0.54694019E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_400                                =v_p_e_dent_400                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_400                                =v_p_e_dent_400                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_400                                =v_p_e_dent_400                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        ;

    return v_p_e_dent_400                                ;
}
float l_e_dent_400                                (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.2110900E-02;
    float xmin[10]={
        -0.39997E-02,-0.60022E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59995E-01, 0.30032E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.21784485E-02, 0.23867969E+00,-0.83166724E-02,-0.18408652E-01,
         0.10898276E-01,-0.61717997E-02, 0.20750123E-02,-0.34044334E-03,
        -0.22869373E-02,-0.63567050E-02, 0.13646227E-02,-0.15556520E-02,
        -0.12433201E-02,-0.14848815E-02,-0.35904734E-02, 0.55693544E-03,
        -0.10509175E-02, 0.16734796E-03,-0.14630130E-02, 0.57300628E-03,
        -0.65414677E-02, 0.41826838E-03,-0.40856164E-03,-0.43620321E-03,
        -0.29587836E-02,-0.11587800E-03,-0.84172888E-03, 0.31219967E-03,
        -0.19985759E-03, 0.27589113E-03, 0.10614359E-02, 0.13973893E-02,
         0.15537598E-02, 0.43336098E-03,-0.78203576E-02,-0.62810145E-02,
         0.47814750E-03, 0.68614114E-03,-0.10836174E-01,-0.74036401E-02,
        -0.26846689E-02,-0.10669112E-02, 0.79153897E-03,-0.16733448E-02,
         0.71336341E-03, 0.22621448E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_dent_400                                =v_l_e_dent_400                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21        *x52
    ;
    v_l_e_dent_400                                =v_l_e_dent_400                                
        +coeff[ 17]                *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x23        *x52
        +coeff[ 22]    *x23*x33*x41    
        +coeff[ 23]    *x21*x34*x42    
        +coeff[ 24]    *x23*x31*x43    
        +coeff[ 25]*x11    *x31        
    ;
    v_l_e_dent_400                                =v_l_e_dent_400                                
        +coeff[ 26]    *x23            
        +coeff[ 27]        *x32    *x51
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]*x11            *x52
        +coeff[ 30]    *x22*x32        
        +coeff[ 31]    *x22    *x42    
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]*x11    *x32    *x51
        +coeff[ 34]    *x23*x31*x41    
    ;
    v_l_e_dent_400                                =v_l_e_dent_400                                
        +coeff[ 35]    *x21*x33*x41    
        +coeff[ 36]        *x34*x41    
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x21    *x44    
        +coeff[ 41]    *x22    *x42*x51
        +coeff[ 42]*x12    *x31*x41*x51
        +coeff[ 43]    *x21*x31*x43*x51
    ;
    v_l_e_dent_400                                =v_l_e_dent_400                                
        +coeff[ 44]    *x23        *x53
        +coeff[ 45]    *x22    *x44*x51
        ;

    return v_l_e_dent_400                                ;
}
float x_e_dent_350                                (float *x,int m){
    int ncoeff= 20;
    float avdat= -0.5101354E+01;
    float xmin[10]={
        -0.39987E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
         0.10011934E-02, 0.42429948E-02,-0.39029019E-04,-0.12378497E+00,
        -0.56411689E-02, 0.37027001E-02, 0.35027417E-02, 0.24868224E-02,
         0.78210194E-03,-0.34392689E-03,-0.29864596E-03, 0.14912636E-04,
         0.34715532E-03, 0.88918151E-03, 0.14322118E-02,-0.13263113E-03,
         0.35349638E-03,-0.17234779E-03, 0.24830678E-02, 0.18117881E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_350                                =v_x_e_dent_350                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dent_350                                =v_x_e_dent_350                                
        +coeff[ 17]*x12    *x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        ;

    return v_x_e_dent_350                                ;
}
float t_e_dent_350                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3660800E+01;
    float xmin[10]={
        -0.39987E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.83854251E-01,-0.34782495E-01, 0.79037941E+00, 0.22520694E+00,
         0.35597228E-01, 0.60394155E-02,-0.18305499E-01, 0.29044056E-01,
         0.49902890E-01, 0.18809743E-01, 0.18457685E-01,-0.79603093E-02,
        -0.21398265E-01,-0.14016328E-01,-0.98098144E-02, 0.24763325E-02,
        -0.94751073E-02,-0.28306502E-02,-0.21415386E-01, 0.77216467E-02,
         0.30074758E-04,-0.44794742E-04, 0.44601937E-02,-0.25765268E-01,
        -0.30509381E-01,-0.22478804E-01, 0.12543846E-03,-0.34473295E-03,
         0.28029273E-02,-0.30303511E-03, 0.43513170E-02,-0.87366356E-02,
         0.41638792E-03, 0.62948238E-03, 0.91793033E-03,-0.12661114E-02,
         0.46635978E-02, 0.33595453E-02,-0.11163091E-02,-0.99023804E-02,
        -0.10724155E-01,-0.82090514E-06, 0.15080687E-03, 0.27996732E-03,
        -0.77298773E-03,-0.71276189E-03,-0.21688452E-03,-0.27252806E-03,
        -0.70096971E-03, 0.15919651E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_350                                =v_t_e_dent_350                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_350                                =v_t_e_dent_350                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_350                                =v_t_e_dent_350                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]*x12                
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_dent_350                                =v_t_e_dent_350                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]*x12*x21*x32    *x51
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_dent_350                                =v_t_e_dent_350                                
        +coeff[ 44]*x11    *x31*x41    
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]*x11            *x52
        +coeff[ 47]*x11*x22    *x41    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_dent_350                                ;
}
float y_e_dent_350                                (float *x,int m){
    int ncoeff= 61;
    float avdat=  0.9151949E-03;
    float xmin[10]={
        -0.39987E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 62]={
        -0.89466013E-03, 0.12839796E+00, 0.41780166E-01, 0.11781266E-01,
         0.62036486E-02, 0.45772544E-02, 0.11846353E-04, 0.37243678E-02,
        -0.26791643E-02,-0.18106846E-02,-0.27945621E-02,-0.18778103E-02,
        -0.55227947E-03,-0.66543324E-03,-0.46456043E-03, 0.23516766E-04,
        -0.15694338E-03,-0.14809701E-02,-0.43197809E-03,-0.26393082E-03,
         0.39114270E-05,-0.13348204E-03, 0.40020628E-03, 0.60238544E-04,
         0.14106611E-03,-0.26549937E-02,-0.12079776E-02,-0.97130623E-03,
        -0.76967408E-03,-0.22988704E-02,-0.12181323E-02, 0.62949221E-04,
        -0.29314481E-03, 0.23004333E-04,-0.10473505E-04, 0.37129157E-04,
         0.30030398E-04, 0.32248170E-03,-0.77153752E-04, 0.21673908E-03,
        -0.12993140E-02,-0.19031861E-02,-0.52596990E-03, 0.17977174E-04,
        -0.20890424E-02,-0.12409290E-03,-0.43478165E-04,-0.25451416E-04,
         0.58240224E-04, 0.32644768E-05,-0.85822234E-04, 0.12335165E-03,
        -0.45187200E-04, 0.30196750E-04, 0.44772052E-04, 0.27234535E-04,
         0.54650744E-04,-0.42348118E-04, 0.33788587E-04, 0.22239066E-04,
         0.64267617E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]    *x22*x32*x43    
        +coeff[ 30]    *x22*x33*x42    
        +coeff[ 31]    *x24*x32*x41    
        +coeff[ 32]    *x22*x34*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x22*x32*x45    
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[ 35]*x11        *x41*x51
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x22    *x41*x51
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]            *x45*x51
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[ 44]    *x22*x31*x44    
        +coeff[ 45]    *x21*x32*x41    
        +coeff[ 46]*x11*x22    *x41    
        +coeff[ 47]*x11*x22*x31        
        +coeff[ 48]        *x33    *x51
        +coeff[ 49]                *x51
        +coeff[ 50]    *x21*x31*x42    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]    *x21*x33        
    ;
    v_y_e_dent_350                                =v_y_e_dent_350                                
        +coeff[ 53]            *x41*x53
        +coeff[ 54]*x11*x21*x31*x42    
        +coeff[ 55]        *x31    *x53
        +coeff[ 56]    *x23    *x41*x51
        +coeff[ 57]*x11*x23*x31        
        +coeff[ 58]    *x23*x31    *x51
        +coeff[ 59]*x13*x21*x31        
        +coeff[ 60]    *x22*x32*x41*x51
        ;

    return v_y_e_dent_350                                ;
}
float p_e_dent_350                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.6359859E-03;
    float xmin[10]={
        -0.39987E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.60632342E-03,-0.69377139E-01,-0.53293929E-01,-0.18906819E-01,
        -0.26209014E-01, 0.75433520E-02, 0.11707403E-01,-0.17772324E-01,
         0.90535294E-03,-0.10068098E-01, 0.10506074E-02,-0.66450951E-06,
         0.85227436E-03,-0.36650621E-02,-0.21154748E-02, 0.10376144E-02,
         0.25608263E-02,-0.80688129E-04,-0.40723683E-03,-0.20142859E-02,
         0.43296459E-03, 0.65967091E-03,-0.61467401E-03, 0.83373376E-03,
        -0.63882919E-03,-0.85172732E-03, 0.41780113E-04,-0.19284869E-04,
        -0.49172144E-03,-0.52792771E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_350                                =v_p_e_dent_350                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_350                                =v_p_e_dent_350                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]    *x21    *x41*x51
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_350                                =v_p_e_dent_350                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x22*x31    *x51
        ;

    return v_p_e_dent_350                                ;
}
float l_e_dent_350                                (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2369720E-02;
    float xmin[10]={
        -0.39987E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.28568199E-02, 0.23939106E+00,-0.80793574E-02,-0.18133176E-01,
         0.10892209E-01,-0.61978735E-02, 0.20042653E-02, 0.57162146E-04,
        -0.20841321E-02,-0.68686018E-02, 0.12756485E-02,-0.60916143E-02,
        -0.51210993E-02,-0.13830216E-02,-0.15202899E-02, 0.67716726E-03,
        -0.14586248E-02,-0.30320738E-02,-0.79300138E-03,-0.71321199E-04,
         0.12891089E-03, 0.85911603E-03,-0.33139129E-03, 0.13787915E-02,
         0.78642875E-03,-0.51572719E-02,-0.39049836E-02,-0.14574073E-02,
         0.14088895E-03, 0.93696750E-03, 0.23198661E-02,-0.10955642E-02,
        -0.70749648E-03, 0.72173326E-03, 0.37122951E-03, 0.29354615E-02,
         0.21272285E-02,-0.24882280E-02, 0.35040900E-02,-0.18274763E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_350                                =v_l_e_dent_350                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dent_350                                =v_l_e_dent_350                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]        *x31        
        +coeff[ 20]                *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]*x12    *x31*x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dent_350                                =v_l_e_dent_350                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x34    *x51
        +coeff[ 29]        *x32*x42*x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x21*x33        
        +coeff[ 32]*x12    *x31    *x52
        +coeff[ 33]*x12*x22*x32        
        +coeff[ 34]    *x22*x33    *x52
    ;
    v_l_e_dent_350                                =v_l_e_dent_350                                
        +coeff[ 35]        *x33*x42*x52
        +coeff[ 36]        *x32*x43*x52
        +coeff[ 37]*x11*x23*x33        
        +coeff[ 38]*x11*x21*x33    *x52
        +coeff[ 39]*x11*x21*x31    *x54
        ;

    return v_l_e_dent_350                                ;
}
float x_e_dent_300                                (float *x,int m){
    int ncoeff= 22;
    float avdat= -0.5100771E+01;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 23]={
         0.62040472E-03,-0.12458167E+00,-0.56239725E-02, 0.23729222E-05,
         0.42222291E-02, 0.37090455E-02, 0.34929749E-02, 0.24729085E-02,
         0.75156725E-03,-0.33650955E-03,-0.30135631E-03, 0.21202338E-03,
        -0.30062542E-04, 0.35545009E-03, 0.14172733E-02, 0.18696286E-02,
        -0.34349711E-03,-0.14262552E-03, 0.83003426E-03,-0.25223664E-03,
         0.27256971E-03, 0.27058686E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_dent_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_300                                =v_x_e_dent_300                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]                *x51
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x23    *x42    
        +coeff[ 16]    *x23*x33*x41    
    ;
    v_x_e_dent_300                                =v_x_e_dent_300                                
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x21        *x54
        +coeff[ 21]    *x23*x31*x41    
        ;

    return v_x_e_dent_300                                ;
}
float t_e_dent_300                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3658047E+01;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.82423225E-01,-0.34637410E-01, 0.79584265E+00, 0.22863719E+00,
         0.35329215E-01, 0.60026599E-02,-0.18326474E-01, 0.29209094E-01,
         0.51079009E-01, 0.18724883E-01, 0.18355269E-01,-0.79441993E-02,
        -0.24197049E-01,-0.14769476E-01,-0.85313274E-02, 0.24414570E-02,
        -0.10728694E-01,-0.28558201E-02,-0.22030331E-01, 0.75239707E-02,
        -0.56134601E-03,-0.12673367E-02, 0.44951751E-02,-0.25376394E-01,
        -0.27335608E-01,-0.21207685E-01,-0.64992078E-05,-0.32203316E-04,
         0.30878242E-02,-0.26223078E-03, 0.42243185E-02,-0.89286296E-02,
         0.37455553E-03, 0.57625689E-03, 0.87424065E-03,-0.12795915E-02,
         0.40806411E-02, 0.31650825E-02,-0.11530244E-02, 0.26679114E-02,
        -0.36037180E-02,-0.37307490E-03, 0.24398696E-03,-0.82518556E-03,
        -0.75203867E-03,-0.19362025E-02,-0.17719505E-02, 0.14910890E-02,
         0.45747650E-03,-0.33668915E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_300                                =v_t_e_dent_300                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_300                                =v_t_e_dent_300                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]*x12*x22*x31*x41    
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_300                                =v_t_e_dent_300                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]        *x31*x41*x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]*x12                
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_dent_300                                =v_t_e_dent_300                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21*x33*x41    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]    *x23*x32    *x51
        +coeff[ 42]*x12*x21            
        +coeff[ 43]*x11    *x31*x41    
    ;
    v_t_e_dent_300                                =v_t_e_dent_300                                
        +coeff[ 44]*x11        *x42    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x13    *x32        
        ;

    return v_t_e_dent_300                                ;
}
float y_e_dent_300                                (float *x,int m){
    int ncoeff= 64;
    float avdat=  0.5497286E-03;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 65]={
        -0.54990087E-03, 0.12815495E+00, 0.41472245E-01, 0.11785702E-01,
         0.62103611E-02, 0.46244836E-02, 0.30630297E-04, 0.37694748E-02,
        -0.28033000E-02,-0.19658527E-02,-0.28276236E-02,-0.18912674E-02,
        -0.55419147E-03,-0.66428381E-03,-0.34659595E-04, 0.84848216E-05,
        -0.15941376E-03,-0.14404907E-02,-0.43595812E-03,-0.25520366E-03,
        -0.52380246E-05,-0.12847644E-03,-0.46778278E-03, 0.40238921E-03,
         0.65872126E-04, 0.13990044E-03,-0.12878473E-02,-0.11029126E-02,
        -0.78256219E-03,-0.60481613E-03,-0.38329833E-02,-0.22391030E-02,
        -0.94402645E-03,-0.61748660E-03,-0.23030983E-03, 0.35124605E-04,
         0.35164423E-04, 0.29629835E-04, 0.31007690E-03,-0.54441040E-04,
         0.23308548E-03,-0.11453022E-02,-0.99004770E-03,-0.34868511E-03,
         0.68749000E-05,-0.31845125E-02,-0.59712864E-04,-0.10911048E-03,
        -0.42124459E-04,-0.42931439E-04, 0.54374377E-04, 0.95859541E-04,
        -0.78584570E-04, 0.66900306E-04,-0.14551331E-02,-0.66499010E-03,
         0.25817935E-04,-0.68517415E-05, 0.12737728E-03,-0.17847711E-04,
         0.27014950E-04, 0.23372639E-04,-0.35636647E-04, 0.38239730E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x33    *x52
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]        *x31    *x52
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23*x31        
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x24*x31        
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x32*x43    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]    *x24*x32*x41    
        +coeff[ 33]    *x22*x34*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 35]    *x22*x32*x45    
        +coeff[ 36]*x11        *x41*x51
        +coeff[ 37]*x11    *x31    *x51
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x22    *x41*x51
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 44]            *x45*x51
        +coeff[ 45]    *x22*x31*x44    
        +coeff[ 46]    *x21*x31*x42    
        +coeff[ 47]    *x21*x32*x41    
        +coeff[ 48]    *x21*x33        
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]*x11*x21*x31*x42    
        +coeff[ 52]*x11*x23*x31        
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 53]*x11*x21    *x45    
        +coeff[ 54]    *x24*x31*x42    
        +coeff[ 55]    *x24    *x45    
        +coeff[ 56]*x11*x21*x31        
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]            *x43*x51
        +coeff[ 59]*x11*x22*x31        
        +coeff[ 60]            *x41*x53
        +coeff[ 61]        *x31    *x53
    ;
    v_y_e_dent_300                                =v_y_e_dent_300                                
        +coeff[ 62]*x11*x23    *x41    
        +coeff[ 63]    *x23    *x41*x51
        ;

    return v_y_e_dent_300                                ;
}
float p_e_dent_300                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2228675E-03;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.21506743E-03,-0.69455422E-01,-0.53409301E-01,-0.19013308E-01,
        -0.26361613E-01, 0.75402232E-02, 0.11722950E-01,-0.18041471E-01,
         0.90983434E-03,-0.10178785E-01, 0.10373247E-02,-0.90630101E-05,
         0.85105299E-03,-0.36654179E-02,-0.21204220E-02, 0.10494753E-02,
         0.26552547E-02,-0.82122142E-05, 0.85882566E-04,-0.40133204E-03,
        -0.20625899E-02, 0.45507884E-03,-0.63699216E-03, 0.83247654E-03,
        -0.65649563E-03,-0.86020550E-03, 0.12285794E-04, 0.10832167E-04,
         0.62260602E-03,-0.51054644E-03,-0.52004517E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_300                                =v_p_e_dent_300                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_300                                =v_p_e_dent_300                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_300                                =v_p_e_dent_300                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        ;

    return v_p_e_dent_300                                ;
}
float l_e_dent_300                                (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.3477128E-02;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.35468508E-02, 0.24060120E+00,-0.81488863E-02,-0.18135566E-01,
         0.10900905E-01,-0.57943938E-02, 0.54593933E-04,-0.19703270E-02,
        -0.58722766E-02, 0.13610085E-02,-0.37857518E-02,-0.22873853E-02,
        -0.18054618E-02, 0.64045365E-03,-0.16236760E-02, 0.11789759E-03,
        -0.16335354E-02,-0.56881376E-03,-0.41281818E-02,-0.24281636E-03,
        -0.77639990E-04,-0.97183126E-03, 0.87332283E-03, 0.61600673E-03,
         0.23872562E-03, 0.93970814E-03,-0.32499371E-03,-0.10111089E-02,
        -0.47883196E-02,-0.72366791E-02,-0.10975617E-01,-0.39196643E-02,
         0.44477871E-03, 0.89666888E-03,-0.55419636E-03, 0.15996149E-02,
        -0.75763551E-03,-0.62295636E-02,-0.61943382E-02,-0.55274405E-02,
        -0.51155020E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_dent_300                                =v_l_e_dent_300                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]                *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dent_300                                =v_l_e_dent_300                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x23    *x42    
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x21*x31        
        +coeff[ 21]    *x23            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_dent_300                                =v_l_e_dent_300                                
        +coeff[ 26]    *x21    *x41*x52
        +coeff[ 27]*x12    *x31*x41    
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]        *x32    *x53
        +coeff[ 33]    *x23    *x42*x51
        +coeff[ 34]*x12*x21*x33        
    ;
    v_l_e_dent_300                                =v_l_e_dent_300                                
        +coeff[ 35]*x12*x22*x31*x41    
        +coeff[ 36]    *x23*x34        
        +coeff[ 37]    *x23*x33*x41    
        +coeff[ 38]    *x23*x32*x42    
        +coeff[ 39]*x12*x21*x33*x41    
        +coeff[ 40]*x12*x21*x32*x42    
        ;

    return v_l_e_dent_300                                ;
}
float x_e_dent_250                                (float *x,int m){
    int ncoeff= 21;
    float avdat= -0.5100543E+01;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
         0.45614195E-03, 0.41982555E-02,-0.12549540E+00,-0.55882488E-02,
         0.38068441E-02, 0.34698802E-02, 0.24749583E-02, 0.79088443E-03,
        -0.33803703E-03,-0.29450445E-03, 0.21239705E-03,-0.25808224E-04,
         0.34412107E-03, 0.14129141E-02, 0.18424694E-02,-0.20506656E-03,
        -0.13598352E-03, 0.81075769E-03,-0.17711922E-03, 0.27251319E-03,
         0.26532726E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_dent_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_250                                =v_x_e_dent_250                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x23        *x52
        +coeff[ 11]                *x51
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]    *x23*x33*x41    
        +coeff[ 16]            *x42    
    ;
    v_x_e_dent_250                                =v_x_e_dent_250                                
        +coeff[ 17]    *x23            
        +coeff[ 18]*x12    *x31*x41    
        +coeff[ 19]    *x21        *x54
        +coeff[ 20]    *x23*x31*x41    
        ;

    return v_x_e_dent_250                                ;
}
float t_e_dent_250                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3657162E+01;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.81937797E-01,-0.34400173E-01, 0.80201793E+00, 0.23209594E+00,
         0.35086993E-01, 0.59908950E-02,-0.18412922E-01, 0.28646374E-01,
         0.53014141E-01, 0.18380171E-01, 0.18435029E-01,-0.81455465E-02,
        -0.15148270E-01,-0.12732909E-01,-0.11843033E-01, 0.24466068E-02,
        -0.82415370E-02,-0.28804990E-02,-0.21184484E-01, 0.78743361E-02,
        -0.39066604E-03, 0.43879026E-02,-0.25390131E-01,-0.34283675E-01,
        -0.23831399E-01,-0.19548988E-03, 0.10609040E-03, 0.26940741E-02,
        -0.23431290E-03, 0.40912959E-02,-0.84485840E-02, 0.39043531E-03,
         0.34831764E-03, 0.61597489E-03, 0.87410782E-03,-0.12869549E-02,
         0.43786289E-02, 0.34704923E-02,-0.13279704E-02,-0.22314938E-01,
        -0.17593551E-01,-0.10778361E-03,-0.86629664E-03,-0.81771892E-03,
        -0.13207655E-02, 0.16539497E-02,-0.33120782E-03,-0.10287150E-01,
         0.10553268E-02,-0.70525822E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_250                                =v_t_e_dent_250                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_250                                =v_t_e_dent_250                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x12    *x31*x41*x51
    ;
    v_t_e_dent_250                                =v_t_e_dent_250                                
        +coeff[ 26]        *x31*x41*x53
        +coeff[ 27]        *x32        
        +coeff[ 28]                *x52
        +coeff[ 29]        *x31*x41*x51
        +coeff[ 30]    *x22*x32        
        +coeff[ 31]*x12                
        +coeff[ 32]*x12*x21            
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_dent_250                                =v_t_e_dent_250                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]    *x23*x32    *x51
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]*x11        *x42    
    ;
    v_t_e_dent_250                                =v_t_e_dent_250                                
        +coeff[ 44]        *x31*x43    
        +coeff[ 45]    *x21*x32    *x51
        +coeff[ 46]*x13    *x32        
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]        *x33*x43    
        +coeff[ 49]*x11*x22    *x41*x52
        ;

    return v_t_e_dent_250                                ;
}
float y_e_dent_250                                (float *x,int m){
    int ncoeff= 58;
    float avdat= -0.4955233E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 59]={
         0.48625324E-03, 0.12760824E+00, 0.41032817E-01, 0.11777788E-01,
         0.62071709E-02, 0.46655498E-02, 0.18406148E-04, 0.38119045E-02,
        -0.27105394E-02,-0.18640679E-02,-0.27822237E-02,-0.18650282E-02,
        -0.57329406E-03,-0.66570070E-03,-0.46421745E-03, 0.84116837E-05,
        -0.15635758E-03,-0.14665733E-02,-0.43262242E-03,-0.27078431E-03,
        -0.39614915E-05,-0.12738806E-03, 0.42267126E-03, 0.64835251E-04,
         0.15579378E-03,-0.28434980E-02,-0.12266105E-02,-0.20904853E-02,
        -0.94990828E-03,-0.73657063E-03,-0.19767149E-02,-0.31684165E-04,
         0.34498778E-04, 0.29214385E-04, 0.32047177E-03,-0.58629517E-04,
         0.23498564E-03,-0.13852974E-02,-0.49045769E-03, 0.47250219E-05,
        -0.19541678E-02,-0.66012712E-04,-0.11362095E-03,-0.44575845E-04,
        -0.47219772E-04, 0.55414097E-04, 0.39565533E-04,-0.76148659E-03,
         0.12339347E-03, 0.96874255E-05,-0.27130458E-04, 0.64166043E-05,
         0.49610815E-04, 0.26018171E-04, 0.69362148E-04,-0.37407743E-04,
         0.30274936E-04, 0.18994333E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x24*x31        
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x31*x44    
        +coeff[ 31]    *x24*x33        
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[ 35]    *x22    *x41*x51
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x22*x33        
        +coeff[ 39]            *x45*x51
        +coeff[ 40]    *x22*x32*x43    
        +coeff[ 41]    *x21*x31*x42    
        +coeff[ 42]    *x21*x32*x41    
        +coeff[ 43]    *x21*x33        
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]            *x41*x53
        +coeff[ 47]    *x22*x33*x42    
        +coeff[ 48]            *x43*x51
        +coeff[ 49]*x11*x21    *x42    
        +coeff[ 50]*x11*x22*x31        
        +coeff[ 51]*x11        *x44    
        +coeff[ 52]*x11*x21*x31*x42    
    ;
    v_y_e_dent_250                                =v_y_e_dent_250                                
        +coeff[ 53]        *x31    *x53
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]*x11*x23*x31        
        +coeff[ 56]    *x23*x31    *x51
        +coeff[ 57]*x13*x21*x31        
        ;

    return v_y_e_dent_250                                ;
}
float p_e_dent_250                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.5156662E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.50831976E-03,-0.69590963E-01,-0.53553026E-01,-0.19113865E-01,
        -0.26510887E-01, 0.75290469E-02, 0.11708103E-01,-0.18232651E-01,
         0.90131705E-03,-0.10293291E-01, 0.10327728E-02,-0.43380351E-05,
         0.84703712E-03,-0.36385057E-02,-0.21140594E-02, 0.10509129E-02,
         0.26686219E-02,-0.22273905E-04, 0.78134821E-04,-0.40207262E-03,
        -0.20314206E-02, 0.47757439E-03,-0.69859176E-03, 0.82633545E-03,
        -0.63885027E-03,-0.84338256E-03, 0.68279340E-04, 0.96570602E-05,
         0.63084922E-03,-0.50579186E-03,-0.50961529E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_250                                =v_p_e_dent_250                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_250                                =v_p_e_dent_250                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_250                                =v_p_e_dent_250                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x22*x31    *x51
        ;

    return v_p_e_dent_250                                ;
}
float l_e_dent_250                                (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.3871556E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.38016541E-02, 0.24280609E+00,-0.79160305E-02,-0.18494703E-01,
         0.10889518E-01,-0.55917036E-02, 0.25817673E-03,-0.20074544E-02,
        -0.60280426E-02, 0.11548273E-02,-0.61728004E-02,-0.56355023E-02,
         0.24474703E-04, 0.69418037E-03,-0.15917842E-02,-0.90805319E-03,
         0.12200469E-03,-0.34937679E-02,-0.29336044E-03,-0.24145725E-02,
         0.30132863E-02, 0.78792422E-04,-0.12136015E-03,-0.15353381E-02,
         0.52047259E-03, 0.12647902E-03,-0.46891003E-03,-0.31595532E-03,
         0.36136192E-03, 0.42393096E-03,-0.51070722E-02, 0.10254066E-02,
        -0.33718252E-02, 0.94501808E-03, 0.17852971E-02, 0.10002268E-02,
        -0.72318024E-03,-0.84295758E-03, 0.54609781E-03,-0.26152754E-03,
         0.73954684E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_dent_250                                =v_l_e_dent_250                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23        *x52
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_250                                =v_l_e_dent_250                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x23*x31*x43    
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]*x11        *x41    
        +coeff[ 23]    *x23            
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_l_e_dent_250                                =v_l_e_dent_250                                
        +coeff[ 26]*x11    *x32        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]        *x33*x41*x51
        +coeff[ 34]        *x32*x42*x51
    ;
    v_l_e_dent_250                                =v_l_e_dent_250                                
        +coeff[ 35]        *x31*x43*x51
        +coeff[ 36]*x11    *x33*x41    
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]*x11*x22    *x41*x51
        +coeff[ 39]*x11            *x54
        +coeff[ 40]*x12*x22*x32        
        ;

    return v_l_e_dent_250                                ;
}
float x_e_dent_200                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5101638E+01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
         0.91285602E-03, 0.41610338E-02,-0.12610650E+00,-0.54955967E-02,
         0.38385126E-02, 0.34360616E-02, 0.24627612E-02, 0.74685417E-03,
        -0.33422431E-03,-0.29317438E-03, 0.36233698E-03, 0.34803577E-03,
         0.87798852E-03, 0.14194686E-02,-0.56504286E-04,-0.14631245E-03,
        -0.23648930E-03, 0.25399867E-02, 0.18120984E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_200                                =v_x_e_dent_200                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x22*x31*x41    
    ;
    v_x_e_dent_200                                =v_x_e_dent_200                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_200                                ;
}
float t_e_dent_200                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3666100E+01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.86743824E-01,-0.34235451E-01, 0.80937487E+00, 0.23579209E+00,
         0.34856100E-01, 0.62139235E-02,-0.18440263E-01, 0.28395668E-01,
         0.54299641E-01, 0.17897295E-01, 0.18191345E-01,-0.82638022E-02,
        -0.14348787E-01,-0.12636106E-01,-0.12262437E-01, 0.24197104E-02,
        -0.80244904E-02,-0.28430058E-02,-0.21814477E-01, 0.86689004E-04,
         0.47122288E-03, 0.42656451E-02,-0.26819132E-01, 0.75404663E-02,
        -0.34963530E-01,-0.24344567E-01, 0.24205503E-03,-0.99170415E-04,
        -0.13344901E-03, 0.44125976E-03, 0.25968044E-02, 0.39938539E-02,
        -0.89931609E-02,-0.12140117E-02, 0.38850424E-03, 0.61939063E-03,
         0.84963907E-03,-0.14094871E-02, 0.39991424E-02, 0.28496936E-02,
        -0.23746645E-01,-0.18845335E-01,-0.80814380E-05,-0.25341986E-03,
        -0.84845268E-03,-0.74479252E-03,-0.57953765E-03, 0.15320237E-02,
        -0.36308248E-03,-0.11167847E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_200                                =v_t_e_dent_200                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_200                                =v_t_e_dent_200                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]*x12*x22*x31*x41    
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_200                                =v_t_e_dent_200                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]    *x22*x32    *x52
        +coeff[ 29]*x12                
        +coeff[ 30]        *x32        
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]    *x22*x32        
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]*x12*x21            
    ;
    v_t_e_dent_200                                =v_t_e_dent_200                                
        +coeff[ 35]*x11*x21        *x51
        +coeff[ 36]        *x32    *x51
        +coeff[ 37]*x11*x23            
        +coeff[ 38]    *x21*x31*x41*x51
        +coeff[ 39]    *x21    *x42*x51
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]    *x21*x31*x43    
        +coeff[ 42]    *x23*x32    *x51
        +coeff[ 43]                *x52
    ;
    v_t_e_dent_200                                =v_t_e_dent_200                                
        +coeff[ 44]*x11    *x31*x41    
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x13    *x32        
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dent_200                                ;
}
float y_e_dent_200                                (float *x,int m){
    int ncoeff= 64;
    float avdat= -0.5295894E-04;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 65]={
        -0.25676347E-04, 0.12689529E+00, 0.40374119E-01, 0.11764100E-01,
         0.62025012E-02, 0.46776892E-02, 0.18211566E-05, 0.38147613E-02,
        -0.26409989E-02,-0.18630892E-02,-0.24422463E-02,-0.16692777E-02,
        -0.56923862E-03,-0.66556450E-03,-0.45642693E-03,-0.19774331E-03,
        -0.15841439E-03,-0.12505624E-02,-0.40878166E-03,-0.25964895E-03,
        -0.23676575E-05,-0.12489325E-03, 0.40738127E-03,-0.35142112E-02,
         0.73661140E-04,-0.32615202E-03,-0.16788203E-03, 0.65842025E-04,
        -0.19834707E-02,-0.23030576E-02,-0.84003352E-03,-0.20234140E-03,
        -0.48227352E-03,-0.84334788E-05, 0.37094127E-04, 0.26933832E-04,
         0.30772970E-03, 0.13347159E-03, 0.58068249E-05, 0.23416073E-03,
        -0.11267642E-02,-0.86705941E-05,-0.13754549E-03,-0.52219748E-04,
        -0.52384334E-03, 0.57465451E-04,-0.57504006E-03,-0.32594163E-03,
        -0.46848378E-03,-0.29287596E-04,-0.59597887E-05,-0.28035777E-05,
        -0.98928169E-04, 0.14389823E-03,-0.45900080E-04,-0.92347764E-04,
         0.37276230E-04, 0.53370262E-04, 0.23758612E-04, 0.49832426E-04,
        -0.28958939E-04,-0.42645355E-04, 0.22129834E-04,-0.79548001E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22    *x45    
        +coeff[ 25]    *x24*x32*x41    
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 26]    *x24*x33        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x24*x31        
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24*x31*x42    
        +coeff[ 33]                *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x22    *x41*x51
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21*x32*x41    
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]        *x32*x43    
        +coeff[ 47]        *x33*x42    
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]*x11*x24*x31        
        +coeff[ 50]            *x43*x53
        +coeff[ 51]    *x21            
        +coeff[ 52]    *x21*x31*x42    
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 53]            *x43*x51
        +coeff[ 54]    *x21*x33        
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]    *x23    *x41*x51
        +coeff[ 60]        *x31*x42*x52
        +coeff[ 61]*x11*x23*x31        
    ;
    v_y_e_dent_200                                =v_y_e_dent_200                                
        +coeff[ 62]*x13*x21*x31        
        +coeff[ 63]    *x24    *x41*x51
        ;

    return v_y_e_dent_200                                ;
}
float p_e_dent_200                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.3049849E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.34885530E-03,-0.69956705E-01,-0.53975575E-01,-0.19296771E-01,
        -0.26808310E-01, 0.75047584E-02, 0.11693557E-01,-0.18339897E-01,
         0.91083563E-03,-0.10292760E-01, 0.10247935E-02,-0.54306415E-05,
         0.83875033E-03,-0.36046796E-02,-0.21004425E-02, 0.10601785E-02,
         0.26933639E-02,-0.72466770E-04, 0.92580638E-04,-0.39120024E-03,
        -0.19646285E-02, 0.48881734E-03,-0.69974444E-03, 0.86572004E-03,
        -0.64384053E-03,-0.83929644E-03,-0.13158954E-03,-0.48130605E-03,
         0.62843936E-03,-0.50210283E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_200                                =v_p_e_dent_200                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_200                                =v_p_e_dent_200                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dent_200                                =v_p_e_dent_200                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x22*x31    *x51
        ;

    return v_p_e_dent_200                                ;
}
float l_e_dent_200                                (float *x,int m){
    int ncoeff= 43;
    float avdat= -0.1856947E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
         0.30249262E-02, 0.24313183E+00,-0.80789002E-02,-0.18642850E-01,
         0.10329383E-01,-0.57338630E-02, 0.47314828E-03,-0.60478244E-02,
         0.12141956E-02,-0.43347254E-02,-0.32760676E-02, 0.16195765E-03,
        -0.22698561E-04,-0.22391360E-02,-0.17745348E-02, 0.79150643E-03,
        -0.15746685E-02, 0.38944939E-03,-0.83423860E-03,-0.21417378E-02,
        -0.20930309E-03,-0.23351896E-02, 0.28774550E-03,-0.21102912E-03,
         0.26999725E-03,-0.57976309E-03,-0.29261629E-03, 0.61881001E-03,
         0.47340500E-03, 0.40793256E-03, 0.26948904E-03,-0.12495368E-02,
        -0.12189265E-02,-0.10614555E-02, 0.62025624E-03,-0.66086545E-03,
         0.29633229E-03,-0.12162979E-01,-0.27108127E-01,-0.24844589E-01,
        -0.79247067E-02,-0.11819742E-02,-0.81708969E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dent_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_dent_200                                =v_l_e_dent_200                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dent_200                                =v_l_e_dent_200                                
        +coeff[ 17]                *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23    *x42*x52
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]*x12    *x31        
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_dent_200                                =v_l_e_dent_200                                
        +coeff[ 26]        *x32    *x52
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x11*x22    *x41    
        +coeff[ 29]*x11    *x31*x41*x51
        +coeff[ 30]    *x21*x34        
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x22    *x44    
    ;
    v_l_e_dent_200                                =v_l_e_dent_200                                
        +coeff[ 35]*x11*x22        *x53
        +coeff[ 36]*x13    *x33        
        +coeff[ 37]    *x23*x33*x41    
        +coeff[ 38]    *x23*x32*x42    
        +coeff[ 39]    *x23*x31*x43    
        +coeff[ 40]    *x23    *x44    
        +coeff[ 41]*x11*x21*x32*x42*x51
        +coeff[ 42]*x12*x22*x33        
        ;

    return v_l_e_dent_200                                ;
}
float x_e_dent_175                                (float *x,int m){
    int ncoeff= 18;
    float avdat= -0.5102530E+01;
    float xmin[10]={
        -0.40000E-02,-0.58938E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
         0.13034558E-02, 0.41311402E-02,-0.12642729E+00,-0.54187952E-02,
         0.39137993E-02, 0.34615940E-02, 0.24876022E-02, 0.71747712E-03,
        -0.33638920E-03,-0.29502757E-03, 0.36386665E-03, 0.33794486E-03,
         0.86944865E-03, 0.14115968E-02,-0.75324286E-04,-0.10613564E-03,
         0.23348778E-02, 0.17014396E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_dent_175                                =v_x_e_dent_175                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_x_e_dent_175                                =v_x_e_dent_175                                
        +coeff[ 17]    *x23    *x42    
        ;

    return v_x_e_dent_175                                ;
}
float t_e_dent_175                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3671030E+01;
    float xmin[10]={
        -0.40000E-02,-0.58938E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.88531390E-01,-0.34160420E-01, 0.81446254E+00, 0.23824047E+00,
         0.34303084E-01, 0.62305373E-02,-0.18405629E-01, 0.28199958E-01,
         0.54869026E-01, 0.17598502E-01, 0.18431930E-01,-0.82823094E-02,
        -0.14873507E-01,-0.13179282E-01,-0.11943818E-01, 0.24452093E-02,
        -0.80595501E-02,-0.28918071E-02, 0.78168577E-02, 0.45754672E-02,
        -0.27044429E-01,-0.21940568E-01,-0.34086399E-01,-0.22979220E-01,
         0.51619412E-04, 0.28550660E-03,-0.32524343E-03, 0.27378760E-02,
         0.73468743E-03, 0.38727147E-02,-0.91020847E-02,-0.12387030E-02,
         0.38226930E-03, 0.86768449E-03,-0.14349387E-02, 0.39544012E-02,
         0.29487519E-02,-0.22506399E-01,-0.18087728E-01, 0.33266036E-03,
        -0.28877342E-03, 0.25897758E-03,-0.79535053E-03,-0.72615437E-03,
         0.22667230E-03,-0.14934645E-02,-0.14565182E-02, 0.12197675E-02,
        -0.10405761E-01,-0.96789293E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_175                                =v_t_e_dent_175                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_175                                =v_t_e_dent_175                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x22*x31*x41    
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]*x12    *x31*x41*x51
        +coeff[ 25]        *x31*x41*x53
    ;
    v_t_e_dent_175                                =v_t_e_dent_175                                
        +coeff[ 26]    *x22*x32    *x52
        +coeff[ 27]        *x32        
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]        *x31*x41*x51
        +coeff[ 30]    *x22*x32        
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]*x12                
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_dent_175                                =v_t_e_dent_175                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x21    *x42*x51
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x21*x31*x43    
        +coeff[ 39]    *x23*x32    *x51
        +coeff[ 40]                *x52
        +coeff[ 41]*x12*x21            
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]*x11        *x42    
    ;
    v_t_e_dent_175                                =v_t_e_dent_175                                
        +coeff[ 44]    *x23*x31        
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_dent_175                                ;
}
float y_e_dent_175                                (float *x,int m){
    int ncoeff= 62;
    float avdat= -0.1104946E-03;
    float xmin[10]={
        -0.40000E-02,-0.58938E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 63]={
         0.35057048E-04, 0.12639117E+00, 0.39940786E-01, 0.11773502E-01,
         0.62020528E-02, 0.46916488E-02,-0.26195639E-04, 0.38222573E-02,
        -0.27466652E-02,-0.18231901E-02,-0.27494198E-02,-0.18572845E-02,
        -0.57961320E-03,-0.66372793E-03,-0.46338630E-03, 0.11519822E-04,
        -0.15647992E-03,-0.14579751E-02,-0.42596442E-03,-0.28216929E-03,
        -0.16833734E-05, 0.38652206E-03,-0.12696814E-03, 0.63810839E-04,
        -0.28261340E-02,-0.11443174E-02,-0.20353887E-02,-0.96035335E-03,
        -0.70637732E-03,-0.19024066E-02,-0.88995977E-04, 0.24587922E-04,
        -0.47866655E-04,-0.71582917E-05,-0.16188787E-03, 0.30983164E-03,
         0.12160194E-03,-0.14759804E-04, 0.22905268E-03,-0.12925763E-02,
        -0.51631784E-03, 0.42150527E-05,-0.18316387E-02, 0.33254040E-04,
        -0.14459425E-03, 0.29124149E-04,-0.55555422E-04,-0.44799999E-04,
         0.51712115E-04, 0.12937840E-04,-0.30704803E-04,-0.66960265E-03,
        -0.24851465E-05, 0.12572345E-03, 0.32382410E-04, 0.48428614E-04,
         0.25610092E-04, 0.68579226E-04,-0.33045362E-04, 0.37123446E-04,
         0.19252264E-04,-0.58513328E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x13        *x41    
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11        *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x24    *x41    
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22    *x45    
        +coeff[ 29]    *x22*x31*x44    
        +coeff[ 30]    *x24    *x43    
        +coeff[ 31]    *x24*x33        
        +coeff[ 32]    *x24    *x45    
        +coeff[ 33]                *x51
        +coeff[ 34]    *x21*x32*x41    
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x23*x31        
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x22*x32*x43    
        +coeff[ 43]*x11        *x41*x51
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[ 44]    *x21*x31*x42    
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]    *x21*x33        
        +coeff[ 47]*x11*x22    *x41    
        +coeff[ 48]        *x33    *x51
        +coeff[ 49]        *x34*x41    
        +coeff[ 50]*x11*x24*x31        
        +coeff[ 51]    *x22*x33*x42    
        +coeff[ 52]    *x21            
    ;
    v_y_e_dent_175                                =v_y_e_dent_175                                
        +coeff[ 53]            *x43*x51
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]        *x31    *x53
        +coeff[ 57]    *x23    *x41*x51
        +coeff[ 58]*x11*x23*x31        
        +coeff[ 59]    *x23*x31    *x51
        +coeff[ 60]*x13*x21*x31        
        +coeff[ 61]    *x24    *x41*x51
        ;

    return v_y_e_dent_175                                ;
}
float p_e_dent_175                                (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.2895069E-03;
    float xmin[10]={
        -0.40000E-02,-0.58938E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.25218475E-03,-0.70187204E-01,-0.54273829E-01,-0.19424161E-01,
        -0.27017608E-01, 0.73821926E-02, 0.11633107E-01,-0.18423265E-01,
         0.10669401E-02,-0.10435550E-01, 0.10268361E-02, 0.21369037E-06,
         0.83247281E-03,-0.35590413E-02,-0.20811998E-02, 0.10333530E-02,
         0.27934529E-02,-0.53452572E-04, 0.12618107E-03,-0.38875511E-03,
        -0.19404651E-02, 0.51484042E-03,-0.73064159E-03, 0.88668586E-03,
        -0.61627378E-03,-0.47501060E-04,-0.48538431E-03, 0.60469646E-03,
        -0.54656883E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_175                                =v_p_e_dent_175                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x24    *x41    
    ;
    v_p_e_dent_175                                =v_p_e_dent_175                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22*x31    *x52
    ;
    v_p_e_dent_175                                =v_p_e_dent_175                                
        +coeff[ 26]        *x31    *x54
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x22    *x41*x51
        ;

    return v_p_e_dent_175                                ;
}
float l_e_dent_175                                (float *x,int m){
    int ncoeff= 53;
    float avdat= -0.5749125E-04;
    float xmin[10]={
        -0.40000E-02,-0.58938E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 54]={
         0.22222004E-02, 0.24382581E+00,-0.79088528E-02,-0.18918237E-01,
         0.10470826E-01,-0.60788621E-02, 0.20731154E-02, 0.52414788E-03,
        -0.13725831E-02,-0.64818817E-02, 0.13377933E-02,-0.34197448E-02,
        -0.29526581E-02,-0.22997055E-02, 0.72655245E-03,-0.11399318E-02,
        -0.34441956E-03,-0.15046916E-02,-0.34949658E-02,-0.92331264E-02,
        -0.49731284E-02,-0.56978455E-02, 0.14045286E-03, 0.10928536E-03,
        -0.10282152E-02, 0.53118367E-03, 0.30578059E-03,-0.45607326E-03,
         0.13664083E-03,-0.74683077E-03, 0.84277242E-03, 0.95035147E-03,
         0.20383409E-03,-0.13859797E-02, 0.65905630E-03,-0.62435367E-02,
        -0.35187281E-02,-0.65104775E-02,-0.20924029E-02,-0.81627152E-03,
        -0.41054643E-03,-0.70878933E-03,-0.42930778E-03,-0.21305867E-02,
         0.11248831E-02, 0.36387492E-03, 0.17452610E-02,-0.20492920E-02,
         0.31013068E-03,-0.10833831E-01, 0.23168295E-02,-0.43673272E-03,
         0.30320289E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dent_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_175                                =v_l_e_dent_175                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23        *x52
    ;
    v_l_e_dent_175                                =v_l_e_dent_175                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x23    *x42    
        +coeff[ 19]    *x23*x33*x41    
        +coeff[ 20]    *x21*x34*x42    
        +coeff[ 21]    *x21*x33*x43    
        +coeff[ 22]        *x31        
        +coeff[ 23]                *x51
        +coeff[ 24]    *x23            
        +coeff[ 25]        *x31*x41*x51
    ;
    v_l_e_dent_175                                =v_l_e_dent_175                                
        +coeff[ 26]            *x42*x51
        +coeff[ 27]    *x21        *x52
        +coeff[ 28]            *x41*x52
        +coeff[ 29]        *x34        
        +coeff[ 30]    *x22    *x42    
        +coeff[ 31]    *x21*x31*x41*x51
        +coeff[ 32]*x11*x23            
        +coeff[ 33]*x11*x21*x31    *x51
        +coeff[ 34]*x11*x21        *x52
    ;
    v_l_e_dent_175                                =v_l_e_dent_175                                
        +coeff[ 35]    *x23*x31*x41    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x21    *x44    
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]*x11*x24            
        +coeff[ 41]*x11*x21    *x42*x51
        +coeff[ 42]*x12    *x31    *x52
        +coeff[ 43]    *x22*x33*x41    
    ;
    v_l_e_dent_175                                =v_l_e_dent_175                                
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]        *x33*x42*x51
        +coeff[ 46]*x11*x23*x31    *x51
        +coeff[ 47]*x11*x23        *x52
        +coeff[ 48]*x13    *x33        
        +coeff[ 49]    *x23*x32*x42    
        +coeff[ 50]*x11*x24*x31    *x51
        +coeff[ 51]*x11*x22*x33    *x51
        +coeff[ 52]*x11*x22*x32*x41*x51
        ;

    return v_l_e_dent_175                                ;
}
float x_e_dent_150                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5101970E+01;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
        -0.50585560E-03,-0.12619874E+00,-0.53022127E-02, 0.21207459E-05,
         0.40897685E-02, 0.39415718E-02, 0.33438816E-02, 0.23955838E-02,
         0.74909680E-03,-0.33316339E-03,-0.29145347E-03, 0.34226547E-03,
         0.79777307E-03, 0.13516885E-02,-0.12787968E-03,-0.92141345E-04,
         0.33326237E-03, 0.24169837E-02, 0.17749646E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_150                                =v_x_e_dent_150                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x22        *x51
    ;
    v_x_e_dent_150                                =v_x_e_dent_150                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_150                                ;
}
float t_e_dent_150                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3667185E+01;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.76609775E-01,-0.34011640E-01, 0.81931496E+00, 0.24011444E+00,
         0.33814751E-01, 0.66120429E-02,-0.18382901E-01, 0.27893903E-01,
         0.55505712E-01, 0.17078366E-01, 0.17966742E-01,-0.82255937E-02,
        -0.15342431E-01,-0.12914741E-01,-0.11365398E-01, 0.24099473E-02,
        -0.80686808E-02,-0.22637406E-01, 0.76063597E-02,-0.17813001E-02,
         0.44320161E-02,-0.26621528E-02,-0.26714602E-01, 0.21694489E-03,
         0.16237585E-04,-0.53338819E-04, 0.27382921E-02, 0.65243500E-03,
         0.40942961E-02,-0.95558688E-02,-0.32932729E-01,-0.23206413E-01,
         0.39440620E-03,-0.34654498E-03, 0.90383604E-03,-0.15701333E-02,
         0.42864848E-02, 0.33391102E-02,-0.95324684E-03,-0.21519061E-01,
        -0.17144460E-01,-0.28590969E-03, 0.28900502E-03,-0.78404759E-03,
        -0.75646647E-03,-0.20268380E-02,-0.17103682E-02, 0.15319372E-02,
        -0.32153900E-03,-0.98649897E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_150                                =v_t_e_dent_150                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_150                                =v_t_e_dent_150                                
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x22*x33*x41    
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]*x12    *x31*x41*x51
        +coeff[ 24]        *x31*x41*x53
        +coeff[ 25]    *x22*x32    *x52
    ;
    v_t_e_dent_150                                =v_t_e_dent_150                                
        +coeff[ 26]        *x32        
        +coeff[ 27]*x11*x21        *x51
        +coeff[ 28]        *x31*x41*x51
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]*x12                
        +coeff[ 33]                *x52
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_dent_150                                =v_t_e_dent_150                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]    *x23*x32    *x51
        +coeff[ 42]*x12*x21            
        +coeff[ 43]*x11    *x31*x41    
    ;
    v_t_e_dent_150                                =v_t_e_dent_150                                
        +coeff[ 44]*x11        *x42    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x13    *x32        
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dent_150                                ;
}
float y_e_dent_150                                (float *x,int m){
    int ncoeff= 63;
    float avdat= -0.2395871E-03;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 64]={
         0.18626649E-03, 0.12570964E+00, 0.39350547E-01, 0.11758573E-01,
         0.61964728E-02, 0.46671163E-02,-0.49986789E-04, 0.37802830E-02,
        -0.25908500E-02,-0.18097959E-02,-0.23972779E-02,-0.16251116E-02,
        -0.58007863E-03,-0.66383637E-03,-0.46013543E-03,-0.20171079E-03,
        -0.15384750E-03,-0.12239773E-02,-0.39640412E-03,-0.28120939E-03,
         0.39373631E-05,-0.12773363E-03, 0.33803118E-03,-0.34897844E-02,
         0.39079299E-04,-0.25636799E-03,-0.10406184E-03,-0.19119744E-02,
        -0.22839522E-02,-0.81916997E-03,-0.20129434E-03,-0.41441762E-03,
         0.63673018E-04, 0.37866321E-04, 0.28218798E-04, 0.29802456E-03,
         0.78730336E-04,-0.14110844E-05, 0.23023166E-03,-0.10837789E-02,
        -0.19379702E-04,-0.55071159E-05, 0.14269674E-03,-0.21016582E-03,
        -0.45884077E-04,-0.53863018E-03, 0.53337095E-04,-0.61284070E-03,
        -0.34887835E-03,-0.49638347E-03, 0.16355732E-04, 0.90422227E-05,
        -0.21918281E-03,-0.69921516E-04,-0.24308119E-04,-0.10449196E-03,
         0.33219338E-04, 0.54482000E-04, 0.27538679E-04, 0.58066929E-04,
        -0.16528462E-04, 0.27879823E-04,-0.62305240E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22    *x45    
        +coeff[ 25]    *x24*x32*x41    
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 26]    *x24*x33        
        +coeff[ 27]    *x22    *x43    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x24*x31        
        +coeff[ 30]    *x24    *x43    
        +coeff[ 31]    *x24*x31*x42    
        +coeff[ 32]*x11*x21    *x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]*x11    *x31    *x51
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x23*x31        
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]            *x45*x51
        +coeff[ 41]                *x51
        +coeff[ 42]            *x43*x51
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]        *x32*x43    
        +coeff[ 48]        *x33*x42    
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]    *x21*x31*x44    
        +coeff[ 51]    *x23*x33        
        +coeff[ 52]    *x21*x31*x42    
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 53]    *x21*x33        
        +coeff[ 54]*x11*x22*x31        
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]    *x23    *x41*x51
        +coeff[ 60]*x11*x23*x31        
        +coeff[ 61]    *x23*x31    *x51
    ;
    v_y_e_dent_150                                =v_y_e_dent_150                                
        +coeff[ 62]    *x24    *x41*x51
        ;

    return v_y_e_dent_150                                ;
}
float p_e_dent_150                                (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.3600392E-03;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.33208265E-03,-0.70583269E-01,-0.54786678E-01,-0.19621203E-01,
        -0.27348720E-01, 0.74890545E-02, 0.11678578E-01,-0.18218363E-01,
         0.84522925E-03,-0.10246934E-01, 0.10299751E-02,-0.70151450E-05,
         0.84008765E-03,-0.35683364E-02,-0.20784573E-02, 0.10225443E-02,
        -0.28231134E-04,-0.38944074E-03,-0.19832293E-02, 0.56849053E-03,
         0.65537682E-03,-0.67030202E-03, 0.95936767E-03,-0.63123187E-03,
        -0.75995259E-03, 0.25967809E-02,-0.14004039E-03,-0.47407902E-03,
         0.14818077E-03,-0.47138872E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_150                                =v_p_e_dent_150                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_dent_150                                =v_p_e_dent_150                                
        +coeff[ 17]        *x33        
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]    *x21    *x41*x51
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x24    *x41    
    ;
    v_p_e_dent_150                                =v_p_e_dent_150                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]    *x21*x31    *x51
        +coeff[ 29]    *x22*x31    *x51
        ;

    return v_p_e_dent_150                                ;
}
float l_e_dent_150                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1115168E-02;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.57349573E-02, 0.24321651E+00,-0.77312328E-02,-0.18857073E-01,
         0.10401347E-01,-0.59037614E-02,-0.11722789E-04,-0.20130251E-02,
        -0.62711453E-02, 0.12569271E-02,-0.51778280E-02,-0.43085031E-02,
        -0.30114194E-02, 0.60905528E-03,-0.15478333E-02, 0.62439585E-03,
         0.37817119E-03,-0.72664983E-03,-0.16237301E-02, 0.93969022E-03,
        -0.84380305E-03, 0.99736231E-03,-0.32540169E-03,-0.22048781E-03,
        -0.10794357E-02, 0.28284625E-03, 0.56539429E-03, 0.52978937E-03,
         0.32125736E-03,-0.64095641E-02,-0.42412332E-02,-0.20421040E-02,
         0.36103104E-03,-0.64686238E-03, 0.95078204E-03, 0.66600996E-03,
         0.31556239E-03, 0.89265470E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_dent_150                                =v_l_e_dent_150                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]            *x42*x51
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_150                                =v_l_e_dent_150                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23*x31*x41*x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11        *x42    
        +coeff[ 24]*x11            *x52
        +coeff[ 25]*x13                
    ;
    v_l_e_dent_150                                =v_l_e_dent_150                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x22        *x52
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x41*x52
        +coeff[ 32]        *x32    *x53
        +coeff[ 33]    *x21    *x41*x53
        +coeff[ 34]*x11            *x54
    ;
    v_l_e_dent_150                                =v_l_e_dent_150                                
        +coeff[ 35]*x12*x21    *x41*x51
        +coeff[ 36]*x13        *x41*x51
        +coeff[ 37]*x11*x23    *x41*x52
        ;

    return v_l_e_dent_150                                ;
}
float x_e_dent_125                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5101318E+01;
    float xmin[10]={
        -0.39992E-02,-0.57552E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
        -0.14380726E-02,-0.12763745E+00,-0.52081095E-02,-0.40922046E-05,
         0.40431386E-02, 0.40516881E-02, 0.34031961E-02, 0.24306835E-02,
         0.64875034E-03,-0.32610970E-03,-0.29261049E-03, 0.34774153E-03,
         0.81310753E-03, 0.13790280E-02,-0.13606057E-03,-0.87639834E-04,
         0.33037676E-03, 0.21575142E-02, 0.16046372E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_125                                =v_x_e_dent_125                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x22        *x51
    ;
    v_x_e_dent_125                                =v_x_e_dent_125                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_125                                ;
}
float t_e_dent_125                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3664439E+01;
    float xmin[10]={
        -0.39992E-02,-0.57552E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.71990184E-01,-0.33777431E-01, 0.83157855E+00, 0.24682270E+00,
         0.33201888E-01, 0.66638994E-02,-0.18616781E-01, 0.27302466E-01,
         0.57827353E-01, 0.16321614E-01, 0.17749228E-01,-0.82659991E-02,
        -0.15693801E-01,-0.13449346E-01,-0.11031445E-01, 0.23840903E-02,
        -0.82255863E-02,-0.22509197E-01, 0.77612465E-02, 0.29230080E-03,
        -0.52305526E-03, 0.44704895E-02,-0.27067354E-02,-0.27216056E-01,
        -0.28639497E-04, 0.30381107E-03,-0.13839347E-03, 0.24422545E-02,
         0.66892500E-03, 0.39135399E-02,-0.91547156E-02,-0.31610910E-01,
        -0.22014944E-01, 0.40905131E-03,-0.32396615E-03, 0.85599959E-03,
        -0.12430638E-02, 0.39540553E-02, 0.29816730E-02,-0.10951479E-02,
        -0.10091952E-01,-0.22385133E-01,-0.18089112E-01,-0.59767964E-03,
         0.35580798E-03,-0.62475167E-03,-0.58921427E-03,-0.16011597E-02,
        -0.13976722E-02, 0.15212413E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_dent_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_125                                =v_t_e_dent_125                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_125                                =v_t_e_dent_125                                
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]*x12*x22*x31*x41    
        +coeff[ 20]    *x22*x31*x43    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]*x12    *x31*x41*x51
        +coeff[ 25]        *x31*x43*x51
    ;
    v_t_e_dent_125                                =v_t_e_dent_125                                
        +coeff[ 26]    *x22*x32    *x52
        +coeff[ 27]        *x32        
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]        *x31*x41*x51
        +coeff[ 30]    *x22*x32        
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]*x12                
        +coeff[ 34]                *x52
    ;
    v_t_e_dent_125                                =v_t_e_dent_125                                
        +coeff[ 35]        *x32    *x51
        +coeff[ 36]*x11*x23            
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_dent_125                                =v_t_e_dent_125                                
        +coeff[ 44]*x12*x21            
        +coeff[ 45]*x11    *x31*x41    
        +coeff[ 46]*x11        *x42    
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_dent_125                                ;
}
float y_e_dent_125                                (float *x,int m){
    int ncoeff= 67;
    float avdat= -0.1612022E-03;
    float xmin[10]={
        -0.39992E-02,-0.57552E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 68]={
         0.16718564E-03, 0.12479156E+00, 0.38472634E-01, 0.11765478E-01,
         0.61924849E-02, 0.47250181E-02,-0.64636130E-04, 0.38446237E-02,
        -0.26871765E-02,-0.18436133E-02,-0.23922150E-02,-0.16228441E-02,
        -0.58076321E-03,-0.65956282E-03,-0.45732653E-03,-0.15686876E-03,
        -0.15171958E-03,-0.12592041E-02,-0.39676236E-03,-0.27298531E-03,
        -0.59319785E-06, 0.32683258E-03,-0.33780835E-02,-0.20598305E-04,
        -0.45269189E-03,-0.15005171E-03,-0.12518044E-03, 0.24052482E-04,
        -0.17765622E-02,-0.21342230E-02,-0.79091504E-03,-0.37114619E-03,
        -0.63586410E-03, 0.34858898E-04, 0.26922817E-04, 0.29753803E-03,
         0.77162716E-04, 0.16637054E-04, 0.22798050E-03,-0.10055437E-02,
         0.76944025E-05,-0.20994162E-03,-0.44657292E-04,-0.50717959E-03,
        -0.27041031E-04, 0.52515908E-04,-0.58504206E-03,-0.33339593E-03,
        -0.45410127E-03,-0.97506621E-04,-0.65889863E-05,-0.12784264E-03,
        -0.66682318E-04, 0.14025568E-04,-0.70219103E-05,-0.17832613E-03,
         0.12200016E-03,-0.62922576E-04, 0.90961861E-04,-0.10059047E-03,
         0.30123592E-04, 0.12664625E-03, 0.25805464E-04, 0.45088778E-04,
         0.38577855E-04,-0.41694737E-04,-0.86933236E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x13        *x41    
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x22    *x45    
        +coeff[ 24]    *x24*x32*x41    
        +coeff[ 25]    *x24*x33        
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 26]*x11        *x41    
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x24*x31        
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24*x31*x42    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]*x11    *x31    *x51
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x23*x31        
        +coeff[ 37]    *x22    *x41*x51
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]            *x45*x51
        +coeff[ 41]    *x21*x32*x41    
        +coeff[ 42]*x11*x22    *x41    
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 44]*x11*x22*x31        
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]        *x32*x43    
        +coeff[ 47]        *x33*x42    
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]    *x21*x31*x44    
        +coeff[ 50]    *x23*x33        
        +coeff[ 51]    *x21*x32*x45    
        +coeff[ 52]    *x21*x33*x44    
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 53]*x11*x21*x31        
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]    *x21*x31*x42    
        +coeff[ 56]            *x43*x51
        +coeff[ 57]    *x21*x33        
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]        *x34*x41    
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_dent_125                                =v_y_e_dent_125                                
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x23    *x41*x51
        +coeff[ 65]*x11*x23*x31        
        +coeff[ 66]    *x24    *x41*x51
        ;

    return v_y_e_dent_125                                ;
}
float p_e_dent_125                                (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.1685408E-03;
    float xmin[10]={
        -0.39992E-02,-0.57552E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.17996202E-03,-0.70874058E-01,-0.55223923E-01,-0.19861551E-01,
        -0.27736286E-01, 0.74769827E-02, 0.11678746E-01,-0.18586438E-01,
         0.90429775E-03,-0.10450846E-01, 0.10189580E-02,-0.12407997E-04,
         0.83100609E-03,-0.35216182E-02,-0.20583274E-02, 0.10259729E-02,
        -0.72199597E-04,-0.38895314E-03,-0.19156917E-02, 0.61363319E-03,
         0.65460335E-03,-0.72978367E-03, 0.97356114E-03,-0.62756758E-03,
        -0.77598222E-03, 0.27174335E-02,-0.14010948E-03,-0.47781991E-03,
         0.17879103E-03,-0.48301107E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dent_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_125                                =v_p_e_dent_125                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_dent_125                                =v_p_e_dent_125                                
        +coeff[ 17]        *x33        
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]    *x21    *x41*x51
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x24    *x41    
    ;
    v_p_e_dent_125                                =v_p_e_dent_125                                
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]    *x21*x31    *x51
        +coeff[ 29]    *x22*x31    *x51
        ;

    return v_p_e_dent_125                                ;
}
float l_e_dent_125                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2453950E-02;
    float xmin[10]={
        -0.39992E-02,-0.57552E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.76230257E-02, 0.24615784E+00,-0.75854543E-02,-0.19293873E-01,
         0.10078711E-01,-0.60213050E-02, 0.11948562E-02, 0.17671529E-03,
        -0.20320253E-02,-0.65202042E-02, 0.12370037E-02,-0.72877058E-02,
        -0.54168738E-02,-0.13290248E-02, 0.30854763E-03, 0.69848931E-03,
         0.44255529E-03,-0.24584096E-02,-0.18488661E-02, 0.76894776E-03,
        -0.55454945E-03,-0.15403436E-02,-0.14334477E-02, 0.58824313E-03,
         0.17742200E-03,-0.14139175E-02,-0.24960304E-03, 0.79337100E-03,
        -0.26523911E-02, 0.74753037E-03, 0.46391223E-03, 0.14004691E-02,
         0.55096974E-03, 0.57718647E-03,-0.75109594E-03,-0.38100968E-03,
         0.12293749E-02, 0.10747637E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dent_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_125                                =v_l_e_dent_125                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x24        *x51
        +coeff[ 16]                *x51
    ;
    v_l_e_dent_125                                =v_l_e_dent_125                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_dent_125                                =v_l_e_dent_125                                
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]        *x33*x42    
        +coeff[ 30]*x13            *x51
        +coeff[ 31]*x11*x24            
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]*x12        *x42*x51
        +coeff[ 34]*x12*x22*x31    *x51
    ;
    v_l_e_dent_125                                =v_l_e_dent_125                                
        +coeff[ 35]*x13        *x41*x52
        +coeff[ 36]*x11    *x33*x42*x51
        +coeff[ 37]*x11*x22    *x43*x51
        ;

    return v_l_e_dent_125                                ;
}
float x_e_dent_100                                (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.5101443E+01;
    float xmin[10]={
        -0.39997E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
        -0.16467174E-02,-0.12911712E+00,-0.50415760E-02,-0.34408075E-06,
         0.39593922E-02, 0.41591832E-02, 0.32489435E-02, 0.23328431E-02,
         0.71757921E-03,-0.32684612E-03,-0.28977593E-03, 0.33166492E-03,
         0.74829371E-03, 0.13040504E-02,-0.14276401E-03,-0.82624909E-04,
         0.32571852E-03, 0.22604114E-02, 0.16843352E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dent_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x11                
        +coeff[  5]    *x22            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_dent_100                                =v_x_e_dent_100                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]*x11            *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]                *x51
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x22        *x51
    ;
    v_x_e_dent_100                                =v_x_e_dent_100                                
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x23    *x42    
        ;

    return v_x_e_dent_100                                ;
}
float t_e_dent_100                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3665329E+01;
    float xmin[10]={
        -0.39997E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.70616387E-01,-0.33253331E-01, 0.84513146E+00, 0.25468281E+00,
         0.31925149E-01, 0.66712159E-02,-0.18363032E-01, 0.26459243E-01,
         0.61282024E-01, 0.15596185E-01, 0.17281394E-01,-0.83771721E-02,
        -0.14862757E-01,-0.12784788E-01,-0.11503562E-01, 0.24152226E-02,
        -0.78386627E-02,-0.22426054E-01, 0.76787174E-02,-0.21639189E-02,
         0.44631278E-02,-0.26915986E-02,-0.89818854E-02,-0.26821693E-01,
        -0.32689698E-01,-0.23042589E-01,-0.11263782E-04,-0.76220269E-04,
         0.20699482E-02,-0.32712903E-03, 0.85859012E-03, 0.41183899E-02,
         0.36105199E-03, 0.90111996E-03,-0.17510927E-02, 0.41074418E-02,
         0.32100228E-02,-0.10991637E-02,-0.98089250E-02,-0.17764872E-01,
        -0.43338383E-04, 0.24433195E-03,-0.47267225E-03,-0.11892705E-02,
         0.14707359E-02,-0.71448227E-03,-0.21836080E-01,-0.54477444E-02,
        -0.33349823E-02, 0.10359874E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dent_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dent_100                                =v_t_e_dent_100                                
        +coeff[  8]    *x23            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dent_100                                =v_t_e_dent_100                                
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x22*x33*x41    
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_dent_100                                =v_t_e_dent_100                                
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]        *x31*x41*x53
        +coeff[ 28]        *x32        
        +coeff[ 29]                *x52
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]*x12                
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_dent_100                                =v_t_e_dent_100                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x21    *x42*x51
        +coeff[ 37]    *x22        *x52
        +coeff[ 38]    *x21*x33*x41    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x23*x32    *x51
        +coeff[ 41]*x12*x21            
        +coeff[ 42]*x11        *x42    
        +coeff[ 43]        *x31*x43    
    ;
    v_t_e_dent_100                                =v_t_e_dent_100                                
        +coeff[ 44]    *x21*x32    *x51
        +coeff[ 45]*x13    *x31*x41    
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]    *x22*x31*x43    
        +coeff[ 49]        *x33*x43    
        ;

    return v_t_e_dent_100                                ;
}
float y_e_dent_100                                (float *x,int m){
    int ncoeff= 64;
    float avdat= -0.6208836E-03;
    float xmin[10]={
        -0.39997E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 65]={
         0.54801616E-03, 0.12334011E+00, 0.37195098E-01, 0.11764377E-01,
         0.61876629E-02, 0.47786711E-02,-0.80531019E-04, 0.39093806E-02,
        -0.26430939E-02,-0.18402586E-02,-0.23509357E-02,-0.15798801E-02,
        -0.59316691E-03,-0.66120806E-03,-0.45715639E-03,-0.18076657E-03,
        -0.14954848E-03,-0.12104060E-02,-0.38710039E-03,-0.28323865E-03,
        -0.36099600E-05,-0.12246113E-03, 0.33866457E-03,-0.33980273E-02,
         0.58103331E-04,-0.44848223E-03,-0.15215595E-03, 0.63808271E-04,
        -0.19298316E-02,-0.21453779E-02,-0.77421236E-03,-0.27939517E-03,
        -0.62348659E-03,-0.76416563E-05, 0.33150078E-04, 0.28090853E-04,
         0.29013472E-03, 0.77992190E-04, 0.21795576E-03,-0.10475933E-02,
        -0.45502221E-03,-0.12049549E-04,-0.21628344E-03,-0.47146754E-04,
         0.28482573E-05,-0.50942501E-03, 0.53806572E-04,-0.57980994E-03,
        -0.31960252E-03,-0.33163605E-03,-0.21224354E-03,-0.29856415E-03,
        -0.32141613E-05, 0.29679451E-04, 0.13084237E-03,-0.41218354E-04,
        -0.27047650E-04,-0.10818068E-03, 0.30056974E-04, 0.30519233E-04,
         0.25005880E-04, 0.36212328E-04,-0.52383086E-04,-0.58283906E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dent_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x21    *x41    
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]            *x45    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22    *x45    
        +coeff[ 25]    *x24*x32*x41    
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 26]    *x24*x33        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x24*x31        
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24*x31*x42    
        +coeff[ 33]                *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21*x32*x41    
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 44]    *x22    *x41*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]        *x32*x43    
        +coeff[ 48]        *x33*x42    
        +coeff[ 49]    *x21*x31*x44    
        +coeff[ 50]    *x21*x33*x42    
        +coeff[ 51]    *x21*x32*x45    
        +coeff[ 52]    *x21            
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 53]*x11*x21*x31        
        +coeff[ 54]            *x43*x51
        +coeff[ 55]    *x21*x33        
        +coeff[ 56]*x11*x22*x31        
        +coeff[ 57]        *x34*x41    
        +coeff[ 58]            *x41*x53
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]        *x31    *x53
        +coeff[ 61]    *x23    *x41*x51
    ;
    v_y_e_dent_100                                =v_y_e_dent_100                                
        +coeff[ 62]*x11*x23*x31        
        +coeff[ 63]    *x24    *x41*x51
        ;

    return v_y_e_dent_100                                ;
}
float p_e_dent_100                                (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.3273820E-03;
    float xmin[10]={
        -0.39997E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.28657290E-03,-0.71287632E-01,-0.55817466E-01,-0.20143602E-01,
        -0.28155336E-01, 0.74477354E-02, 0.11648219E-01,-0.18848686E-01,
         0.86541660E-03,-0.10629145E-01, 0.99627650E-03,-0.98467062E-05,
         0.81423181E-03,-0.34403144E-02,-0.20257428E-02, 0.10310209E-02,
        -0.23215411E-04,-0.37130949E-03,-0.18869118E-02, 0.68358204E-03,
         0.66033890E-03,-0.77729678E-03, 0.10140693E-02,-0.63572038E-03,
        -0.70172420E-03, 0.27169299E-02, 0.13291692E-04, 0.23083670E-03,
        -0.48800651E-03,-0.44603637E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dent_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dent_100                                =v_p_e_dent_100                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x33        
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_dent_100                                =v_p_e_dent_100                                
        +coeff[ 17]        *x33        
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]    *x21    *x41*x51
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x24    *x41    
    ;
    v_p_e_dent_100                                =v_p_e_dent_100                                
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]    *x21*x31    *x51
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x22*x31    *x51
        ;

    return v_p_e_dent_100                                ;
}
float l_e_dent_100                                (float *x,int m){
    int ncoeff= 47;
    float avdat= -0.2129076E-02;
    float xmin[10]={
        -0.39997E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 48]={
         0.79793101E-02, 0.24864325E+00,-0.78040203E-02,-0.19676711E-01,
         0.92634140E-02,-0.60884035E-02, 0.26555394E-03,-0.44100528E-03,
        -0.21077874E-02,-0.60321074E-02, 0.13983211E-02,-0.53668688E-02,
        -0.33569979E-02,-0.35040078E-02, 0.10260993E-02,-0.88530086E-03,
        -0.12699885E-02, 0.23249644E-03, 0.72732480E-03,-0.48482482E-03,
        -0.11683992E-04,-0.50583743E-02, 0.27570414E-03, 0.11415903E-03,
        -0.19103709E-02,-0.84917555E-02,-0.45893332E-02,-0.89392345E-02,
         0.21434613E-03,-0.15775829E-03,-0.66683313E-03, 0.72596688E-03,
         0.11879376E-02, 0.32827698E-03, 0.41170785E-03, 0.50626794E-03,
        -0.78724799E-04, 0.19215833E-03,-0.57306299E-02,-0.62077609E-03,
         0.41155721E-03, 0.17908351E-02, 0.73800003E-03, 0.73848106E-03,
        -0.13303105E-01,-0.82402932E-03,-0.68395119E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dent_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]            *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dent_100                                =v_l_e_dent_100                                
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dent_100                                =v_l_e_dent_100                                
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]            *x42*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x22*x31*x41*x51
        +coeff[ 23]    *x23        *x52
        +coeff[ 24]*x11*x24            
        +coeff[ 25]    *x21*x34*x42    
    ;
    v_l_e_dent_100                                =v_l_e_dent_100                                
        +coeff[ 26]    *x23*x31*x43    
        +coeff[ 27]    *x21*x32*x44    
        +coeff[ 28]                *x51
        +coeff[ 29]    *x21*x31        
        +coeff[ 30]    *x23            
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]*x11*x22            
        +coeff[ 33]*x13                
        +coeff[ 34]    *x21    *x42*x51
    ;
    v_l_e_dent_100                                =v_l_e_dent_100                                
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]*x11*x21*x32        
        +coeff[ 37]*x11    *x33        
        +coeff[ 38]    *x23*x31*x41    
        +coeff[ 39]*x13            *x51
        +coeff[ 40]            *x44*x52
        +coeff[ 41]*x11*x23*x31*x41    
        +coeff[ 42]*x11*x21    *x44    
        +coeff[ 43]*x12*x21    *x41*x52
    ;
    v_l_e_dent_100                                =v_l_e_dent_100                                
        +coeff[ 44]    *x21*x33*x43    
        +coeff[ 45]*x11    *x31*x42*x53
        +coeff[ 46]*x11*x22        *x54
        ;

    return v_l_e_dent_100                                ;
}
