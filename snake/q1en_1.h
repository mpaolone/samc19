float x_e_q1en_1_1200                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.2887741E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.29544081E-03, 0.39994968E-02, 0.77080637E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_1200                              ;
}
float t_e_q1en_1_1200                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2522523E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.25770543E-03, 0.60063694E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_1200                              ;
}
float y_e_q1en_1_1200                              (float *x,int m){
    int ncoeff= 20;
    float avdat=  0.1304652E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
        -0.14838400E-03, 0.38544070E-01, 0.53974710E-01,-0.18922586E-05,
         0.73597294E-05, 0.29065056E-05,-0.29614228E-05, 0.47861436E-05,
        -0.12536184E-04,-0.51623542E-05,-0.96629346E-05, 0.81660655E-05,
         0.87442331E-05, 0.12331763E-04,-0.84002650E-05, 0.75067182E-05,
         0.12243871E-04, 0.66205712E-05,-0.76929273E-05,-0.13686059E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]*x11    *x31        
        +coeff[  4]            *x43    
        +coeff[  5]        *x32    *x51
        +coeff[  6]    *x21*x33        
        +coeff[  7]            *x42*x52
    ;
    v_y_e_q1en_1_1200                              =v_y_e_q1en_1_1200                              
        +coeff[  8]*x11    *x33*x41    
        +coeff[  9]    *x24        *x51
        +coeff[ 10]    *x21*x31*x41*x52
        +coeff[ 11]*x12        *x42*x51
        +coeff[ 12]*x11*x21    *x41*x52
        +coeff[ 13]*x11    *x31*x41*x52
        +coeff[ 14]        *x32*x45    
        +coeff[ 15]*x12*x21    *x43    
        +coeff[ 16]*x11    *x31*x43*x51
    ;
    v_y_e_q1en_1_1200                              =v_y_e_q1en_1_1200                              
        +coeff[ 17]        *x31*x43*x52
        +coeff[ 18]*x11    *x34*x41    
        +coeff[ 19]    *x22*x32*x41*x51
        ;

    return v_y_e_q1en_1_1200                              ;
}
float p_e_q1en_1_1200                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.7559288E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.22607848E-04, 0.30036364E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_1200                              ;
}
float l_e_q1en_1_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9985368E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.94483420E-03,-0.21306784E-02,-0.32303535E-03,-0.17806931E-03,
        -0.22108881E-03,-0.10145803E-02,-0.12108310E-02, 0.14971880E-02,
         0.44425346E-04, 0.11314588E-03,-0.35955879E-03, 0.38656661E-04,
        -0.80764369E-03, 0.64800726E-03, 0.47111938E-04,-0.20804080E-04,
        -0.30286331E-03,-0.45280802E-03, 0.18036297E-03,-0.12744936E-02,
         0.65917113E-04,-0.12398277E-03,-0.16941353E-03, 0.55679586E-03,
         0.10943878E-03, 0.79310738E-03, 0.13915260E-03,-0.29419287E-03,
        -0.25562709E-03,-0.54969860E-03,-0.28991921E-04,-0.17551114E-03,
        -0.86819980E-03,-0.22579421E-03, 0.61263214E-03,-0.35166851E-03,
         0.21765872E-02, 0.69303677E-03,-0.99696314E-04, 0.85797627E-03,
        -0.47258587E-03,-0.48536152E-03, 0.12188436E-02,-0.80083514E-03,
        -0.11515460E-02,-0.47049884E-03, 0.25420214E-03, 0.41358755E-03,
        -0.48640242E-03,-0.30289043E-03, 0.49734232E-03, 0.38853541E-03,
         0.18119232E-02, 0.18161323E-03,-0.71740686E-03, 0.60600502E-03,
        -0.40872631E-03, 0.27265094E-03, 0.75584342E-03, 0.72442886E-03,
        -0.90408529E-03,-0.73502888E-03, 0.50752063E-03, 0.19282909E-03,
         0.53565658E-03,-0.11915864E-02,-0.10766074E-02, 0.10218172E-02,
        -0.47093601E-03, 0.66606153E-03, 0.72714593E-03, 0.70071989E-03,
         0.10051334E-02,-0.19586622E-03,-0.36838627E-03, 0.81817096E-04,
         0.36967976E-03,-0.78443816E-03,-0.53714164E-04,-0.50839101E-03,
         0.10492542E-02,-0.20911624E-02,-0.15207973E-02,-0.24311358E-03,
         0.55210170E-03,-0.32969884E-03,-0.24542242E-03, 0.26007425E-02,
        -0.14095631E-02, 0.11219062E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]            *x41*x52
        +coeff[  4]*x11*x23        *x51
        +coeff[  5]*x12    *x32*x41*x51
        +coeff[  6]*x11*x22*x31*x42*x51
        +coeff[  7]*x12*x23*x31    *x51
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[  8]    *x21*x31        
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]*x11    *x32        
        +coeff[ 13]*x11    *x31*x41    
        +coeff[ 14]*x11        *x41*x51
        +coeff[ 15]*x13                
        +coeff[ 16]    *x24            
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 17]            *x44    
        +coeff[ 18]        *x33    *x51
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22        *x52
        +coeff[ 21]            *x42*x52
        +coeff[ 22]    *x21        *x53
        +coeff[ 23]*x11*x21*x32        
        +coeff[ 24]*x11    *x33        
        +coeff[ 25]*x11*x21    *x42    
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 26]*x11*x22        *x51
        +coeff[ 27]*x11    *x31*x41*x51
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x11    *x31    *x52
        +coeff[ 31]*x12*x21        *x51
        +coeff[ 32]*x12    *x31    *x51
        +coeff[ 33]    *x24*x31        
        +coeff[ 34]    *x21*x33*x41    
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 35]    *x21*x33    *x51
        +coeff[ 36]    *x21*x31*x42*x51
        +coeff[ 37]        *x31*x43*x51
        +coeff[ 38]    *x22*x31    *x52
        +coeff[ 39]        *x33    *x52
        +coeff[ 40]        *x31*x42*x52
        +coeff[ 41]        *x31*x41*x53
        +coeff[ 42]*x11    *x34        
        +coeff[ 43]*x11    *x31*x43    
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 44]*x11*x21*x32    *x51
        +coeff[ 45]*x11*x22    *x41*x51
        +coeff[ 46]*x12*x22*x31        
        +coeff[ 47]*x12*x21*x31*x41    
        +coeff[ 48]*x12    *x32*x41    
        +coeff[ 49]*x12*x21    *x42    
        +coeff[ 50]*x12*x21    *x41*x51
        +coeff[ 51]        *x32*x44    
        +coeff[ 52]    *x24    *x41*x51
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 53]*x13            *x52
        +coeff[ 54]        *x34    *x52
        +coeff[ 55]    *x21*x31*x42*x52
        +coeff[ 56]    *x21*x31*x41*x53
        +coeff[ 57]            *x43*x53
        +coeff[ 58]        *x32    *x54
        +coeff[ 59]*x11*x23*x32        
        +coeff[ 60]*x11*x21*x34        
        +coeff[ 61]*x11    *x34*x41    
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 62]*x11    *x33*x42    
        +coeff[ 63]*x11*x22    *x43    
        +coeff[ 64]*x11    *x32*x43    
        +coeff[ 65]*x11*x21    *x44    
        +coeff[ 66]*x11*x22*x31    *x52
        +coeff[ 67]*x11    *x31*x41*x53
        +coeff[ 68]*x12*x21*x31*x42    
        +coeff[ 69]*x12*x23        *x51
        +coeff[ 70]*x12*x22*x31    *x51
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 71]*x12    *x33    *x51
        +coeff[ 72]*x12*x22        *x52
        +coeff[ 73]*x12    *x32    *x52
        +coeff[ 74]*x12            *x54
        +coeff[ 75]    *x22*x32*x43    
        +coeff[ 76]        *x34*x43    
        +coeff[ 77]    *x23*x33    *x51
        +coeff[ 78]    *x22*x34    *x51
        +coeff[ 79]    *x24*x31*x41*x51
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 80]    *x22*x32*x42*x51
        +coeff[ 81]    *x21*x31*x44*x51
        +coeff[ 82]    *x24*x31    *x52
        +coeff[ 83]    *x23*x32    *x52
        +coeff[ 84]    *x21*x34    *x52
        +coeff[ 85]    *x21    *x43*x53
        +coeff[ 86]            *x44*x53
        +coeff[ 87]    *x22*x31    *x54
        +coeff[ 88]        *x33    *x54
    ;
    v_l_e_q1en_1_1200                              =v_l_e_q1en_1_1200                              
        +coeff[ 89]*x11*x23*x32    *x51
        ;

    return v_l_e_q1en_1_1200                              ;
}
float x_e_q1en_1_1100                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.3397115E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.33667916E-03, 0.40010461E-02, 0.77088773E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_1100                              ;
}
float t_e_q1en_1_1100                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2494720E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.24767252E-03, 0.60069252E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_1100                              ;
}
float y_e_q1en_1_1100                              (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.7671071E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.77377982E-03, 0.38546439E-01, 0.53990360E-01,-0.70923790E-06,
         0.29580451E-05, 0.54571028E-05, 0.32227474E-05,-0.68351378E-05,
         0.12836890E-04,-0.64063247E-05, 0.87043845E-05,-0.11689437E-04,
        -0.69270650E-05, 0.18404224E-04,-0.17730896E-04, 0.15124374E-04,
        -0.77139193E-05,-0.10696008E-04, 0.10736987E-04, 0.11699431E-04,
        -0.13211506E-04, 0.40627169E-05, 0.10419765E-04,-0.10603278E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31*x41    
        +coeff[  4]    *x21    *x42    
        +coeff[  5]    *x22    *x41    
        +coeff[  6]*x11*x21    *x41    
        +coeff[  7]    *x21    *x43    
    ;
    v_y_e_q1en_1_1100                              =v_y_e_q1en_1_1100                              
        +coeff[  8]*x12        *x41    
        +coeff[  9]    *x21*x31*x42    
        +coeff[ 10]    *x21*x31*x41*x51
        +coeff[ 11]        *x31*x44    
        +coeff[ 12]*x11*x21    *x41*x51
        +coeff[ 13]        *x31*x42*x52
        +coeff[ 14]*x12*x22    *x41    
        +coeff[ 15]    *x22    *x41*x52
        +coeff[ 16]        *x32*x41*x52
    ;
    v_y_e_q1en_1_1100                              =v_y_e_q1en_1_1100                              
        +coeff[ 17]    *x22*x31    *x52
        +coeff[ 18]*x11*x21*x31*x43    
        +coeff[ 19]*x13    *x31*x41    
        +coeff[ 20]*x11    *x31*x41*x52
        +coeff[ 21]    *x22        *x53
        +coeff[ 22]*x12*x21    *x43    
        +coeff[ 23]        *x31*x43*x52
        ;

    return v_y_e_q1en_1_1100                              ;
}
float p_e_q1en_1_1100                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1796777E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.19117758E-03, 0.30040611E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_1100                              ;
}
float l_e_q1en_1_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9715972E-03;
    float xmin[10]={
        -0.39999E-02,-0.60071E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.95008657E-03, 0.14800795E-03,-0.23983708E-02,-0.23423875E-03,
        -0.17847698E-03, 0.17540890E-03,-0.69950236E-03, 0.54711639E-03,
         0.10481772E-02, 0.38217654E-03,-0.13949110E-02,-0.17414321E-03,
         0.17397149E-03, 0.30279945E-03,-0.12309450E-04, 0.30211509E-05,
        -0.12815133E-03,-0.20374783E-03, 0.29499817E-04,-0.29896112E-03,
         0.21064433E-03,-0.72252537E-05, 0.72518087E-04,-0.91557476E-05,
         0.29754435E-03, 0.68730849E-03,-0.99197926E-03,-0.40777909E-03,
        -0.84812025E-03, 0.13816956E-02,-0.54695853E-03, 0.11402917E-02,
         0.21709979E-03, 0.22898737E-03, 0.34170045E-03, 0.34813256E-04,
         0.24397932E-03,-0.55855856E-03,-0.31366682E-03,-0.27217108E-03,
        -0.77433727E-03,-0.68924366E-03, 0.76274405E-03,-0.42266992E-03,
         0.59033983E-03,-0.38880308E-03, 0.28547249E-03, 0.64486044E-03,
        -0.51670609E-03, 0.23911230E-03,-0.49107691E-03, 0.34080859E-03,
        -0.36588951E-03, 0.40287353E-03, 0.79430995E-03,-0.14125814E-02,
        -0.36088764E-03,-0.12010932E-02,-0.37745957E-03, 0.53130410E-03,
        -0.45226823E-03,-0.15926900E-02, 0.13073672E-02,-0.23219106E-03,
         0.30949910E-03, 0.59798337E-03,-0.51232806E-03,-0.24596282E-03,
         0.23479715E-03, 0.31203526E-03,-0.20925576E-03,-0.13540809E-02,
         0.85889420E-03,-0.48475788E-03,-0.38620495E-03, 0.27504898E-03,
         0.89566404E-03,-0.33035321E-03, 0.10872421E-03, 0.31535709E-03,
        -0.83780050E-03,-0.23739912E-03, 0.18379904E-02, 0.77808567E-03,
        -0.10093293E-02, 0.12887082E-02,-0.60840964E-03, 0.10437132E-02,
        -0.12873752E-02, 0.27362216E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]            *x42    
        +coeff[  4]*x12                
        +coeff[  5]*x12*x22            
        +coeff[  6]    *x21*x31*x42*x51
        +coeff[  7]    *x21*x33*x43    
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[  8]*x11*x23    *x42*x51
        +coeff[  9]*x13*x24            
        +coeff[ 10]    *x24    *x41*x53
        +coeff[ 11]*x11                
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21    *x41    
        +coeff[ 15]*x11*x21            
        +coeff[ 16]*x11    *x31        
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 17]*x11        *x41    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]    *x21    *x42    
        +coeff[ 21]        *x31*x42    
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]*x11    *x32        
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22*x31*x41    
        +coeff[ 30]        *x32*x42    
        +coeff[ 31]    *x22*x31    *x51
        +coeff[ 32]    *x22    *x41*x51
        +coeff[ 33]*x11    *x32*x41    
        +coeff[ 34]*x11    *x32    *x51
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 35]*x11    *x31*x41*x51
        +coeff[ 36]*x11    *x31    *x52
        +coeff[ 37]*x12        *x42    
        +coeff[ 38]*x12        *x41*x51
        +coeff[ 39]*x13            *x51
        +coeff[ 40]*x11*x21*x33        
        +coeff[ 41]*x11    *x34        
        +coeff[ 42]*x11*x23        *x51
        +coeff[ 43]*x11*x22    *x41*x51
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 44]*x11        *x43*x51
        +coeff[ 45]*x11    *x32    *x52
        +coeff[ 46]*x11*x21    *x41*x52
        +coeff[ 47]*x11*x21        *x53
        +coeff[ 48]*x12*x21    *x42    
        +coeff[ 49]*x12*x22        *x51
        +coeff[ 50]*x12*x21    *x41*x51
        +coeff[ 51]*x12*x21        *x52
        +coeff[ 52]*x12    *x31    *x52
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 53]*x13    *x32        
        +coeff[ 54]    *x24*x32        
        +coeff[ 55]    *x24*x31*x41    
        +coeff[ 56]    *x22*x32*x42    
        +coeff[ 57]    *x24*x31    *x51
        +coeff[ 58]*x13        *x41*x51
        +coeff[ 59]    *x22*x32*x41*x51
        +coeff[ 60]        *x33*x41*x52
        +coeff[ 61]    *x22    *x42*x52
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 62]        *x32*x42*x52
        +coeff[ 63]        *x33    *x53
        +coeff[ 64]            *x43*x53
        +coeff[ 65]    *x22        *x54
        +coeff[ 66]        *x32    *x54
        +coeff[ 67]*x11*x24*x31        
        +coeff[ 68]*x11*x24    *x41    
        +coeff[ 69]*x11*x21*x33*x41    
        +coeff[ 70]*x11*x21*x32*x42    
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 71]*x11    *x33*x41*x51
        +coeff[ 72]*x11    *x31*x43*x51
        +coeff[ 73]*x11*x21    *x42*x52
        +coeff[ 74]*x11*x22        *x53
        +coeff[ 75]*x12    *x34        
        +coeff[ 76]*x12*x22    *x42    
        +coeff[ 77]*x12*x21    *x43    
        +coeff[ 78]    *x22*x31*x44    
        +coeff[ 79]*x13*x21*x31    *x51
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 80]    *x24*x31*x41*x51
        +coeff[ 81]    *x23*x32*x41*x51
        +coeff[ 82]    *x22*x33*x41*x51
        +coeff[ 83]    *x21*x34*x41*x51
        +coeff[ 84]    *x23*x31*x42*x51
        +coeff[ 85]    *x21*x33*x42*x51
        +coeff[ 86]        *x32*x44*x51
        +coeff[ 87]    *x22*x31*x42*x52
        +coeff[ 88]    *x22*x31*x41*x53
    ;
    v_l_e_q1en_1_1100                              =v_l_e_q1en_1_1100                              
        +coeff[ 89]            *x44*x53
        ;

    return v_l_e_q1en_1_1100                              ;
}
float x_e_q1en_1_1000                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.4483431E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.43354213E-03, 0.39982945E-02, 0.77067964E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_1000                              ;
}
float t_e_q1en_1_1000                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.3433976E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.33229793E-03, 0.60053349E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_1000                              ;
}
float y_e_q1en_1_1000                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.4200559E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.40931019E-03, 0.38544443E-01, 0.53990010E-01, 0.11728283E-04,
        -0.15137335E-04,-0.12542951E-04, 0.36713441E-05,-0.19369332E-04,
         0.64634764E-05, 0.72476510E-05,-0.79025767E-05, 0.87476110E-05,
        -0.36420809E-05, 0.12355674E-04,-0.80330801E-05,-0.11989294E-04,
        -0.63836001E-05,-0.59272293E-05, 0.14874106E-04,-0.17749746E-04,
         0.28993405E-04,-0.50125846E-05, 0.18599167E-04, 0.92982582E-05,
         0.56737572E-05, 0.10987003E-04,-0.11083986E-04, 0.10853656E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31*x41*x53
        +coeff[  4]*x11    *x31*x41    
        +coeff[  5]    *x21    *x41*x51
        +coeff[  6]    *x21    *x43    
        +coeff[  7]    *x22    *x42    
    ;
    v_y_e_q1en_1_1000                              =v_y_e_q1en_1_1000                              
        +coeff[  8]    *x21*x31*x41*x51
        +coeff[  9]    *x22    *x43    
        +coeff[ 10]*x11*x21    *x41*x51
        +coeff[ 11]    *x21    *x41*x52
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x22    *x42*x51
        +coeff[ 14]    *x21*x31*x42*x51
        +coeff[ 15]        *x32*x42*x51
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1en_1_1000                              =v_y_e_q1en_1_1000                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]    *x23    *x41*x51
        +coeff[ 19]    *x23    *x43    
        +coeff[ 20]    *x24    *x42    
        +coeff[ 21]*x11    *x33    *x51
        +coeff[ 22]*x13    *x31*x41    
        +coeff[ 23]    *x23*x32*x41    
        +coeff[ 24]            *x44*x52
        +coeff[ 25]    *x22*x31*x42*x51
    ;
    v_y_e_q1en_1_1000                              =v_y_e_q1en_1_1000                              
        +coeff[ 26]*x12*x21    *x43    
        +coeff[ 27]*x11*x22*x32*x41    
        ;

    return v_y_e_q1en_1_1000                              ;
}
float p_e_q1en_1_1000                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1016794E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.96281496E-04, 0.30035818E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_1000                              ;
}
float l_e_q1en_1_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9673577E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30031E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60042E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.97806531E-03,-0.23189457E-02,-0.57620002E-03, 0.59961790E-03,
        -0.19501994E-02,-0.13536297E-02, 0.49165206E-03, 0.23964469E-03,
        -0.70882619E-04, 0.13867130E-02,-0.44318309E-03,-0.30647812E-03,
        -0.48069578E-05,-0.13365322E-03,-0.44374049E-03, 0.20919509E-03,
         0.19129046E-03, 0.31052422E-03, 0.19156300E-03,-0.96507120E-03,
         0.55083586E-03,-0.47654237E-03,-0.62618073E-03,-0.87783783E-03,
         0.11107077E-03, 0.10028190E-03, 0.14842011E-02,-0.36825653E-03,
         0.90016448E-03,-0.53387594E-04,-0.14296475E-03, 0.13353105E-02,
        -0.32832450E-04, 0.33173827E-03,-0.12323331E-02,-0.63328276E-03,
        -0.16444390E-03, 0.39876101E-03,-0.14061771E-02,-0.41899813E-03,
        -0.24316057E-03,-0.34534186E-03, 0.33927677E-03,-0.47441508E-03,
         0.16515565E-03, 0.55635272E-03, 0.20680315E-03,-0.19342745E-03,
        -0.27404088E-03,-0.25259898E-03, 0.29843181E-03,-0.29143741E-03,
         0.32049563E-03,-0.39653742E-03,-0.32108018E-03, 0.89168665E-03,
         0.41288001E-03, 0.37305354E-03, 0.88841037E-03,-0.78901899E-03,
         0.17390222E-02,-0.52647234E-03,-0.51849120E-03,-0.37235266E-03,
         0.93378435E-03,-0.48940664E-03,-0.63713937E-03,-0.14631695E-02,
         0.33837621E-03,-0.61490695E-03,-0.88446504E-04, 0.45956179E-03,
         0.11778513E-02, 0.16711274E-02, 0.37463105E-03,-0.57435362E-03,
        -0.85431203E-03,-0.32519978E-02,-0.94371609E-03, 0.32991378E-03,
         0.29889946E-02, 0.58294699E-03,-0.63085795E-03, 0.85864245E-03,
         0.11599986E-02, 0.61329786E-03, 0.13076713E-02,-0.11225524E-02,
         0.14732790E-02,-0.45602518E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x31    *x51
        +coeff[  4]    *x24*x31        
        +coeff[  5]    *x22*x33        
        +coeff[  6]*x11    *x33*x41*x51
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x22*x31        
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11    *x32        
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 17]*x12            *x51
        +coeff[ 18]*x13                
        +coeff[ 19]        *x33    *x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]            *x43*x51
        +coeff[ 22]        *x31*x41*x52
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]*x11*x23            
        +coeff[ 25]*x11    *x33        
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 26]*x11    *x31*x42    
        +coeff[ 27]*x11    *x31    *x52
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]*x13    *x31        
        +coeff[ 30]    *x24    *x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21    *x44    
        +coeff[ 33]        *x33    *x52
        +coeff[ 34]    *x21    *x42*x52
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 35]        *x31    *x54
        +coeff[ 36]*x11*x24            
        +coeff[ 37]*x11*x22*x31*x41    
        +coeff[ 38]*x11*x21*x32*x41    
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x11*x21    *x41*x52
        +coeff[ 41]*x12*x22*x31        
        +coeff[ 42]*x12*x21*x32        
        +coeff[ 43]*x12    *x32    *x51
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 44]    *x23*x33        
        +coeff[ 45]    *x22*x33*x41    
        +coeff[ 46]    *x23    *x43    
        +coeff[ 47]        *x33*x43    
        +coeff[ 48]    *x22    *x44    
        +coeff[ 49]*x13    *x31    *x51
        +coeff[ 50]*x13        *x41*x51
        +coeff[ 51]    *x24    *x41*x51
        +coeff[ 52]    *x23*x31*x41*x51
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 53]    *x23*x31    *x52
        +coeff[ 54]    *x23        *x53
        +coeff[ 55]        *x33    *x53
        +coeff[ 56]            *x43*x53
        +coeff[ 57]*x11*x23*x32        
        +coeff[ 58]*x11*x22*x32*x41    
        +coeff[ 59]*x11    *x31*x44    
        +coeff[ 60]*x11*x22*x32    *x51
        +coeff[ 61]*x11    *x34    *x51
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 62]*x11*x23    *x41*x51
        +coeff[ 63]*x11*x22    *x42*x51
        +coeff[ 64]*x11*x21    *x43*x51
        +coeff[ 65]*x11*x22        *x53
        +coeff[ 66]*x11*x21    *x41*x53
        +coeff[ 67]*x11        *x41*x54
        +coeff[ 68]*x12*x23*x31        
        +coeff[ 69]*x12*x21*x32*x41    
        +coeff[ 70]*x12    *x33*x41    
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 71]*x12*x21    *x41*x52
        +coeff[ 72]*x12    *x31*x41*x52
        +coeff[ 73]    *x24*x33        
        +coeff[ 74]    *x23*x34        
        +coeff[ 75]*x13    *x32*x41    
        +coeff[ 76]*x13    *x31*x42    
        +coeff[ 77]    *x23*x32*x42    
        +coeff[ 78]    *x21*x34*x42    
        +coeff[ 79]    *x22*x31*x44    
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 80]    *x21*x32*x44    
        +coeff[ 81]    *x24*x32    *x51
        +coeff[ 82]    *x23*x31*x42*x51
        +coeff[ 83]        *x33    *x54
        +coeff[ 84]    *x21    *x42*x54
        +coeff[ 85]*x11*x22*x34        
        +coeff[ 86]*x11*x23*x32*x41    
        +coeff[ 87]*x11*x22*x33*x41    
        +coeff[ 88]*x11*x21*x34*x41    
    ;
    v_l_e_q1en_1_1000                              =v_l_e_q1en_1_1000                              
        +coeff[ 89]*x11*x24    *x41*x51
        ;

    return v_l_e_q1en_1_1000                              ;
}
float x_e_q1en_1_900                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.1839477E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30060E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.17794411E-03, 0.39989036E-02, 0.77078126E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_900                              ;
}
float t_e_q1en_1_900                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.1312817E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30060E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.12643506E-03, 0.60063597E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_900                              ;
}
float y_e_q1en_1_900                              (float *x,int m){
    int ncoeff= 18;
    float avdat=  0.6285991E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30060E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
        -0.62722858E-03, 0.38572256E-01, 0.53996101E-01, 0.15228446E-05,
         0.52853284E-05, 0.67273754E-06, 0.46748360E-05,-0.55320133E-05,
         0.62826016E-05, 0.16795764E-04,-0.10527587E-04,-0.13074528E-04,
        -0.96411841E-05, 0.90081921E-05,-0.93484459E-05,-0.68043691E-05,
        -0.68953545E-05, 0.58873470E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21*x31*x41    
        +coeff[  4]        *x32*x41    
        +coeff[  5]            *x42*x51
        +coeff[  6]*x12        *x41    
        +coeff[  7]    *x21*x31*x42    
    ;
    v_y_e_q1en_1_900                              =v_y_e_q1en_1_900                              
        +coeff[  8]        *x31*x42*x51
        +coeff[  9]    *x21*x31*x43    
        +coeff[ 10]*x11    *x31*x41*x51
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]        *x32*x42*x51
        +coeff[ 13]*x11    *x31*x42*x51
        +coeff[ 14]*x11    *x32*x41*x51
        +coeff[ 15]    *x24    *x42    
        +coeff[ 16]*x11*x22    *x43    
    ;
    v_y_e_q1en_1_900                              =v_y_e_q1en_1_900                              
        +coeff[ 17]*x12        *x44    
        ;

    return v_y_e_q1en_1_900                              ;
}
float p_e_q1en_1_900                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1287880E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30060E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.12824121E-03, 0.30060166E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_900                              ;
}
float l_e_q1en_1_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9448958E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53996E-01,-0.30060E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60059E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.95605891E-03,-0.23810498E-02,-0.44820574E-03,-0.36547676E-03,
         0.10281361E-02, 0.32839974E-03,-0.93137071E-03,-0.26919882E-03,
        -0.11798491E-03, 0.12844625E-03,-0.23349642E-03,-0.78566016E-04,
         0.62262639E-03, 0.24255556E-03,-0.30696177E-03,-0.22800214E-03,
        -0.47454951E-03, 0.13774263E-02,-0.28598279E-03, 0.26587077E-03,
         0.34925085E-03, 0.30529327E-03, 0.71416848E-03,-0.26942635E-03,
         0.61086129E-03,-0.54319171E-04, 0.13939924E-04,-0.28501765E-03,
        -0.66765718E-03, 0.23019622E-03,-0.89952105E-03,-0.82997081E-03,
         0.27150920E-03, 0.76458609E-03, 0.22790930E-03, 0.35363753E-03,
        -0.13477273E-02, 0.75447094E-03, 0.22471631E-03,-0.10776622E-02,
         0.22995868E-02, 0.67075505E-03, 0.41098541E-03,-0.15783573E-02,
         0.58253827E-04, 0.75319433E-03,-0.11237434E-02, 0.70244749E-03,
        -0.87020372E-03, 0.14365320E-03,-0.54181844E-03, 0.15030412E-02,
        -0.61393634E-03, 0.87501225E-03,-0.52564264E-04, 0.42256163E-03,
         0.12536607E-03,-0.57023874E-03,-0.31964312E-03,-0.54947537E-03,
        -0.43186202E-03, 0.10802452E-02,-0.46941114E-03, 0.80517872E-03,
        -0.71134727E-03,-0.96668419E-03,-0.63362427E-03,-0.61121525E-03,
         0.81413146E-03, 0.36040030E-03, 0.65080333E-03, 0.13359726E-02,
        -0.17320141E-03, 0.10274411E-02, 0.45380235E-03, 0.14963237E-02,
        -0.14450434E-02, 0.52026880E-03,-0.11711509E-02,-0.13080133E-02,
         0.85292215E-03, 0.33979880E-03, 0.17800020E-02,-0.52742608E-03,
        -0.42855783E-03, 0.26772459E-03, 0.44689907E-03,-0.13384795E-02,
        -0.97901456E-03,-0.25460422E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x31    *x51
        +coeff[  4]*x11    *x31*x42*x51
        +coeff[  5]        *x34*x41*x51
        +coeff[  6]*x12    *x32*x42    
        +coeff[  7]    *x22*x33*x41*x51
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[  8]        *x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x11*x22            
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]*x11    *x31    *x51
        +coeff[ 14]*x11        *x41*x51
        +coeff[ 15]*x12            *x51
        +coeff[ 16]    *x21*x31*x42    
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 17]        *x31*x42*x51
        +coeff[ 18]    *x21*x31    *x52
        +coeff[ 19]*x11*x22        *x51
        +coeff[ 20]*x11    *x32    *x51
        +coeff[ 21]*x11    *x31*x41*x51
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x24        *x51
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 26]    *x21*x31*x42*x51
        +coeff[ 27]        *x32*x41*x52
        +coeff[ 28]    *x21        *x54
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x21*x33        
        +coeff[ 31]*x11    *x33    *x51
        +coeff[ 32]*x12*x22*x31        
        +coeff[ 33]*x12            *x53
        +coeff[ 34]    *x23*x33        
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 35]    *x21*x32*x43    
        +coeff[ 36]        *x31*x44*x51
        +coeff[ 37]    *x23*x31    *x52
        +coeff[ 38]    *x22*x32    *x52
        +coeff[ 39]    *x23    *x41*x52
        +coeff[ 40]    *x21*x32*x41*x52
        +coeff[ 41]    *x21    *x43*x52
        +coeff[ 42]    *x21*x32    *x53
        +coeff[ 43]*x11*x22*x32*x41    
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 44]*x11    *x34*x41    
        +coeff[ 45]*x11*x22    *x43    
        +coeff[ 46]*x11*x24        *x51
        +coeff[ 47]*x11*x22*x32    *x51
        +coeff[ 48]*x11    *x31*x43*x51
        +coeff[ 49]*x11    *x33    *x52
        +coeff[ 50]*x11*x22    *x41*x52
        +coeff[ 51]*x11    *x32*x41*x52
        +coeff[ 52]*x11    *x32    *x53
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 53]*x11    *x31    *x54
        +coeff[ 54]*x11        *x41*x54
        +coeff[ 55]*x12*x22*x32        
        +coeff[ 56]*x12*x23    *x41    
        +coeff[ 57]*x12*x21    *x43    
        +coeff[ 58]*x12*x23        *x51
        +coeff[ 59]*x12*x21*x31*x41*x51
        +coeff[ 60]*x13*x22*x31        
        +coeff[ 61]    *x23    *x44    
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 62]    *x21*x32*x44    
        +coeff[ 63]*x13*x22        *x51
        +coeff[ 64]*x13    *x31    *x52
        +coeff[ 65]    *x23*x32    *x52
        +coeff[ 66]*x13        *x41*x52
        +coeff[ 67]    *x21    *x44*x52
        +coeff[ 68]    *x23        *x54
        +coeff[ 69]    *x21*x32    *x54
        +coeff[ 70]*x11*x24    *x42    
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 71]*x11*x24    *x41*x51
        +coeff[ 72]*x11    *x34*x41*x51
        +coeff[ 73]*x11    *x32*x43*x51
        +coeff[ 74]*x11*x24        *x52
        +coeff[ 75]*x11*x21*x33    *x52
        +coeff[ 76]*x11    *x32*x42*x52
        +coeff[ 77]*x11    *x32    *x54
        +coeff[ 78]*x12*x23    *x42    
        +coeff[ 79]*x12*x22*x31*x41*x51
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 80]*x12*x21*x31*x42*x51
        +coeff[ 81]*x12*x23        *x52
        +coeff[ 82]*x12*x21*x32    *x52
        +coeff[ 83]*x12*x22        *x53
        +coeff[ 84]*x13*x24            
        +coeff[ 85]*x13    *x34        
        +coeff[ 86]    *x24*x33    *x51
        +coeff[ 87]*x13*x22    *x41*x51
        +coeff[ 88]*x13*x21*x31    *x52
    ;
    v_l_e_q1en_1_900                              =v_l_e_q1en_1_900                              
        +coeff[ 89]    *x21*x34*x41*x52
        ;

    return v_l_e_q1en_1_900                              ;
}
float x_e_q1en_1_800                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.4550191E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60068E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.44549687E-03, 0.39984207E-02, 0.77076897E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_800                              ;
}
float t_e_q1en_1_800                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.3782081E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60068E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.37125853E-03, 0.60060799E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_800                              ;
}
float y_e_q1en_1_800                              (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.6032013E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60068E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.59379271E-03, 0.38535237E-01, 0.53994589E-01,-0.21636852E-05,
        -0.41728899E-05, 0.68018012E-05,-0.51976340E-05,-0.82065153E-05,
         0.86900900E-05,-0.70467613E-05,-0.20985013E-04, 0.58878863E-05,
         0.81375101E-05, 0.13629357E-04,-0.99927975E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x42    
        +coeff[  4]*x11        *x41*x51
        +coeff[  5]        *x33*x42    
        +coeff[  6]*x12        *x41*x51
        +coeff[  7]    *x21*x31*x42*x51
    ;
    v_y_e_q1en_1_800                              =v_y_e_q1en_1_800                              
        +coeff[  8]        *x32*x42*x51
        +coeff[  9]*x11*x22*x31*x41    
        +coeff[ 10]*x11*x21*x32*x41    
        +coeff[ 11]        *x31*x44*x51
        +coeff[ 12]*x13*x21    *x41    
        +coeff[ 13]*x11*x21*x32*x42    
        +coeff[ 14]*x11*x21*x33*x41    
        ;

    return v_y_e_q1en_1_800                              ;
}
float p_e_q1en_1_800                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.5392551E-04;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60068E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.58821330E-04, 0.30028911E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_800                              ;
}
float l_e_q1en_1_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9519872E-03;
    float xmin[10]={
        -0.39991E-02,-0.60054E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60068E-01, 0.53992E-01, 0.30024E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.94098854E-03,-0.26157573E-02,-0.46987139E-03,-0.67904493E-03,
         0.19529674E-02,-0.61869610E-03,-0.24775276E-02, 0.47902472E-06,
         0.98745593E-04,-0.15882477E-03, 0.26849305E-05, 0.14743020E-03,
         0.14189495E-03,-0.17519048E-03,-0.91489062E-04, 0.38474865E-03,
        -0.29692039E-03, 0.30909677E-03,-0.24927349E-03,-0.20471734E-02,
        -0.64044274E-04, 0.19005203E-02,-0.82834152E-03, 0.73369505E-03,
        -0.58453123E-03,-0.16559735E-02,-0.17235114E-03, 0.41967076E-04,
         0.24739353E-03, 0.28497473E-03, 0.58047369E-03,-0.56179176E-03,
        -0.28980276E-03, 0.44276577E-03,-0.26903462E-03, 0.20758430E-02,
         0.21149783E-03, 0.26667025E-03, 0.13659162E-02, 0.17068173E-02,
         0.61632418E-04, 0.59130264E-03, 0.14491376E-02, 0.13766420E-02,
        -0.71602693E-03,-0.10188711E-02,-0.17390596E-02, 0.33370583E-03,
         0.64975338E-03,-0.58950583E-03,-0.28599138E-03, 0.33581242E-03,
        -0.93053392E-03, 0.52406266E-03, 0.74792618E-03,-0.76865050E-03,
         0.10594233E-02, 0.69337356E-03, 0.19937479E-02, 0.22036969E-02,
         0.37318928E-03,-0.20183807E-02,-0.82571997E-03, 0.60994178E-03,
        -0.10926910E-02,-0.29218648E-03, 0.49398665E-03, 0.32356201E-03,
         0.13141592E-02,-0.44602100E-03,-0.28020353E-03,-0.17607130E-04,
        -0.13392403E-02,-0.10147428E-02,-0.11217432E-02,-0.96047670E-03,
         0.61946426E-03, 0.35794592E-03,-0.28564260E-03, 0.53003873E-03,
        -0.79061207E-03, 0.51183178E-03,-0.11030997E-02, 0.43202908E-03,
        -0.26001928E-02, 0.12876686E-02, 0.10998182E-02,-0.14235679E-02,
        -0.95158356E-03,-0.16494128E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x11*x21*x32        
        +coeff[  4]    *x23*x31*x43*x51
        +coeff[  5]    *x22*x32*x43*x51
        +coeff[  6]    *x21*x31*x43*x53
        +coeff[  7]            *x41    
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21        *x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11            *x51
        +coeff[ 12]            *x43    
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]*x12            *x51
        +coeff[ 15]    *x24            
        +coeff[ 16]        *x33*x41    
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x31    *x52
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]*x11*x21*x31*x41    
        +coeff[ 22]*x11*x22        *x51
        +coeff[ 23]*x11    *x32    *x51
        +coeff[ 24]*x12*x21        *x51
        +coeff[ 25]    *x22*x31*x41*x51
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 26]    *x22        *x53
        +coeff[ 27]        *x31    *x54
        +coeff[ 28]*x11*x21*x33        
        +coeff[ 29]*x11*x21*x32*x41    
        +coeff[ 30]*x11    *x31*x41*x52
        +coeff[ 31]*x11        *x42*x52
        +coeff[ 32]*x12*x22    *x41    
        +coeff[ 33]*x12*x21*x31*x41    
        +coeff[ 34]*x12*x21    *x41*x51
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 35]*x12        *x42*x51
        +coeff[ 36]*x13    *x31*x41    
        +coeff[ 37]*x13        *x42    
        +coeff[ 38]    *x21*x32*x42*x51
        +coeff[ 39]    *x23*x31    *x52
        +coeff[ 40]    *x22*x32    *x52
        +coeff[ 41]        *x34    *x52
        +coeff[ 42]    *x21*x31    *x54
        +coeff[ 43]*x11*x21*x34        
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 44]*x11*x24    *x41    
        +coeff[ 45]*x11*x23*x31*x41    
        +coeff[ 46]*x11*x21*x33*x41    
        +coeff[ 47]*x11*x22*x31*x42    
        +coeff[ 48]*x11*x22    *x43    
        +coeff[ 49]*x11*x21*x32*x41*x51
        +coeff[ 50]*x11    *x31*x43*x51
        +coeff[ 51]*x11*x21*x31    *x53
        +coeff[ 52]*x11    *x32    *x53
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 53]*x12*x21*x33        
        +coeff[ 54]*x12*x23        *x51
        +coeff[ 55]*x12*x21*x32    *x51
        +coeff[ 56]*x12*x21        *x53
        +coeff[ 57]*x13*x22        *x51
        +coeff[ 58]    *x24*x31*x41*x51
        +coeff[ 59]    *x24*x31    *x52
        +coeff[ 60]    *x23*x31*x41*x52
        +coeff[ 61]    *x22*x31    *x54
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 62]    *x21*x31*x41*x54
        +coeff[ 63]*x11*x24    *x42    
        +coeff[ 64]*x11*x21*x32*x43    
        +coeff[ 65]*x11*x24        *x52
        +coeff[ 66]*x11*x23    *x41*x52
        +coeff[ 67]*x11    *x33    *x53
        +coeff[ 68]*x11*x22        *x54
        +coeff[ 69]*x12*x24*x31        
        +coeff[ 70]*x12*x21*x32*x42    
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 71]*x12    *x32*x43    
        +coeff[ 72]*x12        *x44*x51
        +coeff[ 73]*x12*x23        *x52
        +coeff[ 74]*x12    *x32*x41*x52
        +coeff[ 75]*x12        *x42*x53
        +coeff[ 76]*x12*x21        *x54
        +coeff[ 77]*x12        *x41*x54
        +coeff[ 78]*x13*x24            
        +coeff[ 79]*x13*x22*x32        
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 80]*x13    *x33*x41    
        +coeff[ 81]*x13*x21*x31*x41*x51
        +coeff[ 82]    *x24*x32*x41*x51
        +coeff[ 83]    *x24*x31*x42*x51
        +coeff[ 84]    *x23*x32*x42*x51
        +coeff[ 85]    *x21*x33*x43*x51
        +coeff[ 86]    *x23    *x44*x51
        +coeff[ 87]*x13*x22        *x52
        +coeff[ 88]    *x23*x33    *x52
    ;
    v_l_e_q1en_1_800                              =v_l_e_q1en_1_800                              
        +coeff[ 89]    *x22*x34    *x52
        ;

    return v_l_e_q1en_1_800                              ;
}
float x_e_q1en_1_700                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.7172621E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30018E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.72260923E-03, 0.40020812E-02, 0.77075683E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_700                              ;
}
float t_e_q1en_1_700                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.5563105E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30018E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.56025881E-03, 0.60063001E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_700                              ;
}
float y_e_q1en_1_700                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.1005125E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30018E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.88537941E-04, 0.38532078E-01, 0.53990856E-01, 0.10554342E-04,
        -0.17963050E-04,-0.15965968E-04,-0.29090555E-04,-0.73551000E-05,
         0.42240167E-05,-0.62522208E-05,-0.10040202E-04,-0.64962323E-05,
        -0.29563273E-05, 0.66507728E-05,-0.52443315E-05,-0.21645683E-04,
         0.63662069E-05,-0.18182447E-04,-0.11682953E-04,-0.57987490E-05,
         0.98398687E-05, 0.74514141E-05, 0.10536931E-04, 0.19053659E-04,
         0.14531602E-04, 0.14242540E-04, 0.30536215E-04, 0.10110035E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]*x11*x21    *x43    
        +coeff[  4]*x11*x22    *x42    
        +coeff[  5]*x12        *x43    
        +coeff[  6]*x11*x22    *x44*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q1en_1_700                              =v_y_e_q1en_1_700                              
        +coeff[  8]*x11        *x42    
        +coeff[  9]            *x42*x51
        +coeff[ 10]        *x31*x43    
        +coeff[ 11]    *x21*x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]    *x22*x31*x41    
        +coeff[ 14]*x11*x21    *x42    
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]        *x31*x41*x52
    ;
    v_y_e_q1en_1_700                              =v_y_e_q1en_1_700                              
        +coeff[ 17]*x11    *x31*x43    
        +coeff[ 18]    *x21    *x43*x51
        +coeff[ 19]*x13        *x41    
        +coeff[ 20]    *x22    *x42*x51
        +coeff[ 21]            *x43*x52
        +coeff[ 22]*x12*x22    *x41    
        +coeff[ 23]    *x21*x33*x42    
        +coeff[ 24]*x13    *x31*x41    
        +coeff[ 25]*x12*x21    *x41*x51
    ;
    v_y_e_q1en_1_700                              =v_y_e_q1en_1_700                              
        +coeff[ 26]    *x23    *x42*x51
        +coeff[ 27]*x11*x24    *x41    
        ;

    return v_y_e_q1en_1_700                              ;
}
float p_e_q1en_1_700                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.9395612E-04;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30018E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.10005495E-03, 0.30024115E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_700                              ;
}
float l_e_q1en_1_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9477981E-03;
    float xmin[10]={
        -0.39994E-02,-0.60059E-01,-0.53987E-01,-0.30018E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53995E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.88407786E-03,-0.20499055E-02,-0.62082830E-03,-0.13134703E-03,
         0.12178938E-02, 0.74097718E-03,-0.31089882E-03, 0.10098156E-02,
        -0.49599278E-03, 0.15811414E-02, 0.27607682E-02, 0.45689103E-04,
         0.18380053E-03,-0.77250406E-04,-0.15745896E-03, 0.83082390E-03,
         0.35143303E-03,-0.23687108E-03, 0.55553165E-03, 0.53137273E-03,
        -0.61536790E-03, 0.94544535E-04,-0.26295442E-03, 0.61284233E-03,
        -0.36313482E-04,-0.23610782E-03,-0.14024468E-03, 0.22901634E-03,
        -0.57416153E-03, 0.16081864E-03, 0.21405512E-03,-0.30833911E-03,
        -0.19604710E-02, 0.16733543E-02,-0.56060764E-03,-0.86246070E-03,
         0.27088713E-03,-0.74937841E-03, 0.26513849E-05,-0.76236506E-03,
        -0.98216499E-03, 0.10121375E-02,-0.82110759E-03, 0.40290973E-03,
         0.76907896E-03, 0.38905404E-03,-0.14303286E-02,-0.24289919E-03,
        -0.92249253E-03,-0.25300417E-03, 0.29523129E-03,-0.22948436E-03,
        -0.56040037E-03,-0.64118207E-03,-0.26677598E-03, 0.79351907E-04,
        -0.63056988E-03, 0.46818179E-04, 0.43386544E-03, 0.45884924E-03,
         0.28857810E-04, 0.14620835E-02,-0.42022578E-03, 0.91145700E-03,
         0.20580490E-02,-0.16467085E-02, 0.49712369E-04, 0.50406042E-03,
         0.57723839E-03, 0.41620634E-03, 0.11251202E-02, 0.44384538E-03,
        -0.11118790E-02,-0.66966622E-03,-0.19013157E-03, 0.65592525E-03,
        -0.71821921E-03,-0.41282192E-03,-0.69169450E-03, 0.58461737E-03,
        -0.37693323E-03,-0.69133460E-03, 0.72495465E-03,-0.51959290E-03,
        -0.15345126E-02, 0.66054519E-03, 0.14179709E-02,-0.10119480E-02,
        -0.96745545E-03, 0.10427557E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x11*x21            
        +coeff[  4]*x11*x22    *x41    
        +coeff[  5]*x12        *x42    
        +coeff[  6]    *x21*x33*x41    
        +coeff[  7]*x11*x22    *x41*x51
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[  8]*x12*x24            
        +coeff[  9]*x12    *x32*x42*x51
        +coeff[ 10]*x13        *x42*x52
        +coeff[ 11]*x11                
        +coeff[ 12]            *x41*x51
        +coeff[ 13]                *x52
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]    *x21    *x42    
        +coeff[ 16]        *x31*x42    
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]    *x21    *x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x21    *x43    
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21    *x41*x52
        +coeff[ 29]*x11*x21*x32        
        +coeff[ 30]*x11*x21*x31*x41    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]*x11*x22        *x51
        +coeff[ 33]*x11        *x42*x51
        +coeff[ 34]*x12*x21        *x51
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 35]    *x24*x31        
        +coeff[ 36]    *x23*x32        
        +coeff[ 37]    *x21    *x44    
        +coeff[ 38]    *x24        *x51
        +coeff[ 39]    *x23    *x41*x51
        +coeff[ 40]            *x44*x51
        +coeff[ 41]    *x22*x31    *x52
        +coeff[ 42]        *x33    *x52
        +coeff[ 43]    *x21        *x54
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 44]*x11*x21*x32*x41    
        +coeff[ 45]*x11    *x33    *x51
        +coeff[ 46]*x11        *x42*x52
        +coeff[ 47]*x11*x21        *x53
        +coeff[ 48]*x11    *x31    *x53
        +coeff[ 49]*x12*x23            
        +coeff[ 50]*x12    *x33        
        +coeff[ 51]*x12    *x31*x42    
        +coeff[ 52]    *x23*x33        
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 53]*x13*x21    *x41    
        +coeff[ 54]        *x32*x44    
        +coeff[ 55]    *x23*x32    *x51
        +coeff[ 56]    *x22*x32*x41*x51
        +coeff[ 57]    *x23    *x42*x51
        +coeff[ 58]    *x21    *x44*x51
        +coeff[ 59]    *x23*x31    *x52
        +coeff[ 60]    *x21*x32*x41*x52
        +coeff[ 61]    *x21    *x41*x54
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 62]*x11*x22*x32*x41    
        +coeff[ 63]*x11    *x33*x42    
        +coeff[ 64]*x11*x24        *x51
        +coeff[ 65]*x11        *x44*x51
        +coeff[ 66]*x11*x22*x31    *x52
        +coeff[ 67]*x11*x21*x32    *x52
        +coeff[ 68]*x11*x22        *x53
        +coeff[ 69]*x12*x23*x31        
        +coeff[ 70]*x12*x21*x32*x41    
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 71]*x12*x21*x32    *x51
        +coeff[ 72]*x12*x21    *x41*x52
        +coeff[ 73]*x12        *x42*x52
        +coeff[ 74]*x13*x22*x31        
        +coeff[ 75]    *x24*x33        
        +coeff[ 76]*x13*x22    *x41    
        +coeff[ 77]*x13    *x31*x42    
        +coeff[ 78]*x13*x22        *x51
        +coeff[ 79]    *x24    *x42*x51
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 80]*x13    *x31    *x52
        +coeff[ 81]    *x22*x31*x42*x52
        +coeff[ 82]        *x33*x41*x53
        +coeff[ 83]        *x31*x43*x53
        +coeff[ 84]*x11*x21*x34*x41    
        +coeff[ 85]*x11*x22*x32*x42    
        +coeff[ 86]*x11*x23*x32    *x51
        +coeff[ 87]*x11*x22*x32*x41*x51
        +coeff[ 88]*x11*x21*x32    *x53
    ;
    v_l_e_q1en_1_700                              =v_l_e_q1en_1_700                              
        +coeff[ 89]*x11    *x33    *x53
        ;

    return v_l_e_q1en_1_700                              ;
}
float x_e_q1en_1_600                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.1053028E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30039E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30041E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.97078700E-04, 0.39992332E-02, 0.77077597E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_600                              ;
}
float t_e_q1en_1_600                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.8677797E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30039E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30041E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.80273610E-04, 0.60062952E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_600                              ;
}
float y_e_q1en_1_600                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.1988267E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30039E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30041E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.20046486E-03, 0.38552355E-01, 0.53999294E-01,-0.14276037E-04,
         0.36884751E-05,-0.41454809E-05, 0.14884638E-06,-0.48336624E-05,
         0.56331046E-05, 0.11381659E-04,-0.11228085E-04, 0.69862758E-05,
         0.16981212E-04,-0.18227111E-04,-0.10251230E-04,-0.17386152E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x22*x32*x41    
        +coeff[  4]        *x31*x41*x51
        +coeff[  5]        *x32*x42    
        +coeff[  6]*x11    *x31    *x51
        +coeff[  7]*x11*x21    *x42    
    ;
    v_y_e_q1en_1_600                              =v_y_e_q1en_1_600                              
        +coeff[  8]*x12*x21    *x41    
        +coeff[  9]    *x22*x31*x42    
        +coeff[ 10]*x11    *x31*x42*x51
        +coeff[ 11]    *x22*x31*x43    
        +coeff[ 12]    *x22    *x43*x51
        +coeff[ 13]*x12*x21    *x43    
        +coeff[ 14]*x11*x21*x33*x41    
        +coeff[ 15]    *x22*x32*x41*x51
        ;

    return v_y_e_q1en_1_600                              ;
}
float p_e_q1en_1_600                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.1239054E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30039E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30041E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.12460667E-03, 0.30039912E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_600                              ;
}
float l_e_q1en_1_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9699005E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53999E-01,-0.30039E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60056E-01, 0.54000E-01, 0.30041E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.10179892E-02,-0.21645662E-02,-0.30753823E-03,-0.81557262E-03,
         0.28009134E-04,-0.26728914E-03,-0.77666907E-03, 0.11389819E-02,
         0.17418573E-02, 0.10344865E-02, 0.13871414E-02,-0.34938868E-04,
         0.11210343E-03, 0.27060632E-04,-0.12514494E-04,-0.19968051E-04,
        -0.10309114E-03,-0.45749589E-03,-0.29891325E-03,-0.86140273E-04,
         0.24701712E-04,-0.30823701E-03,-0.36930843E-03,-0.14864969E-02,
        -0.45794662E-03, 0.19639348E-03,-0.13282768E-03, 0.14780459E-02,
         0.65821654E-03, 0.57656749E-03,-0.11011067E-03,-0.35765828E-03,
         0.25001430E-03,-0.67016954E-03, 0.26602298E-02,-0.14042178E-02,
         0.46228681E-03,-0.19529321E-03,-0.10680138E-02, 0.52153011E-03,
        -0.37188485E-03, 0.43665082E-03, 0.68140699E-03,-0.57923485E-03,
        -0.50203281E-03,-0.15665871E-02, 0.41204412E-03, 0.28092455E-03,
         0.14292767E-02,-0.81515906E-03, 0.10410594E-03, 0.49866014E-03,
         0.92052110E-03, 0.10094979E-02, 0.12747196E-02, 0.11089270E-02,
         0.13827229E-02,-0.84055529E-03,-0.66538499E-03,-0.11633262E-02,
         0.12226535E-02, 0.22299812E-03, 0.41474562E-03, 0.63527288E-03,
        -0.77483023E-03,-0.49778074E-03,-0.11793418E-02, 0.14196493E-02,
        -0.80280867E-03, 0.65584498E-03,-0.14678012E-02, 0.13574533E-02,
        -0.12329874E-02,-0.15616197E-02,-0.28713149E-03,-0.25713199E-03,
         0.14280343E-02, 0.84762141E-03, 0.19356362E-03, 0.12871579E-02,
        -0.99398626E-03,-0.16211092E-02, 0.58146584E-03,-0.10683257E-02,
         0.13518358E-02, 0.12481314E-02,-0.10964205E-02,-0.11692719E-02,
         0.11036568E-02,-0.75143413E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x33    *x51
        +coeff[  5]*x11    *x33        
        +coeff[  6]    *x22*x33*x41    
        +coeff[  7]    *x22*x32*x42    
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[  8]        *x31*x41*x54
        +coeff[  9]*x11*x22*x31*x41*x51
        +coeff[ 10]    *x24*x31    *x53
        +coeff[ 11]        *x31        
        +coeff[ 12]    *x21        *x51
        +coeff[ 13]*x12                
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 17]*x11    *x31    *x51
        +coeff[ 18]    *x24            
        +coeff[ 19]        *x34        
        +coeff[ 20]        *x31*x41*x52
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]            *x41*x53
        +coeff[ 23]*x11*x21    *x41*x51
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]        *x32*x43    
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 26]    *x22*x31*x41*x51
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]        *x31*x43*x51
        +coeff[ 29]    *x21*x31*x41*x52
        +coeff[ 30]    *x22        *x53
        +coeff[ 31]        *x31*x41*x53
        +coeff[ 32]            *x42*x53
        +coeff[ 33]*x11*x23    *x41    
        +coeff[ 34]*x11    *x31*x42*x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 35]*x11*x21    *x41*x52
        +coeff[ 36]*x11*x21        *x53
        +coeff[ 37]*x12*x21*x32        
        +coeff[ 38]*x12*x21*x31*x41    
        +coeff[ 39]*x12*x21    *x42    
        +coeff[ 40]*x12    *x31*x41*x51
        +coeff[ 41]*x13*x21    *x41    
        +coeff[ 42]        *x33*x43    
        +coeff[ 43]    *x22*x33    *x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 44]    *x21*x31*x42*x52
        +coeff[ 45]        *x31*x43*x52
        +coeff[ 46]            *x43*x53
        +coeff[ 47]*x11*x23*x32        
        +coeff[ 48]*x11*x23*x31*x41    
        +coeff[ 49]*x11*x21*x33*x41    
        +coeff[ 50]*x11    *x34*x41    
        +coeff[ 51]*x11*x22*x31*x42    
        +coeff[ 52]*x11*x23*x31    *x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 53]*x11*x23    *x41*x51
        +coeff[ 54]*x11    *x33*x41*x51
        +coeff[ 55]*x11*x21    *x43*x51
        +coeff[ 56]*x11*x21*x31*x41*x52
        +coeff[ 57]*x11    *x32*x41*x52
        +coeff[ 58]*x11*x21*x31    *x53
        +coeff[ 59]*x11    *x31*x41*x53
        +coeff[ 60]*x11        *x41*x54
        +coeff[ 61]*x12*x21*x33        
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 62]*x12*x22    *x42    
        +coeff[ 63]*x12    *x31*x43    
        +coeff[ 64]*x12    *x31*x41*x52
        +coeff[ 65]    *x24*x33        
        +coeff[ 66]*x13*x21*x31*x41    
        +coeff[ 67]    *x22*x33*x42    
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]    *x24*x32    *x51
        +coeff[ 70]    *x24*x31*x41*x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 71]    *x22*x33*x41*x51
        +coeff[ 72]    *x24    *x42*x51
        +coeff[ 73]    *x22    *x44*x51
        +coeff[ 74]    *x22    *x43*x52
        +coeff[ 75]        *x34    *x53
        +coeff[ 76]*x11*x21*x33*x42    
        +coeff[ 77]*x11*x23    *x43    
        +coeff[ 78]*x11*x21*x32*x43    
        +coeff[ 79]*x11*x24    *x41*x51
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 80]*x11*x22    *x43*x51
        +coeff[ 81]*x11    *x31*x44*x51
        +coeff[ 82]*x11*x23*x31    *x52
        +coeff[ 83]*x11*x21*x33    *x52
        +coeff[ 84]*x11*x23    *x41*x52
        +coeff[ 85]*x11*x21*x32*x41*x52
        +coeff[ 86]*x11*x21*x31*x42*x52
        +coeff[ 87]*x11    *x31*x42*x53
        +coeff[ 88]*x12*x21*x31*x43    
    ;
    v_l_e_q1en_1_600                              =v_l_e_q1en_1_600                              
        +coeff[ 89]*x12*x21*x32    *x52
        ;

    return v_l_e_q1en_1_600                              ;
}
float x_e_q1en_1_500                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.7260653E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.73069212E-03, 0.39995434E-02, 0.77082917E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_500                              ;
}
float t_e_q1en_1_500                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.5583736E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.56122657E-03, 0.60068198E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_500                              ;
}
float y_e_q1en_1_500                              (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.9298771E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.73696589E-04, 0.38541816E-01, 0.53994969E-01,-0.21954942E-04,
         0.20679170E-05,-0.40624859E-05, 0.59867211E-05, 0.46118744E-05,
        -0.43901282E-05,-0.63243347E-05,-0.77136810E-05,-0.84816338E-05,
         0.10144462E-04, 0.65305289E-05,-0.40385539E-05, 0.66017428E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x22*x32*x44    
        +coeff[  4]    *x21    *x41    
        +coeff[  5]    *x22    *x41    
        +coeff[  6]        *x31*x43    
        +coeff[  7]            *x41*x52
    ;
    v_y_e_q1en_1_500                              =v_y_e_q1en_1_500                              
        +coeff[  8]    *x21*x31*x42    
        +coeff[  9]*x12    *x31*x41    
        +coeff[ 10]*x12        *x43    
        +coeff[ 11]*x11*x21    *x42*x51
        +coeff[ 12]*x12        *x42*x51
        +coeff[ 13]        *x32*x45    
        +coeff[ 14]    *x22        *x53
        +coeff[ 15]    *x24    *x41*x51
        ;

    return v_y_e_q1en_1_500                              ;
}
float p_e_q1en_1_500                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.1883747E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.17552215E-03,-0.34971637E-08, 0.30033562E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x21 = x2;
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]            *x41    
        ;

    return v_p_e_q1en_1_500                              ;
}
float l_e_q1en_1_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9554409E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53998E-01,-0.30047E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.88086131E-03,-0.26605399E-02,-0.39914713E-03, 0.34780169E-03,
        -0.17912860E-02,-0.34504780E-02,-0.95227457E-04,-0.25220166E-03,
         0.26200483E-04, 0.22948199E-03,-0.22290857E-03, 0.10355291E-03,
        -0.86778600E-05, 0.10497496E-02,-0.27390174E-03, 0.65649895E-03,
         0.10295192E-02, 0.13676164E-02, 0.20727685E-03, 0.70731260E-03,
         0.40072820E-03, 0.25100372E-03, 0.41001558E-03,-0.82598138E-03,
         0.32628776E-03, 0.10789899E-04, 0.13260433E-03,-0.57620468E-03,
         0.27545640E-03, 0.38740577E-03, 0.10809248E-02,-0.13760952E-02,
         0.26076066E-03,-0.62752188E-04,-0.44223055E-03, 0.18650622E-03,
         0.17956574E-03,-0.64377981E-03,-0.28031546E-03,-0.90411224E-03,
        -0.24206832E-03, 0.10582437E-02,-0.50888420E-03,-0.98936132E-03,
        -0.95377536E-03,-0.12994948E-02, 0.40492593E-03, 0.66914252E-03,
        -0.35004952E-03,-0.16282914E-02,-0.10930666E-02, 0.46764742E-03,
         0.12668193E-02,-0.16020237E-02, 0.13289439E-02, 0.66739216E-03,
        -0.52376703E-03,-0.13371505E-03,-0.53549226E-03, 0.12814290E-02,
        -0.89065760E-03,-0.74414868E-03, 0.71732729E-03, 0.14282389E-02,
        -0.14064690E-02,-0.28044774E-03,-0.10854631E-02,-0.48527561E-03,
         0.59151190E-03,-0.20651500E-03, 0.69793471E-03,-0.82086877E-03,
         0.13785818E-03,-0.47571503E-03,-0.16272902E-02, 0.18422266E-02,
         0.10653442E-02, 0.10149359E-02, 0.67664700E-03, 0.11493178E-02,
        -0.89736673E-03, 0.53305342E-03, 0.52145007E-03,-0.58806979E-03,
         0.22562340E-03,-0.15245519E-02, 0.41031846E-03, 0.61153009E-03,
         0.14819780E-02,-0.11652200E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x32*x43    
        +coeff[  4]    *x22        *x54
        +coeff[  5]*x11*x23*x31*x41*x51
        +coeff[  6]    *x21            
        +coeff[  7]            *x41    
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[  8]        *x31    *x51
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]                *x53
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]        *x32*x42    
        +coeff[ 16]        *x33    *x51
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 17]    *x22        *x52
        +coeff[ 18]    *x21    *x41*x52
        +coeff[ 19]*x11*x22        *x51
        +coeff[ 20]*x11*x21*x31    *x51
        +coeff[ 21]*x11*x21    *x41*x51
        +coeff[ 22]*x12*x22            
        +coeff[ 23]*x12    *x31    *x51
        +coeff[ 24]*x12        *x41*x51
        +coeff[ 25]*x12            *x52
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]*x11*x21*x32    *x51
        +coeff[ 31]*x11*x22    *x41*x51
        +coeff[ 32]*x11*x21*x31*x41*x51
        +coeff[ 33]*x11*x21    *x42*x51
        +coeff[ 34]*x11        *x43*x51
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 35]*x11    *x31    *x53
        +coeff[ 36]*x11        *x41*x53
        +coeff[ 37]*x12*x21*x31*x41    
        +coeff[ 38]    *x22*x34        
        +coeff[ 39]        *x34*x42    
        +coeff[ 40]        *x33*x43    
        +coeff[ 41]    *x24    *x41*x51
        +coeff[ 42]    *x21*x32*x42*x51
        +coeff[ 43]    *x22*x31*x41*x52
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 44]            *x44*x52
        +coeff[ 45]        *x33    *x53
        +coeff[ 46]        *x31*x41*x54
        +coeff[ 47]            *x42*x54
        +coeff[ 48]*x11*x22*x31*x42    
        +coeff[ 49]*x11*x22*x32    *x51
        +coeff[ 50]*x11*x21*x33    *x51
        +coeff[ 51]*x11    *x34    *x51
        +coeff[ 52]*x11*x21*x31*x41*x52
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 53]*x11*x21    *x42*x52
        +coeff[ 54]*x11*x21        *x54
        +coeff[ 55]*x12*x22*x31*x41    
        +coeff[ 56]*x12    *x31*x42*x51
        +coeff[ 57]*x12        *x43*x51
        +coeff[ 58]*x12    *x32    *x52
        +coeff[ 59]*x12    *x31    *x53
        +coeff[ 60]*x12        *x41*x53
        +coeff[ 61]*x13*x21*x31*x41    
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 62]*x13*x21    *x42    
        +coeff[ 63]    *x24*x31*x42    
        +coeff[ 64]    *x22*x33*x42    
        +coeff[ 65]        *x34*x42*x51
        +coeff[ 66]*x13*x21        *x52
        +coeff[ 67]    *x23*x32    *x52
        +coeff[ 68]    *x21*x33*x41*x52
        +coeff[ 69]*x13            *x53
        +coeff[ 70]    *x21*x31*x42*x53
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 71]*x11*x24*x31*x41    
        +coeff[ 72]*x11*x22*x31*x43    
        +coeff[ 73]*x11    *x34*x41*x51
        +coeff[ 74]*x11*x21*x32*x42*x51
        +coeff[ 75]*x11*x21*x31*x43*x51
        +coeff[ 76]*x11*x23*x31    *x52
        +coeff[ 77]*x11*x21*x31*x42*x52
        +coeff[ 78]*x11    *x31*x43*x52
        +coeff[ 79]*x11        *x43*x53
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 80]*x11*x21*x31    *x54
        +coeff[ 81]*x12*x22*x33        
        +coeff[ 82]*x12*x24    *x41    
        +coeff[ 83]*x12*x23*x31    *x51
        +coeff[ 84]*x13    *x34        
        +coeff[ 85]    *x24*x32*x42    
        +coeff[ 86]    *x21*x34*x43    
        +coeff[ 87]    *x24    *x44    
        +coeff[ 88]*x13*x22    *x41*x51
    ;
    v_l_e_q1en_1_500                              =v_l_e_q1en_1_500                              
        +coeff[ 89]    *x24    *x43*x51
        ;

    return v_l_e_q1en_1_500                              ;
}
float x_e_q1en_1_450                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.4309592E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.43443911E-03, 0.39987573E-02, 0.77081852E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_450                              ;
}
float t_e_q1en_1_450                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.3302270E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.33317655E-03, 0.60064498E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_450                              ;
}
float y_e_q1en_1_450                              (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1359879E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.13499150E-03, 0.38542435E-01, 0.53996552E-01, 0.13792226E-04,
        -0.14138908E-04,-0.23950886E-05,-0.28621212E-05,-0.21120566E-05,
        -0.37416453E-05, 0.75873468E-05,-0.46657965E-05,-0.14179570E-04,
        -0.75643425E-05, 0.59606436E-05,-0.71568847E-05,-0.89858831E-05,
        -0.92064520E-05,-0.11429939E-04, 0.12726884E-04, 0.26946713E-04,
         0.44719291E-05,-0.81008393E-05,-0.65977652E-05,-0.67834731E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]*x11        *x41*x52
        +coeff[  4]*x11    *x31*x45    
        +coeff[  5]    *x22            
        +coeff[  6]*x11        *x41    
        +coeff[  7]*x11*x21            
    ;
    v_y_e_q1en_1_450                              =v_y_e_q1en_1_450                              
        +coeff[  8]*x11*x21    *x41    
        +coeff[  9]    *x21    *x43    
        +coeff[ 10]    *x21*x31*x42    
        +coeff[ 11]        *x31*x42*x51
        +coeff[ 12]*x11*x21*x31*x41    
        +coeff[ 13]    *x21*x31*x41*x51
        +coeff[ 14]*x11*x21    *x41*x51
        +coeff[ 15]*x11    *x31*x41*x51
        +coeff[ 16]    *x21    *x41*x52
    ;
    v_y_e_q1en_1_450                              =v_y_e_q1en_1_450                              
        +coeff[ 17]*x12    *x31*x42    
        +coeff[ 18]        *x31*x42*x52
        +coeff[ 19]        *x31*x44*x51
        +coeff[ 20]*x13        *x42    
        +coeff[ 21]    *x22*x31    *x52
        +coeff[ 22]        *x32*x43*x51
        +coeff[ 23]    *x21*x31*x45    
        ;

    return v_y_e_q1en_1_450                              ;
}
float p_e_q1en_1_450                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.2368941E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.25388968E-04, 0.30032817E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_450                              ;
}
float l_e_q1en_1_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9987797E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53993E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60067E-01, 0.53997E-01, 0.30031E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.80510607E-03,-0.21608234E-02,-0.42887052E-03, 0.24346262E-03,
        -0.62156690E-03, 0.33326645E-03,-0.13911681E-02, 0.13973116E-03,
         0.11040517E-02,-0.52356540E-03, 0.35620335E-05,-0.31924160E-03,
         0.19950907E-03, 0.15224374E-03,-0.10367918E-03,-0.18955079E-03,
         0.90933223E-04, 0.59898070E-03, 0.80839859E-03, 0.44025172E-03,
         0.16717052E-03, 0.83956239E-03, 0.16011983E-03,-0.32201567E-03,
        -0.95761335E-03, 0.83709729E-03,-0.29063839E-03,-0.34661326E-03,
         0.11349582E-02,-0.79127305E-04, 0.77945017E-03,-0.14847190E-03,
         0.98267605E-03,-0.15764702E-02,-0.50836061E-05,-0.33841650E-04,
         0.16884512E-03,-0.31274423E-03, 0.98305907E-06,-0.44194260E-03,
        -0.10556267E-02,-0.57259778E-03,-0.39480231E-03,-0.73229181E-04,
        -0.45917390E-03, 0.72635553E-03,-0.39632557E-03, 0.36357489E-03,
         0.50030276E-03,-0.49181463E-03, 0.39080917E-03,-0.58007269E-03,
         0.17410329E-02,-0.13087733E-02, 0.37961875E-03,-0.43479260E-03,
         0.81438373E-03,-0.52166951E-03, 0.16416154E-03,-0.13558252E-02,
        -0.92141476E-03, 0.82553359E-03,-0.61428768E-03, 0.59679337E-03,
        -0.97792107E-03, 0.60123496E-03, 0.15470890E-02, 0.90750511E-03,
         0.56137616E-03,-0.50501572E-03,-0.13686591E-02,-0.22376102E-03,
         0.14921387E-02,-0.97836822E-03, 0.98708668E-03,-0.16826562E-02,
         0.69887162E-03,-0.56164054E-03, 0.64178556E-03, 0.61769364E-03,
         0.15338312E-03,-0.83037355E-03, 0.12776058E-02,-0.60688931E-03,
        -0.11618707E-02,-0.64998743E-03, 0.55508252E-03, 0.10722779E-02,
        -0.20338490E-03,-0.32321792E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x11*x21*x31        
        +coeff[  4]*x11            *x52
        +coeff[  5]*x11    *x31*x42    
        +coeff[  6]*x11    *x32*x41*x53
        +coeff[  7]*x11                
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x12                
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x42    
        +coeff[ 16]            *x42*x51
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x11*x21        *x51
        +coeff[ 20]*x11    *x31    *x51
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]*x12    *x31        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]        *x34        
        +coeff[ 25]        *x33*x41    
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 26]            *x44    
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]                *x54
        +coeff[ 30]*x11*x22*x31        
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]*x11*x21    *x41*x51
        +coeff[ 33]*x12    *x32        
        +coeff[ 34]*x12    *x31*x41    
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 35]*x13        *x41    
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x23*x31    *x51
        +coeff[ 39]        *x31*x43*x51
        +coeff[ 40]        *x31*x42*x52
        +coeff[ 41]    *x21        *x54
        +coeff[ 42]*x11*x21    *x43    
        +coeff[ 43]*x11    *x31*x43    
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 44]*x11*x21*x31*x41*x51
        +coeff[ 45]*x11    *x31*x42*x51
        +coeff[ 46]*x11        *x43*x51
        +coeff[ 47]*x11    *x32    *x52
        +coeff[ 48]*x11    *x31*x41*x52
        +coeff[ 49]*x11*x21        *x53
        +coeff[ 50]*x12    *x31*x41*x51
        +coeff[ 51]*x13*x21    *x41    
        +coeff[ 52]    *x23*x32*x41    
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 53]    *x21*x34*x41    
        +coeff[ 54]    *x21*x33*x42    
        +coeff[ 55]*x13    *x31    *x51
        +coeff[ 56]    *x22*x33    *x51
        +coeff[ 57]*x13        *x41*x51
        +coeff[ 58]    *x23    *x42*x51
        +coeff[ 59]    *x22    *x43*x51
        +coeff[ 60]    *x23    *x41*x52
        +coeff[ 61]    *x21*x32*x41*x52
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 62]        *x33*x41*x52
        +coeff[ 63]    *x22    *x42*x52
        +coeff[ 64]*x11*x24*x31        
        +coeff[ 65]*x11*x22*x33        
        +coeff[ 66]*x11*x21*x33    *x51
        +coeff[ 67]*x11*x22    *x41*x52
        +coeff[ 68]*x11    *x31*x42*x52
        +coeff[ 69]*x11*x21*x31    *x53
        +coeff[ 70]*x11*x21    *x41*x53
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 71]*x12*x23*x31        
        +coeff[ 72]*x12    *x34        
        +coeff[ 73]*x12    *x33*x41    
        +coeff[ 74]*x12    *x31*x43    
        +coeff[ 75]*x12*x22    *x41*x51
        +coeff[ 76]*x12        *x43*x51
        +coeff[ 77]*x12*x22        *x52
        +coeff[ 78]*x12    *x31*x41*x52
        +coeff[ 79]*x13*x21    *x42    
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 80]        *x33*x44    
        +coeff[ 81]*x13*x21*x31    *x51
        +coeff[ 82]    *x22*x33*x41*x51
        +coeff[ 83]    *x21*x33*x42*x51
        +coeff[ 84]    *x22*x31*x43*x51
        +coeff[ 85]*x13        *x41*x52
        +coeff[ 86]    *x21    *x44*x52
        +coeff[ 87]        *x31*x44*x52
        +coeff[ 88]            *x43*x54
    ;
    v_l_e_q1en_1_450                              =v_l_e_q1en_1_450                              
        +coeff[ 89]*x11*x24*x32        
        ;

    return v_l_e_q1en_1_450                              ;
}
float x_e_q1en_1_449                              (float *x,int m){
    int ncoeff=  4;
    float avdat= -0.3652532E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59996E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  5]={
         0.36849576E-03, 0.40003420E-02,-0.72293523E-06, 0.77086978E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x51 = x5;

//                 function

    float v_x_e_q1en_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        ;

    return v_x_e_q1en_1_449                              ;
}
float t_e_q1en_1_449                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2912232E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59996E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.29257353E-03, 0.60069811E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_449                              ;
}
float y_e_q1en_1_449                              (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.4949864E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59996E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.49205835E-03, 0.38564030E-01, 0.59995018E-01, 0.86445671E-05,
        -0.44578710E-05, 0.48433676E-05, 0.55510100E-05, 0.55453097E-05,
        -0.43454706E-05,-0.13111502E-04,-0.76686674E-05,-0.20257956E-04,
         0.10232350E-04,-0.75894027E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31*x41*x51
        +coeff[  4]            *x41*x52
        +coeff[  5]    *x21*x31*x42    
        +coeff[  6]        *x31*x42*x51
        +coeff[  7]    *x21    *x41*x52
    ;
    v_y_e_q1en_1_449                              =v_y_e_q1en_1_449                              
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x22*x31*x41*x51
        +coeff[ 10]*x12    *x31*x42    
        +coeff[ 11]*x12*x21    *x41*x51
        +coeff[ 12]    *x21    *x41*x53
        +coeff[ 13]    *x23    *x42*x51
        ;

    return v_y_e_q1en_1_449                              ;
}
float p_e_q1en_1_449                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.4436589E-04;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59996E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.48015187E-04,-0.14261605E-08, 0.30049751E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x21 = x2;
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]            *x41    
        ;

    return v_p_e_q1en_1_449                              ;
}
float l_e_q1en_1_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9433784E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59996E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.95642812E-03,-0.23509492E-02,-0.69276564E-03, 0.54107996E-03,
         0.88219118E-03, 0.29946741E-03, 0.11132596E-03, 0.51583986E-04,
        -0.27908914E-03, 0.13156836E-03, 0.18270050E-03, 0.35118128E-03,
         0.27404801E-03,-0.26765061E-03,-0.17628078E-03,-0.18575416E-03,
         0.62739966E-03,-0.65903427E-04, 0.32363078E-03,-0.23902013E-03,
        -0.17816667E-03,-0.74257393E-03,-0.74345403E-03, 0.30492968E-03,
        -0.51033200E-03,-0.34025923E-03,-0.36186387E-03, 0.13505033E-02,
         0.71737455E-03, 0.16643775E-03, 0.28905226E-03,-0.57669746E-04,
        -0.88728731E-03, 0.38819248E-03,-0.45088850E-03,-0.60987566E-03,
        -0.12352351E-02, 0.52379817E-03,-0.75445249E-03,-0.48490905E-03,
         0.66445925E-03,-0.48833160E-03, 0.16733622E-02,-0.83541323E-03,
        -0.94613107E-03, 0.71998389E-03, 0.60735119E-03, 0.65991248E-03,
        -0.30086920E-03,-0.15978027E-02,-0.10458309E-02, 0.15330333E-02,
        -0.11904094E-02,-0.47038915E-03, 0.57843613E-03,-0.51827892E-03,
        -0.39012553E-03, 0.58313058E-03, 0.71931578E-03, 0.15183278E-03,
         0.17365773E-02,-0.65946160E-03,-0.36888820E-03, 0.77485415E-03,
        -0.13205432E-02,-0.47140886E-03,-0.74795698E-03, 0.93962229E-03,
         0.12960304E-02, 0.38438686E-03,-0.69987692E-03,-0.15121927E-03,
         0.61944542E-04,-0.55585132E-03, 0.16587710E-02,-0.97590050E-03,
        -0.49816183E-03,-0.26918118E-03, 0.53545588E-03, 0.54455799E-03,
         0.88149088E-03, 0.44798155E-03,-0.40414275E-03,-0.64311700E-03,
         0.76821528E-03, 0.46710946E-03, 0.13848898E-02,-0.68684621E-03,
         0.64778567E-03, 0.11672101E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]    *x23*x32*x41    
        +coeff[  4]*x12*x23        *x51
        +coeff[  5]    *x24        *x53
        +coeff[  6]    *x21            
        +coeff[  7]            *x41    
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[  8]    *x21    *x41    
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]        *x31    *x51
        +coeff[ 12]                *x52
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]*x12    *x31        
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 17]    *x21*x33        
        +coeff[ 18]    *x23    *x41    
        +coeff[ 19]        *x33*x41    
        +coeff[ 20]    *x21*x31*x42    
        +coeff[ 21]        *x33    *x51
        +coeff[ 22]        *x31*x42*x51
        +coeff[ 23]            *x42*x52
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]                *x54
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 26]*x11*x21*x32        
        +coeff[ 27]*x11*x21    *x42    
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]*x11        *x42*x51
        +coeff[ 30]*x12    *x31*x41    
        +coeff[ 31]    *x24*x31        
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]    *x21*x33    *x51
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 35]    *x21    *x42*x52
        +coeff[ 36]*x11*x21*x31*x42    
        +coeff[ 37]*x11    *x31*x41*x52
        +coeff[ 38]*x12    *x33        
        +coeff[ 39]*x12*x21        *x52
        +coeff[ 40]    *x23*x31*x42    
        +coeff[ 41]*x13        *x41*x51
        +coeff[ 42]    *x22*x31*x42*x51
        +coeff[ 43]    *x21*x33    *x52
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 44]    *x22*x31    *x53
        +coeff[ 45]        *x33    *x53
        +coeff[ 46]    *x21*x31    *x54
        +coeff[ 47]*x11*x22*x32*x41    
        +coeff[ 48]*x11*x23    *x42    
        +coeff[ 49]*x11*x21    *x44    
        +coeff[ 50]*x11*x24        *x51
        +coeff[ 51]*x11*x22    *x42*x51
        +coeff[ 52]*x11        *x44*x51
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 53]*x11*x22    *x41*x52
        +coeff[ 54]*x12*x21*x31*x41*x51
        +coeff[ 55]*x12*x21*x31    *x52
        +coeff[ 56]*x13*x23            
        +coeff[ 57]    *x24*x33        
        +coeff[ 58]*x13*x21    *x42    
        +coeff[ 59]    *x23*x32*x42    
        +coeff[ 60]    *x22*x33*x42    
        +coeff[ 61]    *x22*x32*x43    
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 62]    *x22*x31*x44    
        +coeff[ 63]    *x21*x32*x44    
        +coeff[ 64]    *x24*x31*x41*x51
        +coeff[ 65]        *x31*x44*x52
        +coeff[ 66]    *x23*x31    *x53
        +coeff[ 67]    *x23    *x41*x53
        +coeff[ 68]    *x22*x31*x41*x53
        +coeff[ 69]    *x23        *x54
        +coeff[ 70]    *x21*x32    *x54
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 71]    *x22    *x41*x54
        +coeff[ 72]*x11*x24*x32        
        +coeff[ 73]*x11*x24*x31*x41    
        +coeff[ 74]*x11*x23*x31*x42    
        +coeff[ 75]*x11*x22*x32*x42    
        +coeff[ 76]*x11*x22*x33    *x51
        +coeff[ 77]*x11    *x34*x41*x51
        +coeff[ 78]*x11*x22    *x43*x51
        +coeff[ 79]*x11    *x32*x42*x52
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 80]*x11    *x32*x41*x53
        +coeff[ 81]*x12*x22*x32*x41    
        +coeff[ 82]*x12*x23    *x42    
        +coeff[ 83]*x12*x22*x31*x42    
        +coeff[ 84]*x12*x21*x32*x41*x51
        +coeff[ 85]*x12    *x33    *x52
        +coeff[ 86]*x12*x22    *x41*x52
        +coeff[ 87]*x12    *x32*x41*x52
        +coeff[ 88]*x12*x21*x31    *x53
    ;
    v_l_e_q1en_1_449                              =v_l_e_q1en_1_449                              
        +coeff[ 89]*x12*x21        *x54
        ;

    return v_l_e_q1en_1_449                              ;
}
float x_e_q1en_1_400                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.1264419E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30036E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.60000E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.12570868E-03, 0.39982432E-02, 0.77089429E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_400                              ;
}
float t_e_q1en_1_400                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1189330E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30036E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.60000E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.11863250E-03, 0.60071558E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_400                              ;
}
float y_e_q1en_1_400                              (float *x,int m){
    int ncoeff= 17;
    float avdat= -0.3334617E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30036E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.60000E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
         0.34656329E-03, 0.38552728E-01, 0.59995208E-01, 0.28176215E-04,
         0.27441970E-05,-0.25170139E-05,-0.27722201E-05,-0.15531234E-04,
         0.58260471E-05,-0.80455557E-05,-0.26883870E-05, 0.91425100E-05,
         0.49981286E-05, 0.19354467E-04, 0.77942714E-05,-0.13369050E-04,
        -0.26849830E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x32*x45*x51
        +coeff[  4]        *x31*x41    
        +coeff[  5]*x11    *x31        
        +coeff[  6]*x11        *x42    
        +coeff[  7]*x11*x21    *x41    
    ;
    v_y_e_q1en_1_400                              =v_y_e_q1en_1_400                              
        +coeff[  8]    *x22    *x42    
        +coeff[  9]        *x31*x42*x51
        +coeff[ 10]        *x34        
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11        *x42*x51
        +coeff[ 13]*x11*x23    *x41    
        +coeff[ 14]*x12    *x31*x42    
        +coeff[ 15]*x11*x21*x31*x41*x51
        +coeff[ 16]    *x22    *x43*x51
        ;

    return v_y_e_q1en_1_400                              ;
}
float p_e_q1en_1_400                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2335790E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30036E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.60000E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.24008020E-03, 0.30042205E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_400                              ;
}
float l_e_q1en_1_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9573544E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30036E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60072E-01, 0.60000E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.91461709E-03,-0.23193299E-02,-0.30926647E-03,-0.71854003E-04,
         0.19177100E-03, 0.55054040E-03, 0.11477690E-02,-0.18075939E-02,
         0.41608364E-03, 0.14147143E-02,-0.11516965E-04, 0.48607439E-03,
        -0.47379124E-03, 0.38969392E-04, 0.79226709E-04,-0.20655541E-03,
        -0.33026841E-03, 0.14381339E-03,-0.39389078E-03,-0.17324288E-03,
        -0.45378969E-04, 0.32189306E-04,-0.37241040E-04,-0.52537717E-03,
         0.26438604E-03, 0.30129743E-03,-0.13270126E-03,-0.54660948E-04,
         0.24173512E-03,-0.19717787E-03,-0.94376926E-04, 0.24853917E-03,
         0.10736122E-02,-0.84966881E-03,-0.59509173E-03,-0.53996319E-03,
        -0.59513364E-03,-0.54442999E-03, 0.84142777E-03, 0.12636699E-02,
         0.42970097E-03,-0.49894257E-03, 0.57253469E-03,-0.57180360E-03,
        -0.40391379E-03, 0.16140052E-03,-0.23340671E-03, 0.86835155E-03,
        -0.33209115E-03, 0.57230960E-03,-0.38085628E-03,-0.68788603E-03,
         0.45153286E-03, 0.95458445E-03,-0.58420305E-03,-0.55443274E-03,
        -0.26914859E-03, 0.25380877E-03,-0.38039158E-03,-0.32182052E-03,
         0.48436387E-03, 0.11589484E-03, 0.52748929E-03,-0.52750751E-03,
         0.38511999E-03, 0.46613623E-03, 0.34621597E-03,-0.52919704E-03,
        -0.85446588E-03, 0.15144610E-02,-0.11887441E-02, 0.71027770E-03,
        -0.39145508E-03, 0.49848639E-03, 0.15728085E-02,-0.51317347E-03,
         0.78391691E-03, 0.38533198E-03,-0.70770719E-03, 0.66650746E-03,
        -0.25712898E-04, 0.63670176E-03,-0.10275908E-03,-0.65411732E-03,
        -0.94874681E-03, 0.13200253E-02,-0.59324183E-03, 0.65731292E-03,
        -0.13874016E-02,-0.15147467E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x31    *x52
        +coeff[  4]*x11    *x31*x41    
        +coeff[  5]        *x31*x43*x51
        +coeff[  6]*x11*x22*x32    *x51
        +coeff[  7]        *x32*x44*x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[  8]    *x22    *x42*x53
        +coeff[  9]*x13*x21*x33        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]    *x22    *x41    
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]            *x42*x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 17]                *x53
        +coeff[ 18]*x11*x21*x31        
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]*x11*x21        *x51
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]*x11            *x52
        +coeff[ 23]*x11*x23            
        +coeff[ 24]*x11    *x33        
        +coeff[ 25]*x11*x21*x31    *x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 26]*x11*x21    *x41*x51
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]*x12*x21    *x41    
        +coeff[ 29]*x12        *x42    
        +coeff[ 30]*x12        *x41*x51
        +coeff[ 31]*x13    *x31        
        +coeff[ 32]        *x34*x41    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]        *x33*x41*x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 35]    *x21*x32    *x52
        +coeff[ 36]    *x22    *x41*x52
        +coeff[ 37]        *x31*x42*x52
        +coeff[ 38]    *x21*x31    *x53
        +coeff[ 39]*x11*x21*x32*x41    
        +coeff[ 40]*x11    *x33*x41    
        +coeff[ 41]*x11*x22    *x41*x51
        +coeff[ 42]*x11*x22        *x52
        +coeff[ 43]*x11    *x31*x41*x52
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 44]*x12*x21*x31    *x51
        +coeff[ 45]*x12    *x31*x41*x51
        +coeff[ 46]*x13*x21    *x41    
        +coeff[ 47]    *x23*x32*x41    
        +coeff[ 48]    *x22    *x44    
        +coeff[ 49]    *x22*x32*x41*x51
        +coeff[ 50]    *x22    *x43*x51
        +coeff[ 51]    *x22*x31    *x53
        +coeff[ 52]*x11*x22*x32*x41    
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 53]*x11*x23    *x41*x51
        +coeff[ 54]*x11*x21*x32*x41*x51
        +coeff[ 55]*x11*x22    *x42*x51
        +coeff[ 56]*x11    *x31*x43*x51
        +coeff[ 57]*x11    *x31    *x54
        +coeff[ 58]*x12*x21*x31*x42    
        +coeff[ 59]*x12    *x31*x43    
        +coeff[ 60]*x12*x22*x31    *x51
        +coeff[ 61]*x12    *x31*x42*x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 62]*x12*x22        *x52
        +coeff[ 63]*x12        *x42*x52
        +coeff[ 64]*x12    *x31    *x53
        +coeff[ 65]    *x23*x34        
        +coeff[ 66]*x13*x21*x31*x41    
        +coeff[ 67]        *x34*x43    
        +coeff[ 68]    *x23*x33    *x51
        +coeff[ 69]        *x34*x42*x51
        +coeff[ 70]    *x21*x32*x43*x51
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 71]    *x22    *x44*x51
        +coeff[ 72]*x13*x21        *x52
        +coeff[ 73]    *x23    *x42*x52
        +coeff[ 74]    *x22    *x43*x52
        +coeff[ 75]    *x22*x32    *x53
        +coeff[ 76]    *x21*x32*x41*x53
        +coeff[ 77]*x11*x21    *x43*x52
        +coeff[ 78]*x11*x23        *x53
        +coeff[ 79]*x11*x21    *x42*x53
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 80]*x12*x23*x32        
        +coeff[ 81]*x12*x24    *x41    
        +coeff[ 82]*x12*x22*x32*x41    
        +coeff[ 83]*x12    *x34*x41    
        +coeff[ 84]*x12*x21*x32*x42    
        +coeff[ 85]*x12*x22*x31*x41*x51
        +coeff[ 86]*x12*x21*x31*x41*x52
        +coeff[ 87]*x12    *x31*x42*x52
        +coeff[ 88]*x13*x21*x32*x41    
    ;
    v_l_e_q1en_1_400                              =v_l_e_q1en_1_400                              
        +coeff[ 89]    *x23*x34*x41    
        ;

    return v_l_e_q1en_1_400                              ;
}
float x_e_q1en_1_350                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.8868249E-05;
    float xmin[10]={
        -0.39997E-02,-0.60056E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30034E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.14232424E-04, 0.39995746E-02, 0.77076226E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_350                              ;
}
float t_e_q1en_1_350                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2698423E-05;
    float xmin[10]={
        -0.39997E-02,-0.60056E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30034E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.65987638E-05, 0.60059860E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_350                              ;
}
float y_e_q1en_1_350                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.1816238E-03;
    float xmin[10]={
        -0.39997E-02,-0.60056E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30034E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.17205160E-03, 0.38536735E-01, 0.59995037E-01, 0.66220637E-05,
        -0.42260913E-05,-0.12235309E-04, 0.81212556E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_y_e_q1en_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]            *x45    
        +coeff[  5]        *x32*x41*x51
        +coeff[  6]*x12    *x32*x41    
        ;

    return v_y_e_q1en_1_350                              ;
}
float p_e_q1en_1_350                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1634839E-03;
    float xmin[10]={
        -0.39997E-02,-0.60056E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30034E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.15748314E-03, 0.30028002E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_350                              ;
}
float l_e_q1en_1_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1003776E-02;
    float xmin[10]={
        -0.39997E-02,-0.60056E-01,-0.59993E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60064E-01, 0.59997E-01, 0.30034E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.95704698E-03,-0.22648636E-02,-0.69383351E-03, 0.37943971E-03,
        -0.14264681E-02, 0.49833625E-05,-0.12831356E-03,-0.12924975E-03,
         0.30064577E-03, 0.12815278E-03,-0.13634501E-03, 0.20665709E-03,
         0.95146429E-03, 0.68208034E-03, 0.29045367E-03, 0.23398489E-03,
         0.33533355E-03,-0.32560338E-03,-0.11089846E-02, 0.69301465E-03,
        -0.88988442E-03,-0.12779742E-02, 0.35520416E-03, 0.61753840E-03,
        -0.10500579E-02, 0.61360077E-03,-0.11749722E-03,-0.15632625E-03,
         0.27604692E-03,-0.38423558E-03,-0.65468234E-03,-0.16518063E-04,
        -0.37756257E-03,-0.65887260E-04, 0.21645632E-02,-0.10872759E-02,
         0.53940690E-03,-0.10123580E-02,-0.37654920E-03,-0.82830526E-03,
         0.57930057E-03, 0.10168635E-02, 0.24057631E-02,-0.15323763E-02,
         0.16184534E-02, 0.22263861E-03,-0.40128682E-03,-0.73592237E-03,
         0.15521954E-02,-0.17474650E-02,-0.58501435E-03, 0.11166780E-02,
         0.57839893E-03,-0.70729066E-03,-0.11083825E-02, 0.82178233E-03,
        -0.51079952E-03,-0.83284423E-03, 0.91609842E-03, 0.35958592E-03,
        -0.12102579E-02, 0.29977778E-03,-0.11836292E-03, 0.10072218E-02,
        -0.45405803E-02, 0.15828075E-02, 0.22438362E-02,-0.48345505E-03,
        -0.79747586E-03, 0.78244327E-03,-0.62878925E-03, 0.96660304E-04,
         0.10042778E-03,-0.13613947E-03,-0.53517448E-04,-0.10588221E-03,
         0.45456694E-04, 0.67785906E-04, 0.58835343E-04,-0.22521989E-03,
        -0.85084335E-04, 0.57612877E-04,-0.88122899E-04,-0.94134120E-04,
        -0.16461436E-03, 0.21941299E-03, 0.17171344E-03,-0.14704296E-03,
        -0.39606154E-03, 0.10709517E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]*x11*x24*x31        
        +coeff[  4]*x11*x24    *x41*x51
        +coeff[  5]        *x31        
        +coeff[  6]                *x51
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[  8]    *x21*x32        
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]*x13                
        +coeff[ 15]            *x44    
        +coeff[ 16]    *x21    *x41*x52
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 17]*x12        *x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]        *x31*x43*x51
        +coeff[ 21]    *x21*x32    *x52
        +coeff[ 22]*x11*x24            
        +coeff[ 23]*x11*x23*x31        
        +coeff[ 24]*x11*x21*x33        
        +coeff[ 25]*x11*x23        *x51
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 26]*x11    *x32    *x52
        +coeff[ 27]*x11            *x54
        +coeff[ 28]*x12*x21    *x42    
        +coeff[ 29]*x12    *x31*x41*x51
        +coeff[ 30]*x12    *x31    *x52
        +coeff[ 31]    *x24*x32        
        +coeff[ 32]*x13        *x41*x51
        +coeff[ 33]    *x24    *x41*x51
        +coeff[ 34]    *x22*x31*x42*x51
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 35]        *x33*x42*x51
        +coeff[ 36]    *x22    *x43*x51
        +coeff[ 37]*x11*x22*x33        
        +coeff[ 38]*x11*x22        *x53
        +coeff[ 39]*x12    *x32*x42    
        +coeff[ 40]*x12    *x32    *x52
        +coeff[ 41]*x13*x22*x31        
        +coeff[ 42]    *x23*x32    *x52
        +coeff[ 43]    *x22*x32*x41*x52
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 44]    *x22*x31*x42*x52
        +coeff[ 45]    *x24        *x53
        +coeff[ 46]*x11    *x33*x43    
        +coeff[ 47]*x11    *x32*x44    
        +coeff[ 48]*x11*x23*x32    *x51
        +coeff[ 49]*x11*x21*x32*x42*x51
        +coeff[ 50]*x11    *x32*x43*x51
        +coeff[ 51]*x11    *x31*x42*x53
        +coeff[ 52]*x12    *x33*x42    
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 53]*x12*x21    *x44    
        +coeff[ 54]*x12*x22*x31*x41*x51
        +coeff[ 55]*x12    *x32*x41*x52
        +coeff[ 56]*x12*x21    *x42*x52
        +coeff[ 57]*x13*x24            
        +coeff[ 58]*x13*x21*x33        
        +coeff[ 59]*x13    *x34        
        +coeff[ 60]*x13*x23        *x51
        +coeff[ 61]*x13    *x33    *x51
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 62]    *x24*x33    *x51
        +coeff[ 63]*x13*x21    *x42*x51
        +coeff[ 64]    *x24*x31*x42*x51
        +coeff[ 65]    *x22*x33*x42*x51
        +coeff[ 66]    *x22*x31*x44*x51
        +coeff[ 67]    *x24*x32    *x52
        +coeff[ 68]*x13    *x31*x41*x52
        +coeff[ 69]    *x22    *x44*x52
        +coeff[ 70]    *x21*x32*x41*x54
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 71]    *x21            
        +coeff[ 72]            *x41    
        +coeff[ 73]*x11                
        +coeff[ 74]    *x21        *x51
        +coeff[ 75]                *x52
        +coeff[ 76]*x11        *x41    
        +coeff[ 77]*x11            *x51
        +coeff[ 78]*x12                
        +coeff[ 79]    *x22*x31        
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 80]            *x43    
        +coeff[ 81]    *x21*x31    *x51
        +coeff[ 82]    *x21    *x41*x51
        +coeff[ 83]            *x42*x51
        +coeff[ 84]    *x21        *x52
        +coeff[ 85]*x11    *x31*x41    
        +coeff[ 86]*x11        *x42    
        +coeff[ 87]*x11*x21        *x51
        +coeff[ 88]*x11    *x31    *x51
    ;
    v_l_e_q1en_1_350                              =v_l_e_q1en_1_350                              
        +coeff[ 89]*x12    *x31        
        ;

    return v_l_e_q1en_1_350                              ;
}
float x_e_q1en_1_300                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.5884306E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60064E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.58369868E-03, 0.39969566E-02, 0.77084795E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_300                              ;
}
float t_e_q1en_1_300                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.4667520E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60064E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.46290233E-03, 0.60067508E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_300                              ;
}
float y_e_q1en_1_300                              (float *x,int m){
    int ncoeff= 18;
    float avdat=  0.5139824E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60064E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 19]={
        -0.48931909E-03, 0.38554542E-01, 0.59987370E-01, 0.27693495E-04,
        -0.36172580E-05,-0.11645714E-06, 0.12352895E-06, 0.35477933E-05,
        -0.63214175E-05,-0.85234342E-05,-0.98592536E-05, 0.64193491E-05,
        -0.72599018E-05,-0.48692073E-05,-0.11158051E-04, 0.14404579E-04,
         0.57313014E-05,-0.15090755E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x22    *x45*x51
        +coeff[  4]            *x42*x51
        +coeff[  5]    *x21*x32        
        +coeff[  6]        *x31*x41*x51
        +coeff[  7]    *x21    *x43    
    ;
    v_y_e_q1en_1_300                              =v_y_e_q1en_1_300                              
        +coeff[  8]*x11    *x31*x41*x51
        +coeff[  9]    *x22*x31*x42    
        +coeff[ 10]    *x21*x32*x42    
        +coeff[ 11]*x11    *x31*x43    
        +coeff[ 12]    *x21    *x43*x51
        +coeff[ 13]            *x41*x53
        +coeff[ 14]    *x21*x31*x42*x51
        +coeff[ 15]    *x22*x31*x41*x51
        +coeff[ 16]    *x24    *x42    
    ;
    v_y_e_q1en_1_300                              =v_y_e_q1en_1_300                              
        +coeff[ 17]        *x31*x41*x53
        ;

    return v_y_e_q1en_1_300                              ;
}
float p_e_q1en_1_300                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.2828830E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60064E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.27203385E-03,-0.12629393E-06, 0.30043254E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x31 = x3;
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        ;

    return v_p_e_q1en_1_300                              ;
}
float l_e_q1en_1_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9628282E-03;
    float xmin[10]={
        -0.39976E-02,-0.60071E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60064E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.93246286E-03,-0.20981533E-02,-0.62488002E-03,-0.18831030E-03,
         0.64577843E-03, 0.18772091E-03, 0.15043274E-03,-0.29571895E-04,
        -0.74683558E-04,-0.22300106E-03,-0.11285983E-03, 0.83284780E-04,
        -0.25253682E-03, 0.74579839E-04, 0.17024301E-02, 0.67424006E-03,
        -0.15277052E-03, 0.31097344E-03,-0.43433390E-03, 0.87778614E-03,
         0.46776811E-03,-0.66311745E-03,-0.52930118E-03,-0.24672237E-03,
         0.10886472E-02, 0.31951210E-03,-0.29244914E-03,-0.29901965E-03,
         0.23573797E-04, 0.37289198E-03,-0.61782054E-03,-0.18473783E-02,
        -0.26744639E-02, 0.19218519E-03, 0.23153061E-02,-0.12225987E-02,
        -0.65165496E-03,-0.52810047E-03,-0.18335867E-02, 0.71728718E-03,
         0.85557869E-03, 0.11976728E-02,-0.78268128E-03, 0.30395272E-03,
        -0.84363087E-03,-0.39522615E-03,-0.14364129E-03, 0.10305344E-02,
         0.16427143E-03, 0.92086737E-03,-0.61914278E-03, 0.12558990E-02,
        -0.50567766E-03, 0.54095377E-03,-0.49539335E-03,-0.79595763E-03,
         0.62191830E-03,-0.27019545E-03, 0.40471402E-03, 0.20890677E-03,
        -0.95687172E-03,-0.61425997E-03,-0.99473214E-03, 0.24349712E-03,
        -0.60618663E-03,-0.29010157E-03,-0.12689506E-02,-0.13493170E-02,
         0.89126622E-03,-0.36756269E-03, 0.78385574E-03, 0.12240025E-02,
         0.37706406E-02,-0.30006394E-02,-0.52389182E-03, 0.55646541E-03,
        -0.60522341E-03, 0.11469984E-02,-0.53718995E-03, 0.41485493E-03,
         0.13223987E-02,-0.63880323E-03, 0.13182971E-02,-0.76417992E-03,
         0.59749914E-03,-0.11858470E-02,-0.36980951E-03, 0.60602429E-03,
         0.11599456E-02, 0.11201404E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]    *x21*x32*x41    
        +coeff[  4]        *x31    *x53
        +coeff[  5]    *x24    *x42*x51
        +coeff[  6]    *x21            
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[  8]    *x21        *x51
        +coeff[  9]        *x31    *x51
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]                *x53
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11    *x31    *x51
        +coeff[ 16]    *x24            
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]            *x43*x51
        +coeff[ 21]    *x21    *x41*x52
        +coeff[ 22]*x11    *x31*x42    
        +coeff[ 23]*x12*x21*x31        
        +coeff[ 24]*x12*x21        *x51
        +coeff[ 25]*x13    *x31        
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x24        *x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x21*x31    *x53
        +coeff[ 30]*x11*x21*x33        
        +coeff[ 31]*x11    *x33*x41    
        +coeff[ 32]*x11    *x31*x43    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11*x21*x31*x41*x51
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 35]*x11*x22        *x52
        +coeff[ 36]*x11    *x31*x41*x52
        +coeff[ 37]*x12*x21*x31*x41    
        +coeff[ 38]*x12*x21*x31    *x51
        +coeff[ 39]*x13*x21*x31        
        +coeff[ 40]    *x21*x34*x41    
        +coeff[ 41]    *x22    *x44    
        +coeff[ 42]*x13    *x31    *x51
        +coeff[ 43]*x13        *x41*x51
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 44]    *x23*x31*x41*x51
        +coeff[ 45]    *x22    *x43*x51
        +coeff[ 46]    *x21    *x44*x51
        +coeff[ 47]    *x23*x31    *x52
        +coeff[ 48]    *x22*x32    *x52
        +coeff[ 49]    *x23    *x41*x52
        +coeff[ 50]    *x21*x31    *x54
        +coeff[ 51]*x11*x21*x33    *x51
        +coeff[ 52]*x11*x21*x31*x42*x51
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 53]*x11    *x32*x42*x51
        +coeff[ 54]*x11    *x33    *x52
        +coeff[ 55]*x11*x21    *x42*x52
        +coeff[ 56]*x11    *x31*x42*x52
        +coeff[ 57]*x11        *x43*x52
        +coeff[ 58]*x11*x21        *x54
        +coeff[ 59]*x12        *x44    
        +coeff[ 60]*x12*x23        *x51
        +coeff[ 61]*x12*x22*x31    *x51
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 62]*x12*x21    *x42*x51
        +coeff[ 63]    *x23*x33*x41    
        +coeff[ 64]*x13*x21*x31    *x51
        +coeff[ 65]*x13        *x42*x51
        +coeff[ 66]    *x23*x31*x42*x51
        +coeff[ 67]    *x22    *x44*x51
        +coeff[ 68]    *x21*x31*x44*x51
        +coeff[ 69]    *x23*x32    *x52
        +coeff[ 70]    *x22*x32*x41*x52
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 71]    *x22    *x42*x53
        +coeff[ 72]*x11    *x33*x43    
        +coeff[ 73]*x11*x21*x31*x43*x51
        +coeff[ 74]*x11    *x32*x43*x51
        +coeff[ 75]*x11*x21    *x43*x52
        +coeff[ 76]*x11*x22*x31    *x53
        +coeff[ 77]*x11*x22        *x54
        +coeff[ 78]*x11*x21*x31    *x54
        +coeff[ 79]*x12*x21*x34        
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 80]*x12*x21*x33    *x51
        +coeff[ 81]*x12*x21    *x42*x52
        +coeff[ 82]*x12*x21*x31    *x53
        +coeff[ 83]*x13*x23    *x41    
        +coeff[ 84]*x13*x21    *x43    
        +coeff[ 85]    *x24    *x44    
        +coeff[ 86]    *x23*x31*x44    
        +coeff[ 87]*x13    *x31*x42*x51
        +coeff[ 88]    *x23    *x44*x51
    ;
    v_l_e_q1en_1_300                              =v_l_e_q1en_1_300                              
        +coeff[ 89]*x13*x22        *x52
        ;

    return v_l_e_q1en_1_300                              ;
}
float x_e_q1en_1_250                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.4268839E-03;
    float xmin[10]={
        -0.39989E-02,-0.60072E-01,-0.59997E-01,-0.30057E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.42680412E-03, 0.39996020E-02, 0.77089414E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_250                              ;
}
float t_e_q1en_1_250                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.3308720E-03;
    float xmin[10]={
        -0.39989E-02,-0.60072E-01,-0.59997E-01,-0.30057E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.33027702E-03, 0.60071055E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_250                              ;
}
float y_e_q1en_1_250                              (float *x,int m){
    int ncoeff= 19;
    float avdat= -0.9255433E-04;
    float xmin[10]={
        -0.39989E-02,-0.60072E-01,-0.59997E-01,-0.30057E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 20]={
         0.77660232E-04, 0.38558885E-01, 0.59992541E-01,-0.10058116E-04,
        -0.30132030E-05, 0.23834248E-05, 0.56415734E-05, 0.47176868E-05,
        -0.58513187E-05, 0.56699341E-05, 0.77320610E-05, 0.79099036E-05,
        -0.54171092E-05, 0.10023137E-04, 0.22331546E-04,-0.92276023E-05,
         0.11047086E-04,-0.79669626E-05,-0.13209548E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x43*x51
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22    *x41    
        +coeff[  7]*x11        *x42    
    ;
    v_y_e_q1en_1_250                              =v_y_e_q1en_1_250                              
        +coeff[  8]        *x31*x41*x51
        +coeff[  9]        *x31*x42*x51
        +coeff[ 10]            *x44*x51
        +coeff[ 11]    *x22*x31*x42    
        +coeff[ 12]*x12        *x43    
        +coeff[ 13]*x11    *x31*x42*x51
        +coeff[ 14]    *x21*x31*x41*x52
        +coeff[ 15]*x12        *x42*x51
        +coeff[ 16]    *x22    *x43*x51
    ;
    v_y_e_q1en_1_250                              =v_y_e_q1en_1_250                              
        +coeff[ 17]    *x22*x33*x41    
        +coeff[ 18]    *x21*x31*x45    
        ;

    return v_y_e_q1en_1_250                              ;
}
float p_e_q1en_1_250                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.3151106E-04;
    float xmin[10]={
        -0.39989E-02,-0.60072E-01,-0.59997E-01,-0.30057E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.40560295E-04, 0.30047853E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_250                              ;
}
float l_e_q1en_1_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9600220E-03;
    float xmin[10]={
        -0.39989E-02,-0.60072E-01,-0.59997E-01,-0.30057E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.91444916E-03,-0.22078082E-02,-0.33802845E-03, 0.24462666E-03,
         0.43664101E-03,-0.12101059E-03,-0.81929041E-03,-0.60049392E-03,
         0.10521336E-03,-0.31211760E-02,-0.15618762E-02,-0.13047177E-02,
        -0.13890878E-03, 0.11682751E-04, 0.78408128E-04, 0.24333048E-03,
        -0.81931839E-05, 0.21090085E-03,-0.56170730E-03, 0.21736094E-03,
         0.33530720E-04, 0.20903659E-04,-0.26085522E-03,-0.27936832E-04,
        -0.19140086E-03,-0.93980503E-04,-0.16180219E-03, 0.30293377E-04,
        -0.26353000E-03,-0.84039598E-03, 0.26485384E-04, 0.11135030E-03,
         0.23050242E-03,-0.14406886E-03,-0.22162168E-03,-0.30180809E-03,
        -0.64721785E-03, 0.81868857E-04, 0.40357784E-03, 0.44006199E-04,
         0.26442780E-03, 0.75157086E-03, 0.24050525E-02,-0.16988151E-02,
         0.61811873E-03,-0.58038335E-03,-0.51954924E-03,-0.85602369E-03,
         0.10688468E-02, 0.13803249E-02, 0.28062321E-03, 0.23510629E-03,
         0.68948354E-03, 0.78339438E-03,-0.28074000E-03, 0.31780850E-03,
        -0.35703479E-03,-0.90769958E-03,-0.47661847E-03, 0.40970065E-03,
         0.10314535E-02,-0.25685463E-03, 0.58260793E-03,-0.89235441E-03,
         0.84555126E-03, 0.79709705E-03, 0.46567043E-03,-0.64386759E-03,
         0.14487142E-02,-0.64890709E-03, 0.22751106E-03, 0.25369553E-03,
        -0.94527553E-03,-0.10908296E-02,-0.24100118E-02, 0.51035220E-03,
         0.35861743E-03, 0.17911980E-02,-0.11156002E-02,-0.12156541E-02,
        -0.74577436E-03, 0.90916926E-03,-0.59400301E-03,-0.16090997E-02,
         0.16019037E-02, 0.99123770E-03, 0.11641791E-02,-0.62830892E-03,
         0.17997224E-02, 0.57714357E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x42*x51
        +coeff[  5]*x11*x21    *x41    
        +coeff[  6]    *x22    *x42    
        +coeff[  7]*x11        *x43    
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[  8]*x12*x22    *x43    
        +coeff[  9]    *x22*x32*x43*x51
        +coeff[ 10]    *x22*x32    *x54
        +coeff[ 11]    *x21*x31*x42*x54
        +coeff[ 12]                *x51
        +coeff[ 13]    *x21        *x51
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]*x12    *x31        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]            *x42*x52
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11    *x31*x41*x51
        +coeff[ 30]*x11            *x53
        +coeff[ 31]*x12*x22            
        +coeff[ 32]*x12*x21        *x51
        +coeff[ 33]*x12            *x52
        +coeff[ 34]    *x21*x33    *x51
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 35]    *x21*x32*x41*x51
        +coeff[ 36]    *x23        *x52
        +coeff[ 37]    *x22*x31    *x52
        +coeff[ 38]        *x31*x41*x53
        +coeff[ 39]*x11    *x33    *x51
        +coeff[ 40]*x11*x22    *x41*x51
        +coeff[ 41]*x11    *x31*x42*x51
        +coeff[ 42]*x11*x22        *x52
        +coeff[ 43]*x11    *x32    *x52
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 44]*x11*x21        *x53
        +coeff[ 45]*x12    *x31*x41*x51
        +coeff[ 46]*x12        *x41*x52
        +coeff[ 47]    *x21*x33*x42    
        +coeff[ 48]    *x21*x31*x44    
        +coeff[ 49]    *x22*x32*x41*x51
        +coeff[ 50]        *x33*x42*x51
        +coeff[ 51]    *x22*x32    *x52
        +coeff[ 52]    *x21*x32*x41*x52
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 53]    *x22    *x42*x52
        +coeff[ 54]    *x21    *x41*x54
        +coeff[ 55]*x11    *x32*x43    
        +coeff[ 56]*x11*x24        *x51
        +coeff[ 57]*x11*x21*x32*x41*x51
        +coeff[ 58]*x11*x21*x31*x42*x51
        +coeff[ 59]*x11*x21    *x41*x53
        +coeff[ 60]*x11    *x31*x41*x53
        +coeff[ 61]*x11        *x41*x54
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 62]*x12*x22*x32        
        +coeff[ 63]*x12*x23        *x51
        +coeff[ 64]*x12    *x32*x41*x51
        +coeff[ 65]*x12*x21        *x53
        +coeff[ 66]    *x23*x34        
        +coeff[ 67]    *x24*x31*x42    
        +coeff[ 68]    *x22*x33*x42    
        +coeff[ 69]    *x21*x34*x42    
        +coeff[ 70]        *x34*x43    
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 71]    *x23    *x44    
        +coeff[ 72]    *x24    *x42*x51
        +coeff[ 73]    *x22*x33    *x52
        +coeff[ 74]    *x24    *x41*x52
        +coeff[ 75]    *x22    *x43*x52
        +coeff[ 76]*x13            *x53
        +coeff[ 77]    *x22    *x41*x54
        +coeff[ 78]*x11*x22*x31*x42*x51
        +coeff[ 79]*x11*x24        *x52
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 80]*x11*x21*x32*x41*x52
        +coeff[ 81]*x11*x21*x31*x42*x52
        +coeff[ 82]*x11    *x33    *x53
        +coeff[ 83]*x11*x22        *x54
        +coeff[ 84]*x11    *x32    *x54
        +coeff[ 85]*x12*x24    *x41    
        +coeff[ 86]*x12*x22    *x42*x51
        +coeff[ 87]*x12    *x32*x42*x51
        +coeff[ 88]*x12    *x31*x41*x53
    ;
    v_l_e_q1en_1_250                              =v_l_e_q1en_1_250                              
        +coeff[ 89]*x12    *x31    *x54
        ;

    return v_l_e_q1en_1_250                              ;
}
float x_e_q1en_1_200                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.3105637E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.33184569E-03, 0.39997674E-02, 0.77065088E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_200                              ;
}
float t_e_q1en_1_200                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2543424E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.27034103E-03, 0.60052861E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_200                              ;
}
float y_e_q1en_1_200                              (float *x,int m){
    int ncoeff= 22;
    float avdat= -0.2359405E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 23]={
         0.22589504E-03, 0.38560938E-01, 0.59986819E-01,-0.86708733E-05,
        -0.24364545E-06,-0.43657660E-05,-0.65928560E-06,-0.59694248E-05,
        -0.41508729E-05, 0.62029594E-05,-0.77902214E-05, 0.78575422E-05,
        -0.34144559E-05,-0.13608947E-04, 0.62912363E-05, 0.12340823E-04,
         0.57862380E-05,-0.13893650E-04, 0.93473018E-05, 0.13024704E-04,
        -0.85436222E-05, 0.72810326E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x43    
        +coeff[  4]*x11    *x31*x41    
        +coeff[  5]    *x21    *x41*x51
        +coeff[  6]    *x23    *x41    
        +coeff[  7]*x11*x21    *x42    
    ;
    v_y_e_q1en_1_200                              =v_y_e_q1en_1_200                              
        +coeff[  8]    *x21    *x44    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]*x11        *x44    
        +coeff[ 11]            *x44*x51
        +coeff[ 12]            *x41*x53
        +coeff[ 13]    *x22    *x42*x51
        +coeff[ 14]            *x43*x52
        +coeff[ 15]*x11*x22*x31*x41    
        +coeff[ 16]        *x33*x41*x51
    ;
    v_y_e_q1en_1_200                              =v_y_e_q1en_1_200                              
        +coeff[ 17]*x11    *x31*x42*x51
        +coeff[ 18]*x11        *x42*x52
        +coeff[ 19]    *x23*x32*x41    
        +coeff[ 20]*x11*x21    *x41*x52
        +coeff[ 21]*x12*x22        *x51
        ;

    return v_y_e_q1en_1_200                              ;
}
float p_e_q1en_1_200                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.1001643E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.10021706E-03, 0.30046457E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_200                              ;
}
float l_e_q1en_1_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9715543E-03;
    float xmin[10]={
        -0.39998E-02,-0.60037E-01,-0.59997E-01,-0.30047E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.94859634E-03, 0.83830811E-04,-0.21826786E-02,-0.68831968E-03,
        -0.11467809E-02,-0.60464279E-03,-0.97312266E-03, 0.14588649E-02,
         0.68249524E-03, 0.60462124E-04, 0.51906238E-04,-0.30192011E-03,
        -0.68089845E-04,-0.39973570E-05,-0.45730916E-03,-0.42873377E-03,
         0.30443573E-03,-0.44612790E-03, 0.60058478E-03, 0.47810969E-03,
         0.87512970E-04,-0.11275239E-03, 0.33121157E-03, 0.76495548E-03,
        -0.22714355E-03, 0.14211633E-03, 0.95627859E-03, 0.13173106E-02,
        -0.22624311E-03,-0.82177494E-03, 0.47633870E-03, 0.71339589E-03,
        -0.19746833E-02,-0.57690591E-03, 0.39235598E-04, 0.39118776E-03,
         0.45933059E-03, 0.87757464E-04,-0.58798003E-03, 0.42162216E-03,
         0.25081198E-03,-0.55338000E-03, 0.59368194E-03, 0.14238615E-03,
         0.58225385E-03,-0.51393412E-03,-0.98524417E-03, 0.36820801E-03,
         0.41658565E-03,-0.94241358E-03, 0.15992242E-03,-0.49699773E-03,
         0.83418417E-03,-0.12781208E-02, 0.19526173E-03,-0.96597592E-03,
        -0.16095744E-02,-0.12373680E-02, 0.11783487E-02,-0.58431900E-03,
         0.12714886E-02, 0.41160433E-03, 0.27320089E-03, 0.48997591E-03,
        -0.13682782E-02, 0.14744807E-03, 0.14689470E-02, 0.11552911E-02,
        -0.50175405E-03,-0.39912603E-03, 0.13435116E-02,-0.86706667E-03,
        -0.11082856E-02, 0.67276519E-03,-0.73116639E-03, 0.16403269E-02,
         0.92324609E-03,-0.12675291E-02, 0.64404489E-03,-0.44179749E-03,
         0.85550727E-03, 0.22480355E-03, 0.34619551E-03, 0.52176113E-03,
         0.86221116E-03,-0.80015074E-03,-0.76947093E-03,-0.38592250E-04,
         0.32175223E-04, 0.14355002E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]            *x42    
        +coeff[  4]    *x21*x32*x42    
        +coeff[  5]        *x32*x42*x52
        +coeff[  6]*x11*x22*x31*x42    
        +coeff[  7]*x11    *x31*x43*x51
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[  8]    *x23    *x43*x52
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41    
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]        *x33        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]*x12        *x41    
        +coeff[ 16]            *x44    
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 17]        *x33    *x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21        *x53
        +coeff[ 20]        *x31    *x53
        +coeff[ 21]*x11    *x33        
        +coeff[ 22]*x11*x22    *x41    
        +coeff[ 23]*x11*x21*x31*x41    
        +coeff[ 24]*x12*x22            
        +coeff[ 25]    *x23        *x52
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]    *x21*x32    *x52
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x21*x31*x41*x52
        +coeff[ 30]*x11*x21*x32*x41    
        +coeff[ 31]*x11*x21    *x43    
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x11*x21*x31    *x52
        +coeff[ 34]*x11        *x42*x52
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 35]*x12*x22    *x41    
        +coeff[ 36]*x12    *x32*x41    
        +coeff[ 37]*x12*x21*x31    *x51
        +coeff[ 38]*x12        *x42*x51
        +coeff[ 39]*x13    *x31*x41    
        +coeff[ 40]    *x21*x31*x43*x51
        +coeff[ 41]*x11*x21*x34        
        +coeff[ 42]*x11*x21    *x44    
        +coeff[ 43]*x11*x23        *x52
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 44]*x11*x21*x32    *x52
        +coeff[ 45]*x11*x22    *x41*x52
        +coeff[ 46]*x12*x22    *x41*x51
        +coeff[ 47]*x12    *x31*x41*x52
        +coeff[ 48]*x13    *x33        
        +coeff[ 49]*x13*x21*x31*x41    
        +coeff[ 50]    *x23*x33*x41    
        +coeff[ 51]*x13*x21    *x42    
        +coeff[ 52]    *x21*x34*x42    
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 53]    *x21*x33*x43    
        +coeff[ 54]    *x23*x33    *x51
        +coeff[ 55]*x13    *x31*x41*x51
        +coeff[ 56]    *x23*x32    *x52
        +coeff[ 57]    *x22*x33    *x52
        +coeff[ 58]    *x23*x31*x41*x52
        +coeff[ 59]    *x22*x32*x41*x52
        +coeff[ 60]    *x21*x31*x43*x52
        +coeff[ 61]    *x23*x31    *x53
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 62]            *x44*x53
        +coeff[ 63]*x11*x24*x31*x41    
        +coeff[ 64]*x11*x22*x33*x41    
        +coeff[ 65]*x11*x23*x31*x42    
        +coeff[ 66]*x11*x22*x32*x42    
        +coeff[ 67]*x11*x21*x33*x42    
        +coeff[ 68]*x11*x22    *x44    
        +coeff[ 69]*x11    *x33*x42*x51
        +coeff[ 70]*x11*x24        *x52
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 71]*x11    *x32*x42*x52
        +coeff[ 72]*x11*x21*x32    *x53
        +coeff[ 73]*x11    *x32*x41*x53
        +coeff[ 74]*x11        *x43*x53
        +coeff[ 75]*x11*x22        *x54
        +coeff[ 76]*x12*x21*x33*x41    
        +coeff[ 77]*x12*x21*x33    *x51
        +coeff[ 78]*x12    *x32*x42*x51
        +coeff[ 79]*x13*x23*x31        
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 80]*x13*x21*x32    *x51
        +coeff[ 81]    *x24*x32*x41*x51
        +coeff[ 82]        *x34*x43*x51
        +coeff[ 83]*x13        *x41*x53
        +coeff[ 84]    *x24    *x41*x53
        +coeff[ 85]        *x32*x43*x53
        +coeff[ 86]    *x21    *x44*x53
        +coeff[ 87]    *x21            
        +coeff[ 88]*x11                
    ;
    v_l_e_q1en_1_200                              =v_l_e_q1en_1_200                              
        +coeff[ 89]        *x31    *x51
        ;

    return v_l_e_q1en_1_200                              ;
}
float x_e_q1en_1_175                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.5184317E-03;
    float xmin[10]={
        -0.40000E-02,-0.60065E-01,-0.59989E-01,-0.30030E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30018E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.52030373E-03, 0.40008575E-02, 0.77077746E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_175                              ;
}
float t_e_q1en_1_175                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.3915154E-03;
    float xmin[10]={
        -0.40000E-02,-0.60065E-01,-0.59989E-01,-0.30030E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30018E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.39341702E-03, 0.52214979E-08, 0.60063161E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_t_e_q1en_1_175                              ;
}
float y_e_q1en_1_175                              (float *x,int m){
    int ncoeff= 21;
    float avdat= -0.8154940E-04;
    float xmin[10]={
        -0.40000E-02,-0.60065E-01,-0.59989E-01,-0.30030E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30018E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
         0.73337171E-04, 0.38531743E-01, 0.59987873E-01, 0.40281513E-04,
         0.23296391E-05,-0.46778628E-05, 0.19765787E-05,-0.11334139E-04,
        -0.84464818E-05,-0.75102848E-05, 0.86501277E-05,-0.23731001E-04,
         0.48995330E-05, 0.25455503E-04, 0.74588179E-05, 0.24217868E-04,
        -0.52587598E-05, 0.94579418E-05, 0.63694538E-05, 0.95230998E-05,
        -0.22668270E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]*x13        *x44    
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]*x11        *x42    
        +coeff[  7]    *x21*x31*x42    
    ;
    v_y_e_q1en_1_175                              =v_y_e_q1en_1_175                              
        +coeff[  8]    *x22*x31*x41    
        +coeff[  9]    *x21*x31*x41*x51
        +coeff[ 10]*x11*x21    *x41*x51
        +coeff[ 11]*x11        *x44    
        +coeff[ 12]        *x31*x45    
        +coeff[ 13]    *x21*x31*x44    
        +coeff[ 14]*x12    *x31*x42    
        +coeff[ 15]        *x31*x44*x51
        +coeff[ 16]    *x24    *x42    
    ;
    v_y_e_q1en_1_175                              =v_y_e_q1en_1_175                              
        +coeff[ 17]*x11*x21*x31*x43    
        +coeff[ 18]*x13    *x31*x41    
        +coeff[ 19]*x11*x21    *x41*x52
        +coeff[ 20]        *x33*x42*x51
        ;

    return v_y_e_q1en_1_175                              ;
}
float p_e_q1en_1_175                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.1990193E-04;
    float xmin[10]={
        -0.40000E-02,-0.60065E-01,-0.59989E-01,-0.30030E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30018E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.13552081E-04, 0.30023754E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_175                              ;
}
float l_e_q1en_1_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9572852E-03;
    float xmin[10]={
        -0.40000E-02,-0.60065E-01,-0.59989E-01,-0.30030E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60061E-01, 0.59988E-01, 0.30018E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.10002006E-02,-0.23653514E-02,-0.69783244E-03, 0.22057512E-03,
        -0.17844988E-02,-0.21071800E-02, 0.59789576E-03,-0.69252175E-03,
        -0.60361141E-03,-0.10534661E-02, 0.22210942E-02, 0.99106145E-03,
        -0.18567110E-02,-0.11068342E-02,-0.23032778E-02,-0.11172871E-02,
         0.16627803E-03, 0.19533536E-05, 0.23767543E-03,-0.20589454E-03,
        -0.60664420E-05, 0.20142522E-03, 0.40062092E-03,-0.14171870E-03,
         0.50108170E-03, 0.12402341E-03, 0.11531078E-03, 0.12077626E-03,
        -0.66529441E-03, 0.47373227E-03, 0.18728575E-03, 0.30477214E-03,
         0.33741738E-03,-0.67605542E-04,-0.87263106E-04, 0.76716242E-04,
         0.10062516E-02,-0.14840199E-02, 0.73396676E-03, 0.30731529E-03,
        -0.16920624E-03, 0.26027972E-03,-0.95071952E-03, 0.28208975E-03,
        -0.79563947E-03, 0.29909439E-03, 0.48955914E-03, 0.14510150E-02,
         0.31739791E-03,-0.67454757E-03, 0.34128118E-03, 0.10180903E-02,
         0.59441605E-03, 0.10904974E-02,-0.57747628E-03, 0.94502285E-03,
         0.96250040E-03,-0.40285609E-03,-0.14336841E-03, 0.32176083E-03,
         0.76534960E-03,-0.74633904E-03, 0.28971059E-03,-0.93326764E-03,
        -0.29358812E-03, 0.36055563E-03, 0.73408324E-03,-0.35493457E-03,
        -0.11216858E-02,-0.10599762E-02, 0.92517555E-03,-0.72839449E-03,
         0.10452690E-02, 0.13865778E-02, 0.70790807E-03,-0.43121388E-03,
         0.92734565E-03, 0.25913927E-02,-0.50085806E-03,-0.62586908E-03,
        -0.14086801E-02, 0.86770050E-03,-0.17104201E-02,-0.29860213E-03,
        -0.15004959E-02,-0.13398418E-02,-0.64359599E-03,-0.76259830E-03,
         0.81138115E-03, 0.67742990E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]            *x42    
        +coeff[  3]    *x22*x31        
        +coeff[  4]*x11*x21*x31    *x51
        +coeff[  5]*x11*x21        *x52
        +coeff[  6]*x13*x21            
        +coeff[  7]*x13        *x41    
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]*x11*x23        *x52
        +coeff[ 10]*x11*x21        *x54
        +coeff[ 11]    *x21    *x44*x52
        +coeff[ 12]*x11*x21*x32*x42*x51
        +coeff[ 13]*x12*x23*x31*x41    
        +coeff[ 14]*x12    *x31*x43*x51
        +coeff[ 15]*x12*x21    *x42*x52
        +coeff[ 16]    *x21        *x51
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 17]            *x41*x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]    *x22    *x41    
        +coeff[ 20]            *x43    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x12        *x41    
        +coeff[ 25]    *x23*x31        
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]        *x33    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]*x11*x21*x32        
        +coeff[ 30]*x11*x21    *x41*x51
        +coeff[ 31]*x11        *x42*x51
        +coeff[ 32]*x11        *x41*x52
        +coeff[ 33]*x12*x21*x31        
        +coeff[ 34]*x12    *x31*x41    
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 35]    *x24        *x51
        +coeff[ 36]        *x33*x41*x51
        +coeff[ 37]        *x31*x41*x53
        +coeff[ 38]*x11*x21*x31*x42    
        +coeff[ 39]*x11    *x31*x42*x51
        +coeff[ 40]*x11*x21        *x53
        +coeff[ 41]*x11    *x31    *x53
        +coeff[ 42]*x12        *x43    
        +coeff[ 43]*x12*x22        *x51
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 44]*x12    *x31*x41*x51
        +coeff[ 45]*x13*x21*x31        
        +coeff[ 46]    *x24*x31*x41    
        +coeff[ 47]    *x21*x33*x42    
        +coeff[ 48]    *x22*x31*x43    
        +coeff[ 49]*x13        *x41*x51
        +coeff[ 50]    *x23*x31    *x52
        +coeff[ 51]    *x22*x31*x41*x52
        +coeff[ 52]    *x21    *x41*x54
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 53]*x11*x21*x33    *x51
        +coeff[ 54]*x11*x22    *x42*x51
        +coeff[ 55]*x11*x21*x31*x42*x51
        +coeff[ 56]*x11*x21*x32    *x52
        +coeff[ 57]*x11    *x31*x41*x53
        +coeff[ 58]*x12*x23    *x41    
        +coeff[ 59]*x12*x21*x32    *x51
        +coeff[ 60]*x12*x21*x31    *x52
        +coeff[ 61]*x12    *x31*x41*x52
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 62]*x12        *x41*x53
        +coeff[ 63]*x13*x21*x32        
        +coeff[ 64]*x13    *x31*x42    
        +coeff[ 65]    *x24    *x43    
        +coeff[ 66]*x13*x21*x31    *x51
        +coeff[ 67]    *x22*x32    *x53
        +coeff[ 68]*x11*x23*x31*x42    
        +coeff[ 69]*x11*x21*x33*x42    
        +coeff[ 70]*x11*x24    *x41*x51
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 71]*x11*x22*x32*x41*x51
        +coeff[ 72]*x11    *x34*x41*x51
        +coeff[ 73]*x11*x21*x32*x41*x52
        +coeff[ 74]*x11    *x33*x41*x52
        +coeff[ 75]*x12    *x33*x42    
        +coeff[ 76]*x12*x22    *x43    
        +coeff[ 77]*x12    *x31*x41*x53
        +coeff[ 78]*x13*x22*x31*x41    
        +coeff[ 79]*x13*x21*x32*x41    
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 80]    *x24*x33*x41    
        +coeff[ 81]    *x24*x32*x42    
        +coeff[ 82]    *x21*x33*x44    
        +coeff[ 83]        *x34*x44    
        +coeff[ 84]*x13    *x31*x42*x51
        +coeff[ 85]    *x23*x33    *x52
        +coeff[ 86]    *x23    *x43*x52
        +coeff[ 87]    *x21*x31*x44*x52
        +coeff[ 88]*x13*x21        *x53
    ;
    v_l_e_q1en_1_175                              =v_l_e_q1en_1_175                              
        +coeff[ 89]    *x21    *x44*x53
        ;

    return v_l_e_q1en_1_175                              ;
}
float x_e_q1en_1_150                              (float *x,int m){
    int ncoeff=  3;
    float avdat= -0.3293687E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
         0.33133916E-03, 0.40009650E-02, 0.77086031E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_150                              ;
}
float t_e_q1en_1_150                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.2532200E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.25436786E-03, 0.60069609E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_150                              ;
}
float y_e_q1en_1_150                              (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1057673E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.10412510E-03, 0.38549930E-01, 0.59995502E-01,-0.13333842E-04,
        -0.65753302E-05,-0.52326113E-05, 0.13343747E-04,-0.17085142E-05,
        -0.18067535E-05,-0.57775524E-05,-0.11693021E-04,-0.72037888E-05,
         0.74234517E-05,-0.55029191E-05, 0.72348166E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1en_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x32*x43    
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x22    *x42    
        +coeff[  6]*x11        *x43    
        +coeff[  7]*x11    *x31*x42    
    ;
    v_y_e_q1en_1_150                              =v_y_e_q1en_1_150                              
        +coeff[  8]                *x53
        +coeff[  9]*x11        *x42*x51
        +coeff[ 10]*x13        *x41    
        +coeff[ 11]*x12        *x41*x51
        +coeff[ 12]*x11    *x32*x42    
        +coeff[ 13]*x11    *x31    *x52
        +coeff[ 14]    *x24    *x41*x51
        ;

    return v_y_e_q1en_1_150                              ;
}
float p_e_q1en_1_150                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1024632E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.98663833E-04, 0.30037709E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_150                              ;
}
float l_e_q1en_1_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9342891E-03;
    float xmin[10]={
        -0.39990E-02,-0.60068E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60071E-01, 0.59992E-01, 0.30042E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.10082399E-02,-0.23757850E-02,-0.72768904E-04,-0.79238642E-03,
         0.46998070E-03,-0.38942549E-03,-0.99683697E-04,-0.29739499E-03,
         0.58857515E-03, 0.11213799E-03,-0.55839097E-04, 0.16617037E-03,
         0.60010271E-03,-0.67248278E-04, 0.13574385E-03, 0.25125794E-03,
         0.98122400E-04,-0.55144745E-03, 0.36050810E-03,-0.60425064E-03,
        -0.34761193E-03,-0.12552675E-02,-0.89673522E-04, 0.32557768E-03,
        -0.50611288E-05, 0.58858882E-03,-0.28872369E-02,-0.27322705E-03,
         0.15078270E-02, 0.28164912E-04,-0.84705959E-03,-0.11389921E-02,
        -0.12051538E-02, 0.36951262E-03, 0.81239163E-03,-0.95188495E-03,
        -0.64231013E-03,-0.53290051E-03, 0.52963261E-03, 0.74881525E-03,
        -0.94093621E-03, 0.33157243E-03,-0.39793874E-03, 0.10874139E-02,
         0.19694825E-03, 0.45728835E-03, 0.51819236E-03, 0.37576782E-02,
         0.27795341E-02, 0.51159726E-03,-0.19341444E-02, 0.68483717E-03,
        -0.56795415E-03,-0.88283635E-03, 0.31196102E-03, 0.44843013E-03,
         0.12970922E-02, 0.77928480E-03, 0.18809446E-03,-0.29346009E-03,
         0.44131934E-03,-0.59619814E-03,-0.10979761E-02, 0.12487135E-02,
         0.17620177E-02, 0.44769290E-03, 0.15975716E-04,-0.64794027E-03,
        -0.57162292E-03, 0.17228893E-02,-0.13234024E-02, 0.11094568E-02,
         0.63476432E-03,-0.12793808E-02,-0.65451086E-03, 0.11448091E-02,
        -0.14545179E-02, 0.11478335E-02, 0.80530223E-03,-0.94956317E-03,
         0.91893936E-03, 0.10736294E-02,-0.56874671E-03,-0.58305234E-03,
         0.30343627E-03,-0.48056180E-02, 0.11026431E-02,-0.98098547E-03,
        -0.10038919E-02, 0.16543312E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]            *x42    
        +coeff[  4]*x13                
        +coeff[  5]        *x32*x42    
        +coeff[  6]*x11*x22    *x42    
        +coeff[  7]*x11        *x44    
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[  8]*x11    *x31*x41*x52
        +coeff[  9]                *x51
        +coeff[ 10]*x11                
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]            *x41*x51
        +coeff[ 13]*x11*x21            
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]    *x22*x31        
        +coeff[ 16]    *x22    *x41    
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]*x11    *x32        
        +coeff[ 19]*x11*x21        *x51
        +coeff[ 20]*x11        *x41*x51
        +coeff[ 21]*x11            *x52
        +coeff[ 22]*x12        *x41    
        +coeff[ 23]    *x21*x31*x42    
        +coeff[ 24]            *x44    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]            *x41*x53
        +coeff[ 30]*x11    *x32*x41    
        +coeff[ 31]*x12        *x41*x51
        +coeff[ 32]        *x31*x42*x52
        +coeff[ 33]    *x22        *x53
        +coeff[ 34]*x11*x23        *x51
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 35]*x11*x21*x32    *x51
        +coeff[ 36]*x11    *x33    *x51
        +coeff[ 37]*x11*x22    *x41*x51
        +coeff[ 38]*x11        *x43*x51
        +coeff[ 39]*x11            *x54
        +coeff[ 40]*x12*x21*x31*x41    
        +coeff[ 41]*x12*x22        *x51
        +coeff[ 42]*x13    *x32        
        +coeff[ 43]        *x32*x44    
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 44]*x13*x21        *x51
        +coeff[ 45]*x13    *x31    *x51
        +coeff[ 46]*x13        *x41*x51
        +coeff[ 47]    *x22*x32*x41*x51
        +coeff[ 48]        *x34*x41*x51
        +coeff[ 49]*x13            *x52
        +coeff[ 50]    *x21*x31    *x54
        +coeff[ 51]*x11    *x34*x41    
        +coeff[ 52]*x11*x21*x31*x41*x52
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 53]*x11    *x31*x42*x52
        +coeff[ 54]*x11    *x31    *x54
        +coeff[ 55]*x12*x22*x32        
        +coeff[ 56]*x12    *x32*x41*x51
        +coeff[ 57]*x12        *x41*x53
        +coeff[ 58]*x13    *x33        
        +coeff[ 59]*x13*x22    *x41    
        +coeff[ 60]        *x33*x44    
        +coeff[ 61]    *x21*x33*x41*x52
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 62]    *x22*x31*x42*x52
        +coeff[ 63]    *x21*x32*x42*x52
        +coeff[ 64]        *x33*x42*x52
        +coeff[ 65]    *x22    *x43*x52
        +coeff[ 66]    *x23        *x54
        +coeff[ 67]    *x21*x32    *x54
        +coeff[ 68]*x11*x21*x33*x41*x51
        +coeff[ 69]*x11*x21*x32*x42*x51
        +coeff[ 70]*x11*x21    *x44*x51
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 71]*x11*x22*x32    *x52
        +coeff[ 72]*x11*x23    *x41*x52
        +coeff[ 73]*x11    *x31*x41*x54
        +coeff[ 74]*x12*x22*x33        
        +coeff[ 75]*x12*x23*x31*x41    
        +coeff[ 76]*x12    *x33*x42    
        +coeff[ 77]*x12*x21*x31*x43    
        +coeff[ 78]*x12    *x31*x44    
        +coeff[ 79]*x12*x22*x32    *x51
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 80]*x12*x22    *x42*x51
        +coeff[ 81]*x12*x22*x31    *x52
        +coeff[ 82]*x13*x22*x32        
        +coeff[ 83]*x13*x21*x32*x41    
        +coeff[ 84]    *x23*x34*x41    
        +coeff[ 85]    *x22*x34*x41*x51
        +coeff[ 86]*x13*x21    *x42*x51
        +coeff[ 87]    *x24    *x43*x51
        +coeff[ 88]    *x21*x33*x43*x51
    ;
    v_l_e_q1en_1_150                              =v_l_e_q1en_1_150                              
        +coeff[ 89]*x13    *x31*x41*x52
        ;

    return v_l_e_q1en_1_150                              ;
}
float x_e_q1en_1_125                              (float *x,int m){
    int ncoeff=  3;
    float avdat=  0.1357072E-03;
    float xmin[10]={
        -0.39992E-02,-0.60064E-01,-0.59983E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60064E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  4]={
        -0.13361851E-03, 0.39998936E-02, 0.77080950E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;

//                 function

    float v_x_e_q1en_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        ;

    return v_x_e_q1en_1_125                              ;
}
float t_e_q1en_1_125                              (float *x,int m){
    int ncoeff=  2;
    float avdat=  0.1093578E-03;
    float xmin[10]={
        -0.39992E-02,-0.60064E-01,-0.59983E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60064E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
        -0.10970645E-03, 0.60064010E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_125                              ;
}
float y_e_q1en_1_125                              (float *x,int m){
    int ncoeff= 22;
    float avdat=  0.9265263E-04;
    float xmin[10]={
        -0.39992E-02,-0.60064E-01,-0.59983E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60064E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 23]={
        -0.83762781E-04, 0.38545918E-01, 0.59984028E-01, 0.29017558E-04,
        -0.25707262E-04, 0.44583280E-05,-0.21421705E-04, 0.48075854E-05,
        -0.11900708E-04,-0.12041047E-04,-0.33740848E-05,-0.17433227E-04,
         0.30153316E-04, 0.12276500E-04,-0.21620046E-04, 0.70091555E-05,
        -0.90083668E-05,-0.14212681E-04, 0.26073028E-04,-0.26823705E-04,
         0.10439165E-04, 0.99268409E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x43    
        +coeff[  4]*x11*x21*x33*x42    
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]            *x42*x51
    ;
    v_y_e_q1en_1_125                              =v_y_e_q1en_1_125                              
        +coeff[  8]    *x21*x32*x41    
        +coeff[  9]*x12        *x42    
        +coeff[ 10]    *x23        *x51
        +coeff[ 11]            *x44*x51
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]        *x32*x42*x51
        +coeff[ 14]    *x21    *x45    
        +coeff[ 15]            *x43*x52
        +coeff[ 16]        *x32*x44    
    ;
    v_y_e_q1en_1_125                              =v_y_e_q1en_1_125                              
        +coeff[ 17]*x11    *x31*x41*x52
        +coeff[ 18]*x12        *x44    
        +coeff[ 19]*x11*x22*x31*x42    
        +coeff[ 20]*x11    *x33*x42    
        +coeff[ 21]*x11    *x31*x43*x51
        ;

    return v_y_e_q1en_1_125                              ;
}
float p_e_q1en_1_125                              (float *x,int m){
    int ncoeff=  4;
    float avdat= -0.1166947E-04;
    float xmin[10]={
        -0.39992E-02,-0.60064E-01,-0.59983E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60064E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  5]={
         0.17521133E-04, 0.45723718E-08,-0.12614028E-06, 0.30037757E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x21 = x2;
    float x31 = x3;
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        ;

    return v_p_e_q1en_1_125                              ;
}
float l_e_q1en_1_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9655645E-03;
    float xmin[10]={
        -0.39992E-02,-0.60064E-01,-0.59983E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60064E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.89974602E-03,-0.18725789E-03,-0.23258659E-02,-0.45014292E-03,
         0.47867093E-03,-0.37544232E-03, 0.97848533E-03, 0.90119109E-03,
         0.15195083E-02,-0.14986092E-02,-0.11930489E-04,-0.11194462E-03,
        -0.33463060E-03,-0.87655324E-04, 0.21937941E-03,-0.27681499E-04,
         0.40808113E-03,-0.10931848E-03,-0.19399666E-03,-0.10532121E-03,
         0.13423749E-03, 0.12086756E-04, 0.79471894E-04,-0.10609878E-02,
        -0.20183125E-03,-0.10739388E-03, 0.23348191E-03, 0.45939174E-03,
         0.97182661E-03,-0.29382607E-03,-0.29056498E-02, 0.88846759E-03,
         0.43523623E-03, 0.50728937E-03, 0.27564753E-03, 0.46761247E-03,
        -0.21377683E-03,-0.28630553E-03, 0.71582326E-03, 0.91689726E-03,
        -0.52374741E-03,-0.31452949E-03,-0.10606443E-02, 0.42276536E-03,
        -0.25902371E-03, 0.26071316E-03, 0.44629996E-03,-0.36367276E-03,
        -0.48345671E-03,-0.90622948E-03,-0.54540928E-03,-0.22384791E-04,
        -0.41781779E-03,-0.15361207E-03, 0.46072309E-03, 0.33491710E-03,
         0.65508514E-03,-0.29040501E-03, 0.89804974E-03, 0.60325459E-03,
        -0.42463909E-03,-0.49116602E-03, 0.38898271E-03,-0.38419411E-03,
        -0.40516656E-03,-0.11858926E-02, 0.67809032E-03, 0.32485728E-03,
         0.19075045E-02, 0.13313501E-02,-0.14080566E-02, 0.12161231E-02,
        -0.84480428E-03, 0.63260522E-03, 0.21040073E-03,-0.48707728E-03,
        -0.97739242E-03, 0.72130875E-03, 0.47271137E-03,-0.76397922E-03,
         0.64792589E-03,-0.67995326E-03, 0.13010605E-02,-0.66283107E-03,
        -0.59262122E-03,-0.59533416E-03,-0.89451775E-03,-0.78896392E-03,
         0.41842519E-03, 0.13693891E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q1en_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]            *x42    
        +coeff[  4]    *x21*x32        
        +coeff[  5]        *x32    *x52
        +coeff[  6]*x11*x21*x31*x41    
        +coeff[  7]    *x23    *x41*x51
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[  8]*x11    *x31*x42*x52
        +coeff[  9]*x12        *x43*x52
        +coeff[ 10]            *x41    
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]            *x41*x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]*x12                
        +coeff[ 15]    *x23            
        +coeff[ 16]        *x32*x41    
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 17]    *x21    *x42    
        +coeff[ 18]            *x43    
        +coeff[ 19]    *x21    *x41*x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]                *x53
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]*x11    *x31    *x51
        +coeff[ 25]*x12    *x31        
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 26]*x12        *x41    
        +coeff[ 27]*x12            *x51
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22        *x52
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x21        *x53
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]*x11    *x32*x41    
        +coeff[ 39]*x11*x22        *x51
        +coeff[ 40]*x11*x21        *x52
        +coeff[ 41]*x12*x22            
        +coeff[ 42]*x12*x21*x31        
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 44]*x12    *x31    *x51
        +coeff[ 45]*x12        *x41*x51
        +coeff[ 46]    *x24*x31        
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]        *x34*x41    
        +coeff[ 49]    *x22*x31*x42    
        +coeff[ 50]    *x21*x33    *x51
        +coeff[ 51]        *x34    *x51
        +coeff[ 52]    *x21    *x43*x51
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 53]            *x44*x51
        +coeff[ 54]        *x32*x41*x52
        +coeff[ 55]    *x21*x31    *x53
        +coeff[ 56]*x11*x22*x31*x41    
        +coeff[ 57]*x11    *x33*x41    
        +coeff[ 58]*x11*x23        *x51
        +coeff[ 59]*x11    *x33    *x51
        +coeff[ 60]*x11        *x43*x51
        +coeff[ 61]*x11    *x31    *x53
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 62]*x12*x21*x32        
        +coeff[ 63]*x12*x22        *x51
        +coeff[ 64]*x12    *x32    *x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]*x13*x21        *x51
        +coeff[ 67]*x13        *x41*x51
        +coeff[ 68]    *x23*x31*x41*x51
        +coeff[ 69]    *x21*x33*x41*x51
        +coeff[ 70]        *x34*x41*x51
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 71]    *x21*x31*x43*x51
        +coeff[ 72]            *x44*x52
        +coeff[ 73]    *x23        *x53
        +coeff[ 74]    *x22*x31    *x53
        +coeff[ 75]*x11    *x33*x42    
        +coeff[ 76]*x11*x24        *x51
        +coeff[ 77]*x11*x21*x33    *x51
        +coeff[ 78]*x11*x22*x31*x41*x51
        +coeff[ 79]*x11*x21*x31*x42*x51
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 80]*x11*x23        *x52
        +coeff[ 81]*x11*x22        *x53
        +coeff[ 82]*x12*x23*x31        
        +coeff[ 83]*x12*x21*x32*x41    
        +coeff[ 84]*x12    *x31*x42*x51
        +coeff[ 85]    *x23*x34        
        +coeff[ 86]*x13*x21*x31*x41    
        +coeff[ 87]*x13    *x32*x41    
        +coeff[ 88]    *x22*x32*x43    
    ;
    v_l_e_q1en_1_125                              =v_l_e_q1en_1_125                              
        +coeff[ 89]    *x22*x31*x44    
        ;

    return v_l_e_q1en_1_125                              ;
}
float x_e_q1en_1_100                              (float *x,int m){
    int ncoeff=  4;
    float avdat= -0.5264169E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  5]={
         0.52271533E-03, 0.39985729E-02, 0.66149300E-06, 0.77088259E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x51 = x5;

//                 function

    float v_x_e_q1en_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        ;

    return v_x_e_q1en_1_100                              ;
}
float t_e_q1en_1_100                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.3940556E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.39190621E-03, 0.60069308E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
//  set up monomials   functions
    float x21 = x2;

//                 function

    float v_t_e_q1en_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        ;

    return v_t_e_q1en_1_100                              ;
}
float y_e_q1en_1_100                              (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.4865607E-04;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.64067317E-04, 0.38547173E-01, 0.59989322E-01, 0.21175314E-04,
        -0.48374095E-05,-0.10173188E-04, 0.45958509E-05,-0.42912947E-06,
        -0.74995364E-05,-0.10079581E-04,-0.12357516E-04,-0.85148704E-05,
        -0.42096776E-05, 0.13050601E-04, 0.10229764E-04, 0.12449645E-04,
         0.10859535E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1en_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]*x11        *x45*x52
        +coeff[  4]*x11        *x42*x51
        +coeff[  5]*x12*x21    *x41    
        +coeff[  6]            *x44*x51
        +coeff[  7]        *x31*x43*x51
    ;
    v_y_e_q1en_1_100                              =v_y_e_q1en_1_100                              
        +coeff[  8]*x13        *x41    
        +coeff[  9]*x11*x21*x31*x42    
        +coeff[ 10]    *x22*x31*x41*x51
        +coeff[ 11]*x11*x21    *x42*x51
        +coeff[ 12]            *x45*x51
        +coeff[ 13]    *x21*x32*x43    
        +coeff[ 14]*x11*x22*x31    *x51
        +coeff[ 15]*x11    *x32*x43    
        +coeff[ 16]*x11    *x31*x43*x51
        ;

    return v_y_e_q1en_1_100                              ;
}
float p_e_q1en_1_100                              (float *x,int m){
    int ncoeff=  2;
    float avdat= -0.1277938E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  3]={
         0.12004114E-03, 0.30037051E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x41 = x4;

//                 function

    float v_p_e_q1en_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        ;

    return v_p_e_q1en_1_100                              ;
}
float l_e_q1en_1_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.9824471E-03;
    float xmin[10]={
        -0.39998E-02,-0.60071E-01,-0.59994E-01,-0.30045E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.98880427E-03,-0.23461767E-02,-0.30636045E-03,-0.60782372E-03,
        -0.16891670E-03,-0.50454255E-03,-0.84870280E-03, 0.55422634E-03,
        -0.70456177E-03,-0.14441228E-04,-0.10133001E-03,-0.99522978E-04,
         0.44910845E-04, 0.15087318E-03, 0.49174963E-04,-0.43387199E-03,
        -0.13444814E-04,-0.43520948E-03, 0.90363254E-04, 0.24690339E-03,
         0.35000281E-03, 0.28292704E-03,-0.38205829E-03, 0.47072396E-03,
        -0.43438995E-03,-0.20729838E-03, 0.47380207E-03, 0.30961313E-03,
         0.33112295E-03,-0.30934316E-03, 0.23957607E-03,-0.83782000E-03,
         0.18016962E-03,-0.32039164E-03, 0.47596049E-03, 0.21398340E-03,
         0.12087884E-02,-0.17637847E-02,-0.48418398E-03, 0.39620594E-04,
        -0.34738128E-03, 0.21741395E-02, 0.41573989E-03,-0.19544404E-03,
         0.48661037E-03,-0.63588045E-03,-0.13523059E-02,-0.69629232E-03,
         0.59339934E-03, 0.93223760E-03,-0.14805229E-03, 0.62810519E-03,
        -0.11612293E-02, 0.20705043E-03,-0.10439642E-02, 0.37808638E-03,
        -0.66034042E-03, 0.66075940E-03, 0.97966194E-03, 0.34368836E-03,
        -0.50646899E-03, 0.62906911E-03, 0.82565850E-03, 0.91112842E-03,
        -0.81181701E-03,-0.10705809E-02,-0.11541637E-02,-0.13823940E-02,
         0.14055737E-02, 0.53720834E-03, 0.17925568E-02,-0.65930292E-03,
        -0.11927752E-02, 0.12264261E-02,-0.69580798E-03, 0.20559721E-02,
        -0.73661620E-03, 0.82025916E-04,-0.20829008E-02,-0.16349289E-02,
         0.96375187E-03, 0.34023204E-03,-0.93804573E-03, 0.14109128E-02,
        -0.84205176E-03, 0.13516514E-02, 0.67276618E-03,-0.32029382E-03,
        -0.51771844E-03, 0.70058048E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1en_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x31    *x51
        +coeff[  5]*x11        *x42*x52
        +coeff[  6]*x11*x22*x31*x41*x51
        +coeff[  7]*x11*x24    *x41*x51
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[  8]*x11    *x33*x42*x51
        +coeff[  9]*x11                
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]        *x33        
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11    *x32        
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]*x11*x21        *x51
        +coeff[ 19]*x12    *x31        
        +coeff[ 20]*x13                
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]        *x33*x41    
        +coeff[ 23]        *x31*x43    
        +coeff[ 24]            *x43*x51
        +coeff[ 25]    *x21    *x41*x52
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 26]            *x41*x53
        +coeff[ 27]*x11    *x33        
        +coeff[ 28]*x11*x21*x31*x41    
        +coeff[ 29]*x11*x21*x31    *x51
        +coeff[ 30]*x11*x21    *x41*x51
        +coeff[ 31]    *x21*x32*x42    
        +coeff[ 32]    *x23*x31    *x51
        +coeff[ 33]        *x34    *x51
        +coeff[ 34]    *x22    *x42*x51
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 35]            *x44*x51
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]    *x21*x31*x41*x52
        +coeff[ 38]    *x22        *x53
        +coeff[ 39]*x11*x23    *x41    
        +coeff[ 40]*x11    *x32*x42    
        +coeff[ 41]*x11*x21*x31    *x52
        +coeff[ 42]*x11*x21        *x53
        +coeff[ 43]*x12*x21*x32        
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 44]*x12*x21    *x42    
        +coeff[ 45]*x12    *x31*x41*x51
        +coeff[ 46]*x12*x21        *x52
        +coeff[ 47]*x13*x21        *x51
        +coeff[ 48]    *x22*x33    *x51
        +coeff[ 49]        *x34*x41*x51
        +coeff[ 50]        *x32*x43*x51
        +coeff[ 51]    *x23    *x41*x52
        +coeff[ 52]        *x31*x43*x52
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 53]        *x33    *x53
        +coeff[ 54]        *x32*x41*x53
        +coeff[ 55]        *x31*x41*x54
        +coeff[ 56]*x11*x22*x32    *x51
        +coeff[ 57]*x11    *x34    *x51
        +coeff[ 58]*x11*x21    *x43*x51
        +coeff[ 59]*x11*x21    *x42*x52
        +coeff[ 60]*x11    *x32    *x53
        +coeff[ 61]*x12    *x31*x41*x52
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 62]*x13*x21*x31    *x51
        +coeff[ 63]    *x24*x32    *x51
        +coeff[ 64]    *x23*x33    *x51
        +coeff[ 65]*x13*x21    *x41*x51
        +coeff[ 66]    *x22*x32*x42*x51
        +coeff[ 67]    *x23*x32    *x52
        +coeff[ 68]    *x21*x31*x43*x52
        +coeff[ 69]    *x21*x31*x42*x53
        +coeff[ 70]*x11*x24*x31*x41    
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 71]*x11*x22*x33*x41    
        +coeff[ 72]*x11*x22*x31*x43    
        +coeff[ 73]*x11*x22*x31*x42*x51
        +coeff[ 74]*x11*x21*x32*x42*x51
        +coeff[ 75]*x11    *x34    *x52
        +coeff[ 76]*x11    *x31*x42*x53
        +coeff[ 77]*x11*x22        *x54
        +coeff[ 78]*x11*x21*x31    *x54
        +coeff[ 79]*x11    *x32    *x54
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 80]*x12*x23*x32        
        +coeff[ 81]*x12    *x34    *x51
        +coeff[ 82]*x12    *x33    *x52
        +coeff[ 83]*x12*x21*x31*x41*x52
        +coeff[ 84]*x12    *x31*x42*x52
        +coeff[ 85]*x12*x21        *x54
        +coeff[ 86]*x12    *x31    *x54
        +coeff[ 87]*x13*x24            
        +coeff[ 88]*x13    *x34        
    ;
    v_l_e_q1en_1_100                              =v_l_e_q1en_1_100                              
        +coeff[ 89]*x13*x23    *x41    
        ;

    return v_l_e_q1en_1_100                              ;
}
