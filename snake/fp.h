float x_e_fp_1200                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3362806E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.32848559E-01, 0.47689268E+00,-0.11166961E-01,-0.30632775E-01,
         0.37113119E-01,-0.86716730E-02, 0.56005376E-02,-0.12202922E-01,
        -0.81547238E-02,-0.18624624E-01, 0.18970239E-02, 0.14614187E-02,
        -0.52604116E-02,-0.73492546E-02, 0.92499899E-02, 0.17578388E-02,
        -0.59668417E-03, 0.73625124E-03,-0.19022900E-02,-0.14804370E-02,
        -0.13586995E-02, 0.23511557E-02, 0.98738550E-04, 0.17049933E-02,
         0.54233130E-02, 0.10740500E-02,-0.16999032E-02,-0.23425822E-02,
         0.17814563E-02,-0.10343682E-02,-0.32597173E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_1200                                  =v_x_e_fp_1200                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x22        *x53
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_fp_1200                                  =v_x_e_fp_1200                                  
        +coeff[ 17]*x11*x21            
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_1200                                  =v_x_e_fp_1200                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_1200                                  ;
}
float t_e_fp_1200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6811236E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64572594E-02,-0.28004151E-01, 0.78750141E-01, 0.36704300E-02,
        -0.59034494E-02,-0.41310539E-03,-0.21159162E-02,-0.25084412E-02,
        -0.88638818E-03,-0.11140620E-02, 0.24996215E-03, 0.23552771E-03,
        -0.45890996E-03,-0.23505658E-03,-0.34826106E-03, 0.48546804E-03,
         0.17489098E-02,-0.10649015E-03, 0.49610960E-03, 0.79760088E-04,
         0.23983981E-03, 0.85414824E-03,-0.27578266E-03, 0.39461633E-03,
        -0.78757346E-03,-0.54451975E-03,-0.41409966E-03, 0.21143715E-03,
         0.70019887E-03, 0.10101196E-03, 0.10257288E-03, 0.23694271E-03,
        -0.13846481E-03,-0.12049867E-03,-0.11371964E-03,-0.29127090E-03,
         0.96578304E-04, 0.41658161E-03,-0.31514678E-03,-0.18340563E-03,
        -0.15338075E-04, 0.30657506E-04,-0.21261380E-04,-0.65539265E-04,
         0.30899329E-04, 0.12157855E-03, 0.37951831E-04, 0.11536845E-03,
        -0.31421383E-03, 0.13481810E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_1200                                  =v_t_e_fp_1200                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_1200                                  =v_t_e_fp_1200                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_1200                                  =v_t_e_fp_1200                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_1200                                  =v_t_e_fp_1200                                  
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]    *x21*x33*x41    
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]    *x22        *x51
    ;
    v_t_e_fp_1200                                  =v_t_e_fp_1200                                  
        +coeff[ 44]*x11            *x52
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_fp_1200                                  ;
}
float y_e_fp_1200                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7066326E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.86118001E-04,-0.22644682E-01,-0.35385426E-01,-0.10247526E-01,
        -0.87576769E-02, 0.18402398E-01,-0.37855438E-02,-0.13272920E-01,
        -0.69516855E-02, 0.41810828E-02, 0.19919528E-02,-0.44831182E-02,
         0.13335014E-03, 0.39631006E-03,-0.12844098E-02, 0.10213795E-02,
         0.22155726E-02,-0.13773916E-02,-0.99896104E-03,-0.10845613E-02,
        -0.23103440E-02,-0.11977588E-02,-0.10386943E-02, 0.11856310E-02,
        -0.10925372E-02, 0.26143284E-03,-0.47025602E-04, 0.25580099E-03,
         0.19430298E-03,-0.11880431E-03,-0.11231661E-02, 0.22908040E-03,
        -0.31982188E-03,-0.73407457E-04, 0.13039422E-02, 0.71183668E-03,
         0.90237882E-03,-0.14078647E-02, 0.11092003E-03,-0.36795798E-03,
         0.60408733E-04, 0.13448282E-02,-0.13049644E-02,-0.27862116E-04,
        -0.25879833E-03,-0.70585695E-03,-0.96762305E-04, 0.12945314E-03,
        -0.54653751E-05, 0.11854317E-02, 0.48874214E-03, 0.13014827E-02,
        -0.11696421E-02, 0.50114002E-03,-0.56251336E-03,-0.46930482E-03,
        -0.11960577E-04,-0.22622540E-04, 0.33089472E-03, 0.12238498E-03,
         0.20803922E-03, 0.12488586E-02, 0.44998644E-04, 0.66187239E-03,
         0.51017897E-03,-0.17940191E-02, 0.47678075E-03,-0.13835421E-03,
        -0.11302447E-02, 0.10191173E-02,-0.56964176E-03, 0.73923217E-03,
         0.41732311E-03,-0.52958733E-03, 0.10773441E-02,-0.28947767E-03,
        -0.21857915E-04, 0.90746180E-04, 0.10252146E-03,-0.25236743E-03,
        -0.10105703E-03, 0.57169244E-04,-0.17404686E-03,-0.13297821E-03,
        -0.54118014E-03,-0.13325294E-03,-0.13170396E-03, 0.78869933E-04,
         0.19187752E-03,-0.10603976E-03,-0.31602214E-03,-0.18398261E-03,
         0.55084936E-03, 0.14468981E-03,-0.18528399E-03, 0.38649159E-03,
        -0.43174849E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]    *x22*x31    *x53
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]*x11        *x41    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]*x11        *x41*x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x24    *x41    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 35]    *x22    *x41*x52
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x21    *x41*x53
        +coeff[ 38]        *x34*x41*x51
        +coeff[ 39]    *x22*x34*x41    
        +coeff[ 40]        *x31*x44*x52
        +coeff[ 41]    *x23*x32*x43    
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]        *x31*x42*x51
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 44]    *x23*x31        
        +coeff[ 45]        *x32*x41*x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x21*x31    *x52
        +coeff[ 49]        *x31*x42*x52
        +coeff[ 50]        *x33    *x52
        +coeff[ 51]    *x21*x31    *x53
        +coeff[ 52]    *x22*x32*x43    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 53]    *x24*x33        
        +coeff[ 54]        *x33    *x53
        +coeff[ 55]    *x23*x31    *x53
        +coeff[ 56]                *x51
        +coeff[ 57]*x11*x21*x31        
        +coeff[ 58]    *x21    *x43    
        +coeff[ 59]*x11*x21    *x41*x51
        +coeff[ 60]    *x21    *x43*x51
        +coeff[ 61]    *x21*x31*x42*x51
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 62]*x11    *x31    *x52
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x22*x31*x42*x51
        +coeff[ 66]    *x23*x33        
        +coeff[ 67]*x11*x21*x31    *x52
        +coeff[ 68]    *x22*x32*x41*x51
        +coeff[ 69]    *x23    *x43*x51
        +coeff[ 70]    *x24*x32*x41    
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 71]    *x22    *x43*x52
        +coeff[ 72]    *x23*x33    *x51
        +coeff[ 73]    *x24    *x43*x51
        +coeff[ 74]    *x23    *x41*x53
        +coeff[ 75]*x11    *x31*x42*x53
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]            *x45    
        +coeff[ 78]*x11*x22*x31        
        +coeff[ 79]            *x43*x52
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 80]*x11*x23    *x41    
        +coeff[ 81]*x11    *x31*x42*x51
        +coeff[ 82]*x11*x22    *x41*x51
        +coeff[ 83]        *x32*x41*x52
        +coeff[ 84]    *x23*x31*x42    
        +coeff[ 85]*x11*x22*x31    *x51
        +coeff[ 86]    *x24    *x41*x51
        +coeff[ 87]*x11        *x41*x53
        +coeff[ 88]*x11*x21*x31*x42*x51
    ;
    v_y_e_fp_1200                                  =v_y_e_fp_1200                                  
        +coeff[ 89]*x11*x22*x33        
        +coeff[ 90]    *x24*x31    *x51
        +coeff[ 91]    *x24    *x43    
        +coeff[ 92]    *x23    *x41*x52
        +coeff[ 93]*x11    *x31*x42*x52
        +coeff[ 94]        *x31*x42*x53
        +coeff[ 95]    *x22    *x41*x53
        +coeff[ 96]    *x22*x31*x42*x52
        ;

    return v_y_e_fp_1200                                  ;
}
float p_e_fp_1200                                  (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.7453226E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.58752627E-04, 0.16863106E-01,-0.35098858E-01, 0.17091846E-01,
        -0.82102008E-02,-0.10492195E-01, 0.78655658E-02,-0.62134053E-03,
        -0.10579068E-03, 0.15570770E-03,-0.28185123E-05, 0.52946201E-02,
         0.15814358E-02, 0.40720045E-02,-0.63397986E-03, 0.30357372E-02,
         0.15682471E-02, 0.11516163E-02, 0.52612733E-04,-0.18486306E-03,
         0.43001547E-03, 0.16303830E-02, 0.59041660E-03, 0.37864278E-03,
        -0.37071973E-03,-0.12163103E-02,-0.18383158E-03,-0.15511441E-02,
        -0.77187020E-03,-0.37312802E-03, 0.78675264E-04,-0.73766627E-03,
         0.86567903E-04, 0.11714373E-02, 0.95823186E-03,-0.28359509E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_1200                                  =v_p_e_fp_1200                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_1200                                  =v_p_e_fp_1200                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_fp_1200                                  =v_p_e_fp_1200                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_fp_1200                                  =v_p_e_fp_1200                                  
        +coeff[ 35]            *x41*x53
        ;

    return v_p_e_fp_1200                                  ;
}
float l_e_fp_1200                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1936974E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.19246776E-01,-0.31182063E+00,-0.25486972E-01, 0.12425548E-01,
        -0.34142453E-01,-0.13338742E-01,-0.79581505E-02,-0.45514996E-02,
         0.21244942E-02, 0.11247835E-01,-0.10360708E-03,-0.31037831E-02,
         0.88067511E-02, 0.56675938E-03,-0.33982331E-02,-0.68953761E-03,
         0.33576884E-02,-0.15525171E-02, 0.18136213E-02,-0.12266852E-02,
         0.20658462E-02,-0.24528924E-03,-0.48348095E-03, 0.19019644E-02,
        -0.63405820E-03, 0.13955268E-02, 0.52639475E-03, 0.45172218E-03,
         0.82219357E-03,-0.75809588E-03, 0.45775590E-02, 0.28652807E-02,
         0.16189100E-02, 0.54627360E-03, 0.31748123E-03, 0.98978076E-03,
        -0.17566292E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_fp_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_1200                                  =v_l_e_fp_1200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]        *x34        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_1200                                  =v_l_e_fp_1200                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]            *x44*x51
        +coeff[ 25]        *x32    *x51
    ;
    v_l_e_fp_1200                                  =v_l_e_fp_1200                                  
        +coeff[ 26]*x11*x22            
        +coeff[ 27]*x11    *x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]        *x31*x43*x51
        +coeff[ 33]    *x22*x31    *x52
        +coeff[ 34]*x11            *x54
    ;
    v_l_e_fp_1200                                  =v_l_e_fp_1200                                  
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]        *x33*x41*x53
        ;

    return v_l_e_fp_1200                                  ;
}
float x_e_fp_1100                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2999578E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.29954594E-01, 0.47658381E+00,-0.10976221E-01,-0.30649101E-01,
         0.37237488E-01,-0.86665330E-02, 0.56349630E-02,-0.12115582E-01,
        -0.81574125E-02,-0.18561324E-01, 0.23558699E-02, 0.13239518E-02,
        -0.14140080E-02, 0.13880678E-02,-0.53031524E-02,-0.72972104E-02,
         0.91481544E-02,-0.56192861E-03, 0.76838519E-03,-0.19121028E-02,
        -0.14982773E-02, 0.24850280E-02, 0.11439063E-03, 0.16050820E-02,
         0.11211614E-02,-0.17930713E-02,-0.25009383E-02, 0.18676602E-02,
        -0.72522915E-03, 0.47518094E-02,-0.72288828E-03,-0.34418574E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_1100                                  =v_x_e_fp_1100                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_1100                                  =v_x_e_fp_1100                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_1100                                  =v_x_e_fp_1100                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22*x31*x41*x51
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_1100                                  ;
}
float t_e_fp_1100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6212545E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59878416E-02,-0.28010381E-01, 0.78749917E-01, 0.36666016E-02,
        -0.59024761E-02,-0.41061550E-03,-0.21250655E-02,-0.24711036E-02,
        -0.11034820E-02, 0.22987057E-03,-0.88386424E-03, 0.26602586E-03,
        -0.42535507E-03,-0.22366612E-03,-0.34967728E-03, 0.48623240E-03,
         0.17598931E-02,-0.10896351E-03, 0.50673250E-03, 0.69276342E-04,
         0.22062007E-03, 0.84099313E-03,-0.26085461E-03, 0.39495167E-03,
        -0.74698526E-03,-0.50442794E-03,-0.45346766E-03, 0.21887160E-03,
         0.20515670E-03, 0.99702913E-04, 0.13593967E-03, 0.70737966E-04,
         0.27124540E-03,-0.12937748E-03,-0.11366948E-03, 0.35388346E-03,
        -0.28105365E-03,-0.21582385E-03, 0.60669356E-03,-0.13953378E-04,
         0.33389402E-04,-0.24723589E-04,-0.48708473E-04, 0.23077739E-04,
        -0.87929562E-04, 0.53551521E-04,-0.24802639E-03, 0.15790039E-04,
        -0.23040184E-03, 0.11881987E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_1100                                  =v_t_e_fp_1100                                  
        +coeff[  8]            *x42    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_1100                                  =v_t_e_fp_1100                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_1100                                  =v_t_e_fp_1100                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41*x52
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]*x11*x21*x31*x41    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_1100                                  =v_t_e_fp_1100                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]    *x22        *x51
        +coeff[ 43]*x11            *x52
    ;
    v_t_e_fp_1100                                  =v_t_e_fp_1100                                  
        +coeff[ 44]*x11*x21    *x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_fp_1100                                  ;
}
float y_e_fp_1100                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1592393E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.17739374E-03,-0.22663321E-01,-0.35419978E-01,-0.10354157E-01,
        -0.88959616E-02, 0.18373094E-01,-0.38142288E-02,-0.13254267E-01,
        -0.70815869E-02, 0.42366162E-02, 0.19092270E-02,-0.44564516E-02,
         0.38926658E-03,-0.12513835E-02, 0.10201201E-02, 0.24535458E-02,
        -0.24675727E-02,-0.14266801E-02,-0.99548895E-03,-0.11233763E-02,
        -0.11637943E-02,-0.98942663E-03, 0.14351520E-02,-0.87790220E-03,
        -0.13386864E-02, 0.26085871E-03,-0.42254720E-04, 0.22819276E-03,
         0.52242633E-03,-0.45205947E-04,-0.13047924E-02, 0.39694124E-03,
        -0.33294543E-03, 0.12097307E-02, 0.10297997E-02,-0.45849266E-03,
         0.11344357E-03,-0.11688421E-02, 0.47926413E-03, 0.27302097E-03,
        -0.72765123E-03, 0.13190185E-03, 0.60341154E-05,-0.78273547E-03,
        -0.53217558E-04, 0.11077211E-02, 0.45961968E-03, 0.10881173E-03,
        -0.15196393E-02, 0.10861271E-02,-0.33583475E-03, 0.38821180E-03,
         0.17822531E-03,-0.16310010E-04,-0.19788282E-04, 0.12546162E-03,
         0.14959708E-03,-0.68754089E-04, 0.32695354E-03, 0.10817156E-02,
         0.77162396E-04,-0.21224126E-03, 0.39709575E-03, 0.18067547E-03,
        -0.14624061E-02,-0.18162069E-03,-0.89622725E-03, 0.11864501E-03,
        -0.14993642E-03, 0.57383929E-03,-0.35586182E-03, 0.34278221E-03,
        -0.62852306E-03, 0.10514143E-02, 0.60536154E-05, 0.13319876E-04,
        -0.26079126E-04, 0.55915378E-04,-0.94836934E-04,-0.13100173E-03,
        -0.23669654E-04,-0.96329262E-04,-0.13487859E-03, 0.19310492E-03,
        -0.30806992E-03, 0.78388832E-04,-0.10995183E-03,-0.10795018E-03,
         0.76666003E-03,-0.20900062E-03, 0.15700402E-03, 0.22510957E-03,
         0.42823298E-03, 0.21787382E-03, 0.29317485E-03, 0.69156045E-03,
         0.18388007E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]            *x41*x53
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]    *x23    *x41*x51
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]    *x21    *x41*x53
        +coeff[ 25]*x11        *x41    
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]*x11        *x41*x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 35]        *x33    *x53
        +coeff[ 36]    *x22*x33    *x52
        +coeff[ 37]    *x21*x31    *x51
        +coeff[ 38]    *x21    *x43    
        +coeff[ 39]    *x21*x32*x41    
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]*x11*x21*x31    *x51
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 44]            *x43*x52
        +coeff[ 45]        *x31*x42*x52
        +coeff[ 46]    *x23*x31    *x51
        +coeff[ 47]        *x31*x44*x51
        +coeff[ 48]    *x23*x31*x42    
        +coeff[ 49]    *x21*x31    *x53
        +coeff[ 50]        *x31*x42*x53
        +coeff[ 51]    *x21*x33*x42*x51
        +coeff[ 52]    *x24*x31    *x52
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 53]                *x51
        +coeff[ 54]*x11*x21*x31        
        +coeff[ 55]            *x45    
        +coeff[ 56]*x11*x21    *x41*x51
        +coeff[ 57]    *x21    *x41*x52
        +coeff[ 58]    *x21    *x43*x51
        +coeff[ 59]    *x21*x31*x42*x51
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]*x11*x22    *x41*x51
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 62]        *x33    *x52
        +coeff[ 63]        *x32*x43*x51
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]*x11*x21*x31    *x52
        +coeff[ 66]    *x22*x32*x41*x51
        +coeff[ 67]*x11        *x41*x53
        +coeff[ 68]*x11    *x31    *x53
        +coeff[ 69]    *x24*x33        
        +coeff[ 70]    *x23*x32*x41*x51
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 71]    *x23*x33    *x51
        +coeff[ 72]    *x24    *x43*x51
        +coeff[ 73]    *x23    *x41*x53
        +coeff[ 74]    *x21            
        +coeff[ 75]    *x22            
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]        *x33    *x51
        +coeff[ 79]        *x32*x43    
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 80]*x11    *x32    *x51
        +coeff[ 81]*x11*x23    *x41    
        +coeff[ 82]*x11    *x31*x42*x51
        +coeff[ 83]            *x45*x51
        +coeff[ 84]    *x23    *x43    
        +coeff[ 85]*x11    *x32*x41*x51
        +coeff[ 86]        *x32*x41*x52
        +coeff[ 87]*x11*x22*x31    *x51
        +coeff[ 88]    *x22*x31    *x52
    ;
    v_y_e_fp_1100                                  =v_y_e_fp_1100                                  
        +coeff[ 89]        *x33*x42*x51
        +coeff[ 90]    *x23*x33        
        +coeff[ 91]*x11*x21*x31*x42*x51
        +coeff[ 92]    *x23    *x41*x52
        +coeff[ 93]    *x21*x32*x41*x52
        +coeff[ 94]    *x24*x31*x42    
        +coeff[ 95]    *x23    *x43*x51
        +coeff[ 96]    *x22    *x41*x53
        ;

    return v_y_e_fp_1100                                  ;
}
float p_e_fp_1100                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.2446843E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.20983523E-03, 0.16923202E-01,-0.35005189E-01, 0.17076254E-01,
        -0.82343454E-02,-0.10509700E-01, 0.78761848E-02,-0.61921810E-03,
        -0.95001124E-04, 0.13036688E-03,-0.30566156E-06, 0.52747549E-02,
         0.15793064E-02, 0.40951292E-02,-0.62870461E-03, 0.30378283E-02,
         0.15813842E-02, 0.11602687E-02, 0.12200168E-04,-0.18452830E-03,
         0.42423976E-03, 0.16536951E-02, 0.60147134E-03, 0.38045686E-03,
        -0.12093860E-02,-0.38154240E-03,-0.15214551E-02,-0.77019626E-03,
        -0.34120856E-03, 0.53432736E-05, 0.16461476E-03,-0.19448885E-03,
        -0.79900265E-03, 0.95763622E-04, 0.11529431E-02, 0.94843307E-03,
        -0.26478298E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_1100                                  =v_p_e_fp_1100                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_1100                                  =v_p_e_fp_1100                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_1100                                  =v_p_e_fp_1100                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_fp_1100                                  =v_p_e_fp_1100                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        ;

    return v_p_e_fp_1100                                  ;
}
float l_e_fp_1100                                  (float *x,int m){
    int ncoeff= 43;
    float avdat= -0.1963023E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
         0.19671403E-01,-0.31204841E+00,-0.25328387E-01, 0.12258616E-01,
        -0.35000600E-01,-0.12997096E-01,-0.76897927E-02,-0.40174164E-02,
         0.11197602E-01, 0.37716917E-03, 0.60525122E-02,-0.30097405E-02,
         0.23608594E-02, 0.35958246E-02, 0.86265709E-02,-0.34514952E-02,
        -0.97605411E-03,-0.13289396E-02, 0.21379266E-02, 0.18592195E-02,
         0.10037251E-02,-0.29058757E-03, 0.10643527E-02, 0.37494041E-02,
        -0.19870393E-03, 0.42913033E-03,-0.84722857E-03, 0.10261325E-02,
         0.84430201E-03,-0.11399899E-02,-0.67116180E-03,-0.37061441E-03,
         0.12898357E-02,-0.92632044E-03, 0.51956455E-03,-0.12092518E-02,
         0.55488141E-03,-0.84524509E-03,-0.59396034E-03,-0.79328916E-03,
        -0.69058308E-03,-0.88223635E-03, 0.87932375E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_1100                                  =v_l_e_fp_1100                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]*x11*x21            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_fp_1100                                  =v_l_e_fp_1100                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]*x12                
        +coeff[ 25]        *x31*x42    
    ;
    v_l_e_fp_1100                                  =v_l_e_fp_1100                                  
        +coeff[ 26]            *x42*x51
        +coeff[ 27]    *x24            
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]    *x21*x31*x41*x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]            *x42*x52
        +coeff[ 34]*x11    *x32    *x51
    ;
    v_l_e_fp_1100                                  =v_l_e_fp_1100                                  
        +coeff[ 35]    *x23        *x52
        +coeff[ 36]*x11    *x34        
        +coeff[ 37]*x11    *x32*x42    
        +coeff[ 38]*x12*x22*x31        
        +coeff[ 39]        *x34    *x52
        +coeff[ 40]*x11*x24*x31        
        +coeff[ 41]*x11*x23*x32        
        +coeff[ 42]*x11        *x43*x53
        ;

    return v_l_e_fp_1100                                  ;
}
float x_e_fp_1000                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3400850E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.34656484E-01, 0.47690469E+00,-0.10795964E-01,-0.30602088E-01,
         0.37206311E-01,-0.86712940E-02, 0.56921225E-02,-0.12129081E-01,
        -0.81780097E-02,-0.18528683E-01, 0.19015707E-02,-0.14125706E-02,
         0.14133155E-02,-0.53512552E-02,-0.73676771E-02, 0.91025084E-02,
         0.18073916E-02,-0.56852779E-03, 0.75844734E-03,-0.18608299E-02,
        -0.15184751E-02, 0.23090031E-02, 0.24145733E-04, 0.16875181E-02,
         0.53639072E-02, 0.11143906E-02,-0.17887419E-02,-0.24985075E-02,
         0.16397735E-02,-0.88578206E-03,-0.30879993E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_1000                                  =v_x_e_fp_1000                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x22        *x53
    ;
    v_x_e_fp_1000                                  =v_x_e_fp_1000                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_1000                                  =v_x_e_fp_1000                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_1000                                  ;
}
float t_e_fp_1000                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6852973E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.67400173E-02,-0.27961558E-01, 0.78732342E-01, 0.36532236E-02,
        -0.59024580E-02,-0.21158389E-02,-0.25184336E-02,-0.41373339E-03,
        -0.11162864E-02, 0.25042045E-03,-0.88313664E-03, 0.23957014E-03,
        -0.48221371E-03,-0.19195207E-03,-0.37233648E-03, 0.49854961E-03,
         0.17318750E-02,-0.95098541E-04, 0.52914477E-03, 0.11364315E-03,
         0.23845570E-03, 0.81820611E-03,-0.26794348E-03, 0.38017379E-03,
        -0.73754368E-03,-0.53816719E-03,-0.42759589E-03, 0.21334189E-03,
         0.10305626E-03, 0.10089517E-03, 0.28544446E-03,-0.12444824E-03,
        -0.10795927E-03,-0.11755661E-03, 0.39960406E-03,-0.26803339E-03,
        -0.25311630E-03,-0.25593615E-03, 0.21607286E-03,-0.21711104E-03,
         0.58569910E-03, 0.26488002E-04, 0.19392404E-04,-0.21302389E-04,
        -0.49274327E-04, 0.28951790E-04, 0.82564962E-04,-0.24004653E-03,
         0.97314318E-04, 0.11561126E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_1000                                  =v_t_e_fp_1000                                  
        +coeff[  8]            *x42    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_1000                                  =v_t_e_fp_1000                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_1000                                  =v_t_e_fp_1000                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x23            
        +coeff[ 30]    *x22*x31*x41    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]            *x42*x52
        +coeff[ 34]    *x23    *x42    
    ;
    v_t_e_fp_1000                                  =v_t_e_fp_1000                                  
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]        *x32*x42*x51
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]    *x23        *x53
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]    *x22    *x41    
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_fp_1000                                  =v_t_e_fp_1000                                  
        +coeff[ 44]    *x22        *x51
        +coeff[ 45]*x11            *x52
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_fp_1000                                  ;
}
float y_e_fp_1000                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2347366E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.24888013E-03,-0.22757389E-01,-0.35453003E-01,-0.10259063E-01,
        -0.89006973E-02, 0.18312428E-01,-0.38334278E-02,-0.13192398E-01,
        -0.69251917E-02, 0.41860426E-02, 0.20144833E-02,-0.43930481E-02,
        -0.24623324E-02, 0.38582718E-03,-0.12441416E-02, 0.99842343E-03,
         0.22211589E-02,-0.97435818E-03,-0.12841892E-02,-0.10333722E-02,
        -0.11472623E-02,-0.12144775E-02, 0.12388090E-02,-0.12031019E-02,
        -0.13410097E-02, 0.26455670E-03,-0.54211130E-04, 0.30752245E-03,
         0.23800765E-03,-0.11054892E-03,-0.10688470E-02, 0.38816669E-03,
        -0.31772064E-03, 0.12487539E-02,-0.68226957E-03,-0.16958747E-03,
        -0.13237982E-02,-0.56906028E-04,-0.11542689E-02,-0.68764971E-03,
        -0.10188190E-03, 0.17041231E-03,-0.13193027E-04,-0.22902340E-03,
         0.12548078E-02, 0.42796618E-03, 0.76411257E-03, 0.69022179E-04,
         0.48289157E-03,-0.17017428E-02, 0.10970159E-02, 0.22056907E-03,
         0.85493748E-03,-0.19823028E-04, 0.12064608E-03,-0.80434380E-04,
         0.99738862E-03, 0.96672093E-05, 0.84710562E-04,-0.28263499E-04,
        -0.25393168E-03, 0.97052229E-03, 0.47997033E-03,-0.88186882E-03,
         0.30951851E-03, 0.40024635E-03,-0.34779325E-03, 0.62758412E-03,
        -0.49785228E-03, 0.52923860E-03, 0.10915028E-02, 0.56475774E-05,
        -0.72156472E-05,-0.22673174E-04, 0.51193748E-03, 0.57446105E-04,
         0.63181076E-04, 0.52302556E-04,-0.12190831E-03,-0.91684298E-04,
         0.11700908E-03,-0.18574421E-03,-0.50364691E-03,-0.35744783E-04,
         0.39261206E-04, 0.17075308E-03,-0.12756666E-03,-0.19804915E-03,
         0.10115997E-03,-0.94253504E-04, 0.54540602E-03, 0.17513051E-03,
         0.45908691E-03, 0.34813417E-03,-0.43969645E-03, 0.46770566E-03,
         0.21785223E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 17]        *x31    *x53
        +coeff[ 18]            *x43    
        +coeff[ 19]        *x31*x42    
        +coeff[ 20]    *x22*x31        
        +coeff[ 21]            *x41*x53
        +coeff[ 22]    *x23    *x41*x51
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]    *x21    *x41*x53
        +coeff[ 25]*x11        *x41    
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]*x11        *x41*x51
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 35]    *x21*x31*x44    
        +coeff[ 36]    *x23*x31*x42    
        +coeff[ 37]    *x22*x33    *x52
        +coeff[ 38]    *x21*x31    *x51
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]        *x33    *x51
        +coeff[ 41]*x11*x21*x31    *x51
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]            *x43*x52
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 44]        *x31*x42*x52
        +coeff[ 45]    *x23*x31    *x51
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]        *x31*x44*x51
        +coeff[ 48]    *x23*x32*x41    
        +coeff[ 49]    *x22*x31*x42*x51
        +coeff[ 50]    *x21*x31    *x53
        +coeff[ 51]    *x21*x31*x44*x51
        +coeff[ 52]    *x23    *x43*x51
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 53]*x11*x21*x31        
        +coeff[ 54]*x11*x21    *x41*x51
        +coeff[ 55]    *x21    *x41*x52
        +coeff[ 56]    *x21*x31*x42*x51
        +coeff[ 57]    *x24*x31        
        +coeff[ 58]*x11    *x31    *x52
        +coeff[ 59]    *x21*x33    *x51
        +coeff[ 60]        *x32*x41*x52
        +coeff[ 61]    *x22*x31    *x52
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 62]        *x33    *x52
        +coeff[ 63]    *x22*x32*x41*x51
        +coeff[ 64]    *x21    *x45*x51
        +coeff[ 65]    *x22*x33*x42    
        +coeff[ 66]        *x31*x42*x53
        +coeff[ 67]    *x24*x33        
        +coeff[ 68]        *x33    *x53
        +coeff[ 69]    *x22    *x43*x52
        +coeff[ 70]    *x23    *x41*x53
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 71]    *x21            
        +coeff[ 72]                *x51
        +coeff[ 73]*x12        *x41    
        +coeff[ 74]    *x21*x31*x42    
        +coeff[ 75]    *x21*x32*x41    
        +coeff[ 76]*x11    *x31*x42    
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]    *x22    *x43    
        +coeff[ 79]*x11*x23    *x41    
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 80]*x11*x21*x32*x41    
        +coeff[ 81]*x11*x22    *x41*x51
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]        *x31*x41*x53
        +coeff[ 84]*x11        *x44*x51
        +coeff[ 85]    *x23*x33        
        +coeff[ 86]*x11*x21*x31    *x52
        +coeff[ 87]    *x24    *x41*x51
        +coeff[ 88]*x11        *x41*x53
    ;
    v_y_e_fp_1000                                  =v_y_e_fp_1000                                  
        +coeff[ 89]*x11    *x31    *x53
        +coeff[ 90]    *x23    *x41*x52
        +coeff[ 91]    *x21*x32*x41*x52
        +coeff[ 92]    *x24*x31*x42    
        +coeff[ 93]    *x22    *x41*x53
        +coeff[ 94]    *x22*x31*x42*x52
        +coeff[ 95]    *x23*x33    *x51
        +coeff[ 96]*x11*x22*x32*x41*x51
        ;

    return v_y_e_fp_1000                                  ;
}
float p_e_fp_1000                                  (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.1107975E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.94376519E-04, 0.16990457E-01,-0.34876268E-01, 0.16946819E-01,
        -0.82307039E-02,-0.10517324E-01, 0.78617325E-02,-0.54935564E-03,
        -0.75995493E-04, 0.12681320E-03,-0.60791681E-04, 0.52246768E-02,
         0.15873524E-02, 0.40908614E-02,-0.63225016E-03, 0.30180840E-02,
         0.15746413E-02, 0.60752581E-03, 0.11597361E-02, 0.10994494E-03,
        -0.18459799E-03, 0.42724825E-03, 0.15854533E-02, 0.38077388E-03,
        -0.37368233E-03,-0.11812170E-02,-0.18155215E-03,-0.13064350E-02,
        -0.65655983E-03,-0.36546215E-03,-0.66509709E-03, 0.87248729E-04,
         0.11509937E-02, 0.93521201E-03,-0.27791483E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_1000                                  =v_p_e_fp_1000                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_1000                                  =v_p_e_fp_1000                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_fp_1000                                  =v_p_e_fp_1000                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_1000                                  ;
}
float l_e_fp_1000                                  (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1918966E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.19093383E-01,-0.31222340E+00,-0.25475573E-01, 0.12478156E-01,
        -0.33974830E-01,-0.13213449E-01,-0.77978335E-02,-0.41222959E-02,
         0.11042096E-01,-0.50942198E-03,-0.32064104E-02, 0.22851231E-02,
         0.86484896E-02, 0.64251036E-03,-0.28975417E-02,-0.83865755E-03,
         0.36135335E-02,-0.17671031E-02, 0.27199462E-02, 0.20221148E-02,
        -0.70264406E-03,-0.94381685E-03, 0.59933023E-03,-0.11923007E-02,
         0.43550394E-02, 0.16665695E-02,-0.73683169E-03, 0.13270786E-02,
        -0.35484711E-03, 0.62232972E-02,-0.13686426E-02,-0.55194326E-03,
         0.10568834E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_1000                                  =v_l_e_fp_1000                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_1000                                  =v_l_e_fp_1000                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]        *x32    *x51
    ;
    v_l_e_fp_1000                                  =v_l_e_fp_1000                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]*x12            *x52
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]        *x33*x41*x51
        +coeff[ 31]            *x42*x53
        +coeff[ 32]    *x24    *x42    
        ;

    return v_l_e_fp_1000                                  ;
}
float x_e_fp_900                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2829994E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.29478297E-01, 0.47683474E+00,-0.10687833E-01,-0.30637436E-01,
         0.37224498E-01,-0.86598536E-02, 0.56091240E-02,-0.12152893E-01,
        -0.82001258E-02,-0.18425493E-01, 0.19406138E-02,-0.14925185E-02,
         0.14105983E-02,-0.54042675E-02,-0.73253987E-02, 0.93478262E-02,
         0.16827590E-02,-0.57562685E-03, 0.76697022E-03,-0.18760753E-02,
        -0.13968939E-02, 0.23137997E-02, 0.30410003E-04, 0.16742334E-02,
         0.57111573E-02, 0.10673407E-02,-0.17975103E-02,-0.24803325E-02,
         0.17105126E-02,-0.11546721E-02,-0.31093438E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_900                                  =v_x_e_fp_900                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x22        *x53
    ;
    v_x_e_fp_900                                  =v_x_e_fp_900                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_900                                  =v_x_e_fp_900                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_900                                  ;
}
float t_e_fp_900                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5998309E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59824251E-02,-0.27978327E-01, 0.78734346E-01, 0.36647590E-02,
        -0.59112841E-02,-0.21406419E-02,-0.24785900E-02,-0.40263630E-03,
        -0.89090300E-03,-0.11339124E-02, 0.24002555E-03, 0.24524139E-03,
        -0.42156590E-03,-0.23219822E-03,-0.34345040E-03, 0.48458859E-03,
         0.17537369E-02,-0.11079689E-03, 0.48579701E-03, 0.79181169E-04,
         0.21998826E-03, 0.85097115E-03,-0.27263846E-03, 0.37450175E-03,
        -0.77113457E-03,-0.53328957E-03,-0.42141444E-03, 0.21645983E-03,
         0.62810618E-03, 0.10363567E-03, 0.13845757E-03, 0.44973222E-04,
         0.26148694E-03,-0.12812066E-03,-0.11545679E-03,-0.77177159E-04,
        -0.25274933E-03, 0.88440749E-04, 0.42692109E-03, 0.85935317E-04,
        -0.31540359E-03,-0.19322723E-03,-0.38531845E-04, 0.10565989E-03,
         0.63745116E-04, 0.77392404E-04,-0.31278565E-03,-0.80835176E-04,
         0.14520095E-03,-0.73878538E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_900                                  =v_t_e_fp_900                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_900                                  =v_t_e_fp_900                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_900                                  =v_t_e_fp_900                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]*x11*x21*x31*x41    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_fp_900                                  =v_t_e_fp_900                                  
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x21        *x53
        +coeff[ 37]    *x21*x33*x41    
        +coeff[ 38]    *x23    *x42    
        +coeff[ 39]*x11    *x32    *x52
        +coeff[ 40]    *x21*x32    *x52
        +coeff[ 41]        *x32    *x53
        +coeff[ 42]    *x22        *x51
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_fp_900                                  =v_t_e_fp_900                                  
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]        *x31*x41*x52
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        ;

    return v_t_e_fp_900                                  ;
}
float y_e_fp_900                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6880845E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.76804434E-04,-0.22782393E-01,-0.35481520E-01,-0.10293555E-01,
        -0.89261364E-02, 0.18183347E-01,-0.38571684E-02,-0.13266069E-01,
        -0.70034917E-02, 0.41617453E-02, 0.19344436E-02,-0.42701536E-02,
         0.39210636E-03,-0.13474355E-02, 0.10142843E-02, 0.22506572E-02,
        -0.23292596E-02,-0.13198992E-02,-0.10630253E-02,-0.12469119E-02,
        -0.11686500E-02,-0.95993752E-03, 0.14359326E-02,-0.82119420E-03,
        -0.13054828E-02, 0.26348021E-03,-0.65591274E-04, 0.23589247E-03,
         0.43677579E-03,-0.58067391E-04,-0.98022353E-03, 0.42286428E-03,
        -0.30290592E-03,-0.44925694E-03,-0.87972476E-04, 0.12769561E-02,
         0.15430965E-02, 0.92244474E-03, 0.10294939E-02,-0.88453403E-03,
         0.83657989E-03,-0.19609659E-04,-0.34785160E-04, 0.16845706E-03,
        -0.26784912E-04, 0.22541724E-03, 0.28053945E-03,-0.11138832E-02,
         0.46189706E-03, 0.15665137E-02,-0.54907327E-03,-0.89850585E-03,
        -0.23099003E-03,-0.12150807E-04,-0.14843990E-02, 0.33508666E-03,
         0.11254576E-03, 0.11713705E-03,-0.10441890E-03, 0.27489744E-03,
         0.14172796E-02, 0.14678900E-03, 0.90830385E-04, 0.10728072E-02,
        -0.18049177E-03, 0.37057055E-03, 0.28076195E-03,-0.17775514E-02,
        -0.12952894E-02,-0.14904924E-03,-0.51917764E-03,-0.70820219E-03,
         0.90763526E-03,-0.25693711E-03,-0.71182218E-03,-0.20251797E-04,
         0.25571938E-03,-0.14403925E-03,-0.10579304E-03, 0.69891641E-04,
        -0.12692237E-03,-0.97143179E-03, 0.14568033E-03,-0.34143540E-03,
         0.95104879E-04,-0.20024927E-03,-0.82963525E-03, 0.58820081E-03,
        -0.36534853E-03, 0.76214917E-03, 0.25963498E-03,-0.53304923E-03,
        -0.10592781E-02, 0.32418969E-03, 0.10192965E-03,-0.75444754E-03,
         0.81649225E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]            *x41*x53
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]    *x23    *x41*x51
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]    *x21    *x41*x53
        +coeff[ 25]*x11        *x41    
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]*x11        *x41*x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]        *x31*x42*x52
        +coeff[ 37]    *x22    *x41*x52
        +coeff[ 38]    *x22*x31    *x52
        +coeff[ 39]    *x24*x32*x41    
        +coeff[ 40]    *x23*x32*x43    
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 44]    *x21*x31    *x52
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]    *x21*x33    *x51
        +coeff[ 47]    *x23*x31*x42    
        +coeff[ 48]        *x33    *x52
        +coeff[ 49]    *x21*x31    *x53
        +coeff[ 50]        *x33    *x53
        +coeff[ 51]    *x23*x32*x41*x51
        +coeff[ 52]    *x21*x33    *x53
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 53]                *x51
        +coeff[ 54]    *x21*x31    *x51
        +coeff[ 55]    *x21    *x43    
        +coeff[ 56]    *x21*x32*x41    
        +coeff[ 57]*x11*x21    *x41*x51
        +coeff[ 58]    *x21    *x41*x52
        +coeff[ 59]    *x21    *x43*x51
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]    *x24*x31        
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 62]*x11    *x31    *x52
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]*x11*x22    *x41*x51
        +coeff[ 65]    *x23*x32*x41    
        +coeff[ 66]*x11*x22*x31*x42    
        +coeff[ 67]    *x22*x31*x42*x51
        +coeff[ 68]    *x22*x32*x41*x51
        +coeff[ 69]*x11    *x31    *x53
        +coeff[ 70]        *x32*x43*x52
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 71]    *x22    *x45*x51
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x11*x21*x33    *x52
        +coeff[ 74]    *x23*x31    *x53
        +coeff[ 75]*x12        *x41    
        +coeff[ 76]    *x22*x31*x42    
        +coeff[ 77]*x11*x21*x31*x42    
        +coeff[ 78]*x11*x23    *x41    
        +coeff[ 79]*x11    *x32*x41*x51
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 80]*x11*x22*x31    *x51
        +coeff[ 81]        *x32*x43*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x24    *x41*x51
        +coeff[ 84]*x11        *x41*x53
        +coeff[ 85]    *x24*x31    *x51
        +coeff[ 86]    *x22*x32*x43    
        +coeff[ 87]    *x23    *x41*x52
        +coeff[ 88]        *x31*x42*x53
    ;
    v_y_e_fp_900                                  =v_y_e_fp_900                                  
        +coeff[ 89]    *x23    *x43*x51
        +coeff[ 90]    *x22    *x41*x53
        +coeff[ 91]        *x31*x44*x52
        +coeff[ 92]    *x23*x31*x42*x51
        +coeff[ 93]    *x24*x33        
        +coeff[ 94]    *x22    *x43*x52
        +coeff[ 95]    *x22*x31*x42*x52
        +coeff[ 96]        *x32*x45*x51
        ;

    return v_y_e_fp_900                                  ;
}
float p_e_fp_900                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1622911E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.66385460E-05, 0.17063854E-01,-0.34732044E-01, 0.17042501E-01,
        -0.82387179E-02,-0.10535044E-01, 0.78649567E-02,-0.62093732E-03,
        -0.93970018E-04, 0.13248892E-03, 0.32024658E-04, 0.52500535E-02,
         0.15870503E-02, 0.41017001E-02,-0.62690635E-03, 0.30202558E-02,
         0.15707108E-02, 0.11660772E-02, 0.37150108E-04,-0.18345195E-03,
         0.43100698E-03, 0.16345659E-02, 0.60541660E-03, 0.37838219E-03,
        -0.12179607E-02,-0.37495940E-03,-0.14909349E-02,-0.76512003E-03,
        -0.36438223E-03, 0.11076858E-04, 0.10279175E-03,-0.19767931E-03,
        -0.72945113E-03, 0.91730355E-04, 0.11367105E-02, 0.96698297E-03,
        -0.26368239E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_900                                  =v_p_e_fp_900                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_900                                  =v_p_e_fp_900                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_900                                  =v_p_e_fp_900                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_fp_900                                  =v_p_e_fp_900                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        ;

    return v_p_e_fp_900                                  ;
}
float l_e_fp_900                                  (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2039652E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.20502629E-01,-0.31200573E+00,-0.25270145E-01, 0.12347382E-01,
        -0.34312196E-01,-0.13343779E-01,-0.76493998E-02,-0.43536765E-02,
         0.10488122E-01,-0.10958776E-03,-0.31065540E-02, 0.23193643E-02,
         0.58034812E-02, 0.18150551E-02,-0.32814369E-02,-0.87235600E-03,
         0.30655207E-02,-0.18032910E-02, 0.19261013E-02,-0.13922650E-02,
         0.20124253E-02, 0.14295510E-02,-0.87049813E-03, 0.75945223E-03,
         0.13876576E-02,-0.21915780E-03,-0.82058861E-03, 0.53702120E-03,
         0.10013180E-02,-0.52618241E-03,-0.93840691E-03,-0.10229356E-02,
         0.88919997E-02, 0.64277817E-02, 0.63900114E-02, 0.38093538E-03,
        -0.51820051E-03, 0.14461171E-02, 0.38221787E-03, 0.57713799E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_900                                  =v_l_e_fp_900                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_900                                  =v_l_e_fp_900                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]        *x31    *x51
    ;
    v_l_e_fp_900                                  =v_l_e_fp_900                                  
        +coeff[ 26]    *x23            
        +coeff[ 27]*x11    *x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]        *x32*x42    
        +coeff[ 31]    *x23        *x51
        +coeff[ 32]    *x23*x31*x41    
        +coeff[ 33]    *x23    *x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_l_e_fp_900                                  =v_l_e_fp_900                                  
        +coeff[ 35]*x11    *x34        
        +coeff[ 36]*x11*x23    *x41    
        +coeff[ 37]*x12    *x31*x43    
        +coeff[ 38]    *x23*x32*x42    
        +coeff[ 39]    *x21*x32*x44    
        ;

    return v_l_e_fp_900                                  ;
}
float x_e_fp_800                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3018775E-01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.31466633E-01, 0.47676852E+00,-0.10385332E-01,-0.30569414E-01,
         0.37305351E-01,-0.86575272E-02, 0.55653541E-02,-0.12072583E-01,
        -0.82164444E-02,-0.18562142E-01, 0.18935170E-02, 0.12068971E-02,
        -0.54259337E-02,-0.74571450E-02, 0.93072047E-02, 0.19987100E-02,
        -0.59705490E-03, 0.77388267E-03,-0.17227047E-02,-0.14451942E-02,
        -0.14294820E-02, 0.23218994E-02, 0.14981885E-03, 0.16917323E-02,
         0.52509736E-02,-0.19496010E-02,-0.25580628E-02, 0.18018389E-02,
         0.11133656E-02,-0.78506983E-03,-0.33294724E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_fp_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_800                                  =v_x_e_fp_800                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x22        *x53
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_fp_800                                  =v_x_e_fp_800                                  
        +coeff[ 17]*x11*x21            
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_800                                  =v_x_e_fp_800                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]        *x32    *x54
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_800                                  ;
}
float t_e_fp_800                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6407314E-02;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64038164E-02,-0.27943796E-01, 0.78717649E-01, 0.36829896E-02,
        -0.59096864E-02,-0.41408624E-03,-0.21854267E-02,-0.24430053E-02,
        -0.89524238E-03,-0.11268444E-02, 0.25311185E-03, 0.22683461E-03,
        -0.41792044E-03,-0.21406256E-03,-0.34394962E-03, 0.49389555E-03,
         0.17137948E-02,-0.10841974E-03, 0.52748551E-03, 0.11452087E-03,
         0.23121356E-03, 0.81076176E-03,-0.25417557E-03, 0.29985010E-03,
        -0.81359019E-03,-0.54432708E-03,-0.42080658E-03, 0.21608389E-03,
         0.66074927E-03, 0.10671667E-03, 0.10351393E-03, 0.28000952E-03,
        -0.12538867E-03,-0.11133635E-03,-0.89313340E-04,-0.26918144E-03,
         0.37377959E-03,-0.45013195E-03,-0.21824190E-03,-0.32864144E-03,
        -0.20837977E-03, 0.10933194E-02, 0.60670183E-03,-0.15570322E-04,
         0.39290400E-04,-0.50644543E-04, 0.23758648E-04, 0.91702554E-04,
         0.49148715E-04, 0.70103801E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_800                                  =v_t_e_fp_800                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_800                                  =v_t_e_fp_800                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_800                                  =v_t_e_fp_800                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_800                                  =v_t_e_fp_800                                  
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]        *x32*x42*x51
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]    *x22*x32*x42    
        +coeff[ 42]    *x22*x31*x43    
        +coeff[ 43]*x12                
    ;
    v_t_e_fp_800                                  =v_t_e_fp_800                                  
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]*x11            *x52
        +coeff[ 47]        *x33*x41    
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]        *x31*x41*x52
        ;

    return v_t_e_fp_800                                  ;
}
float y_e_fp_800                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1798284E-03;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.17432364E-03,-0.22894369E-01,-0.35589531E-01,-0.10364948E-01,
        -0.87894015E-02, 0.18193694E-01,-0.39259121E-02,-0.13201829E-01,
        -0.69539351E-02, 0.42507672E-02, 0.20349310E-02,-0.43979152E-02,
        -0.23977486E-02, 0.40337045E-03,-0.12080050E-02, 0.10218904E-02,
         0.22461861E-02,-0.13460371E-02,-0.10327428E-02,-0.10692992E-02,
        -0.12287257E-02,-0.92590996E-03, 0.12641470E-02,-0.11633353E-02,
         0.26548671E-03,-0.16550875E-04, 0.24449869E-03, 0.28704578E-03,
        -0.13548456E-03,-0.10558252E-02, 0.23289885E-03,-0.65053703E-03,
        -0.34790639E-04, 0.12696142E-02, 0.12424981E-02, 0.75845618E-03,
         0.67947054E-03,-0.12958520E-02, 0.56566152E-03,-0.12117538E-02,
         0.42518100E-03, 0.82719263E-04, 0.11473902E-03,-0.25224680E-03,
        -0.33120727E-03, 0.41231422E-04, 0.26463720E-03, 0.17142073E-03,
        -0.55932342E-04,-0.87185059E-03, 0.59576228E-03,-0.93093986E-03,
         0.42743032E-03, 0.12863391E-02,-0.62198279E-03, 0.56723633E-03,
        -0.45282763E-04, 0.12114132E-03, 0.26607499E-03, 0.83561067E-03,
         0.63018801E-04,-0.24503094E-03, 0.62215218E-03,-0.17989243E-02,
         0.50761283E-03,-0.11135953E-02, 0.40640519E-03, 0.67178364E-03,
         0.55004086E-03,-0.48455968E-03, 0.10096807E-02,-0.24400202E-03,
        -0.22034391E-03,-0.25840453E-04, 0.64702966E-04, 0.33462522E-04,
        -0.23824972E-03,-0.10803201E-03, 0.14878322E-04,-0.21978522E-03,
        -0.72639006E-04,-0.22067975E-03,-0.34027261E-03, 0.10063293E-03,
        -0.10444764E-03,-0.19620438E-03, 0.44188550E-03,-0.41298522E-03,
         0.83309202E-03,-0.49659808E-03, 0.22204690E-03, 0.28822140E-03,
        -0.46850870E-04,-0.38121748E-03,-0.19578905E-03,-0.28186683E-04,
        -0.15543362E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]            *x41*x53
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]    *x23    *x41*x51
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]*x11        *x41    
        +coeff[ 25]        *x33        
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]*x11    *x31    *x51
        +coeff[ 29]            *x43*x51
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]        *x31*x42*x52
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 35]    *x22    *x41*x52
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x21    *x41*x53
        +coeff[ 38]    *x24*x33        
        +coeff[ 39]    *x21*x31    *x51
        +coeff[ 40]    *x21    *x43    
        +coeff[ 41]    *x21*x32*x41    
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]    *x23*x31        
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]    *x22*x31*x42    
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x21*x31    *x52
        +coeff[ 49]    *x22*x32*x41    
        +coeff[ 50]    *x23*x31    *x51
        +coeff[ 51]    *x23*x31*x42    
        +coeff[ 52]        *x33    *x52
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 53]    *x21*x31    *x53
        +coeff[ 54]        *x33    *x53
        +coeff[ 55]    *x21*x31*x42*x53
        +coeff[ 56]*x11*x21*x31        
        +coeff[ 57]*x11*x21    *x41*x51
        +coeff[ 58]    *x21    *x43*x51
        +coeff[ 59]    *x21*x31*x42*x51
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]*x11*x22    *x41*x51
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x22*x31*x42*x51
        +coeff[ 64]    *x23*x33        
        +coeff[ 65]    *x22*x32*x41*x51
        +coeff[ 66]    *x22    *x41*x53
        +coeff[ 67]    *x22    *x43*x52
        +coeff[ 68]    *x23*x33    *x51
        +coeff[ 69]    *x22    *x45*x51
        +coeff[ 70]    *x23    *x41*x53
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 71]*x11    *x31*x42*x53
        +coeff[ 72]*x11*x21*x33    *x52
        +coeff[ 73]*x12        *x41    
        +coeff[ 74]            *x45    
        +coeff[ 75]*x11*x22*x31        
        +coeff[ 76]            *x43*x52
        +coeff[ 77]*x11*x23    *x41    
        +coeff[ 78]        *x34    *x51
        +coeff[ 79]        *x32*x41*x52
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 80]*x11*x22*x31    *x51
        +coeff[ 81]    *x21    *x43*x52
        +coeff[ 82]    *x24    *x41*x51
        +coeff[ 83]*x11        *x41*x53
        +coeff[ 84]    *x24*x31    *x51
        +coeff[ 85]    *x24    *x43    
        +coeff[ 86]    *x23    *x41*x52
        +coeff[ 87]        *x31*x42*x53
        +coeff[ 88]    *x23    *x43*x51
    ;
    v_y_e_fp_800                                  =v_y_e_fp_800                                  
        +coeff[ 89]    *x22*x31*x42*x52
        +coeff[ 90]*x11*x22*x32*x41*x51
        +coeff[ 91]    *x24*x31    *x52
        +coeff[ 92]    *x21*x34    *x52
        +coeff[ 93]    *x23*x31    *x53
        +coeff[ 94]    *x21*x33    *x53
        +coeff[ 95]        *x31*x44    
        +coeff[ 96]        *x32*x43    
        ;

    return v_y_e_fp_800                                  ;
}
float p_e_fp_800                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2257062E-03;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.22454836E-03, 0.17174931E-01,-0.34573000E-01, 0.16908513E-01,
        -0.82353968E-02,-0.10545047E-01, 0.78603439E-02,-0.55858231E-03,
        -0.93823212E-04, 0.16649615E-03, 0.29729019E-05, 0.52039898E-02,
         0.15826433E-02, 0.40571275E-02,-0.63066697E-03, 0.30302212E-02,
         0.15792025E-02, 0.11696976E-02, 0.90546644E-04,-0.18663004E-03,
         0.43091652E-03, 0.16039328E-02, 0.59754960E-03, 0.38317920E-03,
        -0.37512256E-03,-0.11557903E-02,-0.18893302E-03,-0.13077086E-02,
        -0.66241203E-03,-0.38919086E-03,-0.63885114E-03, 0.87942084E-04,
         0.11625014E-02, 0.94351982E-03,-0.29391760E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_800                                  =v_p_e_fp_800                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_800                                  =v_p_e_fp_800                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_fp_800                                  =v_p_e_fp_800                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_800                                  ;
}
float l_e_fp_800                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2149043E-01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.21437788E-01,-0.31234822E+00,-0.25501406E-01, 0.12389513E-01,
        -0.34917939E-01,-0.13324429E-01,-0.74570235E-02,-0.41049696E-02,
         0.11005381E-01, 0.83574734E-04,-0.29978380E-02, 0.22781717E-02,
         0.87958938E-02, 0.59320667E-03,-0.34277623E-02,-0.76116028E-03,
        -0.17415009E-02, 0.22960114E-02, 0.18722621E-02, 0.91807317E-03,
         0.32825773E-02, 0.11395509E-02,-0.40446495E-03, 0.81510062E-03,
         0.10096523E-02,-0.97381178E-03,-0.67177200E-03, 0.70364290E-03,
         0.45278938E-02, 0.38296189E-02,-0.12962880E-02,-0.11074835E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_800                                  =v_l_e_fp_800                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_fp_800                                  =v_l_e_fp_800                                  
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]    *x22*x32    *x51
        +coeff[ 20]    *x21*x32        
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_fp_800                                  =v_l_e_fp_800                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]    *x23*x32    *x51
        ;

    return v_l_e_fp_800                                  ;
}
float x_e_fp_700                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2963690E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.30458564E-01, 0.47678444E+00,-0.10349974E-01,-0.30603128E-01,
         0.37207402E-01,-0.86563304E-02, 0.55472492E-02,-0.12108586E-01,
        -0.82147233E-02,-0.18353783E-01, 0.18564934E-02,-0.14588030E-02,
         0.13960183E-02,-0.53811371E-02,-0.73235333E-02, 0.92660580E-02,
         0.18991606E-02,-0.54598454E-03, 0.79206144E-03,-0.18482989E-02,
        -0.15132456E-02, 0.22577052E-02, 0.16144698E-03, 0.17352629E-02,
         0.51830993E-02, 0.11344954E-02,-0.17513912E-02,-0.23696274E-02,
         0.17731283E-02,-0.94869942E-03,-0.31785229E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_700                                  =v_x_e_fp_700                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x22        *x53
    ;
    v_x_e_fp_700                                  =v_x_e_fp_700                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_700                                  =v_x_e_fp_700                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_700                                  ;
}
float t_e_fp_700                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6211271E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.61334148E-02,-0.27977133E-01, 0.78711972E-01, 0.35182298E-02,
        -0.59338431E-02,-0.40704713E-03,-0.22151447E-02,-0.24137944E-02,
        -0.91236434E-03,-0.11564668E-02, 0.24511214E-03, 0.26004898E-03,
        -0.42641000E-03,-0.22497462E-03,-0.34444063E-03, 0.49535447E-03,
         0.17359335E-02,-0.10692111E-03, 0.37703585E-03, 0.12971407E-03,
         0.23312970E-03, 0.80579956E-03,-0.26077076E-03, 0.31374491E-03,
        -0.52974385E-03,-0.53128874E-03,-0.44128555E-03, 0.24993901E-03,
        -0.30591461E-03,-0.20222287E-04, 0.88761648E-04, 0.12358972E-03,
         0.22401173E-03,-0.11296842E-03,-0.10620324E-03, 0.13765332E-03,
         0.36892772E-03,-0.36156038E-03,-0.24421033E-03, 0.22735124E-03,
        -0.19277903E-03, 0.24057872E-03, 0.95434039E-03, 0.68729930E-03,
         0.23010238E-03, 0.25040008E-04,-0.61223604E-04, 0.28105322E-04,
         0.10621328E-03, 0.56843975E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_700                                  =v_t_e_fp_700                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_700                                  =v_t_e_fp_700                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_700                                  =v_t_e_fp_700                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x21*x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_fp_700                                  =v_t_e_fp_700                                  
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]        *x32*x42*x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]        *x31*x41*x53
        +coeff[ 42]    *x22*x32*x42    
        +coeff[ 43]    *x22*x31*x43    
    ;
    v_t_e_fp_700                                  =v_t_e_fp_700                                  
        +coeff[ 44]    *x23        *x53
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]    *x22        *x51
        +coeff[ 47]*x11            *x52
        +coeff[ 48]        *x33*x41    
        +coeff[ 49]*x11*x22        *x51
        ;

    return v_t_e_fp_700                                  ;
}
float y_e_fp_700                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1041245E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.93432638E-04,-0.23008605E-01,-0.35657845E-01,-0.10480305E-01,
        -0.89701852E-02, 0.18119816E-01,-0.39528529E-02,-0.13196528E-01,
        -0.69178403E-02, 0.41837487E-02, 0.19512627E-02,-0.44716117E-02,
        -0.19109890E-04, 0.39898904E-03,-0.11995875E-02, 0.10295111E-02,
         0.24277358E-02,-0.13166972E-02,-0.10499309E-02,-0.10813373E-02,
        -0.23739198E-02,-0.12074445E-02,-0.99642505E-03, 0.12292070E-02,
        -0.12327232E-02,-0.12899508E-02, 0.26949547E-03,-0.55374378E-04,
         0.54966978E-03, 0.23352685E-03,-0.77428645E-04,-0.10652666E-02,
         0.50377089E-03, 0.11703161E-02, 0.13296916E-02,-0.10611267E-02,
         0.12154964E-03,-0.20261770E-03,-0.48143298E-04,-0.51838631E-03,
        -0.21191567E-04,-0.12024046E-02, 0.11915986E-03,-0.32645621E-03,
        -0.63329341E-03, 0.13662416E-03, 0.25644180E-03, 0.64937328E-03,
         0.70770335E-03, 0.10683155E-02, 0.10167065E-02, 0.49059563E-05,
         0.38440735E-03, 0.26484075E-03,-0.42125021E-03, 0.11948199E-03,
         0.41772888E-04, 0.54024662E-04, 0.19134005E-03,-0.34423528E-03,
        -0.14118348E-03, 0.10176443E-02, 0.54958655E-03,-0.12456173E-02,
        -0.16812925E-03,-0.12587556E-02,-0.12512288E-03, 0.45003620E-03,
        -0.14734098E-03, 0.58403582E-03, 0.10537434E-02,-0.93007606E-03,
         0.10499851E-02, 0.51720177E-04, 0.27008384E-04,-0.56666730E-04,
        -0.21552152E-04,-0.51285833E-03,-0.12961849E-03,-0.18832198E-03,
         0.79859899E-04,-0.89583649E-04,-0.20892285E-03, 0.37709056E-03,
         0.76481920E-04, 0.25064233E-03,-0.14693363E-03,-0.92403544E-03,
        -0.33982049E-03, 0.81916997E-03, 0.36823144E-03,-0.31070699E-03,
        -0.60970918E-03,-0.10846536E-03, 0.75990794E-03,-0.21678711E-03,
        -0.88146894E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]    *x22*x31    *x53
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]    *x21    *x41*x53
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 26]*x11        *x41    
        +coeff[ 27]        *x33        
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]*x11        *x41*x51
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]            *x43*x51
        +coeff[ 32]    *x21*x33        
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]        *x31*x42*x52
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 35]    *x23*x31*x42    
        +coeff[ 36]    *x21*x32*x41*x52
        +coeff[ 37]    *x24*x32*x41    
        +coeff[ 38]        *x32*x41*x53
        +coeff[ 39]        *x33    *x53
        +coeff[ 40]    *x22*x33    *x52
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 44]        *x32*x41*x51
        +coeff[ 45]*x11*x21*x31    *x51
        +coeff[ 46]    *x21    *x43*x51
        +coeff[ 47]    *x21*x31*x42*x51
        +coeff[ 48]    *x23*x31    *x51
        +coeff[ 49]    *x22    *x41*x52
        +coeff[ 50]    *x21*x31    *x53
        +coeff[ 51]*x11*x21*x31        
        +coeff[ 52]    *x21*x31*x42    
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 53]    *x21*x32*x41    
        +coeff[ 54]        *x32*x43    
        +coeff[ 55]*x11*x21    *x41*x51
        +coeff[ 56]    *x24*x31        
        +coeff[ 57]*x11    *x31    *x52
        +coeff[ 58]    *x21*x33    *x51
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]        *x32*x41*x52
        +coeff[ 61]    *x22*x31    *x52
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 62]        *x33    *x52
        +coeff[ 63]    *x22*x31*x42*x51
        +coeff[ 64]*x11*x21*x31    *x52
        +coeff[ 65]    *x22*x32*x41*x51
        +coeff[ 66]*x11    *x31    *x53
        +coeff[ 67]    *x23    *x41*x52
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]    *x24*x33        
        +coeff[ 70]    *x23*x32*x43    
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 71]    *x24    *x43*x51
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x11    *x31*x42    
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]        *x33    *x51
        +coeff[ 76]*x11    *x32    *x51
        +coeff[ 77]    *x22*x32*x41    
        +coeff[ 78]*x11*x23    *x41    
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 80]*x11    *x32*x41*x51
        +coeff[ 81]*x11*x22*x31    *x51
        +coeff[ 82]        *x33*x42*x51
        +coeff[ 83]        *x32*x45    
        +coeff[ 84]*x11        *x41*x53
        +coeff[ 85]*x11*x21*x31*x42*x51
        +coeff[ 86]    *x24*x31    *x51
        +coeff[ 87]    *x22*x32*x43    
        +coeff[ 88]        *x31*x42*x53
    ;
    v_y_e_fp_700                                  =v_y_e_fp_700                                  
        +coeff[ 89]    *x23    *x43*x51
        +coeff[ 90]    *x22    *x41*x53
        +coeff[ 91]*x11*x22*x31*x42*x51
        +coeff[ 92]    *x22*x31*x42*x52
        +coeff[ 93]*x11*x23*x33        
        +coeff[ 94]    *x21*x31*x42*x53
        +coeff[ 95]    *x21    *x45*x52
        +coeff[ 96]    *x24*x31*x42*x51
        ;

    return v_y_e_fp_700                                  ;
}
float p_e_fp_700                                  (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.5597818E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.61268365E-04, 0.17295297E-01,-0.34347951E-01, 0.17021617E-01,
        -0.82613844E-02,-0.10573104E-01, 0.78868773E-02,-0.63117134E-03,
        -0.10186939E-03, 0.14707756E-03, 0.37643167E-04, 0.52365353E-02,
         0.16018342E-02, 0.40847478E-02,-0.62952284E-03, 0.30277874E-02,
         0.15720846E-02, 0.11692591E-02, 0.39845505E-04,-0.18404555E-03,
         0.44044806E-03, 0.16379294E-02, 0.57345169E-03, 0.46010662E-03,
        -0.37226471E-03,-0.12179947E-02,-0.18539053E-03,-0.15057739E-02,
        -0.74982928E-03,-0.37626681E-03,-0.69362577E-03, 0.92462920E-04,
         0.11913114E-02, 0.93692931E-03,-0.28544039E-03,-0.38403017E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_700                                  =v_p_e_fp_700                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_700                                  =v_p_e_fp_700                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_fp_700                                  =v_p_e_fp_700                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_fp_700                                  =v_p_e_fp_700                                  
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_700                                  ;
}
float l_e_fp_700                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.2020274E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.19991575E-01,-0.31195042E+00,-0.25361447E-01, 0.12444250E-01,
        -0.34036644E-01,-0.12809715E-01,-0.73288609E-02,-0.43882821E-02,
         0.55404943E-02, 0.89481990E-04,-0.28741092E-02, 0.21457328E-02,
         0.43639643E-02, 0.32858963E-02,-0.35192857E-02,-0.75326243E-03,
         0.11790100E-02,-0.16644812E-02, 0.23754439E-02,-0.13858118E-02,
         0.19564307E-02, 0.10542222E-02, 0.16247077E-02,-0.12027816E-02,
        -0.54883509E-03, 0.53400168E-03,-0.31477551E-03,-0.10420760E-02,
        -0.62536862E-03, 0.59293872E-02, 0.75491159E-02, 0.13499875E-01,
         0.13942382E-01, 0.54799770E-02,-0.12999048E-02, 0.76350286E-02,
        -0.76569093E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_fp_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_700                                  =v_l_e_fp_700                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_700                                  =v_l_e_fp_700                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23            
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_fp_700                                  =v_l_e_fp_700                                  
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]                *x54
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x32*x42    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]    *x21    *x44    
        +coeff[ 34]    *x22*x31*x41*x51
    ;
    v_l_e_fp_700                                  =v_l_e_fp_700                                  
        +coeff[ 35]    *x23*x33*x41    
        +coeff[ 36]*x13*x22        *x51
        ;

    return v_l_e_fp_700                                  ;
}
float x_e_fp_600                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3319860E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.32591559E-01, 0.47689098E+00,-0.10283909E-01,-0.30587409E-01,
         0.37332106E-01,-0.86497506E-02, 0.55463701E-02,-0.12092334E-01,
        -0.82022306E-02,-0.18284691E-01, 0.18688053E-02,-0.15175993E-02,
         0.14362682E-02,-0.53717145E-02,-0.73557715E-02, 0.91746422E-02,
         0.18986680E-02,-0.58310875E-03, 0.74390997E-03,-0.18311775E-02,
        -0.15261831E-02, 0.22628270E-02, 0.11016332E-03, 0.16793823E-02,
         0.54670530E-02, 0.11219274E-02,-0.18414106E-02,-0.25441325E-02,
         0.17921755E-02,-0.11949252E-02,-0.32559328E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_600                                  =v_x_e_fp_600                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x22        *x53
    ;
    v_x_e_fp_600                                  =v_x_e_fp_600                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_600                                  =v_x_e_fp_600                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_600                                  ;
}
float t_e_fp_600                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6870187E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.65563368E-02,-0.27965091E-01, 0.78738242E-01, 0.36697015E-02,
        -0.59211417E-02,-0.21188383E-02,-0.24686167E-02,-0.40991328E-03,
        -0.91721892E-03,-0.11234375E-02, 0.26661254E-03, 0.22827377E-03,
        -0.54948038E-03,-0.26499853E-03,-0.35096213E-03, 0.48836449E-03,
         0.16984931E-02,-0.10970886E-03, 0.54473040E-03, 0.90518508E-04,
         0.24347784E-03, 0.83965051E-03,-0.25051847E-03, 0.36824110E-03,
        -0.75635570E-03,-0.54218731E-03,-0.45169229E-03, 0.24685758E-03,
         0.18592285E-03, 0.98294651E-04, 0.90066278E-04, 0.21491657E-03,
        -0.11674078E-03, 0.40116470E-03,-0.27696521E-03,-0.20013363E-03,
         0.61167241E-03,-0.13277154E-04, 0.29267405E-04,-0.18559933E-04,
        -0.41069019E-04, 0.26716247E-04,-0.38669281E-04, 0.55290882E-04,
        -0.75899159E-04, 0.50194161E-04,-0.75985452E-04,-0.24401084E-03,
         0.15162559E-03, 0.93582268E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_600                                  =v_t_e_fp_600                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_600                                  =v_t_e_fp_600                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_600                                  =v_t_e_fp_600                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41*x52
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x23    *x42    
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_fp_600                                  =v_t_e_fp_600                                  
        +coeff[ 35]        *x32    *x53
        +coeff[ 36]    *x23        *x53
        +coeff[ 37]*x12                
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11*x21        *x51
        +coeff[ 40]    *x22        *x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x11*x21*x32        
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_fp_600                                  =v_t_e_fp_600                                  
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]        *x31*x41*x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]    *x23*x31*x41    
        ;

    return v_t_e_fp_600                                  ;
}
float y_e_fp_600                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1705053E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.14132598E-04,-0.23125323E-01,-0.35791025E-01,-0.10380695E-01,
        -0.90145804E-02, 0.18010842E-01,-0.39917915E-02,-0.13197616E-01,
        -0.70964755E-02, 0.42833146E-02, 0.20007936E-02,-0.45041307E-02,
         0.39801685E-03,-0.12854654E-02, 0.99792716E-03, 0.22305502E-02,
        -0.24858401E-02,-0.98809868E-03,-0.14076425E-02,-0.10357921E-02,
        -0.10309197E-02,-0.10061064E-02,-0.11976076E-02, 0.14499280E-02,
        -0.29363105E-03,-0.11433342E-02, 0.27175638E-03,-0.33019449E-04,
         0.30887304E-03, 0.19546304E-03,-0.13703480E-03, 0.44813211E-03,
        -0.32150166E-03,-0.60911698E-03,-0.10851883E-03, 0.11957588E-02,
         0.11197549E-02,-0.12847590E-02,-0.64556854E-03, 0.62909146E-03,
         0.11513080E-03,-0.38804686E-04,-0.14426511E-02, 0.57466561E-04,
        -0.75176169E-04,-0.94719391E-04, 0.13724114E-03,-0.67284144E-03,
         0.83512440E-03, 0.97303477E-03, 0.15695232E-02, 0.68261632E-03,
        -0.56049810E-03, 0.11837184E-03, 0.49219036E-03, 0.12987564E-03,
         0.13327481E-02, 0.57960027E-04, 0.34655325E-03,-0.17680343E-03,
         0.44506040E-03,-0.10224336E-02,-0.14010169E-03,-0.12040546E-02,
        -0.51204086E-03, 0.39806627E-03,-0.16380733E-03,-0.81250287E-03,
         0.10277469E-02,-0.11222685E-02,-0.65684668E-03, 0.97570563E-04,
         0.56807727E-04, 0.37470795E-03,-0.10828850E-03, 0.10151637E-03,
         0.39226477E-03,-0.11600733E-03, 0.10017276E-03, 0.94075374E-04,
        -0.12544711E-03, 0.78240386E-03,-0.20573476E-03, 0.48140419E-03,
         0.13120525E-03, 0.11332699E-03, 0.20819955E-03, 0.60711167E-03,
         0.20450624E-03,-0.30745074E-03, 0.36775650E-03, 0.27406515E-03,
        -0.71492314E-03,-0.41151688E-04,-0.85580250E-04,-0.11587320E-02,
        -0.44738952E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 17]        *x31    *x53
        +coeff[ 18]            *x43    
        +coeff[ 19]        *x31*x42    
        +coeff[ 20]    *x22*x31        
        +coeff[ 21]            *x43*x51
        +coeff[ 22]            *x41*x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]    *x21    *x41*x53
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 26]*x11        *x41    
        +coeff[ 27]        *x33        
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]*x11        *x41*x51
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]        *x31*x42*x52
        +coeff[ 37]    *x23*x31*x42    
        +coeff[ 38]    *x22*x32*x43    
        +coeff[ 39]    *x24*x33        
        +coeff[ 40]    *x22*x33    *x52
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 44]        *x31*x42*x51
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]*x11*x21*x31    *x51
        +coeff[ 47]    *x22*x32*x41    
        +coeff[ 48]    *x23*x31    *x51
        +coeff[ 49]    *x22    *x41*x52
        +coeff[ 50]    *x21*x31    *x53
        +coeff[ 51]    *x23    *x43*x51
        +coeff[ 52]    *x23*x31*x42*x51
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 53]    *x24*x31    *x52
        +coeff[ 54]    *x21*x31*x42    
        +coeff[ 55]*x11*x21    *x41*x51
        +coeff[ 56]    *x21*x31*x42*x51
        +coeff[ 57]*x11    *x31    *x52
        +coeff[ 58]    *x21*x33    *x51
        +coeff[ 59]        *x32*x41*x52
        +coeff[ 60]        *x33    *x52
        +coeff[ 61]    *x22*x31*x42*x51
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 62]*x11*x21*x31    *x52
        +coeff[ 63]    *x22*x32*x41*x51
        +coeff[ 64]        *x33    *x53
        +coeff[ 65]    *x23*x33    *x51
        +coeff[ 66]    *x22    *x45*x51
        +coeff[ 67]    *x24    *x43*x51
        +coeff[ 68]    *x23    *x41*x53
        +coeff[ 69]    *x21*x32*x41*x53
        +coeff[ 70]    *x21*x33    *x53
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 71]            *x45    
        +coeff[ 72]*x11*x22*x31        
        +coeff[ 73]    *x21    *x43*x51
        +coeff[ 74]*x11*x23    *x41    
        +coeff[ 75]*x11*x21*x32*x41    
        +coeff[ 76]    *x21*x32*x43    
        +coeff[ 77]*x11*x22    *x41*x51
        +coeff[ 78]*x11    *x32*x41*x51
        +coeff[ 79]*x11    *x31*x44    
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 80]*x11*x22*x31    *x51
        +coeff[ 81]    *x22*x31    *x52
        +coeff[ 82]        *x32*x43*x51
        +coeff[ 83]    *x23*x32*x41    
        +coeff[ 84]    *x23*x33        
        +coeff[ 85]*x11        *x41*x53
        +coeff[ 86]*x11*x21*x31*x42*x51
        +coeff[ 87]    *x23    *x41*x52
        +coeff[ 88]    *x24*x31*x42    
    ;
    v_y_e_fp_600                                  =v_y_e_fp_600                                  
        +coeff[ 89]        *x31*x42*x53
        +coeff[ 90]    *x22    *x41*x53
        +coeff[ 91]    *x22    *x43*x52
        +coeff[ 92]    *x23*x32*x41*x51
        +coeff[ 93]*x12    *x31    *x53
        +coeff[ 94]*x12        *x43*x52
        +coeff[ 95]    *x24*x31*x42*x51
        +coeff[ 96]    *x23*x31    *x53
        ;

    return v_y_e_fp_600                                  ;
}
float p_e_fp_600                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.4800375E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.50396560E-04, 0.17482275E-01,-0.34025557E-01, 0.16977711E-01,
        -0.83038369E-02,-0.10598719E-01, 0.78703929E-02,-0.63377223E-03,
        -0.10173878E-03, 0.14852091E-03, 0.55840839E-04, 0.52142176E-02,
         0.16011785E-02, 0.40808250E-02,-0.62595319E-03, 0.29967020E-02,
         0.15654757E-02, 0.11768236E-02, 0.31964621E-05,-0.18150869E-03,
         0.43156417E-03, 0.16533853E-02, 0.56616467E-03, 0.45807275E-03,
        -0.11907300E-02,-0.37646852E-03,-0.14604022E-02,-0.75767294E-03,
        -0.35480474E-03, 0.19509793E-04,-0.19968992E-03,-0.64941193E-03,
         0.87790992E-04, 0.12382190E-02, 0.99039264E-03,-0.28437498E-03,
        -0.41208754E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_600                                  =v_p_e_fp_600                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_600                                  =v_p_e_fp_600                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_600                                  =v_p_e_fp_600                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_fp_600                                  =v_p_e_fp_600                                  
        +coeff[ 35]            *x41*x53
        +coeff[ 36]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_600                                  ;
}
float l_e_fp_600                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2096620E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.20925347E-01,-0.31309474E+00,-0.25412871E-01, 0.12420865E-01,
        -0.34230765E-01,-0.13416452E-01,-0.75844522E-02,-0.41753883E-02,
         0.72252667E-02,-0.34197203E-02,-0.31498913E-02, 0.23004930E-02,
         0.47814529E-02, 0.21698892E-02,-0.89318666E-03, 0.18624366E-02,
        -0.12594603E-02, 0.19033566E-02, 0.19575495E-02, 0.17149604E-03,
        -0.25495695E-03,-0.21128745E-02,-0.51170391E-04, 0.10017309E-02,
         0.54866198E-03,-0.93988149E-03, 0.13970596E-02,-0.25222360E-03,
         0.68094060E-02, 0.48882840E-02, 0.10953416E-01, 0.10188003E-01,
         0.41906014E-02,-0.16977780E-02, 0.48395693E-02, 0.37251525E-02,
         0.24977932E-02,-0.14744363E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_600                                  =v_l_e_fp_600                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_fp_600                                  =v_l_e_fp_600                                  
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x34    *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x31        
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_fp_600                                  =v_l_e_fp_600                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]*x11    *x33        
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x32*x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x21    *x44    
        +coeff[ 33]    *x23        *x52
        +coeff[ 34]    *x23*x33*x41    
    ;
    v_l_e_fp_600                                  =v_l_e_fp_600                                  
        +coeff[ 35]    *x21*x33*x41*x52
        +coeff[ 36]    *x23    *x42*x52
        +coeff[ 37]        *x33*x42*x52
        ;

    return v_l_e_fp_600                                  ;
}
float x_e_fp_500                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3255827E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.28863773E-01,-0.86524775E-02, 0.47665453E+00,-0.10070869E-01,
        -0.30629991E-01, 0.37305955E-01, 0.56802393E-02,-0.12074804E-01,
        -0.81144711E-02,-0.18270411E-01, 0.24199926E-02, 0.13386473E-02,
        -0.15777490E-02, 0.11328672E-02,-0.55757500E-02,-0.61176247E-02,
         0.84520727E-02,-0.59298926E-03, 0.73123560E-03,-0.16692904E-02,
        -0.16246644E-02, 0.23507609E-02, 0.29934669E-03, 0.16405694E-02,
         0.10174609E-02,-0.18200824E-02,-0.26905579E-02,-0.26819061E-02,
         0.14878388E-02,-0.30821119E-03, 0.46950351E-02,-0.27531905E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_500                                  =v_x_e_fp_500                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_500                                  =v_x_e_fp_500                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_500                                  =v_x_e_fp_500                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22*x31*x41*x51
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]    *x23*x31*x41    
        ;

    return v_x_e_fp_500                                  ;
}
float t_e_fp_500                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6636442E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.58143269E-02,-0.28019913E-01, 0.78763574E-01, 0.36568872E-02,
        -0.59037679E-02,-0.20899754E-02,-0.23718064E-02,-0.39994504E-03,
        -0.91201108E-03,-0.11334564E-02, 0.25401847E-03, 0.24708675E-03,
        -0.49367081E-03,-0.25847449E-03,-0.34748056E-03, 0.49368065E-03,
         0.16495383E-02,-0.99209668E-04, 0.53413329E-03, 0.83621271E-04,
         0.23735630E-03, 0.85750152E-03,-0.23913002E-03, 0.35789711E-03,
        -0.73129666E-03,-0.55889500E-03,-0.24190896E-03, 0.20655438E-03,
        -0.35756436E-03, 0.71871094E-03, 0.63289706E-04, 0.97317381E-04,
         0.87345114E-04, 0.20611861E-03,-0.11326079E-03,-0.10735011E-03,
        -0.11108643E-03,-0.30013814E-03,-0.38751514E-04, 0.28953652E-03,
         0.73432158E-04,-0.22847540E-03,-0.41360059E-03,-0.11587099E-04,
        -0.21233256E-04,-0.50042738E-04, 0.11475253E-03, 0.92316172E-04,
         0.60893693E-04,-0.17372194E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_500                                  =v_t_e_fp_500                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_500                                  =v_t_e_fp_500                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_500                                  =v_t_e_fp_500                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]    *x21*x31*x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]*x11*x21    *x42    
    ;
    v_t_e_fp_500                                  =v_t_e_fp_500                                  
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]    *x23*x31*x41    
        +coeff[ 39]    *x23    *x42    
        +coeff[ 40]*x11    *x32    *x52
        +coeff[ 41]        *x32    *x53
        +coeff[ 42]    *x23    *x42*x51
        +coeff[ 43]*x12                
    ;
    v_t_e_fp_500                                  =v_t_e_fp_500                                  
        +coeff[ 44]*x11*x21        *x51
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x21*x32*x42    
        ;

    return v_t_e_fp_500                                  ;
}
float y_e_fp_500                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.8878826E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.81170569E-04,-0.23383398E-01,-0.35944071E-01,-0.10433153E-01,
        -0.89316312E-02, 0.17948903E-01,-0.40621818E-02,-0.13142240E-01,
        -0.69280094E-02, 0.43183197E-02, 0.20857002E-02,-0.45298631E-02,
         0.39160580E-03,-0.12688491E-02, 0.94185740E-03, 0.22365935E-02,
        -0.24762144E-02,-0.12870176E-02,-0.10253728E-02,-0.10961923E-02,
        -0.12711579E-02,-0.11063701E-02, 0.12581098E-02,-0.90533163E-03,
        -0.13706695E-02, 0.27208173E-03,-0.41115018E-04, 0.24228205E-03,
         0.16306972E-03,-0.58308400E-04,-0.11021835E-02, 0.30803835E-03,
        -0.29894407E-03,-0.61114761E-03,-0.97020835E-04, 0.12238545E-02,
        -0.81678684E-03, 0.11484964E-02, 0.84059051E-03,-0.55978243E-03,
         0.10024777E-02,-0.41764306E-04,-0.15509939E-03,-0.12620491E-04,
         0.16972268E-03,-0.35845624E-04, 0.22263045E-03, 0.46175654E-03,
         0.58574590E-03,-0.11643650E-02, 0.92729268E-03,-0.10465598E-02,
         0.31261743E-03, 0.15074137E-03,-0.25290140E-03, 0.13063138E-03,
         0.94608194E-03, 0.70024405E-04, 0.52431114E-04, 0.59007638E-03,
         0.42750745E-03, 0.14180232E-03,-0.20849619E-02, 0.97506761E-03,
        -0.11939702E-02,-0.16391696E-03,-0.52644225E-03, 0.28239825E-03,
        -0.75966585E-03, 0.27142279E-03,-0.22262608E-03,-0.80828735E-03,
         0.12745436E-02,-0.25642064E-04, 0.51358325E-04,-0.59632544E-04,
        -0.21746449E-03, 0.40844703E-03, 0.37905742E-04,-0.17522940E-03,
         0.62513129E-04,-0.11147685E-03,-0.40669375E-03, 0.39930365E-03,
        -0.13568859E-03, 0.67989466E-04, 0.21373661E-03, 0.41563553E-03,
         0.85591909E-03, 0.14468684E-03, 0.19911592E-03, 0.28915011E-03,
         0.43175640E-03,-0.96026703E-03,-0.50897896E-03, 0.12156356E-02,
        -0.43316683E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]            *x41*x53
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]    *x23    *x41*x51
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]    *x21    *x41*x53
        +coeff[ 25]*x11        *x41    
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]*x11        *x41*x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22*x32*x41    
        +coeff[ 37]        *x31*x42*x52
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]    *x23*x31*x42    
        +coeff[ 40]    *x22*x31    *x52
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]    *x21    *x41*x52
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 44]*x11*x21*x31    *x51
        +coeff[ 45]    *x21*x31    *x52
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]        *x33    *x52
        +coeff[ 48]    *x23    *x43*x51
        +coeff[ 49]    *x23*x31*x42*x51
        +coeff[ 50]    *x21*x31*x42*x53
        +coeff[ 51]    *x21*x31    *x51
        +coeff[ 52]    *x21    *x43    
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 53]    *x21*x32*x41    
        +coeff[ 54]    *x23*x31        
        +coeff[ 55]*x11*x21    *x41*x51
        +coeff[ 56]    *x21*x31*x42*x51
        +coeff[ 57]    *x24*x31        
        +coeff[ 58]*x11    *x31    *x52
        +coeff[ 59]    *x23*x31    *x51
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]*x11*x22*x31*x42    
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 62]    *x22*x31*x42*x51
        +coeff[ 63]    *x21*x31    *x53
        +coeff[ 64]    *x22*x32*x41*x51
        +coeff[ 65]*x11    *x31    *x53
        +coeff[ 66]        *x33    *x53
        +coeff[ 67]    *x22    *x43*x52
        +coeff[ 68]        *x32*x43*x52
        +coeff[ 69]    *x23*x33    *x51
        +coeff[ 70]    *x22    *x45*x51
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 71]    *x24    *x43*x51
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x12        *x41    
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]    *x22    *x43    
        +coeff[ 76]    *x22*x31*x42    
        +coeff[ 77]    *x21    *x43*x51
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 80]*x11    *x31*x44    
        +coeff[ 81]*x11*x22*x31    *x51
        +coeff[ 82]        *x32*x43*x51
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]*x11*x21*x31    *x52
        +coeff[ 85]*x11        *x41*x53
        +coeff[ 86]            *x43*x53
        +coeff[ 87]    *x23    *x41*x52
        +coeff[ 88]    *x24*x31*x42    
    ;
    v_y_e_fp_500                                  =v_y_e_fp_500                                  
        +coeff[ 89]*x11    *x31*x42*x52
        +coeff[ 90]*x11*x21*x32*x43    
        +coeff[ 91]    *x22    *x41*x53
        +coeff[ 92]    *x24*x33        
        +coeff[ 93]    *x23*x32*x41*x51
        +coeff[ 94]    *x22*x31*x42*x52
        +coeff[ 95]    *x23*x32*x43    
        +coeff[ 96]    *x21*x34*x43    
        ;

    return v_y_e_fp_500                                  ;
}
float p_e_fp_500                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.6381563E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.72965871E-04, 0.17715789E-01,-0.33609036E-01, 0.16970266E-01,
        -0.83251074E-02,-0.10637894E-01, 0.78877937E-02,-0.61067275E-03,
        -0.71613249E-04, 0.12244035E-03, 0.14684287E-04, 0.51630931E-02,
         0.16230905E-02, 0.41204537E-02,-0.62454218E-03, 0.30250633E-02,
         0.15766093E-02, 0.11999414E-02, 0.17570883E-04,-0.18170755E-03,
         0.44438633E-03, 0.16537558E-02, 0.58300083E-03, 0.45503769E-03,
        -0.12179185E-02,-0.37148868E-03,-0.14798375E-02,-0.75373339E-03,
        -0.38338831E-03, 0.15891232E-04, 0.12815197E-03,-0.18813887E-03,
        -0.77370420E-03, 0.85704414E-04, 0.11670586E-02, 0.95405412E-03,
        -0.29391688E-03,-0.36315809E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_500                                  =v_p_e_fp_500                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_500                                  =v_p_e_fp_500                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_500                                  =v_p_e_fp_500                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_fp_500                                  =v_p_e_fp_500                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_500                                  ;
}
float l_e_fp_500                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1962954E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.19622412E-01,-0.31395295E+00,-0.25079548E-01, 0.12291227E-01,
        -0.34953471E-01,-0.13366259E-01,-0.74940817E-02,-0.42860401E-02,
         0.23036394E-02, 0.10997834E-01, 0.46973632E-03,-0.31058348E-02,
         0.76910285E-02, 0.32687420E-03,-0.38426972E-02,-0.83950034E-03,
         0.36774562E-02,-0.28986686E-02, 0.21633259E-02, 0.20809977E-02,
         0.16437939E-02,-0.85858710E-03, 0.68246841E-03,-0.13259369E-03,
         0.30538990E-03,-0.35447907E-03, 0.97835797E-03,-0.40215644E-03,
        -0.90209150E-03,-0.55116054E-03, 0.92791225E-03, 0.57134894E-02,
         0.39840168E-02,-0.10684931E-02, 0.28995178E-02, 0.14735570E-02,
         0.27960869E-02,-0.52519317E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_500                                  =v_l_e_fp_500                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]        *x34        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_500                                  =v_l_e_fp_500                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x42    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x12        *x41    
    ;
    v_l_e_fp_500                                  =v_l_e_fp_500                                  
        +coeff[ 26]    *x24            
        +coeff[ 27]    *x23    *x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_l_e_fp_500                                  =v_l_e_fp_500                                  
        +coeff[ 35]    *x24        *x51
        +coeff[ 36]    *x22    *x44    
        +coeff[ 37]            *x43*x53
        ;

    return v_l_e_fp_500                                  ;
}
float x_e_fp_450                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3706805E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.30779378E-01, 0.47654253E+00,-0.10289260E-01,-0.30629849E-01,
         0.37368059E-01,-0.86242575E-02, 0.54735709E-02,-0.12053016E-01,
        -0.82362480E-02,-0.18114781E-01, 0.24237793E-02, 0.13685100E-02,
        -0.15528603E-02, 0.13666762E-02,-0.55359993E-02,-0.72740959E-02,
         0.87634483E-02,-0.57144294E-03, 0.77771372E-03,-0.17542747E-02,
        -0.15351035E-02, 0.22589874E-02, 0.28677200E-03, 0.17918143E-02,
         0.10921867E-02,-0.18339792E-02,-0.27262848E-02, 0.18326351E-02,
        -0.32248031E-03, 0.47901110E-02,-0.99327345E-03,-0.30153799E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_450                                  =v_x_e_fp_450                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_450                                  =v_x_e_fp_450                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_450                                  =v_x_e_fp_450                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22*x31*x41*x51
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_450                                  ;
}
float t_e_fp_450                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7523087E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62612845E-02,-0.28084816E-01, 0.78749128E-01, 0.36316619E-02,
        -0.59128371E-02,-0.21275596E-02,-0.23247546E-02,-0.40844674E-03,
        -0.94962737E-03,-0.11347583E-02, 0.23642388E-03, 0.28017478E-03,
        -0.46397175E-03,-0.26857341E-03,-0.34285634E-03, 0.49162866E-03,
         0.16814991E-02,-0.10869961E-03, 0.53474325E-03, 0.84090105E-04,
         0.24574337E-03, 0.86285366E-03,-0.23651317E-03, 0.39594551E-03,
        -0.65501244E-03,-0.50072721E-03,-0.21730622E-03, 0.23994069E-03,
         0.23198892E-03,-0.56838134E-03, 0.93586175E-04, 0.12722606E-03,
         0.22049318E-03,-0.78931509E-04,-0.97849428E-04, 0.79390338E-04,
        -0.28428872E-03,-0.21411997E-03, 0.65123843E-03, 0.36614139E-04,
        -0.25777354E-04,-0.41743908E-04, 0.29223178E-04, 0.66377499E-04,
         0.51872903E-04,-0.92019560E-04,-0.26371668E-03,-0.87766139E-05,
         0.22616576E-03,-0.26018670E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_450                                  =v_t_e_fp_450                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_450                                  =v_t_e_fp_450                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_fp_450                                  =v_t_e_fp_450                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41*x52
        +coeff[ 29]    *x23    *x42*x51
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_fp_450                                  =v_t_e_fp_450                                  
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]    *x22        *x51
        +coeff[ 42]*x11            *x52
        +coeff[ 43]*x11*x21*x31*x41    
    ;
    v_t_e_fp_450                                  =v_t_e_fp_450                                  
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]            *x42*x52
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x23    *x42    
        +coeff[ 49]    *x21*x32*x42    
        ;

    return v_t_e_fp_450                                  ;
}
float y_e_fp_450                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1014206E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.77648809E-04,-0.23526566E-01,-0.36040753E-01,-0.10496937E-01,
        -0.90932911E-02, 0.17889645E-01,-0.41441615E-02,-0.13236480E-01,
        -0.69293422E-02, 0.43292427E-02, 0.20903395E-02,-0.47097644E-02,
        -0.23914543E-02, 0.39000454E-03,-0.12477094E-02, 0.98590157E-03,
         0.22108441E-02,-0.10859827E-02, 0.27349516E-03,-0.13161820E-02,
        -0.11056064E-02,-0.11002226E-02, 0.22711681E-03,-0.12230062E-02,
         0.11780505E-02,-0.11171086E-02,-0.12729433E-02,-0.51907027E-04,
         0.34001085E-03,-0.96904609E-04,-0.12173825E-02, 0.46247104E-03,
        -0.30990862E-03,-0.71348378E-03,-0.93715047E-04, 0.12841801E-02,
        -0.64105215E-03, 0.21771186E-03,-0.11415237E-02,-0.40235111E-04,
         0.32313738E-03, 0.17075131E-03,-0.39853068E-04, 0.20313283E-03,
         0.11201828E-02, 0.10709270E-02, 0.92910416E-03, 0.53597975E-03,
         0.12030405E-02, 0.56986581E-03, 0.11152107E-02,-0.44757235E-03,
        -0.14513920E-04,-0.11740355E-02, 0.40438541E-03,-0.19460006E-03,
         0.72182404E-04,-0.26373155E-03, 0.11250158E-03, 0.27457112E-03,
         0.22581268E-03, 0.15527055E-03, 0.10591695E-03, 0.99110592E-03,
        -0.43738542E-04,-0.18176431E-03,-0.16032190E-02,-0.11048134E-02,
         0.86387838E-04,-0.13736702E-03, 0.10043583E-02, 0.38085986E-03,
         0.77457802E-03,-0.57615765E-03,-0.61566464E-03, 0.11669931E-02,
         0.10540389E-04, 0.15525522E-04, 0.92779383E-05,-0.24488378E-04,
         0.62871724E-04, 0.29053894E-03,-0.11582872E-03, 0.14209762E-03,
         0.24488376E-03,-0.36263838E-04, 0.54960005E-03,-0.21103778E-03,
         0.91080816E-04,-0.12298136E-03,-0.18014554E-03, 0.94296258E-04,
        -0.18237111E-03,-0.28525208E-03,-0.26694048E-03, 0.38793648E-03,
        -0.20932996E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 17]        *x31    *x53
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]    *x22*x31        
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]            *x41*x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]    *x21*x32*x41*x51
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 26]    *x21    *x41*x53
        +coeff[ 27]        *x33        
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22*x32*x41    
        +coeff[ 37]        *x31*x44*x51
        +coeff[ 38]    *x23*x31*x42    
        +coeff[ 39]*x11*x21*x31        
        +coeff[ 40]    *x21*x32*x41    
        +coeff[ 41]*x11*x21*x31    *x51
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 44]        *x31*x42*x52
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]    *x22*x31    *x52
        +coeff[ 47]        *x33    *x52
        +coeff[ 48]    *x21*x31    *x53
        +coeff[ 49]    *x23    *x41*x52
        +coeff[ 50]    *x21*x31*x42*x53
        +coeff[ 51]    *x23*x31    *x53
        +coeff[ 52]                *x51
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 53]    *x21*x31    *x51
        +coeff[ 54]    *x21    *x43    
        +coeff[ 55]        *x31*x42*x51
        +coeff[ 56]            *x45    
        +coeff[ 57]        *x32*x43    
        +coeff[ 58]*x11*x21    *x41*x51
        +coeff[ 59]    *x21    *x43*x51
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]    *x24*x31        
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 62]*x11    *x31    *x52
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]*x11*x22    *x41*x51
        +coeff[ 66]    *x22*x31*x42*x51
        +coeff[ 67]    *x22*x32*x41*x51
        +coeff[ 68]*x11*x24*x31        
        +coeff[ 69]*x11    *x31    *x53
        +coeff[ 70]    *x23    *x43*x51
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 71]    *x22    *x41*x53
        +coeff[ 72]    *x21*x33*x42*x51
        +coeff[ 73]        *x33    *x53
        +coeff[ 74]    *x22    *x45*x51
        +coeff[ 75]    *x23    *x41*x53
        +coeff[ 76]    *x21            
        +coeff[ 77]    *x22            
        +coeff[ 78]    *x21        *x51
        +coeff[ 79]*x12        *x41    
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 80]*x11    *x31*x42    
        +coeff[ 81]    *x22*x31*x42    
        +coeff[ 82]*x11*x23    *x41    
        +coeff[ 83]*x11*x21*x32*x41    
        +coeff[ 84]            *x45*x51
        +coeff[ 85]    *x21*x32    *x52
        +coeff[ 86]    *x23*x32*x41    
        +coeff[ 87]    *x21*x34*x41    
        +coeff[ 88]    *x23*x33        
    ;
    v_y_e_fp_450                                  =v_y_e_fp_450                                  
        +coeff[ 89]*x11*x21*x31    *x52
        +coeff[ 90]    *x21    *x43*x52
        +coeff[ 91]*x11        *x41*x53
        +coeff[ 92]    *x24*x31    *x51
        +coeff[ 93]        *x31*x42*x53
        +coeff[ 94]    *x24*x32*x41    
        +coeff[ 95]    *x24*x33        
        +coeff[ 96]*x11*x22*x31*x42*x51
        ;

    return v_y_e_fp_450                                  ;
}
float p_e_fp_450                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.2609954E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.58285677E-04, 0.17894339E-01,-0.33295579E-01, 0.16934242E-01,
        -0.83421133E-02,-0.10665966E-01, 0.78873299E-02,-0.59682562E-03,
        -0.86906854E-04, 0.11495651E-03, 0.17237428E-04, 0.51488332E-02,
         0.16265349E-02, 0.41159061E-02,-0.62044011E-03, 0.29998696E-02,
         0.15638703E-02, 0.12067193E-02, 0.59942911E-04,-0.18059614E-03,
         0.43898678E-03, 0.16207589E-02, 0.57368417E-03, 0.45906779E-03,
        -0.37698264E-03,-0.11947505E-02,-0.18487142E-03,-0.14567041E-02,
        -0.75439329E-03,-0.40007001E-03,-0.65069774E-03, 0.91647576E-04,
         0.12043609E-02, 0.98213286E-03,-0.28880770E-03,-0.38901065E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_450                                  =v_p_e_fp_450                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_450                                  =v_p_e_fp_450                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_fp_450                                  =v_p_e_fp_450                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_fp_450                                  =v_p_e_fp_450                                  
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_450                                  ;
}
float l_e_fp_450                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2138404E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.21158475E-01,-0.31434673E+00,-0.25168017E-01, 0.12319237E-01,
        -0.34585137E-01,-0.13394092E-01,-0.73644859E-02,-0.43642577E-02,
         0.10699324E-01,-0.14702747E-03,-0.31098588E-02, 0.22405691E-02,
         0.69025466E-02, 0.91140962E-03,-0.30765540E-02,-0.79566054E-03,
         0.34325412E-02,-0.80054102E-03, 0.19182946E-02,-0.10714105E-02,
         0.18949689E-02, 0.15545586E-02, 0.92111109E-03, 0.69332356E-03,
        -0.91695483E-03,-0.60202304E-03, 0.11669403E-02,-0.76335133E-03,
         0.66956780E-02, 0.45571974E-02, 0.52748290E-02,-0.14448910E-02,
        -0.93986216E-03, 0.48107668E-02, 0.21228052E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_450                                  =v_l_e_fp_450                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_450                                  =v_l_e_fp_450                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_fp_450                                  =v_l_e_fp_450                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x31*x41*x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x22*x32    *x51
        +coeff[ 32]    *x22    *x42*x51
        +coeff[ 33]    *x21*x32*x44    
        +coeff[ 34]*x12*x21*x33*x41    
        ;

    return v_l_e_fp_450                                  ;
}
float x_e_fp_449                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3167368E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.30632669E-01, 0.46980461E+00,-0.15387768E-01,-0.30025911E-01,
         0.36588091E-01,-0.82160840E-02, 0.50093294E-02,-0.13276538E-01,
        -0.79004271E-02,-0.18303420E-01, 0.23434930E-02, 0.14843296E-02,
        -0.17099000E-02, 0.14811556E-02,-0.62500532E-02,-0.74578947E-02,
         0.93692848E-02,-0.60144911E-03, 0.81039406E-03,-0.22119959E-02,
        -0.14032247E-02, 0.26357572E-02, 0.22118857E-03, 0.19722583E-02,
         0.13087589E-02,-0.26777994E-02,-0.30346708E-02,-0.25786667E-02,
         0.20918176E-02, 0.47690026E-02, 0.17163148E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_449                                  =v_x_e_fp_449                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_449                                  =v_x_e_fp_449                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_449                                  =v_x_e_fp_449                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23        *x53
        ;

    return v_x_e_fp_449                                  ;
}
float t_e_fp_449                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6665003E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62342710E-02,-0.29491544E-01, 0.77148966E-01, 0.34973370E-02,
        -0.57749376E-02,-0.23237134E-02,-0.24751807E-02,-0.31921928E-03,
        -0.10859019E-02,-0.10536979E-02, 0.25775449E-03, 0.20858586E-03,
        -0.61445142E-03,-0.24468001E-03,-0.33188498E-03, 0.47963660E-03,
         0.17679563E-02,-0.98123826E-04, 0.59355068E-03, 0.11203009E-03,
         0.30943294E-03, 0.86973049E-03,-0.30103198E-03, 0.79528538E-04,
         0.45191601E-03,-0.77283772E-03,-0.63057721E-03,-0.39959105E-03,
         0.27315109E-03,-0.37261113E-03, 0.66546514E-03, 0.96643525E-04,
         0.11594435E-03, 0.59711583E-04, 0.27997332E-03,-0.11935255E-03,
        -0.10293177E-03,-0.84235704E-04,-0.25111545E-03, 0.43099196E-03,
        -0.34451115E-03, 0.79262420E-04,-0.25223216E-03,-0.11805702E-04,
        -0.20349349E-04,-0.44814744E-04, 0.65388791E-04, 0.26035111E-03,
         0.19627609E-03, 0.17030630E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_449                                  =v_t_e_fp_449                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_449                                  =v_t_e_fp_449                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_449                                  =v_t_e_fp_449                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]*x11*x21*x31*x41    
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_fp_449                                  =v_t_e_fp_449                                  
        +coeff[ 35]*x11*x21    *x42    
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]    *x21        *x53
        +coeff[ 39]    *x23    *x42    
        +coeff[ 40]        *x32*x42*x51
        +coeff[ 41]*x11    *x32    *x52
        +coeff[ 42]        *x32    *x53
        +coeff[ 43]*x12                
    ;
    v_t_e_fp_449                                  =v_t_e_fp_449                                  
        +coeff[ 44]*x11*x21        *x51
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]        *x31*x41*x52
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x23*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_fp_449                                  ;
}
float y_e_fp_449                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.7496479E-05;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.10678920E-04,-0.18032892E-01,-0.43141890E-01,-0.13142210E-01,
        -0.11141166E-01, 0.20309620E-01,-0.30642841E-02,-0.15159861E-01,
        -0.72305980E-02, 0.44046701E-02, 0.17788807E-02,-0.46166983E-02,
         0.47214766E-03,-0.18220830E-02,-0.19288114E-02, 0.11144505E-02,
         0.26001839E-02,-0.26547418E-02,-0.98619750E-03, 0.35069528E-03,
        -0.15965885E-02,-0.16062728E-02, 0.23196058E-03,-0.11324269E-02,
         0.11845533E-02,-0.14267474E-02,-0.15475535E-03,-0.12553290E-02,
         0.41282611E-03,-0.69176473E-04,-0.10500061E-02, 0.53071568E-03,
        -0.73418592E-03,-0.59036622E-04, 0.12946901E-02,-0.10295063E-02,
         0.65696781E-03, 0.88523643E-03,-0.13353812E-02, 0.96645433E-03,
        -0.89026103E-03,-0.23312996E-03, 0.12951039E-03,-0.34573980E-03,
        -0.10311683E-03, 0.17945224E-03, 0.14624122E-02, 0.55028714E-03,
         0.64806524E-03, 0.61205676E-03,-0.80280652E-03, 0.38583006E-04,
         0.11364137E-03,-0.54543343E-04, 0.14996006E-03, 0.54919970E-03,
        -0.65576976E-04, 0.10392412E-03, 0.10686916E-02, 0.71185385E-03,
        -0.18539017E-02,-0.13745934E-02, 0.84168751E-04,-0.27885317E-03,
         0.87767845E-03,-0.89870003E-03, 0.12150983E-02,-0.29646842E-04,
         0.74926921E-03,-0.12768392E-04,-0.10197185E-03,-0.10592904E-03,
        -0.66147550E-04,-0.18849580E-03, 0.95047464E-04,-0.66222958E-04,
        -0.14672201E-02,-0.17022019E-04, 0.31314461E-03,-0.14307334E-03,
         0.86180284E-04,-0.35467089E-03,-0.12742079E-03,-0.36720824E-03,
        -0.64401218E-03, 0.57864643E-03, 0.22068358E-03,-0.40534500E-03,
         0.10444660E-02, 0.37844671E-03,-0.38917101E-03, 0.20998520E-03,
        -0.36247820E-03, 0.42039689E-03, 0.96944009E-03,-0.23523009E-03,
        -0.15367643E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]        *x31    *x53
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]            *x43    
        +coeff[ 21]        *x31*x42    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]            *x41*x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]    *x21*x32*x41*x51
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]    *x21*x31    *x51
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]        *x32*x41*x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x24    *x41    
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]    *x21*x32*x43    
        +coeff[ 37]    *x22    *x41*x52
        +coeff[ 38]    *x21    *x41*x53
        +coeff[ 39]    *x21*x31    *x53
        +coeff[ 40]        *x32*x43*x52
        +coeff[ 41]    *x22*x33    *x52
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 44]    *x21    *x41*x52
        +coeff[ 45]*x11*x21*x31    *x51
        +coeff[ 46]        *x31*x42*x52
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]    *x23*x32*x41    
        +coeff[ 49]    *x21*x33*x42*x51
        +coeff[ 50]        *x33    *x53
        +coeff[ 51]*x11*x21*x31        
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 53]    *x21*x31    *x52
        +coeff[ 54]    *x21    *x43*x51
        +coeff[ 55]    *x21*x31*x42*x51
        +coeff[ 56]    *x24*x31        
        +coeff[ 57]*x11    *x31    *x52
        +coeff[ 58]    *x22*x31    *x52
        +coeff[ 59]        *x33    *x52
        +coeff[ 60]    *x22*x31*x42*x51
        +coeff[ 61]    *x22*x32*x41*x51
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 62]*x11*x24*x31        
        +coeff[ 63]    *x22*x34*x41    
        +coeff[ 64]    *x24*x33        
        +coeff[ 65]    *x24    *x43*x51
        +coeff[ 66]    *x23    *x41*x53
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]    *x21*x31*x42    
        +coeff[ 69]        *x32*x43    
        +coeff[ 70]*x11*x23    *x41    
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 71]*x11*x21*x33        
        +coeff[ 72]    *x21*x33    *x51
        +coeff[ 73]*x11*x22    *x41*x51
        +coeff[ 74]*x11    *x32*x41*x51
        +coeff[ 75]        *x32*x41*x52
        +coeff[ 76]    *x23*x31*x42    
        +coeff[ 77]*x11*x22*x31    *x51
        +coeff[ 78]    *x23*x33        
        +coeff[ 79]*x11*x21*x31    *x52
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 80]*x11        *x41*x53
        +coeff[ 81]    *x24*x31    *x51
        +coeff[ 82]*x11    *x31    *x53
        +coeff[ 83]    *x24    *x43    
        +coeff[ 84]    *x22*x32*x43    
        +coeff[ 85]    *x23    *x41*x52
        +coeff[ 86]    *x21*x32*x41*x52
        +coeff[ 87]        *x31*x42*x53
        +coeff[ 88]    *x23    *x43*x51
    ;
    v_y_e_fp_449                                  =v_y_e_fp_449                                  
        +coeff[ 89]    *x22    *x41*x53
        +coeff[ 90]        *x31*x44*x52
        +coeff[ 91]    *x22    *x43*x52
        +coeff[ 92]    *x22*x31*x42*x52
        +coeff[ 93]    *x23*x33    *x51
        +coeff[ 94]    *x21*x31*x42*x53
        +coeff[ 95]*x11*x22*x33    *x51
        +coeff[ 96]*x13    *x31*x42*x51
        ;

    return v_y_e_fp_449                                  ;
}
float p_e_fp_449                                  (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.2583388E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.25677143E-03, 0.17923839E-01,-0.34319840E-01, 0.16441585E-01,
        -0.87911505E-02,-0.99827973E-02, 0.77012409E-02,-0.39002270E-03,
         0.95754811E-04, 0.19168541E-03,-0.15704481E-03, 0.54162429E-02,
         0.17022023E-02, 0.44355360E-02,-0.60031249E-03, 0.32051443E-02,
         0.15041201E-02, 0.11220755E-02, 0.27962822E-04,-0.19005909E-03,
         0.57046325E-03, 0.19010989E-02, 0.62451151E-03,-0.10484122E-02,
         0.29702400E-03,-0.35866693E-03,-0.92014507E-03,-0.40287286E-03,
         0.67489882E-05,-0.20131536E-03,-0.40173222E-03, 0.85552158E-04,
         0.12325525E-02, 0.87618083E-03,-0.24475274E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_449                                  =v_p_e_fp_449                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_449                                  =v_p_e_fp_449                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_449                                  =v_p_e_fp_449                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_449                                  ;
}
float l_e_fp_449                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2204917E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.21458555E-01,-0.31635705E+00,-0.25414160E-01, 0.12219173E-01,
        -0.35337273E-01,-0.12194046E-01,-0.75916881E-02,-0.41688853E-02,
         0.93318755E-02, 0.17970719E-03,-0.33426764E-02, 0.21813533E-02,
         0.75839865E-02, 0.40789368E-02,-0.41385475E-02,-0.79834426E-03,
        -0.15090856E-02, 0.18747910E-02,-0.13902945E-02, 0.16596977E-02,
         0.21028030E-02, 0.17689327E-02,-0.10333151E-02, 0.70841046E-03,
        -0.39894501E-03, 0.61434158E-03,-0.36933375E-03, 0.11449963E-02,
        -0.68185822E-03, 0.57086959E-02, 0.33980135E-02, 0.10013672E-01,
         0.71775042E-02, 0.69689187E-02, 0.38803867E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_fp_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_449                                  =v_l_e_fp_449                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_fp_449                                  =v_l_e_fp_449                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]                *x53
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_fp_449                                  =v_l_e_fp_449                                  
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]                *x54
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x32*x42    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]    *x23*x33*x41    
        +coeff[ 34]    *x23    *x44    
        ;

    return v_l_e_fp_449                                  ;
}
float x_e_fp_400                                  (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.3533399E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.34324832E-01, 0.46943241E+00,-0.15190984E-01,-0.30072724E-01,
         0.36739897E-01,-0.81994096E-02, 0.49950434E-02,-0.13232027E-01,
        -0.78742476E-02,-0.18216308E-01, 0.23589465E-02, 0.14262327E-02,
        -0.17530031E-02, 0.13847015E-02,-0.63911490E-02,-0.75248345E-02,
         0.92922840E-02,-0.58517890E-03, 0.76784502E-03,-0.21853345E-02,
        -0.15833146E-02, 0.26009479E-02, 0.21750695E-03, 0.19559844E-02,
         0.13059009E-02,-0.19145517E-02,-0.30378988E-02,-0.27515618E-02,
         0.20151890E-02, 0.48070527E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_400                                  =v_x_e_fp_400                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_400                                  =v_x_e_fp_400                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_400                                  =v_x_e_fp_400                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        ;

    return v_x_e_fp_400                                  ;
}
float t_e_fp_400                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7546576E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.71182833E-02,-0.29585959E-01, 0.77072226E-01, 0.35173181E-02,
        -0.57651866E-02,-0.23195308E-02,-0.24378051E-02,-0.32949809E-03,
        -0.10955808E-02,-0.10409150E-02, 0.24676172E-03, 0.24299632E-03,
        -0.50984480E-03,-0.26833979E-03,-0.34135219E-03, 0.47119000E-03,
         0.17439431E-02,-0.96938718E-04, 0.54030033E-03, 0.11160965E-03,
         0.28875232E-03, 0.84855425E-03,-0.29549023E-03, 0.11601938E-03,
         0.44081718E-03,-0.74638694E-03,-0.61625807E-03,-0.44685174E-03,
         0.27520698E-03,-0.32589035E-03, 0.10315661E-03, 0.10780084E-03,
         0.24592530E-03,-0.13222263E-03,-0.90791160E-04, 0.43204226E-03,
        -0.22649120E-03, 0.57553116E-03,-0.17059656E-04, 0.44816872E-04,
        -0.47784488E-04, 0.26228081E-04, 0.53130989E-04,-0.66764667E-04,
         0.83472689E-04,-0.24431851E-03,-0.30453666E-03, 0.12803935E-03,
        -0.28937927E-03, 0.20895500E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_400                                  =v_t_e_fp_400                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_400                                  =v_t_e_fp_400                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_400                                  =v_t_e_fp_400                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_400                                  =v_t_e_fp_400                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]        *x32    *x53
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]*x11*x22            
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]    *x22        *x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_fp_400                                  =v_t_e_fp_400                                  
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]        *x33*x41*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_fp_400                                  ;
}
float y_e_fp_400                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1201242E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.11927988E-03,-0.17920839E-01,-0.43398112E-01,-0.13316469E-01,
        -0.11065135E-01, 0.20302825E-01,-0.29773014E-02,-0.15101743E-01,
        -0.73263566E-02, 0.44872765E-02, 0.17329530E-02,-0.43409895E-02,
         0.47028391E-03,-0.17802609E-02,-0.18840181E-02, 0.11218900E-02,
         0.26359367E-02,-0.29298149E-02,-0.11021246E-02, 0.35694393E-03,
        -0.15602831E-02,-0.16174666E-02, 0.21352926E-03,-0.11175590E-02,
         0.13760758E-02,-0.10655168E-02,-0.11850930E-03,-0.13319025E-02,
         0.44985549E-03,-0.78144316E-04,-0.10327317E-02, 0.30828713E-03,
        -0.36444730E-03,-0.72898075E-03,-0.11375501E-03, 0.11256115E-02,
        -0.15061663E-02, 0.10550396E-02,-0.94752194E-03,-0.13038527E-02,
         0.10407533E-02,-0.11845728E-03, 0.28026852E-03,-0.11082311E-03,
         0.18508089E-03,-0.72089293E-04, 0.13455721E-02, 0.50215214E-03,
         0.48930006E-03,-0.67621225E-03,-0.13477420E-04, 0.56552952E-04,
         0.46438180E-03, 0.18222908E-03,-0.19293079E-03, 0.12830457E-03,
        -0.31941036E-04, 0.28215678E-03, 0.90417551E-03,-0.81619386E-04,
         0.89967391E-04, 0.55440672E-03,-0.38151152E-03,-0.11153043E-02,
         0.80407131E-03,-0.20922457E-03,-0.15219842E-02,-0.14438386E-03,
         0.79492608E-03, 0.72756235E-03,-0.68735227E-03,-0.77952113E-03,
         0.52466162E-03, 0.27567509E-03,-0.40882724E-03, 0.11953476E-02,
         0.14896530E-04,-0.25513751E-04,-0.29707819E-03, 0.48684808E-04,
        -0.43941857E-03,-0.11322874E-03,-0.10849523E-03,-0.14515358E-03,
         0.13950445E-03,-0.97919699E-04, 0.66528009E-03, 0.76104829E-03,
        -0.32797211E-03, 0.80267695E-04,-0.45863507E-03, 0.53225778E-03,
         0.36562487E-03, 0.25835217E-03, 0.71801187E-04, 0.66221226E-03,
        -0.13156901E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]        *x31    *x53
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]            *x43    
        +coeff[ 21]        *x31*x42    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]            *x41*x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]    *x21*x32*x41*x51
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 26]        *x33        
        +coeff[ 27]    *x21*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22*x32*x41    
        +coeff[ 37]    *x22    *x41*x52
        +coeff[ 38]    *x23*x31*x42    
        +coeff[ 39]    *x21    *x41*x53
        +coeff[ 40]    *x21*x31    *x53
        +coeff[ 41]        *x31*x44*x52
        +coeff[ 42]    *x22*x33    *x52
        +coeff[ 43]        *x31*x42*x51
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 44]*x11*x21*x31    *x51
        +coeff[ 45]    *x21*x31    *x52
        +coeff[ 46]        *x31*x42*x52
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]    *x21*x33*x42*x51
        +coeff[ 49]        *x33    *x53
        +coeff[ 50]                *x51
        +coeff[ 51]*x11*x21*x31        
        +coeff[ 52]    *x21    *x43    
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 53]    *x21*x32*x41    
        +coeff[ 54]        *x32*x43    
        +coeff[ 55]*x11*x21    *x41*x51
        +coeff[ 56]    *x21    *x41*x52
        +coeff[ 57]    *x21    *x43*x51
        +coeff[ 58]    *x21*x31*x42*x51
        +coeff[ 59]    *x24*x31        
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]        *x33    *x52
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 62]    *x22    *x43*x51
        +coeff[ 63]    *x22*x31*x42*x51
        +coeff[ 64]    *x23*x33        
        +coeff[ 65]*x11*x21*x31    *x52
        +coeff[ 66]    *x22*x32*x41*x51
        +coeff[ 67]*x11    *x31    *x53
        +coeff[ 68]    *x23    *x43*x51
        +coeff[ 69]    *x24*x33        
        +coeff[ 70]        *x32*x43*x52
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 71]    *x23*x32*x41*x51
        +coeff[ 72]    *x23*x33    *x51
        +coeff[ 73]    *x24*x31    *x52
        +coeff[ 74]    *x24    *x43*x51
        +coeff[ 75]    *x23    *x41*x53
        +coeff[ 76]    *x22            
        +coeff[ 77]*x12        *x41    
        +coeff[ 78]    *x23*x31        
        +coeff[ 79]*x11*x22*x31        
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 80]    *x22    *x43    
        +coeff[ 81]*x11*x23    *x41    
        +coeff[ 82]*x11*x21*x33        
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]*x11    *x32*x41*x51
        +coeff[ 85]*x11*x22*x31    *x51
        +coeff[ 86]    *x22*x31    *x52
        +coeff[ 87]    *x23*x32*x41    
        +coeff[ 88]    *x24    *x41*x51
    ;
    v_y_e_fp_400                                  =v_y_e_fp_400                                  
        +coeff[ 89]*x11        *x41*x53
        +coeff[ 90]    *x22*x31*x44    
        +coeff[ 91]    *x23    *x41*x52
        +coeff[ 92]    *x22    *x41*x53
        +coeff[ 93]    *x22    *x43*x52
        +coeff[ 94]*x11    *x31*x41*x53
        +coeff[ 95]    *x21*x31*x42*x53
        +coeff[ 96]    *x24*x31*x42*x51
        ;

    return v_y_e_fp_400                                  ;
}
float p_e_fp_400                                  (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.2828951E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.24930626E-03, 0.17979512E-01,-0.34099367E-01, 0.16435212E-01,
        -0.87976307E-02,-0.99671222E-02, 0.45738583E-02, 0.77021169E-02,
        -0.56175620E-03, 0.50997543E-04, 0.89587316E-04, 0.54372130E-02,
         0.17072825E-02,-0.59137703E-03, 0.32236453E-02, 0.14409302E-02,
         0.61470212E-03, 0.11130369E-02, 0.31649004E-04,-0.18935796E-03,
         0.57354977E-03, 0.18721027E-02, 0.33030647E-03,-0.10612471E-02,
        -0.36018755E-03,-0.95209730E-03,-0.39794415E-03, 0.29254796E-04,
        -0.43403197E-03,-0.21020368E-03,-0.40132151E-03, 0.85259984E-04,
        -0.22031835E-03, 0.12970448E-02, 0.93069300E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_fp_400                                  =v_p_e_fp_400                                  
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_fp_400                                  =v_p_e_fp_400                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21    *x41*x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_fp_400                                  =v_p_e_fp_400                                  
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]            *x43*x53
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]        *x32*x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
        ;

    return v_p_e_fp_400                                  ;
}
float l_e_fp_400                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.2507332E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.24635501E-01,-0.31796217E+00,-0.25395870E-01, 0.12067113E-01,
        -0.35547320E-01,-0.12881699E-01,-0.75406032E-02,-0.35004094E-02,
         0.11806730E-01, 0.71227405E-04,-0.33432273E-02, 0.22931208E-02,
         0.70742415E-02, 0.16001926E-03,-0.40831920E-02,-0.78235177E-03,
         0.39011962E-02,-0.16399869E-02, 0.17245581E-02, 0.18688607E-02,
        -0.12786095E-02, 0.19482709E-02, 0.64491370E-03,-0.46729515E-03,
        -0.26171588E-03, 0.36267500E-03,-0.68100542E-03, 0.79679582E-02,
         0.34474948E-03, 0.74569387E-02,-0.10216879E-02,-0.57244068E-03,
         0.10997956E-02,-0.13514460E-02, 0.63888151E-02, 0.36471814E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_400                                  =v_l_e_fp_400                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_400                                  =v_l_e_fp_400                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x53
        +coeff[ 22]*x11*x22            
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]*x11            *x52
    ;
    v_l_e_fp_400                                  =v_l_e_fp_400                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]        *x32*x42*x51
        +coeff[ 31]*x11        *x43*x51
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]        *x33*x43    
        +coeff[ 34]    *x23*x32*x42    
    ;
    v_l_e_fp_400                                  =v_l_e_fp_400                                  
        +coeff[ 35]    *x23    *x44    
        ;

    return v_l_e_fp_400                                  ;
}
float x_e_fp_350                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3499509E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.34028240E-01, 0.46921048E+00,-0.15242830E-01,-0.29996198E-01,
         0.36699053E-01,-0.81547117E-02, 0.48438655E-02,-0.13243866E-01,
        -0.79311123E-02,-0.18084412E-01, 0.23083757E-02, 0.13619855E-02,
        -0.18287576E-02, 0.13717234E-02,-0.57647345E-02,-0.72492724E-02,
         0.93895597E-02,-0.58520079E-03, 0.77409338E-03,-0.21881550E-02,
        -0.10699136E-02, 0.25221559E-02, 0.14927001E-03, 0.18146422E-02,
         0.12744325E-02,-0.18456938E-02,-0.26253194E-02,-0.25692079E-02,
         0.21103325E-02,-0.18730944E-02, 0.50443504E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_350                                  =v_x_e_fp_350                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_350                                  =v_x_e_fp_350                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_350                                  =v_x_e_fp_350                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x22    *x42*x51
        ;

    return v_x_e_fp_350                                  ;
}
float t_e_fp_350                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7353973E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.69102161E-02,-0.29658819E-01, 0.77001885E-01, 0.34489487E-02,
        -0.57531246E-02,-0.22839117E-02,-0.24021647E-02,-0.31269933E-03,
        -0.11427071E-02,-0.10676468E-02, 0.24704699E-03, 0.21474609E-03,
        -0.60182402E-03,-0.30952221E-03,-0.32817054E-03, 0.47331993E-03,
         0.17955689E-02,-0.11034888E-03, 0.60175132E-03, 0.52983862E-04,
         0.29569864E-03, 0.93724218E-03,-0.30792397E-03, 0.90825044E-04,
         0.47306242E-03,-0.66764315E-03,-0.59758988E-03,-0.20111953E-03,
         0.26136896E-03,-0.41200159E-03, 0.92105591E-04, 0.11373193E-03,
         0.26348623E-03,-0.11955295E-03,-0.11125704E-03, 0.32826152E-03,
         0.88045948E-04,-0.23464987E-03,-0.43339093E-03, 0.60496968E-03,
        -0.21604206E-04,-0.51644816E-04, 0.46496454E-04,-0.78688281E-04,
         0.12221784E-03, 0.43632797E-04,-0.22979181E-03, 0.23139072E-03,
         0.21872752E-03, 0.16680339E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_350                                  =v_t_e_fp_350                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_350                                  =v_t_e_fp_350                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_350                                  =v_t_e_fp_350                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_350                                  =v_t_e_fp_350                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]    *x22    *x41    
        +coeff[ 41]    *x22        *x51
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_fp_350                                  =v_t_e_fp_350                                  
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x23*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_fp_350                                  ;
}
float y_e_fp_350                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.3189477E-03;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.32149386E-03,-0.17792687E-01,-0.43696661E-01,-0.13569561E-01,
        -0.11248760E-01, 0.20391112E-01,-0.30230975E-02,-0.15118627E-01,
        -0.72983499E-02, 0.44381823E-02, 0.17345628E-02,-0.45048581E-02,
         0.47767229E-03,-0.17841759E-02,-0.20165120E-02, 0.10578334E-02,
         0.27329796E-02,-0.29422825E-02,-0.96630724E-03, 0.36139117E-03,
        -0.15280134E-02,-0.16617146E-02,-0.11755332E-02, 0.14667474E-02,
        -0.10023807E-02,-0.17866367E-03,-0.16383908E-02, 0.47911925E-03,
         0.22165259E-03,-0.81547936E-04,-0.11226925E-02, 0.45933519E-03,
        -0.80467173E-03,-0.27262951E-04, 0.10583648E-02,-0.15665721E-02,
         0.37532012E-03, 0.91650453E-03,-0.13563664E-02, 0.13461864E-02,
        -0.31765451E-03, 0.43705729E-03,-0.19526207E-04, 0.60977520E-04,
        -0.35457872E-03, 0.19134000E-03, 0.15066102E-02, 0.88896201E-03,
        -0.16152434E-02,-0.84739114E-03,-0.98100281E-05, 0.73782331E-03,
         0.38412152E-03,-0.40836833E-03, 0.98253637E-04,-0.16235119E-04,
        -0.55453063E-04, 0.16764690E-03, 0.14684276E-02, 0.37012345E-03,
         0.90615613E-04, 0.57211559E-03,-0.20911092E-03, 0.61099377E-03,
        -0.42176139E-03,-0.15939517E-03, 0.72977506E-03,-0.14013709E-02,
         0.12371718E-02,-0.63436921E-03,-0.77640907E-05,-0.30138708E-04,
        -0.11803074E-03, 0.10823415E-03,-0.59756759E-03,-0.11136487E-03,
         0.75539108E-03, 0.14168261E-03,-0.17546640E-02, 0.44154396E-03,
        -0.14523792E-03,-0.30206298E-03,-0.13069862E-02, 0.11562103E-03,
        -0.52084925E-03,-0.11475687E-03, 0.59736968E-03, 0.68028494E-04,
        -0.36602159E-03, 0.43195853E-03,-0.13606340E-02, 0.37123816E-03,
         0.72353263E-03, 0.69392705E-03,-0.13620882E-03,-0.30351721E-03,
         0.26334290E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]        *x31    *x53
        +coeff[ 19]*x11        *x41    
        +coeff[ 20]            *x43    
        +coeff[ 21]        *x31*x42    
        +coeff[ 22]            *x41*x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]        *x33        
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 26]    *x21*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]*x11        *x41*x51
        +coeff[ 29]*x11    *x31    *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]        *x32*x41*x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x24    *x41    
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]    *x21*x32*x43    
        +coeff[ 37]    *x22    *x41*x52
        +coeff[ 38]    *x21    *x41*x53
        +coeff[ 39]    *x21*x31    *x53
        +coeff[ 40]        *x31*x44*x52
        +coeff[ 41]    *x24*x31    *x52
        +coeff[ 42]    *x22*x33    *x52
        +coeff[ 43]        *x31*x42*x51
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]*x11*x21*x31    *x51
        +coeff[ 46]        *x31*x42*x52
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]    *x23*x31*x42    
        +coeff[ 49]        *x33    *x53
        +coeff[ 50]*x11*x21*x31        
        +coeff[ 51]    *x21*x31*x42    
        +coeff[ 52]    *x21*x32*x41    
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 53]        *x32*x43    
        +coeff[ 54]*x11*x21    *x41*x51
        +coeff[ 55]    *x21    *x41*x52
        +coeff[ 56]    *x21*x31    *x52
        +coeff[ 57]    *x21    *x43*x51
        +coeff[ 58]    *x21*x31*x42*x51
        +coeff[ 59]    *x22*x33        
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]    *x21*x33    *x51
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 62]*x11*x22    *x41*x51
        +coeff[ 63]        *x33    *x52
        +coeff[ 64]    *x22    *x43*x51
        +coeff[ 65]*x11    *x31    *x53
        +coeff[ 66]    *x23    *x43*x51
        +coeff[ 67]    *x23*x32*x41*x51
        +coeff[ 68]    *x23    *x41*x53
        +coeff[ 69]    *x21*x33    *x53
        +coeff[ 70]                *x51
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 71]*x12        *x41    
        +coeff[ 72]    *x23*x31        
        +coeff[ 73]*x11*x22*x31        
        +coeff[ 74]    *x22    *x43    
        +coeff[ 75]*x11    *x31*x42*x51
        +coeff[ 76]    *x22*x31    *x52
        +coeff[ 77]        *x32*x43*x51
        +coeff[ 78]    *x22*x31*x42*x51
        +coeff[ 79]    *x23*x33        
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 80]*x11*x21*x31    *x52
        +coeff[ 81]    *x24    *x41*x51
        +coeff[ 82]    *x22*x32*x41*x51
        +coeff[ 83]*x11        *x41*x53
        +coeff[ 84]    *x22*x31*x44    
        +coeff[ 85]*x11*x22*x33        
        +coeff[ 86]    *x23    *x41*x52
        +coeff[ 87]        *x33*x41*x52
        +coeff[ 88]        *x31*x42*x53
    ;
    v_y_e_fp_350                                  =v_y_e_fp_350                                  
        +coeff[ 89]    *x22    *x41*x53
        +coeff[ 90]    *x23*x31*x42*x51
        +coeff[ 91]    *x24*x33        
        +coeff[ 92]    *x22    *x43*x52
        +coeff[ 93]    *x21*x31*x42*x53
        +coeff[ 94]*x11*x24*x31    *x51
        +coeff[ 95]    *x21    *x45*x52
        +coeff[ 96]            *x45*x53
        ;

    return v_y_e_fp_350                                  ;
}
float p_e_fp_350                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.3575525E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.48375096E-04, 0.18105390E-01,-0.33849966E-01, 0.16407566E-01,
        -0.87685604E-02,-0.99332295E-02, 0.76879440E-02,-0.58245618E-03,
         0.43669330E-04, 0.15574481E-03, 0.72440234E-04, 0.54355771E-02,
         0.16968776E-02, 0.44527850E-02,-0.59432723E-03, 0.31636301E-02,
         0.14722754E-02, 0.62351453E-03, 0.11354414E-02, 0.16990780E-04,
        -0.18474252E-03, 0.56652131E-03, 0.18914274E-02,-0.10514020E-02,
         0.29557015E-03,-0.34991311E-03,-0.96839317E-03,-0.41060479E-03,
         0.27021588E-04,-0.20706377E-03,-0.40232693E-03, 0.92167742E-04,
         0.12901963E-02, 0.93117810E-03,-0.26384761E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_350                                  =v_p_e_fp_350                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_350                                  =v_p_e_fp_350                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_350                                  =v_p_e_fp_350                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_350                                  ;
}
float l_e_fp_350                                  (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2338143E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.22637825E-01,-0.31798816E+00,-0.25449898E-01, 0.12235578E-01,
        -0.35702977E-01,-0.12868841E-01,-0.77099353E-02,-0.35908984E-02,
         0.88587254E-02, 0.20225989E-03,-0.35999173E-02, 0.22453275E-02,
         0.71073882E-02, 0.37129114E-02,-0.42821914E-02,-0.82825409E-03,
        -0.17564559E-02, 0.15357175E-02,-0.13821408E-02, 0.15823107E-02,
         0.23004210E-02, 0.18651204E-02,-0.10451796E-02,-0.26570791E-04,
        -0.63581066E-03, 0.59448666E-03, 0.11327976E-03,-0.15142997E-03,
        -0.81825437E-03, 0.10551015E-02, 0.88723851E-02, 0.59829447E-02,
        -0.68715750E-03, 0.27239427E-02, 0.28337850E-02,-0.76487917E-03,
         0.15517546E-01, 0.24516759E-01, 0.99899909E-02, 0.12177710E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_350                                  =v_l_e_fp_350                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_fp_350                                  =v_l_e_fp_350                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]                *x53
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]        *x31        
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_fp_350                                  =v_l_e_fp_350                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]*x12    *x31    *x52
        +coeff[ 33]    *x24*x31*x41    
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_fp_350                                  =v_l_e_fp_350                                  
        +coeff[ 35]    *x22*x31    *x53
        +coeff[ 36]    *x21*x34*x42    
        +coeff[ 37]    *x21*x33*x43    
        +coeff[ 38]    *x21*x32*x44    
        +coeff[ 39]*x11*x23*x31    *x52
        ;

    return v_l_e_fp_350                                  ;
}
float x_e_fp_300                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3330574E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.32367386E-01, 0.46852961E+00,-0.15042298E-01,-0.29902864E-01,
         0.36882486E-01,-0.81138713E-02, 0.47689271E-02,-0.13197735E-01,
        -0.78582000E-02,-0.18169675E-01, 0.23874126E-02, 0.15600185E-02,
        -0.17750950E-02, 0.13814677E-02,-0.63469480E-02,-0.73298295E-02,
         0.94331345E-02,-0.58432308E-03, 0.79317228E-03,-0.21191135E-02,
        -0.14193987E-02, 0.26659330E-02, 0.28341095E-03, 0.19483825E-02,
         0.12431855E-02,-0.30152556E-02,-0.31789572E-02,-0.26636000E-02,
         0.23360061E-02,-0.63706754E-03, 0.45890235E-02, 0.16919861E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_300                                  =v_x_e_fp_300                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_300                                  =v_x_e_fp_300                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_300                                  =v_x_e_fp_300                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22*x31*x41*x51
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]    *x23        *x53
        ;

    return v_x_e_fp_300                                  ;
}
float t_e_fp_300                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7035340E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.66408142E-02,-0.29867535E-01, 0.76841280E-01, 0.34800044E-02,
        -0.57498263E-02,-0.23046744E-02,-0.23652278E-02,-0.31806214E-03,
        -0.11934629E-02,-0.10473724E-02, 0.26000288E-03, 0.22540383E-03,
        -0.59450639E-03,-0.24244534E-03,-0.33118104E-03, 0.49227255E-03,
         0.17798690E-02,-0.10222880E-03, 0.57631877E-03, 0.12361960E-03,
        -0.73883601E-03, 0.30876297E-03,-0.69529988E-03, 0.82249969E-03,
        -0.48661031E-03,-0.28126547E-03, 0.15094123E-03, 0.48132846E-03,
         0.26720468E-03,-0.36841136E-03, 0.69114700E-03, 0.88684261E-04,
         0.96205222E-04, 0.24688384E-03,-0.98674878E-04,-0.25617162E-03,
        -0.32632548E-03,-0.27888172E-03, 0.37570913E-04, 0.22592880E-04,
         0.66840897E-04,-0.72907809E-04, 0.53723837E-04,-0.36890659E-04,
        -0.20092668E-03,-0.81347935E-04, 0.72738745E-04, 0.17453285E-03,
         0.23651206E-03, 0.21982580E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_300                                  =v_t_e_fp_300                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_fp_300                                  =v_t_e_fp_300                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21*x31*x41*x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x23    *x42*x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_fp_300                                  =v_t_e_fp_300                                  
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_fp_300                                  =v_t_e_fp_300                                  
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]        *x32*x42*x51
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11            *x52
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]*x11*x21    *x42    
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]*x11    *x32    *x51
    ;
    v_t_e_fp_300                                  =v_t_e_fp_300                                  
        +coeff[ 44]    *x21    *x42*x51
        +coeff[ 45]    *x22        *x52
        +coeff[ 46]        *x31*x41*x52
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x23    *x42    
        ;

    return v_t_e_fp_300                                  ;
}
float y_e_fp_300                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1396820E-04;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.56059348E-05,-0.17615665E-01,-0.44121787E-01,-0.13792664E-01,
        -0.11340008E-01, 0.20464472E-01,-0.29685018E-02,-0.15449972E-01,
        -0.74082892E-02, 0.43836390E-02, 0.18067529E-02,-0.47448007E-02,
         0.47094421E-03,-0.18372706E-02,-0.22137051E-02, 0.11135080E-02,
         0.27088784E-02,-0.29411810E-02, 0.36323201E-03,-0.15532726E-02,
        -0.16629733E-02,-0.11407632E-02,-0.10910594E-02, 0.15430442E-02,
        -0.12670853E-02, 0.44537318E-03, 0.27190647E-03,-0.12102054E-03,
        -0.10411801E-02, 0.31672424E-03,-0.10728007E-03, 0.11086919E-02,
        -0.15785157E-02, 0.33468529E-03, 0.64082799E-03,-0.11993499E-02,
         0.94566407E-04, 0.36977071E-04,-0.75775426E-03,-0.19770952E-03,
        -0.33071602E-03,-0.85190783E-03, 0.18559830E-03, 0.49244455E-03,
         0.91888389E-03, 0.13079381E-02, 0.98297477E-03,-0.15209834E-02,
         0.16234108E-02,-0.70651702E-03,-0.80970553E-03,-0.55328128E-03,
        -0.30929263E-04,-0.15920051E-02, 0.11784817E-03,-0.71223141E-04,
         0.31453959E-03, 0.10962931E-02, 0.17360327E-03, 0.10255499E-03,
         0.12693848E-02, 0.88572787E-03, 0.72977680E-03, 0.75189554E-03,
        -0.13268692E-02,-0.10671925E-02, 0.97536598E-03, 0.46234776E-03,
        -0.29501831E-03, 0.40978110E-04,-0.47788784E-03,-0.11467031E-03,
        -0.14486547E-03,-0.10955665E-03,-0.11653076E-03,-0.18236086E-03,
        -0.10092332E-02, 0.37395832E-03, 0.29770998E-03,-0.96849311E-04,
         0.59649459E-03, 0.17150318E-03, 0.25260227E-03, 0.74758031E-03,
         0.47173933E-03,-0.47790271E-03, 0.37561890E-03, 0.28840307E-03,
        -0.71281614E-03, 0.34677485E-03, 0.44269115E-03,-0.41701246E-03,
        -0.91539230E-04,-0.11037668E-02,-0.12687407E-03,-0.49829012E-03,
         0.13987080E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]    *x21    *x43    
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]*x11    *x31    *x51
        +coeff[ 28]            *x43*x51
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]        *x33    *x51
        +coeff[ 31]    *x24    *x41    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x21*x33    *x51
        +coeff[ 34]    *x21*x32*x43    
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 35]    *x21    *x41*x53
        +coeff[ 36]        *x34*x41*x51
        +coeff[ 37]    *x23*x31    *x52
        +coeff[ 38]        *x32*x43*x52
        +coeff[ 39]        *x33        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]*x11*x21*x31    *x51
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x22*x31    *x52
        +coeff[ 46]    *x23*x32*x41    
        +coeff[ 47]    *x22*x31*x42*x51
        +coeff[ 48]    *x21*x31    *x53
        +coeff[ 49]        *x33    *x53
        +coeff[ 50]    *x23*x32*x41*x51
        +coeff[ 51]    *x21*x33    *x53
        +coeff[ 52]*x11*x21*x31        
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 53]    *x21*x31    *x51
        +coeff[ 54]*x11*x21    *x41*x51
        +coeff[ 55]    *x21    *x41*x52
        +coeff[ 56]    *x21    *x43*x51
        +coeff[ 57]    *x21*x31*x42*x51
        +coeff[ 58]    *x24*x31        
        +coeff[ 59]*x11    *x31    *x52
        +coeff[ 60]        *x31*x42*x52
        +coeff[ 61]    *x23*x31    *x51
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 62]        *x33    *x52
        +coeff[ 63]    *x23*x33        
        +coeff[ 64]    *x22*x32*x41*x51
        +coeff[ 65]    *x24    *x43*x51
        +coeff[ 66]    *x23    *x41*x53
        +coeff[ 67]    *x21*x31*x42    
        +coeff[ 68]    *x23*x31        
        +coeff[ 69]*x11*x22*x31        
        +coeff[ 70]    *x22    *x43    
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 71]        *x32*x43    
        +coeff[ 72]    *x22*x31*x42    
        +coeff[ 73]    *x21*x31    *x52
        +coeff[ 74]*x11*x23    *x41    
        +coeff[ 75]*x11*x22    *x41*x51
        +coeff[ 76]    *x23*x31*x42    
        +coeff[ 77]    *x21*x33*x42    
        +coeff[ 78]*x11*x22*x31*x42    
        +coeff[ 79]*x11    *x31    *x53
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 80]    *x23    *x41*x52
        +coeff[ 81]    *x21*x32*x41*x52
        +coeff[ 82]*x11*x21*x32*x43    
        +coeff[ 83]    *x23    *x43*x51
        +coeff[ 84]    *x22    *x41*x53
        +coeff[ 85]    *x23*x31*x42*x51
        +coeff[ 86]    *x24*x33        
        +coeff[ 87]    *x22    *x43*x52
        +coeff[ 88]    *x22*x31*x42*x52
    ;
    v_y_e_fp_300                                  =v_y_e_fp_300                                  
        +coeff[ 89]    *x23*x33    *x51
        +coeff[ 90]    *x21*x31*x42*x53
        +coeff[ 91]    *x22*x33    *x52
        +coeff[ 92]*x12        *x43*x52
        +coeff[ 93]    *x24*x31*x42*x51
        +coeff[ 94]*x11*x23*x31    *x52
        +coeff[ 95]    *x23*x31    *x53
        +coeff[ 96]*x11    *x31*x41    
        ;

    return v_y_e_fp_300                                  ;
}
float p_e_fp_300                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1182458E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.12392171E-03, 0.18248457E-01,-0.33587817E-01, 0.16409496E-01,
        -0.87650921E-02,-0.99301105E-02, 0.77537191E-02,-0.58253290E-03,
         0.46852798E-04, 0.16631381E-03, 0.76550161E-04, 0.53968215E-02,
         0.17017074E-02, 0.44724867E-02,-0.58617402E-03, 0.31341417E-02,
         0.14745665E-02, 0.11363592E-02,-0.15401127E-04,-0.18343222E-03,
         0.55429986E-03, 0.19003184E-02, 0.62981871E-03,-0.10375526E-02,
         0.29869159E-03,-0.35052816E-03,-0.95404236E-03,-0.40163775E-03,
         0.93430372E-05,-0.19240906E-03,-0.39446854E-03, 0.78761623E-04,
         0.13500478E-02, 0.91482344E-03,-0.26390268E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_300                                  =v_p_e_fp_300                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_300                                  =v_p_e_fp_300                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_300                                  =v_p_e_fp_300                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_300                                  ;
}
float l_e_fp_300                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.2307006E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.22802731E-01,-0.32065222E+00,-0.25441444E-01, 0.12085338E-01,
        -0.36097664E-01,-0.12808803E-01,-0.74187545E-02,-0.34107445E-02,
         0.97420616E-02, 0.20409995E-03,-0.29684331E-02, 0.23011097E-02,
         0.69390237E-02, 0.29997418E-02,-0.42650057E-02,-0.87884418E-03,
         0.29825966E-02,-0.15411186E-02, 0.14502726E-02, 0.21600882E-02,
         0.20369194E-02,-0.94698806E-03, 0.74161205E-03, 0.90678744E-02,
         0.55765421E-02,-0.12615145E-02, 0.13057938E-02,-0.77979465E-03,
         0.94450283E-03,-0.11100938E-02, 0.21039958E-02,-0.65199641E-03,
         0.40791684E-03,-0.16516566E-02, 0.16307037E-02,-0.17297616E-02,
         0.76412351E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_300                                  =v_l_e_fp_300                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_300                                  =v_l_e_fp_300                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]                *x53
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x23            
    ;
    v_l_e_fp_300                                  =v_l_e_fp_300                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x31*x41*x52
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]            *x44*x51
        +coeff[ 32]*x11    *x34        
        +coeff[ 33]*x11    *x31*x43    
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_fp_300                                  =v_l_e_fp_300                                  
        +coeff[ 35]    *x22    *x42*x52
        +coeff[ 36]    *x22*x31    *x53
        ;

    return v_l_e_fp_300                                  ;
}
float x_e_fp_250                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3491238E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.33967249E-01, 0.46787795E+00,-0.14857599E-01,-0.29865393E-01,
         0.36841154E-01,-0.80576232E-02, 0.46275039E-02,-0.13067050E-01,
        -0.78732809E-02,-0.17976819E-01, 0.23365514E-02, 0.16861829E-02,
        -0.18670581E-02, 0.14489852E-02,-0.64671412E-02,-0.72931997E-02,
         0.93361679E-02,-0.56266709E-03, 0.81277947E-03,-0.21082917E-02,
        -0.14925485E-02, 0.25432443E-02, 0.27949203E-03, 0.20548319E-02,
         0.13044650E-02,-0.28887603E-02,-0.30855576E-02,-0.26683074E-02,
         0.23179026E-02, 0.45141266E-02, 0.18048646E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_250                                  =v_x_e_fp_250                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_250                                  =v_x_e_fp_250                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_250                                  =v_x_e_fp_250                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23        *x53
        ;

    return v_x_e_fp_250                                  ;
}
float t_e_fp_250                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7261088E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.68721627E-02,-0.30062517E-01, 0.76694347E-01, 0.35132687E-02,
        -0.57458440E-02,-0.22763796E-02,-0.23961228E-02,-0.31137685E-03,
        -0.12412834E-02,-0.10677311E-02,-0.38166053E-03, 0.27950894E-03,
        -0.31034459E-03, 0.47719470E-03, 0.17389820E-02,-0.95130992E-04,
         0.19140994E-03,-0.60086284E-03, 0.60407288E-03, 0.92084294E-04,
        -0.34051799E-03, 0.30800657E-03, 0.81684761E-03,-0.29122512E-03,
         0.20496974E-03, 0.46879763E-03,-0.80144347E-03,-0.61237335E-03,
        -0.44045661E-03, 0.28362463E-03, 0.68285014E-03, 0.94876428E-04,
         0.23435449E-03,-0.10212519E-03,-0.24769912E-03, 0.42341757E-03,
        -0.53436674E-04,-0.25719666E-03,-0.12974885E-04, 0.38545473E-04,
         0.27514623E-04, 0.59563270E-04, 0.14407988E-03,-0.86030217E-04,
         0.68497495E-04,-0.65924934E-04,-0.30438094E-04, 0.17985544E-03,
         0.10303849E-03,-0.20484395E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_250                                  =v_t_e_fp_250                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_fp_250                                  =v_t_e_fp_250                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]        *x32        
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_fp_250                                  =v_t_e_fp_250                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_fp_250                                  =v_t_e_fp_250                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11*x23        *x51
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]*x12                
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11            *x52
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]    *x22        *x52
    ;
    v_t_e_fp_250                                  =v_t_e_fp_250                                  
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]            *x42*x52
        +coeff[ 46]*x13*x22            
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x21*x32*x42    
        ;

    return v_t_e_fp_250                                  ;
}
float y_e_fp_250                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4407289E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.43674681E-03,-0.17363409E-01,-0.44758130E-01,-0.14190774E-01,
        -0.11572435E-01, 0.20516505E-01,-0.29270889E-02,-0.15823677E-01,
        -0.74677411E-02, 0.44430047E-02, 0.19081873E-02,-0.47379136E-02,
         0.48427758E-03,-0.18299066E-02,-0.21124971E-02, 0.11122200E-02,
         0.27885737E-02,-0.30115419E-02, 0.37056164E-03,-0.15172262E-02,
        -0.17236922E-02,-0.11358698E-02,-0.11281277E-02, 0.16523869E-02,
        -0.91683824E-03,-0.19283975E-03, 0.25272917E-03, 0.62210427E-03,
        -0.84585336E-04,-0.10412374E-02, 0.37187740E-03,-0.74598525E-03,
        -0.10743488E-03,-0.13531664E-03, 0.12139892E-02,-0.14421134E-02,
         0.95824251E-03,-0.40735287E-03,-0.11069975E-02,-0.12940776E-02,
        -0.45564543E-05,-0.12181849E-02,-0.12507130E-03,-0.33759672E-03,
         0.18759345E-03, 0.38955599E-03, 0.11726116E-02, 0.40306864E-03,
         0.11591557E-02, 0.10658295E-04,-0.65707922E-03, 0.23085905E-03,
        -0.12706050E-04,-0.46408545E-05, 0.43880608E-03, 0.20801686E-03,
        -0.46701977E-03, 0.73554933E-04,-0.73002343E-05, 0.42970816E-03,
         0.13153659E-02, 0.92298513E-04,-0.73678064E-04, 0.67425426E-03,
        -0.91890676E-03, 0.77600300E-03,-0.15262519E-03,-0.13736775E-02,
        -0.19374149E-03, 0.53123874E-03,-0.14267031E-02,-0.99978491E-03,
         0.11673140E-02,-0.27862210E-04, 0.87328990E-04,-0.29447381E-03,
         0.61725237E-04,-0.34018853E-04,-0.20773811E-03,-0.13398490E-03,
         0.19705950E-03, 0.66638121E-03,-0.15678191E-03, 0.13533734E-03,
         0.90522930E-03, 0.90476172E-03,-0.25215570E-03,-0.23166483E-03,
         0.26448609E-03, 0.57582278E-03, 0.12308483E-03, 0.39932455E-03,
        -0.51924732E-03, 0.58373890E-03, 0.43086617E-03, 0.77829510E-03,
        -0.14985801E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]        *x33        
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]*x11    *x31    *x51
        +coeff[ 29]            *x43*x51
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]    *x24    *x41    
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]    *x22    *x41*x52
        +coeff[ 37]        *x32*x41*x52
        +coeff[ 38]    *x23*x31*x42    
        +coeff[ 39]    *x21    *x41*x53
        +coeff[ 40]    *x22*x33    *x52
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]        *x31*x42*x51
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 44]*x11*x21*x31    *x51
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]        *x31*x42*x52
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]    *x21*x31    *x53
        +coeff[ 49]    *x21*x32*x41*x52
        +coeff[ 50]        *x33    *x53
        +coeff[ 51]    *x24*x31    *x52
        +coeff[ 52]                *x51
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 53]*x11*x21*x31        
        +coeff[ 54]    *x21    *x43    
        +coeff[ 55]    *x21*x32*x41    
        +coeff[ 56]    *x22    *x43    
        +coeff[ 57]*x11*x21    *x41*x51
        +coeff[ 58]    *x21    *x41*x52
        +coeff[ 59]    *x21    *x43*x51
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]*x11    *x31    *x52
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 62]    *x21*x33    *x51
        +coeff[ 63]        *x33    *x52
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]*x11*x21*x31    *x52
        +coeff[ 67]    *x22*x32*x41*x51
        +coeff[ 68]*x11    *x31    *x53
        +coeff[ 69]    *x23    *x43*x51
        +coeff[ 70]    *x23*x32*x41*x51
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 71]    *x24    *x43*x51
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x12        *x41    
        +coeff[ 74]*x11    *x31*x42    
        +coeff[ 75]    *x23*x31        
        +coeff[ 76]*x11*x22*x31        
        +coeff[ 77]*x11    *x33        
        +coeff[ 78]            *x43*x52
        +coeff[ 79]*x11*x23    *x41    
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 80]*x11*x21*x32*x41    
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]*x11*x22    *x41*x51
        +coeff[ 83]*x11    *x32*x41*x51
        +coeff[ 84]    *x22*x31    *x52
        +coeff[ 85]    *x23*x32*x41    
        +coeff[ 86]    *x21*x34*x41    
        +coeff[ 87]        *x32*x45    
        +coeff[ 88]*x11*x21*x32*x41*x51
    ;
    v_y_e_fp_250                                  =v_y_e_fp_250                                  
        +coeff[ 89]    *x23    *x41*x52
        +coeff[ 90]*x11    *x32*x41*x52
        +coeff[ 91]    *x22    *x41*x53
        +coeff[ 92]    *x23*x31*x42*x51
        +coeff[ 93]    *x24*x33        
        +coeff[ 94]    *x22    *x43*x52
        +coeff[ 95]    *x23*x33    *x51
        +coeff[ 96]    *x24*x31*x42*x51
        ;

    return v_y_e_fp_250                                  ;
}
float p_e_fp_250                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1943950E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.21666836E-03, 0.18424653E-01,-0.33115294E-01, 0.16388282E-01,
        -0.87498557E-02,-0.98945228E-02, 0.77752438E-02,-0.39105065E-03,
         0.69624992E-04, 0.22458225E-03,-0.16415087E-03, 0.53044381E-02,
         0.17071639E-02, 0.44733589E-02,-0.57533820E-03, 0.31252215E-02,
         0.14693715E-02, 0.64357975E-03, 0.11401980E-02,-0.29803397E-04,
        -0.17609942E-03, 0.57515502E-03, 0.18959573E-02,-0.10583223E-02,
         0.28233312E-03,-0.35295109E-03,-0.93984063E-03,-0.40805485E-03,
        -0.55015539E-05,-0.20120532E-03,-0.40692012E-03, 0.85669053E-04,
         0.12810406E-02, 0.92669373E-03,-0.26487524E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_250                                  =v_p_e_fp_250                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_250                                  =v_p_e_fp_250                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_250                                  =v_p_e_fp_250                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_fp_250                                  ;
}
float l_e_fp_250                                  (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.2259706E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.22206895E-01,-0.32259017E+00,-0.25384467E-01, 0.12099904E-01,
        -0.36576442E-01,-0.12019712E-01,-0.72281463E-02,-0.29755165E-02,
         0.92374338E-02, 0.20504404E-03,-0.34333747E-02, 0.21576968E-02,
         0.89207506E-02, 0.40759197E-02,-0.42125811E-02,-0.81181212E-03,
        -0.19237768E-02, 0.23321267E-02,-0.15244182E-02, 0.17992452E-02,
         0.20697552E-02, 0.17238901E-02,-0.11827815E-02, 0.84395404E-03,
        -0.22395281E-03, 0.46459539E-03, 0.88197982E-03,-0.86261041E-03,
        -0.74126618E-03, 0.42179850E-03, 0.57166955E-03, 0.74268742E-02,
         0.57249614E-02, 0.72496594E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_fp_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_250                                  =v_l_e_fp_250                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_fp_250                                  =v_l_e_fp_250                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]                *x53
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_fp_250                                  =v_l_e_fp_250                                  
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]                *x54
        +coeff[ 29]*x11*x22*x31        
        +coeff[ 30]*x11*x21        *x52
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]        *x32*x41*x52
        ;

    return v_l_e_fp_250                                  ;
}
float x_e_fp_200                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3236279E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.31326525E-01, 0.46694434E+00,-0.14632530E-01,-0.29792544E-01,
         0.36767781E-01,-0.79609444E-02, 0.44026431E-02,-0.13105693E-01,
        -0.78857904E-02,-0.17705491E-01, 0.22911041E-02, 0.16719288E-02,
        -0.19254503E-02, 0.14789900E-02,-0.59457663E-02,-0.69347564E-02,
         0.93060741E-02,-0.58956211E-03, 0.79273898E-03,-0.20241940E-02,
        -0.10340993E-02, 0.23496503E-02, 0.30928760E-03, 0.19282962E-02,
         0.13684919E-02,-0.26434816E-02,-0.29283483E-02,-0.27329212E-02,
         0.21666365E-02,-0.18383232E-02, 0.43442454E-02, 0.16985169E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_200                                  =v_x_e_fp_200                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21*x31*x41    
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_200                                  =v_x_e_fp_200                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]        *x32    *x52
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_fp_200                                  =v_x_e_fp_200                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]    *x23        *x53
        ;

    return v_x_e_fp_200                                  ;
}
float t_e_fp_200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6974665E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64427159E-02,-0.30248109E-01, 0.76446861E-01, 0.34869013E-02,
        -0.57265414E-02,-0.22732308E-02,-0.23605539E-02,-0.29119407E-03,
        -0.13133388E-02,-0.10766506E-02,-0.42997141E-03, 0.29066924E-03,
        -0.32224329E-03, 0.46927051E-03, 0.17345767E-02,-0.99630808E-04,
         0.18490193E-03,-0.58802892E-03, 0.48447811E-03, 0.97750803E-04,
        -0.32724571E-03, 0.31047771E-03, 0.77137433E-03,-0.27873105E-03,
         0.17280527E-03, 0.46553375E-03,-0.76609477E-03,-0.61668630E-03,
        -0.44160750E-03, 0.29407948E-03, 0.65593258E-03, 0.83252562E-04,
        -0.13756166E-03,-0.92020666E-04,-0.23739238E-03, 0.49314759E-03,
        -0.59285943E-04, 0.86250700E-04,-0.24414292E-03,-0.11457572E-04,
         0.52236537E-04, 0.19506662E-03, 0.15518286E-03, 0.88634864E-04,
        -0.62406078E-04, 0.17822567E-03, 0.16788827E-03, 0.94519601E-04,
        -0.30533760E-03, 0.16998676E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_200                                  =v_t_e_fp_200                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_fp_200                                  =v_t_e_fp_200                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]        *x32        
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_fp_200                                  =v_t_e_fp_200                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_fp_200                                  =v_t_e_fp_200                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11*x23        *x51
        +coeff[ 37]*x11    *x32    *x52
        +coeff[ 38]        *x32    *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11*x23            
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]        *x31*x41*x52
    ;
    v_t_e_fp_200                                  =v_t_e_fp_200                                  
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_fp_200                                  ;
}
float y_e_fp_200                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2047481E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.21958261E-03,-0.17101835E-01,-0.45784354E-01,-0.14951735E-01,
        -0.11875927E-01, 0.20566918E-01,-0.28841526E-02,-0.16051920E-01,
        -0.75293216E-02, 0.44438490E-02, 0.18504451E-02,-0.43748282E-02,
         0.49299002E-03, 0.11198234E-02, 0.30985915E-02,-0.30669952E-02,
        -0.10399571E-03,-0.73896219E-04, 0.41810327E-03,-0.16222497E-02,
        -0.17651813E-02,-0.17770093E-02,-0.10975767E-02,-0.11172858E-02,
         0.16415315E-02,-0.12629764E-02,-0.19827648E-02,-0.18622176E-03,
         0.58441266E-03, 0.25816957E-03,-0.79552447E-04,-0.91941358E-03,
         0.43254351E-03,-0.82679850E-03,-0.13847295E-03,-0.13454369E-03,
         0.11701355E-02, 0.32887241E-03, 0.10118994E-02,-0.19240231E-02,
        -0.11472014E-02,-0.75432291E-03,-0.10630029E-02, 0.52701426E-03,
        -0.36993157E-03,-0.48580230E-03, 0.17158253E-03,-0.16433027E-02,
         0.40677207E-03, 0.54093456E-03, 0.74138283E-03,-0.21337948E-02,
         0.10048895E-02,-0.14765190E-02, 0.53939049E-03, 0.81495626E-03,
        -0.11652061E-04, 0.43432315E-05, 0.10081133E-02, 0.97545337E-04,
         0.32307580E-03, 0.59692271E-03, 0.12069279E-02,-0.71613875E-03,
        -0.39503476E-03, 0.97293538E-04,-0.14930475E-03,-0.56163804E-04,
         0.64947415E-03,-0.67121355E-03, 0.10386250E-02, 0.78988187E-05,
         0.14800568E-04,-0.55640394E-04,-0.23211478E-03, 0.67867484E-04,
        -0.51956944E-03,-0.30074193E-03, 0.63543004E-04,-0.14568525E-03,
         0.12348937E-03,-0.43091623E-03,-0.17531411E-03,-0.43814146E-03,
        -0.25424728E-03,-0.97608769E-04, 0.60629280E-03,-0.96964301E-04,
         0.28194260E-03, 0.57847815E-03, 0.33708310E-03,-0.67982654E-03,
        -0.13668597E-03, 0.75117452E-03, 0.38105252E-03, 0.48168527E-03,
         0.64067799E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]        *x34*x41    
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]            *x41*x53
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]    *x21*x32*x41*x51
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]        *x33        
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]*x11        *x41*x51
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]            *x43*x51
        +coeff[ 32]    *x21*x33        
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]    *x23*x31*x42    
        +coeff[ 40]    *x21    *x41*x53
        +coeff[ 41]        *x32*x43*x52
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]    *x22*x31*x42    
        +coeff[ 46]*x11*x21*x31    *x51
        +coeff[ 47]    *x22*x32*x41    
        +coeff[ 48]    *x23*x31    *x51
        +coeff[ 49]    *x22*x31    *x52
        +coeff[ 50]        *x33    *x52
        +coeff[ 51]    *x22*x31*x42*x51
        +coeff[ 52]    *x21*x31    *x53
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 53]    *x22*x32*x41*x51
        +coeff[ 54]    *x23    *x41*x52
        +coeff[ 55]    *x21*x33*x42*x51
        +coeff[ 56]                *x51
        +coeff[ 57]*x11*x21*x31        
        +coeff[ 58]    *x21*x31*x42    
        +coeff[ 59]*x11*x21    *x41*x51
        +coeff[ 60]    *x21    *x43*x51
        +coeff[ 61]    *x21*x31*x42*x51
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 62]        *x31*x42*x52
        +coeff[ 63]    *x22    *x43*x51
        +coeff[ 64]    *x24    *x41*x51
        +coeff[ 65]*x11*x24*x31        
        +coeff[ 66]*x11    *x31    *x53
        +coeff[ 67]    *x24    *x43    
        +coeff[ 68]    *x23    *x43*x51
        +coeff[ 69]        *x33    *x53
        +coeff[ 70]    *x23    *x41*x53
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 71]    *x21            
        +coeff[ 72]    *x22            
        +coeff[ 73]*x11        *x43    
        +coeff[ 74]    *x23*x31        
        +coeff[ 75]            *x45    
        +coeff[ 76]    *x22    *x43    
        +coeff[ 77]        *x32*x43    
        +coeff[ 78]*x11    *x31    *x52
        +coeff[ 79]*x11*x23    *x41    
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 80]*x11*x21*x32*x41    
        +coeff[ 81]    *x21*x31*x44    
        +coeff[ 82]    *x21*x33    *x51
        +coeff[ 83]    *x23    *x43    
        +coeff[ 84]*x11*x22    *x41*x51
        +coeff[ 85]*x11*x22*x31    *x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]*x11*x21*x31    *x52
        +coeff[ 88]    *x22    *x41*x53
    ;
    v_y_e_fp_200                                  =v_y_e_fp_200                                  
        +coeff[ 89]    *x24*x33        
        +coeff[ 90]    *x22    *x43*x52
        +coeff[ 91]    *x23*x32*x41*x51
        +coeff[ 92]*x11*x23*x33        
        +coeff[ 93]    *x23*x33    *x51
        +coeff[ 94]*x11*x22*x32*x41*x51
        +coeff[ 95]    *x21*x31*x42*x53
        +coeff[ 96]    *x24*x31    *x52
        ;

    return v_y_e_fp_200                                  ;
}
float p_e_fp_200                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1621352E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.14684137E-03, 0.18737990E-01,-0.32416012E-01, 0.16458677E-01,
        -0.87335827E-02,-0.98665999E-02, 0.77393390E-02,-0.68568427E-03,
        -0.10574720E-03, 0.17646661E-03, 0.98216282E-04, 0.54323408E-02,
         0.17105531E-02, 0.44729705E-02,-0.56331750E-03, 0.30839823E-02,
         0.14453233E-02, 0.60697144E-03, 0.11496461E-02, 0.27703069E-04,
        -0.17407302E-03, 0.56378689E-03, 0.18272385E-02,-0.11516924E-02,
         0.35521193E-03,-0.35258391E-03,-0.13902128E-02,-0.40251072E-03,
         0.77231862E-05,-0.61141409E-03,-0.19721728E-03,-0.60612592E-03,
         0.77692384E-04, 0.12716257E-02, 0.96307526E-03,-0.27351393E-03,
        -0.50643343E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_200                                  =v_p_e_fp_200                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_200                                  =v_p_e_fp_200                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_200                                  =v_p_e_fp_200                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_fp_200                                  =v_p_e_fp_200                                  
        +coeff[ 35]            *x41*x53
        +coeff[ 36]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_200                                  ;
}
float l_e_fp_200                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2451507E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.22678526E-01,-0.32556918E+00,-0.25354316E-01, 0.11956461E-01,
        -0.36930058E-01,-0.12647117E-01,-0.70323711E-02,-0.38364162E-02,
         0.22482276E-02, 0.10912996E-01, 0.11846617E-03,-0.29724631E-02,
         0.97063258E-02, 0.18619701E-02,-0.39927163E-02,-0.71468105E-03,
        -0.10466535E-02, 0.34647193E-02,-0.14836257E-02, 0.15726214E-02,
         0.18838749E-02, 0.19029739E-02,-0.73787820E-03, 0.66132011E-03,
        -0.34112675E-03,-0.42503854E-03,-0.35328706E-03,-0.77367190E-03,
        -0.10445083E-02, 0.11807223E-02,-0.17000287E-02, 0.78242074E-03,
         0.47955201E-02, 0.31216557E-02, 0.26235427E-02, 0.22937225E-02,
        -0.88682777E-03, 0.15953343E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_200                                  =v_l_e_fp_200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]        *x34        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_fp_200                                  =v_l_e_fp_200                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]                *x53
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x21*x31    *x51
        +coeff[ 25]            *x42*x51
    ;
    v_l_e_fp_200                                  =v_l_e_fp_200                                  
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]        *x32*x42    
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]        *x31*x41*x52
        +coeff[ 31]    *x21        *x53
        +coeff[ 32]    *x23*x31*x41    
        +coeff[ 33]    *x23    *x42    
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_fp_200                                  =v_l_e_fp_200                                  
        +coeff[ 35]    *x22*x31*x41*x52
        +coeff[ 36]*x11*x22        *x53
        +coeff[ 37]*x11*x21*x34*x41    
        ;

    return v_l_e_fp_200                                  ;
}
float x_e_fp_175                                  (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.3559240E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.34341972E-01, 0.46614435E+00,-0.14245317E-01,-0.29735349E-01,
         0.36583669E-01,-0.78982497E-02, 0.41357558E-02,-0.13010424E-01,
        -0.79394085E-02,-0.17280439E-01,-0.65018465E-02,-0.68415916E-02,
         0.23142777E-02, 0.16120148E-02,-0.18884392E-02, 0.94284359E-02,
        -0.57072932E-03, 0.83815452E-03,-0.19339866E-02,-0.15772559E-02,
         0.23304329E-02, 0.25112403E-03, 0.14033085E-02, 0.22145903E-02,
         0.24152286E-02, 0.11714427E-02,-0.17799551E-02,-0.29031504E-02,
        -0.23369782E-02, 0.47441535E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_175                                  =v_x_e_fp_175                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]                *x53
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_fp_175                                  =v_x_e_fp_175                                  
        +coeff[ 17]*x11*x21            
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_175                                  =v_x_e_fp_175                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22    *x42*x51
        ;

    return v_x_e_fp_175                                  ;
}
float t_e_fp_175                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7637562E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.69491751E-02,-0.30372474E-01, 0.76243438E-01, 0.34653042E-02,
        -0.56855972E-02,-0.22276794E-02,-0.22713442E-02,-0.28293187E-03,
        -0.13535786E-02,-0.10594182E-02,-0.43275492E-03, 0.28570474E-03,
         0.18260266E-03,-0.27085180E-03, 0.47764328E-03, 0.17358309E-02,
        -0.97491808E-04,-0.55448251E-03, 0.23504665E-03, 0.48867008E-03,
         0.11165388E-03,-0.31941963E-03, 0.33491469E-03, 0.83363795E-03,
         0.48187171E-03,-0.79331099E-03,-0.61390217E-03,-0.35691357E-03,
         0.43139298E-03, 0.67807024E-03,-0.24737939E-03, 0.85583750E-04,
         0.20541980E-03,-0.13468678E-03,-0.12715609E-03, 0.25117467E-03,
        -0.11422738E-03,-0.24248612E-03, 0.82401493E-04,-0.27503300E-03,
        -0.11067408E-04,-0.20115427E-04, 0.70243608E-04, 0.11397807E-03,
         0.10653774E-03, 0.12849247E-03,-0.28210462E-03,-0.74922136E-04,
         0.89040201E-04,-0.18865323E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_fp_175                                  =v_t_e_fp_175                                  
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]                *x53
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_fp_175                                  =v_t_e_fp_175                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_175                                  =v_t_e_fp_175                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]        *x32        
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_fp_175                                  =v_t_e_fp_175                                  
        +coeff[ 35]        *x32    *x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]*x11    *x32    *x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]*x12                
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_fp_175                                  =v_t_e_fp_175                                  
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_fp_175                                  ;
}
float y_e_fp_175                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2996468E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.28560709E-03,-0.16918158E-01,-0.46508737E-01,-0.15354079E-01,
        -0.12023057E-01, 0.20599701E-01,-0.28623547E-02,-0.16110122E-01,
        -0.75140484E-02, 0.44030785E-02, 0.18802213E-02,-0.47206958E-02,
         0.48336937E-03,-0.19408688E-02,-0.23966781E-02, 0.10602366E-02,
         0.29038943E-02,-0.30530177E-02, 0.41067039E-03,-0.16027034E-02,
        -0.17684181E-02,-0.11071053E-02,-0.11457526E-02, 0.15523724E-02,
        -0.12526565E-02,-0.19539685E-03, 0.17914776E-03, 0.51562116E-03,
        -0.12278982E-03,-0.93313609E-03, 0.42871907E-03,-0.77232224E-03,
        -0.14955418E-03, 0.11052785E-02, 0.79275679E-03, 0.10536052E-02,
        -0.11783874E-02, 0.12883307E-03, 0.22424145E-03,-0.12055265E-02,
         0.41850188E-03,-0.29242707E-04,-0.36318845E-03, 0.21334982E-03,
        -0.30804080E-04,-0.92775718E-03, 0.11782097E-02, 0.10906840E-02,
        -0.62499457E-03, 0.24077884E-03, 0.73518777E-05, 0.47334132E-03,
         0.94555537E-04,-0.18173680E-04, 0.30081507E-03,-0.15875036E-02,
         0.80547697E-03, 0.43255501E-03, 0.76056545E-04, 0.10817067E-02,
         0.63634443E-03, 0.28371080E-04,-0.33006075E-03, 0.68200403E-03,
        -0.19667498E-02, 0.81529823E-03,-0.13237515E-02, 0.41278792E-04,
        -0.55813504E-03,-0.86381659E-03, 0.11575555E-02,-0.82551351E-05,
        -0.21977230E-04,-0.40686259E-03, 0.14669872E-03,-0.80374896E-03,
        -0.51809323E-03,-0.17125625E-03,-0.10635852E-03,-0.15419668E-03,
         0.13886635E-03, 0.10721636E-02,-0.37543444E-03, 0.27530693E-03,
        -0.10845190E-03, 0.99045508E-04,-0.11567315E-03,-0.11902805E-03,
         0.24739318E-03, 0.59742294E-03, 0.63970836E-03, 0.41901990E-03,
        -0.65378781E-03, 0.69352658E-03, 0.30004821E-03, 0.47252877E-03,
         0.59886667E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x23    *x41*x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]        *x33        
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]*x11    *x31    *x51
        +coeff[ 29]            *x43*x51
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x22*x33        
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 35]    *x22    *x41*x52
        +coeff[ 36]    *x21    *x41*x53
        +coeff[ 37]    *x23*x31    *x52
        +coeff[ 38]    *x22*x32*x41*x52
        +coeff[ 39]    *x21*x31    *x51
        +coeff[ 40]    *x21*x32*x41    
        +coeff[ 41]        *x31*x42*x51
        +coeff[ 42]*x11*x22    *x41    
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 44]    *x21    *x45    
        +coeff[ 45]    *x23*x31*x42    
        +coeff[ 46]    *x22*x31    *x52
        +coeff[ 47]    *x21*x31    *x53
        +coeff[ 48]    *x23*x32*x41*x51
        +coeff[ 49]        *x33*x42*x52
        +coeff[ 50]*x11*x21*x31        
        +coeff[ 51]    *x21    *x43    
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]    *x21    *x43*x51
        +coeff[ 55]    *x22*x32*x41    
        +coeff[ 56]    *x21*x31*x42*x51
        +coeff[ 57]    *x24*x31        
        +coeff[ 58]*x11    *x31    *x52
        +coeff[ 59]        *x31*x42*x52
        +coeff[ 60]    *x23*x31    *x51
        +coeff[ 61]    *x21*x33    *x51
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 62]        *x32*x41*x52
        +coeff[ 63]        *x33    *x52
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]    *x22*x32*x41*x51
        +coeff[ 67]    *x23    *x45    
        +coeff[ 68]        *x33    *x53
        +coeff[ 69]    *x24    *x43*x51
        +coeff[ 70]    *x23    *x41*x53
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 71]                *x51
        +coeff[ 72]*x12        *x41    
        +coeff[ 73]    *x23*x31        
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]    *x22    *x43    
        +coeff[ 76]    *x22*x31*x42    
        +coeff[ 77]    *x21*x31    *x52
        +coeff[ 78]*x11*x21*x33        
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 80]*x11    *x32*x41*x51
        +coeff[ 81]    *x23*x32*x41    
        +coeff[ 82]    *x21*x34*x41    
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]*x11*x21*x31    *x52
        +coeff[ 85]*x11        *x41*x53
        +coeff[ 86]*x11*x22*x33        
        +coeff[ 87]*x11    *x31    *x53
        +coeff[ 88]*x11*x21*x32*x41*x51
    ;
    v_y_e_fp_175                                  =v_y_e_fp_175                                  
        +coeff[ 89]    *x23    *x41*x52
        +coeff[ 90]    *x23    *x43*x51
        +coeff[ 91]    *x22    *x41*x53
        +coeff[ 92]    *x23*x31*x42*x51
        +coeff[ 93]    *x21*x33*x42*x51
        +coeff[ 94]    *x22    *x43*x52
        +coeff[ 95]    *x23*x33    *x51
        +coeff[ 96]    *x21*x31*x42*x53
        ;

    return v_y_e_fp_175                                  ;
}
float p_e_fp_175                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.5990999E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.67556699E-04, 0.18953674E-01,-0.31843204E-01, 0.16584950E-01,
        -0.87206317E-02,-0.98343939E-02, 0.45848382E-02, 0.76725795E-02,
        -0.66466123E-03,-0.10482282E-03, 0.10690032E-03, 0.53897807E-02,
         0.17014252E-02,-0.55845565E-03, 0.30274349E-02, 0.14250780E-02,
         0.61381032E-03, 0.11512412E-02, 0.64887381E-05,-0.17055815E-03,
         0.55409991E-03, 0.18049830E-02,-0.11592581E-02, 0.34836197E-03,
        -0.34309915E-03,-0.14610296E-02,-0.69655653E-03,-0.38774987E-03,
         0.12378523E-04,-0.20104124E-03,-0.70836884E-03, 0.83738800E-04,
         0.11837933E-02, 0.93598291E-03,-0.25176443E-03,-0.48423649E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_fp_175                                  =v_p_e_fp_175                                  
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_fp_175                                  =v_p_e_fp_175                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_fp_175                                  =v_p_e_fp_175                                  
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_fp_175                                  =v_p_e_fp_175                                  
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_175                                  ;
}
float l_e_fp_175                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.2574191E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.22658985E-01,-0.32650110E+00,-0.25535369E-01, 0.11884457E-01,
        -0.37218913E-01,-0.12535492E-01,-0.73731747E-02,-0.32196546E-02,
         0.92077572E-02,-0.49766782E-03,-0.34080164E-02, 0.22904112E-02,
         0.10020668E-01, 0.22934061E-02,-0.36608116E-02,-0.77851320E-03,
        -0.16519628E-02, 0.31459145E-02, 0.21223354E-02, 0.20343775E-02,
         0.30239017E-03,-0.11275357E-02,-0.86678599E-03, 0.79141458E-03,
         0.10010343E-02,-0.40587879E-03, 0.15893513E-02, 0.12863768E-02,
         0.12793235E-02, 0.41319979E-02,-0.12097525E-02, 0.14074332E-01,
         0.49116719E-02, 0.67063374E-02,-0.15798218E-02, 0.21277040E-02,
         0.30875714E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_fp_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_fp_175                                  =v_l_e_fp_175                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_fp_175                                  =v_l_e_fp_175                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]            *x42*x51
    ;
    v_l_e_fp_175                                  =v_l_e_fp_175                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]        *x31*x41*x54
        +coeff[ 31]    *x23*x31*x43    
        +coeff[ 32]    *x23    *x44    
        +coeff[ 33]    *x21*x32*x44    
        +coeff[ 34]    *x21*x32    *x54
    ;
    v_l_e_fp_175                                  =v_l_e_fp_175                                  
        +coeff[ 35]*x11*x24*x31    *x51
        +coeff[ 36]*x11*x22*x32*x41*x51
        ;

    return v_l_e_fp_175                                  ;
}
float x_e_fp_150                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.3437787E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.33119317E-01, 0.46568686E+00,-0.13838750E-01,-0.29648857E-01,
         0.36392160E-01,-0.78030555E-02, 0.39275186E-02,-0.13000206E-01,
        -0.81269732E-02,-0.16901113E-01,-0.58417600E-02,-0.65355781E-02,
         0.22552107E-02, 0.15079814E-02,-0.20185574E-02, 0.90527991E-02,
        -0.58292301E-03, 0.80975902E-03,-0.19533925E-02,-0.10758564E-02,
         0.23474977E-02, 0.26791528E-03, 0.13586296E-02, 0.20313989E-02,
         0.23230370E-02, 0.12736180E-02,-0.18062450E-02,-0.27958152E-02,
        -0.25652847E-02,-0.18741036E-02, 0.46011801E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_150                                  =v_x_e_fp_150                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]                *x53
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_fp_150                                  =v_x_e_fp_150                                  
        +coeff[ 17]*x11*x21            
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_150                                  =v_x_e_fp_150                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x22    *x42*x51
        ;

    return v_x_e_fp_150                                  ;
}
float t_e_fp_150                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7400908E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.63418322E-02,-0.30299313E-01, 0.76041162E-01, 0.33449791E-02,
        -0.56917742E-02,-0.13998894E-02,-0.22351199E-02,-0.21468794E-02,
        -0.27166237E-03,-0.10911808E-02,-0.34973011E-03, 0.30881714E-03,
         0.21800572E-03,-0.28225072E-03, 0.47838280E-03, 0.16933820E-02,
        -0.10140914E-03,-0.60121861E-03, 0.22525890E-03, 0.44917112E-03,
         0.12418815E-03,-0.35528588E-03, 0.35133422E-03, 0.80313708E-03,
         0.47963599E-03,-0.58141159E-03,-0.60657144E-03,-0.19644698E-03,
        -0.24144043E-03, 0.80842030E-04,-0.38986589E-04,-0.87056549E-04,
        -0.77803728E-04, 0.29017500E-03,-0.17338988E-03, 0.17484734E-03,
         0.96246695E-04,-0.28085825E-03,-0.47234949E-03, 0.51689311E-03,
         0.55767763E-04, 0.12550336E-03, 0.10536131E-03,-0.74324635E-04,
         0.12829046E-03,-0.26697610E-03, 0.16204210E-03,-0.27063335E-03,
         0.28938620E-03, 0.13993795E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_150                                  =v_t_e_fp_150                                  
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]                *x53
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_fp_150                                  =v_t_e_fp_150                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_150                                  =v_t_e_fp_150                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32        
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_fp_150                                  =v_t_e_fp_150                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x31*x41*x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_fp_150                                  =v_t_e_fp_150                                  
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x21*x32*x42    
        +coeff[ 46]        *x33*x41*x51
        +coeff[ 47]        *x32*x42*x51
        +coeff[ 48]    *x21*x31*x41*x52
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_fp_150                                  ;
}
float y_e_fp_150                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2109605E-03;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.20014230E-03,-0.16830351E-01,-0.47643777E-01,-0.16107585E-01,
        -0.12192406E-01, 0.20627115E-01,-0.28669322E-02,-0.16120665E-01,
        -0.74511352E-02, 0.43710656E-02, 0.19115811E-02,-0.44650077E-02,
         0.51336782E-03,-0.19665936E-02,-0.20859556E-02, 0.10860971E-02,
         0.29278621E-02,-0.30376019E-02, 0.43369984E-03,-0.16974347E-02,
        -0.18548749E-02,-0.11453826E-02,-0.10525823E-02,-0.15277404E-02,
        -0.18793876E-03, 0.25231269E-03, 0.57763525E-03,-0.66131965E-04,
        -0.88781741E-03, 0.37813789E-03,-0.78179402E-03,-0.73492498E-04,
         0.11127776E-02,-0.13744357E-02, 0.13723151E-02,-0.83586137E-03,
        -0.11304999E-02,-0.90657031E-05,-0.19887842E-03,-0.13803227E-02,
        -0.29542465E-04,-0.33283079E-03, 0.57109189E-03, 0.12934303E-02,
         0.11246263E-02, 0.80506230E-03, 0.14316227E-02, 0.42069738E-03,
        -0.75968110E-03, 0.73442975E-03, 0.20632190E-03,-0.22592375E-03,
         0.41638914E-03,-0.21352214E-04,-0.45564960E-03,-0.59969375E-05,
         0.17340906E-03, 0.11633916E-03, 0.10733764E-02, 0.71926060E-03,
         0.10253990E-02, 0.10406964E-02,-0.18285869E-02, 0.90508861E-03,
        -0.13164820E-02,-0.13316667E-03, 0.15502831E-03, 0.38414949E-03,
         0.42537437E-03, 0.19733525E-03,-0.26817288E-03, 0.89852285E-03,
        -0.85237425E-05, 0.22697646E-04,-0.26012105E-04,-0.41460776E-03,
         0.83986306E-04,-0.11683431E-02,-0.13728959E-03,-0.71685667E-04,
        -0.15916352E-03,-0.23349875E-03,-0.97697906E-04, 0.22947029E-03,
        -0.16659568E-03,-0.50645164E-03, 0.49744768E-03, 0.95950294E-03,
         0.10511267E-02, 0.96608739E-03,-0.61978976E-03,-0.26927874E-03,
        -0.53478131E-03, 0.61797309E-05,-0.30605137E-04, 0.11819393E-04,
         0.45901190E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]            *x41*x53
        +coeff[ 22]        *x31    *x53
        +coeff[ 23]    *x21*x32*x41*x51
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11        *x41*x51
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]*x11    *x31    *x51
        +coeff[ 28]            *x43*x51
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]        *x32*x41*x51
        +coeff[ 31]        *x33    *x51
        +coeff[ 32]    *x24    *x41    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 35]    *x23*x31*x42    
        +coeff[ 36]    *x21    *x41*x53
        +coeff[ 37]    *x23*x31    *x52
        +coeff[ 38]    *x22*x33    *x52
        +coeff[ 39]    *x21*x31    *x51
        +coeff[ 40]        *x31*x42*x51
        +coeff[ 41]*x11*x22    *x41    
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]        *x31*x42*x52
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]        *x33    *x52
        +coeff[ 46]    *x21*x31    *x53
        +coeff[ 47]    *x21*x32*x41*x52
        +coeff[ 48]        *x33    *x53
        +coeff[ 49]    *x23*x33    *x51
        +coeff[ 50]    *x24*x31    *x52
        +coeff[ 51]*x11*x21*x33    *x52
        +coeff[ 52]    *x21    *x43    
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 53]    *x21*x32*x41    
        +coeff[ 54]    *x22    *x43    
        +coeff[ 55]    *x21    *x41*x52
        +coeff[ 56]*x11*x21*x31    *x51
        +coeff[ 57]    *x21    *x43*x51
        +coeff[ 58]    *x21*x31*x42*x51
        +coeff[ 59]    *x23*x31    *x51
        +coeff[ 60]    *x22*x31    *x52
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 62]    *x22*x31*x42*x51
        +coeff[ 63]    *x23*x33        
        +coeff[ 64]    *x22*x32*x41*x51
        +coeff[ 65]*x11    *x31    *x53
        +coeff[ 66]*x11*x23    *x41*x51
        +coeff[ 67]    *x22    *x41*x53
        +coeff[ 68]    *x24*x33        
        +coeff[ 69]*x11*x22*x31    *x52
        +coeff[ 70]    *x22    *x45*x51
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 71]    *x23    *x41*x53
        +coeff[ 72]                *x51
        +coeff[ 73]    *x21    *x42    
        +coeff[ 74]*x12        *x41    
        +coeff[ 75]    *x23*x31        
        +coeff[ 76]            *x45    
        +coeff[ 77]    *x22*x31*x42    
        +coeff[ 78]    *x21*x31    *x52
        +coeff[ 79]*x11*x23    *x41    
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 80]*x11*x22    *x41*x51
        +coeff[ 81]        *x32*x41*x52
        +coeff[ 82]*x11    *x33    *x51
        +coeff[ 83]*x11*x22*x31*x42    
        +coeff[ 84]    *x24    *x41*x51
        +coeff[ 85]        *x34*x43    
        +coeff[ 86]    *x23    *x41*x52
        +coeff[ 87]    *x24*x31*x42    
        +coeff[ 88]    *x23    *x43*x51
    ;
    v_y_e_fp_150                                  =v_y_e_fp_150                                  
        +coeff[ 89]    *x23*x32*x43    
        +coeff[ 90]    *x24    *x43*x51
        +coeff[ 91]*x12*x21*x32*x41*x51
        +coeff[ 92]    *x23*x31    *x53
        +coeff[ 93]    *x21        *x51
        +coeff[ 94]*x11*x21*x31        
        +coeff[ 95]*x12    *x31        
        +coeff[ 96]*x11    *x32*x41    
        ;

    return v_y_e_fp_150                                  ;
}
float p_e_fp_150                                  (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.7379500E-04;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.62593776E-04, 0.19290756E-01,-0.31024083E-01, 0.16539935E-01,
        -0.86882506E-02,-0.97294943E-02, 0.74537415E-02,-0.69097691E-03,
        -0.15495245E-03, 0.16885564E-03, 0.14158158E-03, 0.54045198E-02,
         0.17086348E-02, 0.43177120E-02,-0.55172702E-03, 0.29892642E-02,
         0.13732855E-02, 0.62189525E-03, 0.11413126E-02, 0.76066917E-05,
        -0.16847611E-03, 0.55885763E-03, 0.17678927E-02,-0.11328261E-02,
         0.34385867E-03,-0.33558608E-03,-0.14064875E-02,-0.63374912E-03,
        -0.40042913E-03, 0.21537780E-05,-0.18897484E-03,-0.69604779E-03,
         0.75551841E-04,-0.14166890E-03, 0.11992583E-02, 0.95410296E-03,
        -0.27908752E-03,-0.46520159E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_150                                  =v_p_e_fp_150                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_150                                  =v_p_e_fp_150                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_150                                  =v_p_e_fp_150                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_fp_150                                  =v_p_e_fp_150                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_150                                  ;
}
float l_e_fp_150                                  (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2533006E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.17906528E-01,-0.32667753E+00,-0.25370138E-01, 0.11870374E-01,
        -0.36809132E-01,-0.12347812E-01,-0.67639640E-02, 0.10300052E-01,
         0.28929042E-03,-0.34985526E-02,-0.23569977E-02, 0.22677495E-02,
         0.90531129E-02,-0.84365992E-03,-0.43025105E-02,-0.85184065E-03,
         0.49024932E-02,-0.15640610E-02, 0.23534009E-02,-0.12542829E-02,
         0.19261077E-02, 0.29339161E-03,-0.10215257E-02, 0.12350114E-02,
         0.56586426E-03,-0.47188226E-03,-0.11531701E-02,-0.78027113E-03,
         0.41682008E-02, 0.29841952E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21    *x42    
    ;
    v_l_e_fp_150                                  =v_l_e_fp_150                                  
        +coeff[  8]        *x34        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_fp_150                                  =v_l_e_fp_150                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x53
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_fp_150                                  =v_l_e_fp_150                                  
        +coeff[ 26]        *x32*x42    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        ;

    return v_l_e_fp_150                                  ;
}
float x_e_fp_125                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3043668E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.29160859E-01, 0.46415907E+00,-0.13816101E-01,-0.29493369E-01,
         0.36310211E-01,-0.76828105E-02, 0.36360354E-02,-0.12814702E-01,
        -0.80748508E-02,-0.16584432E-01,-0.59095789E-02,-0.61877887E-02,
         0.22400285E-02, 0.16288966E-02,-0.20402728E-02, 0.16029027E-02,
         0.92374869E-02,-0.57384360E-03, 0.85003366E-03,-0.19097619E-02,
        -0.10482011E-02, 0.20681019E-02, 0.27075293E-03, 0.25772408E-02,
         0.25538397E-02, 0.14368288E-02,-0.23929584E-02,-0.22067339E-02,
        -0.16889808E-02, 0.49500442E-02, 0.13913229E-02,-0.30775887E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_125                                  =v_x_e_fp_125                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]                *x53
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_fp_125                                  =v_x_e_fp_125                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]        *x32    *x52
    ;
    v_x_e_fp_125                                  =v_x_e_fp_125                                  
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]    *x21*x31*x41*x53
        ;

    return v_x_e_fp_125                                  ;
}
float t_e_fp_125                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6516626E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54946565E-02,-0.30811690E-01, 0.75706445E-01, 0.33589415E-02,
        -0.56505990E-02,-0.15119106E-02,-0.22037460E-02,-0.21027450E-02,
        -0.26108150E-03,-0.10825400E-02,-0.43411664E-03, 0.32552524E-03,
        -0.33292777E-03, 0.46977069E-03, 0.16969802E-02,-0.97446966E-04,
         0.20274275E-03,-0.64555526E-03, 0.26408405E-03, 0.42432465E-03,
         0.85298634E-04,-0.33199441E-03, 0.38796512E-03, 0.84223348E-03,
         0.49242226E-03,-0.65194059E-03,-0.57275855E-03,-0.19474891E-03,
         0.60691335E-03,-0.21872685E-03, 0.89484289E-04,-0.12994753E-03,
        -0.10755139E-03, 0.28857993E-03,-0.19671855E-03, 0.26095301E-03,
         0.96261232E-04,-0.26544117E-03,-0.45588473E-03,-0.14109542E-04,
        -0.25241119E-04, 0.13544044E-03, 0.85948515E-04,-0.74086551E-04,
         0.24277013E-03,-0.41506133E-04, 0.17468545E-03, 0.17531174E-03,
        -0.69199501E-04, 0.18929350E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_125                                  =v_t_e_fp_125                                  
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_fp_125                                  =v_t_e_fp_125                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_125                                  =v_t_e_fp_125                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]        *x32        
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_fp_125                                  =v_t_e_fp_125                                  
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]*x12                
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x31*x41*x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_fp_125                                  =v_t_e_fp_125                                  
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x23    *x41    
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_fp_125                                  ;
}
float y_e_fp_125                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.9767291E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.81290222E-04,-0.16479686E-01,-0.48968032E-01,-0.16918210E-01,
        -0.12617419E-01, 0.20861587E-01,-0.27859344E-02,-0.16670229E-01,
        -0.75508403E-02, 0.44031688E-02, 0.19518280E-02,-0.48422799E-02,
         0.50205551E-03, 0.11083283E-02, 0.31157553E-02,-0.31242222E-02,
         0.17417367E-05, 0.33504507E-03, 0.45471152E-03,-0.16119727E-02,
        -0.20044043E-02,-0.20460356E-02,-0.11841886E-02,-0.11272765E-02,
         0.14734038E-02,-0.13783218E-02,-0.24124519E-02,-0.21977311E-03,
         0.16823529E-03, 0.46212788E-03,-0.10084893E-03,-0.92023425E-03,
         0.42359013E-03,-0.91909082E-03,-0.16183933E-03, 0.11105654E-02,
         0.10305783E-02, 0.11964436E-02,-0.97653957E-03, 0.41491479E-04,
         0.42111112E-03, 0.16755215E-04,-0.37167026E-03, 0.20663053E-03,
        -0.16337037E-02, 0.12272344E-02, 0.59513468E-03, 0.10408320E-02,
        -0.12408874E-03, 0.13842885E-02, 0.77245600E-03, 0.17797173E-02,
         0.43251817E-03, 0.55026496E-03,-0.90620539E-03,-0.16014619E-04,
        -0.39543276E-04,-0.15395867E-02,-0.72099618E-03,-0.62487990E-03,
         0.11951666E-03, 0.94819540E-03, 0.73799267E-04, 0.85112220E-03,
        -0.86535898E-03,-0.20688633E-02, 0.95908821E-03,-0.11193177E-02,
         0.49274706E-03, 0.54790429E-03,-0.62295771E-03,-0.77414553E-03,
         0.87542774E-03,-0.31120601E-03, 0.11209185E-04,-0.29979978E-04,
        -0.49979321E-03, 0.15512150E-03, 0.89628949E-04, 0.17305654E-03,
        -0.18389053E-03,-0.11382735E-03, 0.43794455E-03,-0.14648198E-03,
         0.15267174E-03, 0.85378502E-04, 0.33286487E-03,-0.14622710E-03,
        -0.14348456E-03, 0.98100165E-03, 0.33890552E-03,-0.47585499E-03,
        -0.68740087E-03,-0.13235533E-03,-0.47043816E-03,-0.73983578E-03,
        -0.52292133E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]        *x34*x41    
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]        *x31*x42    
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]            *x41*x53
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]    *x21*x32*x41*x51
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]        *x33        
        +coeff[ 28]*x11        *x41*x51
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]            *x43*x51
        +coeff[ 32]    *x21*x33        
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22*x33        
        +coeff[ 37]    *x23*x32*x41    
        +coeff[ 38]    *x21    *x41*x53
        +coeff[ 39]    *x23*x31    *x52
        +coeff[ 40]    *x21    *x43    
        +coeff[ 41]        *x31*x42*x51
        +coeff[ 42]*x11*x22    *x41    
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]        *x31*x42*x52
        +coeff[ 46]    *x21*x33    *x51
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]        *x32*x41*x52
        +coeff[ 49]    *x22*x31    *x52
        +coeff[ 50]        *x33    *x52
        +coeff[ 51]    *x21*x31    *x53
        +coeff[ 52]    *x21*x32*x41*x52
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 53]    *x23*x33    *x51
        +coeff[ 54]    *x21*x33    *x53
        +coeff[ 55]                *x51
        +coeff[ 56]*x11*x21*x31        
        +coeff[ 57]    *x21*x31    *x51
        +coeff[ 58]    *x22    *x43    
        +coeff[ 59]    *x22*x31*x42    
        +coeff[ 60]    *x21    *x43*x51
        +coeff[ 61]    *x21*x31*x42*x51
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 62]*x11    *x31    *x52
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]    *x22*x31*x42*x51
        +coeff[ 66]    *x23*x33        
        +coeff[ 67]    *x22*x32*x41*x51
        +coeff[ 68]    *x23    *x41*x52
        +coeff[ 69]    *x22    *x41*x53
        +coeff[ 70]        *x33    *x53
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 71]    *x24    *x43*x51
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x11*x21*x33    *x52
        +coeff[ 74]    *x21            
        +coeff[ 75]*x12        *x41    
        +coeff[ 76]    *x23*x31        
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]*x11*x21    *x41*x51
        +coeff[ 79]        *x33*x42    
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 80]    *x21*x31    *x52
        +coeff[ 81]*x11*x23    *x41    
        +coeff[ 82]    *x21*x32*x43    
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]*x11    *x32*x41*x51
        +coeff[ 85]*x11        *x41*x53
        +coeff[ 86]    *x21*x31*x42*x52
        +coeff[ 87]*x11*x22*x33        
        +coeff[ 88]*x11    *x31    *x53
    ;
    v_y_e_fp_125                                  =v_y_e_fp_125                                  
        +coeff[ 89]    *x23    *x43*x51
        +coeff[ 90]    *x22    *x43*x52
        +coeff[ 91]        *x32*x43*x52
        +coeff[ 92]    *x22*x31*x42*x52
        +coeff[ 93]*x11*x22*x33    *x51
        +coeff[ 94]    *x22*x33    *x52
        +coeff[ 95]    *x21*x32*x41*x53
        +coeff[ 96]    *x23*x31    *x53
        ;

    return v_y_e_fp_125                                  ;
}
float p_e_fp_125                                  (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.7507670E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.52420950E-04, 0.19631982E-01,-0.30205561E-01, 0.16237205E-01,
        -0.86672483E-02,-0.97424788E-02, 0.75562936E-02,-0.50019025E-03,
         0.10331002E-03, 0.13863054E-03, 0.50856420E-02, 0.17026936E-02,
         0.43807784E-02,-0.53898687E-03, 0.29175114E-02, 0.13890279E-02,
         0.62032422E-03, 0.11703765E-02,-0.38188984E-04,-0.15857561E-03,
         0.55703893E-03, 0.17822878E-02,-0.99475170E-03, 0.30654183E-03,
        -0.32608290E-03,-0.83140604E-03,-0.39152754E-03,-0.25920037E-05,
        -0.18685983E-03,-0.35050538E-03, 0.76401026E-04, 0.13892179E-02,
         0.96082973E-03,-0.27468384E-03,-0.43155224E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_fp_125                                  =v_p_e_fp_125                                  
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_fp_125                                  =v_p_e_fp_125                                  
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_fp_125                                  =v_p_e_fp_125                                  
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11        *x41*x51
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]            *x41*x53
        +coeff[ 34]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_125                                  ;
}
float l_e_fp_125                                  (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.2297842E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.16113885E-01,-0.33154738E+00,-0.25586462E-01, 0.11752394E-01,
        -0.38112581E-01,-0.12518955E-01,-0.68589556E-02, 0.90120733E-02,
         0.39798084E-04,-0.31773001E-02,-0.28044411E-02, 0.22469279E-02,
         0.55741435E-02,-0.92719105E-03,-0.42815185E-02,-0.89743390E-03,
         0.15378037E-02, 0.22771307E-02, 0.20134773E-02, 0.41387831E-02,
        -0.12168769E-02,-0.95120957E-03, 0.15697599E-02, 0.15922673E-02,
        -0.20869493E-02, 0.64078730E-03, 0.54844725E-03,-0.39499890E-03,
         0.11488305E-02,-0.97928476E-03, 0.82589546E-02, 0.10298360E-02,
         0.68870210E-02, 0.10494692E-02, 0.76893670E-02, 0.10593132E-02,
         0.90975611E-03,-0.16132831E-03, 0.65913093E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]    *x21    *x42    
    ;
    v_l_e_fp_125                                  =v_l_e_fp_125                                  
        +coeff[  8]        *x34        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]        *x32    *x51
    ;
    v_l_e_fp_125                                  =v_l_e_fp_125                                  
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23*x32        
        +coeff[ 24]    *x23            
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_fp_125                                  =v_l_e_fp_125                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11*x21        *x51
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]        *x31*x41*x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]        *x34*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]        *x33*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_l_e_fp_125                                  =v_l_e_fp_125                                  
        +coeff[ 35]*x12*x21    *x41*x51
        +coeff[ 36]*x12*x23    *x41    
        +coeff[ 37]    *x23*x32*x42    
        +coeff[ 38]    *x21*x32*x44    
        ;

    return v_l_e_fp_125                                  ;
}
float x_e_fp_100                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.3131838E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.30020889E-01,-0.75043961E-02, 0.46211469E+00,-0.13395958E-01,
        -0.29364165E-01, 0.36262047E-01, 0.31816724E-02,-0.12683867E-01,
        -0.80389436E-02,-0.16111193E-01,-0.59316666E-02,-0.23709547E-02,
         0.22355779E-02, 0.15584719E-02,-0.21839277E-02, 0.89007271E-02,
        -0.53304160E-03, 0.86164783E-03,-0.17584942E-02,-0.10526839E-02,
         0.15427592E-02,-0.49309465E-02, 0.22929166E-02, 0.26051390E-02,
         0.52121631E-02,-0.10563121E-03, 0.19577586E-02, 0.14204275E-02,
        -0.15131700E-02,-0.26879075E-02,-0.24011792E-02,-0.22927311E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_fp_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_fp_100                                  =v_x_e_fp_100                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]                *x53
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_fp_100                                  =v_x_e_fp_100                                  
        +coeff[ 17]*x11*x21            
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x21*x31*x41    
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]        *x31*x43*x51
    ;
    v_x_e_fp_100                                  =v_x_e_fp_100                                  
        +coeff[ 26]        *x31*x41*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x21*x32    *x52
        ;

    return v_x_e_fp_100                                  ;
}
float t_e_fp_100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6701418E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55862586E-02,-0.31252086E-01, 0.75184152E-01, 0.33659944E-02,
        -0.56349444E-02,-0.16544969E-02,-0.21823018E-02,-0.21122396E-02,
        -0.23945069E-03,-0.10949943E-02,-0.45858094E-03, 0.33739893E-03,
        -0.34758641E-03, 0.47334068E-03, 0.15551706E-02,-0.10951670E-03,
         0.17724138E-03,-0.66767796E-03, 0.39242813E-03, 0.30167046E-03,
         0.11489868E-03,-0.34472940E-03, 0.39902359E-03, 0.83113491E-03,
         0.49319683E-03,-0.72926161E-03,-0.57350547E-03,-0.39012605E-03,
        -0.27459420E-03, 0.61955396E-03,-0.20040915E-03, 0.80281068E-04,
        -0.11669550E-03,-0.10000645E-03, 0.32413824E-03, 0.16102783E-03,
        -0.19320207E-03, 0.40170003E-03, 0.99453187E-04,-0.21419000E-04,
        -0.44465552E-04, 0.62961670E-04, 0.20701971E-03, 0.13859366E-03,
        -0.10037498E-03, 0.22248563E-03, 0.12197952E-03, 0.16771999E-03,
         0.35843698E-03, 0.45827279E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_fp_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_fp_100                                  =v_t_e_fp_100                                  
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_fp_100                                  =v_t_e_fp_100                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_fp_100                                  =v_t_e_fp_100                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x53
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]        *x32        
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]        *x32    *x52
    ;
    v_t_e_fp_100                                  =v_t_e_fp_100                                  
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x21        *x53
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]*x11    *x32    *x52
        +coeff[ 39]*x11*x21        *x51
        +coeff[ 40]*x11*x21*x32        
        +coeff[ 41]*x11*x22        *x51
        +coeff[ 42]    *x23*x32        
        +coeff[ 43]        *x33*x41*x51
    ;
    v_t_e_fp_100                                  =v_t_e_fp_100                                  
        +coeff[ 44]        *x32*x42*x51
        +coeff[ 45]    *x21*x31*x41*x52
        +coeff[ 46]    *x21    *x42*x52
        +coeff[ 47]        *x31*x41*x53
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_fp_100                                  ;
}
float y_e_fp_100                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.5326298E-04;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.44007971E-04,-0.16109021E-01,-0.51187564E-01,-0.18119225E-01,
        -0.13148107E-01, 0.21030445E-01,-0.27296401E-02,-0.17297393E-01,
        -0.77852933E-02, 0.43964554E-02, 0.19674788E-02,-0.47734035E-02,
         0.48971520E-03, 0.11447117E-02, 0.31729811E-02,-0.32196338E-02,
         0.48196179E-03,-0.16223615E-02,-0.19917779E-02,-0.20010807E-02,
        -0.21245780E-02,-0.11793117E-02, 0.11575549E-02,-0.10979968E-02,
         0.19537455E-02,-0.17349415E-03, 0.22179849E-03,-0.11810620E-03,
        -0.78142394E-03,-0.63236756E-03, 0.44859067E-03,-0.90083643E-03,
        -0.10563598E-03,-0.16130551E-03, 0.50166127E-03, 0.10730812E-02,
         0.64659183E-03,-0.14213721E-02, 0.70844829E-03, 0.13796537E-02,
        -0.10170740E-02, 0.82262741E-04,-0.36195107E-03, 0.21493492E-03,
        -0.16604258E-02, 0.11538047E-02, 0.11484858E-02, 0.83518063E-03,
         0.15498120E-02, 0.11123647E-02, 0.16101152E-02,-0.15605829E-03,
        -0.71286253E-03, 0.61642623E-03,-0.48430313E-03,-0.11168630E-04,
        -0.37256024E-04,-0.13376197E-02, 0.43648167E-03, 0.54619653E-03,
        -0.76588051E-03, 0.10871382E-03, 0.79378260E-04,-0.10654795E-02,
         0.11531229E-03, 0.78871282E-03, 0.31703670E-03,-0.24484026E-02,
        -0.15329112E-02, 0.14688267E-03, 0.62387227E-03, 0.57086418E-03,
        -0.87193027E-03, 0.78611838E-03,-0.45297141E-03,-0.70977065E-03,
         0.62109548E-05,-0.21571755E-04, 0.88008215E-04,-0.48121993E-03,
        -0.16850262E-03,-0.88743334E-04,-0.16664242E-03,-0.66961558E-03,
        -0.12989307E-03, 0.71293704E-03,-0.53437671E-03,-0.10130340E-02,
         0.11173441E-02, 0.15614579E-03,-0.42533615E-03, 0.97640250E-05,
        -0.28935543E-04,-0.14116924E-03,-0.20718941E-04,-0.19337525E-04,
        -0.99960911E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_fp_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]        *x31    *x52
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x22    *x41*x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x22*x31    *x51
        +coeff[ 16]*x11        *x41    
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x31*x42    
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]    *x22*x31        
        +coeff[ 21]            *x41*x53
        +coeff[ 22]    *x21*x31*x42*x51
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]        *x33        
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]*x11    *x31    *x51
        +coeff[ 28]            *x43*x51
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]    *x21    *x43*x51
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22*x33        
        +coeff[ 37]    *x21*x32*x41*x51
        +coeff[ 38]    *x21*x32*x43    
        +coeff[ 39]    *x22    *x41*x52
        +coeff[ 40]    *x21    *x41*x53
        +coeff[ 41]        *x31*x42*x51
        +coeff[ 42]*x11*x22    *x41    
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]        *x31*x42*x52
        +coeff[ 46]    *x22*x31    *x52
        +coeff[ 47]        *x33    *x52
        +coeff[ 48]    *x23*x32*x41    
        +coeff[ 49]    *x23*x33        
        +coeff[ 50]    *x21*x31    *x53
        +coeff[ 51]    *x21    *x43*x52
        +coeff[ 52]        *x33    *x53
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 53]    *x21*x31*x42*x53
        +coeff[ 54]    *x23*x31    *x53
        +coeff[ 55]                *x51
        +coeff[ 56]*x11*x21*x31        
        +coeff[ 57]    *x21*x31    *x51
        +coeff[ 58]    *x21    *x43    
        +coeff[ 59]    *x21*x31*x42    
        +coeff[ 60]    *x22    *x43    
        +coeff[ 61]*x11*x21    *x41*x51
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 62]    *x21    *x41*x52
        +coeff[ 63]    *x22*x31*x42    
        +coeff[ 64]*x11    *x31    *x52
        +coeff[ 65]    *x23*x31    *x51
        +coeff[ 66]    *x21*x33    *x51
        +coeff[ 67]    *x22*x31*x42*x51
        +coeff[ 68]    *x22*x32*x41*x51
        +coeff[ 69]*x11*x24*x31        
        +coeff[ 70]    *x22    *x41*x53
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 71]    *x24*x33        
        +coeff[ 72]    *x23*x32*x41*x51
        +coeff[ 73]    *x23*x33    *x51
        +coeff[ 74]    *x22    *x45*x51
        +coeff[ 75]    *x24    *x43*x51
        +coeff[ 76]    *x21            
        +coeff[ 77]*x12        *x41    
        +coeff[ 78]*x11    *x31*x42    
        +coeff[ 79]        *x32*x43    
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 80]*x11*x23    *x41    
        +coeff[ 81]*x11*x21*x33        
        +coeff[ 82]*x11*x22    *x41*x51
        +coeff[ 83]    *x23*x31*x42    
        +coeff[ 84]*x11    *x31    *x53
        +coeff[ 85]    *x23    *x41*x52
        +coeff[ 86]    *x21*x31*x44*x51
        +coeff[ 87]    *x23*x31*x42*x51
        +coeff[ 88]    *x23    *x41*x53
    ;
    v_y_e_fp_100                                  =v_y_e_fp_100                                  
        +coeff[ 89]        *x33*x41*x53
        +coeff[ 90]    *x21*x33    *x53
        +coeff[ 91]    *x22            
        +coeff[ 92]        *x31*x41*x51
        +coeff[ 93]        *x31*x44    
        +coeff[ 94]*x11*x21*x32        
        +coeff[ 95]*x12    *x31*x41    
        +coeff[ 96]        *x34*x41    
        ;

    return v_y_e_fp_100                                  ;
}
float p_e_fp_100                                  (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2735001E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.25728464E-03,-0.10601105E-04, 0.20154065E-01,-0.28920965E-01,
         0.16305810E-01,-0.86213145E-02,-0.96213324E-02, 0.74658366E-02,
        -0.59379579E-03,-0.18008759E-04, 0.20557693E-03, 0.50497833E-02,
         0.17102140E-02, 0.43089651E-02,-0.51561813E-03, 0.28155744E-02,
         0.13440462E-02, 0.63120184E-03, 0.11741736E-02,-0.17827006E-04,
         0.55488676E-03, 0.17140367E-02,-0.11319238E-02,-0.14587180E-03,
         0.29740104E-03,-0.32700275E-03,-0.12562417E-02,-0.57363568E-03,
        -0.39513785E-03,-0.28216315E-04,-0.17503020E-03,-0.56576490E-03,
         0.65802938E-04,-0.20822392E-03, 0.13308042E-02, 0.99951017E-03,
        -0.27766140E-03,-0.47302025E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_fp_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_fp_100                                  =v_p_e_fp_100                                  
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_fp_100                                  =v_p_e_fp_100                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_fp_100                                  =v_p_e_fp_100                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_fp_100                                  =v_p_e_fp_100                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_fp_100                                  ;
}
float l_e_fp_100                                  (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2332197E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.15490077E-01,-0.33613151E+00,-0.25443926E-01, 0.11492429E-01,
        -0.39015159E-01,-0.12266657E-01,-0.68979282E-02, 0.23840212E-02,
         0.99221729E-02, 0.31049657E-03,-0.35162484E-02,-0.23320678E-02,
         0.66775288E-02,-0.16752818E-03,-0.45396914E-02,-0.46601507E-03,
         0.24158470E-02, 0.17147938E-02,-0.10321285E-02,-0.18554024E-02,
         0.39766957E-02,-0.11345191E-02, 0.21418084E-02,-0.85601769E-03,
         0.23852561E-02, 0.18726593E-02,-0.42522891E-03,-0.30516667E-03,
         0.12872504E-02, 0.61470605E-02, 0.39674165E-02, 0.40260497E-02,
        -0.58109954E-03,-0.17858131E-02, 0.97725843E-03, 0.31575884E-02,
        -0.53331398E-04,-0.21195814E-02,-0.15478383E-02, 0.61208596E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_fp_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]                *x52
        +coeff[  6]            *x42    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_fp_100                                  =v_l_e_fp_100                                  
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x34        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]    *x21        *x51
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_fp_100                                  =v_l_e_fp_100                                  
        +coeff[ 17]                *x53
        +coeff[ 18]        *x34    *x51
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x21*x32        
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]    *x21    *x42*x53
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_fp_100                                  =v_l_e_fp_100                                  
        +coeff[ 26]*x11*x21    *x41    
        +coeff[ 27]*x11*x21        *x51
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x13            *x51
        +coeff[ 33]*x11*x24            
        +coeff[ 34]*x11    *x31*x43    
    ;
    v_l_e_fp_100                                  =v_l_e_fp_100                                  
        +coeff[ 35]    *x22*x33*x41    
        +coeff[ 36]    *x23*x31*x42    
        +coeff[ 37]    *x21*x31*x42*x52
        +coeff[ 38]*x12    *x33*x41    
        +coeff[ 39]    *x23*x32*x42    
        ;

    return v_l_e_fp_100                                  ;
}
