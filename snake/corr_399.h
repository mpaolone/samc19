float x_e_corr_399                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.3848091E-01;
    float xmin[10]={
        -0.79290E+00, 0.76135E+00,-0.41271E-01,-0.61098E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.27231E+00, 0.11068E+01, 0.41950E-01, 0.62111E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.59112476E-03, 0.46095844E-01,-0.54501958E-01, 0.56674960E-02,
         0.10327784E-01,-0.82831755E-02, 0.38769796E-01,-0.38988646E-01,
         0.11427231E-02,-0.35070505E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;

//                 function

    float v_x_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x12            
        +coeff[  6]*x11*x21        
        +coeff[  7]    *x22        
    ;
    v_x_e_corr                                =v_x_e_corr                                
        +coeff[  8]*x11    *x31    
        +coeff[  9]    *x21*x31    
        ;

    return v_x_e_corr                                ;
}
float t_e_corr_399                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.1019933E-01;
    float xmin[10]={
        -0.79290E+00, 0.76135E+00,-0.41271E-01,-0.61098E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.27231E+00, 0.11068E+01, 0.41950E-01, 0.62111E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.29492658E-03, 0.13858880E-01,-0.14166279E-01, 0.13373195E-02,
         0.22083404E-02,-0.63726497E-02, 0.20334134E-01,-0.16525678E-01,
         0.52519967E-02,-0.75879162E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;

//                 function

    float v_t_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x12            
        +coeff[  6]*x11*x21        
        +coeff[  7]    *x22        
    ;
    v_t_e_corr                                =v_t_e_corr                                
        +coeff[  8]*x11    *x31    
        +coeff[  9]    *x21*x31    
        ;

    return v_t_e_corr                                ;
}
float y_e_corr_399                                (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.6749862E-02;
    float xmin[10]={
        -0.79290E+00, 0.76135E+00,-0.41271E-01,-0.61098E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.27231E+00, 0.11068E+01, 0.41950E-01, 0.62111E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.43760610E-04,-0.63045241E-04, 0.66886802E-04,-0.27375608E-02,
         0.14241731E-02, 0.10189189E-02,-0.28566022E-02, 0.19132703E-02,
        -0.15383082E-01, 0.21570688E-01,-0.64720116E-04,-0.20808054E-01,
         0.34232110E-01,-0.20642977E-01,-0.36028080E-01, 0.95897190E-01,
        -0.65903179E-01, 0.45436780E-04,-0.24147524E-01, 0.26970314E-01,
        -0.72329729E-02, 0.16765364E-02, 0.17197315E-03, 0.66171598E-03,
         0.19796269E-02, 0.20708941E-01, 0.14807446E-02,-0.59974205E-03,
        -0.12258533E-02,-0.94721036E-03,-0.26881185E-03,-0.13319618E-02,
         0.69794111E-01,-0.26910189E+00, 0.34542230E+00,-0.14851807E+00,
         0.98533335E-03,-0.64334454E-03,-0.11367636E-02,-0.36063374E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;

//                 function

    float v_y_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x12            
        +coeff[  6]*x11*x21        
        +coeff[  7]    *x22        
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[  8]*x11    *x31    
        +coeff[  9]    *x21*x31    
        +coeff[ 10]        *x32    
        +coeff[ 11]*x11        *x41
        +coeff[ 12]    *x21    *x41
        +coeff[ 13]    *x22*x31    
        +coeff[ 14]*x12        *x41
        +coeff[ 15]*x11*x21    *x41
        +coeff[ 16]    *x22    *x41
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[ 17]        *x32*x42
        +coeff[ 18]*x13*x21*x31    
        +coeff[ 19]*x12*x22*x31    
        +coeff[ 20]*x11*x23*x31    
        +coeff[ 21]*x13*x22*x31    
        +coeff[ 22]            *x42
        +coeff[ 23]*x11*x22        
        +coeff[ 24]*x12    *x31    
        +coeff[ 25]*x11*x21*x31    
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[ 26]    *x21*x32    
        +coeff[ 27]    *x21*x31*x41
        +coeff[ 28]        *x32*x41
        +coeff[ 29]        *x31*x42
        +coeff[ 30]            *x43
        +coeff[ 31]*x13    *x31    
        +coeff[ 32]*x13        *x41
        +coeff[ 33]*x12*x21    *x41
        +coeff[ 34]*x11*x22    *x41
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[ 35]    *x23    *x41
        +coeff[ 36]*x11    *x31*x43
        +coeff[ 37]    *x23        
        +coeff[ 38]*x11    *x32    
        +coeff[ 39]        *x33    
        ;

    return v_y_e_corr                                ;
}
float p_e_corr_399                                (float *x,int m){
    int ncoeff= 20;
    float avdat=  0.1151673E-02;
    float xmin[10]={
        -0.79290E+00, 0.76135E+00,-0.41271E-01,-0.61098E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.27231E+00, 0.11068E+01, 0.41950E-01, 0.62111E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
        -0.13527268E-03, 0.68724912E-03, 0.39211344E-02,-0.13710775E-02,
         0.30948594E-02,-0.22842041E-02, 0.13920476E-01,-0.19936476E-01,
        -0.21770627E-04, 0.24769334E-01,-0.34145910E-01, 0.29757860E-03,
         0.14056358E-01,-0.42760383E-01, 0.32242082E-01,-0.46512595E-03,
        -0.12933227E-02, 0.10353802E-01,-0.36021303E-01, 0.29044811E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;

//                 function

    float v_p_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]        *x31    
        +coeff[  2]            *x41
        +coeff[  3]*x12            
        +coeff[  4]*x11*x21        
        +coeff[  5]    *x22        
        +coeff[  6]*x11    *x31    
        +coeff[  7]    *x21*x31    
    ;
    v_p_e_corr                                =v_p_e_corr                                
        +coeff[  8]        *x32    
        +coeff[  9]*x11        *x41
        +coeff[ 10]    *x21    *x41
        +coeff[ 11]*x13            
        +coeff[ 12]*x12    *x31    
        +coeff[ 13]*x11*x21*x31    
        +coeff[ 14]    *x22*x31    
        +coeff[ 15]    *x21*x32    
        +coeff[ 16]        *x33    
    ;
    v_p_e_corr                                =v_p_e_corr                                
        +coeff[ 17]*x12        *x41
        +coeff[ 18]*x11*x21    *x41
        +coeff[ 19]    *x22    *x41
        ;

    return v_p_e_corr                                ;
}
