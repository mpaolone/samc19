float x_e_q1q2_1_1200                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.9193696E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.92149299E-03, 0.17735540E-02, 0.12214196E+00, 0.25056696E-02,
        -0.36610285E-03, 0.84327134E-04,-0.51151443E-03,-0.41074114E-03,
        -0.32482634E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_1200                              =v_x_e_q1q2_1_1200                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_1200                              ;
}
float t_e_q1q2_1_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.4849220E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.44832777E-05,-0.20031352E-02,-0.26829152E-02, 0.23065922E-02,
         0.73834002E-04,-0.18626526E-03,-0.27702760E-03,-0.32985807E-03,
         0.49527546E-06,-0.24021172E-03,-0.14991382E-03,-0.88161876E-04,
        -0.80312940E-03,-0.62746037E-03,-0.16785283E-04,-0.41106535E-03,
        -0.17091783E-04,-0.13394460E-04, 0.49528753E-04, 0.12385563E-04,
         0.13267249E-04,-0.49571961E-03, 0.23077291E-04,-0.45994566E-05,
         0.21118026E-06,-0.90661006E-05, 0.67731803E-05,-0.60540115E-05,
         0.59535353E-06,-0.82212227E-05,-0.22035147E-04, 0.78893199E-05,
         0.22989540E-04, 0.81885573E-05, 0.19180036E-05,-0.62299150E-04,
        -0.23953464E-03,-0.50340543E-04, 0.13345332E-04, 0.98648943E-05,
        -0.19107920E-04, 0.21482456E-04, 0.13394009E-04, 0.18129043E-04,
        -0.20643181E-05,-0.12143624E-05, 0.30039439E-05, 0.35105079E-05,
         0.26690927E-05,-0.24849519E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_1200                              =v_t_e_q1q2_1_1200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_1200                              =v_t_e_q1q2_1_1200                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21*x31    *x52
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x32*x42*x51
        +coeff[ 23]*x11*x21            
        +coeff[ 24]    *x21    *x41    
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_1_1200                              =v_t_e_q1q2_1_1200                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]*x11*x22        *x51
        +coeff[ 31]    *x23        *x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]*x11*x21        *x52
        +coeff[ 34]*x11*x22*x32        
    ;
    v_t_e_q1q2_1_1200                              =v_t_e_q1q2_1_1200                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]    *x21*x33*x41    
        +coeff[ 37]*x11*x22    *x42    
        +coeff[ 38]*x11*x21*x31*x41*x51
        +coeff[ 39]*x12*x21        *x52
        +coeff[ 40]    *x22*x32*x42    
        +coeff[ 41]*x13*x22        *x51
        +coeff[ 42]*x12*x22*x31    *x51
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_q1q2_1_1200                              =v_t_e_q1q2_1_1200                              
        +coeff[ 44]        *x31*x41    
        +coeff[ 45]            *x41*x51
        +coeff[ 46]*x11*x21*x31        
        +coeff[ 47]    *x22*x31        
        +coeff[ 48]*x11*x21    *x41    
        +coeff[ 49]        *x31*x42    
        ;

    return v_t_e_q1q2_1_1200                              ;
}
float y_e_q1q2_1_1200                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.9296244E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.29430976E-04, 0.13209207E+00, 0.88304594E-01,-0.16025363E-02,
        -0.14640907E-02,-0.53156953E-04,-0.34310691E-04,-0.14393483E-03,
         0.21651384E-03,-0.97301563E-04, 0.14004239E-03, 0.73124393E-04,
         0.70962284E-04, 0.62540166E-04,-0.26347904E-03,-0.13029563E-04,
         0.97536813E-05,-0.59301259E-04,-0.21150552E-03, 0.58839373E-05,
         0.11137945E-04,-0.47769514E-04, 0.16875729E-05, 0.29050372E-04,
        -0.14676986E-03,-0.52398438E-04,-0.34564022E-04, 0.24345032E-04,
        -0.61035971E-04, 0.26845251E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_1200                              =v_y_e_q1q2_1_1200                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_1_1200                              =v_y_e_q1q2_1_1200                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]    *x24*x32*x41    
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32*x41    
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]        *x32*x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_1_1200                              =v_y_e_q1q2_1_1200                              
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]*x11*x21*x31*x41*x51
        +coeff[ 28]        *x32*x43*x51
        +coeff[ 29]    *x24    *x41*x51
        ;

    return v_y_e_q1q2_1_1200                              ;
}
float p_e_q1q2_1_1200                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.8039842E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.47915844E-04, 0.32424614E-01, 0.66862345E-01,-0.15770235E-02,
         0.60954312E-05,-0.14037741E-02,-0.31729380E-03,-0.37727121E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_1200                              ;
}
float l_e_q1q2_1_1200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1867811E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19015425E-02,-0.35802801E-02,-0.71918359E-03,-0.24158149E-02,
        -0.29001129E-02,-0.27149872E-03, 0.71146316E-03,-0.13725060E-02,
         0.10948851E-03, 0.43138571E-04, 0.37547314E-04, 0.87124718E-04,
        -0.31579880E-03,-0.10765877E-03,-0.39403880E-03, 0.90722606E-04,
        -0.43103454E-03,-0.46866012E-03,-0.11354814E-02,-0.11474086E-03,
         0.41124635E-03, 0.29502530E-03,-0.25578099E-03,-0.37831449E-03,
        -0.17338850E-03, 0.15673038E-03, 0.66335889E-03,-0.19315880E-02,
        -0.14161188E-02,-0.10919756E-02,-0.76748023E-03,-0.24776571E-03,
         0.20169683E-02, 0.10482179E-02, 0.75217726E-03, 0.59582130E-03,
         0.39666942E-02, 0.47659082E-03,-0.66305324E-03, 0.13252944E-03,
        -0.63493534E-03, 0.33199193E-03,-0.26049819E-02, 0.17534402E-02,
         0.93491439E-03,-0.93322428E-03,-0.19301536E-02, 0.13429786E-02,
         0.46950829E-03,-0.76438667E-03, 0.10688138E-02, 0.71873376E-03,
         0.91862847E-03,-0.49105415E-03,-0.35896723E-03,-0.45406169E-03,
         0.11782245E-02, 0.30411393E-02, 0.13434704E-02,-0.16110610E-02,
        -0.10632720E-02,-0.23885618E-02, 0.14280668E-02,-0.13299668E-02,
         0.14959206E-02, 0.97291643E-03, 0.50354260E-03,-0.50953217E-03,
         0.44239810E-03,-0.22169307E-02,-0.10320756E-02,-0.27802398E-02,
        -0.51723857E-03, 0.27199183E-02, 0.55487710E-03,-0.21384472E-02,
        -0.23216596E-02,-0.24823463E-02, 0.13713638E-02, 0.51055453E-03,
        -0.18374273E-02,-0.22898796E-02, 0.14945506E-02, 0.32908373E-03,
         0.58595656E-03, 0.89009688E-03,-0.49301816E-04,-0.78456076E-04,
         0.66481269E-04,-0.14108616E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21        *x51
        +coeff[  6]*x11*x23*x32        
        +coeff[  7]*x12    *x32*x41*x51
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[  8]        *x31        
        +coeff[  9]*x11*x21            
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21    *x41*x51
        +coeff[ 15]                *x53
        +coeff[ 16]        *x33*x41    
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 17]            *x44    
        +coeff[ 18]*x11    *x32*x41    
        +coeff[ 19]*x11    *x31*x42    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11*x22        *x51
        +coeff[ 22]*x11    *x31*x41*x51
        +coeff[ 23]*x11*x21        *x52
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]*x12*x22            
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 26]*x12    *x31*x41    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]        *x33*x41*x51
        +coeff[ 30]        *x32*x42*x51
        +coeff[ 31]    *x21    *x43*x51
        +coeff[ 32]        *x31*x43*x51
        +coeff[ 33]    *x22*x31    *x52
        +coeff[ 34]        *x33    *x52
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 35]*x11    *x34        
        +coeff[ 36]*x11*x22*x31*x41    
        +coeff[ 37]*x12*x21*x32        
        +coeff[ 38]*x12*x21    *x42    
        +coeff[ 39]*x12    *x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x24    *x41*x51
        +coeff[ 42]        *x31*x44*x51
        +coeff[ 43]        *x31*x42*x53
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 44]*x11*x24    *x41    
        +coeff[ 45]*x11*x22    *x43    
        +coeff[ 46]*x11*x21*x31*x42*x51
        +coeff[ 47]*x11    *x32*x41*x52
        +coeff[ 48]*x11*x21*x31    *x53
        +coeff[ 49]*x11*x21    *x41*x53
        +coeff[ 50]*x11    *x31*x41*x53
        +coeff[ 51]*x12*x21    *x42*x51
        +coeff[ 52]*x12*x22        *x52
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 53]*x12    *x31    *x53
        +coeff[ 54]*x12            *x54
        +coeff[ 55]    *x24*x33        
        +coeff[ 56]    *x23    *x43*x51
        +coeff[ 57]        *x32*x44*x51
        +coeff[ 58]    *x24    *x41*x52
        +coeff[ 59]        *x33    *x54
        +coeff[ 60]    *x22    *x41*x54
        +coeff[ 61]*x11*x24*x31*x41    
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 62]*x11    *x34*x42    
        +coeff[ 63]*x11    *x33*x42*x51
        +coeff[ 64]*x11    *x33*x41*x52
        +coeff[ 65]*x11*x22    *x42*x52
        +coeff[ 66]*x11    *x33    *x53
        +coeff[ 67]*x11        *x43*x53
        +coeff[ 68]*x12*x23*x31    *x51
        +coeff[ 69]*x12    *x32*x42*x51
        +coeff[ 70]*x12    *x31*x41*x53
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 71]*x13*x22*x31*x41    
        +coeff[ 72]*x13*x22    *x42    
        +coeff[ 73]    *x23*x34    *x51
        +coeff[ 74]    *x23*x33*x41*x51
        +coeff[ 75]    *x23*x32*x42*x51
        +coeff[ 76]    *x21*x34*x42*x51
        +coeff[ 77]        *x34*x43*x51
        +coeff[ 78]    *x21*x32*x44*x51
        +coeff[ 79]    *x24*x31*x41*x52
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 80]    *x23*x32    *x53
        +coeff[ 81]    *x23*x31*x41*x53
        +coeff[ 82]        *x34*x41*x53
        +coeff[ 83]*x13            *x54
        +coeff[ 84]        *x34    *x54
        +coeff[ 85]        *x33*x41*x54
        +coeff[ 86]*x11                
        +coeff[ 87]    *x21        *x51
        +coeff[ 88]        *x31    *x51
    ;
    v_l_e_q1q2_1_1200                              =v_l_e_q1q2_1_1200                              
        +coeff[ 89]            *x41*x51
        ;

    return v_l_e_q1q2_1_1200                              ;
}
float x_e_q1q2_1_1100                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.8172721E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.80359000E-03, 0.12214934E+00, 0.17748980E-02, 0.25039450E-02,
        -0.36444864E-03, 0.84007093E-04,-0.51828066E-03,-0.41177572E-03,
        -0.33018499E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_1100                              =v_x_e_q1q2_1_1100                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_1100                              ;
}
float t_e_q1q2_1_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2424777E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.23080447E-04,-0.20021934E-02,-0.26852011E-02, 0.23098169E-02,
         0.72509152E-04,-0.17748259E-03,-0.26445708E-03,-0.31517181E-03,
         0.47028725E-05,-0.26883124E-03,-0.13150726E-03,-0.80047212E-04,
        -0.84437587E-03,-0.64242975E-03,-0.20568543E-04,-0.51017466E-03,
        -0.41824087E-03,-0.30937612E-04,-0.20386500E-04, 0.66088680E-04,
         0.37536327E-04, 0.12411756E-04,-0.22645303E-03, 0.13668388E-04,
         0.35358033E-04,-0.45854495E-05,-0.52641070E-06,-0.68009081E-06,
         0.25257268E-05,-0.21933041E-04, 0.41232224E-05, 0.87692597E-05,
         0.79283463E-05,-0.68856789E-05, 0.92139226E-05,-0.59308936E-05,
        -0.18276975E-04, 0.12431216E-04,-0.15311782E-04,-0.13409287E-04,
        -0.17776412E-04, 0.10447925E-04,-0.24532175E-04,-0.24136252E-05,
         0.48442053E-05,-0.18732313E-04,-0.10151329E-04,-0.81541020E-05,
        -0.23031133E-04, 0.17384269E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_1100                              =v_t_e_q1q2_1_1100                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x32*x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_1_1100                              =v_t_e_q1q2_1_1100                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]*x13*x23            
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]    *x22            
    ;
    v_t_e_q1q2_1_1100                              =v_t_e_q1q2_1_1100                              
        +coeff[ 26]*x13                
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x22*x31        
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]        *x31*x42    
        +coeff[ 31]*x12*x22            
        +coeff[ 32]*x12    *x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_q1q2_1_1100                              =v_t_e_q1q2_1_1100                              
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x11*x21*x33        
        +coeff[ 38]    *x22*x33        
        +coeff[ 39]*x12*x21    *x42    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]*x11*x21*x31*x41*x51
        +coeff[ 43]*x11*x21    *x42*x51
    ;
    v_t_e_q1q2_1_1100                              =v_t_e_q1q2_1_1100                              
        +coeff[ 44]    *x21*x31*x41*x52
        +coeff[ 45]    *x21    *x42*x52
        +coeff[ 46]*x11*x21        *x53
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]*x12*x21*x31*x41*x51
        +coeff[ 49]*x11*x22    *x42*x51
        ;

    return v_t_e_q1q2_1_1100                              ;
}
float y_e_q1q2_1_1100                              (float *x,int m){
    int ncoeff= 34;
    float avdat=  0.1909245E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
        -0.26634222E-03, 0.13207282E+00, 0.88335827E-01,-0.16103723E-02,
        -0.14741928E-02,-0.11234705E-03, 0.49643142E-04,-0.19661558E-03,
         0.17277220E-03,-0.95444513E-04, 0.12123847E-03, 0.66443921E-04,
         0.70714908E-04, 0.45019890E-04,-0.19315339E-03,-0.18995586E-04,
         0.37519370E-04, 0.30261258E-05,-0.24412402E-04, 0.92531609E-05,
        -0.12574135E-04, 0.23360275E-06, 0.19200941E-04, 0.21546592E-04,
        -0.10426947E-04,-0.74890973E-04,-0.63499472E-04,-0.28058254E-04,
        -0.29512013E-04, 0.40829698E-04,-0.16529028E-04, 0.12491741E-04,
         0.27700875E-04,-0.23308497E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_1100                              =v_y_e_q1q2_1_1100                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_1_1100                              =v_y_e_q1q2_1_1100                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]        *x32*x41*x51
        +coeff[ 24]        *x31*x41*x52
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_1_1100                              =v_y_e_q1q2_1_1100                              
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]            *x43*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]    *x21*x33*x42    
        +coeff[ 31]*x11        *x42*x52
        +coeff[ 32]        *x33    *x52
        +coeff[ 33]*x11*x21*x32*x42    
        ;

    return v_y_e_q1q2_1_1100                              ;
}
float p_e_q1q2_1_1100                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.1688135E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.56821151E-04, 0.32430511E-01, 0.66848576E-01,-0.15777173E-02,
        -0.29409694E-05,-0.14014442E-02,-0.31518086E-03,-0.37158636E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_1100                              ;
}
float l_e_q1q2_1_1100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1854687E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17801789E-02,-0.43032069E-04,-0.34584915E-02,-0.10356037E-03,
        -0.29840078E-02,-0.57584920E-03,-0.10185256E-02, 0.57746528E-03,
         0.11762308E-02,-0.27492654E-03,-0.84008661E-03,-0.44996123E-03,
         0.47276327E-02, 0.73892606E-03,-0.95182640E-03, 0.57606347E-03,
        -0.18568488E-02, 0.71293651E-03,-0.44171750E-02, 0.23408295E-02,
        -0.13578939E-03,-0.18389614E-04,-0.22150641E-02, 0.27191508E-03,
        -0.23548095E-03,-0.24633016E-03, 0.48712162E-04,-0.20001235E-03,
         0.40294948E-04,-0.21283432E-03, 0.12195244E-02, 0.21528649E-04,
         0.85875852E-03,-0.58548387E-04,-0.55593922E-03, 0.31306055E-04,
        -0.88761572E-03,-0.24029484E-03, 0.87551802E-03,-0.43883387E-03,
         0.33168198E-03, 0.35017508E-03, 0.11095262E-03, 0.62765414E-03,
         0.48337536E-03, 0.56318537E-03, 0.27394254E-03, 0.21554167E-03,
         0.22401707E-05,-0.11157572E-02,-0.28954845E-03,-0.10160941E-02,
        -0.17190114E-04, 0.11449314E-03, 0.94186666E-03,-0.66846254E-03,
        -0.98649866E-03, 0.42360104E-03,-0.16497609E-02, 0.26340733E-03,
        -0.94782526E-03, 0.11455285E-02, 0.86578861E-03, 0.61684614E-03,
        -0.53225772E-03,-0.98761660E-03, 0.78764785E-03,-0.45319673E-03,
         0.58724894E-03, 0.10622460E-02, 0.12083135E-02,-0.57430734E-03,
        -0.19607993E-02, 0.16157704E-02, 0.17490389E-02,-0.17197371E-02,
        -0.13262428E-02, 0.16596472E-02, 0.30133931E-02, 0.76185155E-03,
        -0.68428757E-03, 0.11225676E-02,-0.77068026E-03, 0.66236238E-03,
        -0.10381433E-02, 0.53246989E-03,-0.98773243E-03, 0.55288913E-03,
        -0.91374037E-03,-0.18174114E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[  8]        *x31*x41*x52
        +coeff[  9]    *x21*x31    *x53
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x22*x34        
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]        *x33*x43    
        +coeff[ 16]    *x22    *x42*x52
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 17]*x13*x24            
        +coeff[ 18]    *x24*x33*x41    
        +coeff[ 19]        *x34*x42*x52
        +coeff[ 20]*x11                
        +coeff[ 21]    *x21*x31        
        +coeff[ 22]        *x31*x41    
        +coeff[ 23]                *x52
        +coeff[ 24]*x12                
        +coeff[ 25]    *x22*x31        
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]            *x43    
        +coeff[ 28]    *x22        *x51
        +coeff[ 29]        *x32    *x51
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]*x11    *x32        
        +coeff[ 32]    *x21*x31*x42    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]        *x32    *x52
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 35]*x11*x22*x31        
        +coeff[ 36]*x11*x21*x32        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]*x11*x21*x31    *x51
        +coeff[ 39]*x11    *x32    *x51
        +coeff[ 40]*x11            *x53
        +coeff[ 41]*x12*x22            
        +coeff[ 42]*x13*x21            
        +coeff[ 43]        *x32*x43    
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 44]    *x22*x32    *x51
        +coeff[ 45]        *x33*x41*x51
        +coeff[ 46]    *x21    *x43*x51
        +coeff[ 47]            *x42*x53
        +coeff[ 48]    *x21        *x54
        +coeff[ 49]        *x31    *x54
        +coeff[ 50]*x11*x21*x33        
        +coeff[ 51]*x11    *x32*x42    
        +coeff[ 52]*x11    *x31*x43    
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 53]*x12    *x31    *x52
        +coeff[ 54]*x13    *x32        
        +coeff[ 55]*x13    *x31*x41    
        +coeff[ 56]    *x21*x31*x42*x52
        +coeff[ 57]    *x21    *x43*x52
        +coeff[ 58]        *x31*x43*x52
        +coeff[ 59]    *x21*x31    *x54
        +coeff[ 60]*x11*x24*x31        
        +coeff[ 61]*x11    *x34    *x51
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 62]*x11*x22*x31    *x52
        +coeff[ 63]*x11*x21*x31*x41*x52
        +coeff[ 64]*x11*x22        *x53
        +coeff[ 65]*x11*x21*x31    *x53
        +coeff[ 66]*x12*x22*x31    *x51
        +coeff[ 67]*x12*x21*x32    *x51
        +coeff[ 68]*x12*x21*x31    *x52
        +coeff[ 69]*x13*x21*x32        
        +coeff[ 70]    *x22*x31*x42*x52
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 71]*x13            *x53
        +coeff[ 72]*x11    *x33*x43    
        +coeff[ 73]*x11*x23    *x42*x51
        +coeff[ 74]*x11*x21*x31*x43*x51
        +coeff[ 75]*x11*x22*x32    *x52
        +coeff[ 76]*x11*x22*x31*x41*x52
        +coeff[ 77]*x11*x22    *x42*x52
        +coeff[ 78]*x11    *x31*x43*x52
        +coeff[ 79]*x11        *x44*x52
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 80]*x11    *x31*x41*x54
        +coeff[ 81]*x12*x21*x33*x41    
        +coeff[ 82]*x12    *x34*x41    
        +coeff[ 83]*x12*x22    *x43    
        +coeff[ 84]*x12*x22*x31*x41*x51
        +coeff[ 85]*x12*x23        *x52
        +coeff[ 86]*x12    *x33    *x52
        +coeff[ 87]*x13*x22*x31*x41    
        +coeff[ 88]*x13*x22    *x42    
    ;
    v_l_e_q1q2_1_1100                              =v_l_e_q1q2_1_1100                              
        +coeff[ 89]*x13    *x32*x42    
        ;

    return v_l_e_q1q2_1_1100                              ;
}
float x_e_q1q2_1_1000                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.5749500E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.54040976E-03, 0.12212853E+00, 0.17744467E-02, 0.25070971E-02,
        -0.36541483E-03, 0.82412240E-04,-0.50801248E-03,-0.41044404E-03,
        -0.31372573E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_1000                              =v_x_e_q1q2_1_1000                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_1000                              ;
}
float t_e_q1q2_1_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1601387E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.15229554E-04,-0.20045645E-02,-0.26731580E-02, 0.23070083E-02,
         0.73411851E-04,-0.17736573E-03,-0.29542268E-03,-0.32947972E-03,
        -0.89310270E-05,-0.26437055E-03,-0.14258109E-03,-0.91403846E-04,
        -0.82376157E-03,-0.63762238E-03,-0.15895446E-04,-0.39295468E-03,
        -0.19089885E-04,-0.16981401E-04, 0.61647857E-04, 0.30875384E-04,
        -0.18972118E-03,-0.46556411E-03,-0.10491060E-04,-0.10596639E-04,
         0.39746177E-04,-0.17392227E-06, 0.75241178E-05,-0.60073708E-05,
        -0.61035767E-05, 0.19046574E-04,-0.11530351E-04,-0.95954319E-05,
        -0.52572144E-04,-0.30413008E-04,-0.88491015E-05,-0.93944282E-05,
        -0.10864805E-04, 0.15257610E-04, 0.13012630E-04, 0.85260608E-05,
        -0.14479017E-04, 0.26220347E-04, 0.16904092E-04, 0.13330725E-04,
        -0.19349174E-04,-0.86390728E-05, 0.53807812E-05,-0.25360719E-05,
        -0.24095154E-05, 0.32671462E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_1000                              =v_t_e_q1q2_1_1000                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_1000                              =v_t_e_q1q2_1_1000                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x11    *x32    *x52
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x21*x32*x42*x51
        +coeff[ 25]        *x31    *x51
    ;
    v_t_e_q1q2_1_1000                              =v_t_e_q1q2_1_1000                              
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]*x11    *x33        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x13*x21*x31        
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x12*x21*x31    *x51
    ;
    v_t_e_q1q2_1_1000                              =v_t_e_q1q2_1_1000                              
        +coeff[ 35]*x11*x22    *x41*x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]*x12*x23        *x51
        +coeff[ 40]    *x22*x33    *x51
        +coeff[ 41]*x13*x21    *x41*x51
        +coeff[ 42]*x11*x22    *x42*x51
        +coeff[ 43]*x12*x21        *x53
    ;
    v_t_e_q1q2_1_1000                              =v_t_e_q1q2_1_1000                              
        +coeff[ 44]*x11*x21    *x41*x53
        +coeff[ 45]    *x22    *x41*x53
        +coeff[ 46]*x11*x21*x31        
        +coeff[ 47]            *x41*x52
        +coeff[ 48]*x11*x23            
        +coeff[ 49]*x13    *x31        
        ;

    return v_t_e_q1q2_1_1000                              ;
}
float y_e_q1q2_1_1000                              (float *x,int m){
    int ncoeff= 27;
    float avdat=  0.2159327E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
        -0.18752652E-03, 0.13200885E+00, 0.88328011E-01,-0.16057102E-02,
        -0.14764684E-02,-0.10170612E-03, 0.44145811E-04,-0.19129876E-03,
         0.18454366E-03,-0.10472006E-03, 0.11423210E-03, 0.55132768E-04,
         0.69088688E-04, 0.63960695E-04,-0.20892579E-03,-0.13011088E-04,
         0.83891828E-05,-0.64116772E-04, 0.20732271E-05, 0.11104787E-04,
         0.40083065E-04, 0.42464948E-04,-0.14734880E-04,-0.40656447E-04,
         0.32780499E-04,-0.38546099E-04, 0.25021727E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_1000                              =v_y_e_q1q2_1_1000                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_1_1000                              =v_y_e_q1q2_1_1000                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x21*x31*x44    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_1_1000                              =v_y_e_q1q2_1_1000                              
        +coeff[ 26]    *x23*x32*x41    
        ;

    return v_y_e_q1q2_1_1000                              ;
}
float p_e_q1q2_1_1000                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.3779305E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.23685889E-04, 0.32428142E-01, 0.66810690E-01,-0.15783674E-02,
         0.63885277E-05,-0.14062104E-02,-0.31552886E-03,-0.37372732E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_1000                              ;
}
float l_e_q1q2_1_1000                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1837230E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18728678E-02,-0.33842544E-02,-0.10957161E-03,-0.35137259E-02,
        -0.36047764E-04,-0.55439526E-03,-0.11286631E-02,-0.37932102E-03,
         0.65721455E-03, 0.59706345E-03, 0.36428636E-03, 0.47623942E-03,
         0.14867753E-02, 0.63699833E-03,-0.21977257E-02,-0.43155826E-03,
        -0.24655502E-03, 0.57748688E-03, 0.65720774E-03,-0.52939613E-05,
         0.19311639E-03, 0.24522902E-03, 0.59121224E-03,-0.75737311E-03,
         0.44190462E-03, 0.51780971E-03,-0.41242817E-03, 0.48088489E-03,
        -0.76847326E-04, 0.21077314E-03, 0.25366165E-03,-0.77567447E-03,
         0.41911623E-03,-0.90668048E-03,-0.98337780E-03, 0.42178555E-03,
        -0.52269414E-03, 0.34774654E-03, 0.42965368E-03,-0.21037571E-03,
         0.10524857E-02,-0.15480076E-02, 0.44978407E-03, 0.30850535E-03,
         0.11786863E-02, 0.12394532E-02, 0.18297507E-03,-0.99223468E-03,
         0.91468322E-03,-0.23374497E-03,-0.29711967E-03,-0.72596874E-03,
         0.71304123E-03, 0.46032702E-03, 0.12931593E-02, 0.45127916E-03,
        -0.60658716E-03,-0.25168914E-03, 0.25285725E-03,-0.85019157E-03,
         0.13616207E-02, 0.24803181E-02,-0.37415221E-03, 0.60623931E-03,
        -0.11125413E-02,-0.14990607E-02,-0.97398122E-03, 0.90548734E-03,
         0.58558816E-03,-0.51645527E-03,-0.12931852E-02, 0.65981824E-03,
         0.84373791E-03, 0.15402591E-03,-0.47687060E-03,-0.10190158E-02,
        -0.23580100E-02, 0.19217933E-03, 0.34489934E-04,-0.36046669E-03,
        -0.12007920E-03, 0.13780990E-03,-0.60911410E-04,-0.92929418E-04,
         0.39493447E-03,-0.11605358E-03,-0.24008965E-03, 0.34417096E-03,
        -0.16201976E-03, 0.72584771E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x34        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]    *x24*x32        
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x31*x43    
        +coeff[ 14]        *x31*x41    
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 17]            *x42*x51
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x11*x21        *x51
        +coeff[ 20]*x11        *x41*x51
        +coeff[ 21]*x12            *x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x21*x33        
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 26]        *x33    *x51
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]*x11*x23            
        +coeff[ 29]*x12*x21        *x51
        +coeff[ 30]        *x31*x44    
        +coeff[ 31]    *x22*x31*x41*x51
        +coeff[ 32]        *x31*x43*x51
        +coeff[ 33]            *x42*x53
        +coeff[ 34]*x11*x21    *x43    
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 35]*x12    *x32*x41    
        +coeff[ 36]*x12    *x32    *x51
        +coeff[ 37]*x12    *x31*x41*x51
        +coeff[ 38]*x12            *x53
        +coeff[ 39]    *x23    *x42*x51
        +coeff[ 40]    *x21    *x44*x51
        +coeff[ 41]    *x23*x31    *x52
        +coeff[ 42]    *x21    *x42*x53
        +coeff[ 43]        *x32    *x54
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 44]*x11*x23    *x42    
        +coeff[ 45]*x11*x22    *x43    
        +coeff[ 46]*x11    *x32*x43    
        +coeff[ 47]*x11*x21    *x44    
        +coeff[ 48]*x11    *x31*x44    
        +coeff[ 49]*x11    *x34    *x51
        +coeff[ 50]*x11*x21*x32*x41*x51
        +coeff[ 51]*x11*x22    *x41*x52
        +coeff[ 52]*x11*x21    *x42*x52
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 53]*x11*x21*x31    *x53
        +coeff[ 54]*x11    *x31*x41*x53
        +coeff[ 55]*x12    *x33*x41    
        +coeff[ 56]*x12*x22*x31    *x51
        +coeff[ 57]*x12            *x54
        +coeff[ 58]*x13*x22        *x51
        +coeff[ 59]*x13    *x31*x41*x51
        +coeff[ 60]    *x22*x33*x41*x51
        +coeff[ 61]    *x22*x32*x42*x51
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 62]*x13    *x31    *x52
        +coeff[ 63]    *x23    *x42*x52
        +coeff[ 64]        *x33*x41*x53
        +coeff[ 65]*x11*x21*x33*x42    
        +coeff[ 66]*x11    *x34*x41*x51
        +coeff[ 67]*x11*x23    *x42*x51
        +coeff[ 68]*x11*x22    *x43*x51
        +coeff[ 69]*x11    *x34    *x52
        +coeff[ 70]*x11*x21*x32*x41*x52
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 71]*x12    *x33    *x52
        +coeff[ 72]*x13*x23*x31        
        +coeff[ 73]*x13        *x44    
        +coeff[ 74]    *x21*x33*x44    
        +coeff[ 75]*x13    *x33    *x51
        +coeff[ 76]    *x23    *x44*x51
        +coeff[ 77]*x13            *x54
        +coeff[ 78]        *x31        
        +coeff[ 79]    *x21*x31        
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 80]    *x22*x31        
        +coeff[ 81]    *x21*x31*x41    
        +coeff[ 82]        *x32*x41    
        +coeff[ 83]    *x21    *x41*x51
        +coeff[ 84]        *x31*x41*x51
        +coeff[ 85]*x11*x22            
        +coeff[ 86]*x11*x21*x31        
        +coeff[ 87]*x11    *x31    *x51
        +coeff[ 88]*x12    *x31        
    ;
    v_l_e_q1q2_1_1000                              =v_l_e_q1q2_1_1000                              
        +coeff[ 89]*x13                
        ;

    return v_l_e_q1q2_1_1000                              ;
}
float x_e_q1q2_1_900                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.4091168E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.38072956E-03, 0.12214248E+00, 0.17745541E-02, 0.25068552E-02,
        -0.36252497E-03, 0.83325336E-04,-0.51573548E-03,-0.41586801E-03,
        -0.32673861E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_900                              =v_x_e_q1q2_1_900                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_900                              ;
}
float t_e_q1q2_1_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1494464E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.13730982E-04,-0.20041040E-02,-0.26684201E-02, 0.23081105E-02,
         0.73439100E-04,-0.18200080E-03,-0.27508990E-03,-0.32708433E-03,
         0.56996732E-05,-0.24663963E-03,-0.14152504E-03,-0.83014835E-04,
        -0.80109114E-03,-0.62635448E-03,-0.16568385E-04,-0.44119311E-03,
        -0.19818806E-04,-0.15927535E-04,-0.30576189E-05, 0.68658410E-04,
         0.25514351E-04,-0.22829363E-03,-0.52489061E-03, 0.82950392E-05,
         0.26196092E-04,-0.24745810E-04, 0.58497084E-04, 0.94237902E-06,
        -0.21051821E-05,-0.10745270E-04,-0.58025089E-05, 0.14777595E-04,
        -0.91531638E-05,-0.10314886E-04, 0.72480993E-05, 0.19780387E-05,
        -0.13191850E-05, 0.58051633E-05,-0.47868129E-04,-0.11039764E-04,
        -0.35390643E-04, 0.76524793E-06,-0.14805154E-04, 0.12373931E-04,
        -0.41271951E-05, 0.12765940E-04,-0.18298380E-04, 0.90680460E-05,
         0.17286404E-04,-0.13342405E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_900                              =v_t_e_q1q2_1_900                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_900                              =v_t_e_q1q2_1_900                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x12*x22    *x41*x51
    ;
    v_t_e_q1q2_1_900                              =v_t_e_q1q2_1_900                              
        +coeff[ 26]    *x21*x32*x42*x51
        +coeff[ 27]*x11        *x41    
        +coeff[ 28]                *x52
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]        *x31*x42    
        +coeff[ 31]*x11*x22*x31        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q1q2_1_900                              =v_t_e_q1q2_1_900                              
        +coeff[ 35]*x12*x22*x31        
        +coeff[ 36]*x11*x22*x32        
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]*x11*x22*x31*x41    
        +coeff[ 39]*x11*x21*x32*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x13*x21        *x51
        +coeff[ 42]*x12*x21*x31    *x51
        +coeff[ 43]*x11*x21*x32    *x51
    ;
    v_t_e_q1q2_1_900                              =v_t_e_q1q2_1_900                              
        +coeff[ 44]            *x42*x53
        +coeff[ 45]*x12*x22*x32        
        +coeff[ 46]*x11*x22*x33        
        +coeff[ 47]    *x22*x33*x41    
        +coeff[ 48]*x12*x22*x31    *x51
        +coeff[ 49]*x11*x21*x33    *x51
        ;

    return v_t_e_q1q2_1_900                              ;
}
float y_e_q1q2_1_900                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.1761749E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.12651220E-03, 0.13203810E+00, 0.88315979E-01,-0.16112184E-02,
        -0.14697158E-02,-0.82514678E-04,-0.26593923E-04,-0.19959647E-03,
         0.20094536E-03,-0.43476593E-04, 0.11255208E-03, 0.10629098E-03,
         0.64242093E-04, 0.60422499E-04,-0.20641244E-03,-0.34057903E-04,
         0.58644546E-04,-0.42472057E-05,-0.20967645E-03,-0.96921875E-04,
         0.11756462E-06,-0.28163233E-04, 0.13952061E-04, 0.54410095E-04,
         0.19780025E-05, 0.10851179E-04, 0.23579523E-04,-0.14534897E-03,
         0.40888583E-04, 0.16674729E-04, 0.25489207E-04, 0.20622623E-04,
        -0.59592490E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_900                              =v_y_e_q1q2_1_900                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_y_e_q1q2_1_900                              =v_y_e_q1q2_1_900                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x24*x33        
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]        *x32*x41*x51
        +coeff[ 25]*x11        *x42*x51
    ;
    v_y_e_q1q2_1_900                              =v_y_e_q1q2_1_900                              
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x11    *x31*x42*x51
        +coeff[ 31]*x11*x23    *x42    
        +coeff[ 32]    *x22*x32*x41*x51
        ;

    return v_y_e_q1q2_1_900                              ;
}
float p_e_q1q2_1_900                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.6142844E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.38215767E-04, 0.32420885E-01, 0.66828653E-01,-0.15773826E-02,
         0.26347686E-05,-0.14018716E-02,-0.31345541E-03,-0.37200283E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_900                              ;
}
float l_e_q1q2_1_900                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1819675E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18468180E-02,-0.35545076E-02,-0.49099146E-03,-0.28602863E-02,
        -0.14363500E-03,-0.89303224E-03, 0.55924797E-03,-0.36839166E-03,
        -0.34431397E-03,-0.70162438E-03,-0.65566809E-03,-0.12177123E-02,
        -0.46111331E-02,-0.92346244E-03,-0.50085271E-03,-0.39073089E-02,
         0.30717050E-03,-0.14830813E-03,-0.15580300E-02, 0.25544886E-03,
         0.26172533E-04,-0.73585875E-03, 0.81835268E-03,-0.63836074E-03,
        -0.26988999E-04,-0.19175911E-03, 0.24169963E-03,-0.60340069E-03,
         0.58744114E-03, 0.15296337E-03,-0.31581230E-03,-0.28729995E-03,
        -0.63565043E-04, 0.11128154E-03,-0.19397431E-03,-0.71932399E-03,
        -0.16433166E-03, 0.58117404E-03,-0.15024568E-02,-0.86805300E-03,
        -0.13075577E-02, 0.17081355E-02, 0.37518790E-03, 0.96770166E-03,
         0.12496590E-02, 0.13421810E-02,-0.11505897E-02, 0.37868164E-03,
         0.59254380E-03, 0.48489426E-03,-0.23511155E-03,-0.20602376E-02,
         0.66319731E-03, 0.94832387E-03,-0.16304917E-02,-0.54305902E-03,
         0.30653217E-03, 0.29401324E-03,-0.59312693E-03,-0.35370587E-03,
         0.51800831E-03,-0.33369902E-03, 0.74154843E-03, 0.16034471E-02,
        -0.99128892E-03,-0.46707943E-03, 0.77272271E-03, 0.16916032E-02,
        -0.25376619E-02, 0.11985388E-02,-0.25395344E-02, 0.70221737E-04,
         0.94264402E-03, 0.44233361E-03,-0.46374183E-03, 0.15670421E-02,
         0.18278444E-02, 0.27295870E-02,-0.68825280E-03,-0.87353663E-03,
        -0.97980395E-04,-0.16979735E-02, 0.51845489E-02, 0.75750158E-03,
         0.69612631E-03, 0.11732612E-02, 0.12537498E-03,-0.65945576E-04,
        -0.21628942E-03,-0.81914986E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]*x11        *x41*x51
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]*x12*x21    *x41*x51
        +coeff[ 11]    *x24*x31*x41    
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]        *x34*x42    
        +coeff[ 14]    *x21*x31*x44    
        +coeff[ 15]*x11*x22*x33    *x51
        +coeff[ 16]*x11*x22*x31    *x53
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 17]    *x21    *x41    
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]*x11*x21            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]*x12*x21            
        +coeff[ 25]*x12    *x31        
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 26]    *x23*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]*x11    *x31*x41*x51
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]*x12    *x31    *x51
        +coeff[ 31]*x13*x21            
        +coeff[ 32]    *x23*x32        
        +coeff[ 33]    *x21*x33*x41    
        +coeff[ 34]        *x34*x41    
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x24        *x51
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]*x11*x21*x33        
        +coeff[ 40]*x11*x21*x31*x42    
        +coeff[ 41]*x11*x22*x31    *x51
        +coeff[ 42]*x11*x21*x32    *x51
        +coeff[ 43]*x11    *x32*x41*x51
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 44]*x11*x21    *x41*x52
        +coeff[ 45]*x11        *x41*x53
        +coeff[ 46]*x12        *x42*x51
        +coeff[ 47]*x12            *x53
        +coeff[ 48]    *x23*x33        
        +coeff[ 49]    *x21*x32    *x53
        +coeff[ 50]    *x21    *x42*x53
        +coeff[ 51]*x11*x22*x32*x41    
        +coeff[ 52]*x11    *x34*x41    
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 53]*x11*x22*x32    *x51
        +coeff[ 54]*x11    *x31*x43*x51
        +coeff[ 55]*x11    *x32    *x53
        +coeff[ 56]*x11    *x31    *x54
        +coeff[ 57]*x12*x24            
        +coeff[ 58]*x12*x22    *x42    
        +coeff[ 59]*x12        *x44    
        +coeff[ 60]*x12*x22        *x52
        +coeff[ 61]*x13*x22*x31        
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 62]*x13*x22    *x41    
        +coeff[ 63]    *x22*x32*x42*x51
        +coeff[ 64]    *x22    *x44*x51
        +coeff[ 65]*x13        *x41*x52
        +coeff[ 66]            *x43*x54
        +coeff[ 67]*x11*x22*x34        
        +coeff[ 68]*x11*x21*x32*x41*x52
        +coeff[ 69]*x11    *x33*x41*x52
        +coeff[ 70]*x11    *x32*x41*x53
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 71]*x12*x21*x34        
        +coeff[ 72]*x12*x21*x31*x43    
        +coeff[ 73]*x12    *x31*x44    
        +coeff[ 74]*x12    *x31*x43*x51
        +coeff[ 75]*x12        *x44*x51
        +coeff[ 76]*x12*x21*x32    *x52
        +coeff[ 77]*x12*x22    *x41*x52
        +coeff[ 78]*x12    *x32*x41*x52
        +coeff[ 79]*x12        *x43*x52
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 80]*x13*x24            
        +coeff[ 81]*x13*x22*x32        
        +coeff[ 82]    *x24*x33*x41    
        +coeff[ 83]*x13*x22    *x42    
        +coeff[ 84]*x13    *x32    *x52
        +coeff[ 85]    *x22*x33*x41*x52
        +coeff[ 86]    *x21            
        +coeff[ 87]*x11                
        +coeff[ 88]    *x21*x31        
    ;
    v_l_e_q1q2_1_900                              =v_l_e_q1q2_1_900                              
        +coeff[ 89]    *x21        *x51
        ;

    return v_l_e_q1q2_1_900                              ;
}
float x_e_q1q2_1_800                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.3445407E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.36765752E-04, 0.12215524E+00, 0.17754275E-02, 0.25039329E-02,
        -0.36229094E-03, 0.83566410E-04,-0.51584322E-03,-0.41361968E-03,
        -0.32857448E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_800                              =v_x_e_q1q2_1_800                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_800                              ;
}
float t_e_q1q2_1_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1942133E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.19559302E-04,-0.20017985E-02,-0.26612075E-02, 0.23084441E-02,
         0.72861774E-04,-0.17751378E-03,-0.27366445E-03,-0.33114819E-03,
        -0.38124838E-05,-0.25774885E-03,-0.13476673E-03,-0.88773813E-04,
        -0.80184540E-03,-0.62235881E-03,-0.17094040E-04,-0.41780158E-03,
        -0.10160888E-04,-0.21009537E-04, 0.58179357E-04, 0.42517822E-04,
        -0.22579475E-03,-0.48819929E-03, 0.86088930E-05,-0.94714579E-06,
        -0.12182129E-04,-0.32621697E-05,-0.72793737E-05,-0.53977698E-06,
        -0.13607571E-04, 0.18458717E-04,-0.12750909E-04,-0.14777119E-04,
        -0.46383371E-04,-0.20815138E-04,-0.24069561E-04, 0.10678954E-04,
         0.87854796E-05, 0.13768513E-04, 0.94133893E-05, 0.20085416E-04,
        -0.82070419E-05,-0.11608516E-04, 0.11754091E-04,-0.52556052E-05,
        -0.13848133E-04, 0.24898023E-04, 0.17349390E-04, 0.11608081E-04,
         0.12830285E-04, 0.13241325E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_800                              =v_t_e_q1q2_1_800                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_800                              =v_t_e_q1q2_1_800                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x21    *x41    
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_t_e_q1q2_1_800                              =v_t_e_q1q2_1_800                              
        +coeff[ 26]    *x21*x31    *x51
        +coeff[ 27]*x11            *x52
        +coeff[ 28]*x11*x22*x31        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11    *x33*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1q2_1_800                              =v_t_e_q1q2_1_800                              
        +coeff[ 35]*x11*x21    *x43    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]*x13*x21        *x51
        +coeff[ 38]*x11*x22*x31    *x51
        +coeff[ 39]    *x23*x31    *x51
        +coeff[ 40]    *x22*x32    *x51
        +coeff[ 41]*x11*x22        *x52
        +coeff[ 42]    *x22*x31    *x52
        +coeff[ 43]        *x33    *x52
    ;
    v_t_e_q1q2_1_800                              =v_t_e_q1q2_1_800                              
        +coeff[ 44]*x11*x21        *x53
        +coeff[ 45]*x11*x22*x33        
        +coeff[ 46]*x11*x22*x31*x42    
        +coeff[ 47]    *x23    *x43    
        +coeff[ 48]*x12*x23        *x51
        +coeff[ 49]*x13*x21*x31    *x51
        ;

    return v_t_e_q1q2_1_800                              ;
}
float y_e_q1q2_1_800                              (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.5207729E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.54655573E-03, 0.13204478E+00, 0.88322081E-01,-0.16058112E-02,
        -0.14750519E-02,-0.37601301E-04,-0.30498937E-04,-0.17795886E-03,
         0.21043996E-03,-0.65280168E-04, 0.12044857E-03, 0.68208115E-04,
         0.71580173E-04, 0.62640160E-04,-0.27785823E-03,-0.16570704E-04,
         0.69651965E-05,-0.69257600E-04,-0.25095016E-03, 0.14254741E-04,
         0.18558942E-04, 0.42475087E-04,-0.18244701E-03,-0.13710722E-04,
        -0.61550905E-04,-0.42025989E-04, 0.20914884E-04,-0.40809307E-04,
        -0.46505018E-04, 0.27888194E-04,-0.68381094E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_800                              =v_y_e_q1q2_1_800                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_1_800                              =v_y_e_q1q2_1_800                              
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]    *x24*x32*x41    
        +coeff[ 19]*x11*x21    *x42    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]        *x31*x43*x51
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x21*x32*x41    
    ;
    v_y_e_q1q2_1_800                              =v_y_e_q1q2_1_800                              
        +coeff[ 26]    *x21*x32*x41*x51
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]        *x32*x43*x51
        +coeff[ 29]*x13*x21    *x41    
        +coeff[ 30]    *x22*x31*x42*x51
        ;

    return v_y_e_q1q2_1_800                              ;
}
float p_e_q1q2_1_800                              (float *x,int m){
    int ncoeff=  8;
    float avdat=  0.3338394E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
        -0.34632848E-03, 0.32421462E-01, 0.66827111E-01,-0.15776873E-02,
         0.59787667E-05,-0.14045383E-02,-0.31560106E-03,-0.37126546E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_800                              ;
}
float l_e_q1q2_1_800                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1819662E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18777717E-02,-0.38856110E-02,-0.40712467E-03,-0.29843550E-02,
         0.16911768E-02, 0.15690533E-02, 0.84637210E-03,-0.16534874E-02,
        -0.11578873E-03,-0.80561306E-03, 0.27533332E-03, 0.14407920E-02,
         0.84660144E-03,-0.28669180E-02, 0.36319837E-02, 0.93466439E-03,
        -0.13000868E-02, 0.17270979E-02,-0.15448315E-04,-0.23757510E-02,
        -0.85897955E-04,-0.14070807E-03, 0.68172510E-03, 0.64012071E-04,
         0.48494362E-03,-0.29275412E-03, 0.37005413E-03,-0.33242715E-03,
         0.16513422E-03,-0.16907895E-03,-0.43942302E-03, 0.18380869E-03,
         0.78558153E-03, 0.28202726E-03, 0.91356458E-03,-0.47775239E-03,
         0.12963566E-02,-0.48840267E-03,-0.28780110E-02, 0.88895147E-03,
         0.17232434E-02,-0.62305084E-03,-0.21371190E-03,-0.32735402E-02,
         0.13156332E-02,-0.86211320E-03, 0.72859734E-03, 0.13162942E-03,
        -0.83330512E-03, 0.93338359E-03,-0.14965168E-02,-0.59289084E-03,
        -0.48936671E-03, 0.15273086E-02,-0.16858590E-03,-0.15359794E-02,
         0.53137413E-03, 0.26632175E-02, 0.27821525E-02, 0.16895379E-02,
         0.10073769E-02,-0.38532165E-03,-0.10845207E-02,-0.17726370E-02,
        -0.20727527E-02,-0.16600621E-02,-0.13717493E-02, 0.57135941E-03,
        -0.51128706E-02,-0.15172909E-02,-0.13555689E-02, 0.62111742E-03,
         0.17548423E-02,-0.71171677E-03,-0.61673380E-03, 0.35266809E-02,
         0.73337212E-03, 0.28696191E-03, 0.16907100E-02,-0.12601600E-02,
         0.87267574E-04,-0.56064560E-04,-0.72054354E-04, 0.13254772E-03,
        -0.88760680E-04, 0.12069957E-03,-0.23892964E-03, 0.16502736E-03,
        -0.64251057E-04,-0.20704960E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x31*x41    
        +coeff[  5]        *x33*x41    
        +coeff[  6]        *x31*x43    
        +coeff[  7]        *x31*x41*x52
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[  8]            *x42*x52
        +coeff[  9]*x12    *x31*x41    
        +coeff[ 10]*x12        *x42    
        +coeff[ 11]        *x32*x42*x51
        +coeff[ 12]*x11*x21*x31*x42    
        +coeff[ 13]    *x22*x33*x41    
        +coeff[ 14]    *x22*x32*x42    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]        *x32*x44    
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 17]    *x22*x31*x41*x52
        +coeff[ 18]*x12    *x33*x41    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]    *x21        *x51
        +coeff[ 21]*x12                
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 26]*x11*x21*x32        
        +coeff[ 27]*x11*x21*x31    *x51
        +coeff[ 28]*x11    *x32    *x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x12*x21    *x41    
        +coeff[ 31]    *x21*x34        
        +coeff[ 32]    *x22    *x41*x52
        +coeff[ 33]*x11    *x33    *x51
        +coeff[ 34]*x11    *x31*x41*x52
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 35]*x12    *x32*x41    
        +coeff[ 36]*x12        *x42*x51
        +coeff[ 37]*x13    *x31*x41    
        +coeff[ 38]        *x33*x43    
        +coeff[ 39]    *x24*x31    *x51
        +coeff[ 40]    *x23*x31*x41*x51
        +coeff[ 41]        *x33*x42*x51
        +coeff[ 42]        *x33    *x53
        +coeff[ 43]    *x21*x31*x41*x53
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 44]        *x31*x41*x54
        +coeff[ 45]*x11*x24    *x41    
        +coeff[ 46]*x11*x22    *x43    
        +coeff[ 47]*x11*x24        *x51
        +coeff[ 48]*x11*x22        *x53
        +coeff[ 49]*x11*x21*x31    *x53
        +coeff[ 50]*x12    *x32*x42    
        +coeff[ 51]        *x33*x44    
        +coeff[ 52]*x13*x21    *x41*x51
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 53]    *x21*x34*x41*x51
        +coeff[ 54]    *x23    *x43*x51
        +coeff[ 55]    *x22*x32*x41*x52
        +coeff[ 56]    *x21*x33    *x53
        +coeff[ 57]*x11*x22*x31*x42*x51
        +coeff[ 58]*x11*x22    *x41*x53
        +coeff[ 59]*x12*x23*x31*x41    
        +coeff[ 60]*x12*x23    *x42    
        +coeff[ 61]*x12    *x34    *x51
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 62]*x12*x23    *x41*x51
        +coeff[ 63]*x12    *x33*x41*x51
        +coeff[ 64]*x12    *x32*x42*x51
        +coeff[ 65]*x12*x22*x31    *x52
        +coeff[ 66]*x12        *x42*x53
        +coeff[ 67]*x12    *x31    *x54
        +coeff[ 68]    *x22*x34*x42    
        +coeff[ 69]*x13*x22    *x41*x51
        +coeff[ 70]*x13    *x31*x42*x51
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 71]    *x21*x34*x42*x51
        +coeff[ 72]        *x33*x44*x51
        +coeff[ 73]*x13*x22        *x52
        +coeff[ 74]*x13        *x41*x53
        +coeff[ 75]    *x21*x33*x41*x53
        +coeff[ 76]    *x22    *x43*x53
        +coeff[ 77]*x13            *x54
        +coeff[ 78]    *x23*x31    *x54
        +coeff[ 79]    *x21*x33    *x54
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 80]    *x21            
        +coeff[ 81]            *x41    
        +coeff[ 82]    *x21*x31        
        +coeff[ 83]    *x21    *x41    
        +coeff[ 84]*x11    *x31        
        +coeff[ 85]*x11            *x51
        +coeff[ 86]    *x23            
        +coeff[ 87]    *x22*x31        
        +coeff[ 88]        *x33        
    ;
    v_l_e_q1q2_1_800                              =v_l_e_q1q2_1_800                              
        +coeff[ 89]    *x21*x31*x41    
        ;

    return v_l_e_q1q2_1_800                              ;
}
float x_e_q1q2_1_700                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.6164515E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.61989255E-03, 0.17767549E-02, 0.12215891E+00, 0.25022037E-02,
        -0.36505720E-03, 0.83583851E-04,-0.51143189E-03,-0.41073837E-03,
        -0.32152771E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_700                              =v_x_e_q1q2_1_700                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_700                              ;
}
float t_e_q1q2_1_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2578130E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.26961063E-04,-0.20031470E-02,-0.26542367E-02, 0.23027088E-02,
         0.70454771E-04,-0.16698102E-03,-0.25670210E-03,-0.31944949E-03,
         0.51054390E-05,-0.27615941E-03,-0.12536903E-03,-0.85421962E-04,
        -0.82905369E-03,-0.64479408E-03,-0.19621231E-04,-0.43526647E-03,
        -0.16855000E-04,-0.17027329E-04,-0.13056164E-04, 0.46415440E-04,
         0.32325770E-04, 0.20321353E-04,-0.23563068E-03,-0.51518565E-03,
        -0.64186015E-05,-0.79815163E-05,-0.46596069E-05, 0.23350919E-05,
        -0.83535870E-05, 0.20673924E-04, 0.78329811E-06, 0.18082404E-04,
        -0.11280657E-04,-0.30944764E-05,-0.47667560E-04,-0.14161676E-04,
        -0.35789431E-04,-0.92261444E-05, 0.14466976E-04,-0.60988168E-05,
         0.96224803E-05, 0.27215303E-04,-0.25767498E-04, 0.18677360E-04,
        -0.15104917E-04, 0.16538424E-04, 0.69938773E-06, 0.20805091E-05,
         0.18653905E-05,-0.24227074E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_700                              =v_t_e_q1q2_1_700                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_700                              =v_t_e_q1q2_1_700                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q1q2_1_700                              =v_t_e_q1q2_1_700                              
        +coeff[ 26]*x12            *x51
        +coeff[ 27]                *x53
        +coeff[ 28]*x11*x23            
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]*x11    *x32    *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x12*x23            
        +coeff[ 33]*x11*x22*x32        
        +coeff[ 34]*x11*x22*x31*x41    
    ;
    v_t_e_q1q2_1_700                              =v_t_e_q1q2_1_700                              
        +coeff[ 35]*x11*x21*x32*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x11*x22        *x52
        +coeff[ 38]        *x32*x41*x52
        +coeff[ 39]            *x43*x52
        +coeff[ 40]*x13*x23            
        +coeff[ 41]*x13*x21    *x42    
        +coeff[ 42]*x11*x23    *x42    
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_t_e_q1q2_1_700                              =v_t_e_q1q2_1_700                              
        +coeff[ 44]*x12*x22        *x52
        +coeff[ 45]*x12*x21*x31    *x52
        +coeff[ 46]        *x31        
        +coeff[ 47]*x12                
        +coeff[ 48]    *x22            
        +coeff[ 49]    *x21*x31        
        ;

    return v_t_e_q1q2_1_700                              ;
}
float y_e_q1q2_1_700                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.2193424E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.26175700E-03, 0.13199690E+00, 0.88309810E-01,-0.16042092E-02,
        -0.14736218E-02,-0.40397263E-04,-0.85719767E-04,-0.21212589E-03,
         0.21201750E-03,-0.21290602E-04, 0.14401109E-03, 0.10455214E-03,
         0.69470836E-04, 0.56880210E-04,-0.26920874E-03,-0.36912708E-04,
         0.37242989E-04,-0.26598206E-03,-0.32785247E-04,-0.45924702E-04,
        -0.79424905E-06,-0.51766829E-05,-0.29754126E-05, 0.34662000E-04,
         0.20226140E-04,-0.19909174E-03,-0.32630251E-04, 0.26516898E-04,
        -0.59743048E-04,-0.26806067E-04,-0.54481283E-04, 0.29564158E-04,
         0.23817518E-04, 0.85331034E-04, 0.29179169E-04,-0.39008199E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_700                              =v_y_e_q1q2_1_700                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q1q2_1_700                              =v_y_e_q1q2_1_700                              
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]*x11*x23    *x41    
        +coeff[ 19]    *x22*x33*x42    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x12        *x41    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1q2_1_700                              =v_y_e_q1q2_1_700                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]*x11    *x32*x42    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]*x11        *x45    
        +coeff[ 31]*x12*x22    *x41    
        +coeff[ 32]    *x23*x31*x42    
        +coeff[ 33]    *x21*x31*x43*x51
        +coeff[ 34]    *x23    *x42*x51
    ;
    v_y_e_q1q2_1_700                              =v_y_e_q1q2_1_700                              
        +coeff[ 35]    *x21*x33*x41*x51
        ;

    return v_y_e_q1q2_1_700                              ;
}
float p_e_q1q2_1_700                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.1466048E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.16708940E-03, 0.32412425E-01, 0.66805989E-01,-0.15766639E-02,
         0.27628416E-05,-0.14025680E-02,-0.31854064E-03,-0.37935845E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_700                              ;
}
float l_e_q1q2_1_700                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1829750E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17134547E-02,-0.31271959E-02,-0.34702770E-03,-0.27859844E-02,
        -0.45044918E-03, 0.18294746E-02,-0.11457723E-03,-0.90521455E-04,
        -0.44682139E-03, 0.62927487E-03,-0.10591919E-02,-0.53310585E-02,
        -0.18468626E-02, 0.66840620E-03,-0.18657121E-02,-0.23247134E-02,
        -0.14891569E-03, 0.23004823E-04,-0.51985984E-03, 0.25308764E-03,
         0.13080055E-02, 0.77324855E-03,-0.55928703E-03,-0.36456654E-03,
        -0.12078756E-04, 0.52823464E-03,-0.33815292E-03, 0.76621385E-04,
        -0.16206400E-02,-0.36894219E-03, 0.93294098E-03, 0.24390857E-03,
        -0.26836433E-02, 0.28602421E-03,-0.43643638E-03, 0.31058970E-03,
        -0.75874658E-03, 0.47438761E-03, 0.53344265E-03,-0.20824611E-04,
        -0.75518532E-03,-0.17309947E-02, 0.50600944E-03, 0.64808247E-03,
        -0.54151326E-03,-0.17748227E-02, 0.14764753E-02,-0.18014056E-02,
         0.53159503E-03, 0.16211011E-02,-0.87523967E-03,-0.89633168E-03,
        -0.49867271E-03, 0.11668123E-02, 0.13462016E-02, 0.64572570E-03,
        -0.25772213E-03, 0.39458714E-03, 0.13621065E-02,-0.16814622E-02,
         0.15055230E-02, 0.14316113E-02, 0.19587670E-02,-0.86013449E-03,
         0.19151693E-02,-0.78862667E-03, 0.78721921E-03,-0.50132792E-03,
        -0.64541248E-03, 0.23353528E-02,-0.27022918E-03,-0.28792892E-02,
         0.15510711E-02, 0.15687721E-02,-0.76130898E-04,-0.14050825E-02,
         0.90545526E-03, 0.31710305E-03, 0.12541401E-02, 0.88900613E-03,
        -0.35297760E-03, 0.42355782E-02,-0.15967588E-02,-0.85989310E-03,
        -0.78410609E-03,-0.33968049E-02,-0.60093990E-02, 0.24291633E-02,
         0.33752874E-02, 0.42993044E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[  8]            *x44    
        +coeff[  9]*x12    *x31*x41    
        +coeff[ 10]    *x24*x31*x41    
        +coeff[ 11]    *x22*x33*x41    
        +coeff[ 12]        *x34*x42    
        +coeff[ 13]*x11*x24    *x41    
        +coeff[ 14]*x11    *x32*x43    
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]*x11*x21            
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]            *x43*x51
        +coeff[ 20]        *x31*x41*x52
        +coeff[ 21]*x11*x21*x31*x41    
        +coeff[ 22]*x11    *x31*x42    
        +coeff[ 23]*x12*x22            
        +coeff[ 24]*x12    *x32        
        +coeff[ 25]*x12        *x42    
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 26]*x12*x21        *x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x21*x31*x42*x51
        +coeff[ 29]*x11*x24            
        +coeff[ 30]*x11    *x31*x43    
        +coeff[ 31]*x11        *x44    
        +coeff[ 32]*x11    *x32*x41*x51
        +coeff[ 33]*x11        *x43*x51
        +coeff[ 34]*x12*x21    *x41*x51
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 35]*x13*x22            
        +coeff[ 36]*x13    *x31*x41    
        +coeff[ 37]*x13        *x41*x51
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x21*x32*x41*x52
        +coeff[ 40]    *x21    *x43*x52
        +coeff[ 41]        *x31*x43*x52
        +coeff[ 42]    *x22*x31    *x53
        +coeff[ 43]    *x21*x31    *x54
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 44]*x11*x23*x32        
        +coeff[ 45]*x11*x23*x31*x41    
        +coeff[ 46]*x11*x21*x32*x41*x51
        +coeff[ 47]*x11*x21    *x43*x51
        +coeff[ 48]*x11*x21*x31    *x53
        +coeff[ 49]*x12    *x32*x42    
        +coeff[ 50]*x12*x22*x31    *x51
        +coeff[ 51]*x12*x22    *x41*x51
        +coeff[ 52]*x12        *x42*x52
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 53]*x13*x21*x32        
        +coeff[ 54]*x13*x21*x31*x41    
        +coeff[ 55]*x13    *x32*x41    
        +coeff[ 56]*x13*x22        *x51
        +coeff[ 57]    *x23*x33    *x51
        +coeff[ 58]*x13*x21    *x41*x51
        +coeff[ 59]    *x24*x31*x41*x51
        +coeff[ 60]    *x22*x33*x41*x51
        +coeff[ 61]    *x21*x33*x42*x51
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 62]    *x23    *x42*x52
        +coeff[ 63]    *x22    *x43*x52
        +coeff[ 64]    *x21*x31*x43*x52
        +coeff[ 65]        *x31*x44*x52
        +coeff[ 66]    *x21*x32    *x54
        +coeff[ 67]        *x33    *x54
        +coeff[ 68]        *x32*x41*x54
        +coeff[ 69]*x11*x24*x31*x41    
        +coeff[ 70]*x11*x22*x32*x42    
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 71]*x11*x22*x31*x43    
        +coeff[ 72]*x11*x23*x32    *x51
        +coeff[ 73]*x11    *x34*x41*x51
        +coeff[ 74]*x11*x23        *x53
        +coeff[ 75]*x11*x21*x32    *x53
        +coeff[ 76]*x12*x24    *x41    
        +coeff[ 77]*x12*x22*x32*x41    
        +coeff[ 78]*x12*x22*x31*x42    
        +coeff[ 79]*x12*x22*x31    *x52
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 80]    *x24*x34        
        +coeff[ 81]    *x24*x33*x41    
        +coeff[ 82]*x13*x21*x31*x41*x51
        +coeff[ 83]*x13*x21    *x42*x51
        +coeff[ 84]*x13    *x31*x42*x51
        +coeff[ 85]    *x23*x32*x41*x52
        +coeff[ 86]    *x23*x31*x42*x52
        +coeff[ 87]    *x21*x31*x44*x52
        +coeff[ 88]    *x21*x32*x41*x54
    ;
    v_l_e_q1q2_1_700                              =v_l_e_q1q2_1_700                              
        +coeff[ 89]    *x21            
        ;

    return v_l_e_q1q2_1_700                              ;
}
float x_e_q1q2_1_600                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.1133926E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.88635446E-04, 0.17750164E-02, 0.12219214E+00, 0.25036214E-02,
        -0.36389264E-03, 0.81411046E-04,-0.50123862E-03,-0.40883510E-03,
        -0.32421871E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_600                              =v_x_e_q1q2_1_600                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_600                              ;
}
float t_e_q1q2_1_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5301717E-05;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35258120E-05,-0.20026867E-02,-0.26249655E-02, 0.23121245E-02,
         0.73770476E-04,-0.19255832E-03,-0.27862520E-03,-0.34903904E-03,
         0.31340453E-05,-0.21702860E-03,-0.15798300E-03,-0.93107192E-04,
        -0.79533417E-03,-0.60819031E-03,-0.16732176E-04,-0.41546303E-03,
        -0.24060295E-04,-0.18894787E-04,-0.20793415E-04, 0.10910940E-04,
        -0.67740853E-05,-0.46577497E-03, 0.79536849E-05,-0.74394234E-05,
        -0.12588572E-04, 0.27084195E-05, 0.33738822E-05, 0.55995333E-05,
         0.12281924E-04,-0.56065387E-05,-0.13865509E-04,-0.12932516E-04,
         0.35620767E-04,-0.47009526E-05, 0.16609060E-05, 0.96264539E-05,
        -0.14436222E-04,-0.12915059E-04,-0.22423980E-03,-0.14731754E-04,
         0.97686361E-05,-0.89290043E-05, 0.13780148E-04, 0.39147031E-04,
         0.29246612E-04, 0.51749470E-04, 0.19601099E-04, 0.89087547E-06,
        -0.15175751E-05,-0.27203582E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_600                              =v_t_e_q1q2_1_600                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_600                              =v_t_e_q1q2_1_600                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x33        
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]*x11*x22*x32        
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_q1q2_1_600                              =v_t_e_q1q2_1_600                              
        +coeff[ 26]            *x41*x52
        +coeff[ 27]*x11*x22    *x41    
        +coeff[ 28]    *x23    *x41    
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]*x12*x22*x31        
    ;
    v_t_e_q1q2_1_600                              =v_t_e_q1q2_1_600                              
        +coeff[ 35]*x13    *x32        
        +coeff[ 36]*x11*x21*x32*x41    
        +coeff[ 37]*x11    *x33*x41    
        +coeff[ 38]    *x21*x33*x41    
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x11*x22*x31    *x51
        +coeff[ 41]*x11    *x32    *x52
        +coeff[ 42]    *x21    *x42*x52
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_q1q2_1_600                              =v_t_e_q1q2_1_600                              
        +coeff[ 44]    *x23*x31*x41*x51
        +coeff[ 45]    *x21*x31*x43*x51
        +coeff[ 46]    *x21*x32    *x53
        +coeff[ 47]        *x31        
        +coeff[ 48]*x12                
        +coeff[ 49]    *x21    *x41    
        ;

    return v_t_e_q1q2_1_600                              ;
}
float y_e_q1q2_1_600                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.5239891E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.58163423E-03, 0.13198060E+00, 0.88318586E-01,-0.16052676E-02,
        -0.14694046E-02,-0.94586976E-04, 0.13894851E-03,-0.15156373E-03,
         0.18070184E-03,-0.12127703E-03, 0.11772979E-03, 0.89207198E-04,
         0.69830312E-04, 0.59445902E-04,-0.23180855E-03,-0.66704772E-04,
        -0.63309640E-05,-0.14690607E-03,-0.30481740E-04, 0.14891957E-03,
         0.54515508E-05, 0.30070989E-05, 0.37526184E-04, 0.36489637E-04,
        -0.77134619E-05,-0.11208493E-03,-0.17680022E-04,-0.47584348E-04,
        -0.14041065E-04,-0.45415913E-04,-0.99724857E-04, 0.66635388E-04,
        -0.55613171E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_600                              =v_y_e_q1q2_1_600                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]    *x24*x32*x41    
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_600                              =v_y_e_q1q2_1_600                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]        *x31*x44*x51
        +coeff[ 19]*x11*x21    *x45    
        +coeff[ 20]        *x31*x41    
        +coeff[ 21]        *x33        
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]*x11        *x44    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q1q2_1_600                              =v_y_e_q1q2_1_600                              
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]    *x21    *x42*x52
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]*x11*x23*x31*x41    
        +coeff[ 32]*x11*x21*x33*x41    
        ;

    return v_y_e_q1q2_1_600                              ;
}
float p_e_q1q2_1_600                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.2772834E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.30571551E-03, 0.32407895E-01, 0.66787466E-01,-0.15740906E-02,
         0.18846324E-04,-0.14060447E-02,-0.31333475E-03,-0.37741417E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]    *x22*x31    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_600                              ;
}
float l_e_q1q2_1_600                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1821677E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18930743E-02, 0.18479947E-03,-0.33960473E-02,-0.46129033E-03,
        -0.36043474E-02,-0.52319508E-03, 0.28884183E-02,-0.48714975E-06,
        -0.13877536E-02, 0.89703238E-03, 0.49128075E-03, 0.10049825E-02,
         0.14229625E-02, 0.15044004E-03,-0.30523648E-02,-0.55773340E-04,
         0.20351423E-04,-0.20337870E-03,-0.37204608E-03,-0.77573577E-05,
         0.36944001E-03, 0.23108350E-05, 0.46398127E-03,-0.52014733E-03,
         0.21733578E-03,-0.10210609E-02,-0.47105152E-03, 0.49211457E-03,
        -0.83778921E-03, 0.12124350E-02,-0.60130458E-03,-0.19553506E-02,
        -0.14173338E-03,-0.63044141E-03, 0.49168531E-04,-0.70473319E-03,
        -0.15624569E-02, 0.10532566E-02,-0.55052528E-05, 0.10916404E-02,
        -0.65008231E-03,-0.17923503E-02,-0.46120191E-03,-0.96745190E-03,
        -0.40980586E-03, 0.73148683E-03, 0.55726548E-03,-0.22822760E-03,
        -0.22096771E-02, 0.37071569E-03, 0.78268237E-02, 0.45076874E-03,
        -0.36024215E-03,-0.12398027E-02, 0.70199376E-03, 0.33761241E-03,
        -0.10914900E-02,-0.29927163E-03, 0.99648931E-03, 0.58414089E-03,
        -0.12997494E-02, 0.18749119E-02, 0.16104364E-02, 0.91533572E-03,
        -0.17630678E-03, 0.84024697E-03,-0.96955983E-03,-0.15191978E-02,
        -0.11964529E-02,-0.51485603E-02, 0.38180046E-02,-0.11576714E-02,
         0.12853724E-02,-0.21723174E-02, 0.13029544E-02, 0.19180871E-02,
         0.14558476E-02,-0.10901046E-02,-0.13341472E-02,-0.15758885E-02,
         0.27215439E-02,-0.58744015E-03,-0.71338669E-03,-0.10507439E-02,
        -0.35242346E-03, 0.11264699E-02,-0.84579299E-03, 0.10834723E-02,
         0.60349697E-03,-0.64471546E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x32        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]        *x34*x42    
        +coeff[ 12]    *x21    *x41*x54
        +coeff[ 13]    *x21*x31        
        +coeff[ 14]        *x31*x41    
        +coeff[ 15]    *x21        *x51
        +coeff[ 16]                *x52
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 17]*x11    *x31        
        +coeff[ 18]        *x33        
        +coeff[ 19]    *x21*x31*x41    
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11        *x41*x51
        +coeff[ 22]    *x21        *x53
        +coeff[ 23]*x11    *x32*x41    
        +coeff[ 24]*x11        *x43    
        +coeff[ 25]*x11*x21    *x41*x51
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 26]*x12*x21        *x51
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]        *x31*x43*x51
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]        *x31*x42*x52
        +coeff[ 32]            *x43*x52
        +coeff[ 33]        *x31*x41*x53
        +coeff[ 34]            *x42*x53
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 35]*x11*x22*x32        
        +coeff[ 36]*x11*x22*x31*x41    
        +coeff[ 37]*x11    *x31*x43    
        +coeff[ 38]*x11    *x32*x41*x51
        +coeff[ 39]*x11    *x31*x42*x51
        +coeff[ 40]*x11*x22        *x52
        +coeff[ 41]*x11    *x31*x41*x52
        +coeff[ 42]*x11    *x31    *x53
        +coeff[ 43]*x12*x21*x31*x41    
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 44]*x12    *x31*x41*x51
        +coeff[ 45]*x12        *x42*x51
        +coeff[ 46]*x12    *x31    *x52
        +coeff[ 47]*x13    *x31*x41    
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]*x13        *x42    
        +coeff[ 50]    *x22*x32*x42    
        +coeff[ 51]*x13        *x41*x51
        +coeff[ 52]    *x24        *x52
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 53]    *x23    *x41*x52
        +coeff[ 54]    *x21*x31*x41*x53
        +coeff[ 55]        *x31*x41*x54
        +coeff[ 56]*x11*x22*x32    *x51
        +coeff[ 57]*x11*x21*x31*x42*x51
        +coeff[ 58]*x11*x21    *x43*x51
        +coeff[ 59]*x11*x22        *x53
        +coeff[ 60]*x12*x22*x31*x41    
        +coeff[ 61]*x12    *x33*x41    
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 62]*x12    *x32*x42    
        +coeff[ 63]*x12*x21*x32    *x51
        +coeff[ 64]    *x24*x33        
        +coeff[ 65]    *x23*x32    *x52
        +coeff[ 66]    *x22*x32*x41*x52
        +coeff[ 67]        *x32*x43*x52
        +coeff[ 68]    *x21*x32    *x54
        +coeff[ 69]*x11*x23*x31*x42    
        +coeff[ 70]*x11*x21*x33*x42    
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 71]*x11*x23    *x43    
        +coeff[ 72]*x11*x22*x31*x43    
        +coeff[ 73]*x11*x23*x31*x41*x51
        +coeff[ 74]*x11*x21*x33*x41*x51
        +coeff[ 75]*x11*x21*x31*x43*x51
        +coeff[ 76]*x11*x24        *x52
        +coeff[ 77]*x11    *x33*x41*x52
        +coeff[ 78]*x11*x21*x31*x42*x52
        +coeff[ 79]*x11*x21*x31*x41*x53
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 80]*x11    *x31*x41*x54
        +coeff[ 81]*x12*x23*x32        
        +coeff[ 82]*x12*x22*x33        
        +coeff[ 83]*x12*x22    *x42*x51
        +coeff[ 84]*x13*x24            
        +coeff[ 85]*x13*x23*x31        
        +coeff[ 86]*x13*x21*x33        
        +coeff[ 87]    *x24*x34        
        +coeff[ 88]    *x23*x34*x41    
    ;
    v_l_e_q1q2_1_600                              =v_l_e_q1q2_1_600                              
        +coeff[ 89]    *x22*x34*x42    
        ;

    return v_l_e_q1q2_1_600                              ;
}
float x_e_q1q2_1_500                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.1299748E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.12953004E-02, 0.12222473E+00, 0.17772624E-02, 0.25055637E-02,
        -0.36667171E-03, 0.81520600E-04,-0.51598559E-03,-0.41277989E-03,
        -0.32149217E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_500                              =v_x_e_q1q2_1_500                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_500                              ;
}
float t_e_q1q2_1_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5152437E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.51615370E-04,-0.19998760E-02,-0.26169843E-02, 0.23070611E-02,
         0.72188712E-04,-0.17731779E-03,-0.27310630E-03,-0.31981090E-03,
         0.37575667E-05,-0.23804580E-03,-0.13743712E-03,-0.88400389E-04,
        -0.81048009E-03,-0.63968881E-03,-0.28255405E-04,-0.52782439E-03,
        -0.44102708E-03,-0.32755106E-04,-0.21048403E-04, 0.55452205E-04,
         0.40664650E-04,-0.22046595E-03,-0.11314025E-04, 0.36697234E-04,
        -0.13430679E-04,-0.88246270E-05, 0.73727556E-05, 0.69334424E-05,
        -0.39699280E-05,-0.72099251E-05,-0.63930011E-05, 0.44840053E-05,
         0.61991800E-05, 0.55245637E-05,-0.79143892E-05, 0.90376761E-05,
         0.10443323E-04, 0.14598070E-04, 0.16116532E-04, 0.14749211E-04,
        -0.14409776E-04, 0.14083149E-04, 0.67947308E-05,-0.12899437E-04,
        -0.12433452E-04,-0.11284468E-04, 0.13091094E-04, 0.11228127E-04,
         0.88504439E-05, 0.17688606E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_500                              =v_t_e_q1q2_1_500                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x32*x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_1_500                              =v_t_e_q1q2_1_500                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x33*x42    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x22    *x41    
    ;
    v_t_e_q1q2_1_500                              =v_t_e_q1q2_1_500                              
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x22        *x51
        +coeff[ 28]*x11            *x52
        +coeff[ 29]*x12*x21    *x41    
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x12*x21        *x51
        +coeff[ 33]*x11*x22        *x51
        +coeff[ 34]*x11*x21    *x41*x51
    ;
    v_t_e_q1q2_1_500                              =v_t_e_q1q2_1_500                              
        +coeff[ 35]*x13*x22            
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x11*x22*x32        
        +coeff[ 38]*x13*x21    *x41    
        +coeff[ 39]    *x22*x32*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12    *x31*x42    
        +coeff[ 42]*x12        *x43    
        +coeff[ 43]*x11*x21    *x43    
    ;
    v_t_e_q1q2_1_500                              =v_t_e_q1q2_1_500                              
        +coeff[ 44]*x11*x21*x32    *x51
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]*x12*x21*x33        
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]*x11*x22*x32    *x51
        ;

    return v_t_e_q1q2_1_500                              ;
}
float y_e_q1q2_1_500                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2755038E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.24682088E-03, 0.13201284E+00, 0.88286422E-01,-0.16116556E-02,
        -0.14707211E-02,-0.96159849E-04, 0.23568264E-04,-0.17509845E-03,
         0.17639554E-03,-0.90420879E-04, 0.11274548E-03, 0.75745280E-04,
         0.71762443E-04, 0.62983752E-04,-0.22153837E-03, 0.19741365E-05,
        -0.61421088E-04,-0.60309463E-04, 0.27445700E-04, 0.42100851E-05,
         0.72662974E-05,-0.11554821E-04, 0.40519346E-04, 0.37892863E-04,
        -0.92153095E-04,-0.44741315E-04,-0.18414723E-04, 0.20749269E-04,
        -0.17314112E-04,-0.47630900E-04, 0.26630965E-04,-0.24831696E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_500                              =v_y_e_q1q2_1_500                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x23    *x41    
    ;
    v_y_e_q1q2_1_500                              =v_y_e_q1q2_1_500                              
        +coeff[ 17]*x11*x23*x31        
        +coeff[ 18]        *x31*x45*x52
        +coeff[ 19]    *x21    *x41    
        +coeff[ 20]            *x42*x51
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_1_500                              =v_y_e_q1q2_1_500                              
        +coeff[ 26]*x11*x21    *x42*x51
        +coeff[ 27]    *x22*x31*x43    
        +coeff[ 28]*x12*x21*x31*x41    
        +coeff[ 29]    *x21*x31*x43*x51
        +coeff[ 30]*x11*x21    *x41*x52
        +coeff[ 31]    *x23    *x42*x51
        ;

    return v_y_e_q1q2_1_500                              ;
}
float p_e_q1q2_1_500                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.1764231E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.16076036E-03, 0.32387871E-01, 0.66794761E-01,-0.15771830E-02,
         0.45756528E-05,-0.14029634E-02,-0.31325748E-03,-0.37581511E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x33    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_500                              ;
}
float l_e_q1q2_1_500                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1823896E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18102328E-02,-0.34822894E-02,-0.61611540E-03,-0.29228737E-02,
         0.74799330E-03,-0.10631564E-02,-0.44954379E-03,-0.10391183E-02,
        -0.26687756E-03,-0.18666212E-02,-0.36002948E-04,-0.12571074E-02,
         0.11754313E-02, 0.59620757E-03, 0.13580384E-02, 0.52866613E-03,
         0.10215299E-02, 0.17770011E-02, 0.20618495E-02,-0.15508242E-03,
         0.18058853E-03,-0.15377026E-02, 0.36355708E-04,-0.41943604E-04,
         0.67024003E-03, 0.19750379E-03,-0.10086360E-02, 0.31872391E-03,
         0.20476746E-03,-0.27777909E-03,-0.62504649E-03, 0.29950266E-03,
        -0.38540876E-03, 0.87847526E-03,-0.12243795E-02,-0.20170095E-03,
        -0.15298756E-02, 0.10994300E-02, 0.48521580E-03,-0.11490356E-02,
         0.54682640E-03,-0.16016072E-02, 0.21258678E-03, 0.24636954E-03,
        -0.84506458E-03, 0.61673828E-03, 0.58014437E-04, 0.85936458E-03,
        -0.48592113E-03, 0.95976115E-03, 0.79801842E-03, 0.11833189E-02,
        -0.96951822E-04, 0.15488416E-02, 0.32545949E-03,-0.10231802E-02,
         0.52380440E-03, 0.10314470E-02, 0.36261266E-03, 0.87613647E-03,
        -0.14084928E-02, 0.38543262E-03,-0.85995608E-03, 0.79540262E-03,
         0.40692237E-03,-0.65835647E-03, 0.83651964E-03, 0.29323596E-03,
        -0.86690503E-03,-0.41846070E-03,-0.61980361E-03,-0.22005080E-02,
         0.22545850E-02,-0.12172167E-02, 0.49432996E-02, 0.40199603E-02,
         0.13759400E-02, 0.14669623E-02,-0.24278210E-02,-0.16654078E-02,
        -0.10863366E-02,-0.68286713E-03,-0.46085725E-02,-0.15385363E-02,
        -0.15218193E-02, 0.38491489E-03, 0.16037727E-02,-0.15655315E-02,
         0.18842906E-02, 0.15790624E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x32        
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]    *x22    *x42    
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]            *x42*x52
        +coeff[ 11]    *x22*x31*x42    
        +coeff[ 12]*x11*x21*x32    *x51
        +coeff[ 13]    *x24*x31*x41    
        +coeff[ 14]    *x22*x33*x41    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]        *x33*x43    
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 17]        *x31*x41*x54
        +coeff[ 18]    *x21*x33*x41*x52
        +coeff[ 19]            *x41    
        +coeff[ 20]    *x21    *x41    
        +coeff[ 21]        *x31*x41    
        +coeff[ 22]    *x21        *x51
        +coeff[ 23]            *x41*x51
        +coeff[ 24]    *x22    *x41    
        +coeff[ 25]            *x43    
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 26]    *x22        *x51
        +coeff[ 27]            *x42*x51
        +coeff[ 28]                *x53
        +coeff[ 29]*x12        *x41    
        +coeff[ 30]    *x23    *x41    
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]            *x43*x51
        +coeff[ 33]*x11*x22        *x51
        +coeff[ 34]*x11*x21*x31    *x51
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 35]*x12            *x52
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x24        *x51
        +coeff[ 38]    *x21*x31*x42*x51
        +coeff[ 39]        *x32*x42*x51
        +coeff[ 40]        *x32*x41*x52
        +coeff[ 41]    *x21*x31    *x53
        +coeff[ 42]    *x21    *x41*x53
        +coeff[ 43]*x11*x21*x33        
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 44]*x11*x22    *x41*x51
        +coeff[ 45]*x11        *x41*x53
        +coeff[ 46]*x12*x22        *x51
        +coeff[ 47]*x12*x21*x31    *x51
        +coeff[ 48]*x12*x21    *x41*x51
        +coeff[ 49]*x12    *x31*x41*x51
        +coeff[ 50]    *x23*x32*x41    
        +coeff[ 51]    *x22    *x44    
        +coeff[ 52]    *x21*x34    *x51
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 53]    *x23*x31*x41*x51
        +coeff[ 54]    *x23        *x53
        +coeff[ 55]    *x21*x31*x41*x53
        +coeff[ 56]        *x32*x41*x53
        +coeff[ 57]*x11*x24*x31        
        +coeff[ 58]*x11*x23*x32        
        +coeff[ 59]*x11*x23*x31    *x51
        +coeff[ 60]*x11*x22*x32    *x51
        +coeff[ 61]*x11    *x34    *x51
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 62]*x11*x21    *x42*x52
        +coeff[ 63]*x11*x21*x31    *x53
        +coeff[ 64]*x12*x24            
        +coeff[ 65]*x12*x21*x32*x41    
        +coeff[ 66]*x12    *x31*x41*x52
        +coeff[ 67]*x12    *x31    *x53
        +coeff[ 68]*x13*x22*x31        
        +coeff[ 69]*x13*x21*x31*x41    
        +coeff[ 70]*x13*x22        *x51
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 71]    *x24*x32    *x51
        +coeff[ 72]    *x22*x34    *x51
        +coeff[ 73]    *x24*x31*x41*x51
        +coeff[ 74]    *x21*x34*x41*x51
        +coeff[ 75]    *x21*x33*x42*x51
        +coeff[ 76]    *x21*x32*x42*x52
        +coeff[ 77]    *x21*x33    *x53
        +coeff[ 78]    *x21*x32*x41*x53
        +coeff[ 79]    *x22    *x42*x53
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 80]        *x31*x43*x53
        +coeff[ 81]*x11    *x34*x41*x51
        +coeff[ 82]*x11*x21*x32*x42*x51
        +coeff[ 83]*x11*x21*x31*x43*x51
        +coeff[ 84]*x11*x21*x31*x41*x53
        +coeff[ 85]*x12*x22*x33        
        +coeff[ 86]*x12*x24    *x41    
        +coeff[ 87]*x12*x22*x32*x41    
        +coeff[ 88]*x12*x23    *x41*x51
    ;
    v_l_e_q1q2_1_500                              =v_l_e_q1q2_1_500                              
        +coeff[ 89]*x12*x22    *x42*x51
        ;

    return v_l_e_q1q2_1_500                              ;
}
float x_e_q1q2_1_450                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.4769921E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.48539028E-03, 0.12222999E+00, 0.17766767E-02, 0.25011487E-02,
        -0.36340277E-03, 0.81939455E-04,-0.50868397E-03,-0.41110802E-03,
        -0.32171331E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_450                              =v_x_e_q1q2_1_450                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_450                              ;
}
float t_e_q1q2_1_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1901240E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18496510E-04,-0.20035512E-02,-0.26039816E-02, 0.23112895E-02,
         0.71007991E-04,-0.17474272E-03,-0.29636786E-03,-0.33526288E-03,
        -0.88666803E-06,-0.25982197E-03,-0.12986826E-03,-0.79819743E-04,
        -0.78412838E-03,-0.59543102E-03,-0.32001866E-04,-0.27156624E-04,
        -0.49842702E-03,-0.39704610E-03,-0.13373899E-04, 0.56986744E-04,
         0.34027980E-04,-0.18079645E-04,-0.19245357E-04,-0.48780563E-04,
        -0.21337697E-03,-0.26659898E-04, 0.70627289E-05, 0.55470935E-06,
         0.43525797E-05,-0.54975612E-05,-0.54421621E-05,-0.16740233E-06,
         0.58333640E-05, 0.33864046E-04, 0.77666409E-05, 0.94386187E-05,
         0.10609941E-04, 0.19265708E-04, 0.12507550E-04,-0.65360309E-05,
        -0.27179025E-04, 0.87671251E-05, 0.94617872E-05,-0.53337153E-05,
        -0.13672796E-04,-0.10910909E-04,-0.42685657E-04,-0.13462065E-04,
         0.95470195E-05, 0.20816131E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_450                              =v_t_e_q1q2_1_450                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x33*x41    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q1q2_1_450                              =v_t_e_q1q2_1_450                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x11*x23    *x41*x51
    ;
    v_t_e_q1q2_1_450                              =v_t_e_q1q2_1_450                              
        +coeff[ 26]*x11*x21            
        +coeff[ 27]        *x31*x41    
        +coeff[ 28]*x13                
        +coeff[ 29]*x12*x21            
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]            *x41*x52
        +coeff[ 32]*x11*x22*x31        
        +coeff[ 33]*x11*x21*x31*x41    
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_q1q2_1_450                              =v_t_e_q1q2_1_450                              
        +coeff[ 35]*x11*x22        *x51
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]*x13*x22            
        +coeff[ 38]*x11*x21*x33        
        +coeff[ 39]*x11*x23    *x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]    *x22    *x42*x51
        +coeff[ 43]*x13            *x52
    ;
    v_t_e_q1q2_1_450                              =v_t_e_q1q2_1_450                              
        +coeff[ 44]    *x23        *x52
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]*x11*x21*x33*x41    
        +coeff[ 47]*x13*x21*x31    *x51
        +coeff[ 48]*x13        *x42*x51
        +coeff[ 49]    *x23    *x42*x51
        ;

    return v_t_e_q1q2_1_450                              ;
}
float y_e_q1q2_1_450                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.5195382E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.50468702E-03, 0.13201861E+00, 0.88270620E-01,-0.15985479E-02,
        -0.14684731E-02,-0.10432732E-03, 0.42619362E-04,-0.20591519E-03,
         0.18322861E-03,-0.85647676E-04, 0.10902608E-03, 0.62986299E-04,
         0.65510911E-04, 0.63734857E-04,-0.21761480E-03,-0.50816983E-04,
        -0.22368431E-04, 0.73374467E-05,-0.21048120E-05,-0.28698576E-05,
         0.55953255E-05, 0.18501631E-04, 0.23539139E-05, 0.24628111E-04,
        -0.15414143E-04,-0.16159865E-04,-0.12885764E-04,-0.61687249E-04,
         0.13769863E-04,-0.44294913E-04,-0.29595569E-04,-0.19516689E-04,
        -0.27596358E-04, 0.27015310E-04,-0.22960328E-04, 0.33564909E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1q2_1_450                              =v_y_e_q1q2_1_450                              
        +coeff[  8]        *x31*x42    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]*x11*x21    *x43    
    ;
    v_y_e_q1q2_1_450                              =v_y_e_q1q2_1_450                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]            *x44    
        +coeff[ 21]    *x21    *x44    
        +coeff[ 22]*x11        *x42*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]*x11*x21    *x41*x51
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_y_e_q1q2_1_450                              =v_y_e_q1q2_1_450                              
        +coeff[ 26]    *x21    *x43*x51
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x12*x21    *x42    
        +coeff[ 32]*x12    *x31*x42    
        +coeff[ 33]*x11*x21*x33        
        +coeff[ 34]            *x45*x51
    ;
    v_y_e_q1q2_1_450                              =v_y_e_q1q2_1_450                              
        +coeff[ 35]    *x22    *x43*x51
        ;

    return v_y_e_q1q2_1_450                              ;
}
float p_e_q1q2_1_450                              (float *x,int m){
    int ncoeff=  8;
    float avdat= -0.2386774E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  9]={
         0.23090529E-03, 0.32377027E-01, 0.66790096E-01,-0.15754760E-02,
         0.18346715E-04,-0.14038906E-02,-0.31543322E-03,-0.37573304E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]    *x22*x31    *x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_450                              ;
}
float l_e_q1q2_1_450                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1860887E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17310039E-02,-0.31842799E-02,-0.27012386E-03,-0.31933761E-02,
        -0.61055220E-03, 0.69964438E-03,-0.35253758E-03, 0.11726682E-02,
         0.59803965E-03, 0.23310730E-03, 0.28795618E-03,-0.68103784E-03,
         0.25124871E-03, 0.64202736E-03,-0.21909433E-02, 0.38260138E-02,
        -0.82564453E-03, 0.63937501E-03, 0.56804129E-03, 0.40568416E-02,
         0.16640873E-03, 0.12590356E-03,-0.11127215E-04,-0.27198552E-02,
         0.24112426E-03, 0.35957026E-03,-0.25300174E-04,-0.46227162E-03,
        -0.67648897E-03,-0.13159238E-04,-0.17737597E-04, 0.11170662E-03,
         0.31607700E-03, 0.22236980E-03, 0.69200841E-03, 0.10362882E-03,
        -0.36427999E-03,-0.92678203E-03,-0.35277853E-03,-0.29558997E-03,
        -0.54031744E-03,-0.72479458E-03,-0.10539352E-03, 0.10951166E-03,
         0.35423867E-02, 0.43262940E-03,-0.48350176E-03, 0.34096089E-03,
        -0.25365062E-03,-0.42547216E-03,-0.12800122E-02, 0.69179886E-03,
         0.33540453E-03, 0.19051472E-02, 0.99647767E-03,-0.82299870E-03,
         0.44448389E-03, 0.88470982E-03,-0.24757051E-03, 0.97198563E-03,
         0.55055507E-03, 0.92598895E-03,-0.31303891E-03,-0.58726995E-03,
        -0.16686895E-03, 0.69534592E-03,-0.21969236E-02,-0.13861024E-02,
        -0.16023726E-02, 0.16005599E-02,-0.48654623E-03, 0.75154233E-03,
        -0.14916388E-02, 0.40089004E-03,-0.90572191E-03, 0.22327197E-02,
        -0.70084346E-03, 0.21512837E-03, 0.11853474E-02, 0.11855437E-02,
         0.10038158E-02,-0.18079523E-02,-0.25934631E-02, 0.14937951E-02,
        -0.92222699E-03, 0.90107752E-03,-0.26652403E-03,-0.61735429E-03,
        -0.98214566E-03, 0.18133533E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]*x11            *x52
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x32*x42    
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]            *x44    
        +coeff[ 10]*x11    *x32*x41*x51
        +coeff[ 11]    *x24*x32        
        +coeff[ 12]    *x22*x34        
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]        *x33*x43    
        +coeff[ 16]*x12*x22    *x42    
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 17]*x13    *x31*x42    
        +coeff[ 18]*x11*x23    *x42*x51
        +coeff[ 19]        *x34*x44    
        +coeff[ 20]                *x51
        +coeff[ 21]*x11                
        +coeff[ 22]    *x21    *x41    
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x41*x51
        +coeff[ 25]    *x23            
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]        *x32*x41    
        +coeff[ 28]    *x21    *x42    
        +coeff[ 29]    *x22        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]    *x21    *x41*x51
        +coeff[ 32]            *x42*x51
        +coeff[ 33]*x11*x21*x31        
        +coeff[ 34]*x11    *x31*x41    
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 35]*x12    *x31        
        +coeff[ 36]*x12            *x51
        +coeff[ 37]    *x22    *x42    
        +coeff[ 38]    *x22*x31    *x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]*x11*x21*x32        
        +coeff[ 41]*x12        *x41*x51
        +coeff[ 42]*x12            *x52
        +coeff[ 43]    *x22*x33        
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x21*x31*x43    
        +coeff[ 46]    *x22*x32    *x51
        +coeff[ 47]    *x21    *x42*x52
        +coeff[ 48]            *x43*x52
        +coeff[ 49]            *x42*x53
        +coeff[ 50]*x11    *x33*x41    
        +coeff[ 51]*x12    *x32    *x51
        +coeff[ 52]    *x24*x31*x41    
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 53]    *x24    *x42    
        +coeff[ 54]    *x22*x33    *x51
        +coeff[ 55]    *x23    *x42*x51
        +coeff[ 56]*x13            *x52
        +coeff[ 57]        *x32*x42*x52
        +coeff[ 58]    *x22        *x54
        +coeff[ 59]*x11*x21*x34        
        +coeff[ 60]*x11*x23    *x42    
        +coeff[ 61]*x11*x21*x31*x41*x52
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 62]*x12    *x33    *x51
        +coeff[ 63]*x12*x22    *x41*x51
        +coeff[ 64]*x12*x21        *x53
        +coeff[ 65]*x12        *x41*x53
        +coeff[ 66]    *x24*x32*x41    
        +coeff[ 67]    *x23*x32*x42    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]    *x21*x32*x44    
        +coeff[ 70]*x13    *x32    *x51
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 71]    *x24*x32    *x51
        +coeff[ 72]    *x22*x34    *x51
        +coeff[ 73]*x13*x21    *x41*x51
        +coeff[ 74]    *x24*x31*x41*x51
        +coeff[ 75]    *x21*x32*x42*x52
        +coeff[ 76]        *x33*x42*x52
        +coeff[ 77]*x13            *x53
        +coeff[ 78]    *x21*x31*x41*x54
        +coeff[ 79]*x11*x22*x31*x41*x52
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 80]*x11*x21*x32*x41*x52
        +coeff[ 81]*x11    *x32*x41*x53
        +coeff[ 82]*x12*x21*x32*x42    
        +coeff[ 83]*x12*x21    *x44    
        +coeff[ 84]*x12*x23        *x52
        +coeff[ 85]*x12*x21*x32    *x52
        +coeff[ 86]*x13    *x34        
        +coeff[ 87]*x13*x22*x31    *x51
        +coeff[ 88]*x13*x21*x31*x41*x51
    ;
    v_l_e_q1q2_1_450                              =v_l_e_q1q2_1_450                              
        +coeff[ 89]    *x23    *x44*x51
        ;

    return v_l_e_q1q2_1_450                              ;
}
float x_e_q1q2_1_449                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1050435E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.10373222E-02, 0.12272488E+00, 0.17937107E-02, 0.24810890E-02,
        -0.35768782E-03, 0.82466104E-04,-0.55523863E-03,-0.39840577E-03,
        -0.15917461E-03,-0.31194271E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_449                              =v_x_e_q1q2_1_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_449                              ;
}
float t_e_q1q2_1_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6145153E-05;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59541549E-05,-0.19857050E-02,-0.21436731E-02, 0.22858968E-02,
         0.71884555E-04,-0.17909909E-03,-0.30729937E-03,-0.31985325E-03,
         0.19685061E-04,-0.32312260E-03,-0.17214419E-03,-0.84158637E-04,
        -0.91229181E-03,-0.63881249E-03,-0.22183925E-04, 0.67300306E-06,
        -0.13522153E-04,-0.12717977E-04, 0.23423656E-04, 0.57278703E-04,
         0.34237117E-04,-0.53045937E-05,-0.27768328E-03,-0.58055128E-03,
        -0.43935550E-03,-0.14147279E-04,-0.18434674E-05,-0.52921609E-05,
        -0.11707219E-05,-0.86461296E-05,-0.12644754E-04, 0.31874029E-05,
         0.46421528E-05, 0.13940001E-04, 0.15314585E-04,-0.15591591E-04,
         0.23667837E-04,-0.99066674E-05,-0.13984664E-04,-0.45370063E-04,
        -0.35693618E-04, 0.11107290E-04,-0.64064538E-05, 0.10983474E-04,
        -0.84198482E-05,-0.17822360E-04, 0.10207314E-04, 0.59295171E-05,
        -0.18367327E-04, 0.20385107E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_449                              =v_t_e_q1q2_1_449                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x22*x31        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_449                              =v_t_e_q1q2_1_449                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x31    *x51
    ;
    v_t_e_q1q2_1_449                              =v_t_e_q1q2_1_449                              
        +coeff[ 26]    *x23*x32    *x51
        +coeff[ 27]    *x21*x31        
        +coeff[ 28]*x13                
        +coeff[ 29]*x12*x21            
        +coeff[ 30]*x11    *x32        
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]*x11    *x32*x41    
        +coeff[ 33]    *x21*x31*x42    
        +coeff[ 34]    *x21    *x43    
    ;
    v_t_e_q1q2_1_449                              =v_t_e_q1q2_1_449                              
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]    *x22*x33        
        +coeff[ 38]*x13    *x31*x41    
        +coeff[ 39]*x11*x22*x31*x41    
        +coeff[ 40]*x11*x22    *x42    
        +coeff[ 41]*x12*x22        *x51
        +coeff[ 42]*x12    *x32    *x51
        +coeff[ 43]    *x21*x31*x42*x51
    ;
    v_t_e_q1q2_1_449                              =v_t_e_q1q2_1_449                              
        +coeff[ 44]    *x22*x33*x41    
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]*x11*x21*x31*x43    
        +coeff[ 47]*x11*x23*x31    *x51
        +coeff[ 48]*x11*x21*x33    *x51
        +coeff[ 49]    *x22*x33    *x51
        ;

    return v_t_e_q1q2_1_449                              ;
}
float y_e_q1q2_1_449                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.5669281E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.57668181E-03, 0.13166462E+00, 0.97759232E-01,-0.16145472E-02,
         0.14530252E-05,-0.15951497E-02,-0.43623848E-04,-0.40141269E-04,
        -0.19034649E-03, 0.23900400E-03,-0.39488081E-04, 0.12681357E-03,
         0.11863675E-03, 0.61253384E-04, 0.48879003E-04,-0.27288531E-03,
        -0.22713335E-04, 0.34221484E-05,-0.28450738E-03,-0.11282501E-03,
         0.44723325E-04,-0.32838718E-04,-0.58870351E-05, 0.44230885E-04,
         0.26609867E-04,-0.22301283E-03, 0.12753938E-04, 0.27452135E-04,
         0.24742931E-04,-0.31145082E-04,-0.40374405E-04, 0.29112407E-04,
        -0.27309032E-04,-0.53438700E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_449                              =v_y_e_q1q2_1_449                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_449                              =v_y_e_q1q2_1_449                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x24*x31    *x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]            *x44    
        +coeff[ 23]*x11*x21*x31*x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1q2_1_449                              =v_y_e_q1q2_1_449                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]*x11        *x43*x51
        +coeff[ 28]*x11    *x33*x41    
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x22    *x41*x51
        +coeff[ 31]        *x33    *x52
        +coeff[ 32]*x11    *x31*x41*x52
        +coeff[ 33]*x11*x23*x31*x41    
        ;

    return v_y_e_q1q2_1_449                              ;
}
float p_e_q1q2_1_449                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3147566E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.31811881E-03, 0.35666779E-01, 0.66461354E-01,-0.15397263E-02,
        -0.15610921E-02,-0.34012849E-03,-0.36327686E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_449                              ;
}
float l_e_q1q2_1_449                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1806016E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18634992E-02, 0.10549877E-03,-0.33915248E-02,-0.66060945E-03,
        -0.33082801E-02,-0.27304087E-03, 0.29483379E-03, 0.12373884E-03,
        -0.89027779E-03, 0.12792326E-02, 0.19646192E-03,-0.24433882E-03,
        -0.52977010E-03,-0.57956745E-03,-0.35046524E-03, 0.83537316E-05,
        -0.25490769E-02,-0.19058012E-03, 0.86789049E-04,-0.11283014E-02,
         0.19501860E-03,-0.47700104E-03, 0.64450712E-03, 0.12201941E-03,
         0.16346904E-03, 0.22929374E-02, 0.31109477E-03, 0.55844727E-03,
        -0.85658983E-04, 0.10941329E-02,-0.10482285E-02,-0.72472241E-04,
         0.30549330E-03, 0.67565899E-03, 0.11944163E-02,-0.21663542E-03,
         0.76640735E-03, 0.74303983E-03, 0.49493229E-03, 0.54026395E-03,
        -0.75121585E-03, 0.77511475E-03,-0.30708697E-03,-0.50193525E-03,
         0.73988724E-03,-0.74315950E-03,-0.85529807E-03, 0.10156373E-02,
         0.15013310E-02, 0.33767958E-03, 0.51788543E-03,-0.17973009E-02,
        -0.65002905E-03,-0.12036832E-02,-0.16350059E-02, 0.16497865E-02,
         0.75450196E-03,-0.41403106E-03,-0.18822316E-02, 0.16753594E-02,
        -0.38871611E-04, 0.15566356E-02, 0.12365982E-02,-0.12135459E-02,
        -0.95667131E-03, 0.50919486E-03,-0.62208925E-03,-0.54521899E-03,
         0.69673371E-03,-0.24248981E-02, 0.96789980E-03,-0.57450478E-03,
        -0.43090424E-03,-0.53831941E-03, 0.29482343E-02,-0.17516736E-02,
        -0.93125377E-03,-0.10683178E-02, 0.41423517E-03, 0.91053866E-03,
         0.13113233E-02,-0.18088744E-02,-0.16254218E-02, 0.64500602E-03,
         0.74059918E-03,-0.14588621E-02,-0.92413183E-03, 0.30706862E-02,
         0.10677517E-03, 0.21334698E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x23*x31        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]    *x24*x31*x41    
        +coeff[ 11]    *x22*x33*x41    
        +coeff[ 12]    *x24    *x42    
        +coeff[ 13]        *x34*x42    
        +coeff[ 14]        *x32*x44    
        +coeff[ 15]            *x41    
        +coeff[ 16]        *x31*x41    
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 17]            *x41*x51
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]    *x22*x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]*x12    *x31        
        +coeff[ 23]    *x21*x33        
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 26]        *x31*x42*x51
        +coeff[ 27]            *x42*x52
        +coeff[ 28]                *x54
        +coeff[ 29]*x11*x22    *x41    
        +coeff[ 30]*x11    *x32*x41    
        +coeff[ 31]*x11*x22        *x51
        +coeff[ 32]*x11        *x42*x51
        +coeff[ 33]*x11    *x31    *x52
        +coeff[ 34]*x12    *x31*x41    
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 35]*x13    *x31        
        +coeff[ 36]    *x24*x31        
        +coeff[ 37]    *x22*x32*x41    
        +coeff[ 38]        *x34*x41    
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21    *x44    
        +coeff[ 41]    *x24        *x51
        +coeff[ 42]    *x21*x32    *x52
        +coeff[ 43]*x11*x21*x33        
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 44]*x11*x21*x32*x41    
        +coeff[ 45]*x11    *x33    *x51
        +coeff[ 46]*x12    *x33        
        +coeff[ 47]*x12*x21*x31*x41    
        +coeff[ 48]*x12*x21    *x42    
        +coeff[ 49]*x12*x21    *x41*x51
        +coeff[ 50]*x13    *x31    *x51
        +coeff[ 51]        *x34*x41*x51
        +coeff[ 52]    *x22*x31*x42*x51
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 53]    *x21*x33    *x52
        +coeff[ 54]        *x33*x41*x52
        +coeff[ 55]    *x21*x31*x42*x52
        +coeff[ 56]    *x21*x31    *x54
        +coeff[ 57]*x11*x21*x34        
        +coeff[ 58]*x11*x24    *x41    
        +coeff[ 59]*x11    *x34*x41    
        +coeff[ 60]*x11*x22*x31*x41*x51
        +coeff[ 61]*x11*x22    *x42*x51
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 62]*x11*x21*x31*x42*x51
        +coeff[ 63]*x11        *x44*x51
        +coeff[ 64]*x11*x22*x31    *x52
        +coeff[ 65]*x11*x21    *x42*x52
        +coeff[ 66]*x11*x21*x31    *x53
        +coeff[ 67]*x12*x22*x32        
        +coeff[ 68]*x12    *x34        
        +coeff[ 69]*x12*x22*x31*x41    
        +coeff[ 70]*x12*x23        *x51
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 71]*x12*x21*x32    *x51
        +coeff[ 72]*x12*x22        *x52
        +coeff[ 73]*x12*x21        *x53
        +coeff[ 74]    *x22*x33*x42    
        +coeff[ 75]    *x23*x31*x43    
        +coeff[ 76]        *x31*x44*x52
        +coeff[ 77]        *x32*x41*x54
        +coeff[ 78]            *x43*x54
        +coeff[ 79]*x11*x23*x33        
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 80]*x11*x22*x31*x43    
        +coeff[ 81]*x11*x22*x31*x42*x51
        +coeff[ 82]*x12*x23    *x42    
        +coeff[ 83]*x12    *x33    *x52
        +coeff[ 84]*x13*x22    *x42    
        +coeff[ 85]    *x24*x33    *x51
        +coeff[ 86]*x13    *x32*x41*x51
        +coeff[ 87]    *x22*x33*x42*x51
        +coeff[ 88]    *x21*x34*x41*x52
    ;
    v_l_e_q1q2_1_449                              =v_l_e_q1q2_1_449                              
        +coeff[ 89]    *x21*x32*x41*x54
        ;

    return v_l_e_q1q2_1_449                              ;
}
float x_e_q1q2_1_400                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2021944E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.21607155E-03, 0.12275229E+00, 0.17969689E-02, 0.24821479E-02,
        -0.28421369E-03, 0.83221501E-04,-0.63518161E-03,-0.43551851E-03,
        -0.23026510E-03,-0.28271091E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_400                              =v_x_e_q1q2_1_400                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_1_400                              ;
}
float t_e_q1q2_1_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1810953E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.17565721E-04,-0.19838361E-02,-0.20664306E-02, 0.22847576E-02,
         0.71273113E-04,-0.18437323E-03,-0.32736498E-03,-0.32899584E-03,
         0.13068733E-05,-0.28615823E-03,-0.18526147E-03,-0.81728285E-04,
        -0.85013144E-03,-0.60268928E-03,-0.14372839E-04, 0.65499269E-04,
        -0.43498244E-03,-0.22693266E-04,-0.10186601E-04, 0.35528068E-04,
         0.12727868E-04,-0.18996050E-04, 0.91489455E-05, 0.15132920E-04,
        -0.27149994E-03,-0.56672742E-03,-0.16293172E-04, 0.19754845E-04,
        -0.18242892E-04, 0.39000988E-05, 0.90227923E-05,-0.31302989E-05,
        -0.34603499E-05,-0.12433380E-04, 0.13817867E-05,-0.77435325E-05,
        -0.53446361E-05,-0.76192509E-05, 0.15114475E-04, 0.11228591E-04,
        -0.67146034E-05,-0.15346244E-04, 0.13206264E-04,-0.39493916E-04,
        -0.80247164E-05,-0.28907607E-04, 0.73566775E-05, 0.13978944E-04,
        -0.15389031E-04, 0.88425659E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_400                              =v_t_e_q1q2_1_400                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q1q2_1_400                              =v_t_e_q1q2_1_400                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]*x13*x21*x31        
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x21*x33        
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_t_e_q1q2_1_400                              =v_t_e_q1q2_1_400                              
        +coeff[ 26]*x11        *x42*x52
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]*x11*x21    *x41    
        +coeff[ 30]    *x21    *x41*x51
        +coeff[ 31]*x12*x21*x31        
        +coeff[ 32]*x12    *x32        
        +coeff[ 33]*x11*x21*x32        
        +coeff[ 34]*x12*x21    *x41    
    ;
    v_t_e_q1q2_1_400                              =v_t_e_q1q2_1_400                              
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]*x11    *x31*x42    
        +coeff[ 37]    *x21    *x43    
        +coeff[ 38]    *x21*x32    *x51
        +coeff[ 39]*x11*x21        *x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]*x13*x22            
        +coeff[ 42]*x12*x22    *x41    
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_q1q2_1_400                              =v_t_e_q1q2_1_400                              
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x11*x23        *x51
        +coeff[ 47]*x11*x22*x31    *x51
        +coeff[ 48]*x12*x21    *x41*x51
        +coeff[ 49]*x11*x22    *x41*x51
        ;

    return v_t_e_q1q2_1_400                              ;
}
float y_e_q1q2_1_400                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.5850978E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.62729971E-03, 0.13167922E+00, 0.97710125E-01,-0.16087524E-02,
        -0.10774933E-04,-0.15788589E-02,-0.54096738E-04,-0.41149648E-04,
        -0.22018344E-03, 0.24734507E-03,-0.10915406E-04, 0.12644702E-03,
         0.12570751E-03, 0.77866673E-04, 0.66528199E-04,-0.28043339E-03,
        -0.18137787E-04,-0.37329060E-04, 0.81862263E-05,-0.27544203E-03,
        -0.86362372E-04,-0.57691241E-05,-0.18832505E-04,-0.44046876E-04,
        -0.20062174E-04,-0.11210052E-04,-0.16433463E-04, 0.10488674E-04,
         0.24113109E-04, 0.11651377E-04,-0.22099043E-03,-0.11405226E-04,
         0.53237505E-04,-0.36245390E-04,-0.27552051E-04, 0.21578991E-04,
         0.27799899E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1q2_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_400                              =v_y_e_q1q2_1_400                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_400                              =v_y_e_q1q2_1_400                              
        +coeff[ 17]        *x31*x42*x51
        +coeff[ 18]*x11*x21    *x43    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]        *x32*x42    
    ;
    v_y_e_q1q2_1_400                              =v_y_e_q1q2_1_400                              
        +coeff[ 26]*x11    *x31*x42    
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]        *x31*x41*x52
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]*x11        *x41*x52
        +coeff[ 32]    *x22    *x44    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_y_e_q1q2_1_400                              =v_y_e_q1q2_1_400                              
        +coeff[ 35]            *x42*x53
        +coeff[ 36]*x11*x21    *x41*x52
        ;

    return v_y_e_q1q2_1_400                              ;
}
float p_e_q1q2_1_400                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.3482250E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.36770620E-03, 0.35616823E-01, 0.66446953E-01,-0.15391065E-02,
        -0.15582528E-02,-0.33803089E-03,-0.36399666E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_400                              ;
}
float l_e_q1q2_1_400                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1819188E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.19397101E-02,-0.40831794E-02,-0.59718080E-03,-0.30032811E-02,
        -0.15806092E-03, 0.83905288E-04,-0.85337076E-03, 0.23356773E-03,
        -0.17377031E-02, 0.72204397E-03,-0.77436585E-03,-0.15391943E-03,
        -0.28268469E-03,-0.46910948E-03, 0.16766224E-02, 0.71307167E-03,
         0.38182791E-02,-0.51167852E-03, 0.21201712E-02,-0.14873736E-02,
        -0.10577979E-02,-0.13451064E-03,-0.84095576E-04,-0.25960986E-03,
        -0.74821808E-04,-0.20986681E-02, 0.44161480E-03, 0.40457031E-03,
         0.21190362E-02,-0.39634772E-03, 0.52480289E-03, 0.73955528E-03,
        -0.74795804E-04, 0.13543494E-03, 0.40712539E-03,-0.88565292E-04,
        -0.16890632E-03, 0.37499794E-03, 0.58394705E-03,-0.40001021E-03,
        -0.28369899E-03, 0.24412324E-03, 0.17024273E-03,-0.35662288E-03,
        -0.45819597E-04,-0.53025532E-03,-0.32329215E-02, 0.12145326E-02,
         0.57324982E-03,-0.16562172E-02,-0.14377538E-03,-0.82182809E-03,
        -0.80980652E-03,-0.74646069E-03, 0.62862155E-03, 0.76004956E-03,
         0.11629069E-03, 0.11175860E-02,-0.54776785E-03,-0.97635010E-03,
        -0.74725458E-03, 0.38235617E-03, 0.10353788E-02, 0.51605608E-03,
         0.69867139E-03, 0.83755399E-03, 0.26871671E-03, 0.11632249E-02,
        -0.75714558E-03,-0.97442768E-03,-0.56479196E-03,-0.10023657E-02,
         0.16294736E-02,-0.28499367E-03,-0.56871882E-03,-0.11855627E-02,
        -0.29395241E-02, 0.22321560E-02,-0.13003844E-02,-0.19637034E-02,
        -0.23185590E-02,-0.53671753E-03, 0.73858321E-03,-0.67083433E-03,
         0.52248454E-03,-0.90429239E-03,-0.95196266E-03, 0.14567770E-02,
         0.51728589E-03, 0.54080813E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]*x11    *x32        
        +coeff[  5]*x11            *x52
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]            *x42*x52
        +coeff[ 11]*x12    *x31*x41    
        +coeff[ 12]*x12        *x42    
        +coeff[ 13]        *x32*x42*x51
        +coeff[ 14]    *x24*x31*x41    
        +coeff[ 15]    *x22*x33*x41    
        +coeff[ 16]    *x22*x32*x42    
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 17]        *x34*x42    
        +coeff[ 18]    *x22*x31*x43    
        +coeff[ 19]        *x32*x44    
        +coeff[ 20]*x13*x22*x31        
        +coeff[ 21]        *x31        
        +coeff[ 22]            *x41    
        +coeff[ 23]                *x51
        +coeff[ 24]*x11                
        +coeff[ 25]        *x31*x41    
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 26]            *x41*x51
        +coeff[ 27]    *x22*x31        
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]        *x32*x41    
        +coeff[ 30]    *x21    *x42    
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]    *x21        *x52
        +coeff[ 33]            *x41*x52
        +coeff[ 34]                *x53
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 35]*x11*x21        *x51
        +coeff[ 36]*x11        *x41*x51
        +coeff[ 37]    *x24            
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]            *x41*x53
        +coeff[ 40]                *x54
        +coeff[ 41]*x11*x21*x31    *x51
        +coeff[ 42]*x11    *x32    *x51
        +coeff[ 43]*x11        *x42*x51
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 44]*x12        *x41*x51
        +coeff[ 45]    *x23*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]        *x34*x41    
        +coeff[ 48]    *x22*x31*x42    
        +coeff[ 49]    *x21*x32*x42    
        +coeff[ 50]    *x21*x32*x41*x51
        +coeff[ 51]    *x21*x31*x41*x52
        +coeff[ 52]        *x32    *x53
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 53]*x11*x21*x32    *x51
        +coeff[ 54]*x11*x21    *x42*x51
        +coeff[ 55]*x11*x22        *x52
        +coeff[ 56]*x12*x21    *x41*x51
        +coeff[ 57]*x12    *x31*x41*x51
        +coeff[ 58]*x13*x21    *x41    
        +coeff[ 59]        *x33*x41*x52
        +coeff[ 60]    *x22*x31    *x53
        +coeff[ 61]        *x31*x42*x53
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 62]            *x42*x54
        +coeff[ 63]*x11*x22*x33        
        +coeff[ 64]*x11*x24        *x51
        +coeff[ 65]*x11    *x32*x42*x51
        +coeff[ 66]*x12*x24            
        +coeff[ 67]*x12*x22*x31    *x51
        +coeff[ 68]*x12    *x33    *x51
        +coeff[ 69]*x12    *x32*x41*x51
        +coeff[ 70]*x13*x22        *x51
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 71]    *x24    *x42*x51
        +coeff[ 72]    *x22    *x44*x51
        +coeff[ 73]*x13*x21        *x52
        +coeff[ 74]    *x24*x31    *x52
        +coeff[ 75]    *x23*x32    *x52
        +coeff[ 76]    *x23*x31*x41*x52
        +coeff[ 77]    *x21*x33*x41*x52
        +coeff[ 78]    *x21*x32*x41*x53
        +coeff[ 79]        *x33*x41*x53
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 80]        *x32*x42*x53
        +coeff[ 81]*x11*x24*x32        
        +coeff[ 82]*x11*x21*x32*x43    
        +coeff[ 83]*x11*x23*x31    *x52
        +coeff[ 84]*x11*x21    *x41*x54
        +coeff[ 85]*x12*x24*x31        
        +coeff[ 86]*x12    *x34*x41    
        +coeff[ 87]*x12*x21*x32*x41*x51
        +coeff[ 88]*x12*x22        *x53
    ;
    v_l_e_q1q2_1_400                              =v_l_e_q1q2_1_400                              
        +coeff[ 89]*x13*x21*x33        
        ;

    return v_l_e_q1q2_1_400                              ;
}
float x_e_q1q2_1_350                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2433947E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.24334673E-03, 0.12289883E+00, 0.17999165E-02, 0.24793670E-02,
        -0.35968111E-03, 0.81740196E-04,-0.55216422E-03,-0.39624854E-03,
        -0.14843482E-03,-0.31162793E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_350                              =v_x_e_q1q2_1_350                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_350                              ;
}
float t_e_q1q2_1_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1311512E-04;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12449947E-04,-0.19787527E-02,-0.19665614E-02, 0.22751596E-02,
         0.71883136E-04,-0.17914697E-03,-0.30138923E-03,-0.32248546E-03,
         0.20333666E-04,-0.31016869E-03,-0.41414028E-05,-0.17462783E-03,
        -0.84577419E-04,-0.87374257E-03,-0.60408522E-03,-0.23044850E-04,
        -0.29520985E-04,-0.20292528E-04, 0.78923309E-04,-0.63315895E-03,
        -0.47956521E-03,-0.14683827E-04, 0.33175329E-04, 0.43574066E-04,
        -0.30426664E-03, 0.28246152E-05,-0.56848808E-06,-0.23395469E-05,
        -0.18853317E-05,-0.49882078E-05, 0.95173959E-06, 0.28124423E-05,
        -0.43496871E-05,-0.73419455E-05, 0.24406892E-04,-0.25137051E-05,
        -0.66953908E-05, 0.55188380E-05, 0.17428250E-04,-0.61156293E-05,
         0.83971327E-05, 0.18046576E-04,-0.26299924E-04,-0.17456201E-04,
        -0.11077989E-04, 0.82443312E-05, 0.21501264E-04,-0.13154076E-04,
        -0.25147017E-04, 0.11993521E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_350                              =v_t_e_q1q2_1_350                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_350                              =v_t_e_q1q2_1_350                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21*x32*x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q1q2_1_350                              =v_t_e_q1q2_1_350                              
        +coeff[ 26]        *x31        
        +coeff[ 27]    *x22            
        +coeff[ 28]            *x42    
        +coeff[ 29]*x12*x21            
        +coeff[ 30]    *x22    *x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]*x11            *x52
        +coeff[ 33]*x12*x21*x31        
        +coeff[ 34]*x11*x22*x31        
    ;
    v_t_e_q1q2_1_350                              =v_t_e_q1q2_1_350                              
        +coeff[ 35]*x11    *x33        
        +coeff[ 36]*x12*x21    *x41    
        +coeff[ 37]*x11*x22        *x51
        +coeff[ 38]    *x23        *x51
        +coeff[ 39]*x11*x21    *x41*x51
        +coeff[ 40]*x12*x22    *x41    
        +coeff[ 41]*x12*x21*x31*x41    
        +coeff[ 42]*x11*x22*x31*x41    
        +coeff[ 43]*x11*x22    *x42    
    ;
    v_t_e_q1q2_1_350                              =v_t_e_q1q2_1_350                              
        +coeff[ 44]*x13*x21        *x51
        +coeff[ 45]*x11*x23        *x51
        +coeff[ 46]*x13*x21*x32        
        +coeff[ 47]*x11*x23*x32        
        +coeff[ 48]*x11*x22*x33        
        +coeff[ 49]*x13*x21*x31*x41    
        ;

    return v_t_e_q1q2_1_350                              ;
}
float y_e_q1q2_1_350                              (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.1351502E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.13091493E-02, 0.13146578E+00, 0.97620413E-01,-0.16117464E-02,
        -0.12271614E-04,-0.15733930E-02,-0.11261977E-03, 0.35792993E-04,
        -0.16945526E-03, 0.21304072E-03,-0.10213349E-03, 0.11298277E-03,
         0.93133582E-04, 0.63864856E-04, 0.67967143E-04,-0.23613783E-03,
        -0.10237030E-04, 0.20524701E-05,-0.60988459E-05, 0.28523064E-05,
        -0.26722015E-04, 0.29438650E-04,-0.85522028E-04,-0.57774294E-04,
        -0.24139475E-04,-0.44405744E-04, 0.48547849E-04, 0.16554795E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_350                              =v_y_e_q1q2_1_350                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_350                              =v_y_e_q1q2_1_350                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x21    *x42    
        +coeff[ 19]        *x33        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x12    *x31*x42    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1q2_1_350                              =v_y_e_q1q2_1_350                              
        +coeff[ 26]    *x22    *x43*x51
        +coeff[ 27]*x11*x23    *x42    
        ;

    return v_y_e_q1q2_1_350                              ;
}
float p_e_q1q2_1_350                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.6251067E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.60886651E-03, 0.35540611E-01, 0.66297419E-01,-0.15357023E-02,
        -0.15541046E-02,-0.34149078E-03,-0.36674674E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_350                              ;
}
float l_e_q1q2_1_350                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1860078E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18656322E-02,-0.32757702E-02,-0.10031442E-02,-0.35216236E-02,
         0.91750140E-03,-0.49458322E-03, 0.17344864E-02,-0.51178067E-03,
         0.26391243E-03,-0.80085585E-04,-0.24209609E-02,-0.58184611E-03,
        -0.98774524E-03,-0.56859510E-04,-0.21791506E-03,-0.23938448E-02,
        -0.16672548E-03,-0.17668173E-03,-0.19298407E-02,-0.52642055E-04,
         0.61822204E-04, 0.58204244E-03, 0.35073992E-03, 0.24761859E-03,
         0.76479954E-03,-0.22741997E-03,-0.20000972E-02, 0.49742102E-03,
         0.10607925E-03, 0.24553253E-02, 0.69164939E-03, 0.29663150E-04,
         0.83863467E-03, 0.16701802E-02,-0.30258060E-02,-0.21689430E-03,
        -0.72670245E-03, 0.44342051E-02, 0.13195476E-02, 0.30476239E-03,
         0.31217112E-03,-0.23788921E-03, 0.57128223E-03,-0.41947287E-03,
         0.11495233E-02,-0.13903732E-02,-0.69739996E-04, 0.72734698E-03,
         0.55764255E-03, 0.17270271E-02,-0.14602490E-02, 0.84910309E-03,
        -0.25898288E-02, 0.11373406E-02,-0.26418639E-02, 0.36086575E-02,
        -0.10517802E-02,-0.13028585E-02, 0.21287992E-02,-0.30340366E-02,
         0.10642715E-02,-0.27236829E-02,-0.74442447E-03, 0.27926280E-02,
        -0.12738365E-02, 0.12365578E-02,-0.10040749E-02, 0.11644542E-02,
         0.17422746E-03,-0.77535449E-04, 0.14006822E-03,-0.61986553E-04,
        -0.17632061E-03, 0.69865571E-04,-0.99399360E-04, 0.55389519E-04,
        -0.10465845E-03, 0.13401281E-03, 0.11128646E-03,-0.98573022E-04,
         0.89493828E-04, 0.13362215E-03,-0.13914963E-03,-0.12431527E-03,
         0.29666355E-03,-0.12524433E-03, 0.18843612E-04,-0.19564084E-03,
        -0.38994619E-03,-0.16142015E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x31*x41    
        +coeff[  5]        *x33*x41    
        +coeff[  6]        *x32*x42    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[  8]            *x44    
        +coeff[  9]    *x22*x33*x41    
        +coeff[ 10]        *x34*x42    
        +coeff[ 11]        *x32*x44    
        +coeff[ 12]*x12    *x31    *x54
        +coeff[ 13]                *x51
        +coeff[ 14]    *x21*x31        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]    *x23            
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]*x11*x21*x31        
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]*x13                
        +coeff[ 21]        *x34        
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]*x11*x21    *x41*x51
        +coeff[ 24]*x12    *x31*x41    
        +coeff[ 25]*x12        *x41*x51
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 26]    *x22*x31    *x52
        +coeff[ 27]        *x33    *x52
        +coeff[ 28]*x11*x24            
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x33        
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x21*x32    *x51
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]*x11*x21*x31*x41*x51
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 35]*x11        *x43*x51
        +coeff[ 36]*x11*x22        *x52
        +coeff[ 37]*x11*x21*x31    *x52
        +coeff[ 38]*x13*x21*x31        
        +coeff[ 39]*x13    *x32        
        +coeff[ 40]    *x23*x33        
        +coeff[ 41]    *x23*x31*x42    
        +coeff[ 42]    *x24    *x41*x51
        +coeff[ 43]*x11*x22        *x53
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 44]*x12*x22*x32        
        +coeff[ 45]*x12*x22        *x52
        +coeff[ 46]*x12    *x32    *x52
        +coeff[ 47]*x12            *x54
        +coeff[ 48]    *x22*x31*x43*x51
        +coeff[ 49]    *x24*x31    *x52
        +coeff[ 50]    *x22*x32*x41*x52
        +coeff[ 51]    *x22*x31    *x54
        +coeff[ 52]*x11*x23*x33        
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 53]*x11*x24*x31*x41    
        +coeff[ 54]*x11*x24    *x41*x51
        +coeff[ 55]*x11*x21*x33*x41*x51
        +coeff[ 56]*x11*x23    *x42*x51
        +coeff[ 57]*x11*x22    *x41*x53
        +coeff[ 58]*x11*x21*x31*x41*x53
        +coeff[ 59]*x11*x21*x31    *x54
        +coeff[ 60]*x13*x22        *x52
        +coeff[ 61]*x13*x21*x31    *x52
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 62]*x13*x21    *x41*x52
        +coeff[ 63]    *x23    *x43*x52
        +coeff[ 64]    *x21*x32*x43*x52
        +coeff[ 65]    *x22*x31*x42*x53
        +coeff[ 66]    *x23    *x41*x54
        +coeff[ 67]    *x22    *x42*x54
        +coeff[ 68]    *x21            
        +coeff[ 69]        *x31        
        +coeff[ 70]            *x41    
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 71]        *x31    *x51
        +coeff[ 72]                *x52
        +coeff[ 73]*x11            *x51
        +coeff[ 74]    *x21    *x42    
        +coeff[ 75]        *x31*x42    
        +coeff[ 76]            *x43    
        +coeff[ 77]        *x32    *x51
        +coeff[ 78]        *x31*x41*x51
        +coeff[ 79]    *x21        *x52
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 80]            *x41*x52
        +coeff[ 81]*x11*x21    *x41    
        +coeff[ 82]*x11    *x31*x41    
        +coeff[ 83]*x11    *x31    *x51
        +coeff[ 84]*x11        *x41*x51
        +coeff[ 85]*x11            *x52
        +coeff[ 86]*x12    *x31        
        +coeff[ 87]*x12        *x41    
        +coeff[ 88]    *x22*x32        
    ;
    v_l_e_q1q2_1_350                              =v_l_e_q1q2_1_350                              
        +coeff[ 89]    *x21    *x43    
        ;

    return v_l_e_q1q2_1_350                              ;
}
float x_e_q1q2_1_300                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.8157453E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.83829451E-03, 0.12301857E+00, 0.18040824E-02, 0.24751271E-02,
        -0.27875192E-03, 0.82308470E-04,-0.61403011E-03,-0.41833887E-03,
        -0.21926842E-03,-0.27014804E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_300                              =v_x_e_q1q2_1_300                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_1_300                              ;
}
float t_e_q1q2_1_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1725518E-04;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.18319921E-04,-0.19740756E-02,-0.18333360E-02, 0.22724560E-02,
         0.73181604E-04,-0.16854795E-03,-0.30408162E-03,-0.30809394E-03,
         0.12425146E-04,-0.31154903E-03,-0.17577910E-03,-0.90637259E-04,
        -0.88616775E-03,-0.64580538E-03,-0.17042410E-04,-0.28687640E-04,
        -0.20555768E-04,-0.60433097E-03,-0.45249658E-03, 0.74076088E-05,
        -0.15784261E-04,-0.76321330E-05, 0.65252505E-04, 0.39735198E-04,
         0.16574273E-04,-0.13266736E-04, 0.13799663E-04,-0.28616074E-03,
         0.23857867E-05,-0.12710927E-05,-0.35646658E-05, 0.23491826E-04,
         0.67060387E-05, 0.48057491E-05, 0.53159029E-05,-0.28664879E-04,
        -0.20982139E-04, 0.99837025E-05, 0.12096248E-04,-0.45899383E-05,
         0.86513155E-05, 0.67330725E-05,-0.45322216E-04, 0.37053520E-04,
         0.55097337E-06,-0.30697083E-04, 0.30317446E-04,-0.15585174E-04,
        -0.11487768E-04, 0.10351133E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_300                              =v_t_e_q1q2_1_300                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1q2_1_300                              =v_t_e_q1q2_1_300                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]    *x22            
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x21*x31*x41*x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]*x12*x23            
    ;
    v_t_e_q1q2_1_300                              =v_t_e_q1q2_1_300                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]    *x21*x33*x41    
        +coeff[ 28]    *x23*x32    *x51
        +coeff[ 29]        *x31*x41    
        +coeff[ 30]*x13                
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x11    *x31*x41*x51
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]*x11*x21        *x52
    ;
    v_t_e_q1q2_1_300                              =v_t_e_q1q2_1_300                              
        +coeff[ 35]*x11*x22*x31*x41    
        +coeff[ 36]*x11*x22    *x42    
        +coeff[ 37]*x12*x22        *x51
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x22        *x53
        +coeff[ 40]    *x21*x31    *x53
        +coeff[ 41]    *x21    *x41*x53
        +coeff[ 42]*x12*x22*x31*x41    
        +coeff[ 43]*x11*x23*x31*x41    
    ;
    v_t_e_q1q2_1_300                              =v_t_e_q1q2_1_300                              
        +coeff[ 44]*x11*x21*x33*x41    
        +coeff[ 45]*x12*x22    *x42    
        +coeff[ 46]*x11*x21*x32*x42    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]    *x23    *x43    
        +coeff[ 49]    *x21*x32*x43    
        ;

    return v_t_e_q1q2_1_300                              ;
}
float y_e_q1q2_1_300                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.5269047E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.51837985E-03, 0.13148187E+00, 0.97537152E-01,-0.16062215E-02,
         0.87870203E-05,-0.15899311E-02,-0.62062230E-04,-0.13083278E-04,
        -0.16751089E-03, 0.22228135E-03,-0.69464746E-04, 0.11661265E-03,
         0.10234282E-03, 0.70225746E-04, 0.72376730E-04,-0.26682945E-03,
        -0.88081532E-06,-0.35395504E-04,-0.23239028E-07, 0.56249992E-04,
         0.41397845E-04,-0.10736830E-04,-0.16534539E-03,-0.21445499E-03,
        -0.97483853E-05,-0.81310980E-04,-0.64103871E-04, 0.17734506E-04,
        -0.65674940E-04,-0.22612494E-04, 0.29046942E-04,-0.11536878E-04,
         0.17149903E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_300                              =v_y_e_q1q2_1_300                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_300                              =v_y_e_q1q2_1_300                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]*x11*x21    *x43    
        +coeff[ 19]    *x24    *x41*x51
        +coeff[ 20]    *x24*x31    *x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]*x11        *x41*x52
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1q2_1_300                              =v_y_e_q1q2_1_300                              
        +coeff[ 26]*x11*x23    *x41    
        +coeff[ 27]    *x21    *x42*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x31*x43*x51
        +coeff[ 30]*x13*x21    *x41    
        +coeff[ 31]*x12        *x44    
        +coeff[ 32]    *x21    *x43*x52
        ;

    return v_y_e_q1q2_1_300                              ;
}
float p_e_q1q2_1_300                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2908219E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.28912156E-03, 0.35453930E-01, 0.66261560E-01,-0.15315893E-02,
        -0.15520841E-02,-0.33416014E-03,-0.35694661E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_300                              ;
}
float l_e_q1q2_1_300                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1831643E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17307366E-02,-0.50975879E-04,-0.30375791E-02,-0.47609676E-03,
        -0.28375091E-02,-0.16543503E-03,-0.48790416E-02,-0.36847815E-03,
        -0.84875216E-03,-0.71305415E-03,-0.11696623E-02,-0.58469007E-03,
        -0.32451644E-03,-0.87616261E-03, 0.64980020E-04,-0.13156018E-02,
        -0.45956418E-03, 0.50341263E-02, 0.50143059E-02, 0.28029500E-03,
        -0.15113646E-02, 0.38672434E-03,-0.59965057E-02,-0.55553057E-04,
        -0.84127230E-03, 0.34732441E-03, 0.79681151E-04, 0.30268606E-03,
         0.97407603E-04, 0.13801584E-02, 0.27572263E-04, 0.11756330E-02,
         0.74095966E-03,-0.64875698E-04,-0.34498674E-03, 0.23834271E-03,
         0.40306748E-03, 0.70678815E-03, 0.49369206E-03,-0.13497545E-02,
        -0.25295271E-03, 0.11525449E-02,-0.43222899E-03,-0.69151516E-03,
         0.25526484E-03, 0.89652697E-03,-0.69661572E-03,-0.28244129E-02,
         0.23668685E-02, 0.67651476E-03,-0.62803953E-03,-0.27084004E-03,
         0.48855698E-03,-0.14972094E-02,-0.15598419E-02,-0.20210640E-03,
        -0.83303370E-03, 0.29784674E-03,-0.85173926E-03, 0.13509005E-02,
         0.12321438E-02, 0.15855477E-02,-0.11079496E-02, 0.15211259E-02,
         0.32142099E-03,-0.15352557E-02,-0.80018205E-03, 0.79737330E-03,
         0.27520950E-02,-0.20356630E-02, 0.90831151E-03, 0.88229729E-03,
         0.80748572E-03, 0.35507273E-03, 0.11844522E-02, 0.74961114E-04,
         0.16975961E-04,-0.82607439E-04,-0.15256998E-03, 0.98432865E-04,
         0.96190175E-04, 0.10359187E-03, 0.10091654E-03,-0.24945269E-03,
        -0.44686234E-03,-0.14610765E-03, 0.15477509E-03, 0.84551975E-04,
        -0.27024065E-03,-0.47925752E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]    *x21*x32*x41    
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[  8]        *x33*x41    
        +coeff[  9]    *x22    *x42    
        +coeff[ 10]        *x31*x43    
        +coeff[ 11]        *x31*x41*x52
        +coeff[ 12]            *x42*x52
        +coeff[ 13]*x12    *x31*x41    
        +coeff[ 14]*x12        *x42    
        +coeff[ 15]*x12*x21*x31*x41    
        +coeff[ 16]    *x24*x32        
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 17]    *x24*x31*x41    
        +coeff[ 18]    *x22*x33*x41    
        +coeff[ 19]        *x34*x42    
        +coeff[ 20]        *x32*x44    
        +coeff[ 21]*x13            *x52
        +coeff[ 22]    *x24*x33*x41    
        +coeff[ 23]*x11                
        +coeff[ 24]        *x31*x41    
        +coeff[ 25]*x11*x21            
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 26]*x11    *x31        
        +coeff[ 27]    *x23            
        +coeff[ 28]                *x53
        +coeff[ 29]*x11    *x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]*x12*x21            
        +coeff[ 34]    *x22        *x52
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]        *x31    *x53
        +coeff[ 37]*x11    *x32*x41    
        +coeff[ 38]*x11    *x31*x42    
        +coeff[ 39]*x11        *x41*x52
        +coeff[ 40]*x12*x22            
        +coeff[ 41]*x12*x21        *x51
        +coeff[ 42]*x13*x21            
        +coeff[ 43]    *x24        *x51
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 44]    *x21*x33    *x51
        +coeff[ 45]*x11    *x34        
        +coeff[ 46]*x11    *x33*x41    
        +coeff[ 47]*x11    *x31*x43    
        +coeff[ 48]*x11*x21*x31*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        +coeff[ 50]*x12*x21    *x42    
        +coeff[ 51]*x12*x22        *x51
        +coeff[ 52]*x12    *x31    *x52
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 53]    *x21*x33*x42    
        +coeff[ 54]*x13    *x31    *x51
        +coeff[ 55]    *x21*x34    *x51
        +coeff[ 56]*x13        *x41*x51
        +coeff[ 57]    *x24    *x41*x51
        +coeff[ 58]    *x21    *x43*x52
        +coeff[ 59]*x11        *x41*x54
        +coeff[ 60]*x12*x22*x31*x41    
        +coeff[ 61]*x12*x21*x31*x41*x51
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 62]*x12*x21        *x53
        +coeff[ 63]    *x23*x33*x41    
        +coeff[ 64]    *x24*x31    *x52
        +coeff[ 65]    *x22*x33    *x52
        +coeff[ 66]    *x21    *x44*x52
        +coeff[ 67]    *x24        *x53
        +coeff[ 68]*x11    *x33*x43    
        +coeff[ 69]*x11*x21*x31*x43*x51
        +coeff[ 70]*x11*x21    *x43*x52
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 71]*x12        *x43*x52
        +coeff[ 72]*x12    *x31*x41*x53
        +coeff[ 73]*x13*x23*x31        
        +coeff[ 74]    *x23*x32*x42*x51
        +coeff[ 75]            *x41    
        +coeff[ 76]                *x51
        +coeff[ 77]    *x21        *x51
        +coeff[ 78]        *x31    *x51
        +coeff[ 79]                *x52
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 80]*x11        *x41    
        +coeff[ 81]    *x21*x32        
        +coeff[ 82]        *x33        
        +coeff[ 83]    *x22    *x41    
        +coeff[ 84]    *x21*x31*x41    
        +coeff[ 85]        *x31*x42    
        +coeff[ 86]            *x42*x51
        +coeff[ 87]    *x21        *x52
        +coeff[ 88]            *x41*x52
    ;
    v_l_e_q1q2_1_300                              =v_l_e_q1q2_1_300                              
        +coeff[ 89]*x11    *x32        
        ;

    return v_l_e_q1q2_1_300                              ;
}
float x_e_q1q2_1_250                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1329155E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.13583835E-02, 0.12326034E+00, 0.18107350E-02, 0.24662106E-02,
        -0.35575707E-03, 0.80029051E-04,-0.54774462E-03,-0.40018369E-03,
        -0.16549724E-03,-0.30441562E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_250                              =v_x_e_q1q2_1_250                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_250                              ;
}
float t_e_q1q2_1_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2117849E-04;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.21728376E-04,-0.19712735E-02,-0.16508999E-02, 0.22675991E-02,
         0.70556234E-04,-0.16955331E-03,-0.30631854E-03,-0.30947215E-03,
         0.13552190E-04,-0.31165007E-03,-0.16043799E-04,-0.16454209E-03,
        -0.75390511E-04,-0.88203081E-03,-0.61782263E-03,-0.16444084E-04,
        -0.17825449E-04,-0.12476498E-04,-0.56935952E-03,-0.42172434E-03,
         0.78481817E-05,-0.11039207E-04,-0.98582213E-05, 0.53593249E-04,
        -0.83657178E-04,-0.26270200E-03,-0.62471954E-04, 0.35309829E-04,
         0.24196892E-04, 0.47672129E-04, 0.63708932E-04, 0.11109995E-05,
        -0.29010130E-05, 0.10520910E-04,-0.45499964E-05,-0.51010652E-05,
         0.76937476E-05, 0.20239804E-04,-0.21488802E-05,-0.35316134E-05,
         0.66768021E-05,-0.14926067E-04, 0.14369095E-04,-0.51941443E-05,
         0.53157901E-05, 0.17089520E-04, 0.11216016E-04,-0.16338752E-04,
        -0.93579119E-05,-0.15792451E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1q2_1_250                              =v_t_e_q1q2_1_250                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1q2_1_250                              =v_t_e_q1q2_1_250                              
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]*x11*x22    *x41    
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_1_250                              =v_t_e_q1q2_1_250                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]    *x21*x31*x42*x51
        +coeff[ 28]*x12*x21*x31*x41*x51
        +coeff[ 29]    *x21*x32    *x53
        +coeff[ 30]    *x21*x31*x41*x53
        +coeff[ 31]                *x51
        +coeff[ 32]    *x21*x33        
        +coeff[ 33]    *x23        *x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_t_e_q1q2_1_250                              =v_t_e_q1q2_1_250                              
        +coeff[ 35]*x11    *x32    *x51
        +coeff[ 36]*x11*x21    *x41*x51
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]*x12            *x52
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]*x11            *x53
        +coeff[ 41]*x11*x22*x32        
        +coeff[ 42]*x11*x23    *x41    
        +coeff[ 43]*x11*x21    *x43    
    ;
    v_t_e_q1q2_1_250                              =v_t_e_q1q2_1_250                              
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x21*x32*x41*x51
        +coeff[ 46]*x11*x22        *x52
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]*x11*x21    *x41*x52
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q1q2_1_250                              ;
}
float y_e_q1q2_1_250                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.6822927E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.66975038E-03, 0.13129435E+00, 0.97420551E-01,-0.16014993E-02,
        -0.20628062E-04,-0.15729702E-02,-0.61795210E-04,-0.25053409E-04,
        -0.20441876E-03, 0.21093561E-03,-0.30242805E-04, 0.10222058E-03,
         0.10393707E-03, 0.62818457E-04, 0.65882617E-04,-0.25885631E-03,
        -0.33428965E-04, 0.26358292E-04,-0.11626924E-04,-0.22170429E-04,
        -0.24641291E-03,-0.95530493E-04,-0.42617635E-05,-0.69217235E-05,
         0.65553890E-04, 0.35555968E-04, 0.31724881E-04,-0.18321056E-03,
         0.41395004E-04,-0.21667547E-04,-0.24898789E-04,-0.17789289E-04,
        -0.63077168E-04,-0.31864020E-04,-0.21634849E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_250                              =v_y_e_q1q2_1_250                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_250                              =v_y_e_q1q2_1_250                              
        +coeff[ 17]*x11        *x44    
        +coeff[ 18]*x11*x21    *x43    
        +coeff[ 19]        *x31*x43*x51
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]*x11*x21    *x42    
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q1q2_1_250                              =v_y_e_q1q2_1_250                              
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x11    *x31*x43    
        +coeff[ 29]*x11    *x33*x41    
        +coeff[ 30]    *x21    *x42*x52
        +coeff[ 31]*x12*x22    *x41    
        +coeff[ 32]*x11*x21    *x44    
        +coeff[ 33]*x11*x21    *x41*x52
        +coeff[ 34]    *x21*x31*x45    
        ;

    return v_y_e_q1q2_1_250                              ;
}
float p_e_q1q2_1_250                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.2672976E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.26172071E-03, 0.35332285E-01, 0.66101849E-01,-0.15255976E-02,
        -0.15488359E-02,-0.33221286E-03,-0.35580405E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_250                              ;
}
float l_e_q1q2_1_250                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1801637E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16239091E-02,-0.32538236E-02,-0.46362195E-03,-0.30659752E-02,
         0.69207308E-04, 0.27041967E-03, 0.47272327E-03,-0.12812785E-03,
         0.31016654E-03, 0.85687911E-03,-0.10292272E-03, 0.11593808E-02,
         0.45784202E-03,-0.23584002E-03,-0.54906763E-03, 0.49107429E-03,
         0.74873162E-04,-0.18777864E-03,-0.18068776E-03,-0.20350753E-03,
        -0.59156770E-04, 0.39211431E-04, 0.67909592E-03,-0.11915995E-02,
         0.69494132E-03, 0.70374561E-04,-0.29323988E-02, 0.78862847E-03,
        -0.13682022E-03, 0.16313708E-03,-0.17505368E-03,-0.12910049E-03,
        -0.33398939E-03, 0.16846150E-04,-0.13820551E-03, 0.42566468E-03,
        -0.56307984E-03, 0.68885722E-03, 0.75709038E-04,-0.71917154E-03,
         0.35468899E-03, 0.24421007E-03, 0.19848662E-03,-0.28198864E-03,
        -0.29037768E-03,-0.57809066E-03,-0.38695091E-03, 0.42096508E-03,
        -0.17909963E-02, 0.21239857E-02,-0.52290496E-04,-0.35361294E-03,
         0.81949728E-03, 0.94078219E-03,-0.23983947E-02, 0.46295652E-03,
        -0.72492717E-03,-0.30331640E-03,-0.10807844E-02,-0.69917628E-03,
        -0.33497086E-03, 0.54242328E-03, 0.11529936E-02, 0.45938289E-03,
         0.27317398E-02, 0.20778619E-02,-0.88027370E-03, 0.82281278E-03,
         0.58282202E-03,-0.22916291E-02, 0.15947875E-02,-0.13813938E-02,
         0.32547621E-04, 0.50312869E-03,-0.44962400E-03, 0.10027549E-02,
         0.10641241E-02,-0.32129942E-02, 0.70172729E-03, 0.87214058E-03,
         0.81400573E-03, 0.11579948E-02, 0.39474098E-02,-0.52607676E-04,
         0.61232640E-04,-0.51275219E-04,-0.18066217E-03, 0.45717661E-04,
         0.13387948E-03,-0.59664591E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]*x11        *x41    
        +coeff[  5]        *x32    *x51
        +coeff[  6]            *x42*x51
        +coeff[  7]*x11*x21    *x41    
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[  8]    *x22*x31*x41    
        +coeff[  9]        *x33*x41    
        +coeff[ 10]    *x22    *x42    
        +coeff[ 11]        *x31*x43    
        +coeff[ 12]        *x31*x42*x51
        +coeff[ 13]        *x31*x41*x52
        +coeff[ 14]                *x54
        +coeff[ 15]*x11*x21        *x52
        +coeff[ 16]*x12    *x31*x41    
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 17]*x12        *x42    
        +coeff[ 18]    *x22        *x53
        +coeff[ 19]    *x22*x34        
        +coeff[ 20]    *x22*x33*x41    
        +coeff[ 21]        *x34*x42    
        +coeff[ 22]        *x32*x44    
        +coeff[ 23]        *x32    *x54
        +coeff[ 24]*x12    *x31*x41*x52
        +coeff[ 25]            *x41    
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 26]        *x31*x41    
        +coeff[ 27]                *x52
        +coeff[ 28]*x11*x21            
        +coeff[ 29]*x11    *x31        
        +coeff[ 30]    *x21*x31    *x51
        +coeff[ 31]    *x21        *x52
        +coeff[ 32]*x11    *x32        
        +coeff[ 33]*x12        *x41    
        +coeff[ 34]*x12            *x51
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 35]            *x44    
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]*x11    *x32*x41    
        +coeff[ 39]*x11        *x43    
        +coeff[ 40]*x11            *x53
        +coeff[ 41]*x12*x22            
        +coeff[ 42]*x12*x21        *x51
        +coeff[ 43]*x12            *x52
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 44]    *x21    *x43*x51
        +coeff[ 45]*x11    *x33*x41    
        +coeff[ 46]*x11    *x33    *x51
        +coeff[ 47]*x11*x22    *x41*x51
        +coeff[ 48]*x11    *x32*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        +coeff[ 50]*x11*x21    *x41*x52
        +coeff[ 51]*x12        *x41*x52
        +coeff[ 52]        *x34    *x52
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 53]    *x21*x32*x41*x52
        +coeff[ 54]    *x22    *x42*x52
        +coeff[ 55]*x11*x24*x31        
        +coeff[ 56]*x11    *x31*x44    
        +coeff[ 57]*x11*x24        *x51
        +coeff[ 58]*x11*x22    *x41*x52
        +coeff[ 59]*x11*x21*x31*x41*x52
        +coeff[ 60]*x11    *x31    *x54
        +coeff[ 61]*x12*x22*x32        
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 62]*x12    *x32*x42    
        +coeff[ 63]*x13    *x32    *x51
        +coeff[ 64]    *x21*x32*x43*x51
        +coeff[ 65]    *x21*x31*x44*x51
        +coeff[ 66]    *x22*x33    *x52
        +coeff[ 67]    *x22*x31*x41*x53
        +coeff[ 68]*x11*x24*x31    *x51
        +coeff[ 69]*x11*x23*x32    *x51
        +coeff[ 70]*x11    *x34*x41*x51
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 71]*x11*x21*x32*x41*x52
        +coeff[ 72]*x11*x23        *x53
        +coeff[ 73]*x12    *x31    *x54
        +coeff[ 74]    *x23*x34*x41    
        +coeff[ 75]*x13*x21*x32    *x51
        +coeff[ 76]*x13*x22    *x41*x51
        +coeff[ 77]*x13*x21    *x42*x51
        +coeff[ 78]*x13    *x31*x41*x52
        +coeff[ 79]*x13*x21        *x53
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 80]        *x34*x41*x53
        +coeff[ 81]    *x22*x31*x42*x53
        +coeff[ 82]    *x22    *x42*x54
        +coeff[ 83]                *x51
        +coeff[ 84]*x11                
        +coeff[ 85]    *x21    *x41    
        +coeff[ 86]*x11            *x51
        +coeff[ 87]    *x23            
        +coeff[ 88]    *x22*x31        
    ;
    v_l_e_q1q2_1_250                              =v_l_e_q1q2_1_250                              
        +coeff[ 89]        *x33        
        ;

    return v_l_e_q1q2_1_250                              ;
}
float x_e_q1q2_1_200                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1302068E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.16459754E-03, 0.12355911E+00, 0.18224793E-02, 0.24543304E-02,
        -0.35971258E-03, 0.81149679E-04,-0.54133480E-03,-0.39326903E-03,
        -0.15149354E-03,-0.30764146E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_200                              =v_x_e_q1q2_1_200                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_200                              ;
}
float t_e_q1q2_1_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1408947E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13759995E-04,-0.19628301E-02,-0.13631759E-02, 0.22683418E-02,
        -0.30395624E-03,-0.31552571E-03,-0.30509412E-03, 0.71127441E-04,
        -0.17762318E-03,-0.15968543E-03,-0.77471705E-04,-0.88036963E-03,
        -0.60891057E-03,-0.24511839E-04,-0.31295567E-04,-0.17838633E-04,
         0.57152625E-04,-0.59164397E-03,-0.44295401E-03,-0.12821502E-04,
         0.13446544E-04, 0.26036379E-04, 0.29635145E-04, 0.66564121E-05,
        -0.27099726E-03,-0.10731740E-04, 0.16444777E-04,-0.18530831E-04,
        -0.16338181E-04,-0.43667574E-05, 0.16477790E-05,-0.70899814E-05,
         0.67535234E-05,-0.32434941E-05, 0.89066789E-05, 0.70412830E-05,
        -0.11922073E-04, 0.10027182E-04, 0.67372730E-06,-0.11565615E-04,
        -0.19235264E-04, 0.13279509E-04,-0.20444470E-05, 0.27603853E-05,
         0.10468236E-04,-0.67061833E-05, 0.10931391E-04,-0.43710820E-05,
        -0.14796808E-04,-0.99362023E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_1_200                              =v_t_e_q1q2_1_200                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_1_200                              =v_t_e_q1q2_1_200                              
        +coeff[ 17]    *x21*x32*x42    
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x31    *x52
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]*x11*x21    *x41*x52
    ;
    v_t_e_q1q2_1_200                              =v_t_e_q1q2_1_200                              
        +coeff[ 26]    *x22        *x53
        +coeff[ 27]*x12*x23        *x51
        +coeff[ 28]*x11*x22*x31    *x52
        +coeff[ 29]    *x22            
        +coeff[ 30]                *x52
        +coeff[ 31]    *x22        *x51
        +coeff[ 32]*x12*x22            
        +coeff[ 33]*x12    *x32        
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_q1q2_1_200                              =v_t_e_q1q2_1_200                              
        +coeff[ 35]*x13*x22            
        +coeff[ 36]*x13*x21    *x41    
        +coeff[ 37]*x12*x21*x31*x41    
        +coeff[ 38]*x11*x21*x32*x41    
        +coeff[ 39]*x11*x22    *x42    
        +coeff[ 40]*x11*x21*x31*x42    
        +coeff[ 41]*x12*x21*x31    *x51
        +coeff[ 42]    *x23*x31    *x51
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_t_e_q1q2_1_200                              =v_t_e_q1q2_1_200                              
        +coeff[ 44]    *x21*x32*x41*x51
        +coeff[ 45]*x12*x21        *x52
        +coeff[ 46]*x11*x21*x31    *x52
        +coeff[ 47]    *x22*x31    *x52
        +coeff[ 48]    *x21*x32    *x52
        +coeff[ 49]*x11*x23*x31*x41    
        ;

    return v_t_e_q1q2_1_200                              ;
}
float y_e_q1q2_1_200                              (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.3689580E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.13182382E-03, 0.13113585E+00, 0.97207122E-01,-0.15910511E-02,
        -0.18134766E-04,-0.15678495E-02,-0.58660840E-04,-0.17122371E-04,
        -0.18750527E-03, 0.22309777E-03,-0.51888790E-04, 0.10746525E-03,
         0.92766953E-04, 0.67344205E-04, 0.74026451E-04,-0.26397558E-03,
        -0.67881076E-04, 0.10668628E-05,-0.21477397E-03,-0.84918007E-04,
         0.53868254E-04,-0.39804812E-06, 0.39411818E-04, 0.76602682E-05,
         0.32386350E-04, 0.32147080E-04,-0.13020512E-03, 0.20235550E-04,
        -0.48045047E-04,-0.17013968E-04,-0.31521369E-04,-0.13338063E-04,
        -0.23658933E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_200                              =v_y_e_q1q2_1_200                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x23*x31        
    ;
    v_y_e_q1q2_1_200                              =v_y_e_q1q2_1_200                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]*x12        *x45*x51
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]*x12        *x42    
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]        *x32*x43    
    ;
    v_y_e_q1q2_1_200                              =v_y_e_q1q2_1_200                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]*x11    *x31*x42*x51
        +coeff[ 30]        *x31*x42*x52
        +coeff[ 31]*x11*x21    *x44    
        +coeff[ 32]*x11*x22*x32*x41    
        ;

    return v_y_e_q1q2_1_200                              ;
}
float p_e_q1q2_1_200                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.4499117E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.28976986E-07, 0.35137225E-01, 0.65929867E-01,-0.15154345E-02,
        -0.15380587E-02,-0.32832459E-03,-0.35617055E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_200                              ;
}
float l_e_q1q2_1_200                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1856247E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18417127E-02, 0.39986495E-04,-0.34679314E-02,-0.55699080E-03,
        -0.30773990E-02,-0.63040428E-03,-0.12033201E-02,-0.48036757E-03,
         0.30622687E-03,-0.94073935E-03, 0.12728600E-03,-0.63803798E-03,
        -0.13942229E-03, 0.82104962E-03, 0.37442328E-03,-0.13747116E-02,
         0.88340841E-03,-0.87239000E-03,-0.12170923E-02,-0.19907310E-03,
        -0.87847446E-04,-0.98024320E-04, 0.35861519E-03,-0.10907607E-02,
         0.78433804E-03, 0.36571609E-03,-0.59205812E-03,-0.21530075E-03,
        -0.19635668E-03, 0.31156465E-03, 0.96270826E-03, 0.16597742E-02,
         0.46497243E-03,-0.11924120E-02, 0.38823159E-03,-0.12243484E-02,
         0.39570130E-03,-0.12220656E-02, 0.25758916E-02,-0.22620679E-03,
         0.79981913E-03,-0.53306168E-03,-0.39890179E-03,-0.35219462E-03,
        -0.71108219E-03, 0.87856821E-03, 0.61625493E-03,-0.22863127E-03,
        -0.91753574E-03,-0.89389534E-03, 0.18620767E-02, 0.42978318E-04,
         0.36636242E-03, 0.31523942E-02,-0.61913372E-04, 0.22107302E-02,
        -0.89410081E-03,-0.85294340E-03,-0.88889746E-03,-0.10743281E-02,
        -0.11993283E-02,-0.21533822E-02,-0.10764406E-02, 0.15986887E-02,
         0.14862254E-02,-0.12685886E-02,-0.38750705E-03, 0.17158219E-02,
         0.11400951E-02, 0.17414923E-02,-0.15794090E-03, 0.13058553E-03,
        -0.43650137E-03, 0.40814590E-04, 0.12962255E-03,-0.33189560E-03,
         0.16464961E-03, 0.64441032E-04,-0.12983946E-03, 0.25037865E-03,
        -0.10282717E-03, 0.10580913E-03, 0.66435787E-04, 0.39833100E-03,
         0.33144321E-03,-0.34004595E-03,-0.14399570E-03, 0.24487326E-03,
         0.13255625E-03, 0.13310678E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]*x12        *x41    
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[  8]        *x32*x42    
        +coeff[  9]        *x31*x43    
        +coeff[ 10]            *x44    
        +coeff[ 11]*x11*x21*x31    *x52
        +coeff[ 12]*x12*x22        *x51
        +coeff[ 13]    *x24*x31*x41    
        +coeff[ 14]    *x22*x33*x41    
        +coeff[ 15]        *x34*x42    
        +coeff[ 16]*x11*x22*x32*x41    
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 17]        *x32*x44*x52
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]*x12                
        +coeff[ 21]    *x21*x31*x41    
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]        *x31*x41*x52
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]*x11    *x33        
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 26]*x11    *x31*x42    
        +coeff[ 27]*x11    *x31*x41*x51
        +coeff[ 28]*x11        *x41*x52
        +coeff[ 29]*x13    *x31        
        +coeff[ 30]    *x22*x31    *x52
        +coeff[ 31]    *x21*x32    *x52
        +coeff[ 32]*x11    *x32*x41*x51
        +coeff[ 33]*x11*x21    *x42*x51
        +coeff[ 34]*x11        *x43*x51
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 35]*x11        *x42*x52
        +coeff[ 36]*x12*x22    *x41    
        +coeff[ 37]*x12        *x42*x51
        +coeff[ 38]*x12        *x41*x52
        +coeff[ 39]    *x23*x33        
        +coeff[ 40]*x13        *x41*x51
        +coeff[ 41]    *x21*x33    *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]        *x32    *x54
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 44]*x11*x21*x34        
        +coeff[ 45]*x11    *x31*x43*x51
        +coeff[ 46]*x13*x21*x32        
        +coeff[ 47]    *x24*x33        
        +coeff[ 48]    *x23*x32    *x52
        +coeff[ 49]    *x22*x33    *x52
        +coeff[ 50]    *x23*x31*x41*x52
        +coeff[ 51]    *x21*x32    *x54
        +coeff[ 52]*x11*x24*x31*x41    
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 53]*x11*x22*x32*x42    
        +coeff[ 54]*x11    *x34*x42    
        +coeff[ 55]*x11    *x33*x41*x52
        +coeff[ 56]*x11        *x43*x53
        +coeff[ 57]*x11    *x31*x41*x54
        +coeff[ 58]*x12*x24        *x51
        +coeff[ 59]*x12*x21*x33    *x51
        +coeff[ 60]*x12*x22*x31*x41*x51
        +coeff[ 61]*x12        *x41*x54
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 62]*x13*x22    *x42    
        +coeff[ 63]    *x24*x32*x41*x51
        +coeff[ 64]*x13*x21    *x42*x51
        +coeff[ 65]*x13        *x43*x51
        +coeff[ 66]*x13    *x31*x41*x52
        +coeff[ 67]*x13        *x42*x52
        +coeff[ 68]    *x22*x33    *x53
        +coeff[ 69]        *x31*x43*x54
        +coeff[ 70]    *x21            
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 71]    *x21*x31        
        +coeff[ 72]    *x21        *x51
        +coeff[ 73]*x11            *x51
        +coeff[ 74]    *x23            
        +coeff[ 75]    *x21*x32        
        +coeff[ 76]    *x21    *x42    
        +coeff[ 77]    *x21*x31    *x51
        +coeff[ 78]    *x21    *x41*x51
        +coeff[ 79]            *x42*x51
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 80]            *x41*x52
        +coeff[ 81]*x12*x21            
        +coeff[ 82]*x12    *x31        
        +coeff[ 83]*x12            *x51
        +coeff[ 84]        *x34        
        +coeff[ 85]        *x33    *x51
        +coeff[ 86]    *x21    *x42*x51
        +coeff[ 87]        *x31*x42*x51
        +coeff[ 88]    *x22        *x52
    ;
    v_l_e_q1q2_1_200                              =v_l_e_q1q2_1_200                              
        +coeff[ 89]        *x31    *x53
        ;

    return v_l_e_q1q2_1_200                              ;
}
float x_e_q1q2_1_175                              (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.7019521E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.72617951E-03, 0.12377213E+00, 0.18293373E-02, 0.24468270E-02,
        -0.35222643E-03, 0.81403217E-04,-0.53862447E-03,-0.38795287E-03,
        -0.16223734E-03,-0.30155983E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_175                              =v_x_e_q1q2_1_175                              
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_175                              ;
}
float t_e_q1q2_1_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5234260E-05;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.45490820E-05,-0.19557490E-02,-0.11563743E-02, 0.22637132E-02,
        -0.32005605E-03,-0.32870946E-03,-0.28027868E-03, 0.71297232E-04,
        -0.18408596E-03,-0.17730622E-03,-0.86200373E-04,-0.84723160E-03,
        -0.58215833E-03,-0.25796666E-04,-0.31676293E-04,-0.21251932E-04,
        -0.51774562E-03,-0.39835746E-03,-0.10141461E-04, 0.10724196E-05,
         0.68838158E-05,-0.15148539E-04, 0.53514009E-05, 0.45967077E-04,
         0.17833365E-04, 0.14760121E-04,-0.24490413E-03,-0.23411594E-04,
         0.29534538E-04, 0.26978920E-04, 0.24311439E-04, 0.45653986E-04,
         0.39038649E-04,-0.20615644E-04, 0.25170004E-05,-0.30896069E-06,
         0.28095287E-05, 0.42394854E-05,-0.43092955E-05, 0.22202723E-05,
        -0.34609504E-05, 0.26760806E-05,-0.44490062E-05, 0.42706329E-05,
         0.55030664E-05, 0.55744758E-05, 0.58564469E-05, 0.63005700E-05,
         0.51701063E-05,-0.48878537E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_1_175                              =v_t_e_q1q2_1_175                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q1q2_1_175                              =v_t_e_q1q2_1_175                              
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11    *x32        
        +coeff[ 19]*x11            *x52
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x12*x21        *x51
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]*x13*x22            
    ;
    v_t_e_q1q2_1_175                              =v_t_e_q1q2_1_175                              
        +coeff[ 26]    *x21*x33*x41    
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x12*x23        *x51
        +coeff[ 29]*x11*x23*x31    *x51
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]*x11*x21*x32*x41*x51
        +coeff[ 32]*x12*x21    *x42*x51
        +coeff[ 33]*x12*x22        *x52
        +coeff[ 34]*x11    *x31        
    ;
    v_t_e_q1q2_1_175                              =v_t_e_q1q2_1_175                              
        +coeff[ 35]            *x42    
        +coeff[ 36]    *x22*x31        
        +coeff[ 37]    *x22        *x51
        +coeff[ 38]*x11    *x31    *x51
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]            *x42*x51
        +coeff[ 41]            *x41*x52
        +coeff[ 42]    *x22*x31*x41    
        +coeff[ 43]*x11    *x32*x41    
    ;
    v_t_e_q1q2_1_175                              =v_t_e_q1q2_1_175                              
        +coeff[ 44]    *x21*x32*x41    
        +coeff[ 45]    *x22    *x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]*x11    *x31*x41*x51
        +coeff[ 48]*x12            *x52
        +coeff[ 49]        *x32    *x52
        ;

    return v_t_e_q1q2_1_175                              ;
}
float y_e_q1q2_1_175                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.5020197E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.42214058E-03, 0.13096116E+00, 0.97070269E-01,-0.15880968E-02,
        -0.85524625E-05,-0.15495515E-02,-0.61892730E-04,-0.15125677E-04,
        -0.19957771E-03, 0.21861363E-03,-0.34590405E-04, 0.11723256E-03,
         0.63469830E-04, 0.56107623E-04, 0.70162096E-04,-0.25406349E-03,
        -0.14376677E-04, 0.37703947E-04,-0.15243636E-03,-0.44562079E-04,
         0.56590065E-05,-0.93149692E-05,-0.15230060E-04, 0.19398436E-04,
         0.14695656E-04, 0.74045779E-05,-0.24282704E-03, 0.47058391E-04,
        -0.92961884E-04,-0.35785131E-04,-0.35473262E-04, 0.25762656E-04,
         0.21451107E-04, 0.25791742E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_175                              =v_y_e_q1q2_1_175                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_175                              =v_y_e_q1q2_1_175                              
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]*x11*x21    *x43    
        +coeff[ 20]        *x33        
        +coeff[ 21]    *x21    *x43    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]*x11        *x44    
    ;
    v_y_e_q1q2_1_175                              =v_y_e_q1q2_1_175                              
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]        *x34*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]        *x31*x42*x52
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x22    *x41*x51
        +coeff[ 32]*x11*x21*x31*x41*x51
        +coeff[ 33]*x11*x21    *x43*x51
        ;

    return v_y_e_q1q2_1_175                              ;
}
float p_e_q1q2_1_175                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1855418E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.14561671E-03, 0.35002429E-01, 0.65772220E-01,-0.15087597E-02,
        -0.15280735E-02,-0.32209163E-03,-0.34869433E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_175                              ;
}
float l_e_q1q2_1_175                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1805469E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18267747E-02,-0.34657186E-02,-0.36128727E-03,-0.38185387E-02,
        -0.22207764E-03,-0.28832734E-03, 0.81471156E-03, 0.85798081E-03,
         0.12330810E-02,-0.27611479E-02,-0.12461275E-03, 0.12154268E-03,
        -0.39730081E-03, 0.24907493E-02,-0.11187581E-02, 0.99292793E-03,
         0.38873064E-03, 0.13465079E-03, 0.82086633E-04,-0.28525873E-02,
         0.16838068E-03, 0.13782742E-03, 0.10099175E-03, 0.39070434E-03,
        -0.14297439E-03, 0.26168942E-02, 0.59954170E-03,-0.52532640E-04,
         0.61591883E-03, 0.34252927E-03,-0.21776334E-03,-0.13390440E-03,
         0.21678477E-03, 0.68573303E-04,-0.30817378E-02, 0.68840507E-03,
        -0.62696566E-03, 0.32700849E-03, 0.28523445E-03,-0.66488050E-03,
         0.57909160E-03, 0.57010923E-03,-0.13737299E-03,-0.63272344E-03,
         0.14003777E-02,-0.10110490E-02,-0.45113292E-03, 0.30051739E-03,
        -0.40215562E-03,-0.19960315E-02,-0.22774367E-03,-0.37751489E-03,
         0.89866767E-03, 0.11365752E-02, 0.16687019E-02, 0.61111338E-03,
        -0.11881668E-02, 0.77524304E-03, 0.82072255E-03, 0.26113869E-03,
         0.17754921E-02,-0.86502999E-03,-0.75368310E-03, 0.48239040E-03,
         0.50481223E-03,-0.45967379E-03, 0.11472520E-02, 0.58117526E-03,
         0.19139464E-02,-0.10405525E-02,-0.47317709E-03, 0.27526852E-02,
        -0.88525406E-03, 0.18588043E-02, 0.12863550E-02,-0.23147231E-02,
        -0.11632685E-02, 0.74841367E-03, 0.87862107E-03,-0.65248832E-03,
         0.15849044E-02,-0.96565916E-03, 0.13812827E-02,-0.55605912E-03,
        -0.36485249E-03, 0.11715019E-03, 0.75683900E-03,-0.39289749E-04,
        -0.19728020E-02,-0.23365344E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]            *x42    
        +coeff[  4]        *x32    *x51
        +coeff[  5]        *x34        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]*x11*x21*x31    *x51
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]*x12*x22        *x51
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]    *x22*x31*x43    
        +coeff[ 16]*x12    *x33*x41    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 17]        *x31        
        +coeff[ 18]            *x41    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]    *x21        *x51
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]    *x21    *x42    
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 26]            *x44    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x41*x52
        +coeff[ 30]*x11*x22*x31        
        +coeff[ 31]*x11        *x42*x51
        +coeff[ 32]*x12*x21*x31        
        +coeff[ 33]*x13        *x41    
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 35]*x11*x22    *x41*x51
        +coeff[ 36]*x11*x21    *x42*x51
        +coeff[ 37]*x11*x21        *x53
        +coeff[ 38]*x12    *x32*x41    
        +coeff[ 39]*x12        *x43    
        +coeff[ 40]*x12*x21    *x41*x51
        +coeff[ 41]*x12        *x41*x52
        +coeff[ 42]*x13    *x31*x41    
        +coeff[ 43]    *x23*x32*x41    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 44]    *x23*x32    *x51
        +coeff[ 45]    *x21*x34    *x51
        +coeff[ 46]*x13        *x41*x51
        +coeff[ 47]        *x33*x42*x51
        +coeff[ 48]    *x23*x31    *x52
        +coeff[ 49]        *x32*x42*x52
        +coeff[ 50]            *x44*x52
        +coeff[ 51]    *x21*x32    *x53
        +coeff[ 52]            *x42*x54
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 53]*x11*x23*x31    *x51
        +coeff[ 54]*x11*x21*x33    *x51
        +coeff[ 55]*x11*x22*x31*x41*x51
        +coeff[ 56]*x11*x23        *x52
        +coeff[ 57]*x11*x21    *x42*x52
        +coeff[ 58]*x11        *x42*x53
        +coeff[ 59]*x11        *x41*x54
        +coeff[ 60]*x12*x21*x32    *x51
        +coeff[ 61]*x12    *x31*x41*x52
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 62]*x12*x21        *x53
        +coeff[ 63]*x13*x23            
        +coeff[ 64]    *x23*x34        
        +coeff[ 65]*x13        *x43    
        +coeff[ 66]*x13*x21*x31    *x51
        +coeff[ 67]    *x23*x31*x42*x51
        +coeff[ 68]    *x21*x34    *x52
        +coeff[ 69]    *x21*x31*x43*x52
        +coeff[ 70]    *x22    *x41*x54
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 71]*x11*x21*x34*x41    
        +coeff[ 72]*x11*x24    *x42    
        +coeff[ 73]*x11*x24*x31    *x51
        +coeff[ 74]*x11*x22*x32*x41*x51
        +coeff[ 75]*x11*x22*x31*x42*x51
        +coeff[ 76]*x11*x24        *x52
        +coeff[ 77]*x11    *x33*x41*x52
        +coeff[ 78]*x11*x22        *x54
        +coeff[ 79]*x12    *x31*x44    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 80]*x12*x22    *x42*x51
        +coeff[ 81]*x12*x23        *x52
        +coeff[ 82]*x12*x21*x32    *x52
        +coeff[ 83]*x12        *x42*x53
        +coeff[ 84]*x13*x23*x31        
        +coeff[ 85]*x13*x22*x32        
        +coeff[ 86]*x13*x21*x33        
        +coeff[ 87]*x13*x23    *x41    
        +coeff[ 88]*x13*x22*x31*x41    
    ;
    v_l_e_q1q2_1_175                              =v_l_e_q1q2_1_175                              
        +coeff[ 89]*x13*x21*x32*x41    
        ;

    return v_l_e_q1q2_1_175                              ;
}
float x_e_q1q2_1_150                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3133865E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.30255961E-03, 0.12405009E+00, 0.18385560E-02, 0.24393019E-02,
        -0.28335521E-03, 0.82313527E-04,-0.60290348E-03,-0.41447434E-03,
        -0.21998357E-03,-0.26482414E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_150                              =v_x_e_q1q2_1_150                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_1_150                              ;
}
float t_e_q1q2_1_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1110387E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.11548730E-04,-0.19480676E-02,-0.88883715E-03, 0.22445093E-02,
        -0.30277757E-03,-0.31473581E-03,-0.27676747E-03, 0.70433525E-04,
        -0.18131419E-03,-0.17191541E-03,-0.79317957E-04,-0.84234314E-03,
        -0.59664971E-03,-0.17240041E-04,-0.18436280E-04,-0.63071102E-05,
         0.68018358E-04, 0.47210546E-04,-0.55756071E-03,-0.42982498E-03,
        -0.91283327E-05, 0.64703568E-05,-0.12250645E-04, 0.21087750E-04,
        -0.65351240E-04,-0.26221704E-03,-0.36499758E-04,-0.25556421E-04,
        -0.22387145E-04, 0.34511520E-05, 0.13859834E-05,-0.34813181E-06,
        -0.35061871E-05,-0.16601148E-05, 0.57892812E-05,-0.52936061E-05,
        -0.63785023E-07, 0.58647333E-05, 0.79111496E-05,-0.52280911E-05,
         0.61595465E-05, 0.77453733E-05,-0.45930910E-05,-0.53897306E-05,
         0.66323537E-05, 0.37780069E-05,-0.15278771E-04, 0.92880344E-06,
        -0.12700597E-04, 0.43759524E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_1_150                              =v_t_e_q1q2_1_150                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_1_150                              =v_t_e_q1q2_1_150                              
        +coeff[ 17]    *x21    *x42*x51
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]*x11*x21    *x42    
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_1_150                              =v_t_e_q1q2_1_150                              
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]*x12*x22*x31    *x51
        +coeff[ 28]*x12*x22    *x41*x51
        +coeff[ 29]*x11*x21            
        +coeff[ 30]        *x32        
        +coeff[ 31]    *x21    *x41    
        +coeff[ 32]            *x41*x51
        +coeff[ 33]*x12        *x41    
        +coeff[ 34]*x13*x21            
    ;
    v_t_e_q1q2_1_150                              =v_t_e_q1q2_1_150                              
        +coeff[ 35]*x11*x23            
        +coeff[ 36]*x12*x21*x31        
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x23    *x41    
        +coeff[ 39]    *x22    *x42    
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x12        *x41*x51
        +coeff[ 42]*x11*x21        *x52
        +coeff[ 43]        *x31*x41*x52
    ;
    v_t_e_q1q2_1_150                              =v_t_e_q1q2_1_150                              
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x13*x21*x31        
        +coeff[ 46]*x11*x22*x32        
        +coeff[ 47]*x13    *x31*x41    
        +coeff[ 48]*x13        *x42    
        +coeff[ 49]*x11*x23        *x51
        ;

    return v_t_e_q1q2_1_150                              ;
}
float y_e_q1q2_1_150                              (float *x,int m){
    int ncoeff= 34;
    float avdat=  0.2916441E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
        -0.91188267E-04, 0.13078400E+00, 0.96895158E-01,-0.15793348E-02,
        -0.70442493E-05,-0.15557022E-02,-0.57035006E-04,-0.57175643E-04,
        -0.20658031E-03, 0.22010626E-03,-0.96414697E-05, 0.11884031E-03,
         0.10426775E-03, 0.59764639E-04, 0.66133871E-04,-0.25593507E-03,
        -0.13248472E-04,-0.26796066E-04,-0.25707405E-03,-0.88022411E-04,
        -0.54013531E-05,-0.15656715E-04, 0.13239485E-04, 0.81279659E-05,
         0.20604153E-04, 0.27136923E-04, 0.20255882E-04,-0.11913921E-04,
         0.35656569E-04,-0.20516453E-03,-0.51672378E-04,-0.42132368E-04,
         0.25359926E-04, 0.38427806E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_150                              =v_y_e_q1q2_1_150                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_150                              =v_y_e_q1q2_1_150                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]        *x32*x42    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q1q2_1_150                              =v_y_e_q1q2_1_150                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]*x11        *x42*x51
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x13*x21    *x41    
        +coeff[ 33]    *x22*x32*x41*x51
        ;

    return v_y_e_q1q2_1_150                              ;
}
float p_e_q1q2_1_150                              (float *x,int m){
    int ncoeff=  7;
    float avdat=  0.2589941E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
        -0.56224791E-04, 0.34826268E-01, 0.65596089E-01,-0.15027308E-02,
        -0.15241865E-02,-0.32317973E-03,-0.34612024E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_150                              ;
}
float l_e_q1q2_1_150                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1780216E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.18940110E-02, 0.10219466E-03,-0.35852750E-02,-0.11066626E-02,
        -0.34607037E-02,-0.21387733E-03,-0.14794079E-03,-0.78841054E-04,
         0.13860069E-02,-0.58304606E-03, 0.17422950E-03, 0.12163790E-02,
         0.33514386E-02, 0.16994869E-02,-0.11602172E-03, 0.11190592E-02,
         0.19551029E-02,-0.21315808E-02,-0.28445197E-02, 0.11260132E-03,
         0.10540086E-03,-0.16091665E-03, 0.34679167E-03,-0.29009656E-03,
         0.45840228E-04,-0.57739625E-03, 0.30894711E-03, 0.55199524E-03,
         0.51951950E-03, 0.38993679E-03,-0.29166124E-03, 0.28579347E-03,
         0.14703111E-03, 0.87048014E-03,-0.11914165E-02,-0.72554249E-03,
        -0.66458085E-03, 0.44482781E-03,-0.24314476E-02,-0.72808709E-03,
        -0.97994239E-03,-0.12072160E-03, 0.49150235E-03,-0.43206720E-03,
        -0.46480465E-03,-0.36375044E-03,-0.46074897E-03,-0.80912444E-03,
         0.19346002E-02, 0.15439276E-02,-0.32421955E-03, 0.31024931E-03,
        -0.47542565E-03, 0.14242256E-02, 0.45674155E-03, 0.18014248E-02,
         0.79766620E-03,-0.11077637E-02,-0.66373340E-03,-0.35825346E-03,
        -0.94740244E-03,-0.19032025E-02, 0.86952263E-03, 0.17188536E-02,
         0.73532388E-03, 0.23788570E-02, 0.10311134E-02,-0.15742241E-02,
         0.12417743E-02,-0.17875405E-02,-0.64013759E-03,-0.25502188E-03,
         0.15864671E-02,-0.14002945E-02, 0.82504976E-03,-0.66800386E-03,
        -0.34968839E-02,-0.20679559E-02,-0.59994590E-03,-0.24062479E-02,
        -0.22110746E-02, 0.27167068E-02, 0.12092239E-02, 0.40377250E-04,
         0.48743441E-04, 0.38618920E-04, 0.54716907E-04,-0.11108714E-03,
        -0.56660101E-04, 0.63332023E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22        *x51
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]    *x24*x31*x41    
        +coeff[ 12]    *x22*x33*x41    
        +coeff[ 13]    *x22*x32*x42    
        +coeff[ 14]        *x34*x42    
        +coeff[ 15]        *x33*x41*x52
        +coeff[ 16]*x11    *x33*x41*x52
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 17]    *x24*x33*x41    
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]            *x41*x51
        +coeff[ 20]                *x52
        +coeff[ 21]    *x22    *x41    
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 26]*x11        *x41*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]        *x34        
        +coeff[ 29]            *x44    
        +coeff[ 30]        *x32*x41*x51
        +coeff[ 31]*x13    *x31        
        +coeff[ 32]    *x24*x31        
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]        *x32*x43    
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x32    *x51
        +coeff[ 37]    *x21*x33    *x51
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]    *x21    *x42*x52
        +coeff[ 40]        *x31*x42*x52
        +coeff[ 41]            *x43*x52
        +coeff[ 42]        *x32    *x53
        +coeff[ 43]    *x21    *x41*x53
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 44]    *x21        *x54
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x11*x22    *x41*x51
        +coeff[ 47]*x11*x21*x31*x41*x51
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x12*x21    *x42    
        +coeff[ 50]*x12*x21*x31    *x51
        +coeff[ 51]*x12        *x42*x51
        +coeff[ 52]*x12        *x41*x52
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 53]        *x32*x44    
        +coeff[ 54]*x13*x21        *x51
        +coeff[ 55]    *x24    *x41*x51
        +coeff[ 56]*x11*x21*x32    *x52
        +coeff[ 57]*x12*x22*x31*x41    
        +coeff[ 58]*x12*x21    *x41*x52
        +coeff[ 59]*x13*x21*x32        
        +coeff[ 60]    *x23*x31*x43    
        +coeff[ 61]    *x23    *x44    
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 62]    *x22*x33    *x52
        +coeff[ 63]    *x21    *x44*x52
        +coeff[ 64]    *x23        *x54
        +coeff[ 65]    *x21*x31*x41*x54
        +coeff[ 66]*x11*x22*x34        
        +coeff[ 67]*x11*x22*x33    *x51
        +coeff[ 68]*x11*x22*x31    *x53
        +coeff[ 69]*x11    *x31*x41*x54
        +coeff[ 70]*x12*x22*x33        
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 71]*x12    *x34    *x51
        +coeff[ 72]*x12*x22    *x42*x51
        +coeff[ 73]*x12    *x32*x42*x51
        +coeff[ 74]*x12*x21    *x43*x51
        +coeff[ 75]*x12*x23        *x52
        +coeff[ 76]*x12*x21*x31*x41*x52
        +coeff[ 77]*x12*x21    *x42*x52
        +coeff[ 78]*x13*x22*x32        
        +coeff[ 79]    *x24*x32*x41*x51
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 80]    *x24    *x43*x51
        +coeff[ 81]    *x22*x32*x43*x51
        +coeff[ 82]*x13    *x31*x41*x52
        +coeff[ 83]        *x31        
        +coeff[ 84]            *x41    
        +coeff[ 85]*x11                
        +coeff[ 86]    *x21        *x51
        +coeff[ 87]*x11    *x31        
        +coeff[ 88]*x12                
    ;
    v_l_e_q1q2_1_150                              =v_l_e_q1q2_1_150                              
        +coeff[ 89]    *x23            
        ;

    return v_l_e_q1q2_1_150                              ;
}
float x_e_q1q2_1_125                              (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.3131563E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.32426935E-03, 0.12445466E+00, 0.18526308E-02, 0.24235994E-02,
        -0.28126154E-03, 0.81537313E-04,-0.59331924E-03,-0.41156050E-03,
        -0.22189508E-03,-0.26017736E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_125                              =v_x_e_q1q2_1_125                              
        +coeff[  8]    *x21*x32    *x52
        +coeff[  9]    *x23*x32        
        ;

    return v_x_e_q1q2_1_125                              ;
}
float t_e_q1q2_1_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1160351E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.11963151E-04,-0.19347685E-02,-0.51153667E-03, 0.22438555E-02,
        -0.29566852E-03,-0.31488918E-03,-0.27409851E-03, 0.68545211E-04,
        -0.17880576E-03,-0.17133377E-03,-0.79862941E-04,-0.82611863E-03,
        -0.58299478E-03,-0.23084160E-04,-0.28545795E-04,-0.20135778E-04,
         0.34001234E-04,-0.54016268E-05,-0.58675080E-03,-0.44445507E-03,
         0.43007309E-04, 0.34190414E-05,-0.11614782E-04,-0.45370625E-05,
         0.18605329E-04,-0.28071299E-03, 0.16896069E-04,-0.71618433E-05,
        -0.22709237E-04, 0.12311464E-04,-0.14425312E-04,-0.24108693E-04,
         0.36006688E-04, 0.38227674E-04, 0.74277898E-06,-0.19632100E-05,
         0.15759852E-05,-0.38596881E-05,-0.24033689E-05, 0.28430879E-05,
         0.22690060E-05, 0.66470020E-05,-0.19851900E-05, 0.58623300E-05,
        -0.30358028E-05, 0.53699446E-05,-0.27285855E-05,-0.84988587E-05,
         0.53163699E-05, 0.10142544E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1q2_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_t_e_q1q2_1_125                              =v_t_e_q1q2_1_125                              
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1q2_1_125                              =v_t_e_q1q2_1_125                              
        +coeff[ 17]*x13    *x32        
        +coeff[ 18]    *x21*x32*x42    
        +coeff[ 19]    *x21*x31*x43    
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]*x13                
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x33*x41    
    ;
    v_t_e_q1q2_1_125                              =v_t_e_q1q2_1_125                              
        +coeff[ 26]*x12*x21    *x42    
        +coeff[ 27]*x11*x23        *x51
        +coeff[ 28]*x12*x21*x31    *x51
        +coeff[ 29]*x11*x22    *x41*x51
        +coeff[ 30]*x13*x21*x31    *x51
        +coeff[ 31]    *x22*x33    *x51
        +coeff[ 32]*x12*x21    *x42*x51
        +coeff[ 33]    *x21*x31*x43*x51
        +coeff[ 34]                *x51
    ;
    v_t_e_q1q2_1_125                              =v_t_e_q1q2_1_125                              
        +coeff[ 35]    *x22            
        +coeff[ 36]        *x32        
        +coeff[ 37]    *x22*x31        
        +coeff[ 38]*x11*x21    *x41    
        +coeff[ 39]        *x31*x42    
        +coeff[ 40]*x11    *x31    *x51
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]    *x21*x33        
        +coeff[ 43]*x12*x21    *x41    
    ;
    v_t_e_q1q2_1_125                              =v_t_e_q1q2_1_125                              
        +coeff[ 44]    *x23    *x41    
        +coeff[ 45]*x11*x21*x31*x41    
        +coeff[ 46]*x12        *x42    
        +coeff[ 47]*x12*x21        *x51
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]    *x22*x31    *x51
        ;

    return v_t_e_q1q2_1_125                              ;
}
float y_e_q1q2_1_125                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2741395E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.28898756E-03, 0.13057238E+00, 0.96596166E-01,-0.15657570E-02,
        -0.74735785E-05,-0.15336087E-02,-0.10462832E-03, 0.33408982E-04,
        -0.14710553E-03, 0.19698215E-03,-0.93846771E-04, 0.11009896E-03,
         0.82909297E-04,-0.20254201E-04, 0.66665561E-04, 0.67968576E-04,
        -0.21744233E-03,-0.23110062E-04,-0.96177309E-05,-0.16827995E-04,
         0.35003508E-04,-0.80555830E-04,-0.15544036E-04,-0.52530060E-04,
        -0.32221313E-04,-0.16003720E-04, 0.43693148E-04,-0.18771281E-04,
         0.16349197E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_125                              =v_y_e_q1q2_1_125                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_q1q2_1_125                              =v_y_e_q1q2_1_125                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]*x12        *x42*x51
    ;
    v_y_e_q1q2_1_125                              =v_y_e_q1q2_1_125                              
        +coeff[ 26]    *x22    *x43*x51
        +coeff[ 27]    *x23    *x42*x51
        +coeff[ 28]    *x21    *x43*x52
        ;

    return v_y_e_q1q2_1_125                              ;
}
float p_e_q1q2_1_125                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.1488828E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.15426664E-03, 0.34558862E-01, 0.65359712E-01,-0.14910087E-02,
        -0.15115873E-02,-0.31441243E-03,-0.33972095E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_125                              ;
}
float l_e_q1q2_1_125                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1812695E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17301560E-02,-0.79872902E-04,-0.35395951E-02,-0.22021426E-03,
        -0.19636345E-02,-0.28854893E-02, 0.72674820E-03,-0.62815606E-03,
         0.85662236E-03, 0.19647864E-03,-0.17717280E-03, 0.21253472E-03,
        -0.30077147E-03,-0.25108122E-03, 0.30081236E-03, 0.23754220E-03,
         0.43843809E-03,-0.96513471E-03,-0.44741528E-03,-0.47910822E-03,
        -0.19056098E-03,-0.14701347E-03, 0.36891908E-03, 0.18829449E-03,
        -0.46559688E-03,-0.33261225E-03,-0.32835372E-03,-0.40065736E-03,
        -0.55104632E-04, 0.24360100E-03,-0.68168709E-03, 0.30737207E-02,
         0.12778663E-02, 0.11870993E-02,-0.74319419E-03, 0.26192988E-03,
         0.29185647E-03, 0.40536327E-03,-0.18328935E-03,-0.69213973E-03,
        -0.36331936E-03,-0.36468726E-03,-0.73186267E-03,-0.43091446E-03,
        -0.80443220E-03,-0.27606927E-03, 0.34797596E-03, 0.77187084E-03,
         0.36737017E-03, 0.18053864E-02, 0.16233230E-02, 0.48108769E-03,
        -0.72928914E-03,-0.79408899E-03,-0.13903814E-02,-0.14369811E-02,
         0.11965890E-02, 0.32687373E-03, 0.60879270E-03,-0.79615635E-03,
        -0.17849890E-02,-0.13209677E-02, 0.10313180E-02,-0.85442670E-03,
        -0.18998660E-02, 0.13110512E-02,-0.24251693E-02, 0.14724564E-02,
        -0.43436608E-03,-0.22303707E-02,-0.34731568E-02,-0.87558356E-03,
         0.49499731E-03, 0.49410155E-03,-0.56588586E-03, 0.70551340E-03,
        -0.46163038E-03, 0.15574009E-02, 0.57830923E-03,-0.57675864E-03,
         0.79462701E-03,-0.54670207E-03,-0.44603072E-03, 0.12538749E-03,
        -0.99174875E-04, 0.72092160E-04, 0.77891280E-04,-0.80666650E-04,
         0.16776707E-03, 0.87412649E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x12            *x53
        +coeff[  7]*x11*x21*x34        
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[  8]        *x31*x43*x53
        +coeff[  9]            *x41    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]*x12                
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]*x11    *x32        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 17]*x11*x21        *x51
        +coeff[ 18]        *x32    *x52
        +coeff[ 19]        *x31*x41*x52
        +coeff[ 20]*x11    *x33        
        +coeff[ 21]*x11*x22    *x41    
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]*x11    *x31    *x52
        +coeff[ 24]*x12    *x32        
        +coeff[ 25]*x12    *x31*x41    
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 26]*x12    *x31    *x51
        +coeff[ 27]*x13        *x41    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x13            *x51
        +coeff[ 30]    *x21*x33    *x51
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11    *x32*x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x11*x22*x31    *x51
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 35]*x11    *x33    *x51
        +coeff[ 36]*x11    *x31*x42*x51
        +coeff[ 37]*x11        *x43*x51
        +coeff[ 38]*x12    *x33        
        +coeff[ 39]*x12*x22        *x51
        +coeff[ 40]*x12    *x31*x41*x51
        +coeff[ 41]*x12        *x41*x52
        +coeff[ 42]    *x23*x33        
        +coeff[ 43]    *x21*x34*x41    
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 44]        *x34*x41*x51
        +coeff[ 45]    *x24        *x52
        +coeff[ 46]    *x23        *x53
        +coeff[ 47]    *x22        *x54
        +coeff[ 48]*x11    *x34*x41    
        +coeff[ 49]*x11*x23*x31    *x51
        +coeff[ 50]*x11*x23    *x41*x51
        +coeff[ 51]*x11*x22*x31*x41*x51
        +coeff[ 52]*x11*x21*x31*x42*x51
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 53]*x11*x22        *x53
        +coeff[ 54]*x11*x21    *x41*x53
        +coeff[ 55]*x12*x21*x31*x41*x51
        +coeff[ 56]*x12    *x32*x41*x51
        +coeff[ 57]*x13*x21    *x42    
        +coeff[ 58]*x13*x22        *x51
        +coeff[ 59]*x13*x21*x31    *x51
        +coeff[ 60]    *x24*x31*x41*x51
        +coeff[ 61]    *x22*x32*x42*x51
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 62]    *x23    *x43*x51
        +coeff[ 63]    *x21*x32*x43*x51
        +coeff[ 64]    *x21    *x44*x52
        +coeff[ 65]    *x21    *x42*x54
        +coeff[ 66]*x11*x24*x31*x41    
        +coeff[ 67]*x11    *x33*x42*x51
        +coeff[ 68]*x11    *x34    *x52
        +coeff[ 69]*x11    *x33*x41*x52
        +coeff[ 70]*x11    *x32*x42*x52
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 71]*x11    *x31*x41*x54
        +coeff[ 72]*x12    *x34*x41    
        +coeff[ 73]*x12*x22*x31    *x52
        +coeff[ 74]*x12    *x32    *x53
        +coeff[ 75]*x13*x22    *x42    
        +coeff[ 76]    *x23*x34    *x51
        +coeff[ 77]    *x23*x33*x41*x51
        +coeff[ 78]*x13*x21        *x53
        +coeff[ 79]*x13    *x31    *x53
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 80]    *x21*x34    *x53
        +coeff[ 81]    *x24    *x41*x53
        +coeff[ 82]            *x44*x54
        +coeff[ 83]    *x21*x31        
        +coeff[ 84]    *x21        *x51
        +coeff[ 85]        *x31    *x51
        +coeff[ 86]            *x41*x51
        +coeff[ 87]                *x52
        +coeff[ 88]*x11        *x41    
    ;
    v_l_e_q1q2_1_125                              =v_l_e_q1q2_1_125                              
        +coeff[ 89]    *x22*x31        
        ;

    return v_l_e_q1q2_1_125                              ;
}
float x_e_q1q2_1_100                              (float *x,int m){
    int ncoeff=  9;
    float avdat= -0.9197599E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 10]={
         0.95330761E-03, 0.12505677E+00, 0.18724148E-02, 0.23990702E-02,
        -0.34600287E-03, 0.79626130E-04,-0.47461662E-03,-0.35178609E-03,
        -0.33318804E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q1q2_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]*x11            *x51
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q1q2_1_100                              =v_x_e_q1q2_1_100                              
        +coeff[  8]    *x21*x32    *x52
        ;

    return v_x_e_q1q2_1_100                              ;
}
float t_e_q1q2_1_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3199116E-06;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12354313E-06,-0.19202292E-02, 0.54738135E-04, 0.22191291E-02,
        -0.17428401E-03,-0.29492367E-03,-0.30562075E-03,-0.28449745E-03,
         0.71309987E-04,-0.16077222E-03,-0.81958191E-04,-0.82348520E-03,
        -0.58005337E-03,-0.72553480E-05,-0.51755662E-03,-0.39542172E-03,
        -0.64710885E-05,-0.24561416E-04,-0.18964145E-04, 0.65375389E-04,
         0.43572418E-04,-0.88822162E-05,-0.13363619E-05,-0.14286036E-04,
        -0.25072388E-03,-0.15688956E-04, 0.34641143E-04, 0.37579892E-04,
        -0.14614226E-04,-0.18268451E-04, 0.10269552E-04, 0.14859800E-05,
         0.48750221E-05,-0.10015822E-04, 0.32505350E-05,-0.26529217E-05,
         0.22865984E-05,-0.22283884E-05,-0.13760684E-04,-0.45773209E-05,
        -0.62293971E-05,-0.60998973E-05,-0.34498771E-06,-0.60710217E-05,
         0.48417901E-05,-0.14619373E-04, 0.37320465E-05, 0.57901470E-05,
        -0.68024719E-05,-0.12192080E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1q2_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q1q2_1_100                              =v_t_e_q1q2_1_100                              
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32*x42    
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]    *x22*x31        
    ;
    v_t_e_q1q2_1_100                              =v_t_e_q1q2_1_100                              
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]*x13    *x32        
        +coeff[ 23]*x11*x22*x32        
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x22*x31*x41*x51
    ;
    v_t_e_q1q2_1_100                              =v_t_e_q1q2_1_100                              
        +coeff[ 26]*x12*x21*x32    *x51
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]    *x22*x33    *x51
        +coeff[ 29]*x11*x22    *x41*x52
        +coeff[ 30]*x11*x21            
        +coeff[ 31]                *x52
        +coeff[ 32]*x13                
        +coeff[ 33]*x11    *x32        
        +coeff[ 34]*x12        *x41    
    ;
    v_t_e_q1q2_1_100                              =v_t_e_q1q2_1_100                              
        +coeff[ 35]        *x32*x41    
        +coeff[ 36]*x11*x21        *x51
        +coeff[ 37]    *x22        *x51
        +coeff[ 38]*x11*x23            
        +coeff[ 39]*x11*x22*x31        
        +coeff[ 40]*x13            *x51
        +coeff[ 41]*x12*x21        *x51
        +coeff[ 42]*x12        *x41*x51
        +coeff[ 43]    *x22    *x41*x51
    ;
    v_t_e_q1q2_1_100                              =v_t_e_q1q2_1_100                              
        +coeff[ 44]*x11            *x53
        +coeff[ 45]*x13*x22            
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]*x12*x22*x31        
        +coeff[ 48]*x12*x21*x32        
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_q1q2_1_100                              ;
}
float y_e_q1q2_1_100                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1081644E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.99602155E-03, 0.13015752E+00, 0.96223988E-01,-0.15483657E-02,
        -0.22084287E-04,-0.15121981E-02,-0.67501867E-04,-0.30908326E-04,
        -0.16341447E-03, 0.21554655E-03,-0.50994211E-04, 0.14191041E-03,
         0.94778879E-04, 0.78537028E-04, 0.62629479E-04,-0.23358403E-03,
        -0.12204066E-04,-0.30695050E-04, 0.79413776E-05,-0.25243162E-04,
         0.28620942E-04, 0.73209230E-05,-0.14861095E-03,-0.14717815E-04,
        -0.19775132E-03,-0.52448508E-04, 0.13992754E-04,-0.76510441E-04,
        -0.41704396E-04,-0.32961536E-04, 0.45101799E-04, 0.25006277E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1q2_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x43*x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x43    
    ;
    v_y_e_q1q2_1_100                              =v_y_e_q1q2_1_100                              
        +coeff[  8]    *x24    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]            *x43    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x31        
    ;
    v_y_e_q1q2_1_100                              =v_y_e_q1q2_1_100                              
        +coeff[ 17]*x11*x21    *x43    
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]            *x44*x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x21    *x43*x51
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]*x11*x21*x31*x42    
    ;
    v_y_e_q1q2_1_100                              =v_y_e_q1q2_1_100                              
        +coeff[ 26]*x11    *x32*x42    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]            *x43*x52
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x22    *x43*x51
        +coeff[ 31]*x11    *x31*x43*x51
        ;

    return v_y_e_q1q2_1_100                              ;
}
float p_e_q1q2_1_100                              (float *x,int m){
    int ncoeff=  7;
    float avdat= -0.4878296E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  8]={
         0.44642689E-03, 0.34187105E-01, 0.64968944E-01,-0.14738052E-02,
        -0.14936922E-02,-0.30514400E-03,-0.33065988E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;
    float x51 = x5;

//                 function

    float v_p_e_q1q2_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x41    
        ;

    return v_p_e_q1q2_1_100                              ;
}
float l_e_q1q2_1_100                              (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1819320E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17985685E-02,-0.34606536E-02,-0.18434701E-03,-0.46161740E-03,
        -0.24850992E-02,-0.14801288E-02,-0.14031220E-02,-0.33835726E-03,
        -0.10672634E-02, 0.11287171E-02,-0.39228986E-03, 0.42210259E-02,
        -0.17837338E-02,-0.75393135E-03,-0.12863460E-02, 0.53775972E-02,
        -0.20018767E-02,-0.16784737E-02, 0.25145878E-03,-0.15054013E-03,
         0.55997900E-03, 0.76972916E-04,-0.10494035E-02,-0.74175023E-05,
         0.41399969E-03,-0.19906054E-02,-0.15580206E-04,-0.45849205E-03,
         0.20657005E-03,-0.78917982E-03,-0.32315639E-03, 0.22555812E-03,
        -0.16617728E-02, 0.25439160E-02,-0.46797429E-03, 0.26987379E-02,
         0.14619293E-02, 0.20906867E-02,-0.54266350E-03, 0.42350029E-03,
        -0.14947545E-03, 0.17608373E-02, 0.24540084E-02,-0.72790065E-03,
        -0.10607836E-02, 0.99337648E-03, 0.64130762E-03,-0.60485484E-03,
         0.49005647E-03, 0.92861848E-03, 0.65867789E-03,-0.14938110E-02,
        -0.56393369E-03, 0.35606648E-03, 0.72518113E-03,-0.90693362E-03,
        -0.19936322E-03,-0.14261564E-02,-0.44395341E-03, 0.21817004E-02,
         0.71492622E-03, 0.26980566E-02,-0.20001228E-02,-0.35087619E-03,
        -0.22790725E-02,-0.29927860E-02,-0.53138362E-03,-0.49613864E-03,
        -0.26919360E-02,-0.19417853E-02, 0.65060233E-03,-0.10502647E-02,
         0.70795370E-03,-0.12926757E-02, 0.83718752E-03, 0.10929272E-02,
         0.55744513E-02,-0.48362158E-03, 0.13465928E-02,-0.21716135E-02,
         0.38356353E-02,-0.15462663E-02, 0.16162747E-02,-0.14936917E-03,
        -0.75818534E-04,-0.17282394E-03,-0.19612968E-03, 0.16258996E-03,
        -0.76295597E-04,-0.50420429E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1q2_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]        *x32        
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]        *x33*x41    
        +coeff[  7]        *x31*x43    
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[  8]            *x44    
        +coeff[  9]        *x31*x41*x52
        +coeff[ 10]*x12    *x31*x41    
        +coeff[ 11]    *x22*x33*x41    
        +coeff[ 12]    *x22*x32*x42    
        +coeff[ 13]        *x34*x42    
        +coeff[ 14]*x12    *x32*x42    
        +coeff[ 15]    *x22*x34*x42    
        +coeff[ 16]        *x31*x43*x54
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 17]        *x31*x41    
        +coeff[ 18]*x11*x21            
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]            *x41*x52
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]*x12            *x51
        +coeff[ 24]*x13                
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11*x21*x32        
        +coeff[ 28]*x11    *x33        
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]    *x22        *x53
        +coeff[ 31]*x11*x24            
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]*x11*x21*x32*x41    
        +coeff[ 34]*x11*x21*x31*x42    
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 35]*x11    *x31*x42*x51
        +coeff[ 36]*x11*x22        *x52
        +coeff[ 37]*x11*x21*x31    *x52
        +coeff[ 38]*x11        *x42*x52
        +coeff[ 39]*x12        *x42*x51
        +coeff[ 40]    *x23*x32*x41    
        +coeff[ 41]    *x23*x31*x41*x51
        +coeff[ 42]    *x21*x33*x41*x51
        +coeff[ 43]    *x22    *x43*x51
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 44]    *x21*x31*x42*x52
        +coeff[ 45]            *x44*x52
        +coeff[ 46]        *x33    *x53
        +coeff[ 47]            *x42*x54
        +coeff[ 48]*x11    *x33    *x52
        +coeff[ 49]*x12*x21    *x41*x52
        +coeff[ 50]*x12        *x42*x52
        +coeff[ 51]    *x21*x34*x42    
        +coeff[ 52]    *x23*x31*x43    
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 53]*x13*x21*x31    *x51
        +coeff[ 54]    *x24*x32    *x51
        +coeff[ 55]    *x23*x33    *x51
        +coeff[ 56]    *x23*x31*x42*x51
        +coeff[ 57]    *x23*x31*x41*x52
        +coeff[ 58]        *x34    *x53
        +coeff[ 59]    *x21*x31*x42*x53
        +coeff[ 60]    *x21    *x43*x53
        +coeff[ 61]*x11*x23*x33        
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 62]*x11*x21*x34*x41    
        +coeff[ 63]*x11*x22    *x44    
        +coeff[ 64]*x11    *x31*x44*x51
        +coeff[ 65]*x11*x21*x33    *x52
        +coeff[ 66]*x11    *x33*x41*x52
        +coeff[ 67]*x11*x22*x31    *x53
        +coeff[ 68]*x11    *x31*x42*x53
        +coeff[ 69]*x11*x22        *x54
        +coeff[ 70]*x12*x21*x34        
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 71]*x12*x21    *x42*x52
        +coeff[ 72]*x12    *x32    *x53
        +coeff[ 73]*x13*x24            
        +coeff[ 74]*x13*x23    *x41    
        +coeff[ 75]*x13*x22    *x42    
        +coeff[ 76]    *x23*x32*x42*x51
        +coeff[ 77]*x13        *x43*x51
        +coeff[ 78]        *x34*x43*x51
        +coeff[ 79]    *x23*x32*x41*x52
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 80]    *x21*x31*x43*x53
        +coeff[ 81]        *x32*x43*x53
        +coeff[ 82]    *x21    *x44*x53
        +coeff[ 83]*x11                
        +coeff[ 84]    *x21    *x41    
        +coeff[ 85]    *x21        *x51
        +coeff[ 86]        *x31    *x51
        +coeff[ 87]            *x41*x51
        +coeff[ 88]                *x52
    ;
    v_l_e_q1q2_1_100                              =v_l_e_q1q2_1_100                              
        +coeff[ 89]*x11        *x41    
        ;

    return v_l_e_q1q2_1_100                              ;
}
