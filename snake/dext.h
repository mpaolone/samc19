float x_e_dext_1200                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.7271023E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.67358534E-02,-0.15691748E-01, 0.10659558E+00, 0.34230834E+00,
         0.36512256E-01, 0.10245162E-01,-0.22224842E-02,-0.41722395E-02,
        -0.12579672E-01,-0.11760772E-01,-0.62241015E-04,-0.15811572E-02,
        -0.22101579E-02,-0.50440812E-02,-0.65944775E-03,-0.90880576E-03,
        -0.99675544E-03, 0.30967483E-03,-0.56965393E-03,-0.44166687E-03,
        -0.70458668E-03,-0.97812731E-02,-0.64909724E-02,-0.44757565E-02,
        -0.54032085E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_1200                                =v_x_e_dext_1200                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_1200                                =v_x_e_dext_1200                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_1200                                ;
}
float t_e_dext_1200                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5875040E+00;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.45416243E-02, 0.35703473E-02,-0.10228946E+00, 0.24674207E-01,
        -0.66355350E-02, 0.18687717E-02,-0.19630087E-02, 0.35591470E-02,
        -0.95886455E-04, 0.16605912E-02,-0.14828852E-02,-0.39887539E-03,
        -0.66296943E-03,-0.21460264E-03,-0.77791052E-03, 0.19441331E-03,
         0.16125789E-03,-0.13417167E-03, 0.15057469E-03, 0.44117004E-03,
         0.28857644E-03, 0.42119692E-03,-0.40789749E-03, 0.70562045E-03,
         0.17413062E-03,-0.19090958E-04, 0.52300963E-03, 0.18648803E-02,
         0.65145391E-03, 0.10684051E-03, 0.48488058E-04, 0.10534891E-03,
         0.45510786E-03, 0.11097261E-03,-0.28241985E-03,-0.28379762E-03,
         0.20809721E-02, 0.21682859E-02, 0.17563449E-02, 0.53475214E-04,
        -0.31403353E-04, 0.44436551E-04,-0.18919527E-03,-0.10275927E-03,
         0.88141847E-03, 0.24718954E-03, 0.10178876E-02, 0.10390560E-03,
         0.14416165E-03,-0.17362616E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]            *x42    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_1200                                =v_t_e_dext_1200                                
        +coeff[  8]        *x32    *x52
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]            *x42*x51
        +coeff[ 11]    *x22            
        +coeff[ 12]        *x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]                *x52
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_1200                                =v_t_e_dext_1200                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_dext_1200                                =v_t_e_dext_1200                                
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41*x51
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]        *x31*x43    
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_dext_1200                                =v_t_e_dext_1200                                
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23*x31*x41    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x21*x31*x43    
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]                *x53
        +coeff[ 42]    *x22*x31*x41    
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_dext_1200                                =v_t_e_dext_1200                                
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]*x11*x22    *x42    
        +coeff[ 48]    *x22*x31*x41*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_dext_1200                                ;
}
float y_e_dext_1200                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1039615E-03;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.51467403E-04, 0.10621029E+00,-0.57083797E-01,-0.52156013E-01,
        -0.20800237E-01, 0.38920630E-01, 0.20246603E-01,-0.26227007E-01,
        -0.11975732E-01, 0.19514726E-02,-0.78373384E-02,-0.41870931E-02,
        -0.21005366E-02,-0.57528221E-04,-0.42706498E-03, 0.81207184E-03,
        -0.46443720E-02, 0.13472245E-02,-0.22279378E-02,-0.14419380E-02,
        -0.22955243E-02,-0.47154156E-02,-0.95948699E-03, 0.55262138E-03,
         0.24681354E-02,-0.93821727E-03, 0.92976441E-03, 0.10787019E-02,
        -0.37165133E-02, 0.11736560E-02, 0.35264625E-02, 0.62522071E-03,
        -0.28464275E-02,-0.75802230E-02,-0.93094254E-03,-0.33168873E-03,
         0.53895579E-03, 0.98729599E-03, 0.19995615E-03, 0.58981468E-03,
        -0.19084852E-02, 0.39448036E-03, 0.21107221E-03,-0.12697988E-02,
        -0.23667119E-02,-0.10825273E-02, 0.27424994E-04,-0.21441560E-04,
         0.55094826E-03,-0.99131750E-04, 0.43485270E-03,-0.28062757E-03,
         0.12201645E-03,-0.60355995E-03, 0.27039277E-03,-0.73443525E-02,
        -0.38011326E-02, 0.12629409E-04,-0.38132381E-04,-0.10373835E-03,
         0.11655315E-03, 0.51880372E-03, 0.79014128E-04, 0.15826218E-03,
         0.29361488E-02, 0.18073721E-02, 0.27761693E-04, 0.11776810E-03,
         0.31117790E-04,-0.85683605E-04, 0.93065835E-04,-0.17950963E-04,
        -0.20826807E-04,-0.23508372E-04,-0.25995562E-03, 0.25297434E-03,
        -0.11083102E-03,-0.14085640E-03, 0.24989716E-03,-0.10904366E-03,
         0.30823585E-02,-0.75511918E-04, 0.15868207E-02, 0.20848878E-02,
         0.67567518E-04,-0.39555342E-03,-0.41985876E-03, 0.37732348E-03,
        -0.15025912E-03,-0.18572358E-02,-0.27335936E-02, 0.15822114E-03,
        -0.14425021E-02, 0.98570836E-05, 0.13284073E-05, 0.19183364E-04,
         0.25892219E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x24*x31        
        +coeff[ 46]    *x21            
        +coeff[ 47]                *x51
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]    *x22*x31*x44    
        +coeff[ 56]    *x22*x33*x42    
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11*x22    *x41    
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]    *x22    *x41*x52
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]    *x23*x32*x41    
        +coeff[ 66]*x11*x21    *x45    
        +coeff[ 67]    *x23    *x45    
        +coeff[ 68]    *x21*x32*x45    
        +coeff[ 69]*x11*x23*x32*x41    
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x11*x21    *x41*x51
        +coeff[ 74]        *x33*x42    
        +coeff[ 75]*x11*x21    *x43    
        +coeff[ 76]        *x34*x41    
        +coeff[ 77]    *x21*x31*x42*x51
        +coeff[ 78]*x11*x21*x32*x41    
        +coeff[ 79]    *x21*x32*x41*x51
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 80]    *x21*x31*x44    
        +coeff[ 81]*x11*x23*x31        
        +coeff[ 82]    *x23    *x43    
        +coeff[ 83]    *x21*x33*x42    
        +coeff[ 84]    *x22*x31    *x52
        +coeff[ 85]    *x22    *x43*x51
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24    *x41*x51
    ;
    v_y_e_dext_1200                                =v_y_e_dext_1200                                
        +coeff[ 89]    *x24    *x43    
        +coeff[ 90]    *x24*x31*x42    
        +coeff[ 91]*x11*x23    *x43    
        +coeff[ 92]    *x24*x32*x41    
        +coeff[ 93]        *x31*x41    
        +coeff[ 94]            *x44    
        +coeff[ 95]    *x22    *x42    
        +coeff[ 96]*x11    *x31*x42    
        ;

    return v_y_e_dext_1200                                ;
}
float p_e_dext_1200                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.3385060E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.33585849E-04,-0.20954760E-01, 0.61576799E-02,-0.12364735E-01,
         0.38990981E-02, 0.89429589E-02,-0.73845442E-02,-0.23547136E-02,
         0.43264142E-03, 0.82887054E-04,-0.10377826E-03, 0.83826424E-04,
        -0.41970112E-02,-0.20570937E-02, 0.42820905E-03,-0.20746468E-02,
        -0.10349237E-02, 0.16600334E-02,-0.41962168E-04, 0.17120769E-03,
        -0.24266164E-03,-0.99865918E-03,-0.23725940E-03, 0.42818315E-03,
        -0.33365059E-03,-0.98644686E-03, 0.81105257E-03, 0.93317265E-03,
        -0.37956849E-03, 0.68341244E-04,-0.73171119E-04, 0.52688323E-03,
         0.46164679E-03,-0.13292325E-03, 0.41724954E-03,-0.27268639E-03,
         0.22568900E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_1200                                =v_p_e_dext_1200                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_1200                                =v_p_e_dext_1200                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_1200                                =v_p_e_dext_1200                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_dext_1200                                =v_p_e_dext_1200                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dext_1200                                ;
}
float l_e_dext_1200                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2016310E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.19885037E-01,-0.48237032E+00,-0.78643695E-01, 0.20309411E-01,
        -0.32048754E-01,-0.36781549E-01,-0.44685644E-02, 0.21890108E-02,
         0.14170012E-01, 0.13588084E-01, 0.34614871E-02,-0.32111655E-02,
         0.12166370E-02, 0.53941784E-02, 0.66526998E-04,-0.25521658E-03,
        -0.78650267E-03,-0.24592190E-03,-0.16034485E-02, 0.90971624E-03,
        -0.17125723E-02, 0.96411759E-03, 0.81046287E-03, 0.70221734E-03,
        -0.41338085E-03, 0.15643704E-02, 0.11752460E-01, 0.97411890E-02,
         0.76332674E-02, 0.60885707E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_dext_1200                                =v_l_e_dext_1200                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x34        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_dext_1200                                =v_l_e_dext_1200                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x32        
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x24            
    ;
    v_l_e_dext_1200                                =v_l_e_dext_1200                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21*x32*x44    
        ;

    return v_l_e_dext_1200                                ;
}
float x_e_dext_1100                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.8029618E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.74802167E-02,-0.15682356E-01, 0.10661781E+00, 0.34253159E+00,
         0.36539830E-01, 0.10263044E-01,-0.21998556E-02,-0.41339705E-02,
        -0.12355467E-01,-0.11784808E-01,-0.64952081E-04,-0.18385994E-02,
        -0.21352756E-02,-0.48955521E-02,-0.62310195E-03,-0.88444009E-03,
        -0.94956346E-03, 0.33445511E-03,-0.55609364E-03,-0.46711057E-03,
        -0.75203989E-03,-0.10099467E-01,-0.65699974E-02,-0.47781337E-02,
        -0.56711640E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_1100                                =v_x_e_dext_1100                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_1100                                =v_x_e_dext_1100                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_1100                                ;
}
float t_e_dext_1100                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5875823E+00;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.45461273E-02, 0.35687711E-02,-0.10231353E+00, 0.24684273E-01,
        -0.66271788E-02, 0.19508580E-02,-0.19433250E-02, 0.35558471E-02,
        -0.85518404E-04, 0.16502532E-02,-0.14623691E-02,-0.39037297E-03,
        -0.65405568E-03,-0.20947294E-03,-0.78932440E-03, 0.17338367E-03,
         0.15223656E-03,-0.19979388E-03, 0.10969379E-03, 0.41083017E-03,
         0.33793034E-03, 0.32316253E-03,-0.44653288E-03,-0.28894836E-03,
         0.67223056E-03, 0.18849083E-03,-0.37004411E-04, 0.49420539E-03,
         0.18930121E-02, 0.61766675E-03, 0.91857706E-04, 0.56490026E-04,
         0.73399977E-04, 0.49986909E-04,-0.24961453E-03, 0.18839437E-04,
         0.44818822E-03, 0.11527915E-03,-0.26503418E-03,-0.26740853E-03,
         0.21327578E-02, 0.21289575E-02, 0.17660003E-02, 0.59742466E-04,
        -0.13420536E-03,-0.45400779E-04, 0.91415871E-03, 0.11396419E-03,
         0.97404671E-03, 0.12793620E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]            *x42    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_1100                                =v_t_e_dext_1100                                
        +coeff[  8]        *x32    *x52
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]            *x42*x51
        +coeff[ 11]    *x22            
        +coeff[ 12]        *x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]                *x52
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_1100                                =v_t_e_dext_1100                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_1100                                =v_t_e_dext_1100                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x21*x31*x41*x53
        +coeff[ 32]*x11    *x31*x41    
        +coeff[ 33]                *x53
        +coeff[ 34]        *x32*x42    
    ;
    v_t_e_dext_1100                                =v_t_e_dext_1100                                
        +coeff[ 35]        *x31*x43    
        +coeff[ 36]    *x21*x31*x41*x51
        +coeff[ 37]    *x22        *x52
        +coeff[ 38]        *x31*x41*x52
        +coeff[ 39]            *x42*x52
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_dext_1100                                =v_t_e_dext_1100                                
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x23*x32        
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_dext_1100                                ;
}
float y_e_dext_1100                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7742770E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.65210281E-03, 0.10590827E+00,-0.57266444E-01,-0.52106414E-01,
        -0.20809114E-01, 0.38917635E-01, 0.20265764E-01,-0.26375687E-01,
        -0.11993613E-01, 0.19370926E-02,-0.79065785E-02,-0.41958974E-02,
        -0.21054747E-02,-0.30343690E-04,-0.24968846E-03, 0.80898398E-03,
        -0.47764932E-02, 0.12944412E-02,-0.22242283E-02,-0.14404353E-02,
        -0.22863718E-02,-0.47636614E-02,-0.96637360E-03, 0.51324372E-03,
         0.24426512E-02,-0.95201138E-03, 0.10016869E-02, 0.11124399E-02,
        -0.35102109E-02, 0.12496067E-02, 0.36231868E-02, 0.58533222E-03,
        -0.27962381E-02,-0.79769287E-02,-0.11451638E-02, 0.49258757E-04,
        -0.38756905E-04, 0.57542493E-03, 0.95132238E-03, 0.23746246E-03,
         0.58433466E-03, 0.41171201E-03, 0.20489721E-03,-0.11122567E-02,
        -0.18817671E-02,-0.10842177E-02,-0.56756433E-03,-0.72895484E-02,
        -0.41180854E-02,-0.81509927E-04, 0.49919274E-03,-0.10355484E-03,
         0.41804131E-03, 0.13245245E-03,-0.17636296E-02, 0.27672536E-03,
         0.31825337E-04,-0.46452427E-04,-0.72813978E-04, 0.40842991E-03,
         0.12489101E-03, 0.54637139E-03, 0.78247256E-04, 0.21360534E-03,
         0.30795562E-02, 0.17978916E-02,-0.17016448E-03,-0.31033671E-03,
         0.17270770E-04,-0.22760651E-04,-0.24279327E-04,-0.19886522E-03,
         0.91272841E-05,-0.13299154E-03,-0.14065312E-03, 0.52606309E-04,
        -0.10172944E-03, 0.30511348E-02, 0.17593312E-02, 0.13464491E-03,
         0.20702586E-02, 0.77245269E-04,-0.36374287E-03,-0.34666745E-03,
         0.30275781E-03,-0.16237132E-03,-0.20218275E-02,-0.29983136E-02,
        -0.18476949E-02, 0.31268373E-04,-0.35906790E-03, 0.10824690E-04,
         0.77964332E-05, 0.55735568E-05, 0.49551286E-05, 0.20759731E-04,
         0.19597688E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 35]    *x21            
        +coeff[ 36]                *x51
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x24*x31        
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]    *x22*x31*x44    
        +coeff[ 48]    *x22*x33*x42    
        +coeff[ 49]    *x21*x32*x45    
        +coeff[ 50]    *x21    *x43    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 53]        *x33    *x51
        +coeff[ 54]    *x22    *x43    
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]    *x22            
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]*x11*x22    *x41    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]    *x23*x32*x41    
        +coeff[ 66]    *x23    *x45    
        +coeff[ 67]    *x23*x31*x44    
        +coeff[ 68]        *x31*x41    
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x11    *x31    *x51
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 71]        *x31*x44    
        +coeff[ 72]*x12        *x42    
        +coeff[ 73]        *x33*x42    
        +coeff[ 74]    *x21*x31*x42*x51
        +coeff[ 75]*x11*x23    *x41    
        +coeff[ 76]    *x21*x32*x41*x51
        +coeff[ 77]    *x21*x31*x44    
        +coeff[ 78]    *x23    *x43    
        +coeff[ 79]    *x22    *x41*x52
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 80]    *x21*x33*x42    
        +coeff[ 81]    *x22*x31    *x52
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x23*x33        
        +coeff[ 85]    *x24    *x41*x51
        +coeff[ 86]    *x24    *x43    
        +coeff[ 87]    *x24*x31*x42    
        +coeff[ 88]    *x24*x32*x41    
    ;
    v_y_e_dext_1100                                =v_y_e_dext_1100                                
        +coeff[ 89]*x11        *x42*x53
        +coeff[ 90]    *x24*x33        
        +coeff[ 91]            *x42    
        +coeff[ 92]        *x32        
        +coeff[ 93]    *x21        *x51
        +coeff[ 94]                *x52
        +coeff[ 95]*x11    *x31*x42    
        +coeff[ 96]        *x31*x41*x52
        ;

    return v_y_e_dext_1100                                ;
}
float p_e_dext_1100                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1671439E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.16099034E-03,-0.20994097E-01, 0.61070141E-02,-0.12352518E-01,
         0.39034267E-02, 0.89385174E-02,-0.74106506E-02,-0.23545669E-02,
         0.51827892E-03, 0.77857665E-04, 0.93129429E-05,-0.57093388E-04,
        -0.42085960E-02,-0.21030942E-02, 0.42015346E-03,-0.20697282E-02,
        -0.10377072E-02, 0.16494517E-02,-0.70608435E-05, 0.17006486E-03,
        -0.24062801E-03,-0.10265529E-02,-0.23791376E-03, 0.42452448E-03,
        -0.33724049E-03,-0.98608609E-03, 0.82109094E-03, 0.93480787E-03,
        -0.38482260E-03, 0.45169075E-03, 0.70861097E-04,-0.69234557E-04,
         0.54015539E-03, 0.46822333E-03,-0.10737080E-03,-0.27232411E-03,
         0.22397027E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_1100                                =v_p_e_dext_1100                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_1100                                =v_p_e_dext_1100                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_1100                                =v_p_e_dext_1100                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dext_1100                                =v_p_e_dext_1100                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dext_1100                                ;
}
float l_e_dext_1100                                (float *x,int m){
    int ncoeff= 26;
    float avdat= -0.2069260E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
         0.20636234E-01,-0.48289067E+00,-0.78767993E-01, 0.20157311E-01,
        -0.32201413E-01,-0.36717184E-01, 0.20613582E-02, 0.16995458E-01,
         0.14382442E-01, 0.34425496E-02,-0.45767534E-02,-0.32866618E-02,
         0.11844910E-02,-0.95838163E-03, 0.55767652E-02, 0.78576493E-04,
        -0.17541820E-02,-0.14216879E-02, 0.11929699E-02, 0.11262128E-02,
         0.82055517E-02, 0.12869154E-02, 0.92543912E-03, 0.17231439E-02,
         0.85980259E-03, 0.10674023E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_1100                                =v_l_e_dext_1100                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x32        
    ;
    v_l_e_dext_1100                                =v_l_e_dext_1100                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23*x31*x41    
        ;

    return v_l_e_dext_1100                                ;
}
float x_e_dext_1000                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.7190583E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.64467071E-02,-0.15686763E-01, 0.10661236E+00, 0.34264982E+00,
         0.36489084E-01, 0.10246736E-01,-0.22079512E-02,-0.41176369E-02,
        -0.12551348E-01,-0.11755714E-01, 0.11427423E-03,-0.17802295E-02,
        -0.21565591E-02,-0.50250143E-02, 0.33644450E-03,-0.63400471E-03,
        -0.88716560E-03,-0.96233888E-03,-0.66368812E-03,-0.43825593E-03,
        -0.71910530E-03,-0.97167697E-02,-0.65496089E-02,-0.45708278E-02,
        -0.52714846E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_1000                                =v_x_e_dext_1000                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]*x11*x21            
        +coeff[ 16]        *x32        
    ;
    v_x_e_dext_1000                                =v_x_e_dext_1000                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_1000                                ;
}
float t_e_dext_1000                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5874367E+00;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47120731E-02, 0.35713729E-02,-0.10234666E+00, 0.24675962E-01,
        -0.66249520E-02, 0.19630282E-02, 0.35487814E-02,-0.23710253E-03,
        -0.19265122E-02,-0.77921350E-03, 0.16770383E-02,-0.14539483E-02,
        -0.45382755E-03,-0.65811886E-03,-0.20752833E-03, 0.19642105E-03,
         0.15773886E-03,-0.38439172E-03,-0.15252719E-03, 0.15201060E-03,
         0.44610130E-03, 0.33124138E-03, 0.42276943E-03,-0.30296214E-03,
         0.65002241E-03, 0.18541308E-03,-0.22518845E-04, 0.49222499E-03,
         0.18929109E-02, 0.59538480E-03, 0.64822983E-04, 0.57677797E-04,
        -0.12976385E-03, 0.46490386E-03, 0.10410099E-03,-0.28179123E-03,
        -0.27182093E-03, 0.20649682E-02, 0.20585309E-02, 0.17664812E-02,
         0.54049760E-04, 0.40022209E-04,-0.51265273E-04,-0.93113318E-04,
         0.89303398E-03, 0.17850351E-03, 0.93923236E-03,-0.16722306E-03,
        -0.71011364E-05,-0.95190426E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_1000                                =v_t_e_dext_1000                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_1000                                =v_t_e_dext_1000                                
        +coeff[ 17]    *x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_1000                                =v_t_e_dext_1000                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]*x11    *x31*x41    
        +coeff[ 32]        *x33*x41    
        +coeff[ 33]    *x21*x31*x41*x51
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_dext_1000                                =v_t_e_dext_1000                                
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23*x31*x41    
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]                *x53
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_dext_1000                                =v_t_e_dext_1000                                
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]*x12                
        +coeff[ 49]    *x22*x31        
        ;

    return v_t_e_dext_1000                                ;
}
float y_e_dext_1000                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.3489675E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.31214202E-03, 0.10547314E+00,-0.57507966E-01,-0.52043356E-01,
        -0.20780368E-01, 0.38948737E-01, 0.20272115E-01,-0.26155608E-01,
        -0.11823642E-01, 0.19433886E-02,-0.77288072E-02,-0.41827396E-02,
        -0.21024561E-02,-0.28152699E-04,-0.52885455E-03, 0.81438891E-03,
        -0.45444197E-02, 0.13007461E-02,-0.22224509E-02,-0.14366453E-02,
        -0.24509903E-02,-0.47115111E-02,-0.93178015E-03, 0.56113617E-03,
         0.24295626E-02,-0.94716996E-03, 0.87609084E-03, 0.11132100E-02,
        -0.43493658E-02, 0.12023684E-02, 0.33962897E-02, 0.74551033E-03,
        -0.28596248E-02,-0.68974909E-02,-0.56144333E-03, 0.47682534E-03,
         0.98620949E-03, 0.23731576E-03, 0.59729337E-03, 0.40189401E-03,
         0.19843836E-03,-0.12990971E-02,-0.28990072E-02,-0.12805843E-02,
        -0.85113093E-03,-0.68799332E-02, 0.42728969E-03,-0.10799145E-03,
         0.40376183E-03, 0.12732901E-03,-0.19991847E-02, 0.26817565E-03,
        -0.32522103E-02, 0.17205664E-04,-0.11274670E-04,-0.41273219E-04,
        -0.87609340E-04, 0.41143308E-03, 0.10349935E-03, 0.55344356E-03,
         0.85331965E-04, 0.22645250E-03, 0.13938145E-03, 0.28295922E-02,
         0.15950030E-03, 0.20729526E-03, 0.56017027E-03, 0.82000758E-03,
         0.35472916E-03,-0.27981497E-03, 0.10076238E-04,-0.20792413E-04,
        -0.23417855E-04,-0.34539326E-03,-0.38115110E-03,-0.19111963E-03,
        -0.17162604E-03, 0.33531396E-04,-0.12005233E-03, 0.31044276E-02,
        -0.85000276E-04, 0.16882787E-02, 0.20786272E-02, 0.61593368E-04,
        -0.16412616E-04, 0.18736018E-02,-0.39993075E-03, 0.27306192E-03,
         0.23033806E-04,-0.17120601E-02,-0.21653590E-02,-0.11487750E-02,
        -0.46565599E-03, 0.10461881E-04, 0.57834091E-05,-0.10034764E-04,
        -0.96840295E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24    *x41    
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]    *x22*x31*x44    
        +coeff[ 46]    *x21    *x43    
        +coeff[ 47]*x11        *x41*x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]        *x33    *x51
        +coeff[ 50]    *x22    *x43    
        +coeff[ 51]    *x23    *x41*x51
        +coeff[ 52]    *x22*x33*x42    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 53]    *x21            
        +coeff[ 54]                *x51
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11*x22    *x41    
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]            *x41*x53
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]        *x31    *x53
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 62]    *x22    *x41*x52
        +coeff[ 63]    *x23*x31*x42    
        +coeff[ 64]    *x23    *x45    
        +coeff[ 65]    *x21*x32*x45    
        +coeff[ 66]    *x23*x31*x44    
        +coeff[ 67]    *x23*x32*x43    
        +coeff[ 68]    *x23*x33*x42    
        +coeff[ 69]    *x23*x34*x41    
        +coeff[ 70]    *x22            
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]        *x31*x44    
        +coeff[ 74]        *x33*x42    
        +coeff[ 75]        *x34*x41    
        +coeff[ 76]    *x21*x31*x42*x51
        +coeff[ 77]*x11*x23    *x41    
        +coeff[ 78]    *x21*x32*x41*x51
        +coeff[ 79]    *x21*x31*x44    
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 80]*x11*x23*x31        
        +coeff[ 81]    *x23    *x43    
        +coeff[ 82]    *x21*x33*x42    
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]    *x23*x32*x41    
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24    *x41*x51
    ;
    v_y_e_dext_1000                                =v_y_e_dext_1000                                
        +coeff[ 89]    *x24    *x43    
        +coeff[ 90]    *x24*x31*x42    
        +coeff[ 91]    *x24*x32*x41    
        +coeff[ 92]    *x24    *x43*x51
        +coeff[ 93]            *x42    
        +coeff[ 94]        *x31*x41    
        +coeff[ 95]    *x21    *x42    
        +coeff[ 96]        *x31*x41*x51
        ;

    return v_y_e_dext_1000                                ;
}
float p_e_dext_1000                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1372672E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.13593606E-03,-0.21041978E-01, 0.60200952E-02,-0.12341348E-01,
         0.39044637E-02, 0.89421785E-02,-0.73587531E-02,-0.23464726E-02,
         0.42269652E-03, 0.76650700E-04,-0.10742286E-03, 0.90817208E-04,
        -0.41807569E-02,-0.20475835E-02, 0.42705765E-03,-0.20657422E-02,
        -0.10302444E-02, 0.42345840E-03, 0.16594304E-02,-0.42337360E-04,
         0.17014677E-03,-0.24061807E-03,-0.99335413E-03,-0.23672904E-03,
        -0.32889645E-03,-0.10004824E-02, 0.81731688E-03, 0.90015196E-03,
        -0.38679747E-03, 0.68438043E-04,-0.73591676E-04, 0.51791174E-03,
         0.44509702E-03,-0.12133084E-03, 0.41377480E-03,-0.28535005E-03,
         0.20692695E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_1000                                =v_p_e_dext_1000                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_1000                                =v_p_e_dext_1000                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x23    *x41    
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_1000                                =v_p_e_dext_1000                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_dext_1000                                =v_p_e_dext_1000                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dext_1000                                ;
}
float l_e_dext_1000                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1990079E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.19768683E-01,-0.48278099E+00,-0.78639865E-01, 0.20316383E-01,
        -0.31959515E-01,-0.36043607E-01,-0.45476463E-02, 0.21665297E-02,
         0.16338333E-01, 0.13332408E-01, 0.39354884E-02,-0.33223471E-02,
         0.11989134E-02,-0.94478414E-03, 0.55444366E-02,-0.18265949E-02,
        -0.43445334E-03,-0.54918590E-03,-0.49981609E-03, 0.11489439E-02,
        -0.13708496E-02, 0.11690116E-02, 0.10455956E-02, 0.79505274E-03,
         0.15875457E-02,-0.11188901E-02, 0.12437899E-01, 0.10356488E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_dext_1000                                =v_l_e_dext_1000                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_dext_1000                                =v_l_e_dext_1000                                
        +coeff[ 17]        *x34        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x32        
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_dext_1000                                =v_l_e_dext_1000                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        ;

    return v_l_e_dext_1000                                ;
}
float x_e_dext_900                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.9953164E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.92347404E-02,-0.15687646E-01, 0.10659188E+00, 0.34290272E+00,
         0.36521725E-01, 0.10293497E-01,-0.22257580E-02,-0.40808688E-02,
        -0.13249316E-01,-0.11362493E-01, 0.72641607E-03,-0.24064123E-02,
        -0.21756976E-02,-0.48314640E-02, 0.33034966E-03,-0.63854805E-03,
        -0.88363665E-03,-0.82788279E-03,-0.54794765E-03,-0.42652845E-03,
        -0.72471652E-03,-0.83789313E-02,-0.70093269E-02,-0.22982724E-02,
        -0.79136584E-02,-0.49690604E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_900                                =v_x_e_dext_900                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]*x11*x21            
        +coeff[ 16]        *x32        
    ;
    v_x_e_dext_900                                =v_x_e_dext_900                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x21*x32*x42*x52
        +coeff[ 25]    *x21*x31*x43*x52
        ;

    return v_x_e_dext_900                                ;
}
float t_e_dext_900                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5873654E+00;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.48471461E-02, 0.35706535E-02,-0.10244997E+00, 0.24686810E-01,
        -0.66409116E-02, 0.18654694E-02,-0.19189455E-02, 0.35531693E-02,
        -0.10762766E-03, 0.16657904E-02,-0.14609358E-02,-0.39734598E-03,
        -0.67178987E-03,-0.22126503E-03,-0.77293708E-03, 0.17361584E-03,
         0.15762338E-03,-0.12743271E-03, 0.44217886E-03, 0.43367787E-03,
        -0.43772277E-03, 0.65756362E-03, 0.62517653E-03, 0.18643154E-03,
         0.33908265E-03,-0.10157555E-04, 0.55036752E-03, 0.18096153E-02,
         0.40414310E-04, 0.16951388E-03, 0.65995184E-04, 0.14267722E-03,
         0.10501200E-03, 0.30523070E-03, 0.11504107E-03,-0.27620708E-03,
        -0.28355297E-03, 0.20094598E-02, 0.21867703E-02, 0.17880783E-02,
        -0.21409082E-03, 0.51972565E-04,-0.21205775E-04, 0.36392084E-04,
        -0.25104327E-03,-0.13332933E-03, 0.88522845E-03, 0.14599739E-03,
         0.99192548E-03, 0.28475473E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]            *x42    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_900                                =v_t_e_dext_900                                
        +coeff[  8]        *x32    *x52
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]            *x42*x51
        +coeff[ 11]    *x22            
        +coeff[ 12]        *x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]                *x52
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_900                                =v_t_e_dext_900                                
        +coeff[ 17]    *x23            
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_dext_900                                =v_t_e_dext_900                                
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12*x21*x31*x41*x51
        +coeff[ 29]    *x21*x33*x41*x51
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]    *x22        *x51
        +coeff[ 32]        *x31*x43    
        +coeff[ 33]    *x21*x31*x41*x51
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_dext_900                                =v_t_e_dext_900                                
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23*x31*x41    
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x23        *x52
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dext_900                                =v_t_e_dext_900                                
        +coeff[ 44]    *x22*x31*x41    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]    *x23*x32        
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x21*x31*x43*x51
        ;

    return v_t_e_dext_900                                ;
}
float y_e_dext_900                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1093262E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.38563187E-04, 0.10504641E+00,-0.57744186E-01,-0.52030489E-01,
        -0.20769875E-01, 0.38932227E-01, 0.20282198E-01,-0.26293537E-01,
        -0.11965835E-01, 0.19389957E-02,-0.77876090E-02,-0.41852579E-02,
        -0.21046242E-02,-0.31008654E-04,-0.40955044E-03, 0.81288081E-03,
        -0.46574385E-02, 0.13401182E-02,-0.22257760E-02,-0.14451439E-02,
        -0.22970110E-02,-0.47263382E-02,-0.95532497E-03, 0.56642143E-03,
         0.24517393E-02,-0.94342511E-03, 0.86765114E-03, 0.10990766E-02,
        -0.36149046E-02, 0.12994283E-02, 0.38257495E-02, 0.61497989E-03,
        -0.29696394E-02,-0.79398649E-02,-0.93629491E-03, 0.56765490E-03,
         0.94285066E-03, 0.23793484E-03, 0.54362731E-03, 0.40073620E-03,
         0.21267441E-03,-0.12210816E-02,-0.22898398E-02,-0.11169390E-02,
        -0.66366646E-03,-0.74830619E-02, 0.25763700E-04,-0.19281799E-04,
         0.37301052E-03,-0.10048820E-03, 0.41040810E-03, 0.13535972E-03,
        -0.17008189E-02, 0.26399558E-03,-0.39592925E-02,-0.42337982E-04,
        -0.27517157E-04,-0.90430942E-04, 0.36887362E-03, 0.11186362E-03,
         0.50197012E-03, 0.79918260E-04, 0.18762563E-03, 0.32902842E-02,
         0.18026147E-02, 0.17724172E-03, 0.30196372E-02, 0.16837646E-02,
        -0.19789576E-02,-0.28139050E-02,-0.15025664E-02, 0.14207112E-04,
        -0.83368914E-05,-0.15806090E-03,-0.12067199E-03,-0.94268973E-04,
         0.20979000E-02, 0.85571221E-04,-0.40846912E-03, 0.16510404E-03,
        -0.32684443E-03, 0.29399773E-03,-0.13785491E-03,-0.22874305E-03,
         0.85902020E-05, 0.11037845E-04,-0.86788668E-05,-0.31573587E-03,
        -0.13331014E-04,-0.55460100E-05,-0.27807277E-04,-0.26678076E-03,
        -0.19482934E-04,-0.97937278E-04, 0.18223882E-04,-0.41033352E-04,
         0.19739475E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24    *x41    
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]    *x22*x31*x44    
        +coeff[ 46]    *x21            
        +coeff[ 47]                *x51
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x22    *x43    
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 53]    *x23    *x41*x51
        +coeff[ 54]    *x22*x33*x42    
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11    *x31    *x51
        +coeff[ 57]*x11*x22    *x41    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]            *x41*x53
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]        *x31    *x53
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 62]*x11*x21*x32*x41    
        +coeff[ 63]    *x21*x31*x44    
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]    *x22    *x41*x52
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]    *x23*x32*x41    
        +coeff[ 68]    *x24    *x43    
        +coeff[ 69]    *x24*x31*x42    
        +coeff[ 70]    *x24*x32*x41    
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 71]    *x22            
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]    *x21*x31*x42*x51
        +coeff[ 74]    *x21*x32*x41*x51
        +coeff[ 75]*x11*x23*x31        
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22*x31    *x52
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]        *x32*x43*x51
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 80]    *x22*x31*x42*x51
        +coeff[ 81]    *x23*x33        
        +coeff[ 82]    *x24    *x41*x51
        +coeff[ 83]    *x24*x33        
        +coeff[ 84]            *x42    
        +coeff[ 85]        *x31*x41    
        +coeff[ 86]        *x31*x41*x51
        +coeff[ 87]        *x31*x44    
        +coeff[ 88]*x11        *x42*x51
    ;
    v_y_e_dext_900                                =v_y_e_dext_900                                
        +coeff[ 89]*x11*x23            
        +coeff[ 90]*x11*x21    *x41*x51
        +coeff[ 91]        *x33*x42    
        +coeff[ 92]*x11*x21*x31    *x51
        +coeff[ 93]        *x34*x41    
        +coeff[ 94]*x12        *x41*x51
        +coeff[ 95]*x12*x22*x31        
        +coeff[ 96]*x11*x21*x32    *x51
        ;

    return v_y_e_dext_900                                ;
}
float p_e_dext_900                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.3129862E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.26231106E-04,-0.21101614E-01, 0.58821412E-02,-0.12336777E-01,
         0.39041848E-02, 0.89373803E-02,-0.69551715E-02,-0.23429883E-02,
         0.53579843E-03, 0.92779104E-04,-0.35590776E-04,-0.18362267E-03,
        -0.42057061E-02,-0.19516879E-02, 0.42332878E-03,-0.18791970E-02,
        -0.94103982E-03, 0.16636866E-02,-0.99549186E-03,-0.78864186E-03,
         0.17040227E-03,-0.90970489E-03,-0.22964638E-03, 0.42436653E-03,
        -0.33271586E-03,-0.38991039E-03, 0.81538875E-03,-0.64848577E-05,
         0.94265520E-03, 0.18784081E-03,-0.26004072E-03, 0.65159191E-04,
        -0.73189090E-04, 0.52708859E-03, 0.46266970E-03,-0.11404843E-03,
        -0.11782572E-02,-0.86189108E-03, 0.22750947E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x35 = x34*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_900                                =v_p_e_dext_900                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_900                                =v_p_e_dext_900                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_p_e_dext_900                                =v_p_e_dext_900                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]        *x35        
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]        *x33        
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]        *x31    *x52
        +coeff[ 33]    *x21*x32*x41    
        +coeff[ 34]    *x21    *x43    
    ;
    v_p_e_dext_900                                =v_p_e_dext_900                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x22    *x41*x52
        ;

    return v_p_e_dext_900                                ;
}
float l_e_dext_900                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2276163E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.22924747E-01,-0.48383495E+00,-0.78446604E-01, 0.20232575E-01,
        -0.32176241E-01,-0.36809508E-01, 0.21854716E-02, 0.12925853E-01,
         0.14873835E-01, 0.18270439E-02,-0.42104800E-02,-0.33242977E-02,
         0.11951919E-02,-0.97234093E-03, 0.65318430E-02,-0.18370770E-02,
         0.32038099E-03, 0.19384577E-04,-0.20253803E-02, 0.66836446E-03,
         0.80237928E-03, 0.87107526E-03, 0.86868968E-03, 0.14791357E-02,
        -0.87234267E-03, 0.14540589E-01, 0.56997892E-02, 0.87252529E-02,
         0.75138402E-02, 0.52657761E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_900                                =v_l_e_dext_900                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_dext_900                                =v_l_e_dext_900                                
        +coeff[ 17]        *x34        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_900                                =v_l_e_dext_900                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x23*x32*x42    
        +coeff[ 29]    *x23    *x44    
        ;

    return v_l_e_dext_900                                ;
}
float x_e_dext_800                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1051821E-01;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.96739884E-02,-0.15681818E-01, 0.10659849E+00, 0.34317017E+00,
         0.36513537E-01, 0.10277156E-01,-0.22244044E-02,-0.40926840E-02,
        -0.12668357E-01,-0.12000261E-01,-0.46565194E-04,-0.16804014E-02,
        -0.21515777E-02,-0.50334604E-02,-0.63276052E-03,-0.89282787E-03,
        -0.10748954E-02, 0.31028950E-03,-0.59091148E-03,-0.43533099E-03,
        -0.70857228E-03,-0.94435466E-02,-0.61282194E-02,-0.44676592E-02,
        -0.48343721E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_800                                =v_x_e_dext_800                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_800                                =v_x_e_dext_800                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_800                                ;
}
float t_e_dext_800                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5869390E+00;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.52632685E-02, 0.35650935E-02,-0.10245929E+00, 0.24690656E-01,
        -0.66194730E-02, 0.19560577E-02, 0.35813102E-02,-0.24357825E-03,
        -0.18965944E-02,-0.81077247E-03, 0.16025897E-02,-0.14680257E-02,
        -0.36748231E-03,-0.70402480E-03,-0.21693358E-03, 0.18537459E-03,
         0.15849534E-03,-0.42005730E-03,-0.13520922E-03, 0.82240753E-04,
         0.40266977E-03, 0.31506180E-03, 0.41679584E-03,-0.26990235E-03,
         0.65457262E-03, 0.19293380E-03,-0.33656572E-04, 0.47349511E-03,
         0.17783117E-02, 0.18052432E-03, 0.74417469E-04,-0.13217484E-03,
         0.39698367E-03, 0.17990355E-03,-0.26310768E-03,-0.26630034E-03,
         0.20282066E-02, 0.21005301E-02, 0.18035236E-02, 0.19650468E-03,
         0.73409430E-03,-0.20580794E-03, 0.53999320E-04, 0.43773263E-04,
         0.87727455E-03, 0.17413542E-03, 0.10442092E-02, 0.19479470E-03,
        -0.16583921E-03, 0.16937694E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_800                                =v_t_e_dext_800                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_800                                =v_t_e_dext_800                                
        +coeff[ 17]    *x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_800                                =v_t_e_dext_800                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x33*x41*x51
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]        *x33*x41    
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_dext_800                                =v_t_e_dext_800                                
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23*x31*x41    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x21*x31*x43    
        +coeff[ 39]    *x22*x31*x41*x51
        +coeff[ 40]    *x22    *x42*x51
        +coeff[ 41]    *x22*x32    *x52
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]                *x53
    ;
    v_t_e_dext_800                                =v_t_e_dext_800                                
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]    *x23        *x52
        +coeff[ 49]*x11        *x42    
        ;

    return v_t_e_dext_800                                ;
}
float y_e_dext_800                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.7257407E-03;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.72433345E-03, 0.10451102E+00,-0.58092531E-01,-0.52111220E-01,
        -0.20760907E-01, 0.38972698E-01, 0.20291833E-01,-0.25901344E-01,
        -0.11814045E-01, 0.19391432E-02,-0.76982845E-02,-0.41598910E-02,
        -0.20986586E-02,-0.24167304E-03,-0.63224870E-03, 0.81019301E-03,
        -0.45884619E-02, 0.13463827E-02,-0.22052729E-02,-0.14363844E-02,
        -0.24494661E-02,-0.44798157E-02,-0.92746632E-03, 0.55310922E-03,
         0.26210009E-02,-0.94118278E-03, 0.65391843E-03, 0.15511235E-02,
         0.12943585E-02, 0.11327974E-02,-0.36936225E-02,-0.46536545E-02,
        -0.28614288E-02, 0.99113921E-03, 0.24765276E-03, 0.60246629E-03,
         0.40064298E-03, 0.20737937E-03,-0.16364963E-02,-0.12765293E-02,
        -0.87754754E-03,-0.78898665E-04,-0.90528389E-04,-0.41307480E-03,
         0.13840177E-03, 0.24972091E-03,-0.45869529E-04,-0.27102875E-04,
         0.48344574E-03,-0.85757099E-04,-0.38145346E-03, 0.39740512E-03,
         0.11090876E-03, 0.54246403E-03, 0.83684143E-04, 0.23223752E-03,
         0.13684390E-03, 0.13588836E-02,-0.64282892E-02,-0.65184995E-02,
         0.50701655E-03, 0.24704977E-02, 0.38870706E-02, 0.25643767E-02,
         0.15063182E-02,-0.14622665E-02,-0.67876040E-05, 0.49823216E-05,
        -0.22167251E-04,-0.96067415E-04,-0.23877558E-03, 0.10831457E-02,
        -0.67046218E-04,-0.15922321E-03, 0.22242558E-02,-0.76179509E-04,
         0.11228567E-02, 0.22147521E-02, 0.11324086E-02, 0.73732612E-04,
         0.96390999E-04,-0.41387219E-03, 0.25177884E-03,-0.12503458E-02,
         0.40802770E-04,-0.18844011E-02,-0.31920490E-02,-0.88629097E-03,
        -0.92503557E-03,-0.58544439E-03,-0.11702423E-04,-0.63471707E-04,
        -0.12178270E-04,-0.46686742E-04,-0.30782368E-04,-0.18692026E-04,
        -0.37001613E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]*x11        *x41*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x23    *x41*x51
        +coeff[ 46]*x12        *x41    
        +coeff[ 47]*x11    *x31    *x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]        *x33*x42    
        +coeff[ 51]*x11*x21    *x43    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 53]*x11*x21*x31*x42    
        +coeff[ 54]        *x31    *x53
        +coeff[ 55]*x11*x21*x32*x41    
        +coeff[ 56]    *x22    *x41*x52
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x22*x31*x44    
        +coeff[ 59]    *x22*x32*x43    
        +coeff[ 60]    *x23    *x45    
        +coeff[ 61]    *x23*x31*x44    
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 62]    *x23*x32*x43    
        +coeff[ 63]    *x23*x33*x42    
        +coeff[ 64]    *x23*x34*x41    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]    *x21            
        +coeff[ 67]                *x51
        +coeff[ 68]*x12    *x31        
        +coeff[ 69]        *x34*x41    
        +coeff[ 70]    *x21*x31*x42*x51
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 71]    *x21    *x45    
        +coeff[ 72]            *x43*x52
        +coeff[ 73]    *x21*x32*x41*x51
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]*x11*x23*x31        
        +coeff[ 76]    *x23    *x43    
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]    *x21*x33*x42    
        +coeff[ 79]    *x22*x31    *x52
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 80]    *x22    *x43*x51
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]    *x24    *x41*x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_dext_800                                =v_y_e_dext_800                                
        +coeff[ 89]    *x24    *x43*x51
        +coeff[ 90]    *x21    *x42*x51
        +coeff[ 91]    *x21    *x43*x51
        +coeff[ 92]*x11        *x41*x52
        +coeff[ 93]        *x31*x42*x52
        +coeff[ 94]*x11*x22    *x41*x51
        +coeff[ 95]*x11*x21*x32    *x51
        +coeff[ 96]*x13*x21    *x41    
        ;

    return v_y_e_dext_800                                ;
}
float p_e_dext_800                                (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.6154094E-04;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.63554908E-04,-0.21178374E-01, 0.57758000E-02,-0.12333828E-01,
         0.39072833E-02, 0.89441678E-02,-0.69597084E-02,-0.23383575E-02,
         0.41842868E-03, 0.75888587E-04,-0.28694753E-03, 0.87960019E-04,
        -0.41680592E-02,-0.18303078E-02, 0.42104776E-03,-0.18819765E-02,
        -0.95073791E-03, 0.42143115E-03, 0.16552800E-02,-0.99865487E-03,
        -0.78827405E-03, 0.17072500E-03,-0.90559031E-03,-0.22945149E-03,
        -0.33448683E-03,-0.38948466E-03, 0.81305840E-03,-0.12199073E-04,
         0.94042515E-03, 0.19249490E-03,-0.25329911E-03, 0.67073270E-04,
        -0.71337970E-04, 0.53559546E-03, 0.46432356E-03,-0.11116513E-03,
        -0.11758984E-02,-0.84591389E-03, 0.21070294E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x35 = x34*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_800                                =v_p_e_dext_800                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_800                                =v_p_e_dext_800                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x23    *x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_p_e_dext_800                                =v_p_e_dext_800                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]        *x35        
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]        *x33        
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]        *x31    *x52
        +coeff[ 33]    *x21*x32*x41    
        +coeff[ 34]    *x21    *x43    
    ;
    v_p_e_dext_800                                =v_p_e_dext_800                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x22    *x41*x52
        ;

    return v_p_e_dext_800                                ;
}
float l_e_dext_800                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.2387007E-01;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.23817748E-01,-0.48326463E+00,-0.78708887E-01, 0.20249734E-01,
        -0.32591529E-01,-0.36752444E-01, 0.21694815E-02, 0.16820883E-01,
         0.13671608E-01, 0.31472105E-02,-0.46616206E-02,-0.31376935E-02,
         0.12058605E-02, 0.58508688E-02, 0.52400120E-03,-0.18527622E-02,
        -0.87107602E-03,-0.54444390E-03,-0.19923530E-02, 0.10718062E-02,
         0.14813422E-02, 0.12858212E-02, 0.85969974E-03, 0.20329603E-02,
         0.17555269E-02, 0.99767847E-02, 0.95254751E-02, 0.12526341E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_800                                =v_l_e_dext_800                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_dext_800                                =v_l_e_dext_800                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_800                                =v_l_e_dext_800                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x22*x32    *x51
        ;

    return v_l_e_dext_800                                ;
}
float x_e_dext_700                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.9520847E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.87303417E-02,-0.15662618E-01, 0.10659473E+00, 0.34348381E+00,
         0.36551867E-01, 0.10306398E-01,-0.97885037E-04,-0.12441870E-01,
        -0.11614003E-01,-0.14479666E-05,-0.19875790E-02,-0.22069842E-02,
        -0.21660200E-02,-0.40381630E-02,-0.48391731E-02,-0.64643956E-03,
        -0.89266006E-03,-0.83764392E-03, 0.31451893E-03,-0.58197236E-03,
        -0.50504348E-03,-0.69771928E-03,-0.99899312E-02,-0.68560271E-02,
        -0.44307895E-02,-0.49572736E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]            *x42*x52
        +coeff[  7]    *x21*x31*x41    
    ;
    v_x_e_dext_700                                =v_x_e_dext_700                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]                *x52
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]            *x42    
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]*x11*x21            
        +coeff[ 16]        *x32        
    ;
    v_x_e_dext_700                                =v_x_e_dext_700                                
        +coeff[ 17]    *x23            
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x32*x42    
        ;

    return v_x_e_dext_700                                ;
}
float t_e_dext_700                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5872718E+00;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.48817471E-02,-0.10256512E+00, 0.24689436E-01,-0.66409586E-02,
        -0.17726048E-05, 0.35705834E-02, 0.19464564E-02,-0.18752905E-02,
         0.35095802E-02,-0.10713332E-03, 0.15945920E-02,-0.14590222E-02,
        -0.41153200E-03,-0.67906128E-03,-0.21661389E-03,-0.78227167E-03,
         0.15258472E-03, 0.16039363E-03,-0.15444991E-03, 0.10139882E-03,
         0.40765267E-03, 0.32451266E-03, 0.44598326E-03,-0.43710571E-03,
        -0.26253096E-03, 0.64006663E-03, 0.19920962E-03,-0.10941824E-04,
         0.54360705E-03, 0.19094000E-02, 0.69879077E-03, 0.19706621E-03,
         0.64013395E-04,-0.24132525E-03, 0.42614967E-03, 0.13317812E-03,
        -0.26938281E-03,-0.25920133E-03, 0.21077893E-02, 0.21112787E-02,
         0.17292285E-02,-0.22821792E-03, 0.46434197E-04, 0.41689233E-04,
        -0.11733326E-03, 0.93927857E-03, 0.12732064E-03, 0.10361330E-02,
         0.14506641E-03, 0.16060426E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_t_e_dext_700                                =v_t_e_dext_700                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x32    *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22            
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]                *x52
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dext_700                                =v_t_e_dext_700                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_t_e_dext_700                                =v_t_e_dext_700                                
        +coeff[ 26]*x11*x22            
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]*x11    *x31*x41    
        +coeff[ 33]        *x32*x42    
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dext_700                                =v_t_e_dext_700                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]    *x23*x31*x41    
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]    *x23        *x52
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]                *x53
    ;
    v_t_e_dext_700                                =v_t_e_dext_700                                
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dext_700                                ;
}
float y_e_dext_700                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.4425168E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.46311025E-03, 0.10382363E+00,-0.58530875E-01,-0.52276779E-01,
        -0.20761140E-01, 0.38990427E-01, 0.20321609E-01,-0.25946252E-01,
        -0.11798006E-01, 0.19315990E-02,-0.76481933E-02,-0.41873893E-02,
        -0.21029105E-02,-0.17252257E-03,-0.54466049E-03, 0.80772507E-03,
        -0.46098921E-02, 0.13042641E-02,-0.22347006E-02,-0.14379803E-02,
        -0.23285258E-02,-0.45884275E-02,-0.92453999E-03, 0.51543163E-03,
         0.28453195E-02,-0.95023523E-03, 0.14050445E-02, 0.14553216E-02,
         0.14015532E-02, 0.11477598E-02,-0.34975447E-02,-0.46185767E-02,
        -0.27415170E-02, 0.95216581E-03, 0.26150671E-03, 0.63992589E-03,
         0.38528137E-03, 0.20243913E-03,-0.16801694E-02,-0.13133478E-02,
        -0.86993771E-03, 0.10918310E-04,-0.10149465E-03,-0.47967813E-03,
         0.12488713E-03, 0.27521455E-03,-0.15752101E-04, 0.13060639E-04,
         0.42301111E-03,-0.86273561E-04,-0.45026402E-03, 0.36524810E-03,
        -0.14157529E-03, 0.11528958E-03, 0.58311602E-03,-0.22208798E-03,
         0.83275569E-04, 0.23894930E-03, 0.17089826E-03, 0.17897184E-02,
        -0.66251913E-02,-0.70523056E-02, 0.16218765E-02, 0.17380122E-02,
         0.28255747E-02, 0.14519438E-02,-0.76646998E-03, 0.89022273E-03,
        -0.85235591E-03,-0.15634760E-02,-0.57642242E-05,-0.45700970E-04,
        -0.16513823E-04,-0.19468869E-04, 0.64558961E-04,-0.14025845E-03,
         0.24454838E-02, 0.21433488E-02, 0.12936865E-02, 0.65096981E-04,
         0.38335175E-03, 0.24594463E-03,-0.13931562E-02,-0.25223946E-03,
         0.55813489E-04,-0.20219726E-02,-0.33283015E-02,-0.10870053E-02,
        -0.76458824E-03,-0.20740019E-04,-0.15733658E-04,-0.27623153E-04,
        -0.19457890E-04,-0.49513365E-04,-0.18729228E-04, 0.38166012E-03,
         0.30118401E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]*x11        *x41*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x23    *x41*x51
        +coeff[ 46]    *x21            
        +coeff[ 47]                *x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]        *x33*x42    
        +coeff[ 51]*x11*x21    *x43    
        +coeff[ 52]        *x34*x41    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 53]            *x41*x53
        +coeff[ 54]*x11*x21*x31*x42    
        +coeff[ 55]    *x21*x31*x42*x51
        +coeff[ 56]        *x31    *x53
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x22    *x41*x52
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x22*x31*x44    
        +coeff[ 61]    *x22*x32*x43    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 62]    *x23    *x45    
        +coeff[ 63]    *x23*x31*x44    
        +coeff[ 64]    *x23*x32*x43    
        +coeff[ 65]    *x23*x33*x42    
        +coeff[ 66]    *x24    *x43*x51
        +coeff[ 67]    *x23*x34*x41    
        +coeff[ 68]    *x24*x31*x42*x51
        +coeff[ 69]    *x24    *x45    
        +coeff[ 70]    *x22            
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 71]*x12        *x41    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x11*x23    *x41    
        +coeff[ 75]    *x21*x32*x41*x51
        +coeff[ 76]    *x21*x31*x44    
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]    *x21*x33*x42    
        +coeff[ 79]    *x22*x31    *x52
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 80]    *x23*x32*x41    
        +coeff[ 81]    *x23*x33        
        +coeff[ 82]    *x22    *x45    
        +coeff[ 83]    *x22*x32*x41*x51
        +coeff[ 84]*x13        *x43    
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_dext_700                                =v_y_e_dext_700                                
        +coeff[ 89]    *x22    *x42    
        +coeff[ 90]    *x22*x31*x41    
        +coeff[ 91]*x11*x21    *x41*x51
        +coeff[ 92]*x11*x21*x31    *x51
        +coeff[ 93]    *x21    *x43*x51
        +coeff[ 94]*x13        *x41    
        +coeff[ 95]    *x21    *x45    
        +coeff[ 96]*x12        *x43    
        ;

    return v_y_e_dext_700                                ;
}
float p_e_dext_700                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.8738435E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.85403866E-04,-0.21246240E-01, 0.56879818E-02,-0.12339775E-01,
         0.39048877E-02, 0.89390706E-02,-0.73749414E-02,-0.23311230E-02,
         0.44055979E-03, 0.82261293E-04,-0.98974691E-04, 0.72917828E-04,
        -0.41725887E-02,-0.20464184E-02, 0.42496159E-03,-0.20710200E-02,
        -0.10407462E-02, 0.42407311E-03, 0.16686481E-02,-0.46524750E-04,
         0.17036955E-03,-0.24260209E-03,-0.99653564E-03,-0.22692885E-03,
        -0.33736811E-03,-0.98752498E-03, 0.80196257E-03,-0.14510675E-04,
         0.94409147E-03, 0.41452842E-03, 0.69172267E-04,-0.71570474E-04,
         0.54570840E-03, 0.47485245E-03,-0.37381108E-03,-0.11734486E-03,
        -0.27956074E-03, 0.23409845E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_700                                =v_p_e_dext_700                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_700                                =v_p_e_dext_700                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x23    *x41    
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_700                                =v_p_e_dext_700                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x24*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_p_e_dext_700                                =v_p_e_dext_700                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dext_700                                ;
}
float l_e_dext_700                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2239134E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.22086589E-01,-0.48454842E+00,-0.78674778E-01, 0.20266507E-01,
        -0.30870965E-01,-0.36764558E-01, 0.20743045E-02, 0.12561150E-01,
         0.14986936E-01, 0.14684087E-02,-0.42531262E-02,-0.30300028E-02,
         0.11757886E-02,-0.97057503E-03, 0.62840800E-02, 0.79201782E-04,
        -0.17358112E-04,-0.18201780E-02, 0.84771827E-03,-0.15644636E-02,
         0.11981188E-02, 0.96291513E-03, 0.88663021E-03, 0.74996316E-03,
         0.13867841E-01, 0.44561592E-02, 0.10505732E-01, 0.10086981E-01,
         0.57966653E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_700                                =v_l_e_dext_700                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_700                                =v_l_e_dext_700                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dext_700                                =v_l_e_dext_700                                
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x23*x32*x42    
        +coeff[ 28]    *x23    *x44    
        ;

    return v_l_e_dext_700                                ;
}
float x_e_dext_600                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1007858E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.96221454E-02,-0.15662361E-01, 0.10661922E+00, 0.34400764E+00,
         0.36547441E-01, 0.10310551E-01,-0.22105954E-02,-0.40428760E-02,
        -0.12810404E-01,-0.11880051E-01,-0.11987412E-03,-0.15284651E-02,
        -0.21587615E-02,-0.50330544E-02,-0.64928760E-03,-0.90643222E-03,
        -0.10359011E-02, 0.32310947E-03,-0.58803166E-03,-0.44051668E-03,
        -0.72652975E-03,-0.92960158E-02,-0.64049996E-02,-0.42736465E-02,
        -0.45222430E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_600                                =v_x_e_dext_600                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_600                                =v_x_e_dext_600                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_600                                ;
}
float t_e_dext_600                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5867970E+00;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.52885483E-02,-0.10264732E+00, 0.24682038E-01,-0.66388575E-02,
        -0.16125750E-05, 0.35669662E-02, 0.19526703E-02, 0.35429865E-02,
        -0.25809149E-03,-0.18317119E-02,-0.78562251E-03, 0.16254878E-02,
        -0.14142597E-02,-0.44848194E-03,-0.69495576E-03,-0.22045364E-03,
         0.19054575E-03, 0.16687198E-03,-0.40159700E-03,-0.20003824E-03,
         0.13860699E-03, 0.45685202E-03, 0.36980482E-03, 0.32963266E-03,
        -0.28617957E-03, 0.63199044E-03, 0.18503725E-03,-0.37971390E-05,
         0.53710345E-03, 0.18301406E-02, 0.58741512E-03, 0.62960869E-04,
         0.12722527E-03, 0.72869188E-04,-0.12960054E-03, 0.43907558E-03,
         0.11413018E-03,-0.27639448E-03,-0.27824994E-03, 0.20005286E-02,
         0.20325128E-02, 0.17437640E-02, 0.54154734E-04,-0.20360503E-04,
         0.36942020E-04,-0.39046467E-04,-0.79216043E-04, 0.85741788E-03,
         0.12988971E-03, 0.99833077E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_600                                =v_t_e_dext_600                                
        +coeff[  8]        *x32*x42    
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]            *x42*x51
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dext_600                                =v_t_e_dext_600                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22            
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_t_e_dext_600                                =v_t_e_dext_600                                
        +coeff[ 26]*x11*x22            
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]*x12*x21*x31*x41*x51
        +coeff[ 32]    *x21*x33*x41*x51
        +coeff[ 33]*x11    *x31*x41    
        +coeff[ 34]        *x33*x41    
    ;
    v_t_e_dext_600                                =v_t_e_dext_600                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]        *x31*x41*x52
        +coeff[ 38]            *x42*x52
        +coeff[ 39]    *x23*x31*x41    
        +coeff[ 40]    *x21*x32*x42    
        +coeff[ 41]    *x21*x31*x43    
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_dext_600                                =v_t_e_dext_600                                
        +coeff[ 44]                *x53
        +coeff[ 45]*x11*x21    *x42    
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]*x11*x22*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dext_600                                ;
}
float y_e_dext_600                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2016799E-03;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.20914324E-03, 0.10282299E+00,-0.59089087E-01,-0.52213449E-01,
        -0.20749792E-01, 0.39032489E-01, 0.20355139E-01,-0.25965087E-01,
        -0.11961625E-01, 0.19327005E-02,-0.78396574E-02,-0.42430791E-02,
        -0.21205579E-02,-0.88955392E-04,-0.40690572E-03, 0.80242060E-03,
        -0.46665887E-02, 0.12901736E-02,-0.22327877E-02,-0.14416655E-02,
        -0.24404994E-02,-0.46356106E-02,-0.95502118E-03, 0.51742187E-03,
         0.27564312E-02,-0.94549649E-03, 0.16219491E-02, 0.18951513E-02,
         0.12773917E-02, 0.11646911E-02,-0.34494605E-02,-0.40060976E-02,
        -0.25740147E-02, 0.89848111E-03, 0.25854519E-03, 0.55674015E-03,
         0.39902775E-03, 0.20497107E-03,-0.16487517E-02,-0.11333100E-02,
        -0.61806547E-03, 0.11449760E-03,-0.10439320E-03,-0.18827396E-03,
         0.33563867E-03, 0.13865875E-04,-0.36334270E-04, 0.31910173E-03,
        -0.87565844E-04,-0.27096987E-03, 0.39414992E-03, 0.11989626E-03,
         0.55391260E-03, 0.25340190E-03, 0.17369333E-03, 0.12404461E-02,
        -0.72851819E-02,-0.72457865E-02, 0.18860785E-02, 0.28777402E-02,
         0.27988893E-02, 0.15842004E-03, 0.19164227E-02, 0.75367343E-03,
        -0.17068716E-02, 0.68890390E-05,-0.17486862E-04,-0.23024055E-04,
         0.12605627E-03,-0.97302509E-04, 0.31757543E-04, 0.72235103E-04,
         0.65977234E-04, 0.15413411E-02, 0.18902399E-02, 0.18686664E-03,
         0.10322091E-02, 0.82221733E-04,-0.48510728E-05, 0.13144345E-03,
         0.79718773E-03,-0.41552656E-03, 0.25044082E-03,-0.14384816E-02,
         0.18330718E-04,-0.23048953E-02,-0.37567355E-02,-0.12869280E-02,
        -0.86664519E-03,-0.30680443E-03,-0.46306470E-03,-0.31036329E-05,
         0.42134170E-05,-0.19915255E-04, 0.87306817E-05, 0.19897765E-04,
         0.34728208E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]            *x45*x51
        +coeff[ 42]*x11        *x41*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]        *x33    *x53
        +coeff[ 46]*x12        *x41    
        +coeff[ 47]            *x43*x51
        +coeff[ 48]*x11*x22    *x41    
        +coeff[ 49]        *x33*x42    
        +coeff[ 50]*x11*x21    *x43    
        +coeff[ 51]            *x41*x53
        +coeff[ 52]*x11*x21*x31*x42    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 53]*x11*x21*x32*x41    
        +coeff[ 54]    *x22    *x41*x52
        +coeff[ 55]    *x23*x31*x42    
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]    *x22*x32*x43    
        +coeff[ 58]    *x23    *x45    
        +coeff[ 59]    *x23*x31*x44    
        +coeff[ 60]    *x23*x32*x43    
        +coeff[ 61]    *x21*x34*x43    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 62]    *x23*x33*x42    
        +coeff[ 63]    *x23*x34*x41    
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]    *x21            
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11    *x31    *x51
        +coeff[ 68]        *x33    *x51
        +coeff[ 69]        *x34*x41    
        +coeff[ 70]*x12        *x41*x51
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 71]        *x31    *x53
        +coeff[ 72]*x11*x23    *x41    
        +coeff[ 73]    *x21*x31*x44    
        +coeff[ 74]    *x21*x32*x43    
        +coeff[ 75]        *x31*x44*x51
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22*x31    *x52
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]        *x32*x43*x51
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 80]    *x23*x32*x41    
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]    *x24    *x41*x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_dext_600                                =v_y_e_dext_600                                
        +coeff[ 89]    *x24*x33        
        +coeff[ 90]    *x24    *x43*x51
        +coeff[ 91]                *x51
        +coeff[ 92]*x11*x21            
        +coeff[ 93]    *x21    *x42    
        +coeff[ 94]    *x22    *x42    
        +coeff[ 95]*x11    *x31*x42    
        +coeff[ 96]    *x21    *x44    
        ;

    return v_y_e_dext_600                                ;
}
float p_e_dext_600                                (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.6537361E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.65293614E-04,-0.21364372E-01, 0.54889186E-02,-0.12303323E-01,
         0.39109727E-02, 0.89413282E-02,-0.73563964E-02,-0.23309083E-02,
         0.42411173E-03, 0.70549126E-04,-0.10418726E-03, 0.81455451E-04,
        -0.41525597E-02,-0.20333380E-02, 0.42279292E-03,-0.20612918E-02,
        -0.10347915E-02, 0.16553873E-02,-0.29918585E-04, 0.16851188E-03,
        -0.24245554E-03,-0.10075242E-02,-0.22118520E-03, 0.42560676E-03,
        -0.33302716E-03,-0.10087809E-02, 0.82721392E-03, 0.13328194E-04,
         0.90024114E-03, 0.41766113E-03, 0.67435023E-04,-0.73684263E-04,
         0.50853053E-03, 0.46041471E-03,-0.40294862E-03,-0.11776988E-03,
        -0.30158740E-03, 0.21638561E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_600                                =v_p_e_dext_600                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_600                                =v_p_e_dext_600                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_600                                =v_p_e_dext_600                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x24*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x24    *x41    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_p_e_dext_600                                =v_p_e_dext_600                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dext_600                                ;
}
float l_e_dext_600                                (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.2384722E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.23837985E-01,-0.48476222E+00,-0.78654133E-01, 0.20247245E-01,
        -0.32249711E-01,-0.36616988E-01, 0.21971348E-02, 0.16483137E-01,
         0.14166589E-01, 0.27921423E-02,-0.44248407E-02,-0.33821752E-02,
         0.11400704E-02,-0.97073667E-03, 0.58793863E-02, 0.45804522E-03,
         0.26168770E-03,-0.21428552E-02,-0.67041852E-04,-0.17544307E-02,
         0.11004705E-02, 0.98591601E-03, 0.90662157E-03, 0.70401171E-03,
         0.13728720E-02, 0.10666722E-01, 0.85960291E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_600                                =v_l_e_dext_600                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_600                                =v_l_e_dext_600                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_600                                =v_l_e_dext_600                                
        +coeff[ 26]    *x23    *x42    
        ;

    return v_l_e_dext_600                                ;
}
float x_e_dext_500                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.7315662E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.75706444E-02,-0.15663225E-01, 0.10661805E+00, 0.34448293E+00,
         0.36546458E-01, 0.10359288E-01,-0.22103421E-02,-0.40454213E-02,
        -0.12475589E-01,-0.11837993E-01, 0.33679269E-04,-0.16442712E-02,
        -0.21262676E-02,-0.50071697E-02,-0.65604219E-03,-0.90690318E-03,
        -0.94946881E-03, 0.33238842E-03,-0.63006324E-03,-0.42897940E-03,
        -0.69993414E-03,-0.99828234E-02,-0.64398041E-02,-0.47677248E-02,
        -0.59219291E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_500                                =v_x_e_dext_500                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_500                                =v_x_e_dext_500                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_500                                ;
}
float t_e_dext_500                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5875567E+00;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.43821395E-02,-0.10280354E+00, 0.24695853E-01,-0.66243093E-02,
        -0.32806631E-05, 0.35641678E-02, 0.18613593E-02, 0.35045368E-02,
        -0.27198889E-03, 0.17991689E-02,-0.72268228E-03,-0.17998274E-02,
        -0.78253716E-03, 0.16132406E-02,-0.14030611E-02,-0.42657042E-03,
        -0.22481875E-03, 0.15604714E-03, 0.15960074E-03,-0.15924188E-03,
         0.43305961E-03, 0.39959382E-03, 0.42645252E-03,-0.42388352E-03,
        -0.27821911E-03, 0.50079741E-03, 0.59786299E-03, 0.17517268E-03,
         0.10376216E-03, 0.66636864E-03,-0.36270365E-04, 0.49535139E-03,
         0.18375585E-02, 0.72447372E-04, 0.98423749E-04, 0.11155429E-03,
        -0.27138810E-03, 0.20396155E-02, 0.21903566E-02,-0.18343926E-03,
         0.61269173E-04,-0.27453920E-04, 0.38380855E-04,-0.14386605E-03,
        -0.91921778E-04, 0.88267430E-03, 0.22172769E-03, 0.95257268E-03,
         0.97029377E-04, 0.11022820E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_500                                =v_t_e_dext_500                                
        +coeff[  8]            *x42*x52
        +coeff[  9]    *x21*x31*x43    
        +coeff[ 10]        *x32        
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22            
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dext_500                                =v_t_e_dext_500                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]*x11*x21            
        +coeff[ 19]    *x23            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_t_e_dext_500                                =v_t_e_dext_500                                
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]*x11*x22            
        +coeff[ 28]    *x22        *x51
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]    *x21*x31*x41*x51
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]*x11    *x31*x41    
        +coeff[ 34]        *x31*x43    
    ;
    v_t_e_dext_500                                =v_t_e_dext_500                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]    *x23*x31*x41    
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]                *x53
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_dext_500                                =v_t_e_dext_500                                
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]*x11*x22    *x42    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_dext_500                                ;
}
float y_e_dext_500                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2664989E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.29913080E-03, 0.10155453E+00,-0.59883837E-01,-0.51976226E-01,
        -0.20667164E-01, 0.39099187E-01, 0.20378649E-01,-0.26362170E-01,
        -0.11978767E-01, 0.19189904E-02,-0.78778127E-02,-0.42240587E-02,
        -0.21182219E-02,-0.40959461E-04,-0.29383323E-03, 0.80060685E-03,
        -0.47744694E-02, 0.13391717E-02,-0.22519550E-02,-0.14479310E-02,
        -0.24299745E-02,-0.47362368E-02,-0.96054748E-03, 0.56035467E-03,
         0.24507309E-02,-0.94071979E-03, 0.10252566E-02, 0.11041651E-02,
        -0.36099814E-02, 0.11743550E-02, 0.34496819E-02, 0.54212194E-03,
        -0.27196028E-02,-0.80206115E-02,-0.11614135E-02, 0.64593012E-03,
         0.98934211E-03, 0.22892105E-03, 0.58497774E-03, 0.40344457E-03,
        -0.12262667E-02,-0.20516587E-02,-0.11100232E-02,-0.62060880E-03,
        -0.75028036E-02,-0.41263797E-02,-0.11041382E-03, 0.48332199E-03,
        -0.10198681E-03, 0.43503690E-03, 0.13181189E-03,-0.19173402E-02,
         0.20960149E-03, 0.28781776E-03, 0.12588111E-04,-0.10607680E-04,
        -0.47026784E-04,-0.24099736E-04,-0.71921982E-04, 0.34248462E-03,
         0.12002237E-03, 0.47313687E-03, 0.86708373E-04, 0.17189582E-03,
         0.18799465E-03, 0.26778704E-02, 0.14273213E-04, 0.50251553E-03,
         0.85747481E-03, 0.59536815E-03, 0.20964522E-03, 0.93774133E-05,
        -0.19230552E-04,-0.21516954E-03,-0.17764576E-03,-0.14494335E-03,
         0.68673990E-05,-0.10611690E-03, 0.29714860E-02,-0.84477324E-04,
         0.17238229E-02, 0.18868699E-02, 0.66547036E-04,-0.17017221E-03,
         0.14716805E-02,-0.40679338E-03, 0.28981388E-03,-0.19474677E-02,
        -0.27763175E-02,-0.15908579E-02,-0.29606311E-03,-0.26594804E-03,
         0.73190413E-05, 0.32023952E-04, 0.27744698E-04, 0.14457392E-04,
         0.12244748E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 44]    *x22*x31*x44    
        +coeff[ 45]    *x22*x33*x42    
        +coeff[ 46]    *x21*x32*x45    
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]    *x22    *x43    
        +coeff[ 52]    *x21*x31    *x52
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 53]    *x23    *x41*x51
        +coeff[ 54]    *x21            
        +coeff[ 55]                *x51
        +coeff[ 56]*x12        *x41    
        +coeff[ 57]*x11    *x31    *x51
        +coeff[ 58]*x11*x22    *x41    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x22    *x41*x52
        +coeff[ 65]    *x23*x31*x42    
        +coeff[ 66]    *x23    *x45    
        +coeff[ 67]    *x23*x31*x44    
        +coeff[ 68]    *x23*x32*x43    
        +coeff[ 69]    *x23*x33*x42    
        +coeff[ 70]    *x23*x34*x41    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 71]    *x22            
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]        *x31*x44    
        +coeff[ 74]        *x33*x42    
        +coeff[ 75]    *x21*x31*x42*x51
        +coeff[ 76]*x11*x23    *x41    
        +coeff[ 77]    *x21*x32*x41*x51
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]*x11*x23*x31        
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 80]    *x23    *x43    
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22*x31    *x52
        +coeff[ 83]    *x22    *x43*x51
        +coeff[ 84]    *x23*x32*x41    
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]    *x24    *x43    
        +coeff[ 88]    *x24*x31*x42    
    ;
    v_y_e_dext_500                                =v_y_e_dext_500                                
        +coeff[ 89]    *x24*x32*x41    
        +coeff[ 90]    *x24*x33        
        +coeff[ 91]    *x24    *x43*x51
        +coeff[ 92]            *x42    
        +coeff[ 93]*x11    *x31*x42    
        +coeff[ 94]*x11    *x32*x41    
        +coeff[ 95]*x12*x21    *x41    
        +coeff[ 96]*x12    *x31*x41    
        ;

    return v_y_e_dext_500                                ;
}
float p_e_dext_500                                (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.4737623E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.48902119E-04,-0.21508530E-01, 0.52056559E-02,-0.12289883E-01,
         0.39052670E-02, 0.89398846E-02,-0.73253578E-02,-0.23217807E-02,
         0.51343575E-03, 0.71195915E-04,-0.17776733E-05,-0.88474444E-04,
        -0.41579455E-02,-0.20723429E-02, 0.41847330E-03,-0.20792373E-02,
        -0.10436998E-02, 0.16577333E-02,-0.15514884E-03,-0.25395351E-04,
         0.16791938E-03,-0.25339474E-03,-0.97869174E-03,-0.21792950E-03,
         0.42067873E-03,-0.33209479E-03,-0.99719083E-03, 0.82156202E-03,
        -0.38586054E-05, 0.92664105E-03, 0.61933802E-04,-0.71647810E-04,
         0.53604803E-03, 0.46917959E-03,-0.38060729E-03,-0.11375552E-03,
         0.40787726E-03,-0.27984387E-03, 0.23208412E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_500                                =v_p_e_dext_500                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_500                                =v_p_e_dext_500                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]            *x41*x52
    ;
    v_p_e_dext_500                                =v_p_e_dext_500                                
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24*x31    *x51
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_p_e_dext_500                                =v_p_e_dext_500                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x22    *x41*x52
        ;

    return v_p_e_dext_500                                ;
}
float l_e_dext_500                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2054395E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.20443160E-01,-0.48554716E+00,-0.78728922E-01, 0.20202912E-01,
        -0.32259621E-01,-0.36631979E-01, 0.21789614E-02, 0.14141202E-01,
         0.13567829E-01, 0.29475722E-02,-0.43785046E-02,-0.30832069E-02,
         0.11571404E-02,-0.94934972E-03, 0.58393315E-02,-0.19563253E-02,
         0.42552830E-03, 0.29773719E-03,-0.21616777E-02,-0.16417092E-03,
         0.12467114E-02, 0.12975180E-02, 0.12581911E-02, 0.87529933E-03,
         0.14808050E-02, 0.12295721E-01, 0.10571963E-01, 0.87681748E-02,
         0.61464548E-03, 0.70778253E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_500                                =v_l_e_dext_500                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_dext_500                                =v_l_e_dext_500                                
        +coeff[ 17]        *x34        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_500                                =v_l_e_dext_500                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x34    *x51
        +coeff[ 29]    *x21*x32*x44    
        ;

    return v_l_e_dext_500                                ;
}
float x_e_dext_450                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.8066266E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.88608172E-02,-0.15660150E-01, 0.10657166E+00, 0.34490460E+00,
         0.36575802E-01, 0.10402809E-01,-0.22139240E-02,-0.40177284E-02,
        -0.12789301E-01,-0.11888489E-01,-0.47705413E-04,-0.17955493E-02,
        -0.65141363E-03,-0.20998304E-02,-0.49386485E-02, 0.32381029E-03,
        -0.89776248E-03,-0.93505206E-03,-0.58337505E-03,-0.41229112E-03,
        -0.71279355E-03,-0.95723663E-02,-0.62975441E-02,-0.42033414E-02,
        -0.51929154E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_450                                =v_x_e_dext_450                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]        *x31*x41    
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]        *x32        
    ;
    v_x_e_dext_450                                =v_x_e_dext_450                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_450                                ;
}
float t_e_dext_450                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5868241E+00;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.49602147E-02, 0.35603850E-02,-0.10292088E+00, 0.24672799E-01,
        -0.66390540E-02, 0.19518296E-02, 0.35184079E-02,-0.21899136E-03,
        -0.17566783E-02,-0.78126771E-03, 0.16443027E-02,-0.13848966E-02,
        -0.47974312E-03,-0.72167855E-03,-0.21999172E-03, 0.14665973E-03,
         0.14692612E-03,-0.42623878E-03,-0.17172282E-03, 0.46429076E-03,
         0.40056760E-03, 0.41563256E-03,-0.35136426E-03, 0.51248475E-03,
         0.55469584E-03, 0.19593073E-03, 0.15430557E-03, 0.61564322E-03,
         0.50280633E-03, 0.17618357E-02, 0.54601522E-04,-0.12895481E-03,
         0.13637345E-03,-0.27189730E-03,-0.28494839E-03, 0.19568440E-02,
         0.20848801E-02, 0.17032649E-02, 0.50757895E-04,-0.24316074E-04,
         0.45052271E-04,-0.10308909E-03, 0.92439621E-03, 0.19328920E-03,
         0.96667500E-03,-0.16936978E-03, 0.59910549E-05,-0.81563712E-05,
         0.11783866E-04, 0.41260282E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_450                                =v_t_e_dext_450                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_450                                =v_t_e_dext_450                                
        +coeff[ 17]    *x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_450                                =v_t_e_dext_450                                
        +coeff[ 26]    *x22        *x51
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]        *x33*x41    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_dext_450                                =v_t_e_dext_450                                
        +coeff[ 35]    *x23*x31*x41    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11*x21        *x51
        +coeff[ 40]                *x53
        +coeff[ 41]        *x32    *x52
        +coeff[ 42]    *x23*x32        
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_dext_450                                =v_t_e_dext_450                                
        +coeff[ 44]    *x21*x33*x41    
        +coeff[ 45]    *x23        *x52
        +coeff[ 46]            *x41    
        +coeff[ 47]    *x21    *x41    
        +coeff[ 48]*x11            *x52
        +coeff[ 49]*x11*x21*x32        
        ;

    return v_t_e_dext_450                                ;
}
float y_e_dext_450                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1900040E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.28764518E-03, 0.10057402E+00,-0.60426883E-01,-0.52146569E-01,
        -0.20777887E-01, 0.39118558E-01, 0.20400140E-01,-0.26348306E-01,
        -0.12043514E-01, 0.19329741E-02,-0.78916475E-02,-0.42232936E-02,
        -0.21223726E-02,-0.55036984E-04,-0.44966137E-03, 0.80327044E-03,
        -0.46448880E-02,-0.96569792E-03, 0.12859072E-02,-0.22329197E-02,
        -0.14525319E-02,-0.22932785E-02,-0.46772803E-02, 0.55519748E-03,
         0.27558394E-02,-0.95104531E-03, 0.29405081E-02, 0.13084322E-02,
        -0.34369100E-02, 0.18758461E-03, 0.70244237E-03,-0.15644344E-04,
        -0.28325145E-02,-0.77525442E-02,-0.99022093E-03, 0.42625823E-04,
        -0.34165427E-04, 0.15646339E-02, 0.97478204E-03, 0.40518772E-03,
         0.58734068E-03,-0.19705098E-02, 0.41568157E-03,-0.12400128E-02,
        -0.22228546E-02,-0.10531499E-02,-0.57326455E-03,-0.28521099E-03,
         0.14653173E-02,-0.10211403E-03, 0.43566540E-03,-0.21681005E-03,
         0.13360474E-03,-0.19683449E-03, 0.21113132E-03, 0.28034215E-03,
        -0.76255086E-02, 0.29310906E-04,-0.47899419E-04,-0.26360354E-04,
        -0.10117385E-03, 0.41918748E-03, 0.10079923E-03, 0.57336903E-03,
         0.80338679E-04, 0.25436530E-03, 0.15798138E-03,-0.36142609E-03,
         0.72383031E-04,-0.41071805E-02, 0.17955235E-02, 0.52974345E-02,
         0.55458192E-02, 0.35601035E-02,-0.24900972E-04,-0.97115451E-04,
        -0.14967404E-03, 0.54198677E-04,-0.11529296E-03,-0.75900323E-04,
         0.35860150E-04, 0.82837032E-04,-0.37951322E-03,-0.37436729E-03,
        -0.16438054E-03,-0.18808328E-02,-0.28994186E-02,-0.16474299E-02,
        -0.34570153E-03, 0.92034240E-03,-0.22597426E-05, 0.27907814E-04,
         0.16693000E-04, 0.56724780E-05, 0.54725410E-05,-0.20196572E-04,
        -0.18042578E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 35]    *x21            
        +coeff[ 36]                *x51
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]    *x21    *x41*x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x24*x31        
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]    *x21*x32*x45    
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]    *x21*x31    *x52
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11    *x31    *x51
        +coeff[ 60]*x11*x22    *x41    
        +coeff[ 61]*x11*x21    *x43    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 62]            *x41*x53
        +coeff[ 63]*x11*x21*x31*x42    
        +coeff[ 64]        *x31    *x53
        +coeff[ 65]*x11*x21*x32*x41    
        +coeff[ 66]    *x22    *x41*x52
        +coeff[ 67]    *x23*x31*x42    
        +coeff[ 68]    *x23*x32*x41    
        +coeff[ 69]    *x22*x33*x42    
        +coeff[ 70]    *x23    *x45    
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 71]    *x23*x31*x44    
        +coeff[ 72]    *x23*x32*x43    
        +coeff[ 73]    *x23*x33*x42    
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]        *x34*x41    
        +coeff[ 76]    *x21*x31*x42*x51
        +coeff[ 77]*x11*x23    *x41    
        +coeff[ 78]    *x21*x32*x41*x51
        +coeff[ 79]*x11*x23*x31        
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 80]    *x21*x33*x42    
        +coeff[ 81]    *x22*x31    *x52
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x24    *x41*x51
        +coeff[ 85]    *x24    *x43    
        +coeff[ 86]    *x24*x31*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x24*x33        
    ;
    v_y_e_dext_450                                =v_y_e_dext_450                                
        +coeff[ 89]    *x23*x34*x41    
        +coeff[ 90]*x11                
        +coeff[ 91]            *x42    
        +coeff[ 92]        *x31*x41    
        +coeff[ 93]        *x32        
        +coeff[ 94]    *x21        *x51
        +coeff[ 95]            *x44    
        +coeff[ 96]*x11        *x43    
        ;

    return v_y_e_dext_450                                ;
}
float p_e_dext_450                                (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.7457507E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.77414181E-04,-0.21621160E-01, 0.50141118E-02,-0.12258797E-01,
         0.39022441E-02, 0.89407526E-02,-0.73909159E-02,-0.23204605E-02,
         0.49699179E-03, 0.79015394E-04,-0.71073664E-05,-0.41488502E-02,
        -0.20975582E-02, 0.42143362E-03,-0.20613838E-02,-0.10352748E-02,
         0.16397818E-02,-0.35961293E-04, 0.17019363E-03,-0.24625126E-03,
        -0.99941075E-03,-0.21295159E-03, 0.41965520E-03,-0.31719904E-03,
        -0.10100120E-02, 0.82553847E-03, 0.91316237E-03,-0.38072039E-03,
         0.47267115E-03, 0.62740612E-04,-0.69857713E-04, 0.50318270E-03,
         0.45270348E-03,-0.12027825E-03,-0.28069949E-03, 0.20620400E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_450                                =v_p_e_dext_450                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dext_450                                =v_p_e_dext_450                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23    *x41*x51
    ;
    v_p_e_dext_450                                =v_p_e_dext_450                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x24    *x41    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dext_450                                =v_p_e_dext_450                                
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dext_450                                ;
}
float l_e_dext_450                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.2273760E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.22478167E-01,-0.48608491E+00,-0.78639835E-01, 0.20153917E-01,
        -0.32123681E-01,-0.36776692E-01, 0.21292102E-02, 0.14605788E-01,
         0.13601265E-01, 0.34968944E-02,-0.46359664E-02,-0.32430198E-02,
         0.11838131E-02,-0.90121012E-03, 0.56897048E-02,-0.36967269E-03,
        -0.16577000E-02,-0.17752853E-03,-0.15934692E-02, 0.10460200E-02,
         0.11018894E-02, 0.11054135E-02, 0.98650227E-03, 0.14200157E-02,
         0.12525556E-01, 0.10514352E-01, 0.75992071E-02, 0.57253866E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_450                                =v_l_e_dext_450                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x32        
    ;
    v_l_e_dext_450                                =v_l_e_dext_450                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dext_450                                =v_l_e_dext_450                                
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x21*x32*x44    
        ;

    return v_l_e_dext_450                                ;
}
float x_e_dext_449                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.9211699E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.83024641E-02,-0.15438741E-01, 0.10664700E+00, 0.34838670E+00,
         0.36416750E-01, 0.10521887E-01,-0.22180290E-02,-0.41577290E-02,
        -0.14204917E-01,-0.11742751E-01, 0.62185951E-04,-0.21951490E-02,
        -0.24276138E-02,-0.59978445E-02,-0.61613863E-03,-0.11050501E-02,
        -0.89106546E-03, 0.29629152E-03,-0.56679029E-03,-0.46764812E-03,
        -0.73133531E-03,-0.10040306E-01,-0.63256887E-02,-0.44369865E-02,
        -0.51333955E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_449                                =v_x_e_dext_449                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_449                                =v_x_e_dext_449                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_449                                ;
}
float t_e_dext_449                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5870053E+00;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.49026143E-02, 0.35074661E-02,-0.10384724E+00, 0.24667317E-01,
        -0.65542520E-02, 0.21674545E-02, 0.36060917E-02,-0.24930126E-03,
        -0.19770223E-02,-0.78413181E-03, 0.18390125E-02,-0.14769938E-02,
        -0.55012695E-03,-0.81904867E-03,-0.21404464E-03, 0.24048272E-03,
         0.54518430E-03, 0.15605455E-03,-0.42526619E-03,-0.24037593E-03,
         0.14209485E-03, 0.35674544E-03, 0.31962065E-03,-0.26225913E-03,
         0.72095409E-03, 0.19194960E-03,-0.15917238E-04, 0.52368653E-03,
         0.18116132E-02, 0.66820736E-03,-0.32365122E-04, 0.13076037E-03,
         0.77164979E-04,-0.13613855E-03, 0.54852862E-03, 0.10872727E-03,
        -0.29661012E-03,-0.25702600E-03, 0.71935756E-05, 0.10755023E-02,
         0.22032682E-02, 0.24767476E-02, 0.19067810E-02, 0.63128959E-04,
        -0.28070715E-04, 0.43138894E-04, 0.75529926E-04,-0.10260409E-03,
         0.15248806E-03, 0.13048121E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_449                                =v_t_e_dext_449                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]        *x32    *x51
    ;
    v_t_e_dext_449                                =v_t_e_dext_449                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22            
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_449                                =v_t_e_dext_449                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]*x11    *x31*x41    
        +coeff[ 33]        *x33*x41    
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dext_449                                =v_t_e_dext_449                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]    *x23*x32        
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_dext_449                                =v_t_e_dext_449                                
        +coeff[ 44]*x11*x21        *x51
        +coeff[ 45]                *x53
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]        *x32    *x52
        +coeff[ 48]*x11*x22*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dext_449                                ;
}
float y_e_dext_449                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9279696E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.92253921E-03, 0.10681281E+00,-0.63694261E-01,-0.52896783E-01,
        -0.23344403E-01, 0.38677745E-01, 0.22295186E-01,-0.26900342E-01,
        -0.13528735E-01, 0.19052031E-02,-0.85148746E-02,-0.41410634E-02,
        -0.23190659E-02,-0.65778833E-04,-0.50810329E-03, 0.88362891E-03,
        -0.56611029E-02,-0.12937430E-02, 0.13347585E-02,-0.21959501E-02,
        -0.15779798E-02,-0.23891518E-02,-0.46457322E-02, 0.61600521E-03,
         0.24920234E-02,-0.10277428E-02, 0.12324488E-02, 0.12281393E-02,
        -0.43041864E-02, 0.11150759E-02, 0.42854659E-02, 0.21353532E-02,
        -0.27960523E-02,-0.89324759E-02,-0.21917606E-02,-0.45149671E-03,
         0.75481704E-03, 0.10440798E-02, 0.30401227E-03, 0.71586325E-03,
        -0.18398974E-02, 0.40706230E-03, 0.23676094E-03,-0.28840490E-02,
        -0.21241268E-02,-0.32989888E-02, 0.49834879E-03,-0.10030963E-03,
         0.47325110E-03,-0.34206922E-03, 0.17912667E-03,-0.37156171E-03,
        -0.11623229E-02,-0.11947555E-02,-0.83720265E-03, 0.26679281E-03,
        -0.77431113E-02,-0.41618161E-04,-0.31863859E-04,-0.83982610E-04,
         0.37613086E-03, 0.98801036E-04, 0.58547471E-03, 0.85842221E-04,
         0.24563776E-03, 0.17584318E-03, 0.30045952E-02,-0.49227523E-02,
        -0.11705930E-02, 0.18426719E-03,-0.11556252E-03, 0.25935407E-03,
        -0.25191050E-04, 0.30407405E-02,-0.85353480E-04, 0.15725746E-02,
         0.26350750E-02, 0.77399833E-04,-0.15134642E-03, 0.78434864E-03,
        -0.37125350E-03, 0.46737606E-03, 0.81645492E-04,-0.25736287E-03,
         0.24755025E-05,-0.86825321E-05, 0.71312761E-05,-0.25305506E-04,
        -0.27861895E-04,-0.21600288E-04, 0.39750732E-04,-0.14064978E-03,
        -0.18759164E-03,-0.46483397E-04,-0.27377093E-04,-0.21360975E-03,
        -0.83989973E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x23*x32*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x24*x32*x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 44]    *x24    *x43    
        +coeff[ 45]    *x24*x31*x42    
        +coeff[ 46]    *x21    *x43    
        +coeff[ 47]*x11        *x41*x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]        *x31*x44    
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 53]    *x24*x31        
        +coeff[ 54]    *x22*x33        
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]*x11    *x31    *x51
        +coeff[ 59]*x11*x22    *x41    
        +coeff[ 60]*x11*x21    *x43    
        +coeff[ 61]            *x41*x53
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]        *x31    *x53
        +coeff[ 64]*x11*x21*x32*x41    
        +coeff[ 65]    *x22    *x41*x52
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]    *x22*x33*x42    
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]    *x23    *x45    
        +coeff[ 70]    *x21*x32*x45    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 71]    *x23*x31*x44    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]    *x21*x31*x44    
        +coeff[ 74]*x11*x23*x31        
        +coeff[ 75]    *x23    *x43    
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22*x31    *x52
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]    *x21*x34*x41    
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 80]    *x22*x31*x42*x51
        +coeff[ 81]    *x23*x33        
        +coeff[ 82]    *x21*x34*x41*x51
        +coeff[ 83]    *x24    *x43*x51
        +coeff[ 84]                *x51
        +coeff[ 85]    *x21    *x42    
        +coeff[ 86]            *x44    
        +coeff[ 87]*x11*x21*x31*x41    
        +coeff[ 88]*x11*x21    *x41*x51
    ;
    v_y_e_dext_449                                =v_y_e_dext_449                                
        +coeff[ 89]*x11*x21*x31    *x51
        +coeff[ 90]*x11    *x31*x43    
        +coeff[ 91]        *x34*x41    
        +coeff[ 92]    *x21*x31*x42*x51
        +coeff[ 93]            *x43*x52
        +coeff[ 94]*x11    *x33*x41    
        +coeff[ 95]    *x21*x32*x41*x51
        +coeff[ 96]            *x45*x51
        ;

    return v_y_e_dext_449                                ;
}
float p_e_dext_449                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1164530E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.11435539E-03,-0.23278607E-01, 0.63578733E-02,-0.12547063E-01,
         0.42947652E-02, 0.88944314E-02,-0.75995335E-02,-0.23671258E-02,
         0.48057426E-03, 0.10164898E-03,-0.12623251E-03, 0.11284094E-03,
        -0.46957457E-02,-0.22945944E-02, 0.41620253E-03,-0.22665784E-02,
        -0.10274890E-02, 0.17269425E-02,-0.23015940E-04, 0.18483739E-03,
        -0.32697391E-03,-0.12310849E-02,-0.24894704E-03, 0.42838868E-03,
        -0.33959118E-03,-0.99646626E-03, 0.85267308E-03, 0.10020637E-02,
        -0.43340825E-03, 0.73591866E-04,-0.77610734E-04, 0.63962879E-03,
         0.44750667E-03,-0.12135809E-03, 0.45523659E-03,-0.27099121E-03,
         0.24995644E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_449                                =v_p_e_dext_449                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_449                                =v_p_e_dext_449                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_449                                =v_p_e_dext_449                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_dext_449                                =v_p_e_dext_449                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dext_449                                ;
}
float l_e_dext_449                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2286931E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.21939103E-01,-0.49067107E+00,-0.78764580E-01, 0.19934930E-01,
        -0.31823426E-01,-0.36412075E-01, 0.21093944E-02, 0.16014881E-01,
         0.96684983E-02, 0.46222568E-02,-0.48621092E-02,-0.31340690E-02,
         0.12447573E-02,-0.87119144E-03, 0.54804701E-02, 0.52360358E-03,
         0.18208665E-03,-0.23840144E-02,-0.16233046E-02, 0.10313005E-02,
         0.93105724E-02, 0.12633341E-02, 0.11132151E-02, 0.86891529E-03,
         0.12161271E-02, 0.89977346E-02, 0.16684266E-01, 0.13310712E-01,
         0.50336174E-02, 0.61401102E-03, 0.97519904E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_449                                =v_l_e_dext_449                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_449                                =v_l_e_dext_449                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_449                                =v_l_e_dext_449                                
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x21    *x44    
        +coeff[ 29]        *x34    *x51
        +coeff[ 30]    *x23*x33*x41    
        ;

    return v_l_e_dext_449                                ;
}
float x_e_dext_400                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1250409E-01;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59976E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.12141392E-01,-0.15396477E-01, 0.10659228E+00, 0.34991729E+00,
         0.36432210E-01, 0.10640483E-01,-0.22315742E-02,-0.40929532E-02,
        -0.14420929E-01,-0.11728188E-01, 0.39140257E-04,-0.22459968E-02,
        -0.65984501E-03,-0.23601272E-02,-0.60255295E-02,-0.11176298E-02,
        -0.89582027E-03, 0.30662699E-03,-0.58691151E-03,-0.48911158E-03,
        -0.78019209E-03,-0.99332342E-02,-0.62267552E-02,-0.40600128E-02,
        -0.50680051E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_400                                =v_x_e_dext_400                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]        *x31*x41    
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_400                                =v_x_e_dext_400                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_400                                ;
}
float t_e_dext_400                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5855909E+00;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59976E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64832293E-02, 0.34980786E-02,-0.10430079E+00, 0.24687519E-01,
        -0.65459148E-02, 0.21770846E-02, 0.35857640E-02,-0.25417638E-03,
        -0.19649717E-02,-0.77812473E-03, 0.18170252E-02,-0.14692743E-02,
        -0.51209994E-03,-0.83312043E-03,-0.21509055E-03, 0.26989225E-03,
         0.49556472E-03, 0.16435616E-03,-0.45700886E-03, 0.87505723E-04,
         0.33533361E-03, 0.44072489E-03,-0.26982842E-03, 0.69271092E-03,
         0.18127767E-02, 0.19756211E-03,-0.37039681E-04, 0.51352259E-03,
         0.21727867E-02, 0.71103050E-03, 0.11595614E-03, 0.64167500E-04,
        -0.13028120E-03, 0.50751213E-03, 0.12902918E-03,-0.27535140E-03,
        -0.27447267E-03, 0.10252470E-02, 0.25117677E-02, 0.19357506E-02,
        -0.21911135E-03,-0.17609884E-03, 0.61932442E-04,-0.25373620E-04,
         0.40233655E-04,-0.11255196E-03, 0.17758249E-03, 0.13419845E-02,
         0.21341752E-03, 0.17241681E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_400                                =v_t_e_dext_400                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]        *x32    *x51
    ;
    v_t_e_dext_400                                =v_t_e_dext_400                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_400                                =v_t_e_dext_400                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]*x11    *x31*x41    
        +coeff[ 32]        *x33*x41    
        +coeff[ 33]    *x21*x31*x41*x51
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_dext_400                                =v_t_e_dext_400                                
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23*x32        
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x23        *x52
        +coeff[ 41]    *x23            
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_dext_400                                =v_t_e_dext_400                                
        +coeff[ 44]                *x53
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dext_400                                ;
}
float y_e_dext_400                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1072309E-02;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59976E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.97387302E-03, 0.10637461E+00,-0.64045541E-01,-0.52976120E-01,
        -0.23352891E-01, 0.38641781E-01, 0.22284992E-01,-0.26673535E-01,
        -0.13560846E-01, 0.19020425E-02,-0.87476866E-02,-0.41075386E-02,
        -0.22862842E-02,-0.82964842E-04,-0.21071898E-03, 0.87895169E-03,
        -0.57560480E-02,-0.13040991E-02, 0.13458568E-02,-0.22150127E-02,
        -0.15770870E-02,-0.45934352E-02, 0.62697264E-03, 0.25741712E-02,
        -0.23207034E-02,-0.10519314E-02, 0.11425839E-02, 0.12605696E-02,
        -0.42576697E-02, 0.11294243E-02, 0.43059313E-02, 0.97790896E-03,
        -0.14581131E-02,-0.94141914E-02,-0.15233421E-02,-0.35123125E-03,
         0.40211875E-04, 0.70460100E-03, 0.10008893E-02, 0.35639777E-03,
         0.71831146E-03, 0.40707822E-03, 0.23141678E-03,-0.17049463E-02,
        -0.29539359E-02,-0.83411671E-02,-0.28691577E-04, 0.59305242E-03,
        -0.12735512E-03, 0.41164545E-03, 0.17414764E-03,-0.34758789E-02,
        -0.12808893E-02,-0.90078625E-03, 0.27446641E-03,-0.54489803E-02,
         0.22672730E-04,-0.42364503E-04,-0.95264098E-04, 0.12223219E-03,
         0.55311312E-03, 0.89878537E-04, 0.34333414E-02, 0.18615181E-03,
         0.31218368E-02, 0.24395646E-04,-0.28234906E-02, 0.32437601E-04,
        -0.73667048E-04,-0.29457299E-03,-0.17453751E-02,-0.22194408E-04,
        -0.23039194E-04, 0.32285522E-03,-0.25902066E-03, 0.29446368E-03,
        -0.20751990E-03,-0.99211269E-04, 0.15753485E-02, 0.75215670E-04,
         0.69295027E-04, 0.25589396E-02, 0.86894805E-04,-0.20967843E-03,
         0.21401432E-02,-0.31870912E-03, 0.38983763E-03,-0.18511865E-03,
        -0.11840515E-03,-0.16819546E-02, 0.46744381E-05, 0.11385822E-04,
         0.14886498E-04,-0.19149151E-04, 0.15022114E-04, 0.32968564E-04,
        -0.45960842E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x22*x34*x41    
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]    *x21            
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x22*x31*x44    
        +coeff[ 46]                *x51
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]    *x22    *x43    
        +coeff[ 52]    *x24*x31        
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]    *x22*x33*x42    
        +coeff[ 56]    *x22            
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]*x11*x22    *x41    
        +coeff[ 59]            *x41*x53
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]        *x31    *x53
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 62]    *x21*x31*x44    
        +coeff[ 63]    *x22    *x41*x52
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]*x11*x21    *x45    
        +coeff[ 66]    *x24*x31*x42    
        +coeff[ 67]    *x23    *x45    
        +coeff[ 68]*x11*x23*x32*x41    
        +coeff[ 69]    *x23*x34*x41    
        +coeff[ 70]    *x24    *x45    
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x11*x21    *x43    
        +coeff[ 74]    *x21*x31*x42*x51
        +coeff[ 75]*x11*x21*x32*x41    
        +coeff[ 76]    *x21*x32*x41*x51
        +coeff[ 77]*x11*x23*x31        
        +coeff[ 78]    *x23    *x43    
        +coeff[ 79]*x11    *x32*x41*x51
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 80]        *x31*x44*x51
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22*x31    *x52
        +coeff[ 83]    *x22    *x43*x51
        +coeff[ 84]    *x23*x32*x41    
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]    *x24    *x41*x51
        +coeff[ 88]    *x21    *x45*x51
    ;
    v_y_e_dext_400                                =v_y_e_dext_400                                
        +coeff[ 89]    *x24*x32*x41    
        +coeff[ 90]            *x42    
        +coeff[ 91]        *x31*x41    
        +coeff[ 92]            *x42*x52
        +coeff[ 93]*x11*x21    *x41*x51
        +coeff[ 94]*x12    *x31    *x51
        +coeff[ 95]*x11        *x43*x51
        +coeff[ 96]    *x21*x33    *x51
        ;

    return v_y_e_dext_400                                ;
}
float p_e_dext_400                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1783928E-03;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59976E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.17252527E-03,-0.23364758E-01, 0.62671839E-02,-0.12538519E-01,
         0.42923260E-02, 0.88880900E-02,-0.75601074E-02,-0.23754230E-02,
         0.55385759E-03, 0.77744458E-04, 0.50594754E-04,-0.87045511E-04,
        -0.46940981E-02,-0.22732294E-02, 0.41352984E-03,-0.22687057E-02,
        -0.10220945E-02, 0.17249461E-02,-0.29718944E-04, 0.18429977E-03,
        -0.32743977E-03,-0.12222546E-02,-0.24190187E-03, 0.42961526E-03,
        -0.27606313E-03,-0.10157981E-02, 0.88768441E-03, 0.96810327E-05,
         0.98451390E-03,-0.22988925E-03, 0.72046518E-04, 0.61350421E-03,
         0.44601396E-03,-0.45977649E-03,-0.11962367E-03, 0.44628550E-03,
        -0.27603944E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_400                                =v_p_e_dext_400                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_400                                =v_p_e_dext_400                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_400                                =v_p_e_dext_400                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x24*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x22*x31    *x52
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dext_400                                =v_p_e_dext_400                                
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22    *x43    
        ;

    return v_p_e_dext_400                                ;
}
float l_e_dext_400                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2807545E-01;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59976E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.28044967E-01,-0.49305767E+00,-0.78381822E-01, 0.19891646E-01,
        -0.33459507E-01,-0.36288738E-01, 0.21864010E-02, 0.14950248E-01,
         0.15181890E-01, 0.17088513E-02,-0.51938612E-02,-0.31832787E-02,
         0.11073240E-02,-0.87576779E-03, 0.73133679E-02, 0.89340378E-03,
        -0.11935047E-03,-0.22627595E-02, 0.53167314E-03,-0.18580285E-02,
         0.97082567E-03, 0.83368120E-03, 0.77011151E-03, 0.17634506E-02,
         0.18066765E-02, 0.14731624E-01, 0.38406882E-02, 0.96117156E-02,
         0.11803540E-01, 0.55948449E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_400                                =v_l_e_dext_400                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_400                                =v_l_e_dext_400                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dext_400                                =v_l_e_dext_400                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x23*x32*x42    
        +coeff[ 29]    *x23    *x44    
        ;

    return v_l_e_dext_400                                ;
}
float x_e_dext_350                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1061716E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.94234291E-02,-0.15361732E-01, 0.10668277E+00, 0.35054517E+00,
         0.36346886E-01, 0.10618757E-01,-0.22200346E-02,-0.41638631E-02,
        -0.14195748E-01,-0.11595049E-01, 0.98564982E-04,-0.22515906E-02,
        -0.24006038E-02,-0.59653935E-02,-0.65280212E-03,-0.11247119E-02,
        -0.79076120E-03, 0.31996192E-03,-0.58737351E-03,-0.45844266E-03,
        -0.74831228E-03,-0.98862052E-02,-0.62638111E-02,-0.42650281E-02,
        -0.52326522E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_350                                =v_x_e_dext_350                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_350                                =v_x_e_dext_350                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_350                                ;
}
float t_e_dext_350                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5861661E+00;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.56705871E-02, 0.34892075E-02,-0.10441405E+00, 0.24682770E-01,
        -0.65090866E-02, 0.20963945E-02, 0.35615757E-02,-0.84917923E-03,
        -0.19436529E-02,-0.78545930E-03, 0.17922769E-02,-0.14862496E-02,
        -0.46863197E-03,-0.21822508E-03, 0.22301228E-03, 0.48849912E-03,
         0.16310278E-03, 0.77024764E-04, 0.33530226E-03, 0.42670479E-03,
        -0.49845135E-03,-0.23532881E-03, 0.75625174E-03, 0.17849321E-02,
         0.18973299E-03,-0.52460135E-04, 0.48406789E-03, 0.21551417E-02,
         0.77775994E-03, 0.19127081E-04, 0.20165146E-03, 0.83946950E-04,
        -0.14998863E-03, 0.15320948E-03, 0.46258041E-03, 0.12252136E-03,
        -0.30729023E-03,-0.25451757E-03, 0.10807273E-02, 0.25477693E-02,
         0.19260222E-02, 0.22945651E-03,-0.20077152E-03,-0.21307942E-03,
         0.64931039E-04, 0.42014490E-04,-0.10955238E-03, 0.14057677E-03,
         0.13284112E-02, 0.17854967E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32        
    ;
    v_t_e_dext_350                                =v_t_e_dext_350                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22            
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_350                                =v_t_e_dext_350                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_dext_350                                =v_t_e_dext_350                                
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]*x11    *x31*x41    
        +coeff[ 32]        *x32*x42    
        +coeff[ 33]        *x31*x43    
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dext_350                                =v_t_e_dext_350                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]    *x23*x32        
        +coeff[ 39]    *x21*x32*x42    
        +coeff[ 40]    *x21*x31*x43    
        +coeff[ 41]    *x22*x32    *x51
        +coeff[ 42]    *x23        *x52
        +coeff[ 43]    *x23            
    ;
    v_t_e_dext_350                                =v_t_e_dext_350                                
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]                *x53
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dext_350                                ;
}
float y_e_dext_350                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.7856909E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.13246451E-03, 0.10560870E+00,-0.64616702E-01,-0.53240199E-01,
        -0.23499548E-01, 0.38636822E-01, 0.22272192E-01,-0.26966952E-01,
        -0.13527469E-01, 0.18956789E-02,-0.84742857E-02,-0.41740355E-02,
        -0.23217916E-02,-0.67683491E-05,-0.45392505E-03, 0.87092450E-03,
        -0.56346157E-02,-0.12757300E-02, 0.13234460E-02,-0.22059281E-02,
        -0.15788809E-02,-0.46487898E-02, 0.60202536E-03, 0.28300569E-02,
        -0.24086474E-02,-0.10355598E-02, 0.31491546E-02, 0.14809051E-02,
        -0.40840795E-02, 0.14001894E-03,-0.11206709E-03,-0.28610926E-02,
        -0.93427859E-02,-0.20355992E-02,-0.36880648E-03, 0.21084833E-02,
         0.10540658E-02, 0.55384560E-03, 0.71687350E-03,-0.18980113E-02,
         0.40601264E-03, 0.23381469E-03,-0.28672945E-02,-0.20089159E-02,
        -0.32048896E-02, 0.15238413E-02,-0.10216692E-03, 0.39286175E-03,
        -0.24698162E-03, 0.17116572E-03,-0.39490539E-03,-0.12574816E-02,
        -0.12570075E-02,-0.90509752E-03, 0.34284050E-03,-0.84099993E-02,
         0.10632246E-04,-0.37203103E-04,-0.27401158E-04,-0.95465133E-04,
         0.92118200E-04, 0.60463033E-03, 0.89989524E-04,-0.72181254E-04,
         0.17431681E-03,-0.37684641E-03, 0.28001206E-04,-0.51175328E-02,
        -0.13038941E-02, 0.17138441E-02, 0.58629280E-02, 0.70540034E-02,
         0.43486995E-02,-0.71703093E-05,-0.17477530E-04, 0.25928309E-03,
         0.77128134E-04,-0.12007148E-03,-0.28335167E-04,-0.43286142E-03,
         0.16910346E-03,-0.20839309E-03,-0.19119604E-03,-0.27876801E-03,
         0.12101005E-02,-0.59212130E-05, 0.61400001E-05,-0.23465405E-04,
         0.35645215E-04,-0.19021905E-04, 0.21565202E-04,-0.21154730E-04,
        -0.26653335E-04,-0.24819044E-04, 0.28127714E-03,-0.11343755E-03,
         0.22080436E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 44]    *x24*x31*x42    
        +coeff[ 45]    *x21    *x43    
        +coeff[ 46]*x11        *x41*x51
        +coeff[ 47]            *x43*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]        *x33    *x51
        +coeff[ 50]        *x33*x42    
        +coeff[ 51]    *x24    *x41    
        +coeff[ 52]    *x24*x31        
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]    *x22*x31*x44    
        +coeff[ 56]    *x21            
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]*x11    *x31    *x51
        +coeff[ 59]*x11*x22    *x41    
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]*x11*x23*x31        
        +coeff[ 64]    *x22    *x41*x52
        +coeff[ 65]    *x23*x31*x42    
        +coeff[ 66]*x11*x21    *x45    
        +coeff[ 67]    *x22*x33*x42    
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]    *x23    *x45    
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 71]    *x23*x32*x43    
        +coeff[ 72]    *x23*x33*x42    
        +coeff[ 73]                *x51
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]*x11*x21*x32*x41    
        +coeff[ 76]    *x22*x31    *x52
        +coeff[ 77]    *x22    *x43*x51
        +coeff[ 78]    *x21*x34*x41    
        +coeff[ 79]    *x22*x31*x42*x51
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 80]*x11*x23    *x43    
        +coeff[ 81]    *x23*x32*x41*x51
        +coeff[ 82]    *x21*x31*x42*x53
        +coeff[ 83]    *x24    *x43*x51
        +coeff[ 84]    *x23*x34*x41    
        +coeff[ 85]*x11        *x42    
        +coeff[ 86]*x11*x21        *x51
        +coeff[ 87]*x11*x21    *x42    
        +coeff[ 88]*x11    *x31*x42    
    ;
    v_y_e_dext_350                                =v_y_e_dext_350                                
        +coeff[ 89]*x11*x21*x31*x41    
        +coeff[ 90]*x11    *x32*x41    
        +coeff[ 91]    *x21*x31*x43    
        +coeff[ 92]*x11*x21    *x41*x51
        +coeff[ 93]*x11*x21*x31    *x51
        +coeff[ 94]*x11*x21    *x43    
        +coeff[ 95]        *x34*x41    
        +coeff[ 96]    *x22    *x42*x51
        ;

    return v_y_e_dext_350                                ;
}
float p_e_dext_350                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1222740E-03;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.11332593E-03,-0.23443336E-01, 0.61413255E-02,-0.12562206E-01,
         0.42877700E-02, 0.88665569E-02,-0.76089040E-02,-0.23733734E-02,
         0.49042125E-03, 0.10017825E-03, 0.85780266E-04,-0.99584315E-04,
        -0.46870639E-02,-0.23185220E-02, 0.41365399E-03,-0.22410743E-02,
        -0.10172295E-02, 0.17356671E-02,-0.21759584E-04, 0.18253067E-03,
        -0.32639352E-03,-0.12160827E-02,-0.24335660E-03, 0.42234684E-03,
        -0.33758811E-03,-0.99495449E-03, 0.88299578E-03, 0.33455482E-04,
         0.98014052E-03, 0.71364542E-04,-0.78316938E-04, 0.60725457E-03,
         0.43629188E-03,-0.47123834E-03,-0.12092168E-03, 0.43106943E-03,
        -0.25918521E-03, 0.24391149E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_350                                =v_p_e_dext_350                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_350                                =v_p_e_dext_350                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_350                                =v_p_e_dext_350                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x24*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dext_350                                =v_p_e_dext_350                                
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dext_350                                ;
}
float l_e_dext_350                                (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.2536052E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.24100270E-01,-0.49380529E+00,-0.78721583E-01, 0.19910926E-01,
        -0.33117641E-01,-0.36237422E-01, 0.21110794E-02, 0.18501844E-01,
         0.13907438E-01, 0.28317524E-02,-0.49968292E-02,-0.31018208E-02,
         0.11254405E-02,-0.91822108E-03, 0.70688967E-02, 0.34771592E-03,
         0.33158864E-03,-0.25396917E-02,-0.16096683E-02, 0.10712025E-02,
         0.76932125E-02, 0.12703037E-02, 0.58751757E-03, 0.71808597E-03,
         0.15634141E-02, 0.13867265E-02, 0.10526149E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_350                                =v_l_e_dext_350                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_350                                =v_l_e_dext_350                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dext_350                                =v_l_e_dext_350                                
        +coeff[ 26]    *x23*x31*x41    
        ;

    return v_l_e_dext_350                                ;
}
float x_e_dext_300                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1025039E-01;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.96147982E-02,-0.15306750E-01, 0.10661703E+00, 0.35320523E+00,
         0.36423493E-01, 0.10814081E-01,-0.22044326E-02,-0.41103931E-02,
        -0.14197046E-01,-0.11706156E-01,-0.22710337E-05,-0.22565317E-02,
        -0.23604257E-02,-0.58816536E-02,-0.65280066E-03,-0.10949026E-02,
        -0.77181042E-03, 0.30903035E-03,-0.60111284E-03,-0.43861999E-03,
        -0.72182575E-03,-0.10164849E-01,-0.62547056E-02,-0.44179941E-02,
        -0.61201681E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_300                                =v_x_e_dext_300                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_300                                =v_x_e_dext_300                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_300                                ;
}
float t_e_dext_300                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5865046E+00;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54992121E-02,-0.10512121E+00, 0.24687618E-01,-0.65146317E-02,
        -0.15602915E-04, 0.34893551E-02, 0.21083013E-02, 0.35641626E-02,
        -0.26196399E-03,-0.86536928E-03,-0.19237507E-02,-0.78754645E-03,
         0.18296564E-02,-0.14727827E-02,-0.48772522E-03,-0.21430780E-03,
         0.20077440E-03, 0.49556908E-03, 0.16263333E-03, 0.99334604E-04,
         0.35101987E-03, 0.42677028E-03,-0.53212111E-03,-0.26014526E-03,
         0.74581936E-03, 0.17533189E-03,-0.24181879E-03,-0.27931734E-04,
         0.49453642E-03,-0.30431207E-03, 0.17946369E-02, 0.74910722E-03,
         0.74469419E-04, 0.11441611E-03, 0.13724237E-03, 0.53666026E-03,
         0.12229299E-03,-0.10517917E-03, 0.80811937E-04, 0.10873497E-02,
         0.21334868E-02, 0.25490904E-02, 0.18819711E-02,-0.21210528E-04,
         0.47913414E-04,-0.15224046E-03, 0.12477936E-02, 0.21438717E-03,
         0.18175779E-03,-0.19205057E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_300                                =v_t_e_dext_300                                
        +coeff[  8]            *x42*x52
        +coeff[  9]        *x32        
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22            
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dext_300                                =v_t_e_dext_300                                
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]*x11*x22            
    ;
    v_t_e_dext_300                                =v_t_e_dext_300                                
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x31*x41*x52
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x22    *x42*x51
        +coeff[ 32]    *x23*x31*x41*x51
        +coeff[ 33]*x11    *x31*x41    
        +coeff[ 34]        *x31*x43    
    ;
    v_t_e_dext_300                                =v_t_e_dext_300                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]        *x32    *x52
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]    *x23*x32        
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_dext_300                                =v_t_e_dext_300                                
        +coeff[ 44]                *x53
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]    *x22*x31*x41*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_dext_300                                ;
}
float y_e_dext_300                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.5745630E-03;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.59318083E-03, 0.10492491E+00,-0.65303043E-01,-0.53250790E-01,
        -0.23362666E-01, 0.38642444E-01, 0.22262193E-01,-0.26820743E-01,
        -0.13489423E-01, 0.18797740E-02,-0.83236620E-02,-0.41540829E-02,
        -0.23344655E-02,-0.17167346E-03,-0.65907330E-03, 0.86504495E-03,
        -0.55142054E-02,-0.12384707E-02, 0.13719045E-02,-0.22075726E-02,
        -0.15692465E-02,-0.44516921E-02, 0.61583315E-03, 0.25556008E-02,
        -0.22476309E-02,-0.10226642E-02, 0.94495172E-03, 0.12153275E-02,
        -0.51078731E-02, 0.10599386E-02, 0.42006914E-02, 0.78212575E-03,
        -0.13288266E-02,-0.84105292E-02, 0.75940444E-03, 0.10127175E-02,
         0.31419285E-03, 0.67877059E-03, 0.41085598E-03, 0.23884860E-03,
        -0.17897206E-02,-0.37638103E-02,-0.14628625E-02,-0.12112583E-02,
        -0.78691281E-02, 0.51915861E-03,-0.10320063E-03, 0.41407632E-03,
        -0.38342385E-03, 0.16245998E-03,-0.37528186E-02,-0.52524905E-03,
         0.29528860E-03,-0.87204775E-04, 0.10693699E-03, 0.51070959E-03,
         0.92487760E-04, 0.34498705E-02, 0.14175092E-03, 0.33899578E-02,
         0.21254627E-02, 0.15076749E-03, 0.22559051E-03,-0.49755399E-04,
        -0.17926507E-02,-0.17345659E-04,-0.23462693E-04,-0.25464471E-04,
        -0.15759407E-03, 0.23427914E-03,-0.89706562E-04, 0.16054892E-02,
         0.26063703E-02, 0.68865957E-04,-0.36773924E-03, 0.23424751E-03,
        -0.42420754E-03, 0.16533810E-03, 0.44310655E-03,-0.20222495E-03,
        -0.25463372E-02,-0.42502075E-02,-0.12517185E-02,-0.99851249E-03,
         0.33158215E-05, 0.37296846E-04,-0.20292697E-04,-0.20046957E-04,
         0.29674406E-04, 0.19864843E-04, 0.18442704E-03, 0.17320132E-04,
        -0.37707970E-04,-0.34770081E-04,-0.63504769E-04,-0.52579639E-04,
        -0.42752596E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x21*x32*x41    
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 44]    *x22*x31*x44    
        +coeff[ 45]    *x21    *x43    
        +coeff[ 46]*x11        *x41*x51
        +coeff[ 47]            *x43*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]        *x33    *x51
        +coeff[ 50]    *x22    *x43    
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]    *x23    *x41*x51
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 53]*x11*x22    *x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]        *x31    *x53
        +coeff[ 57]    *x21*x31*x44    
        +coeff[ 58]    *x22    *x41*x52
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]*x11*x21    *x45    
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 62]    *x23    *x45    
        +coeff[ 63]*x11*x23*x32*x41    
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]*x12        *x41    
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11    *x31    *x51
        +coeff[ 68]        *x34*x41    
        +coeff[ 69]*x11*x21*x32*x41    
        +coeff[ 70]*x11*x23*x31        
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 71]    *x23    *x43    
        +coeff[ 72]    *x21*x33*x42    
        +coeff[ 73]    *x22*x31    *x52
        +coeff[ 74]    *x22    *x43*x51
        +coeff[ 75]        *x32*x43*x51
        +coeff[ 76]    *x22*x31*x42*x51
        +coeff[ 77]        *x33*x42*x51
        +coeff[ 78]    *x23*x33        
        +coeff[ 79]    *x24    *x41*x51
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 80]    *x24*x31*x42    
        +coeff[ 81]    *x22*x33*x42    
        +coeff[ 82]    *x24*x32*x41    
        +coeff[ 83]    *x22*x34*x41    
        +coeff[ 84]                *x51
        +coeff[ 85]*x11    *x31*x41    
        +coeff[ 86]    *x22    *x42    
        +coeff[ 87]    *x22*x31*x41    
        +coeff[ 88]*x11    *x31*x42    
    ;
    v_y_e_dext_300                                =v_y_e_dext_300                                
        +coeff[ 89]*x11    *x32*x41    
        +coeff[ 90]*x11*x21    *x43    
        +coeff[ 91]    *x21    *x43*x51
        +coeff[ 92]*x11*x22*x31*x41    
        +coeff[ 93]*x11    *x33*x41    
        +coeff[ 94]    *x21*x32*x41*x51
        +coeff[ 95]        *x31*x42*x52
        +coeff[ 96]*x12*x22    *x41    
        ;

    return v_y_e_dext_300                                ;
}
float p_e_dext_300                                (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.6321446E-04;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.68573638E-04,-0.23560598E-01, 0.60216389E-02,-0.12595554E-01,
         0.42751431E-02, 0.88738492E-02,-0.77422359E-02,-0.23608054E-02,
         0.56329189E-03, 0.88805085E-04, 0.31050604E-04,-0.46972288E-02,
        -0.23903155E-02, 0.41258326E-03,-0.22443624E-02,-0.10217963E-02,
         0.17444083E-02,-0.21417768E-05, 0.18351483E-03,-0.32140937E-03,
        -0.12273784E-02,-0.23779743E-03, 0.42701286E-03,-0.33277369E-03,
        -0.10059124E-02, 0.85784413E-03, 0.98838704E-03,-0.44443991E-03,
         0.50349830E-03, 0.68285306E-04,-0.76345605E-04, 0.62257680E-03,
         0.43101591E-03,-0.12697838E-03,-0.26372154E-03, 0.24630595E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_300                                =v_p_e_dext_300                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dext_300                                =v_p_e_dext_300                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23    *x41*x51
    ;
    v_p_e_dext_300                                =v_p_e_dext_300                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x24    *x41    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dext_300                                =v_p_e_dext_300                                
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dext_300                                ;
}
float l_e_dext_300                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2458291E-01;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.24103342E-01,-0.49716815E+00,-0.78626454E-01, 0.19799840E-01,
        -0.33432115E-01,-0.36246445E-01, 0.21875012E-02, 0.14694486E-01,
         0.13065340E-01, 0.45616780E-02,-0.46765707E-02,-0.30897669E-02,
         0.11018524E-02,-0.93184889E-03, 0.63704378E-02,-0.16234980E-03,
         0.20844485E-03,-0.23120458E-02,-0.52165869E-03,-0.17160540E-02,
         0.11387496E-02, 0.14641931E-01, 0.10338940E-01, 0.11587039E-02,
         0.95063605E-03, 0.78205630E-03, 0.13346012E-02, 0.79228533E-02,
         0.69329543E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_300                                =v_l_e_dext_300                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_300                                =v_l_e_dext_300                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_dext_300                                =v_l_e_dext_300                                
        +coeff[ 26]    *x24            
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x21*x32*x44    
        ;

    return v_l_e_dext_300                                ;
}
float x_e_dext_250                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.9870543E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.94431248E-02,-0.15214434E-01, 0.10662632E+00, 0.35608688E+00,
         0.36450475E-01, 0.10933981E-01,-0.22191061E-02,-0.40154820E-02,
        -0.15944054E-01,-0.11482973E-01, 0.85916610E-04,-0.26333646E-02,
        -0.23179653E-02,-0.59154518E-02,-0.65064820E-03,-0.11071798E-02,
        -0.72661554E-03, 0.32140830E-03,-0.58513077E-03,-0.46802015E-03,
        -0.73633849E-03,-0.79647303E-02,-0.57870275E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_250                                =v_x_e_dext_250                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_250                                =v_x_e_dext_250                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        ;

    return v_x_e_dext_250                                ;
}
float t_e_dext_250                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5864673E+00;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55909608E-02, 0.34526626E-02,-0.10586608E+00, 0.24693076E-01,
        -0.64933612E-02, 0.21592572E-02, 0.36813198E-02,-0.26629335E-03,
        -0.18578424E-02,-0.79127948E-03, 0.25911911E-02,-0.14362892E-02,
        -0.57149807E-03,-0.87585149E-03,-0.21632579E-03, 0.32923516E-03,
         0.51503669E-03, 0.16581859E-03,-0.49268134E-03, 0.94474155E-04,
         0.42803655E-03, 0.31751214E-03,-0.33860249E-03, 0.18592534E-03,
        -0.27135378E-03, 0.67336962E-03,-0.36227197E-04, 0.50178648E-03,
         0.15576321E-02, 0.69657445E-03,-0.51602918E-04, 0.35634719E-04,
         0.79250764E-04,-0.10670718E-03, 0.59528119E-03, 0.16357836E-03,
        -0.31010309E-03,-0.27344900E-03, 0.14940726E-06, 0.84272475E-03,
         0.17227876E-02, 0.11277158E-02, 0.84259384E-03, 0.18773583E-03,
        -0.11598824E-03, 0.64071144E-04, 0.46276840E-04, 0.12623629E-03,
        -0.87052627E-04, 0.13486166E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_250                                =v_t_e_dext_250                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]        *x32    *x51
    ;
    v_t_e_dext_250                                =v_t_e_dext_250                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x22    *x42    
    ;
    v_t_e_dext_250                                =v_t_e_dext_250                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x21*x33*x41*x51
        +coeff[ 32]*x11    *x31*x41    
        +coeff[ 33]        *x33*x41    
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dext_250                                =v_t_e_dext_250                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]    *x23*x32        
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]    *x22*x32    *x51
    ;
    v_t_e_dext_250                                =v_t_e_dext_250                                
        +coeff[ 44]    *x22*x32    *x52
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]                *x53
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]        *x32    *x52
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_dext_250                                ;
}
float y_e_dext_250                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4147914E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.47799628E-03, 0.10374861E+00,-0.66249743E-01,-0.53373214E-01,
        -0.23387128E-01, 0.38590748E-01, 0.22252273E-01,-0.27208151E-01,
        -0.13790784E-01, 0.18585572E-02,-0.84491875E-02,-0.41257828E-02,
        -0.23234254E-02,-0.13779897E-03,-0.41277177E-03, 0.85319392E-03,
        -0.56563150E-02,-0.12766209E-02, 0.13442915E-02,-0.21985415E-02,
        -0.15755664E-02,-0.44645187E-02, 0.57007826E-03, 0.25430352E-02,
        -0.22960098E-02,-0.10289551E-02, 0.87551790E-03, 0.12343890E-02,
        -0.44036210E-02, 0.11737032E-02, 0.46355380E-02, 0.87996823E-03,
         0.46455811E-03,-0.12708127E-02,-0.88476986E-02,-0.15609189E-02,
        -0.47547964E-03, 0.33191031E-04, 0.99106797E-03, 0.30194898E-03,
         0.71944238E-03, 0.40834409E-03, 0.23817425E-03,-0.17074852E-02,
        -0.29413728E-02,-0.33292770E-05,-0.78568626E-02,-0.15113302E-03,
        -0.25656911E-04,-0.99416502E-04, 0.58182108E-03, 0.17729287E-03,
        -0.36663550E-02,-0.12288479E-02,-0.85792609E-03, 0.28002687E-03,
         0.17284049E-02, 0.33531992E-02, 0.22448017E-02,-0.50056498E-02,
         0.70010421E-04, 0.20690371E-04, 0.40620239E-03,-0.38039387E-04,
         0.43778704E-03,-0.72996307E-04, 0.11195210E-03, 0.59677160E-03,
         0.88747394E-04, 0.34987940E-02, 0.16066426E-03, 0.28588965E-02,
         0.86683176E-04, 0.15151258E-03,-0.19487168E-02,-0.24252040E-04,
        -0.27452725E-04,-0.27686372E-03, 0.28855549E-03,-0.15303881E-03,
         0.18440465E-03,-0.12235205E-03, 0.33173656E-04, 0.89813664E-04,
        -0.31006645E-03,-0.31316897E-03,-0.16804101E-03,-0.32890146E-02,
        -0.19667018E-02,-0.28142661E-05, 0.12850857E-04, 0.10590269E-04,
         0.49799460E-05,-0.36737781E-05, 0.26943973E-04,-0.23219292E-03,
        -0.16131726E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]    *x21*x34*x41    
        +coeff[ 32]    *x23*x33        
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]    *x22*x32*x43    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 35]    *x22*x34*x41    
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21            
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]            *x45*x51
        +coeff[ 46]    *x22*x31*x44    
        +coeff[ 47]    *x21*x32*x45    
        +coeff[ 48]                *x51
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]    *x21*x32*x41    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x22    *x43    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 53]    *x24*x31        
        +coeff[ 54]    *x22*x33        
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]    *x23    *x43    
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x23*x32*x41    
        +coeff[ 59]    *x22*x33*x42    
        +coeff[ 60]    *x23    *x45    
        +coeff[ 61]    *x22            
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 62]    *x21    *x43    
        +coeff[ 63]*x12        *x41    
        +coeff[ 64]            *x43*x51
        +coeff[ 65]*x11*x22    *x41    
        +coeff[ 66]            *x41*x53
        +coeff[ 67]*x11*x21*x31*x42    
        +coeff[ 68]        *x31    *x53
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x22    *x41*x52
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 71]    *x21*x33*x42    
        +coeff[ 72]*x11*x21    *x45    
        +coeff[ 73]*x11*x23*x32*x41    
        +coeff[ 74]    *x24    *x45    
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]*x11    *x31    *x51
        +coeff[ 77]        *x33*x42    
        +coeff[ 78]*x11*x21    *x43    
        +coeff[ 79]    *x21*x31*x42*x51
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 80]*x11*x21*x32*x41    
        +coeff[ 81]    *x21*x32*x41*x51
        +coeff[ 82]        *x31*x44*x51
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x24    *x41*x51
        +coeff[ 87]    *x24*x31*x42    
        +coeff[ 88]    *x24*x32*x41    
    ;
    v_y_e_dext_250                                =v_y_e_dext_250                                
        +coeff[ 89]*x11                
        +coeff[ 90]            *x42    
        +coeff[ 91]        *x31*x41    
        +coeff[ 92]        *x32        
        +coeff[ 93]*x11*x21            
        +coeff[ 94]*x11    *x31*x42    
        +coeff[ 95]        *x31*x44    
        +coeff[ 96]*x11*x21    *x41*x51
        ;

    return v_y_e_dext_250                                ;
}
float p_e_dext_250                                (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2255054E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.22558027E-03,-0.23743642E-01, 0.58088484E-02,-0.12634271E-01,
         0.42648371E-02, 0.88526485E-02,-0.77724047E-02,-0.23582436E-02,
         0.45356623E-03, 0.10589777E-03, 0.15441938E-03,-0.12220975E-03,
        -0.46851845E-02,-0.23284394E-02, 0.40624337E-03,-0.22342394E-02,
        -0.10139646E-02, 0.17858151E-02,-0.17361448E-04, 0.17816643E-03,
        -0.32689181E-03,-0.12114117E-02,-0.22696803E-03, 0.42491412E-03,
        -0.32778949E-03,-0.10116640E-02, 0.88223320E-03, 0.14745659E-04,
         0.98356744E-03, 0.73500320E-04,-0.76030090E-04, 0.61677129E-03,
         0.44406339E-03,-0.46184176E-03,-0.12198758E-03, 0.46419739E-03,
        -0.28362105E-03, 0.23307183E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_250                                =v_p_e_dext_250                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_250                                =v_p_e_dext_250                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_250                                =v_p_e_dext_250                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x24*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dext_250                                =v_p_e_dext_250                                
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dext_250                                ;
}
float l_e_dext_250                                (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.2427832E-01;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.24033453E-01,-0.50139397E+00,-0.78733131E-01, 0.19650489E-01,
        -0.33981714E-01,-0.36116533E-01, 0.21678202E-02, 0.18675648E-01,
         0.14083031E-01, 0.38689154E-02,-0.47760829E-02,-0.28982060E-02,
         0.12087063E-02,-0.90625271E-03, 0.67783464E-02,-0.18540646E-02,
        -0.21635095E-04, 0.23516014E-03,-0.23109687E-02, 0.95649238E-03,
         0.74468250E-02, 0.10792882E-02, 0.87010482E-03, 0.14467752E-02,
         0.11162249E-01, 0.18402112E-02, 0.74926671E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dext_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_250                                =v_l_e_dext_250                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_dext_250                                =v_l_e_dext_250                                
        +coeff[ 17]        *x34        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]        *x33*x41*x51
    ;
    v_l_e_dext_250                                =v_l_e_dext_250                                
        +coeff[ 26]        *x32    *x53
        ;

    return v_l_e_dext_250                                ;
}
float x_e_dext_200                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1393325E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.11656352E-01,-0.15100216E-01, 0.10678823E+00, 0.35848540E+00,
         0.36271784E-01, 0.10953522E-01,-0.22154083E-02,-0.40640235E-02,
        -0.14015748E-01,-0.11682817E-01, 0.51593295E-04,-0.21523256E-02,
        -0.23850277E-02,-0.59281085E-02,-0.66008215E-03,-0.11533048E-02,
        -0.73560618E-03, 0.30682737E-03,-0.60696085E-03,-0.46087962E-03,
        -0.73725643E-03,-0.10118628E-01,-0.60556149E-02,-0.43182611E-02,
        -0.54947888E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_200                                =v_x_e_dext_200                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_200                                =v_x_e_dext_200                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_200                                ;
}
float t_e_dext_200                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5857140E+00;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.58004553E-02, 0.34184670E-02,-0.10652278E+00, 0.24667799E-01,
        -0.64223181E-02, 0.21172769E-02, 0.37375109E-02,-0.92915475E-03,
        -0.18238383E-02,-0.78824081E-03, 0.18305618E-02,-0.14201328E-02,
        -0.55285695E-03,-0.21414412E-03, 0.35733037E-03, 0.50325185E-03,
         0.16901585E-03, 0.74954631E-04, 0.40317845E-03, 0.52208232E-03,
        -0.49236318E-03,-0.24587195E-03, 0.75446034E-03, 0.17407945E-03,
        -0.47264555E-04, 0.51001750E-03, 0.13321567E-02, 0.76763239E-03,
        -0.20801810E-04, 0.20560890E-03, 0.74470612E-04,-0.12584581E-03,
         0.15234279E-03, 0.50776429E-03, 0.14162857E-03,-0.28502295E-03,
        -0.24984445E-03, 0.17971779E-02, 0.23769215E-02, 0.18143540E-02,
         0.29316905E-03,-0.38230428E-03, 0.75979377E-04, 0.34967841E-04,
        -0.11952852E-03, 0.69187919E-03, 0.23506761E-03, 0.12318749E-02,
         0.97317235E-04, 0.27138548E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32        
    ;
    v_t_e_dext_200                                =v_t_e_dext_200                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22            
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]*x11*x21            
    ;
    v_t_e_dext_200                                =v_t_e_dext_200                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_t_e_dext_200                                =v_t_e_dext_200                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]*x12*x21*x31*x41*x51
        +coeff[ 29]    *x21*x33*x41*x51
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]        *x32*x42    
        +coeff[ 32]        *x31*x43    
        +coeff[ 33]    *x21*x31*x41*x51
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_dext_200                                =v_t_e_dext_200                                
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23*x31*x41    
        +coeff[ 38]    *x21*x32*x42    
        +coeff[ 39]    *x21*x31*x43    
        +coeff[ 40]    *x22*x32    *x51
        +coeff[ 41]    *x23        *x52
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]                *x53
    ;
    v_t_e_dext_200                                =v_t_e_dext_200                                
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]*x11*x22    *x42    
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dext_200                                ;
}
float y_e_dext_200                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.4554805E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.39859590E-03, 0.10191819E+00,-0.67829944E-01,-0.53758349E-01,
        -0.23424236E-01, 0.38594671E-01, 0.22200316E-01,-0.27515991E-01,
        -0.13694101E-01, 0.18371143E-02,-0.83194757E-02,-0.41261129E-02,
        -0.23228545E-02,-0.55736691E-04,-0.56558102E-03, 0.85083873E-03,
        -0.54620211E-02,-0.12513108E-02, 0.13297615E-02,-0.21966035E-02,
        -0.15712355E-02,-0.22691910E-02,-0.45242677E-02, 0.56535745E-03,
         0.25198334E-02,-0.10079758E-02, 0.10299799E-02, 0.12093364E-02,
        -0.43482161E-02, 0.11785995E-02, 0.19697659E-02,-0.26094902E-02,
        -0.89716287E-02,-0.21331098E-02,-0.41289179E-03, 0.69694483E-03,
         0.10558814E-02, 0.29520053E-03, 0.70412393E-03,-0.22723738E-02,
         0.39629976E-03,-0.30172383E-02,-0.12728606E-02,-0.20579658E-02,
         0.33876593E-04,-0.26434705E-04, 0.40966910E-03,-0.99216995E-04,
         0.43758078E-03,-0.32786943E-03, 0.17432265E-03, 0.23264252E-03,
        -0.13289195E-02,-0.87880722E-03, 0.27927410E-03,-0.78463508E-02,
        -0.51571042E-02, 0.25671859E-04,-0.37636786E-04,-0.90126654E-04,
        -0.31537056E-03,-0.14962562E-03, 0.12290919E-03, 0.58792200E-03,
         0.89153800E-04, 0.17447016E-03, 0.30093021E-02,-0.38812133E-04,
        -0.34629828E-02, 0.94669456E-04,-0.12691938E-02,-0.27927052E-03,
        -0.19888785E-04,-0.24894884E-04,-0.45459448E-04, 0.39101369E-03,
        -0.18276216E-03, 0.21805441E-03,-0.12814465E-03, 0.30563397E-02,
         0.18368450E-02, 0.39888108E-02, 0.26235040E-02, 0.88041896E-04,
        -0.43963551E-03, 0.82641683E-03,-0.47646766E-03, 0.41243262E-03,
        -0.14193858E-03, 0.12441214E-04, 0.92007958E-05, 0.33748693E-04,
         0.24758714E-04, 0.33683973E-04,-0.11242390E-04,-0.17858063E-04,
        -0.19846599E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]    *x21    *x43    
        +coeff[ 47]*x11        *x41*x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]        *x31*x44    
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]    *x21*x31    *x52
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x23    *x41*x51
        +coeff[ 55]    *x22*x31*x44    
        +coeff[ 56]    *x22*x33*x42    
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11*x22    *x41    
        +coeff[ 60]        *x33*x42    
        +coeff[ 61]        *x34*x41    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 62]            *x41*x53
        +coeff[ 63]*x11*x21*x31*x42    
        +coeff[ 64]        *x31    *x53
        +coeff[ 65]    *x22    *x41*x52
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]*x11*x21    *x45    
        +coeff[ 68]    *x24*x31*x42    
        +coeff[ 69]*x11*x21*x32*x43    
        +coeff[ 70]    *x22*x34*x41    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 71]    *x23    *x45    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x11*x21    *x41*x51
        +coeff[ 75]*x11*x21    *x43    
        +coeff[ 76]    *x21*x31*x42*x51
        +coeff[ 77]*x11*x21*x32*x41    
        +coeff[ 78]    *x21*x32*x41*x51
        +coeff[ 79]    *x21*x31*x44    
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 80]    *x23    *x43    
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]    *x21*x33*x42    
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]    *x21*x34*x41    
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24    *x41*x51
    ;
    v_y_e_dext_200                                =v_y_e_dext_200                                
        +coeff[ 89]        *x31*x41    
        +coeff[ 90]            *x44    
        +coeff[ 91]        *x32*x42    
        +coeff[ 92]    *x22*x31*x41    
        +coeff[ 93]*x11    *x32*x41    
        +coeff[ 94]*x12        *x42    
        +coeff[ 95]*x11*x22*x31        
        +coeff[ 96]*x11*x21*x31    *x51
        ;

    return v_y_e_dext_200                                ;
}
float p_e_dext_200                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1163229E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.11771307E-03,-0.24020258E-01, 0.54395171E-02,-0.12703720E-01,
         0.42445729E-02, 0.88380016E-02,-0.78172684E-02,-0.23582403E-02,
         0.56014152E-03, 0.86218664E-04, 0.26072451E-05,-0.46790917E-02,
        -0.23573416E-02, 0.40261046E-03,-0.22210421E-02,-0.10136012E-02,
         0.17931403E-02,-0.43045453E-04, 0.17555511E-03,-0.32799915E-03,
        -0.11834304E-02,-0.21881760E-03, 0.42219512E-03,-0.32705607E-03,
        -0.10021467E-02, 0.88989257E-03, 0.95364981E-03,-0.44082821E-03,
         0.50591584E-03, 0.66235807E-04,-0.76605444E-04, 0.56436122E-03,
         0.42716696E-03,-0.13335943E-03,-0.26171800E-03, 0.24585510E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_200                                =v_p_e_dext_200                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dext_200                                =v_p_e_dext_200                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23    *x41*x51
    ;
    v_p_e_dext_200                                =v_p_e_dext_200                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x24    *x41    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dext_200                                =v_p_e_dext_200                                
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dext_200                                ;
}
float l_e_dext_200                                (float *x,int m){
    int ncoeff= 25;
    float avdat= -0.2962232E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
         0.26834194E-01,-0.50452495E+00,-0.78568570E-01, 0.19479396E-01,
        -0.34405380E-01,-0.35824548E-01, 0.21529866E-02, 0.18882118E-01,
         0.14459852E-01, 0.30206800E-02,-0.45578089E-02,-0.30655588E-02,
         0.11613843E-02,-0.92245848E-03, 0.70358715E-02, 0.31989766E-03,
         0.34710325E-03,-0.24099983E-02,-0.18702837E-02, 0.12556545E-02,
        -0.34632240E-03, 0.85625157E-03, 0.16763798E-02, 0.98823477E-02,
         0.68795825E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_200                                =v_l_e_dext_200                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_200                                =v_l_e_dext_200                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23            
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x24            
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        ;

    return v_l_e_dext_200                                ;
}
float x_e_dext_175                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1338569E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.97268783E-02,-0.15011962E-01, 0.10692570E+00, 0.35992828E+00,
         0.36118865E-01, 0.11010431E-01,-0.22368238E-02,-0.40652989E-02,
        -0.14244353E-01,-0.11729066E-01, 0.48817117E-04,-0.22707600E-02,
        -0.24313608E-02,-0.57996460E-02,-0.63454750E-03,-0.11731797E-02,
        -0.69570972E-03, 0.32079179E-03,-0.59651508E-03,-0.48347778E-03,
        -0.77728333E-03,-0.93364082E-02,-0.55980580E-02,-0.39504520E-02,
        -0.49202638E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_175                                =v_x_e_dext_175                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_175                                =v_x_e_dext_175                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_175                                ;
}
float t_e_dext_175                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5853152E+00;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.57766773E-02, 0.33970070E-02,-0.10682194E+00, 0.24630282E-01,
        -0.63753650E-02, 0.22493326E-02, 0.37364191E-02,-0.24355471E-03,
        -0.17486832E-02,-0.78579603E-03, 0.18590133E-02,-0.13884091E-02,
        -0.54654671E-03,-0.94270479E-03,-0.20141050E-03, 0.32194550E-03,
         0.53820648E-03, 0.16738247E-03,-0.55620843E-03, 0.43230486E-03,
         0.54527592E-03, 0.70134201E-03, 0.76048262E-03, 0.17228434E-03,
        -0.25895690E-04, 0.60156983E-03, 0.53335540E-03, 0.12197052E-02,
         0.70519716E-04, 0.94545270E-04,-0.28481818E-03,-0.15467453E-03,
         0.14713636E-03,-0.29476770E-03,-0.25644168E-03, 0.16391882E-02,
         0.22525103E-02, 0.17454065E-02,-0.42238514E-03, 0.71736038E-04,
        -0.22335484E-04, 0.43024767E-04,-0.46419678E-04,-0.10599152E-03,
         0.71019505E-03, 0.25753121E-03, 0.11383169E-02, 0.11850648E-03,
         0.16974910E-03, 0.19828127E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_175                                =v_t_e_dext_175                                
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]        *x32    *x51
    ;
    v_t_e_dext_175                                =v_t_e_dext_175                                
        +coeff[ 17]*x11*x21            
        +coeff[ 18]    *x22            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_dext_175                                =v_t_e_dext_175                                
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x11    *x31*x41    
        +coeff[ 29]    *x22        *x51
        +coeff[ 30]    *x22*x31*x41    
        +coeff[ 31]        *x33*x41    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_dext_175                                =v_t_e_dext_175                                
        +coeff[ 35]    *x23*x31*x41    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]                *x53
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_dext_175                                =v_t_e_dext_175                                
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]*x11*x22    *x42    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dext_175                                ;
}
float y_e_dext_175                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.9565394E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.12713393E-03, 0.10039050E+00,-0.68958730E-01,-0.54082729E-01,
        -0.23605244E-01, 0.38520508E-01, 0.22165714E-01,-0.27511749E-01,
        -0.13688947E-01, 0.18321647E-02,-0.82791913E-02,-0.40792553E-02,
        -0.23124116E-02,-0.16166285E-04,-0.55839535E-03, 0.83534024E-03,
        -0.53776326E-02,-0.12434461E-02, 0.12614059E-02,-0.21843140E-02,
        -0.15622921E-02,-0.45193224E-02, 0.56324061E-03, 0.28043112E-02,
        -0.21836557E-02,-0.10045012E-02, 0.30371556E-02, 0.14472655E-02,
        -0.41984785E-02, 0.23024736E-03,-0.31685042E-04,-0.27886492E-02,
        -0.87585552E-02,-0.20183902E-02,-0.40782412E-03, 0.17765200E-02,
         0.10520935E-02, 0.52108400E-03, 0.70101005E-03,-0.20697773E-02,
         0.38926073E-03, 0.23931661E-03,-0.30599039E-02,-0.21334609E-02,
        -0.35339256E-02, 0.21887816E-04,-0.17174954E-04, 0.12843200E-02,
        -0.10593722E-03, 0.44148386E-03,-0.26969941E-03, 0.17548050E-03,
        -0.12988447E-02,-0.12026402E-02,-0.86080335E-03, 0.26346702E-03,
        -0.79041775E-02,-0.49337931E-02,-0.89308698E-04, 0.40814758E-03,
        -0.19380957E-03, 0.98017692E-04, 0.57716377E-03, 0.93536881E-04,
         0.26159253E-03, 0.17231573E-03,-0.78350503E-03,-0.12362995E-04,
        -0.12136289E-02, 0.17364734E-02, 0.59278389E-02, 0.61839162E-02,
         0.43618316E-02, 0.12883473E-02, 0.11947117E-04,-0.29058208E-04,
        -0.21992288E-04,-0.30544621E-03,-0.24185197E-03, 0.75596225E-04,
        -0.17147115E-03, 0.65364689E-03, 0.77480152E-04,-0.39772765E-03,
        -0.38726165E-03,-0.17444628E-03, 0.12459068E-04,-0.72542383E-06,
         0.10438052E-04,-0.20792202E-04,-0.14552780E-04, 0.39275219E-04,
         0.14409786E-04,-0.18259328E-04,-0.26673051E-04,-0.22702881E-04,
        -0.63965606E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 44]    *x24*x31*x42    
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 53]    *x24*x31        
        +coeff[ 54]    *x22*x33        
        +coeff[ 55]    *x23    *x41*x51
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]    *x22*x33*x42    
        +coeff[ 58]*x11*x22    *x41    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]        *x34*x41    
        +coeff[ 61]            *x41*x53
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]        *x31    *x53
        +coeff[ 64]*x11*x21*x32*x41    
        +coeff[ 65]    *x22    *x41*x52
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]*x11    *x31    *x53
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]    *x23    *x45    
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 71]    *x23*x32*x43    
        +coeff[ 72]    *x23*x33*x42    
        +coeff[ 73]    *x23*x34*x41    
        +coeff[ 74]    *x22            
        +coeff[ 75]*x12        *x41    
        +coeff[ 76]*x12    *x31        
        +coeff[ 77]        *x33*x42    
        +coeff[ 78]    *x21*x31*x42*x51
        +coeff[ 79]*x11*x23    *x41    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 80]    *x21*x32*x41*x51
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]    *x22*x31    *x52
        +coeff[ 83]    *x22    *x43*x51
        +coeff[ 84]    *x22*x31*x42*x51
        +coeff[ 85]    *x24    *x41*x51
        +coeff[ 86]        *x31*x41    
        +coeff[ 87]*x11*x21            
        +coeff[ 88]            *x44    
    ;
    v_y_e_dext_175                                =v_y_e_dext_175                                
        +coeff[ 89]*x11    *x31    *x51
        +coeff[ 90]*x11*x21    *x42    
        +coeff[ 91]*x11    *x31*x42    
        +coeff[ 92]*x11    *x32*x41    
        +coeff[ 93]*x11*x22*x31        
        +coeff[ 94]*x11*x21    *x41*x51
        +coeff[ 95]*x11*x21*x31    *x51
        +coeff[ 96]    *x21    *x43*x51
        ;

    return v_y_e_dext_175                                ;
}
float p_e_dext_175                                (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.6611996E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.64122076E-04,-0.24230950E-01, 0.51523107E-02,-0.12750329E-01,
         0.42323312E-02, 0.88014575E-02,-0.77818856E-02,-0.23584466E-02,
         0.56470954E-03, 0.93232404E-04,-0.45924739E-05,-0.46793474E-02,
        -0.23196060E-02, 0.40066475E-03,-0.21914684E-02,-0.99987362E-03,
         0.18018167E-02,-0.41649306E-04, 0.17503968E-03,-0.32249742E-03,
        -0.11667920E-02,-0.21118649E-03, 0.41319826E-03,-0.32310307E-03,
        -0.97930117E-03, 0.91258733E-03, 0.96452184E-03,-0.43538364E-03,
         0.53579523E-03, 0.63893240E-04,-0.74285861E-04, 0.58681291E-03,
         0.44490467E-03,-0.11600564E-03,-0.29839555E-03, 0.24144957E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_175                                =v_p_e_dext_175                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dext_175                                =v_p_e_dext_175                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23    *x41*x51
    ;
    v_p_e_dext_175                                =v_p_e_dext_175                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x24    *x41    
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dext_175                                =v_p_e_dext_175                                
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dext_175                                ;
}
float l_e_dext_175                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.2945258E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.24525536E-01,-0.50642574E+00,-0.79058051E-01, 0.19388741E-01,
        -0.33332922E-01,-0.35537228E-01, 0.21613773E-02, 0.20705335E-01,
         0.13703052E-01, 0.13422169E-02,-0.47947499E-02,-0.30772742E-02,
         0.12801868E-02, 0.75072213E-02, 0.42759479E-03,-0.57126262E-03,
        -0.87048399E-03,-0.14313631E-02, 0.11851747E-02,-0.16633115E-02,
         0.11749665E-02, 0.80739311E-03, 0.89139276E-03, 0.14687214E-02,
         0.43597622E-02, 0.20019282E-01, 0.75167776E-02, 0.93703521E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_175                                =v_l_e_dext_175                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x34        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_dext_175                                =v_l_e_dext_175                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x23        *x52
        +coeff[ 19]        *x32        
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x23*x31*x43    
    ;
    v_l_e_dext_175                                =v_l_e_dext_175                                
        +coeff[ 26]    *x23    *x44    
        +coeff[ 27]    *x21*x32*x44    
        ;

    return v_l_e_dext_175                                ;
}
float x_e_dext_150                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1315446E-01;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.58781998E-02,-0.14903959E-01, 0.10727511E+00, 0.36023390E+00,
         0.35757806E-01, 0.10928053E-01,-0.22262242E-02,-0.41709817E-02,
        -0.13806730E-01,-0.11386453E-01,-0.83234039E-06,-0.18884657E-02,
        -0.25782245E-02,-0.57227877E-02,-0.66011678E-03,-0.12004630E-02,
        -0.52108988E-03, 0.30453649E-03,-0.53189660E-03,-0.43101871E-03,
        -0.72431081E-03,-0.94464151E-02,-0.58815009E-02,-0.40921965E-02,
        -0.56578177E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_150                                =v_x_e_dext_150                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_150                                =v_x_e_dext_150                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_150                                ;
}
float t_e_dext_150                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5855562E+00;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.44792551E-02,-0.10656396E+00, 0.24574708E-01,-0.62797521E-02,
         0.86758155E-06, 0.33726250E-02, 0.22723107E-02, 0.35546464E-02,
        -0.33322009E-03,-0.57135202E-03,-0.16440655E-02,-0.77658007E-03,
         0.23398169E-02,-0.13523364E-02,-0.53501892E-03,-0.96777419E-03,
        -0.21015119E-03, 0.20859107E-03, 0.53707539E-03, 0.17318489E-03,
         0.49829890E-03, 0.41761287E-03,-0.31382823E-03, 0.64625533E-03,
         0.17881354E-03, 0.10635355E-03, 0.70337503E-03,-0.22417787E-04,
         0.52428473E-03, 0.14806646E-02,-0.29326075E-05,-0.12287052E-04,
        -0.28020047E-03, 0.70974056E-04,-0.14532570E-03, 0.59606723E-03,
         0.14815337E-03,-0.29511924E-03,-0.27978153E-03, 0.78839361E-03,
         0.15553134E-02, 0.12074517E-02, 0.91852254E-03, 0.16923829E-03,
        -0.22035980E-03,-0.13312961E-03, 0.55820830E-04, 0.36108337E-04,
        -0.86343665E-04, 0.14350332E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_150                                =v_t_e_dext_150                                
        +coeff[  8]        *x32*x42    
        +coeff[  9]    *x22            
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dext_150                                =v_t_e_dext_150                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_dext_150                                =v_t_e_dext_150                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x21*x33*x41*x51
        +coeff[ 32]    *x23            
        +coeff[ 33]*x11    *x31*x41    
        +coeff[ 34]        *x33*x41    
    ;
    v_t_e_dext_150                                =v_t_e_dext_150                                
        +coeff[ 35]    *x21*x31*x41*x51
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]        *x31*x41*x52
        +coeff[ 38]            *x42*x52
        +coeff[ 39]    *x23*x32        
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]    *x22*x32    *x51
    ;
    v_t_e_dext_150                                =v_t_e_dext_150                                
        +coeff[ 44]    *x23        *x52
        +coeff[ 45]    *x22*x32    *x52
        +coeff[ 46]*x11    *x32        
        +coeff[ 47]                *x53
        +coeff[ 48]        *x32    *x52
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_dext_150                                ;
}
float y_e_dext_150                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1569766E-03;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.11670030E-03, 0.98350808E-01,-0.70581675E-01,-0.54205384E-01,
        -0.23496687E-01, 0.38528390E-01, 0.22134433E-01,-0.27289469E-01,
        -0.13504108E-01, 0.18299049E-02,-0.81971008E-02,-0.40929914E-02,
        -0.23163569E-02,-0.70846720E-04,-0.50662435E-03, 0.82384713E-03,
        -0.54255710E-02,-0.12261937E-02, 0.12472416E-02,-0.21426429E-02,
        -0.15431928E-02,-0.44331681E-02, 0.55580377E-03, 0.23719305E-02,
        -0.23108197E-02,-0.98723837E-03, 0.82925364E-03, 0.10900781E-02,
        -0.39138924E-02, 0.11759863E-02, 0.18981188E-02,-0.25716496E-02,
        -0.89161480E-02,-0.23226358E-02,-0.44163456E-03, 0.54744649E-03,
         0.10057664E-02, 0.26882812E-03, 0.65547426E-03,-0.20200682E-02,
         0.41539638E-03, 0.23268627E-03,-0.25657064E-02,-0.22753801E-02,
        -0.38046627E-02, 0.22142032E-04,-0.15401729E-04, 0.22151384E-03,
        -0.97894255E-04, 0.37921363E-03,-0.30597209E-03, 0.16936094E-03,
        -0.34274769E-03,-0.11783647E-02,-0.11438385E-02,-0.79234684E-03,
         0.26565508E-03,-0.76645254E-02,-0.40796727E-04,-0.90436515E-04,
         0.37826170E-03, 0.93978822E-04, 0.57055033E-03, 0.83780760E-04,
         0.25602503E-03, 0.16135896E-03, 0.28343929E-02,-0.49964176E-02,
        -0.13685009E-02,-0.30751739E-03, 0.13823062E-04,-0.18922150E-04,
        -0.22599343E-04,-0.38432237E-04, 0.88153836E-04, 0.28292944E-02,
         0.17359637E-02, 0.36629427E-02, 0.22852283E-02,-0.72983945E-04,
         0.11346008E-03, 0.72321028E-03,-0.31573541E-03, 0.38031439E-03,
        -0.17662733E-03,-0.58826485E-04, 0.81295824E-04,-0.32700080E-03,
        -0.59661493E-05, 0.80959608E-05, 0.19711601E-04, 0.28721523E-04,
        -0.23753008E-04,-0.61040126E-04,-0.95403484E-04,-0.13689988E-03,
        -0.54204149E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 44]    *x24*x31*x42    
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]        *x33*x42    
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 53]    *x24    *x41    
        +coeff[ 54]    *x24*x31        
        +coeff[ 55]    *x22*x33        
        +coeff[ 56]    *x23    *x41*x51
        +coeff[ 57]    *x22*x31*x44    
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11*x22    *x41    
        +coeff[ 60]*x11*x21    *x43    
        +coeff[ 61]            *x41*x53
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]        *x31    *x53
        +coeff[ 64]*x11*x21*x32*x41    
        +coeff[ 65]    *x22    *x41*x52
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]    *x22*x33*x42    
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]    *x23    *x45    
        +coeff[ 70]    *x22            
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x11*x21    *x41*x51
        +coeff[ 74]*x11*x23    *x41    
        +coeff[ 75]    *x21*x31*x44    
        +coeff[ 76]    *x23    *x43    
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]    *x21*x33*x42    
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 80]        *x32*x43*x51
        +coeff[ 81]    *x21*x34*x41    
        +coeff[ 82]    *x22*x31*x42*x51
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]    *x21*x34*x41*x51
        +coeff[ 85]    *x21*x31*x42*x53
        +coeff[ 86]    *x24*x31    *x52
        +coeff[ 87]    *x24    *x43*x51
        +coeff[ 88]            *x42    
    ;
    v_y_e_dext_150                                =v_y_e_dext_150                                
        +coeff[ 89]        *x31*x41    
        +coeff[ 90]            *x44    
        +coeff[ 91]*x11    *x31*x42    
        +coeff[ 92]*x11*x21*x31    *x51
        +coeff[ 93]    *x21    *x43*x51
        +coeff[ 94]        *x34*x41    
        +coeff[ 95]    *x21*x31*x42*x51
        +coeff[ 96]            *x43*x52
        ;

    return v_y_e_dext_150                                ;
}
float p_e_dext_150                                (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.4005509E-04;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.40120227E-04,-0.24542768E-01, 0.47341026E-02,-0.12838003E-01,
         0.42140130E-02, 0.87699471E-02,-0.76210247E-02,-0.23561551E-02,
         0.44554166E-03, 0.96327793E-04, 0.11251716E-03,-0.12239491E-03,
        -0.46532527E-02,-0.21811521E-02, 0.40215973E-03,-0.21671592E-02,
        -0.99527952E-03, 0.18160199E-02,-0.35793149E-04, 0.17256070E-03,
        -0.32065151E-03,-0.11541590E-02,-0.20403594E-03, 0.40429612E-03,
        -0.31865542E-03,-0.94059011E-03, 0.90679026E-03, 0.95584121E-03,
        -0.43898931E-03, 0.63431340E-04,-0.69526264E-04, 0.59214869E-03,
         0.42008029E-03,-0.12952408E-03, 0.45514910E-03,-0.28115200E-03,
         0.24998077E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x26 = x25*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_150                                =v_p_e_dext_150                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x26*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dext_150                                =v_p_e_dext_150                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]            *x41*x52
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_p_e_dext_150                                =v_p_e_dext_150                                
        +coeff[ 26]    *x23    *x41*x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_dext_150                                =v_p_e_dext_150                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dext_150                                ;
}
float l_e_dext_150                                (float *x,int m){
    int ncoeff= 26;
    float avdat= -0.2882599E-01;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
         0.18959338E-01,-0.50676847E+00,-0.79187490E-01, 0.19314649E-01,
        -0.34356393E-01,-0.35061810E-01,-0.47915657E-02, 0.21639457E-02,
         0.18063124E-01, 0.13737356E-01, 0.21949697E-02,-0.20590005E-02,
        -0.29644177E-02, 0.13721704E-02,-0.91707322E-03, 0.72573829E-02,
        -0.17994862E-02, 0.14409532E-02,-0.10192066E-02, 0.12028352E-02,
         0.10251339E-02, 0.69403247E-03, 0.16787386E-02, 0.16213986E-02,
         0.90529295E-02, 0.73478352E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_dext_150                                =v_l_e_dext_150                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]        *x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]                *x52
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dext_150                                =v_l_e_dext_150                                
        +coeff[ 17]    *x23        *x52
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x24            
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
        ;

    return v_l_e_dext_150                                ;
}
float x_e_dext_125                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.1202404E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.40603215E-02,-0.14736953E-01, 0.10732591E+00, 0.36527017E+00,
         0.35705272E-01, 0.11153358E-01,-0.22160253E-02,-0.41480921E-02,
        -0.13838870E-01,-0.11490799E-01,-0.25131891E-03,-0.17252652E-02,
        -0.25518509E-02,-0.57033645E-02,-0.63836086E-03,-0.12362815E-02,
        -0.50329324E-03, 0.29823315E-03,-0.52742683E-03,-0.46761287E-03,
        -0.69964759E-03,-0.90159429E-02,-0.54314723E-02,-0.44360189E-02,
        -0.60246224E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_125                                =v_x_e_dext_125                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x21            
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dext_125                                =v_x_e_dext_125                                
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dext_125                                ;
}
float t_e_dext_125                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5863947E+00;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.34433582E-02, 0.33305970E-02,-0.10780656E+00, 0.24560245E-01,
        -0.62271450E-02, 0.22931560E-02, 0.34618471E-02,-0.25211126E-03,
        -0.60324161E-03,-0.15730178E-02,-0.77563239E-03, 0.15139598E-02,
        -0.13072903E-02,-0.56788331E-03,-0.10193913E-02,-0.20933108E-03,
         0.39622701E-04, 0.53890015E-03, 0.17521453E-03, 0.56160800E-03,
         0.38153547E-03,-0.35386387E-03, 0.55119814E-03,-0.24470120E-04,
         0.18471979E-03, 0.11535718E-03, 0.69614750E-03,-0.29789544E-04,
         0.60295867E-03, 0.64754207E-03, 0.68814828E-04,-0.15181501E-03,
         0.11403168E-03,-0.28498931E-03,-0.25522243E-03, 0.15124868E-02,
         0.24000825E-02, 0.19050884E-02, 0.22062968E-03,-0.19261893E-03,
        -0.36058249E-03, 0.63583393E-04, 0.53519125E-04,-0.11435927E-03,
         0.99268358E-03, 0.18030046E-03, 0.17459430E-02, 0.12374874E-02,
         0.12170737E-03,-0.81924718E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]        *x32*x42    
    ;
    v_t_e_dext_125                                =v_t_e_dext_125                                
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]            *x42*x51
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dext_125                                =v_t_e_dext_125                                
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x22        *x53
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_dext_125                                =v_t_e_dext_125                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x11    *x31*x41    
        +coeff[ 31]        *x33*x41    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_dext_125                                =v_t_e_dext_125                                
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x23            
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]                *x53
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_dext_125                                =v_t_e_dext_125                                
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x21*x32    *x52
        +coeff[ 49]*x12                
        ;

    return v_t_e_dext_125                                ;
}
float y_e_dext_125                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1751721E-03;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.91282804E-04, 0.95923580E-01,-0.72539940E-01,-0.54619756E-01,
        -0.23501599E-01, 0.38457438E-01, 0.22093199E-01,-0.27259978E-01,
        -0.13623644E-01, 0.18016720E-02,-0.81240907E-02,-0.40486874E-02,
        -0.23114162E-02,-0.16995771E-03,-0.35662766E-03, 0.80692669E-03,
        -0.53581437E-02,-0.12145131E-02, 0.12586679E-02,-0.21843924E-02,
        -0.15497803E-02,-0.42793774E-02, 0.54871320E-03, 0.24506534E-02,
        -0.23079745E-02,-0.99593436E-03, 0.48345505E-03, 0.10460799E-02,
        -0.36281352E-02,-0.39304588E-02,-0.28209391E-02, 0.10059207E-02,
         0.38248447E-02, 0.75821055E-03, 0.42360014E-04, 0.51081512E-03,
         0.98013948E-03, 0.23448575E-03, 0.67981647E-03, 0.40789467E-03,
        -0.17881636E-02,-0.11506893E-02,-0.79728960E-03,-0.29838806E-04,
         0.31935639E-03,-0.97405202E-04, 0.40823480E-03,-0.22325237E-03,
         0.15563193E-03, 0.24196433E-03, 0.24533615E-03, 0.23555132E-04,
        -0.93607960E-04,-0.25589325E-03, 0.36793636E-03, 0.11669492E-03,
         0.57931134E-03,-0.28655608E-03, 0.85363361E-04, 0.24104890E-03,
         0.19778741E-03, 0.30611446E-02,-0.83610350E-02,-0.94538396E-02,
         0.45359958E-03,-0.18974275E-02, 0.25059775E-04,-0.27783926E-04,
        -0.18811546E-04,-0.19159512E-04,-0.42693067E-04,-0.92683338E-04,
         0.70407434E-04,-0.18672150E-03, 0.31601063E-02, 0.14665762E-02,
         0.25723793E-02, 0.79207879E-04,-0.25491521E-03, 0.17536581E-02,
         0.38528444E-04, 0.40355232E-03,-0.13998115E-02,-0.36738846E-02,
        -0.50886618E-02,-0.20285230E-02,-0.12956837E-02,-0.44718737E-03,
        -0.26805309E-03, 0.35928195E-04, 0.77507102E-05,-0.34156365E-04,
         0.37530626E-04,-0.11322759E-04,-0.19187781E-04,-0.84665546E-04,
        -0.27377346E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x21    *x45    
        +coeff[ 32]    *x21*x32*x43    
        +coeff[ 33]    *x21*x34*x41    
        +coeff[ 34]    *x21            
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]                *x51
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 44]    *x21    *x43    
        +coeff[ 45]*x11        *x41*x51
        +coeff[ 46]            *x43*x51
        +coeff[ 47]        *x31*x44    
        +coeff[ 48]        *x33    *x51
        +coeff[ 49]    *x21*x31    *x52
        +coeff[ 50]    *x23    *x41*x51
        +coeff[ 51]    *x22            
        +coeff[ 52]*x11*x22    *x41    
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]    *x21*x31*x42*x51
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x22    *x41*x52
        +coeff[ 61]    *x23*x31*x42    
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 62]    *x22*x31*x44    
        +coeff[ 63]    *x22*x32*x43    
        +coeff[ 64]    *x23*x32*x43    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]        *x31*x41    
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]*x12    *x31        
        +coeff[ 69]*x11    *x31    *x51
        +coeff[ 70]*x11*x21    *x41*x51
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 71]        *x34*x41    
        +coeff[ 72]*x11*x23    *x41    
        +coeff[ 73]    *x21*x32*x41*x51
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]    *x23    *x43    
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22*x31    *x52
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]    *x23*x32*x41    
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 80]    *x23    *x42*x51
        +coeff[ 81]    *x23*x33        
        +coeff[ 82]    *x22    *x45    
        +coeff[ 83]    *x24*x31*x42    
        +coeff[ 84]    *x22*x33*x42    
        +coeff[ 85]    *x24*x32*x41    
        +coeff[ 86]    *x22*x34*x41    
        +coeff[ 87]    *x24*x33        
        +coeff[ 88]    *x24*x31*x42*x51
    ;
    v_y_e_dext_125                                =v_y_e_dext_125                                
        +coeff[ 89]            *x42    
        +coeff[ 90]        *x32        
        +coeff[ 91]            *x44    
        +coeff[ 92]*x11    *x31*x42    
        +coeff[ 93]*x12        *x42    
        +coeff[ 94]*x11*x21*x31    *x51
        +coeff[ 95]    *x21    *x43*x51
        +coeff[ 96]        *x31*x45    
        ;

    return v_y_e_dext_125                                ;
}
float p_e_dext_125                                (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2426293E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.26090747E-04,-0.24889715E-01, 0.42958171E-02,-0.12903692E-01,
         0.41960902E-02, 0.87417355E-02,-0.77561853E-02,-0.23330005E-02,
         0.53548650E-03, 0.89639747E-04, 0.13063847E-05,-0.46494682E-02,
        -0.22262179E-02, 0.39602138E-03,-0.21436515E-02,-0.99228043E-03,
         0.18478148E-02,-0.44504057E-04, 0.17049949E-03,-0.31918433E-03,
        -0.11273754E-02, 0.40041594E-03,-0.31531430E-03,-0.95738011E-03,
        -0.12515520E-03, 0.79452089E-04, 0.91598765E-03, 0.52854256E-03,
         0.90237311E-03,-0.12490911E-03, 0.58327329E-04,-0.65996450E-04,
         0.54601143E-03, 0.40054164E-03,-0.50308934E-03,-0.12582832E-03,
        -0.28799023E-03, 0.24549160E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_125                                =v_p_e_dext_125                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dext_125                                =v_p_e_dext_125                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]            *x41*x52
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23*x31    *x51
        +coeff[ 25]    *x24*x31    *x51
    ;
    v_p_e_dext_125                                =v_p_e_dext_125                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x24    *x41    
        +coeff[ 28]    *x23    *x41*x51
        +coeff[ 29]    *x21*x31    *x51
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_p_e_dext_125                                =v_p_e_dext_125                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dext_125                                ;
}
float l_e_dext_125                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2677377E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.16034488E-01,-0.51374984E+00,-0.79317935E-01, 0.19103879E-01,
        -0.35219632E-01,-0.35006180E-01,-0.45236568E-02, 0.20936492E-02,
         0.13905182E-01, 0.12810289E-01, 0.27991212E-02,-0.21427667E-02,
        -0.28160680E-02, 0.11083644E-02,-0.95699087E-03,-0.11945112E-02,
         0.67728404E-02,-0.14643051E-02, 0.11915793E-02, 0.12749860E-02,
         0.78532303E-03, 0.72127674E-03, 0.17314954E-02,-0.10286147E-02,
         0.13128348E-01, 0.10234311E-01, 0.10802833E-01, 0.23876222E-03,
         0.10354635E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_dext_125                                =v_l_e_dext_125                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]        *x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]                *x52
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dext_125                                =v_l_e_dext_125                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x24            
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dext_125                                =v_l_e_dext_125                                
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x23*x32*x42    
        +coeff[ 28]    *x21*x32*x44    
        ;

    return v_l_e_dext_125                                ;
}
float x_e_dext_100                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.1207331E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.30081954E-02,-0.14488872E-01, 0.10740171E+00, 0.37055698E+00,
         0.35440829E-01, 0.11478952E-01,-0.22213864E-02,-0.40998333E-02,
        -0.13540470E-01,-0.11236615E-01,-0.13331651E-03,-0.18969941E-02,
        -0.25670778E-02,-0.55224933E-02,-0.24158023E-03, 0.32209916E-03,
        -0.65969606E-03,-0.12060810E-02,-0.25587113E-03,-0.56482863E-03,
        -0.43660428E-03,-0.68591267E-03,-0.89100515E-02,-0.55880626E-02,
        -0.40735840E-02,-0.53418311E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dext_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dext_100                                =v_x_e_dext_100                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]*x11*x21            
    ;
    v_x_e_dext_100                                =v_x_e_dext_100                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x32*x42    
        ;

    return v_x_e_dext_100                                ;
}
float t_e_dext_100                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5863549E+00;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.31572646E-02,-0.10905015E+00, 0.24539914E-01,-0.61071361E-02,
        -0.41746912E-05, 0.32715185E-02, 0.22867145E-02, 0.33536123E-02,
        -0.12295383E-03,-0.66635903E-03,-0.14614562E-02, 0.13901476E-02,
        -0.12415845E-02,-0.57904760E-03,-0.10926470E-02,-0.20945881E-03,
        -0.77253202E-03, 0.62281024E-05, 0.61223743E-03, 0.17656341E-03,
         0.61632233E-03, 0.42757372E-03,-0.32481999E-03, 0.56330336E-03,
         0.16926731E-03, 0.18838492E-03, 0.67764439E-03,-0.35699966E-04,
         0.54650818E-03, 0.49919716E-04, 0.32724733E-04,-0.42226966E-03,
         0.11079489E-03,-0.24546261E-03, 0.55127597E-03, 0.11085717E-03,
        -0.27541479E-03,-0.25810272E-03, 0.84280500E-05, 0.99641655E-03,
         0.15336272E-02, 0.23277851E-02, 0.17742679E-02,-0.22663543E-03,
         0.55582132E-04,-0.30442003E-04, 0.34800927E-04,-0.14345109E-03,
         0.17197853E-02, 0.11495574E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dext_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_dext_100                                =v_t_e_dext_100                                
        +coeff[  8]        *x32    *x52
        +coeff[  9]    *x22            
        +coeff[ 10]            *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]            *x42*x51
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]                *x52
    ;
    v_t_e_dext_100                                =v_t_e_dext_100                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_dext_100                                =v_t_e_dext_100                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]*x12*x21*x31*x41*x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]    *x23            
        +coeff[ 32]*x11    *x31*x41    
        +coeff[ 33]        *x32*x42    
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dext_100                                =v_t_e_dext_100                                
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]            *x42*x52
        +coeff[ 38]*x13    *x32        
        +coeff[ 39]    *x23*x32        
        +coeff[ 40]    *x23    *x42    
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]    *x21*x31*x43    
        +coeff[ 43]    *x23        *x52
    ;
    v_t_e_dext_100                                =v_t_e_dext_100                                
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]*x11*x21        *x51
        +coeff[ 46]                *x53
        +coeff[ 47]        *x33*x41    
        +coeff[ 48]    *x23*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dext_100                                ;
}
float y_e_dext_100                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7126359E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.65645267E-03, 0.92509933E-01,-0.75487643E-01,-0.54965533E-01,
        -0.23430685E-01, 0.38404506E-01, 0.22024155E-01,-0.27565900E-01,
        -0.13688595E-01, 0.17572349E-02,-0.80232071E-02,-0.40782243E-02,
        -0.23100975E-02,-0.20777802E-03,-0.36993969E-03, 0.78552915E-03,
        -0.53169793E-02,-0.11922907E-02, 0.12325021E-02,-0.21504101E-02,
        -0.15349041E-02,-0.42064753E-02, 0.54332201E-03, 0.24326127E-02,
        -0.20569300E-02,-0.94196742E-03, 0.71900763E-03, 0.10346068E-02,
        -0.36468005E-02,-0.35708733E-02,-0.23883372E-02, 0.10017147E-02,
         0.35133613E-02, 0.65409153E-03, 0.50596159E-03, 0.10107402E-02,
         0.21220816E-03, 0.66421519E-03, 0.41157607E-03,-0.17596724E-02,
        -0.10711688E-02,-0.69055025E-03, 0.32371150E-04,-0.21371021E-04,
         0.25938207E-03,-0.94000789E-04, 0.43866411E-03,-0.98525801E-04,
        -0.20786894E-03, 0.15671698E-03,-0.33337079E-03, 0.23874985E-03,
         0.28238565E-03, 0.16612510E-04,-0.29080724E-04, 0.39740151E-03,
         0.10509235E-03, 0.56981714E-03, 0.87779532E-04, 0.24264742E-03,
         0.14569066E-03,-0.84341159E-02,-0.94959512E-02,-0.24703869E-02,
        -0.19766188E-04,-0.19049807E-04, 0.44015764E-04,-0.23194109E-04,
        -0.45234912E-04,-0.27723971E-04,-0.55153741E-04,-0.16870192E-03,
         0.66153196E-04,-0.11007165E-03, 0.27481499E-02, 0.14140690E-02,
         0.25848905E-02, 0.21186084E-02, 0.63059961E-04,-0.40586060E-03,
         0.17361881E-02,-0.38695618E-03, 0.41526469E-03,-0.10683079E-02,
        -0.21330400E-03, 0.18604516E-03,-0.41382541E-02,-0.52109705E-02,
        -0.24399576E-02,-0.14741118E-02,-0.57173410E-03, 0.64396896E-04,
        -0.19487416E-05, 0.13045370E-04, 0.65760892E-05,-0.40052410E-05,
        -0.92277787E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dext_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x21    *x45    
        +coeff[ 32]    *x21*x32*x43    
        +coeff[ 33]    *x21*x34*x41    
        +coeff[ 34]    *x21*x32*x41    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 44]    *x21    *x43    
        +coeff[ 45]*x11        *x41*x51
        +coeff[ 46]            *x43*x51
        +coeff[ 47]*x11*x22    *x41    
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]        *x33    *x51
        +coeff[ 50]        *x33*x42    
        +coeff[ 51]    *x21*x31    *x52
        +coeff[ 52]    *x23    *x41*x51
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 53]    *x22            
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x22    *x41*x52
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]*x12    *x31        
        +coeff[ 65]*x11    *x31    *x51
        +coeff[ 66]*x11    *x31*x42    
        +coeff[ 67]*x11*x22*x31        
        +coeff[ 68]*x11*x21    *x41*x51
        +coeff[ 69]*x11*x21*x31    *x51
        +coeff[ 70]        *x34*x41    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 71]    *x21*x31*x42*x51
        +coeff[ 72]*x11*x23    *x41    
        +coeff[ 73]    *x21*x32*x41*x51
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]    *x23    *x43    
        +coeff[ 76]    *x23*x31*x42    
        +coeff[ 77]    *x21*x33*x42    
        +coeff[ 78]    *x22*x31    *x52
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 80]    *x23*x32*x41    
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]    *x24    *x41*x51
        +coeff[ 85]        *x33*x44    
        +coeff[ 86]    *x24*x31*x42    
        +coeff[ 87]    *x22*x33*x42    
        +coeff[ 88]    *x24*x32*x41    
    ;
    v_y_e_dext_100                                =v_y_e_dext_100                                
        +coeff[ 89]    *x22*x34*x41    
        +coeff[ 90]    *x24*x33        
        +coeff[ 91]    *x24    *x44    
        +coeff[ 92]*x11                
        +coeff[ 93]        *x31*x41    
        +coeff[ 94]        *x32        
        +coeff[ 95]*x11*x21            
        +coeff[ 96]            *x42*x51
        ;

    return v_y_e_dext_100                                ;
}
float p_e_dext_100                                (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.5021589E-04;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.49155158E-04,-0.25445655E-01,-0.12777186E-01, 0.41568587E-02,
         0.86953072E-02,-0.11632264E-02,-0.96673128E-03,-0.22865334E-02,
         0.41873613E-03,-0.65474276E-04, 0.41926935E-03,-0.46705967E-03,
         0.29240331E-04, 0.36454007E-02,-0.44714110E-02,-0.20976800E-02,
        -0.77066277E-02,-0.20109660E-02, 0.38613085E-03, 0.17854082E-02,
         0.16292585E-03,-0.31936719E-03, 0.40031300E-03,-0.30932078E-03,
        -0.91047998E-03,-0.28847440E-03,-0.44708030E-03, 0.82635501E-03,
         0.52930282E-04,-0.64329339E-04, 0.50589157E-03, 0.17743539E-03,
        -0.13759659E-03,-0.59078255E-03, 0.24755887E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dext_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x21    *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x32*x41    
        +coeff[  6]            *x43    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dext_100                                =v_p_e_dext_100                                
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24    *x41    
        +coeff[ 11]    *x22    *x43    
        +coeff[ 12]    *x23*x33        
        +coeff[ 13]            *x41    
        +coeff[ 14]    *x21*x31        
        +coeff[ 15]    *x22*x31        
        +coeff[ 16]    *x22    *x41    
    ;
    v_p_e_dext_100                                =v_p_e_dext_100                                
        +coeff[ 17]        *x31*x42    
        +coeff[ 18]*x11        *x41    
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]            *x41*x52
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23*x31    *x51
    ;
    v_p_e_dext_100                                =v_p_e_dext_100                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x41*x52
        ;

    return v_p_e_dext_100                                ;
}
float l_e_dext_100                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2683797E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59991E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.14533615E-01,-0.52080792E+00,-0.79500861E-01, 0.18801669E-01,
        -0.35927992E-01,-0.34497302E-01, 0.22571622E-02, 0.15015054E-01,
         0.13522419E-01, 0.17595147E-02,-0.48778607E-02,-0.28572562E-02,
         0.11630466E-02,-0.13712146E-02, 0.69944281E-02, 0.33458203E-03,
         0.95947442E-04,-0.22294517E-02,-0.83658751E-03,-0.13751604E-02,
         0.12468992E-02, 0.12130667E-02, 0.10770587E-02, 0.66335930E-03,
         0.16534490E-02, 0.21023694E-02, 0.11581814E-01, 0.80064787E-02,
         0.55206195E-02, 0.71086800E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dext_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x21*x31*x41    
    ;
    v_l_e_dext_100                                =v_l_e_dext_100                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]            *x42    
        +coeff[ 12]                *x52
        +coeff[ 13]    *x23            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x34        
    ;
    v_l_e_dext_100                                =v_l_e_dext_100                                
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dext_100                                =v_l_e_dext_100                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x23*x32*x42    
        ;

    return v_l_e_dext_100                                ;
}
