float x_e_q3en_1200                                (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1775255E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.11924053E-03,-0.10848817E-01, 0.11103605E+00, 0.21904238E+00,
         0.28437216E-01, 0.16964052E-01,-0.37814986E-02,-0.37462257E-02,
        -0.88087087E-02,-0.91078514E-02,-0.11749682E-02,-0.20510170E-02,
         0.21809104E-02,-0.42517805E-02,-0.92351122E-03,-0.46600000E-03,
        -0.67307847E-03, 0.16435722E-03,-0.11178443E-02,-0.65198736E-02,
        -0.40268684E-02,-0.35041524E-02, 0.63798932E-03,-0.48918091E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_1200                                =v_x_e_q3en_1200                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11*x22            
        +coeff[ 16]            *x42*x51
    ;
    v_x_e_q3en_1200                                =v_x_e_q3en_1200                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_q3en_1200                                ;
}
float t_e_q3en_1200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4070926E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.34054830E-02, 0.26843252E-02,-0.76304801E-01, 0.18239599E-01,
        -0.51177046E-02,-0.41367579E-02,-0.91966544E-03, 0.46040968E-03,
        -0.76833507E-03, 0.17235475E-02, 0.75967924E-03, 0.79343602E-03,
         0.34665957E-03,-0.16749815E-03, 0.52733551E-03, 0.33770781E-03,
         0.28101736E-03, 0.53099662E-04,-0.10300241E-03,-0.28820953E-03,
         0.80904306E-03, 0.13044830E-03,-0.12754409E-03, 0.39410460E-03,
         0.49039454E-03,-0.29340325E-03, 0.61126033E-04,-0.46414795E-03,
        -0.39772372E-03, 0.20281740E-02, 0.17740723E-02, 0.15864606E-02,
         0.66526256E-04,-0.47302645E-04, 0.69365182E-04, 0.11501830E-03,
         0.15519148E-03, 0.16296902E-02, 0.27392784E-03,-0.20543551E-03,
        -0.13074263E-04, 0.78566176E-04, 0.52718773E-04, 0.51421044E-04,
        -0.32395626E-04, 0.56624605E-04, 0.51228715E-04, 0.74962043E-03,
         0.10034223E-03,-0.18427554E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_1200                                =v_t_e_q3en_1200                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_1200                                =v_t_e_q3en_1200                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_1200                                =v_t_e_q3en_1200                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]                *x53
    ;
    v_t_e_q3en_1200                                =v_t_e_q3en_1200                                
        +coeff[ 35]        *x32*x42    
        +coeff[ 36]*x11*x22*x31*x41    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x12                
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_q3en_1200                                =v_t_e_q3en_1200                                
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_q3en_1200                                ;
}
float y_e_q3en_1200                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2930714E-03;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.23654374E-03, 0.11582070E+00,-0.77911869E-01,-0.66407792E-01,
        -0.22734722E-01, 0.48053890E-01, 0.25639074E-01,-0.31755131E-01,
        -0.13083687E-01,-0.71941637E-02, 0.24512121E-02,-0.96771717E-02,
        -0.22670617E-02,-0.31061713E-02,-0.13682483E-03,-0.51751238E-03,
         0.88016212E-03,-0.56066373E-02,-0.11800401E-02, 0.16701642E-02,
        -0.17582752E-02, 0.54163369E-02,-0.56550386E-02, 0.54050091E-03,
         0.16854758E-02,-0.21834329E-02,-0.13338898E-02, 0.21708191E-02,
         0.19964450E-02, 0.10388241E-02,-0.38983442E-02,-0.48006922E-02,
        -0.32734328E-02,-0.31983212E-03, 0.10362060E-02, 0.27959089E-03,
        -0.36497341E-03, 0.68014092E-03, 0.33504926E-03,-0.17707963E-02,
        -0.15000053E-02, 0.12880734E-02, 0.24196424E-03, 0.34084627E-04,
        -0.26279185E-04,-0.35196909E-03, 0.17627807E-03,-0.80814818E-03,
         0.53229328E-03, 0.16290125E-04,-0.46831799E-04,-0.61919069E-04,
         0.38351375E-03,-0.65986242E-04,-0.33854615E-03,-0.15063498E-03,
         0.12606139E-03, 0.65168343E-03, 0.22747433E-03, 0.33460751E-02,
         0.20643603E-02, 0.44728711E-03,-0.87669156E-02, 0.65439235E-04,
        -0.89513520E-02, 0.10243002E-03,-0.20932800E-02,-0.18154955E-04,
        -0.23908855E-04,-0.36936181E-04, 0.28297162E-03, 0.46021451E-04,
         0.22766220E-03, 0.29491610E-02,-0.53728250E-04, 0.31829400E-02,
         0.31681583E-03, 0.19406582E-02, 0.17855313E-03, 0.61503117E-03,
        -0.19522695E-02,-0.49108587E-03,-0.16532085E-04,-0.28752752E-02,
        -0.45394925E-02, 0.15332669E-03,-0.13688672E-02,-0.10822287E-02,
         0.19210026E-02,-0.42997987E-03,-0.10298117E-03,-0.12372792E-03,
        -0.51865343E-03, 0.19350382E-04,-0.16845948E-04, 0.19088811E-04,
         0.25436533E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 44]                *x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 53]*x11*x21    *x41*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]    *x23    *x43    
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]    *x23*x33        
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 62]    *x22*x31*x44    
        +coeff[ 63]*x11*x21    *x45    
        +coeff[ 64]    *x22*x32*x43    
        +coeff[ 65]*x11*x21*x32*x43    
        +coeff[ 66]    *x24    *x45    
        +coeff[ 67]*x12    *x31        
        +coeff[ 68]*x11    *x31    *x51
        +coeff[ 69]*x11*x22*x31        
        +coeff[ 70]*x11*x21    *x43    
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 71]        *x31    *x53
        +coeff[ 72]*x11*x21*x32*x41    
        +coeff[ 73]    *x21*x31*x44    
        +coeff[ 74]*x11*x23*x31        
        +coeff[ 75]    *x21*x32*x43    
        +coeff[ 76]        *x31*x44*x51
        +coeff[ 77]    *x21*x33*x42    
        +coeff[ 78]        *x32*x43*x51
        +coeff[ 79]    *x21*x34*x41    
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 80]    *x22    *x45    
        +coeff[ 81]    *x24    *x41*x51
        +coeff[ 82]    *x21    *x45*x51
        +coeff[ 83]    *x24*x31*x42    
        +coeff[ 84]    *x22*x33*x42    
        +coeff[ 85]*x11*x23    *x43    
        +coeff[ 86]    *x24*x32*x41    
        +coeff[ 87]    *x22*x34*x41    
        +coeff[ 88]    *x23    *x45    
    ;
    v_y_e_q3en_1200                                =v_y_e_q3en_1200                                
        +coeff[ 89]    *x22    *x45*x51
        +coeff[ 90]    *x22*x32*x41*x52
        +coeff[ 91]    *x22*x31*x44*x51
        +coeff[ 92]    *x24*x31*x42*x51
        +coeff[ 93]        *x31*x41    
        +coeff[ 94]        *x31*x43    
        +coeff[ 95]    *x22    *x42    
        +coeff[ 96]*x11    *x31*x42    
        ;

    return v_y_e_q3en_1200                                ;
}
float p_e_q3en_1200                                (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.6094652E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.58458507E-04,-0.21062611E-01, 0.10567671E-01,-0.56154639E-02,
        -0.12396672E-01, 0.43508131E-02, 0.10050592E-01,-0.80420198E-02,
        -0.44005035E-03,-0.17042463E-02,-0.25006747E-02, 0.43017865E-03,
        -0.18619103E-02,-0.11658282E-02, 0.50559483E-03, 0.13057349E-02,
         0.19463830E-05,-0.80451532E-03, 0.36854497E-04, 0.25027929E-03,
        -0.25165771E-03,-0.11469028E-02,-0.17337580E-03,-0.85019518E-03,
        -0.81215845E-03, 0.12563552E-03,-0.32493789E-03, 0.72318647E-03,
         0.39475452E-03,-0.17737938E-03,-0.88457688E-04, 0.42071420E-03,
         0.21337952E-03,-0.11599455E-03, 0.39041435E-04,-0.16631688E-03,
         0.12023940E-03,-0.14423135E-02,-0.84123155E-03,-0.31066520E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_1200                                =v_p_e_q3en_1200                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_1200                                =v_p_e_q3en_1200                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_1200                                =v_p_e_q3en_1200                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_1200                                =v_p_e_q3en_1200                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x23*x31    *x51
        ;

    return v_p_e_q3en_1200                                ;
}
float l_e_q3en_1200                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1283848E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.12612531E-01,-0.31138778E+00,-0.25444899E-01, 0.12489455E-01,
        -0.28779145E-01,-0.16644320E-01,-0.57517663E-02,-0.54703676E-02,
         0.19183126E-02, 0.94857616E-02, 0.84541291E-02,-0.86271577E-03,
        -0.15484757E-03, 0.15765092E-02,-0.69073995E-03, 0.33297343E-02,
        -0.19797932E-02,-0.23840594E-02, 0.28367548E-02, 0.44141882E-02,
         0.41834608E-03,-0.32370343E-03, 0.44817739E-03, 0.51854999E-03,
         0.40806158E-03, 0.41538954E-03,-0.36157962E-03, 0.17195487E-02,
        -0.69400814E-03, 0.56733899E-02,-0.15356513E-02, 0.60388434E-03,
        -0.14938934E-02, 0.16940776E-02, 0.14125466E-02, 0.92953304E-03,
        -0.11534850E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_1200                                =v_l_e_q3en_1200                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3en_1200                                =v_l_e_q3en_1200                                
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21    *x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3en_1200                                =v_l_e_q3en_1200                                
        +coeff[ 26]*x11*x21        *x51
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x21        *x53
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]        *x33*x41*x51
        +coeff[ 31]    *x22*x31    *x52
        +coeff[ 32]        *x31*x41*x53
        +coeff[ 33]    *x24*x32        
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_q3en_1200                                =v_l_e_q3en_1200                                
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]*x12    *x32*x41*x51
        ;

    return v_l_e_q3en_1200                                ;
}
float x_e_q3en_1100                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.2615609E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.87888545E-03,-0.10843681E-01, 0.11104701E+00, 0.21922447E+00,
         0.28463552E-01, 0.16996101E-01,-0.37714392E-02,-0.37189429E-02,
        -0.87353401E-02,-0.91386996E-02,-0.11431742E-02,-0.20215837E-02,
         0.22185491E-02,-0.42409399E-02,-0.90320717E-03,-0.70659403E-03,
         0.93912771E-04,-0.45034089E-03, 0.14370540E-03,-0.10223532E-02,
        -0.65500750E-02,-0.40319716E-02,-0.36769058E-02, 0.59666409E-03,
        -0.50051957E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_1100                                =v_x_e_q3en_1100                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3en_1100                                =v_x_e_q3en_1100                                
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_q3en_1100                                ;
}
float t_e_q3en_1100                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3945799E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.33393460E-02, 0.26840526E-02,-0.76348849E-01, 0.18247988E-01,
        -0.51095216E-02,-0.41252091E-02,-0.75342524E-03,-0.92083827E-03,
         0.16994999E-02, 0.48180850E-03, 0.35754382E-03, 0.74532494E-03,
         0.80142135E-03,-0.17017488E-03, 0.52407343E-03, 0.33611050E-03,
         0.30418232E-03, 0.69216891E-04,-0.81791783E-04,-0.30336934E-03,
         0.76420640E-03, 0.12439516E-03,-0.13405984E-03, 0.35368389E-03,
         0.47711731E-03,-0.33059227E-03, 0.63557673E-04,-0.47036220E-03,
        -0.39071115E-03, 0.20981620E-02, 0.17640807E-02, 0.15493821E-02,
         0.67578549E-04,-0.38552509E-04, 0.63200721E-04, 0.15891130E-02,
         0.26599821E-03,-0.19226033E-03,-0.12767287E-04, 0.45449651E-04,
         0.88271612E-04,-0.50303679E-04, 0.57297653E-04, 0.44818808E-04,
         0.74320106E-03, 0.14195601E-03,-0.18732634E-03,-0.10870250E-03,
         0.11901371E-03, 0.19769592E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_1100                                =v_t_e_q3en_1100                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]*x11*x21            
        +coeff[ 10]            *x42    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_1100                                =v_t_e_q3en_1100                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_1100                                =v_t_e_q3en_1100                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]                *x53
    ;
    v_t_e_q3en_1100                                =v_t_e_q3en_1100                                
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]    *x21*x31*x41*x52
        +coeff[ 38]*x12                
        +coeff[ 39]*x11    *x31*x41    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_1100                                =v_t_e_q3en_1100                                
        +coeff[ 44]    *x21*x33*x41    
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]    *x22*x31*x41*x51
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]    *x21    *x42*x52
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_q3en_1100                                ;
}
float y_e_q3en_1100                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9014009E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.76894229E-03, 0.11544488E+00,-0.78131743E-01,-0.66074744E-01,
        -0.22703994E-01, 0.48007883E-01, 0.25642088E-01,-0.31860393E-01,
        -0.13105866E-01,-0.72472901E-02, 0.24324271E-02,-0.97498167E-02,
        -0.22761254E-02,-0.31109191E-02,-0.24066441E-03,-0.40017947E-03,
         0.86375349E-03,-0.57565062E-02,-0.11906266E-02, 0.16315394E-02,
        -0.17609056E-02, 0.50517539E-02,-0.55845841E-02, 0.51938294E-03,
         0.16974801E-02,-0.21012558E-02,-0.11802701E-02, 0.69994503E-03,
         0.14981072E-02, 0.69221767E-03,-0.41840472E-02,-0.46500494E-02,
        -0.27406423E-02,-0.34390343E-03, 0.61806961E-04,-0.47928592E-04,
         0.11872759E-02, 0.26839736E-03, 0.71340177E-03, 0.36230421E-03,
        -0.16212540E-02,-0.15165197E-02, 0.13037936E-02,-0.74281481E-04,
        -0.31901180E-03,-0.28621909E-03, 0.18346880E-03,-0.76408050E-03,
         0.52812166E-03, 0.38167433E-04,-0.54417971E-04,-0.40034622E-04,
         0.63671777E-03,-0.19937041E-03, 0.47522041E-03, 0.13710605E-03,
         0.62003580E-03, 0.25684282E-03, 0.37083500E-02, 0.23334771E-02,
        -0.85654333E-02,-0.92578596E-02, 0.98519980E-04,-0.20876841E-02,
         0.12331650E-04, 0.76017664E-05,-0.24674879E-04,-0.26636435E-04,
         0.49143968E-04, 0.37371465E-04,-0.39750812E-04, 0.43040283E-04,
        -0.12908826E-05, 0.14563181E-02, 0.48471356E-04, 0.37444972E-02,
         0.21259040E-02, 0.44381325E-02,-0.12167869E-03, 0.25264053E-02,
         0.39434603E-04,-0.41232401E-03, 0.68369211E-03, 0.78896141E-04,
        -0.50643861E-03, 0.41010568E-03,-0.15270746E-02,-0.48670146E-03,
        -0.10002081E-03,-0.19524217E-03,-0.31136386E-02,-0.48974901E-02,
         0.10094656E-03,-0.18356099E-02,-0.13716891E-02,-0.24020435E-05,
         0.85779757E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x21            
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 35]                *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x23    *x41*x51
        +coeff[ 43]            *x45*x51
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x22*x31*x44    
        +coeff[ 61]    *x22*x32*x43    
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 62]    *x23    *x45    
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]        *x31*x41    
        +coeff[ 65]    *x21        *x51
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11    *x31    *x51
        +coeff[ 68]*x11        *x42*x51
        +coeff[ 69]            *x42*x52
        +coeff[ 70]*x11*x21    *x41*x51
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 71]        *x31*x41*x52
        +coeff[ 72]        *x34*x41    
        +coeff[ 73]    *x21    *x45    
        +coeff[ 74]        *x31    *x53
        +coeff[ 75]    *x21*x31*x44    
        +coeff[ 76]    *x23    *x43    
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]*x11*x22    *x41*x51
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 80]    *x22*x31    *x52
        +coeff[ 81]    *x22    *x43*x51
        +coeff[ 82]    *x21*x34*x41    
        +coeff[ 83]    *x21    *x41*x53
        +coeff[ 84]    *x22*x31*x42*x51
        +coeff[ 85]    *x23*x33        
        +coeff[ 86]    *x22    *x45    
        +coeff[ 87]    *x24    *x41*x51
        +coeff[ 88]*x11*x22    *x42*x51
    ;
    v_y_e_q3en_1100                                =v_y_e_q3en_1100                                
        +coeff[ 89]    *x24*x31    *x51
        +coeff[ 90]    *x24*x31*x42    
        +coeff[ 91]    *x22*x33*x42    
        +coeff[ 92]*x11    *x31*x42*x52
        +coeff[ 93]    *x24*x32*x41    
        +coeff[ 94]    *x22*x34*x41    
        +coeff[ 95]*x11                
        +coeff[ 96]        *x32        
        ;

    return v_y_e_q3en_1100                                ;
}
float p_e_q3en_1100                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1764147E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.16578739E-03,-0.21104587E-01, 0.10506421E-01,-0.56062294E-02,
        -0.12344557E-01, 0.43542697E-02, 0.10048100E-01,-0.80505824E-02,
        -0.43971502E-03,-0.17034602E-02,-0.25013080E-02, 0.42689007E-03,
        -0.18620144E-02,-0.11673886E-02, 0.49870188E-03, 0.12979696E-02,
        -0.66992652E-05,-0.80123503E-03, 0.49842456E-04, 0.25304628E-03,
        -0.25544321E-03,-0.11627072E-02,-0.18374216E-03,-0.84901659E-03,
        -0.81388181E-03, 0.12257324E-03,-0.32372068E-03, 0.71305415E-03,
         0.39403272E-03,-0.24450224E-03,-0.86354208E-04, 0.43334562E-03,
         0.21436615E-03,-0.10632453E-03, 0.40953579E-04,-0.19420630E-03,
        -0.14405788E-02,-0.84682822E-03,-0.28740725E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_1100                                =v_p_e_q3en_1100                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_1100                                =v_p_e_q3en_1100                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_1100                                =v_p_e_q3en_1100                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_1100                                =v_p_e_q3en_1100                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_1100                                ;
}
float l_e_q3en_1100                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1305697E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.13056369E-01,-0.31133705E+00,-0.25334397E-01, 0.12208717E-01,
        -0.29278276E-01,-0.16794248E-01,-0.52793128E-02,-0.52911150E-02,
         0.21226115E-02, 0.89904265E-02, 0.84232045E-02,-0.14431018E-03,
         0.72770221E-04, 0.23610746E-02,-0.93480520E-03, 0.28676598E-02,
        -0.17919049E-02,-0.24700358E-02, 0.13612249E-02, 0.86345593E-03,
         0.94811124E-03,-0.19056484E-03,-0.63331670E-03, 0.42242763E-03,
         0.95818128E-03,-0.12178384E-02,-0.61742199E-03, 0.78475522E-02,
         0.56398502E-02, 0.58260735E-03,-0.15620957E-02,-0.72242640E-03,
        -0.98900159E-03, 0.70615392E-03, 0.94971241E-03,-0.85516635E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_1100                                =v_l_e_q3en_1100                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3en_1100                                =v_l_e_q3en_1100                                
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]*x12                
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x31*x42    
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q3en_1100                                =v_l_e_q3en_1100                                
        +coeff[ 26]            *x42*x52
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]*x13    *x32        
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]*x11*x24*x31        
        +coeff[ 32]*x11*x23*x32        
        +coeff[ 33]*x11    *x34    *x51
        +coeff[ 34]*x11        *x43*x53
    ;
    v_l_e_q3en_1100                                =v_l_e_q3en_1100                                
        +coeff[ 35]*x12*x22*x33        
        ;

    return v_l_e_q3en_1100                                ;
}
float x_e_q3en_1000                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.1334290E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
         0.58587524E-03,-0.10845678E-01, 0.11104217E+00, 0.21930705E+00,
         0.28532434E-01, 0.16996937E-01,-0.37758409E-02,-0.37076620E-02,
        -0.88586500E-02,-0.91582602E-02,-0.11562966E-02,-0.20597551E-02,
         0.21946426E-02,-0.42968849E-02,-0.90988644E-03,-0.46111774E-03,
        -0.66263328E-03, 0.13337651E-03,-0.92789350E-03,-0.62599280E-02,
        -0.39460431E-02,-0.36060442E-02,-0.47791051E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_1000                                =v_x_e_q3en_1000                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11*x22            
        +coeff[ 16]            *x42*x51
    ;
    v_x_e_q3en_1000                                =v_x_e_q3en_1000                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_q3en_1000                                ;
}
float t_e_q3en_1000                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.3988166E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.33974589E-02, 0.26853378E-02,-0.76361954E-01, 0.18240120E-01,
        -0.51092226E-02,-0.41194162E-02,-0.91931398E-03, 0.45933886E-03,
        -0.74880873E-03, 0.16769414E-02, 0.77399588E-03, 0.80443284E-03,
         0.34745524E-03,-0.17204085E-03, 0.53028949E-03, 0.34192658E-03,
         0.34339086E-03, 0.61336876E-04,-0.87938759E-04,-0.27917072E-03,
         0.76331163E-03, 0.12498697E-03,-0.12783628E-03, 0.36672220E-03,
         0.46414041E-03,-0.30316075E-03, 0.55356919E-04,-0.44746505E-03,
        -0.44328548E-03, 0.20493183E-02, 0.17751858E-02, 0.15605955E-02,
        -0.38079477E-04, 0.64495463E-04, 0.62442785E-04, 0.15507593E-02,
         0.23354620E-03,-0.12370436E-04, 0.60785893E-04, 0.73782299E-04,
         0.48193368E-04,-0.39752991E-04, 0.59693150E-04, 0.56718989E-04,
         0.16129670E-04, 0.11260716E-03, 0.68983587E-03, 0.10729847E-03,
        -0.15924938E-03,-0.89369612E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_1000                                =v_t_e_q3en_1000                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_1000                                =v_t_e_q3en_1000                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_1000                                =v_t_e_q3en_1000                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]        *x32*x42    
    ;
    v_t_e_q3en_1000                                =v_t_e_q3en_1000                                
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]*x12                
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11*x23            
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_1000                                =v_t_e_q3en_1000                                
        +coeff[ 44]*x13    *x31*x41    
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]    *x22*x31*x41*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_q3en_1000                                ;
}
float y_e_q3en_1000                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.5205051E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.46230500E-03, 0.11496072E+00,-0.78414403E-01,-0.66343285E-01,
        -0.22708433E-01, 0.48081219E-01, 0.25657013E-01,-0.31769253E-01,
        -0.13024298E-01,-0.71976255E-02, 0.24427972E-02,-0.95660798E-02,
        -0.22716071E-02,-0.30976012E-02,-0.15936700E-03,-0.64533250E-03,
         0.88162295E-03,-0.55357474E-02,-0.11712404E-02, 0.16295974E-02,
        -0.17532069E-02, 0.53890138E-02,-0.56169885E-02, 0.56415290E-03,
         0.17316868E-02,-0.23317961E-02,-0.12189609E-02, 0.21375141E-02,
         0.21543456E-02, 0.15319226E-02,-0.39685536E-02,-0.51799598E-02,
        -0.33173598E-02,-0.23773221E-03, 0.12194063E-02, 0.33156099E-03,
        -0.34644551E-03, 0.73533319E-03,-0.16566816E-02,-0.15780665E-02,
         0.12413806E-02, 0.10116235E-03,-0.62402809E-03, 0.32622494E-04,
        -0.22141845E-04,-0.44804366E-03, 0.17324161E-03,-0.86190086E-03,
         0.49052824E-03,-0.52920925E-04,-0.50889397E-04, 0.47519660E-03,
         0.47137198E-03,-0.44296248E-03, 0.46743278E-03,-0.20927798E-03,
         0.10446274E-03, 0.64933870E-03, 0.26796843E-03, 0.57229074E-03,
         0.33118003E-02, 0.19179904E-02,-0.12388307E-03,-0.21204070E-03,
        -0.84935986E-02,-0.86390758E-02,-0.21971643E-02, 0.14896747E-04,
         0.20111534E-04, 0.37015467E-04,-0.22671886E-04,-0.32140550E-04,
        -0.39494353E-04, 0.26855401E-04, 0.58335463E-04, 0.28015659E-02,
        -0.88878282E-04, 0.25982363E-02,-0.82291386E-04, 0.16746047E-02,
         0.60161889E-04,-0.51960495E-03, 0.32845399E-03,-0.18147107E-02,
        -0.15088098E-03,-0.25753332E-02,-0.42424682E-02,-0.15273659E-02,
        -0.95977675E-03, 0.16555396E-02, 0.76645281E-03, 0.60861534E-03,
        -0.66818198E-03, 0.11991141E-03,-0.12085050E-04, 0.93967592E-05,
         0.19902840E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21    *x43*x52
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 44]                *x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]*x12        *x41    
        +coeff[ 50]*x11        *x41*x51
        +coeff[ 51]            *x43*x51
        +coeff[ 52]    *x21    *x41*x52
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 62]*x11*x21    *x43*x51
        +coeff[ 63]    *x24    *x41*x51
        +coeff[ 64]    *x22*x31*x44    
        +coeff[ 65]    *x22*x32*x43    
        +coeff[ 66]    *x24    *x45    
        +coeff[ 67]    *x22            
        +coeff[ 68]            *x44    
        +coeff[ 69]        *x31*x43    
        +coeff[ 70]*x12    *x31        
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 71]*x11    *x31    *x51
        +coeff[ 72]*x11*x22*x31        
        +coeff[ 73]*x12*x21    *x41    
        +coeff[ 74]        *x31    *x53
        +coeff[ 75]    *x21*x31*x44    
        +coeff[ 76]*x11*x23*x31        
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]*x11*x22    *x41*x51
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 80]    *x22    *x43*x51
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]    *x24*x31    *x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_q3en_1000                                =v_y_e_q3en_1000                                
        +coeff[ 89]    *x23    *x45    
        +coeff[ 90]    *x21*x34*x43    
        +coeff[ 91]    *x21    *x45*x52
        +coeff[ 92]    *x24    *x43*x51
        +coeff[ 93]    *x23    *x41*x53
        +coeff[ 94]    *x21    *x42    
        +coeff[ 95]*x11    *x31*x41    
        +coeff[ 96]    *x22    *x42    
        ;

    return v_y_e_q3en_1000                                ;
}
float p_e_q3en_1000                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1520533E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.14835721E-03,-0.21156471E-01, 0.10413362E-01,-0.56047472E-02,
        -0.12349310E-01, 0.43536238E-02, 0.10051528E-01,-0.80044512E-02,
        -0.43409612E-03,-0.16828236E-02,-0.25023646E-02, 0.42918883E-03,
        -0.18611439E-02,-0.11603674E-02, 0.50207821E-03, 0.13008610E-02,
        -0.30019025E-05,-0.80548925E-03, 0.28206556E-04, 0.25175337E-03,
        -0.25341497E-03,-0.11376466E-02,-0.17731829E-03,-0.84695534E-03,
        -0.82590297E-03, 0.12149660E-03,-0.31716441E-03, 0.66463556E-03,
         0.36428054E-03,-0.28015953E-03,-0.85938438E-04, 0.40923394E-03,
         0.21506395E-03,-0.11281117E-03, 0.14950168E-04,-0.19930159E-03,
        -0.13783475E-02,-0.84446219E-03,-0.30888477E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_1000                                =v_p_e_q3en_1000                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_1000                                =v_p_e_q3en_1000                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_1000                                =v_p_e_q3en_1000                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_1000                                =v_p_e_q3en_1000                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_1000                                ;
}
float l_e_q3en_1000                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1233449E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.12142314E-01,-0.31138900E+00,-0.25284151E-01, 0.12479055E-01,
        -0.28416257E-01,-0.16429996E-01,-0.55138930E-02,-0.53400500E-02,
         0.20514200E-02, 0.90209935E-02, 0.75090961E-02,-0.22601841E-02,
        -0.56802726E-03,-0.56529732E-03, 0.27006180E-02,-0.84895513E-03,
         0.30163375E-02,-0.19685335E-02,-0.82650367E-03, 0.16065867E-02,
         0.78715768E-03, 0.74787741E-02, 0.19603719E-02, 0.51241089E-03,
        -0.99808932E-03,-0.31193387E-03, 0.81612859E-02, 0.54266484E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_1000                                =v_l_e_q3en_1000                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_1000                                =v_l_e_q3en_1000                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]*x12            *x52
    ;
    v_l_e_q3en_1000                                =v_l_e_q3en_1000                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]            *x44*x51
        ;

    return v_l_e_q3en_1000                                ;
}
float x_e_q3en_900                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.3653570E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.16704858E-02,-0.10848623E-01, 0.11100949E+00, 0.21950401E+00,
         0.28521581E-01, 0.16917709E-01,-0.37979563E-02,-0.38324238E-02,
        -0.94180135E-02,-0.88913720E-02,-0.11513014E-02,-0.21065702E-02,
         0.22017844E-02,-0.41657197E-02,-0.90190180E-03,-0.61930245E-03,
        -0.44276909E-03, 0.17777320E-03,-0.65181527E-03, 0.70288125E-03,
        -0.59566558E-02,-0.38648602E-02,-0.89624841E-02,-0.38207492E-02,
        -0.36451751E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_900                                =v_x_e_q3en_900                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_900                                =v_x_e_q3en_900                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23*x32*x42    
        +coeff[ 23]    *x23*x31*x43    
        +coeff[ 24]    *x21*x33*x43    
        ;

    return v_x_e_q3en_900                                ;
}
float t_e_q3en_900                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4069956E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.35250864E-02, 0.26850784E-02,-0.76414421E-01, 0.18245496E-01,
        -0.51277997E-02,-0.41259211E-02,-0.91626804E-03, 0.45901933E-03,
        -0.76987065E-03, 0.17592531E-02, 0.78998564E-03, 0.77760767E-03,
         0.33692998E-03,-0.17053992E-03, 0.52196701E-03, 0.33021875E-03,
         0.27535218E-03, 0.59748872E-04,-0.99216450E-04,-0.27888207E-03,
         0.76872006E-03, 0.12468267E-03,-0.12715129E-03, 0.37330214E-03,
         0.45524523E-03,-0.30285589E-03, 0.52470368E-04,-0.48102549E-03,
        -0.41256324E-03, 0.19992155E-02, 0.17071929E-02,-0.36888850E-04,
         0.61931125E-04, 0.16572059E-02, 0.15555721E-02, 0.26777675E-03,
        -0.20357041E-03, 0.58930669E-04, 0.76730030E-04, 0.53279673E-04,
         0.55119850E-04, 0.93730676E-04,-0.30563289E-04, 0.47332916E-04,
         0.45924942E-04, 0.27703994E-04, 0.78828503E-04, 0.71437238E-03,
         0.69010566E-04,-0.19420381E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_900                                =v_t_e_q3en_900                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_900                                =v_t_e_q3en_900                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_900                                =v_t_e_q3en_900                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]                *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_q3en_900                                =v_t_e_q3en_900                                
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]    *x21*x31*x41*x52
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]        *x31*x41*x51
        +coeff[ 39]*x11*x23            
        +coeff[ 40]        *x33*x41    
        +coeff[ 41]        *x32*x42    
        +coeff[ 42]    *x22        *x52
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_q3en_900                                =v_t_e_q3en_900                                
        +coeff[ 44]            *x42*x52
        +coeff[ 45]*x13    *x31*x41    
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_q3en_900                                ;
}
float y_e_q3en_900                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1267939E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.41071402E-04, 0.11441917E+00,-0.78722231E-01,-0.66258721E-01,
        -0.22651613E-01, 0.48064407E-01, 0.25676182E-01,-0.31841259E-01,
        -0.12965358E-01,-0.71963994E-02, 0.24350721E-02,-0.94369911E-02,
        -0.22663702E-02,-0.30928303E-02,-0.18698601E-03,-0.42420253E-03,
         0.87698933E-03,-0.56700837E-02, 0.16453209E-02,-0.17721306E-02,
         0.55039017E-02,-0.56180339E-02,-0.11358281E-02, 0.55618543E-03,
         0.17219345E-02,-0.24621256E-02,-0.13239660E-02, 0.20178766E-02,
         0.21414484E-02, 0.17623829E-02,-0.39320630E-02,-0.53013754E-02,
        -0.31856105E-02, 0.11425770E-02, 0.26358449E-03,-0.34575185E-03,
         0.71654905E-03,-0.16717307E-02,-0.16634631E-02,-0.10851793E-02,
         0.11848166E-02, 0.79124366E-05, 0.30732655E-04,-0.21709284E-04,
        -0.68336219E-03, 0.18372700E-03, 0.33037705E-03, 0.55104122E-03,
        -0.51242787E-04,-0.44555611E-04,-0.36359241E-04, 0.54511364E-03,
        -0.66293716E-04,-0.76933432E-03, 0.44323696E-03, 0.11927963E-03,
         0.63200033E-03, 0.25527200E-03,-0.78673875E-05, 0.23498591E-02,
         0.89689449E-03,-0.83965715E-02,-0.89336121E-02,-0.78687235E-03,
        -0.23517434E-02, 0.15160748E-04,-0.20517924E-04,-0.29254945E-04,
        -0.81328893E-04, 0.55791181E-04, 0.26617988E-02,-0.72959541E-04,
         0.24797267E-02,-0.80228274E-04,-0.52610518E-04, 0.23780824E-02,
         0.68019755E-04, 0.12954915E-03, 0.64888940E-04,-0.43033148E-03,
         0.39842693E-03,-0.16788769E-02, 0.83613629E-03,-0.16495335E-03,
        -0.26459761E-02,-0.40372182E-02, 0.28245352E-03,-0.13979728E-02,
        -0.11405966E-02, 0.27498130E-02, 0.29284081E-02,-0.14968192E-02,
         0.21331282E-02, 0.64572430E-03, 0.99294539E-05, 0.12206752E-04,
        -0.47287745E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]    *x24    *x41    
        +coeff[ 38]    *x24*x31        
        +coeff[ 39]    *x22*x33        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]*x11    *x31    *x51
        +coeff[ 51]            *x43*x51
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x23    *x43    
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x24    *x43*x51
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]    *x22            
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11*x22*x31        
        +coeff[ 68]        *x34*x41    
        +coeff[ 69]        *x31    *x53
        +coeff[ 70]    *x21*x31*x44    
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 71]*x11*x23*x31        
        +coeff[ 72]    *x21*x32*x43    
        +coeff[ 73]*x11*x22    *x41*x51
        +coeff[ 74]        *x32*x41*x52
        +coeff[ 75]    *x21*x33*x42    
        +coeff[ 76]    *x22*x31    *x52
        +coeff[ 77]    *x22    *x43*x51
        +coeff[ 78]    *x21    *x41*x53
        +coeff[ 79]    *x22*x31*x42*x51
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 80]    *x23*x33        
        +coeff[ 81]    *x22    *x45    
        +coeff[ 82]        *x33*x44    
        +coeff[ 83]    *x21    *x45*x51
        +coeff[ 84]    *x24*x31*x42    
        +coeff[ 85]    *x22*x33*x42    
        +coeff[ 86]    *x23    *x43*x51
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_q3en_900                                =v_y_e_q3en_900                                
        +coeff[ 89]    *x23    *x45    
        +coeff[ 90]    *x23*x31*x44    
        +coeff[ 91]    *x21*x33*x44    
        +coeff[ 92]    *x23*x32*x43    
        +coeff[ 93]    *x23*x34*x41    
        +coeff[ 94]            *x42    
        +coeff[ 95]        *x31*x41    
        +coeff[ 96]*x11*x21            
        ;

    return v_y_e_q3en_900                                ;
}
float p_e_q3en_900                                (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.3184461E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.22545422E-04,-0.21209460E-01, 0.10320908E-01,-0.56173713E-02,
        -0.12383150E-01, 0.43525924E-02, 0.10044127E-01,-0.80386307E-02,
        -0.43306567E-03,-0.16884352E-02,-0.25085714E-02, 0.42729604E-03,
        -0.18532865E-02,-0.11570127E-02, 0.50027092E-03, 0.13203558E-02,
        -0.80306891E-05,-0.79689943E-03, 0.43857366E-04, 0.25441102E-03,
        -0.25337914E-03,-0.11531365E-02,-0.16916596E-03,-0.85292058E-03,
        -0.82117680E-03, 0.12263338E-03,-0.32119782E-03, 0.72262151E-03,
         0.40402033E-03,-0.15684124E-03,-0.87869535E-04, 0.43397554E-03,
         0.21299002E-03,-0.10775679E-03, 0.43890526E-04,-0.16786442E-03,
         0.11936749E-03,-0.14353880E-02,-0.85250533E-03,-0.31660416E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_900                                =v_p_e_q3en_900                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_900                                =v_p_e_q3en_900                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_900                                =v_p_e_q3en_900                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_900                                =v_p_e_q3en_900                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x23*x31    *x51
        ;

    return v_p_e_q3en_900                                ;
}
float l_e_q3en_900                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1388570E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.13909350E-01,-0.31194329E+00,-0.25318593E-01, 0.12359055E-01,
        -0.28877757E-01,-0.16707223E-01,-0.54240166E-02,-0.53092320E-02,
         0.20951359E-02, 0.69374265E-02, 0.85755419E-02, 0.29423789E-03,
        -0.15930734E-03, 0.17331480E-02,-0.24161772E-02,-0.87695877E-03,
         0.33894756E-02,-0.22381281E-02,-0.44862023E-04, 0.12668361E-02,
         0.90490803E-02, 0.43256660E-02,-0.21692703E-03, 0.43870302E-03,
         0.28374078E-03, 0.63169701E-03, 0.46849326E-03,-0.77205896E-03,
        -0.10526908E-02, 0.68695005E-03, 0.59288708E-02, 0.33979287E-03,
        -0.53128490E-03, 0.77599415E-03,-0.12804649E-02,-0.23610436E-02,
         0.28780983E-02, 0.35528170E-02, 0.34289819E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_900                                =v_l_e_q3en_900                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_900                                =v_l_e_q3en_900                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x31    *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_q3en_900                                =v_l_e_q3en_900                                
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]*x12    *x31*x41    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]*x11    *x34        
        +coeff[ 32]*x11*x23    *x41    
        +coeff[ 33]    *x24    *x42    
        +coeff[ 34]    *x21*x31*x42*x52
    ;
    v_l_e_q3en_900                                =v_l_e_q3en_900                                
        +coeff[ 35]        *x32*x42*x52
        +coeff[ 36]    *x21*x34*x42    
        +coeff[ 37]    *x23    *x44    
        +coeff[ 38]    *x21*x32*x42*x52
        ;

    return v_l_e_q3en_900                                ;
}
float x_e_q3en_800                                (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.4024348E-02;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.19820610E-02,-0.10843370E-01, 0.11102395E+00, 0.21967268E+00,
         0.28438343E-01, 0.17022913E-01,-0.37862316E-02,-0.36857333E-02,
        -0.89204358E-02,-0.92669101E-02,-0.11499958E-02,-0.20142647E-02,
         0.22124755E-02,-0.42883628E-02,-0.90549269E-03,-0.67257159E-03,
        -0.44291132E-03, 0.81878214E-04,-0.10898772E-02,-0.62787537E-02,
        -0.38061240E-02,-0.34254203E-02, 0.69636130E-03,-0.44411635E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_800                                =v_x_e_q3en_800                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_800                                =v_x_e_q3en_800                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_q3en_800                                ;
}
float t_e_q3en_800                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4446542E-02;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.38932888E-02, 0.26817105E-02,-0.76426059E-01, 0.18238971E-01,
        -0.51446017E-02,-0.41107992E-02,-0.92945510E-03, 0.46332018E-03,
        -0.76361478E-03, 0.16506114E-02, 0.81743492E-03, 0.79638121E-03,
         0.32936956E-03,-0.16894411E-03, 0.52420818E-03, 0.33388205E-03,
         0.29419566E-03, 0.58328111E-04,-0.11134717E-03,-0.29834008E-03,
         0.78586687E-03, 0.12830134E-03,-0.13589908E-03, 0.39393900E-03,
         0.47818644E-03,-0.30982215E-03, 0.49232658E-04,-0.44537275E-03,
        -0.44641850E-03, 0.20257523E-02, 0.16952894E-02, 0.15975836E-02,
         0.68319379E-04,-0.34205037E-04, 0.70198657E-04, 0.92562092E-04,
         0.15823758E-02,-0.17983351E-03, 0.19847545E-04, 0.28644587E-03,
        -0.11570596E-04, 0.21978825E-04, 0.72121147E-04, 0.41576539E-04,
         0.41420364E-04, 0.57950030E-04, 0.59523612E-04, 0.10180033E-03,
         0.75436675E-03, 0.14096849E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_800                                =v_t_e_q3en_800                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_800                                =v_t_e_q3en_800                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_800                                =v_t_e_q3en_800                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]                *x53
    ;
    v_t_e_q3en_800                                =v_t_e_q3en_800                                
        +coeff[ 35]        *x32*x42    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]        *x33*x41*x51
        +coeff[ 39]    *x22    *x42*x51
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_q3en_800                                =v_t_e_q3en_800                                
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_q3en_800                                ;
}
float y_e_q3en_800                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.6899556E-03;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.68995217E-03, 0.11378713E+00,-0.79109453E-01,-0.65903179E-01,
        -0.22564976E-01, 0.48097607E-01, 0.25693823E-01,-0.31751733E-01,
        -0.13113145E-01,-0.72126798E-02, 0.24345450E-02,-0.96272128E-02,
        -0.22740674E-02,-0.30886652E-02,-0.30281578E-03,-0.71571855E-03,
         0.87244197E-03,-0.55814129E-02,-0.11790779E-02, 0.16229770E-02,
        -0.17698470E-02, 0.49999738E-02,-0.54644397E-02, 0.53988531E-03,
         0.16136518E-02,-0.23932937E-02,-0.13269648E-02, 0.42546459E-03,
         0.13289871E-02, 0.60857483E-03,-0.43392731E-02,-0.49326350E-02,
        -0.32028861E-02,-0.36802376E-03, 0.11815932E-02, 0.21345765E-03,
        -0.34199443E-03, 0.73070766E-03, 0.34692624E-03,-0.16673190E-02,
        -0.14608453E-02, 0.12599926E-02,-0.10285779E-03,-0.44318472E-03,
         0.18421251E-03,-0.37501816E-03,-0.77012466E-03, 0.51159912E-03,
        -0.55985787E-04,-0.29377039E-04, 0.64517639E-03, 0.48514083E-03,
         0.11533814E-03, 0.66799688E-03, 0.29944730E-03, 0.23604913E-02,
        -0.13318736E-03, 0.37264649E-02, 0.22516288E-02, 0.50692318E-03,
        -0.77592464E-04,-0.83324155E-02,-0.21198800E-05,-0.85717821E-02,
        -0.20692442E-02,-0.69017483E-05, 0.61640517E-05,-0.25145988E-04,
        -0.30100548E-04,-0.38721060E-04,-0.11044228E-03, 0.16492957E-02,
         0.58725418E-04, 0.39757830E-02, 0.45673051E-02, 0.25089872E-02,
         0.38209220E-03, 0.76351862E-03, 0.80682526E-04,-0.61920851E-04,
        -0.14907267E-02,-0.28244928E-02,-0.45930459E-02,-0.14454866E-02,
        -0.13070480E-02,-0.11538160E-02,-0.69083163E-03,-0.33048815E-04,
        -0.17833601E-04, 0.14135540E-04,-0.17748460E-04,-0.60252609E-04,
        -0.67657784E-04,-0.54781805E-04, 0.23909508E-04,-0.19994421E-04,
         0.57810339E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]        *x33*x42    
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]*x11*x21    *x43    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 53]*x11*x21*x31*x42    
        +coeff[ 54]*x11*x21*x32*x41    
        +coeff[ 55]    *x23    *x43    
        +coeff[ 56]*x11*x22    *x41*x51
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x23*x32*x41    
        +coeff[ 59]    *x23*x33        
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 62]*x13    *x31    *x51
        +coeff[ 63]    *x22*x32*x43    
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]    *x21            
        +coeff[ 66]                *x51
        +coeff[ 67]*x12    *x31        
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]*x11*x21    *x41*x51
        +coeff[ 70]        *x34*x41    
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 71]    *x21    *x45    
        +coeff[ 72]        *x31    *x53
        +coeff[ 73]    *x21*x31*x44    
        +coeff[ 74]    *x21*x32*x43    
        +coeff[ 75]    *x21*x33*x42    
        +coeff[ 76]    *x22    *x43*x51
        +coeff[ 77]    *x21*x34*x41    
        +coeff[ 78]    *x21    *x41*x53
        +coeff[ 79]    *x22*x31*x42*x51
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 80]    *x22    *x45    
        +coeff[ 81]    *x24*x31*x42    
        +coeff[ 82]    *x22*x33*x42    
        +coeff[ 83]    *x24*x32*x41    
        +coeff[ 84]    *x22*x34*x41    
        +coeff[ 85]    *x24    *x43*x51
        +coeff[ 86]    *x24*x31*x42*x51
        +coeff[ 87]*x11    *x31    *x51
        +coeff[ 88]    *x21    *x42*x51
    ;
    v_y_e_q3en_800                                =v_y_e_q3en_800                                
        +coeff[ 89]*x11*x21*x31    *x51
        +coeff[ 90]*x11        *x41*x52
        +coeff[ 91]            *x43*x52
        +coeff[ 92]    *x21*x32*x41*x51
        +coeff[ 93]*x11*x23*x31        
        +coeff[ 94]*x12*x21*x31*x41    
        +coeff[ 95]*x11*x21*x32    *x51
        +coeff[ 96]    *x22*x31    *x52
        ;

    return v_y_e_q3en_800                                ;
}
float p_e_q3en_800                                (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.6211523E-04;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.64056308E-04,-0.21285078E-01, 0.10204627E-01,-0.56141298E-02,
        -0.12385418E-01, 0.43554017E-02, 0.10049529E-01,-0.80274576E-02,
        -0.44658154E-03,-0.16758471E-02,-0.24910406E-02, 0.42696836E-03,
        -0.18640534E-02,-0.11607327E-02, 0.49982901E-03, 0.12973616E-02,
         0.18416035E-05,-0.81985042E-03, 0.56325312E-04, 0.24884348E-03,
        -0.25561292E-03,-0.11617999E-02,-0.17018948E-03,-0.86059904E-03,
        -0.82014588E-03, 0.12073149E-03,-0.32649966E-03, 0.69263123E-03,
         0.40347059E-03,-0.15415461E-03,-0.87537243E-04, 0.42917187E-03,
         0.22580968E-03,-0.11365074E-03, 0.31666576E-04,-0.17293644E-03,
         0.12182917E-03,-0.14753959E-02,-0.87413221E-03,-0.31308184E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_800                                =v_p_e_q3en_800                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_800                                =v_p_e_q3en_800                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_800                                =v_p_e_q3en_800                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_800                                =v_p_e_q3en_800                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x23*x31    *x51
        ;

    return v_p_e_q3en_800                                ;
}
float l_e_q3en_800                                (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1499348E-01;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.14934811E-01,-0.31202349E+00,-0.25425661E-01, 0.12398725E-01,
        -0.29715015E-01,-0.16719295E-01,-0.55813403E-02,-0.53195730E-02,
         0.20689087E-02, 0.96759899E-02, 0.87815551E-02, 0.52084739E-03,
        -0.58424219E-04, 0.78598590E-03,-0.24764640E-02,-0.75105455E-03,
         0.36604810E-02,-0.21886956E-02, 0.16937538E-02, 0.72158780E-03,
         0.69262140E-03, 0.43131090E-02, 0.13186964E-02, 0.66100631E-03,
         0.10872594E-02, 0.17675929E-02,-0.14609291E-02, 0.56650313E-02,
         0.12099168E-02, 0.11592504E-02,-0.14537282E-02,-0.15777103E-02,
         0.21895212E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_800                                =v_l_e_q3en_800                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_800                                =v_l_e_q3en_800                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_q3en_800                                =v_l_e_q3en_800                                
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x22*x32    *x51
        +coeff[ 29]    *x24    *x42    
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]    *x23    *x42*x51
        +coeff[ 32]    *x21*x34*x42    
        ;

    return v_l_e_q3en_800                                ;
}
float x_e_q3en_700                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.3241945E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.12418989E-02, 0.11102300E+00, 0.22005081E+00, 0.28532775E-01,
         0.16941560E-01,-0.49696332E-05,-0.10829794E-01,-0.37980275E-02,
        -0.38131711E-02,-0.89998823E-02,-0.93319407E-02,-0.11633090E-02,
        -0.20875155E-02, 0.21695772E-02,-0.42739520E-02,-0.89691149E-03,
        -0.48669201E-03,-0.64107054E-03,-0.74971322E-03, 0.66772272E-03,
        -0.20357758E-03,-0.62405737E-02,-0.35473402E-02,-0.32601110E-02,
        -0.49802060E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_x_e_q3en_700                                =v_x_e_q3en_700                                
        +coeff[  8]            *x42    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_700                                =v_x_e_q3en_700                                
        +coeff[ 17]            *x42*x51
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x52
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_q3en_700                                ;
}
float t_e_q3en_700                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4122936E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.35317000E-02,-0.76525822E-01, 0.18235218E-01,-0.51505142E-02,
        -0.41170600E-02,-0.34430418E-05, 0.26813594E-02,-0.90901507E-03,
         0.45622970E-03,-0.75217290E-03, 0.17000352E-02, 0.79872587E-03,
         0.81804092E-03, 0.34209935E-03,-0.16580251E-03, 0.50999381E-03,
         0.33662847E-03, 0.35626543E-03, 0.92885850E-04,-0.78130768E-04,
        -0.24975455E-03, 0.72886230E-03, 0.13068641E-03,-0.11741272E-03,
         0.34742191E-03, 0.49380981E-03,-0.33108919E-03, 0.58155583E-04,
        -0.46176396E-03,-0.39867355E-03, 0.20702416E-02, 0.17570455E-02,
         0.15748732E-02, 0.63959662E-04,-0.30557472E-04, 0.65161359E-04,
         0.15756220E-02,-0.31301667E-03,-0.59291506E-05,-0.21323064E-03,
        -0.13617117E-04, 0.44895034E-04, 0.94435760E-04, 0.63491410E-04,
         0.47774869E-04, 0.77064964E-03, 0.18484310E-03,-0.15226938E-03,
         0.24025746E-03, 0.12611953E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_700                                =v_t_e_q3en_700                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_700                                =v_t_e_q3en_700                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_700                                =v_t_e_q3en_700                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x11        *x42    
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_q3en_700                                =v_t_e_q3en_700                                
        +coeff[ 35]                *x53
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]        *x33*x41*x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_q3en_700                                =v_t_e_q3en_700                                
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x22    *x42*x51
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]    *x22*x32*x42    
        +coeff[ 49]*x11    *x32        
        ;

    return v_t_e_q3en_700                                ;
}
float y_e_q3en_700                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2691085E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.28848680E-03, 0.11293711E+00,-0.79658948E-01,-0.66282958E-01,
        -0.22620050E-01, 0.48138585E-01, 0.25713971E-01,-0.31789377E-01,
        -0.12972057E-01,-0.72002592E-02, 0.24167888E-02,-0.95223794E-02,
        -0.31123906E-02,-0.22515738E-03,-0.66067139E-03, 0.87060675E-03,
        -0.55941660E-02, 0.16490009E-02,-0.22694857E-02,-0.17522597E-02,
         0.54668887E-02,-0.55650910E-02,-0.11653999E-02, 0.51795918E-03,
         0.17396165E-02,-0.22640610E-02,-0.11721205E-02, 0.22357446E-02,
         0.21705285E-02, 0.16365414E-02,-0.41522621E-02,-0.51516253E-02,
        -0.31019908E-02,-0.20934021E-03, 0.12376911E-02, 0.33396034E-03,
        -0.33999080E-03, 0.71873516E-03,-0.17287014E-02,-0.16381424E-02,
         0.12856175E-02, 0.23062852E-04,-0.18859835E-04, 0.14201984E-04,
        -0.51217532E-03, 0.17847096E-03, 0.31626516E-03,-0.88655337E-03,
         0.53248205E-03,-0.24402152E-04, 0.55912306E-03,-0.48083774E-03,
         0.42699734E-03,-0.17305397E-03, 0.12493804E-03, 0.66169561E-03,
         0.27373148E-03, 0.19097427E-03, 0.30763920E-02, 0.17768163E-02,
        -0.26480810E-03,-0.86801872E-02,-0.92184795E-02,-0.21146717E-02,
        -0.63827183E-05,-0.36778503E-04,-0.18164847E-04,-0.33584707E-04,
        -0.31699390E-04,-0.60789302E-04, 0.50093764E-04, 0.29788080E-02,
         0.48410977E-04, 0.24551435E-02,-0.29117696E-03, 0.16577007E-02,
        -0.56548179E-04,-0.68174285E-03, 0.32182151E-03,-0.16284782E-02,
        -0.22306961E-03, 0.46188903E-04,-0.26038256E-02,-0.46416037E-02,
        -0.15644880E-02,-0.11110333E-02, 0.19119455E-02, 0.80155424E-03,
         0.26438403E-03,-0.64601679E-03,-0.89058331E-05, 0.83617033E-05,
         0.14007896E-04, 0.13958672E-04,-0.21722859E-04, 0.11838874E-04,
         0.22027150E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]*x11*x21*x32*x41    
        +coeff[ 57]    *x23    *x43    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]    *x22            
        +coeff[ 65]*x12        *x41    
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11    *x31    *x51
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]*x11*x21    *x41*x51
        +coeff[ 70]        *x31    *x53
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 71]    *x21*x31*x44    
        +coeff[ 72]*x11    *x31*x42*x51
        +coeff[ 73]    *x21*x32*x43    
        +coeff[ 74]*x11*x22    *x41*x51
        +coeff[ 75]    *x21*x33*x42    
        +coeff[ 76]    *x22    *x43*x51
        +coeff[ 77]    *x22*x31*x42*x51
        +coeff[ 78]    *x23*x33        
        +coeff[ 79]    *x22    *x45    
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 80]    *x24*x31    *x51
        +coeff[ 81]*x13        *x43    
        +coeff[ 82]    *x24*x31*x42    
        +coeff[ 83]    *x22*x33*x42    
        +coeff[ 84]    *x24*x32*x41    
        +coeff[ 85]    *x22*x34*x41    
        +coeff[ 86]    *x23    *x45    
        +coeff[ 87]    *x21*x34*x43    
        +coeff[ 88]*x11*x24    *x41*x51
    ;
    v_y_e_q3en_700                                =v_y_e_q3en_700                                
        +coeff[ 89]    *x24    *x43*x51
        +coeff[ 90]        *x31*x41    
        +coeff[ 91]    *x21*x31*x41    
        +coeff[ 92]            *x42*x51
        +coeff[ 93]        *x31*x41*x51
        +coeff[ 94]    *x22    *x42    
        +coeff[ 95]*x12        *x42    
        +coeff[ 96]*x12*x21    *x41    
        ;

    return v_y_e_q3en_700                                ;
}
float p_e_q3en_700                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.6196403E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.61454572E-04,-0.21381557E-01, 0.10043755E-01,-0.56081382E-02,
        -0.12347361E-01, 0.43585091E-02, 0.10048538E-01,-0.80349501E-02,
        -0.43420502E-03,-0.16814858E-02,-0.25011881E-02, 0.42861907E-03,
        -0.18638850E-02,-0.11655173E-02, 0.50023140E-03, 0.12885397E-02,
        -0.63087960E-07,-0.82955550E-03, 0.37131074E-04, 0.25043276E-03,
        -0.25887138E-03,-0.11547573E-02,-0.17835597E-03,-0.87164820E-03,
        -0.83234377E-03, 0.12079233E-03,-0.31847990E-03, 0.66396547E-03,
         0.38584857E-03,-0.23465071E-03,-0.84260595E-04, 0.41275675E-03,
         0.21186918E-03,-0.11220197E-03, 0.24705241E-04,-0.19439371E-03,
        -0.14425488E-02,-0.84659539E-03,-0.30108154E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_700                                =v_p_e_q3en_700                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_700                                =v_p_e_q3en_700                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_700                                =v_p_e_q3en_700                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_700                                =v_p_e_q3en_700                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_700                                ;
}
float l_e_q3en_700                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1373371E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.13514278E-01,-0.31240949E+00,-0.25369339E-01, 0.12438869E-01,
        -0.28633852E-01,-0.16681280E-01,-0.54285252E-02,-0.50751283E-02,
         0.19539474E-02, 0.65553952E-02, 0.86900387E-02, 0.18571017E-03,
         0.84308820E-04, 0.11509859E-02,-0.26006077E-02,-0.86638355E-03,
         0.32704959E-02,-0.21458629E-02, 0.19882525E-03, 0.19465977E-02,
         0.49785007E-03, 0.14340343E-03, 0.45783489E-03,-0.79450506E-03,
        -0.59444224E-03, 0.81969576E-03, 0.83887577E-02, 0.22174325E-02,
         0.62935250E-02, 0.37665450E-03,-0.22443379E-02,-0.42373652E-03,
         0.38669880E-02, 0.51246984E-02, 0.97297570E-02, 0.51594866E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_700                                =v_l_e_q3en_700                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_700                                =v_l_e_q3en_700                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]        *x31*x41*x52
    ;
    v_l_e_q3en_700                                =v_l_e_q3en_700                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]        *x34    *x51
        +coeff[ 30]    *x22*x31*x41*x51
        +coeff[ 31]*x12            *x54
        +coeff[ 32]    *x23*x32*x42    
        +coeff[ 33]    *x23    *x44    
        +coeff[ 34]    *x21*x32*x42*x52
    ;
    v_l_e_q3en_700                                =v_l_e_q3en_700                                
        +coeff[ 35]    *x21*x31*x43*x52
        ;

    return v_l_e_q3en_700                                ;
}
float x_e_q3en_600                                (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.2919630E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.13003780E-02,-0.10829456E-01, 0.11103991E+00, 0.22018929E+00,
         0.28492766E-01, 0.17093319E-01,-0.37635411E-02,-0.36486685E-02,
        -0.89882594E-02,-0.91445912E-02,-0.11674416E-02,-0.20114556E-02,
         0.22640163E-02,-0.42436607E-02,-0.91200892E-03,-0.66645211E-03,
        -0.46173090E-03, 0.19274591E-03,-0.10982214E-02,-0.62449998E-02,
        -0.40449598E-02,-0.33415938E-02, 0.61263063E-03,-0.43270998E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_600                                =v_x_e_q3en_600                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_600                                =v_x_e_q3en_600                                
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_q3en_600                                ;
}
float t_e_q3en_600                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4404476E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.37633637E-02,-0.76595217E-01, 0.18240616E-01,-0.51630568E-02,
        -0.41309386E-02, 0.32148098E-05, 0.26811922E-02,-0.91529539E-03,
         0.47080481E-03,-0.76002092E-03, 0.17604806E-02, 0.84426726E-03,
         0.77463727E-03, 0.34719406E-03,-0.17914927E-03, 0.53463230E-03,
         0.33625204E-03, 0.28204013E-03, 0.84736945E-04,-0.10335130E-03,
        -0.25464536E-03, 0.75630267E-03, 0.12276327E-03,-0.11103734E-03,
         0.36804285E-03, 0.43328700E-03,-0.30529636E-03, 0.64906635E-04,
        -0.45733215E-03,-0.38241071E-03, 0.19908678E-02, 0.17032357E-02,
         0.15361233E-02,-0.35130248E-04, 0.60648534E-04, 0.15389004E-02,
        -0.30859371E-03, 0.53937707E-04,-0.20228198E-03,-0.11567556E-04,
         0.52275984E-04, 0.72039446E-04, 0.47888705E-04, 0.24032221E-04,
        -0.38739101E-04, 0.39658506E-04, 0.46338791E-04, 0.47494923E-04,
         0.74177852E-03, 0.19217393E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_600                                =v_t_e_q3en_600                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_600                                =v_t_e_q3en_600                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_600                                =v_t_e_q3en_600                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]                *x53
    ;
    v_t_e_q3en_600                                =v_t_e_q3en_600                                
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x12                
        +coeff[ 40]*x11        *x42    
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_q3en_600                                =v_t_e_q3en_600                                
        +coeff[ 44]*x11*x21    *x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]*x13    *x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_q3en_600                                ;
}
float y_e_q3en_600                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1939785E-03;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.20059777E-03, 0.11172567E+00,-0.80355093E-01,-0.66125870E-01,
        -0.22559818E-01, 0.48170973E-01, 0.25753915E-01,-0.31788815E-01,
        -0.13055209E-01,-0.72218701E-02, 0.24229039E-02,-0.96918205E-02,
        -0.30887639E-02,-0.11434298E-03,-0.50451164E-03, 0.86750230E-03,
        -0.56560454E-02, 0.16267848E-02,-0.22768257E-02,-0.17543886E-02,
         0.53420491E-02,-0.56323214E-02,-0.11832648E-02, 0.51651860E-03,
         0.17361785E-02,-0.24594106E-02,-0.11785869E-02, 0.20452279E-02,
         0.23771804E-02, 0.15286251E-02,-0.41635795E-02,-0.47128033E-02,
        -0.32961338E-02,-0.37337263E-03, 0.12231327E-02, 0.30206505E-03,
        -0.34496834E-03, 0.72001835E-03,-0.16854204E-02,-0.15469524E-02,
         0.12152040E-02, 0.63203683E-04,-0.50561375E-04,-0.25432798E-03,
         0.17842681E-03, 0.38177939E-03,-0.74377633E-03, 0.54592825E-03,
        -0.43308286E-04,-0.51012725E-04, 0.51254896E-03,-0.32389967E-03,
         0.46428991E-03, 0.12403741E-03, 0.65801828E-03, 0.29921276E-03,
         0.56036015E-03, 0.25915192E-02, 0.19588592E-02, 0.36441960E-03,
        -0.90872152E-02,-0.88129044E-02,-0.11069645E-02,-0.22598470E-02,
         0.89666801E-05,-0.33470094E-05,-0.20743115E-04,-0.28248529E-04,
        -0.35624998E-04,-0.61697348E-04,-0.11730174E-03, 0.43062530E-04,
         0.47932754E-04,-0.51550465E-04, 0.24448172E-02, 0.29087397E-02,
        -0.76892145E-04, 0.15599047E-02, 0.31240171E-03, 0.10107575E-03,
        -0.64882566E-03,-0.16259885E-02,-0.21010575E-03,-0.18486370E-03,
         0.23835094E-03,-0.27081813E-02,-0.48908181E-02, 0.35612838E-03,
        -0.15603963E-02,-0.98978251E-03, 0.19590463E-02, 0.14970838E-02,
        -0.14849864E-04, 0.40500245E-05,-0.35617875E-04, 0.23672606E-04,
         0.61742699E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21    *x43*x52
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 53]            *x41*x53
        +coeff[ 54]*x11*x21*x31*x42    
        +coeff[ 55]*x11*x21*x32*x41    
        +coeff[ 56]    *x23    *x43    
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x23*x32*x41    
        +coeff[ 59]    *x23*x33        
        +coeff[ 60]    *x22*x31*x44    
        +coeff[ 61]    *x22*x32*x43    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 62]    *x24    *x43*x51
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]    *x21            
        +coeff[ 65]                *x51
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11    *x31    *x51
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]*x11*x21    *x41*x51
        +coeff[ 70]        *x34*x41    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 71]*x12        *x41*x51
        +coeff[ 72]        *x31    *x53
        +coeff[ 73]            *x43*x52
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]    *x21*x32*x43    
        +coeff[ 76]*x11*x22    *x41*x51
        +coeff[ 77]    *x21*x33*x42    
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]    *x21    *x41*x53
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 80]    *x22*x31*x42*x51
        +coeff[ 81]    *x22    *x45    
        +coeff[ 82]    *x24*x31    *x51
        +coeff[ 83]    *x21    *x45*x51
        +coeff[ 84]    *x21*x31*x44*x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x23    *x43*x51
        +coeff[ 88]    *x24*x32*x41    
    ;
    v_y_e_q3en_600                                =v_y_e_q3en_600                                
        +coeff[ 89]    *x22*x34*x41    
        +coeff[ 90]    *x23    *x45    
        +coeff[ 91]    *x23*x31*x44    
        +coeff[ 92]    *x22            
        +coeff[ 93]*x11*x21            
        +coeff[ 94]    *x21    *x42    
        +coeff[ 95]    *x24            
        +coeff[ 96]    *x21    *x44    
        ;

    return v_y_e_q3en_600                                ;
}
float p_e_q3en_600                                (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.3705105E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.37313108E-04,-0.21546029E-01, 0.98129455E-02,-0.56186570E-02,
        -0.12385531E-01, 0.43617510E-02, 0.10043129E-01,-0.79975473E-02,
        -0.45746181E-03,-0.16812020E-02,-0.24039622E-02, 0.42712913E-03,
        -0.18534017E-02,-0.11544162E-02, 0.49892755E-03, 0.13079444E-02,
        -0.27575616E-05,-0.80144464E-03, 0.53339900E-04, 0.25054609E-03,
        -0.24997749E-03,-0.11573895E-02,-0.16942300E-03,-0.88898942E-03,
        -0.83069061E-03, 0.11947839E-03,-0.30780747E-03, 0.72779425E-03,
        -0.21249433E-03,-0.14455155E-03, 0.42406272E-03, 0.22072972E-03,
        -0.11445546E-03, 0.16276612E-04,-0.17494123E-03, 0.12897409E-03,
        -0.14659743E-02,-0.87277143E-03,-0.30414766E-03, 0.34970211E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_600                                =v_p_e_q3en_600                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_600                                =v_p_e_q3en_600                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_600                                =v_p_e_q3en_600                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22*x31    *x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_600                                =v_p_e_q3en_600                                
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        +coeff[ 39]    *x22    *x41*x52
        ;

    return v_p_e_q3en_600                                ;
}
float l_e_q3en_600                                (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1428148E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.14219647E-01,-0.31273237E+00,-0.25402155E-01, 0.12432079E-01,
        -0.28888693E-01,-0.16788960E-01,-0.55725514E-02,-0.53524948E-02,
         0.20767588E-02, 0.56880382E-02, 0.45991964E-02, 0.83251018E-03,
         0.24787153E-03, 0.24337750E-02,-0.28311084E-02,-0.89190091E-03,
         0.21757963E-02,-0.19676036E-02, 0.11905705E-02, 0.87971863E-03,
         0.65969983E-02, 0.74743468E-04,-0.15793947E-03,-0.31263768E-03,
         0.92354789E-03, 0.43236426E-03, 0.97046763E-03,-0.25136952E-03,
         0.78857541E-02, 0.11825311E-01, 0.11285702E-01, 0.46389089E-02,
         0.90712606E-03,-0.18627937E-02, 0.40014139E-02, 0.64768051E-02,
        -0.30966364E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_600                                =v_l_e_q3en_600                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_600                                =v_l_e_q3en_600                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x31        
        +coeff[ 22]                *x52
        +coeff[ 23]        *x33        
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_q3en_600                                =v_l_e_q3en_600                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]*x11    *x33        
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]    *x22*x32    *x51
        +coeff[ 33]    *x22    *x42*x51
        +coeff[ 34]    *x23*x33*x41    
    ;
    v_l_e_q3en_600                                =v_l_e_q3en_600                                
        +coeff[ 35]    *x21*x33*x41*x52
        +coeff[ 36]    *x21*x31*x41*x54
        ;

    return v_l_e_q3en_600                                ;
}
float x_e_q3en_500                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.1851610E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.95796748E-03,-0.10830852E-01, 0.11105850E+00, 0.22064003E+00,
         0.28358428E-01, 0.17151181E-01,-0.37764399E-02,-0.36444485E-02,
        -0.88235773E-02,-0.94380910E-02,-0.11778180E-02,-0.20195828E-02,
         0.22587383E-02,-0.42917142E-02,-0.92314702E-03,-0.64079562E-03,
        -0.43819437E-03, 0.40158743E-03,-0.10300885E-02,-0.65865074E-02,
        -0.34207699E-02,-0.35898478E-02,-0.53203157E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_500                                =v_x_e_q3en_500                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_500                                =v_x_e_q3en_500                                
        +coeff[ 17]    *x23        *x51
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_q3en_500                                ;
}
float t_e_q3en_500                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4001638E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.32515666E-02,-0.76692216E-01, 0.18236846E-01,-0.51710638E-02,
        -0.41174632E-02,-0.37039460E-05, 0.26812281E-02,-0.91303466E-03,
         0.46303251E-03,-0.76225511E-03, 0.16873016E-02, 0.81830693E-03,
         0.79602812E-03, 0.31888238E-03,-0.17473439E-03, 0.51391474E-03,
         0.38382437E-03, 0.29393949E-03, 0.77869074E-04,-0.12479752E-03,
        -0.22096380E-03, 0.73098077E-03, 0.12423116E-03,-0.10921845E-03,
         0.37514203E-03, 0.46345682E-03,-0.30687277E-03, 0.57766945E-04,
        -0.45455518E-03,-0.44277389E-03, 0.20348059E-02, 0.17489247E-02,
         0.15867646E-02, 0.66698783E-04,-0.46011111E-04, 0.62569459E-04,
         0.83558938E-04, 0.16374906E-02,-0.36883561E-03, 0.47813483E-04,
        -0.12593801E-04, 0.26050117E-04, 0.73369403E-04, 0.48023856E-04,
         0.38390623E-04,-0.43594609E-04, 0.49234040E-04, 0.45445002E-04,
         0.91573274E-04, 0.73465356E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_500                                =v_t_e_q3en_500                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_500                                =v_t_e_q3en_500                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_500                                =v_t_e_q3en_500                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21*x31*x43    
        +coeff[ 33]*x11        *x42    
        +coeff[ 34]*x11*x21        *x51
    ;
    v_t_e_q3en_500                                =v_t_e_q3en_500                                
        +coeff[ 35]                *x53
        +coeff[ 36]        *x32*x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x22*x31*x41*x51
        +coeff[ 39]        *x33*x41*x51
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_q3en_500                                =v_t_e_q3en_500                                
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]    *x22        *x52
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]*x11*x22*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_q3en_500                                ;
}
float y_e_q3en_500                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3118269E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.34617417E-03, 0.11014596E+00,-0.81345968E-01,-0.66099979E-01,
        -0.22455877E-01, 0.48238929E-01, 0.25796760E-01,-0.31845689E-01,
        -0.12880465E-01,-0.72032749E-02, 0.24069040E-02,-0.96228216E-02,
        -0.22702254E-02,-0.31263488E-02,-0.17436815E-03,-0.46551332E-03,
         0.86388265E-03,-0.57421681E-02, 0.16758915E-02,-0.17608979E-02,
         0.54908041E-02,-0.56282529E-02,-0.11571376E-02, 0.55132172E-03,
         0.16957618E-02,-0.21627350E-02,-0.12013060E-02, 0.21670246E-02,
         0.22351912E-02, 0.17134142E-02,-0.41628997E-02,-0.52100355E-02,
        -0.30442548E-02, 0.12226336E-02, 0.27331870E-03,-0.32993080E-03,
         0.72635314E-03,-0.17437753E-02,-0.17241581E-02,-0.10720652E-02,
         0.12556347E-02, 0.59336799E-04,-0.14842564E-03,-0.37531025E-03,
         0.18244062E-03, 0.40737569E-03, 0.57027413E-03,-0.32463777E-04,
         0.15549647E-04,-0.13177073E-04,-0.58300284E-04,-0.43846558E-04,
        -0.32698092E-04, 0.60693483E-03,-0.35172026E-03, 0.38939933E-03,
         0.56401314E-03, 0.22158348E-03, 0.15174020E-03, 0.25943755E-02,
         0.10333792E-02,-0.86855153E-02,-0.91060400E-02,-0.20423860E-02,
         0.59631693E-05, 0.22948041E-04,-0.23529936E-04,-0.25931406E-04,
        -0.61158513E-04,-0.19894136E-04, 0.14525265E-03, 0.52782951E-04,
         0.24462594E-02,-0.67453264E-04, 0.24201283E-02,-0.82535182E-04,
         0.18758688E-02,-0.45739691E-03,-0.71454844E-04,-0.59348822E-03,
         0.40541889E-03,-0.16156295E-02,-0.43187395E-03,-0.17362843E-03,
        -0.11839736E-04, 0.16485809E-03,-0.25371383E-02,-0.44891164E-02,
        -0.13881220E-02,-0.13465491E-02, 0.21426547E-02, 0.19824284E-02,
         0.17316507E-02, 0.24086634E-03, 0.58981898E-03, 0.12680604E-03,
         0.20327796E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]    *x24    *x41    
        +coeff[ 38]    *x24*x31        
        +coeff[ 39]    *x22*x33        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21    *x43*x52
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]            *x43*x53
        +coeff[ 48]    *x21            
        +coeff[ 49]                *x51
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]*x11    *x31    *x51
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 53]            *x43*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x23    *x43    
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]    *x22            
        +coeff[ 65]    *x22    *x42    
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]*x11*x22*x31        
        +coeff[ 68]*x11*x21    *x41*x51
        +coeff[ 69]        *x34*x41    
        +coeff[ 70]            *x41*x53
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 71]        *x31    *x53
        +coeff[ 72]    *x21*x31*x44    
        +coeff[ 73]*x11*x23*x31        
        +coeff[ 74]    *x21*x32*x43    
        +coeff[ 75]*x11*x22    *x41*x51
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22    *x43*x51
        +coeff[ 78]*x11*x21    *x41*x52
        +coeff[ 79]    *x22*x31*x42*x51
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 80]    *x23*x33        
        +coeff[ 81]    *x22    *x45    
        +coeff[ 82]    *x24    *x41*x51
        +coeff[ 83]    *x24*x31    *x51
        +coeff[ 84]    *x21    *x45*x51
        +coeff[ 85]    *x21*x31*x44*x51
        +coeff[ 86]    *x24*x31*x42    
        +coeff[ 87]    *x22*x33*x42    
        +coeff[ 88]    *x24*x32*x41    
    ;
    v_y_e_q3en_500                                =v_y_e_q3en_500                                
        +coeff[ 89]    *x22*x34*x41    
        +coeff[ 90]    *x23    *x45    
        +coeff[ 91]    *x23*x31*x44    
        +coeff[ 92]    *x23*x32*x43    
        +coeff[ 93]    *x21*x34*x43    
        +coeff[ 94]    *x23*x34*x41    
        +coeff[ 95]    *x23    *x41*x53
        +coeff[ 96]*x11    *x32*x41    
        ;

    return v_y_e_q3en_500                                ;
}
float p_e_q3en_500                                (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.6432788E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.67018002E-04,-0.21722144E-01, 0.95168883E-02,-0.56363181E-02,
        -0.12372117E-01, 0.43315687E-02, 0.10054497E-01,-0.80005573E-02,
        -0.44848502E-03,-0.16632059E-02,-0.24042875E-02, 0.42573462E-03,
        -0.18402725E-02,-0.11697934E-02, 0.49753138E-03, 0.12965433E-02,
        -0.58034487E-06,-0.80640998E-03, 0.74598109E-04, 0.25035135E-03,
        -0.24924838E-03,-0.11885162E-02,-0.16662465E-03,-0.86878001E-03,
        -0.82071335E-03, 0.11842426E-03,-0.31109239E-03, 0.69947878E-03,
        -0.22324790E-03,-0.19580028E-03, 0.43190148E-03, 0.22050508E-03,
         0.15224739E-03,-0.10981280E-03,-0.28000238E-05, 0.12078351E-03,
        -0.15044707E-02,-0.84976695E-03,-0.31764721E-03, 0.36698868E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_q3en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_500                                =v_p_e_q3en_500                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_q3en_500                                =v_p_e_q3en_500                                
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_500                                =v_p_e_q3en_500                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22*x31    *x52
        +coeff[ 29]    *x21*x31    *x54
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_500                                =v_p_e_q3en_500                                
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        +coeff[ 39]    *x22    *x41*x52
        ;

    return v_p_e_q3en_500                                ;
}
float l_e_q3en_500                                (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1287005E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.12770948E-01,-0.31333005E+00,-0.25454501E-01, 0.12378432E-01,
        -0.28960558E-01,-0.16903261E-01,-0.53156456E-02,-0.52177832E-02,
         0.20860885E-02, 0.84746219E-02, 0.81959022E-02,-0.23314105E-02,
         0.62633824E-03, 0.36538934E-03, 0.15541830E-02,-0.29502437E-02,
        -0.84554189E-03, 0.35284196E-02,-0.27072034E-03, 0.14401510E-02,
         0.87731174E-03,-0.80865371E-04, 0.60155423E-03, 0.78968878E-03,
         0.59689942E-03,-0.32486452E-03, 0.69623902E-02, 0.61855959E-02,
        -0.14683967E-02, 0.28667096E-02,-0.42841432E-03, 0.11253267E-02,
        -0.95916440E-03, 0.14599694E-02, 0.21230313E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_500                                =v_l_e_q3en_500                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3en_500                                =v_l_e_q3en_500                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x52
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]            *x43*x51
    ;
    v_l_e_q3en_500                                =v_l_e_q3en_500                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]        *x33*x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]*x12*x22    *x41    
        +coeff[ 31]    *x22    *x44    
        +coeff[ 32]*x12        *x42*x52
        +coeff[ 33]    *x21*x34*x41*x51
        +coeff[ 34]*x12    *x33*x42    
        ;

    return v_l_e_q3en_500                                ;
}
float x_e_q3en_450                                (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1964474E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.16482829E-02,-0.10829219E-01, 0.11103902E+00, 0.22095330E+00,
         0.28570963E-01, 0.17186303E-01,-0.37876342E-02,-0.36257561E-02,
        -0.90349633E-02,-0.94819339E-02,-0.11605666E-02,-0.20080325E-02,
         0.22234393E-02,-0.42986032E-02,-0.91085362E-03,-0.66529022E-03,
         0.98447796E-04,-0.40748008E-03,-0.10025278E-02,-0.23222910E-03,
        -0.62473267E-02,-0.32761653E-02,-0.33728550E-02,-0.52018911E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_450                                =v_x_e_q3en_450                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3en_450                                =v_x_e_q3en_450                                
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_q3en_450                                ;
}
float t_e_q3en_450                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4601874E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.37374478E-02, 0.26723209E-02,-0.76815896E-01, 0.18244559E-01,
        -0.52050045E-02,-0.41551851E-02,-0.91330172E-03, 0.48680813E-03,
        -0.74913440E-03, 0.17825773E-02, 0.89665496E-03, 0.78679901E-03,
         0.32657170E-03,-0.17139113E-03, 0.52664144E-03, 0.31760891E-03,
         0.34881817E-03, 0.95749514E-04,-0.70828020E-04,-0.25080377E-03,
         0.72123774E-03, 0.13280740E-03,-0.11505690E-03, 0.33081137E-03,
         0.48053311E-03,-0.26566666E-03, 0.85698091E-04,-0.45519811E-03,
        -0.29304510E-03, 0.19848789E-02, 0.15970513E-02,-0.49936261E-04,
         0.64697255E-04, 0.15153760E-02, 0.15030400E-02,-0.31476901E-03,
         0.16922515E-04, 0.64698012E-04, 0.73951538E-04, 0.35162950E-04,
         0.27609945E-04, 0.11000456E-03, 0.71378087E-03, 0.20161315E-03,
        -0.12906524E-03,-0.14837828E-03, 0.29087870E-03,-0.26552554E-03,
        -0.92252385E-05, 0.20641683E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_450                                =v_t_e_q3en_450                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_450                                =v_t_e_q3en_450                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_450                                =v_t_e_q3en_450                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]                *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_q3en_450                                =v_t_e_q3en_450                                
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]        *x33*x41*x51
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]        *x31*x41*x51
        +coeff[ 39]        *x32    *x52
        +coeff[ 40]*x13    *x31*x41    
        +coeff[ 41]*x11*x22*x31*x41    
        +coeff[ 42]    *x21*x33*x41    
        +coeff[ 43]    *x22    *x42*x51
    ;
    v_t_e_q3en_450                                =v_t_e_q3en_450                                
        +coeff[ 44]    *x23        *x52
        +coeff[ 45]    *x21*x31*x41*x52
        +coeff[ 46]    *x22*x32*x42    
        +coeff[ 47]    *x23    *x42*x51
        +coeff[ 48]*x12                
        +coeff[ 49]*x11    *x32        
        ;

    return v_t_e_q3en_450                                ;
}
float y_e_q3en_450                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1455380E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.26236835E-03, 0.10893463E+00,-0.82001336E-01,-0.66029549E-01,
        -0.22433998E-01, 0.48286337E-01, 0.25817132E-01,-0.31846795E-01,
        -0.13041865E-01,-0.72606518E-02, 0.24117681E-02,-0.96876528E-02,
        -0.22696010E-02,-0.31062397E-02,-0.19589339E-03,-0.57570066E-03,
         0.85951498E-03,-0.56277327E-02,-0.11878102E-02, 0.16116721E-02,
        -0.17616305E-02, 0.54514157E-02,-0.55550500E-02, 0.54726360E-03,
         0.17183573E-02,-0.21579186E-02,-0.11948264E-02, 0.21340568E-02,
         0.26371377E-02, 0.17932400E-02,-0.42186449E-02,-0.47420007E-02,
        -0.29853641E-02, 0.62555686E-04,-0.49484457E-04, 0.12002579E-02,
         0.32257126E-03,-0.34486054E-03, 0.72451151E-03,-0.16970607E-02,
        -0.15306073E-02,-0.79656811E-03, 0.13301584E-02,-0.26022465E-06,
        -0.13978583E-03, 0.29781402E-04,-0.34029435E-03, 0.18515089E-03,
         0.41913867E-03, 0.54244354E-03,-0.56463083E-04,-0.49063543E-04,
        -0.35028002E-04, 0.59774792E-03,-0.28240436E-03, 0.49651176E-03,
        -0.10079713E-03, 0.10336929E-03, 0.67862478E-03, 0.31924710E-03,
         0.78808422E-04, 0.21975825E-02, 0.13380318E-02,-0.88572679E-02,
        -0.90363994E-02,-0.20001067E-02, 0.19830613E-04, 0.89347950E-05,
         0.92179616E-04,-0.25722526E-04, 0.31139127E-04,-0.33253469E-04,
        -0.59690363E-04, 0.53003325E-04, 0.17212917E-02,-0.66761633E-04,
         0.19322343E-02,-0.73280411E-04,-0.35975194E-04, 0.14391484E-02,
        -0.42409592E-03, 0.10391777E-03,-0.53719821E-03, 0.34037049E-03,
        -0.17277204E-02,-0.45447267E-03,-0.20558892E-03, 0.16855991E-03,
        -0.29349648E-02,-0.48583476E-02,-0.17837937E-02,-0.12722277E-02,
        -0.33886632E-03, 0.23467992E-02,-0.10367405E-03, 0.29977781E-02,
         0.22255233E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x21            
        +coeff[ 34]                *x51
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]    *x23    *x41*x51
        +coeff[ 43]            *x45*x51
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 44]    *x21    *x43*x52
        +coeff[ 45]    *x22            
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]    *x21    *x41*x52
        +coeff[ 49]    *x22    *x41*x52
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]*x11    *x31    *x51
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 53]            *x43*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]            *x41*x53
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x43    
        +coeff[ 61]    *x23*x31*x42    
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x22*x31*x44    
        +coeff[ 64]    *x22*x32*x43    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]        *x31*x41    
        +coeff[ 67]    *x21        *x51
        +coeff[ 68]    *x22    *x42    
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x11    *x31*x42    
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 71]*x11*x22*x31        
        +coeff[ 72]*x11*x21    *x41*x51
        +coeff[ 73]        *x31    *x53
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]*x11*x23*x31        
        +coeff[ 76]    *x21*x32*x43    
        +coeff[ 77]*x11*x22    *x41*x51
        +coeff[ 78]*x11*x21    *x44    
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 80]    *x22    *x43*x51
        +coeff[ 81]    *x21    *x41*x53
        +coeff[ 82]    *x22*x31*x42*x51
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]    *x22    *x45    
        +coeff[ 85]    *x24    *x41*x51
        +coeff[ 86]    *x24*x31    *x51
        +coeff[ 87]    *x21*x31*x44*x51
        +coeff[ 88]    *x24*x31*x42    
    ;
    v_y_e_q3en_450                                =v_y_e_q3en_450                                
        +coeff[ 89]    *x22*x33*x42    
        +coeff[ 90]    *x24*x32*x41    
        +coeff[ 91]    *x22*x34*x41    
        +coeff[ 92]    *x24*x33        
        +coeff[ 93]    *x23    *x45    
        +coeff[ 94]    *x24    *x44    
        +coeff[ 95]    *x23*x31*x44    
        +coeff[ 96]    *x23*x32*x43    
        ;

    return v_y_e_q3en_450                                ;
}
float p_e_q3en_450                                (float *x,int m){
    int ncoeff= 41;
    float avdat=  0.5922647E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
        -0.66585344E-04,-0.21814041E-01, 0.92994198E-02,-0.56183068E-02,
        -0.12371916E-01, 0.43243282E-02, 0.10047263E-01,-0.80423411E-02,
        -0.44102233E-03,-0.16578261E-02,-0.24703415E-02, 0.42533642E-03,
        -0.18398804E-02,-0.11601192E-02, 0.12863231E-02,-0.61968608E-05,
        -0.82065660E-03, 0.49500642E-04, 0.16971935E-04, 0.25555276E-03,
        -0.25261799E-03,-0.11661355E-02,-0.15932901E-03,-0.84447302E-03,
        -0.82149706E-03, 0.48354125E-03,-0.32552372E-03, 0.69920730E-03,
         0.17815574E-03, 0.44078249E-03,-0.81697661E-04, 0.41410321E-03,
         0.22116459E-03, 0.14846980E-03,-0.10357546E-03, 0.32806027E-04,
        -0.17089822E-03, 0.12785979E-03,-0.15266824E-02,-0.87589887E-03,
        -0.30933690E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_450                                =v_p_e_q3en_450                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_450                                =v_p_e_q3en_450                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x13*x21    *x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3en_450                                =v_p_e_q3en_450                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_q3en_450                                =v_p_e_q3en_450                                
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x21*x31    *x52
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x23*x31    *x51
        ;

    return v_p_e_q3en_450                                ;
}
float l_e_q3en_450                                (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1453822E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.14266775E-01,-0.31367308E+00,-0.25484324E-01, 0.12319131E-01,
        -0.29310659E-01,-0.16990108E-01,-0.58361064E-02,-0.53519458E-02,
         0.20194710E-02, 0.73783733E-02, 0.80373883E-02,-0.17861764E-03,
        -0.24010350E-03, 0.22332813E-02,-0.78152219E-03, 0.32510769E-02,
        -0.15763573E-02,-0.21732396E-02,-0.21829199E-03, 0.12205928E-02,
         0.70522825E-03, 0.82321482E-03, 0.72401733E-03, 0.65728644E-03,
         0.14953382E-02, 0.82237739E-02, 0.63871341E-02, 0.58096959E-02,
         0.90998935E-03, 0.12126917E-02, 0.49236640E-02,-0.19466524E-02,
         0.20964448E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_450                                =v_l_e_q3en_450                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3en_450                                =v_l_e_q3en_450                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3en_450                                =v_l_e_q3en_450                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x34    *x51
        +coeff[ 29]    *x24    *x42    
        +coeff[ 30]    *x21*x32*x44    
        +coeff[ 31]    *x22*x34    *x51
        +coeff[ 32]*x12*x21*x33*x41    
        ;

    return v_l_e_q3en_450                                ;
}
float x_e_q3en_449                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.3711005E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.18320099E-02,-0.10680620E-01, 0.11107146E+00, 0.22329216E+00,
         0.28294055E-01, 0.17474851E-01,-0.37756541E-02,-0.37210651E-02,
        -0.10128333E-01,-0.94673345E-02,-0.11464001E-02,-0.22738534E-02,
         0.22405456E-02,-0.51796315E-02,-0.11195628E-02,-0.46929301E-03,
        -0.67683490E-03, 0.44971771E-03,-0.10834862E-02,-0.63540135E-02,
        -0.31807937E-02,-0.34511485E-02,-0.49965521E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_449                                =v_x_e_q3en_449                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11*x22            
        +coeff[ 16]            *x42*x51
    ;
    v_x_e_q3en_449                                =v_x_e_q3en_449                                
        +coeff[ 17]    *x23        *x51
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_q3en_449                                ;
}
float t_e_q3en_449                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4579797E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.38081708E-02, 0.26424811E-02,-0.77519707E-01, 0.18241340E-01,
        -0.52961353E-02,-0.40542013E-02,-0.91748976E-03, 0.45813742E-03,
        -0.84913184E-03, 0.19063835E-02, 0.75899338E-03, 0.99108974E-03,
         0.37286663E-03,-0.16499513E-03, 0.64508623E-03, 0.32925210E-03,
         0.33422667E-03, 0.92862778E-04,-0.11027075E-03, 0.80778002E-03,
         0.12163140E-03,-0.29291469E-03,-0.16124605E-03, 0.45453216E-03,
         0.49811386E-03,-0.33579118E-03, 0.75328404E-04,-0.50833030E-03,
        -0.40994780E-03, 0.22476048E-02, 0.17153388E-02, 0.56142664E-04,
        -0.45124169E-04, 0.66362511E-04, 0.19470491E-02, 0.17115732E-02,
         0.30836061E-03,-0.12508668E-04, 0.49076731E-04, 0.90612026E-04,
         0.49899438E-04, 0.43687087E-04,-0.46654961E-04, 0.68099092E-04,
         0.50153576E-04, 0.97622379E-03, 0.15944576E-03,-0.16273493E-03,
        -0.11352242E-03,-0.19720891E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_449                                =v_t_e_q3en_449                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_449                                =v_t_e_q3en_449                                
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_449                                =v_t_e_q3en_449                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_449                                =v_t_e_q3en_449                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]*x12                
        +coeff[ 38]*x11    *x31*x41    
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11*x23            
        +coeff[ 41]        *x33*x41    
        +coeff[ 42]    *x22        *x52
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_q3en_449                                =v_t_e_q3en_449                                
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x22*x32    *x51
        +coeff[ 47]    *x22*x31*x41*x51
        +coeff[ 48]    *x23        *x52
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q3en_449                                ;
}
float y_e_q3en_449                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9283733E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.92358392E-03, 0.11654768E+00,-0.86870074E-01,-0.67066245E-01,
        -0.25439674E-01, 0.47691084E-01, 0.28220190E-01,-0.31958502E-01,
        -0.14350206E-01,-0.72137704E-02, 0.24017887E-02,-0.10091139E-01,
        -0.65065972E-02,-0.25093297E-02,-0.45406644E-03, 0.95471879E-03,
        -0.52270433E-02,-0.15455626E-02, 0.16197939E-02,-0.30884668E-02,
        -0.19255279E-02, 0.51271268E-02, 0.57288888E-03, 0.18808795E-02,
        -0.20098491E-02,-0.12708887E-02, 0.48219165E-03, 0.16040389E-02,
         0.89700002E-03,-0.64919805E-02,-0.98022800E-02,-0.68444381E-02,
         0.16389637E-03, 0.12871915E-02, 0.34120717E-03, 0.88338921E-03,
        -0.22147070E-02,-0.21561787E-02, 0.13047447E-02,-0.10196270E-03,
         0.13584098E-03, 0.23850336E-03, 0.34360631E-03,-0.16452321E-02,
         0.52796974E-03,-0.51081897E-04,-0.60393897E-04,-0.37603528E-04,
         0.67928986E-03,-0.43610536E-03,-0.93433470E-03,-0.13971456E-02,
        -0.64390973E-04,-0.94312761E-03,-0.37959573E-03, 0.10750449E-03,
         0.75638061E-03, 0.23668159E-02, 0.40063676E-02, 0.28124610E-02,
         0.59986487E-03,-0.77282166E-04,-0.36712201E-04,-0.27457258E-04,
        -0.37285878E-04, 0.54949272E-03, 0.15176530E-02, 0.51246290E-04,
         0.37804563E-03, 0.41199010E-02, 0.52082855E-02, 0.32866632E-02,
        -0.56894560E-03, 0.92432927E-03, 0.63455627E-04,-0.52848650E-03,
        -0.52289374E-03,-0.52698241E-02,-0.27065538E-03, 0.16428300E-02,
        -0.48338142E-02,-0.20293051E-02, 0.18774165E-03,-0.25062205E-02,
        -0.28526872E-05, 0.11361169E-05,-0.94883471E-05, 0.20828002E-04,
        -0.52062146E-05, 0.19773986E-04, 0.89840396E-05,-0.33776254E-04,
        -0.57707890E-04, 0.17834698E-04, 0.22998282E-04, 0.82139013E-04,
        -0.28421287E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x24*x33        
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x24*x31        
        +coeff[ 38]    *x23    *x41*x51
        +coeff[ 39]            *x45*x51
        +coeff[ 40]*x11*x24    *x41    
        +coeff[ 41]        *x33    *x51
        +coeff[ 42]    *x21    *x41*x52
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]*x12        *x41    
        +coeff[ 46]*x11        *x41*x51
        +coeff[ 47]*x11    *x31    *x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x32*x43    
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]        *x34*x41    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]    *x23    *x43    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x23*x33        
        +coeff[ 61]*x11*x21    *x45    
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 62]*x11*x21*x34*x41    
        +coeff[ 63]*x12    *x31        
        +coeff[ 64]*x11*x22*x31        
        +coeff[ 65]*x11*x21    *x43    
        +coeff[ 66]    *x21    *x45    
        +coeff[ 67]        *x31    *x53
        +coeff[ 68]*x11*x21*x32*x41    
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x21*x32*x43    
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 71]    *x21*x33*x42    
        +coeff[ 72]    *x22    *x43*x51
        +coeff[ 73]    *x21*x34*x41    
        +coeff[ 74]    *x21    *x41*x53
        +coeff[ 75]    *x22*x31*x42*x51
        +coeff[ 76]    *x24    *x41*x51
        +coeff[ 77]    *x22*x31*x44    
        +coeff[ 78]    *x24*x31    *x51
        +coeff[ 79]    *x24    *x43    
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 80]    *x22*x32*x43    
        +coeff[ 81]    *x22*x33*x42    
        +coeff[ 82]    *x22    *x45*x51
        +coeff[ 83]    *x24    *x45    
        +coeff[ 84]    *x21            
        +coeff[ 85]                *x51
        +coeff[ 86]            *x42    
        +coeff[ 87]    *x21    *x42    
        +coeff[ 88]                *x52
    ;
    v_y_e_q3en_449                                =v_y_e_q3en_449                                
        +coeff[ 89]            *x44    
        +coeff[ 90]        *x32    *x51
        +coeff[ 91]*x11*x21*x31*x41    
        +coeff[ 92]    *x21    *x44    
        +coeff[ 93]*x11*x21*x31    *x51
        +coeff[ 94]*x11    *x31*x43    
        +coeff[ 95]    *x21*x31*x42*x51
        +coeff[ 96]        *x31*x45    
        ;

    return v_y_e_q3en_449                                ;
}
float p_e_q3en_449                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1136131E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.11165253E-03,-0.23405226E-01, 0.10767988E-01,-0.63007222E-02,
        -0.12551141E-01, 0.47552432E-02, 0.10000766E-01,-0.82204212E-02,
        -0.44368900E-03,-0.16916826E-02,-0.28830292E-02, 0.41798365E-03,
        -0.20248713E-02,-0.11669850E-02, 0.13431185E-02,-0.10362658E-05,
        -0.90805441E-03, 0.74342672E-04, 0.27267798E-03,-0.33315198E-03,
        -0.14327207E-02,-0.19946827E-03, 0.49999612E-03,-0.93690457E-03,
        -0.77879493E-03, 0.13282616E-03,-0.28110031E-03, 0.74517459E-03,
         0.48671482E-03,-0.99128403E-04, 0.40630987E-03, 0.18738334E-03,
         0.16065258E-03,-0.11483386E-03,-0.18793702E-03, 0.13109855E-03,
        -0.15463306E-02,-0.81871566E-03,-0.31642761E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_449                                =v_p_e_q3en_449                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_449                                =v_p_e_q3en_449                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_449                                =v_p_e_q3en_449                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_449                                =v_p_e_q3en_449                                
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_449                                ;
}
float l_e_q3en_449                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1535797E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.14740221E-01,-0.31602448E+00,-0.25572142E-01, 0.12214922E-01,
        -0.29495450E-01,-0.16779220E-01,-0.56633996E-02,-0.52355421E-02,
         0.19633432E-02, 0.81966771E-02, 0.47980002E-02, 0.25416893E-03,
         0.16864018E-03, 0.44001047E-02,-0.31343114E-02,-0.77171944E-03,
        -0.11873599E-02, 0.22255750E-02,-0.18834047E-02, 0.13510140E-02,
         0.70604001E-03, 0.61119610E-03, 0.79891214E-03, 0.76765422E-03,
        -0.26783874E-03, 0.62345355E-02, 0.68786615E-02, 0.11447687E-01,
         0.86769406E-02, 0.33097689E-02, 0.77800020E-02,-0.14598873E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_449                                =v_l_e_q3en_449                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3en_449                                =v_l_e_q3en_449                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3en_449                                =v_l_e_q3en_449                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21    *x44    
        +coeff[ 30]    *x23*x33*x41    
        +coeff[ 31]*x11    *x34*x41*x51
        ;

    return v_l_e_q3en_449                                ;
}
float x_e_q3en_400                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5223004E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.33653874E-02,-0.10655673E-01, 0.11105426E+00, 0.22402737E+00,
         0.28376907E-01, 0.17457232E-01,-0.38000513E-02,-0.38355717E-02,
        -0.11669681E-01,-0.92817862E-02,-0.11679373E-02,-0.23323959E-02,
         0.22383602E-02,-0.53432025E-02,-0.11247365E-02,-0.48432243E-03,
        -0.68928621E-03,-0.68035146E-03, 0.73248055E-03,-0.21500007E-03,
        -0.42640055E-02,-0.30543855E-02, 0.78887865E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_400                                =v_x_e_q3en_400                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11*x22            
        +coeff[ 16]            *x42*x51
    ;
    v_x_e_q3en_400                                =v_x_e_q3en_400                                
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23*x32    *x51
        ;

    return v_x_e_q3en_400                                ;
}
float t_e_q3en_400                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5497559E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47494168E-02, 0.26333563E-02,-0.77727355E-01, 0.18222731E-01,
        -0.53537944E-02,-0.40443665E-02,-0.92453387E-03, 0.45624285E-03,
        -0.83193567E-03, 0.18382173E-02, 0.72338298E-03, 0.94176212E-03,
         0.37048495E-03,-0.16655754E-03, 0.67332783E-03, 0.37384470E-03,
         0.34628619E-03, 0.49057463E-03, 0.91966373E-04,-0.11932921E-03,
         0.79492538E-03, 0.12560128E-03,-0.24132640E-03,-0.11212505E-03,
         0.46409349E-03,-0.32217259E-03, 0.67164947E-04,-0.52753068E-03,
        -0.42524413E-03, 0.22306058E-02, 0.17644813E-02,-0.46193953E-04,
         0.66674402E-04, 0.19186842E-02, 0.16675884E-02,-0.36311679E-03,
        -0.15582076E-04, 0.11349401E-03,-0.12027745E-04, 0.62352672E-04,
         0.11145405E-03, 0.62352883E-04, 0.61827894E-04, 0.51921808E-04,
         0.27085218E-04, 0.10886170E-03, 0.99732238E-03, 0.20436407E-03,
        -0.14775904E-03,-0.10223761E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_400                                =v_t_e_q3en_400                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_400                                =v_t_e_q3en_400                                
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_400                                =v_t_e_q3en_400                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]                *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_q3en_400                                =v_t_e_q3en_400                                
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]        *x33*x41*x51
        +coeff[ 37]    *x22*x33*x41    
        +coeff[ 38]*x12                
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_400                                =v_t_e_q3en_400                                
        +coeff[ 44]*x13    *x31*x41    
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x22    *x42*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_q3en_400                                ;
}
float y_e_q3en_400                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1099642E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.96964027E-03, 0.11599593E+00,-0.87315857E-01,-0.67394830E-01,
        -0.25637127E-01, 0.47672372E-01, 0.28202962E-01,-0.32346815E-01,
        -0.14349921E-01,-0.71575237E-02, 0.23942199E-02,-0.10293631E-01,
        -0.66534048E-02,-0.24923389E-02,-0.14236287E-03, 0.94965112E-03,
        -0.54793260E-02,-0.15384769E-02, 0.16390699E-02,-0.30878924E-02,
        -0.19313715E-02, 0.56368122E-02, 0.57937822E-03, 0.22598130E-02,
        -0.21203442E-02,-0.13522621E-02, 0.18315526E-02, 0.43801637E-02,
         0.23879830E-02,-0.46107988E-02,-0.89125857E-02,-0.60966481E-02,
         0.25724541E-03, 0.45414810E-04, 0.12861012E-02, 0.67550893E-03,
        -0.36551646E-03, 0.87864569E-03,-0.20010737E-02,-0.22418185E-02,
         0.13205664E-02, 0.70746632E-04,-0.10727902E-04,-0.30100826E-04,
         0.23380881E-03,-0.16850972E-02, 0.54422097E-03, 0.23733299E-04,
        -0.31967120E-04,-0.60408187E-04, 0.50103490E-03,-0.60328830E-03,
        -0.86442719E-03, 0.35787458E-03,-0.77000610E-03, 0.45024388E-03,
        -0.23327641E-03, 0.13759869E-03, 0.70424553E-03, 0.34899553E-03,
        -0.98669552E-03,-0.55390707E-03,-0.70489426E-02, 0.23464330E-02,
         0.80885673E-02, 0.94006872E-02, 0.55923412E-03, 0.60366341E-02,
         0.15245146E-02,-0.24877372E-04,-0.29357407E-04,-0.44182634E-04,
        -0.31640418E-04, 0.29130309E-03, 0.58792226E-04, 0.33715332E-03,
        -0.10041303E-03, 0.70451911E-05, 0.53952179E-04,-0.40271078E-03,
        -0.21201819E-02, 0.54394169E-03,-0.20108589E-03,-0.70124483E-02,
        -0.21399315E-03,-0.33503037E-02,-0.74598263E-03, 0.33130212E-03,
        -0.31319112E-03,-0.70440944E-03, 0.17001927E-04, 0.32857429E-05,
         0.22515907E-04,-0.21647302E-04, 0.15721967E-04, 0.65392436E-04,
        -0.40121133E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x24*x33        
        +coeff[ 33]    *x21            
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21    *x43*x52
        +coeff[ 43]                *x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]    *x22            
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x32*x43    
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]            *x41*x53
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x24    *x41*x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 62]    *x22*x31*x44    
        +coeff[ 63]    *x23    *x45    
        +coeff[ 64]    *x23*x31*x44    
        +coeff[ 65]    *x23*x32*x43    
        +coeff[ 66]    *x21*x34*x43    
        +coeff[ 67]    *x23*x33*x42    
        +coeff[ 68]    *x23*x34*x41    
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x11    *x31    *x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 71]*x11*x22*x31        
        +coeff[ 72]*x11*x21    *x41*x51
        +coeff[ 73]    *x21    *x45    
        +coeff[ 74]        *x31    *x53
        +coeff[ 75]    *x21*x32*x43    
        +coeff[ 76]*x11*x22    *x41*x51
        +coeff[ 77]*x11    *x32*x41*x51
        +coeff[ 78]    *x22*x31    *x52
        +coeff[ 79]    *x22*x31*x42*x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 80]    *x22    *x45    
        +coeff[ 81]        *x33*x44    
        +coeff[ 82]    *x24*x31    *x51
        +coeff[ 83]    *x22*x32*x43    
        +coeff[ 84]*x11*x21*x32*x41*x51
        +coeff[ 85]    *x22*x33*x42    
        +coeff[ 86]    *x22*x34*x41    
        +coeff[ 87]*x11    *x32*x43*x51
        +coeff[ 88]    *x22    *x45*x51
    ;
    v_y_e_q3en_400                                =v_y_e_q3en_400                                
        +coeff[ 89]    *x24    *x45    
        +coeff[ 90]        *x31*x41    
        +coeff[ 91]            *x44    
        +coeff[ 92]            *x42*x52
        +coeff[ 93]*x12    *x31*x41    
        +coeff[ 94]            *x44*x51
        +coeff[ 95]    *x21*x31*x42*x51
        +coeff[ 96]*x12        *x43    
        ;

    return v_y_e_q3en_400                                ;
}
float p_e_q3en_400                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1901275E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.17445566E-03,-0.23483200E-01, 0.10656247E-01,-0.62984154E-02,
        -0.12523993E-01, 0.47741262E-02, 0.99864295E-02,-0.82261991E-02,
        -0.45020704E-03,-0.16948269E-02,-0.28737334E-02, 0.41893718E-03,
        -0.20336560E-02,-0.11454201E-02, 0.13552387E-02,-0.36976351E-05,
        -0.89340459E-03, 0.56458597E-04, 0.27229654E-03,-0.33226883E-03,
        -0.14026497E-02,-0.17632567E-03, 0.50265325E-03,-0.96089090E-03,
        -0.80664741E-03, 0.13396003E-03,-0.27432188E-03, 0.72031398E-03,
         0.39769121E-03,-0.45717700E-03,-0.87560118E-04, 0.42451077E-03,
         0.18328811E-03,-0.12286956E-03,-0.21916629E-03,-0.14363915E-02,
        -0.82359911E-03,-0.35391655E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_400                                =v_p_e_q3en_400                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_400                                =v_p_e_q3en_400                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_400                                =v_p_e_q3en_400                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24    *x41*x52
        +coeff[ 29]    *x22*x31*x42*x52
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_400                                =v_p_e_q3en_400                                
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x23*x31    *x51
        ;

    return v_p_e_q3en_400                                ;
}
float l_e_q3en_400                                (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1840051E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.17970679E-01,-0.31752738E+00,-0.25291540E-01, 0.12224392E-01,
        -0.29959980E-01,-0.16635835E-01,-0.62817144E-02,-0.55207834E-02,
         0.21000400E-02, 0.89259902E-02, 0.90048788E-02,-0.13717744E-02,
         0.47481990E-04, 0.12683540E-02,-0.30192223E-02,-0.88199531E-03,
         0.37680911E-02,-0.22095509E-02, 0.12193453E-02, 0.80112665E-03,
         0.13377307E-02,-0.20102150E-03, 0.33966717E-04, 0.52907929E-03,
         0.26388674E-02, 0.12491070E-02, 0.75220214E-02, 0.27480091E-02,
         0.54829367E-02,-0.57790475E-03, 0.31115825E-02, 0.13457710E-02,
         0.78712142E-03, 0.49714861E-02, 0.32799095E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_400                                =v_l_e_q3en_400                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_400                                =v_l_e_q3en_400                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x34    *x51
        +coeff[ 20]*x11*x22        *x52
        +coeff[ 21]                *x52
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_q3en_400                                =v_l_e_q3en_400                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x11        *x43*x51
        +coeff[ 30]    *x24*x32        
        +coeff[ 31]    *x22*x32*x42    
        +coeff[ 32]*x11    *x32    *x53
        +coeff[ 33]    *x21*x34*x42    
        +coeff[ 34]    *x23    *x44    
        ;

    return v_l_e_q3en_400                                ;
}
float x_e_q3en_350                                (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.4129359E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.20614790E-02,-0.10632666E-01, 0.11110667E+00, 0.22481625E+00,
         0.28282607E-01, 0.17660446E-01,-0.37850172E-02,-0.37257937E-02,
        -0.10155571E-01,-0.94290227E-02,-0.11632249E-02,-0.22606186E-02,
         0.22359288E-02,-0.51656710E-02,-0.11309087E-02,-0.70349436E-03,
        -0.45664198E-03, 0.26146913E-03,-0.11609192E-02,-0.62022889E-02,
        -0.30803876E-02,-0.32092971E-02, 0.68515609E-03,-0.47742380E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_350                                =v_x_e_q3en_350                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_350                                =v_x_e_q3en_350                                
        +coeff[ 17]    *x23        *x51
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_q3en_350                                ;
}
float t_e_q3en_350                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5120818E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.42977100E-02, 0.26284903E-02,-0.77924617E-01, 0.18242499E-01,
        -0.53818985E-02,-0.40101903E-02,-0.92695060E-03, 0.46196301E-03,
        -0.85136422E-03, 0.18479160E-02, 0.72937354E-03, 0.99412317E-03,
         0.35373378E-03,-0.16885756E-03, 0.64324506E-03, 0.32590010E-03,
         0.33739794E-03, 0.48239453E-03, 0.94598749E-04,-0.14625840E-03,
        -0.29129264E-03, 0.83945139E-03, 0.11715363E-03,-0.16626192E-03,
         0.53832965E-03,-0.35162101E-03, 0.52038147E-04,-0.51046564E-03,
        -0.45915914E-03, 0.22376664E-02, 0.17299058E-02, 0.58114976E-04,
        -0.40895906E-04, 0.61766936E-04, 0.19371836E-02, 0.16777299E-02,
        -0.18551078E-03, 0.41918553E-04, 0.31406482E-03,-0.10723456E-04,
         0.51335064E-04, 0.60838720E-04, 0.45533550E-04, 0.43136090E-04,
        -0.39486669E-04, 0.74687341E-04, 0.84534098E-04, 0.95999788E-03,
         0.14342331E-03,-0.10799890E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_350                                =v_t_e_q3en_350                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_350                                =v_t_e_q3en_350                                
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_350                                =v_t_e_q3en_350                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_350                                =v_t_e_q3en_350                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]*x12                
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_q3en_350                                =v_t_e_q3en_350                                
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_q3en_350                                ;
}
float y_e_q3en_350                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1242286E-05;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.49316790E-04, 0.11519447E+00,-0.87915912E-01,-0.67213126E-01,
        -0.25402430E-01, 0.47628589E-01, 0.28192732E-01,-0.33017963E-01,
        -0.14759890E-01,-0.71718968E-02, 0.23865714E-02,-0.10501487E-01,
        -0.68002818E-02,-0.24768789E-02,-0.28279612E-04, 0.94298442E-03,
        -0.15836337E-02, 0.17215587E-02,-0.30830707E-02,-0.19235454E-02,
         0.51668673E-02,-0.56353644E-02, 0.59537229E-03, 0.18734294E-02,
        -0.20892629E-02,-0.13196142E-02, 0.16406254E-02, 0.93488669E-03,
        -0.47745458E-02, 0.14218946E-02,-0.35412244E-02,-0.66316500E-02,
        -0.24463346E-02,-0.45285723E-03, 0.63817075E-03, 0.13096812E-02,
         0.37121112E-03,-0.36881867E-03, 0.87683881E-03,-0.22078396E-02,
        -0.35573407E-02,-0.16999036E-02, 0.12620502E-02,-0.42581109E-04,
         0.58945827E-03,-0.24769546E-02, 0.26583025E-03, 0.23027134E-03,
        -0.12430310E-02,-0.11019012E-02, 0.19461262E-04,-0.12649777E-04,
        -0.47327103E-04,-0.39704708E-04,-0.34498065E-04, 0.62376971E-03,
        -0.31738632E-03,-0.59666112E-03,-0.77896497E-04, 0.24788917E-03,
        -0.43778564E-03, 0.26467571E-03, 0.11606582E-03, 0.72202925E-03,
         0.30420424E-03, 0.22012524E-02, 0.38872880E-02, 0.26610014E-02,
        -0.10410877E-01,-0.11763601E-01,-0.39659902E-02,-0.17362392E-02,
         0.26983951E-03,-0.18497121E-04,-0.33854747E-04,-0.38699865E-04,
         0.52733958E-04, 0.40518264E-02,-0.64511114E-04, 0.49466211E-02,
        -0.10914388E-03, 0.30694266E-02,-0.45453812E-03, 0.92293421E-03,
        -0.74773590E-04,-0.59875200E-03, 0.55220770E-03,-0.39191186E-03,
        -0.21425891E-03, 0.36859242E-03, 0.71793838E-05, 0.11315160E-04,
         0.14068759E-04,-0.14200786E-03,-0.13375621E-03,-0.10671158E-03,
        -0.41059095E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]    *x24*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x21    *x43    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x22*x32*x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x23    *x41*x51
        +coeff[ 43]            *x45*x51
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x24    *x43    
        +coeff[ 46]    *x23    *x41*x52
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]    *x21            
        +coeff[ 51]                *x51
        +coeff[ 52]*x12        *x41    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 53]*x11        *x41*x51
        +coeff[ 54]*x11    *x31    *x51
        +coeff[ 55]            *x43*x51
        +coeff[ 56]        *x31*x44    
        +coeff[ 57]        *x32*x43    
        +coeff[ 58]*x11*x21    *x41*x51
        +coeff[ 59]    *x21    *x41*x52
        +coeff[ 60]        *x33*x42    
        +coeff[ 61]*x11*x21    *x43    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 62]            *x41*x53
        +coeff[ 63]*x11*x21*x31*x42    
        +coeff[ 64]*x11*x21*x32*x41    
        +coeff[ 65]    *x23    *x43    
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]    *x23*x32*x41    
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]    *x22*x32*x43    
        +coeff[ 70]    *x24*x31*x42    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 71]    *x22*x34*x41    
        +coeff[ 72]    *x23    *x41*x53
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x11*x21    *x42    
        +coeff[ 75]*x11*x22*x31        
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]    *x21*x31*x44    
        +coeff[ 78]*x11*x23*x31        
        +coeff[ 79]    *x21*x32*x43    
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 80]*x11*x22    *x41*x51
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x21*x34*x41    
        +coeff[ 84]*x11*x21    *x41*x52
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]    *x24    *x41*x51
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_q3en_350                                =v_y_e_q3en_350                                
        +coeff[ 89]*x11*x23    *x43    
        +coeff[ 90]    *x22            
        +coeff[ 91]*x12        *x42    
        +coeff[ 92]*x11*x21*x32        
        +coeff[ 93]        *x34*x41    
        +coeff[ 94]*x11*x23    *x41    
        +coeff[ 95]    *x21*x32*x41*x51
        +coeff[ 96]    *x23*x31    *x51
        ;

    return v_y_e_q3en_350                                ;
}
float p_e_q3en_350                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.9082457E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.85582753E-04,-0.23595111E-01, 0.10529675E-01,-0.63074748E-02,
        -0.12545416E-01, 0.47414512E-02, 0.99761104E-02,-0.83085755E-02,
        -0.50546805E-03,-0.16905325E-02,-0.28302530E-02, 0.41828299E-03,
        -0.19947127E-02,-0.11492663E-02, 0.13542065E-02,-0.29272426E-05,
        -0.93550631E-03, 0.70748800E-04, 0.27122520E-03,-0.33430735E-03,
        -0.14168913E-02,-0.20008124E-03, 0.49935986E-03,-0.94787829E-03,
        -0.77906728E-03, 0.12881443E-03,-0.32917139E-03, 0.71611791E-03,
         0.47033004E-03,-0.94669791E-04, 0.40649268E-03, 0.17147380E-03,
         0.17409392E-03,-0.11762908E-03, 0.16803608E-04,-0.22247953E-03,
        -0.16060231E-02,-0.82058564E-03,-0.31256501E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_350                                =v_p_e_q3en_350                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_350                                =v_p_e_q3en_350                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_350                                =v_p_e_q3en_350                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_q3en_350                                =v_p_e_q3en_350                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_350                                ;
}
float l_e_q3en_350                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1682582E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.16041586E-01,-0.31777263E+00,-0.25517685E-01, 0.12232319E-01,
        -0.29890018E-01,-0.16611755E-01,-0.60968269E-02,-0.56612771E-02,
         0.20125539E-02, 0.72896569E-02, 0.42234012E-02, 0.46485299E-03,
         0.19627649E-03, 0.39368286E-02,-0.33127521E-02,-0.80864609E-03,
         0.20313489E-02,-0.21852299E-02, 0.15852315E-02, 0.71764534E-03,
        -0.25035877E-04,-0.18612876E-03,-0.11793018E-02, 0.73873834E-03,
         0.49326284E-03, 0.20394837E-02, 0.17095832E-02, 0.75496892E-02,
         0.76161893E-02, 0.11085958E-01, 0.10422581E-01, 0.37168362E-02,
         0.11827438E-02,-0.65462891E-03,-0.75743051E-03, 0.53590271E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_350                                =v_l_e_q3en_350                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_350                                =v_l_e_q3en_350                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x31        
        +coeff[ 21]                *x52
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_q3en_350                                =v_l_e_q3en_350                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]    *x22    *x42*x51
        +coeff[ 33]*x12    *x31    *x52
        +coeff[ 34]    *x22*x31    *x53
    ;
    v_l_e_q3en_350                                =v_l_e_q3en_350                                
        +coeff[ 35]    *x23*x33*x41    
        ;

    return v_l_e_q3en_350                                ;
}
float x_e_q3en_300                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.4346930E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.26374240E-02, 0.11106391E+00, 0.22655165E+00, 0.28571524E-01,
         0.17936809E-01,-0.23969555E-04,-0.10578926E-01,-0.37701586E-02,
        -0.36808632E-02,-0.10195998E-01,-0.94358884E-02,-0.11689494E-02,
        -0.22315022E-02, 0.22503720E-02,-0.50926176E-02,-0.11043203E-02,
        -0.69523876E-03,-0.44978183E-03,-0.11059633E-02,-0.61315140E-02,
        -0.30995924E-02,-0.32722403E-02,-0.26484523E-02,-0.14869827E-02,
        -0.49631880E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_x_e_q3en_300                                =v_x_e_q3en_300                                
        +coeff[  8]            *x42    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]            *x42*x51
    ;
    v_x_e_q3en_300                                =v_x_e_q3en_300                                
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x21*x32*x42*x52
        +coeff[ 23]    *x21*x31*x43*x52
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_q3en_300                                ;
}
float t_e_q3en_300                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4952875E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.42518019E-02,-0.78423619E-01, 0.18229568E-01,-0.54677590E-02,
        -0.40308782E-02, 0.22501999E-05, 0.26157848E-02,-0.92247984E-03,
         0.46441669E-03,-0.85157534E-03, 0.19512173E-02, 0.75379660E-03,
         0.99196937E-03, 0.35213903E-03,-0.16615086E-03, 0.63580659E-03,
         0.34217170E-03, 0.28173899E-03, 0.10194489E-03, 0.83297613E-03,
         0.17188864E-02, 0.11782722E-03,-0.22469833E-03,-0.13421601E-03,
         0.71455652E-04, 0.46424565E-03, 0.49562822E-03,-0.32120739E-03,
         0.84144478E-04,-0.54256298E-03,-0.43139432E-03, 0.22052960E-02,
        -0.19417696E-03,-0.39198781E-04, 0.19436037E-02, 0.16671113E-02,
        -0.33823060E-03, 0.29417512E-04,-0.19096289E-03,-0.10470670E-04,
         0.49167262E-04, 0.51855230E-04, 0.86965869E-04, 0.48433390E-04,
         0.38615519E-04,-0.37912090E-04, 0.67048444E-04, 0.58893835E-04,
         0.91814855E-03, 0.19055912E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_300                                =v_t_e_q3en_300                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_300                                =v_t_e_q3en_300                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]                *x53
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_q3en_300                                =v_t_e_q3en_300                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23            
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_300                                =v_t_e_q3en_300                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x12                
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]*x11        *x42    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_q3en_300                                =v_t_e_q3en_300                                
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]    *x22        *x52
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_q3en_300                                ;
}
float y_e_q3en_300                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4766693E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.50153065E-03, 0.11441885E+00,-0.88741407E-01,-0.67440875E-01,
        -0.25380099E-01, 0.47682978E-01, 0.28182182E-01,-0.33391025E-01,
        -0.14875389E-01,-0.71489071E-02, 0.23595756E-02,-0.10491167E-01,
        -0.24954826E-02,-0.27535645E-04,-0.55601611E-03, 0.93570049E-03,
        -0.68091303E-02,-0.15707761E-02, 0.16575065E-02,-0.30575283E-02,
        -0.19186513E-02, 0.52365549E-02,-0.56148460E-02, 0.57088619E-03,
         0.18543050E-02,-0.21199868E-02,-0.13365380E-02, 0.14688232E-02,
        -0.49700220E-02, 0.16535920E-02, 0.28474801E-02,-0.37853937E-02,
        -0.12440867E-01,-0.24050530E-02,-0.44431037E-03, 0.77877654E-03,
         0.13116323E-02, 0.36172281E-03,-0.36141818E-03, 0.86984033E-03,
        -0.20893125E-02,-0.34831280E-02,-0.17293540E-02, 0.13575583E-02,
         0.24958178E-02, 0.43174000E-02,-0.56360932E-05, 0.36414588E-03,
        -0.26315530E-02, 0.34464468E-03, 0.58071601E-03,-0.24719411E-03,
         0.23785204E-03, 0.37798600E-03,-0.12995250E-02,-0.11256792E-02,
         0.51419140E-03,-0.10700334E-01,-0.66668168E-02,-0.41401261E-04,
        -0.44939618E-03,-0.11241055E-03, 0.11055366E-03, 0.64708805E-03,
         0.42375447E-02, 0.53744838E-02, 0.30991507E-02, 0.59996062E-03,
        -0.40870173E-05,-0.51826466E-03, 0.76791657E-04,-0.40502455E-02,
        -0.18867342E-02,-0.73925265E-04,-0.57162880E-03,-0.24847812E-04,
         0.47535677E-05,-0.32707288E-04,-0.27710670E-04,-0.33156110E-04,
        -0.38683920E-04, 0.57666635E-04, 0.36544728E-03,-0.10134689E-03,
         0.10006959E-02,-0.61167148E-03,-0.20188923E-03, 0.58157311E-04,
         0.20209913E-04,-0.47723672E-04, 0.32564180E-03,-0.22563877E-03,
         0.19273750E-04,-0.19725352E-04, 0.35393434E-04, 0.96292002E-04,
        -0.53582644E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 44]    *x23    *x43    
        +coeff[ 45]    *x23*x31*x42    
        +coeff[ 46]    *x21    *x43*x52
        +coeff[ 47]    *x21    *x45*x51
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]    *x22*x33        
        +coeff[ 56]    *x22    *x41*x52
        +coeff[ 57]    *x22*x31*x44    
        +coeff[ 58]    *x22*x33*x42    
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]        *x33*x42    
        +coeff[ 61]        *x34*x41    
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 62]            *x41*x53
        +coeff[ 63]*x11*x21*x31*x42    
        +coeff[ 64]    *x21*x31*x44    
        +coeff[ 65]    *x21*x32*x43    
        +coeff[ 66]    *x21*x33*x42    
        +coeff[ 67]    *x23*x33        
        +coeff[ 68]*x11*x21    *x43*x51
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]*x11*x21    *x45    
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 71]    *x24*x31*x42    
        +coeff[ 72]    *x22*x34*x41    
        +coeff[ 73]*x11*x21*x34*x41    
        +coeff[ 74]    *x22    *x45*x51
        +coeff[ 75]    *x23*x34*x41    
        +coeff[ 76]                *x51
        +coeff[ 77]*x12        *x41    
        +coeff[ 78]*x12    *x31        
        +coeff[ 79]*x11    *x31    *x51
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 80]*x11*x22*x31        
        +coeff[ 81]        *x31    *x53
        +coeff[ 82]*x11*x21*x32*x41    
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]    *x21*x34*x41    
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x24*x31    *x51
        +coeff[ 87]*x11*x23*x31    *x51
        +coeff[ 88]*x11    *x31*x41    
    ;
    v_y_e_q3en_300                                =v_y_e_q3en_300                                
        +coeff[ 89]*x11*x21    *x41*x51
        +coeff[ 90]*x11*x21    *x43    
        +coeff[ 91]    *x21    *x43*x51
        +coeff[ 92]*x13        *x41    
        +coeff[ 93]*x11        *x41*x52
        +coeff[ 94]*x11    *x32*x42    
        +coeff[ 95]    *x21*x31*x42*x51
        +coeff[ 96]            *x43*x52
        ;

    return v_y_e_q3en_300                                ;
}
float p_e_q3en_300                                (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.5514598E-04;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.60874430E-04,-0.23738783E-01, 0.10388262E-01,-0.63408818E-02,
        -0.12637385E-01, 0.47362554E-02, 0.99754333E-02,-0.83683552E-02,
        -0.48029044E-03,-0.16821586E-02,-0.28743520E-02, 0.41358493E-03,
        -0.20036499E-02,-0.11578250E-02, 0.13806819E-02,-0.12672049E-05,
        -0.93681127E-03, 0.99016841E-04, 0.26892286E-03,-0.32251817E-03,
        -0.14362928E-02,-0.18168837E-03, 0.50070736E-03,-0.97032567E-03,
        -0.78490953E-03, 0.13154466E-03,-0.28169132E-03, 0.75454067E-03,
         0.52463019E-03,-0.97697070E-04, 0.41633516E-03, 0.18604797E-03,
         0.15409212E-03,-0.11688117E-03,-0.20503758E-03, 0.12470922E-03,
        -0.16078287E-02,-0.82643289E-03,-0.33729439E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_300                                =v_p_e_q3en_300                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_300                                =v_p_e_q3en_300                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_300                                =v_p_e_q3en_300                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_300                                =v_p_e_q3en_300                                
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x23*x31    *x51
        ;

    return v_p_e_q3en_300                                ;
}
float l_e_q3en_300                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1658085E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.16230173E-01,-0.32083413E+00,-0.25328245E-01, 0.12112445E-01,
        -0.30037109E-01,-0.16458450E-01,-0.56650708E-02,-0.52533974E-02,
         0.20710882E-02, 0.91879321E-02, 0.79997685E-02,-0.37614684E-05,
         0.13602454E-03, 0.22063709E-02,-0.31285600E-02,-0.83500793E-03,
         0.39477791E-02,-0.20323426E-02, 0.17095247E-02, 0.88665244E-03,
         0.83790021E-02, 0.49727783E-02, 0.21271612E-02,-0.18675214E-03,
         0.57967624E-03, 0.11785970E-02, 0.51215658E-03,-0.16124992E-02,
         0.74238732E-03, 0.78272278E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_300                                =v_l_e_q3en_300                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_300                                =v_l_e_q3en_300                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]                *x52
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3en_300                                =v_l_e_q3en_300                                
        +coeff[ 26]        *x32    *x53
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]    *x22*x31    *x53
        +coeff[ 29]*x11    *x34    *x52
        ;

    return v_l_e_q3en_300                                ;
}
float x_e_q3en_250                                (float *x,int m){
    int ncoeff= 21;
    float avdat=  0.3534126E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
        -0.18372859E-02,-0.10536089E-01, 0.11107174E+00, 0.22839664E+00,
         0.28493544E-01, 0.18159432E-01,-0.37786649E-02,-0.36260369E-02,
        -0.11560371E-01,-0.93793031E-02,-0.11675513E-02,-0.22266053E-02,
         0.23174905E-02,-0.52844482E-02,-0.11220005E-02,-0.68029849E-03,
        -0.46000260E-03,-0.10344100E-02,-0.43649809E-02,-0.26740835E-02,
         0.87876746E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_250                                =v_x_e_q3en_250                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_250                                =v_x_e_q3en_250                                
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x23*x32    *x51
        ;

    return v_x_e_q3en_250                                ;
}
float t_e_q3en_250                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4878622E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.41804193E-02, 0.25988028E-02,-0.78968599E-01, 0.18247638E-01,
        -0.55588270E-02,-0.40092198E-02,-0.92822372E-03, 0.48841874E-03,
        -0.83635736E-03, 0.18937358E-02, 0.77600498E-03, 0.99506404E-03,
         0.34510950E-03,-0.16718893E-03, 0.64012612E-03, 0.32054455E-03,
         0.32099782E-03, 0.44861299E-03, 0.11075623E-03, 0.80356357E-03,
         0.16760173E-02, 0.11412412E-03,-0.28208655E-03,-0.16775135E-03,
         0.52355247E-03,-0.34178019E-03, 0.87888984E-04,-0.60471514E-03,
        -0.40548499E-03, 0.22195671E-02,-0.16930209E-03, 0.60084290E-04,
        -0.46503421E-04, 0.63824846E-04, 0.18246537E-02, 0.17520570E-02,
        -0.27058681E-03, 0.33056724E-04, 0.33013389E-03,-0.21783622E-03,
        -0.10190684E-04, 0.47517638E-04, 0.73957410E-04, 0.60100061E-04,
         0.50349590E-04, 0.97935228E-03, 0.16435658E-03,-0.10288935E-03,
         0.26325710E-03, 0.28781797E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_250                                =v_t_e_q3en_250                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_250                                =v_t_e_q3en_250                                
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_250                                =v_t_e_q3en_250                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23            
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_250                                =v_t_e_q3en_250                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_q3en_250                                =v_t_e_q3en_250                                
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x22*x32    *x51
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]    *x22*x32*x42    
        +coeff[ 49]    *x21*x31*x43*x51
        ;

    return v_t_e_q3en_250                                ;
}
float y_e_q3en_250                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.9084927E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.99868188E-03, 0.11296129E+00,-0.89899443E-01,-0.67612424E-01,
        -0.25344791E-01, 0.47632895E-01, 0.28161032E-01,-0.33747666E-01,
        -0.14975711E-01,-0.71666525E-02, 0.23346252E-02,-0.10385782E-01,
        -0.24741204E-02,-0.47290278E-05,-0.64644229E-03, 0.91688620E-03,
        -0.66858628E-02,-0.15715209E-02, 0.16177845E-02,-0.30696953E-02,
        -0.19245453E-02, 0.53185779E-02,-0.56049870E-02, 0.56795654E-03,
         0.18803747E-02,-0.23010541E-02,-0.13454010E-02, 0.12917370E-02,
        -0.50577302E-02, 0.15575262E-02, 0.28077192E-02,-0.35368800E-02,
        -0.11395468E-01,-0.27021782E-02,-0.52272866E-03, 0.70159259E-03,
         0.12641111E-02, 0.33938026E-03,-0.33346401E-03, 0.88193128E-03,
        -0.21155218E-02,-0.35723343E-02,-0.17022744E-02, 0.13128001E-02,
         0.17134664E-04, 0.24221647E-02, 0.38578486E-06,-0.28787446E-02,
         0.35101682E-04,-0.24845200E-04, 0.40245595E-03,-0.34656207E-03,
         0.24346993E-03,-0.11506085E-02,-0.10911830E-02, 0.56416111E-03,
         0.41882987E-02,-0.99490564E-02,-0.62811757E-02,-0.49938153E-04,
        -0.41086485E-04, 0.54735708E-03, 0.36916739E-03,-0.47275284E-03,
         0.47276742E-03,-0.20525089E-03, 0.12107159E-03, 0.69752976E-03,
         0.32973022E-03, 0.45183902E-02, 0.57054963E-02, 0.34228936E-02,
         0.61371358E-03,-0.17076965E-03,-0.43328214E-02,-0.17082794E-02,
         0.14772829E-03, 0.20048006E-04,-0.26961923E-04,-0.36772497E-04,
        -0.35621222E-04,-0.58319223E-04, 0.57369904E-04,-0.99048280E-04,
         0.50752278E-04, 0.81052392E-04, 0.98799937E-03,-0.48366652E-03,
        -0.19354184E-03,-0.70407084E-03, 0.15370846E-03,-0.11929393E-05,
         0.86813734E-05, 0.12556094E-04,-0.10022449E-04, 0.24710351E-04,
         0.36963993E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 44]            *x45*x51
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]    *x21    *x43*x52
        +coeff[ 47]    *x24    *x43    
        +coeff[ 48]    *x21            
        +coeff[ 49]                *x51
        +coeff[ 50]    *x21    *x43    
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 53]    *x24    *x41    
        +coeff[ 54]    *x22*x33        
        +coeff[ 55]    *x22    *x41*x52
        +coeff[ 56]    *x23*x31*x42    
        +coeff[ 57]    *x22*x31*x44    
        +coeff[ 58]    *x22*x33*x42    
        +coeff[ 59]*x12        *x41    
        +coeff[ 60]*x11        *x41*x51
        +coeff[ 61]            *x43*x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 62]    *x21    *x41*x52
        +coeff[ 63]        *x33*x42    
        +coeff[ 64]*x11*x21    *x43    
        +coeff[ 65]        *x34*x41    
        +coeff[ 66]            *x41*x53
        +coeff[ 67]*x11*x21*x31*x42    
        +coeff[ 68]*x11*x21*x32*x41    
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x21*x32*x43    
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 71]    *x21*x33*x42    
        +coeff[ 72]    *x23*x33        
        +coeff[ 73]    *x24    *x41*x51
        +coeff[ 74]    *x24*x31*x42    
        +coeff[ 75]    *x22*x34*x41    
        +coeff[ 76]    *x23*x34*x41    
        +coeff[ 77]    *x22            
        +coeff[ 78]*x12    *x31        
        +coeff[ 79]*x11    *x31    *x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 80]*x11*x22*x31        
        +coeff[ 81]*x11*x21    *x41*x51
        +coeff[ 82]        *x31    *x53
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]    *x22*x31    *x52
        +coeff[ 85]    *x22    *x43*x51
        +coeff[ 86]    *x21*x34*x41    
        +coeff[ 87]    *x22*x31*x42*x51
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_q3en_250                                =v_y_e_q3en_250                                
        +coeff[ 89]    *x24    *x43*x51
        +coeff[ 90]    *x23    *x41*x53
        +coeff[ 91]*x11                
        +coeff[ 92]            *x42    
        +coeff[ 93]        *x31*x41    
        +coeff[ 94]*x11        *x42    
        +coeff[ 95]        *x32*x42    
        +coeff[ 96]*x11    *x31*x42    
        ;

    return v_y_e_q3en_250                                ;
}
float p_e_q3en_250                                (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2487056E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.25783954E-03,-0.23936186E-01, 0.10134172E-01,-0.63530481E-02,
        -0.12629216E-01, 0.47179270E-02, 0.99585047E-02,-0.84407162E-02,
        -0.49914076E-03,-0.16688542E-02,-0.28729907E-02, 0.40976724E-03,
        -0.19921395E-02,-0.11423583E-02, 0.13791937E-02,-0.64771921E-05,
        -0.91293274E-03, 0.69347952E-04, 0.26956011E-03,-0.32751390E-03,
        -0.14074799E-02,-0.16674689E-03, 0.49991824E-03,-0.96527656E-03,
        -0.79914281E-03, 0.13453228E-03,-0.27273491E-03, 0.73235086E-03,
         0.52979315E-03,-0.94895942E-04, 0.41213620E-03, 0.17952395E-03,
         0.16569694E-03,-0.11275407E-03,-0.22691752E-03,-0.15729693E-02,
        -0.82169176E-03,-0.35198493E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_250                                =v_p_e_q3en_250                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_250                                =v_p_e_q3en_250                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_250                                =v_p_e_q3en_250                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_250                                =v_p_e_q3en_250                                
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x23*x31    *x51
        ;

    return v_p_e_q3en_250                                ;
}
float l_e_q3en_250                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1589305E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.15501115E-01,-0.32317486E+00,-0.25480606E-01, 0.12114642E-01,
        -0.30598197E-01,-0.16348785E-01,-0.57094884E-02,-0.50881994E-02,
         0.19172757E-02, 0.10330634E-01, 0.81742592E-02, 0.19775565E-03,
         0.19178909E-03, 0.29209414E-02,-0.31686795E-02,-0.76904026E-03,
         0.34738635E-02,-0.21177346E-02, 0.14184795E-02, 0.13340778E-02,
         0.65418880E-03, 0.10950419E-02, 0.84990461E-03,-0.31245861E-03,
        -0.28143483E-03,-0.27597460E-03, 0.54324028E-03, 0.71383384E-02,
         0.46052309E-02,-0.13024332E-02, 0.10378931E-02, 0.10321770E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_250                                =v_l_e_q3en_250                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_250                                =v_l_e_q3en_250                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x11        *x43    
    ;
    v_l_e_q3en_250                                =v_l_e_q3en_250                                
        +coeff[ 26]*x11*x21        *x52
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]    *x24    *x42    
        +coeff[ 31]        *x32*x41*x54
        ;

    return v_l_e_q3en_250                                ;
}
float x_e_q3en_200                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.5402653E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.26339681E-02,-0.10459556E-01, 0.11120749E+00, 0.23028541E+00,
         0.28431645E-01, 0.18292649E-01,-0.37771156E-02,-0.36480743E-02,
        -0.10120108E-01,-0.94788074E-02,-0.11773459E-02,-0.22103675E-02,
         0.22699174E-02,-0.50720247E-02,-0.11480764E-02,-0.68313797E-03,
        -0.46165075E-03,-0.12923223E-02,-0.61127967E-02,-0.28517111E-02,
        -0.28862765E-02, 0.82865061E-03,-0.29212814E-02,-0.18272441E-02,
        -0.40250835E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_200                                =v_x_e_q3en_200                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_200                                =v_x_e_q3en_200                                
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]    *x21*x32*x42*x52
        +coeff[ 23]    *x21*x31*x43*x52
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_q3en_200                                ;
}
float t_e_q3en_200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5202047E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.41433536E-02, 0.25791340E-02,-0.79416223E-01, 0.18228414E-01,
        -0.56503573E-02,-0.39741830E-02,-0.93099679E-03, 0.49063854E-03,
        -0.85406902E-03, 0.18885148E-02, 0.74194151E-03, 0.96633908E-03,
         0.30885419E-03,-0.16560634E-03, 0.63495134E-03, 0.32318314E-03,
         0.29511447E-03, 0.52579440E-03, 0.10221532E-03, 0.84289198E-03,
         0.17018957E-02, 0.11878803E-03,-0.26265913E-03,-0.17437198E-03,
         0.55167195E-03,-0.30989919E-03, 0.84527994E-04,-0.51693601E-03,
        -0.40547340E-03, 0.21944963E-02,-0.19211163E-03,-0.46563509E-04,
         0.62439656E-04, 0.19016879E-02, 0.17473924E-02, 0.16866856E-03,
        -0.23911684E-03, 0.26075070E-03,-0.19009660E-03,-0.13465484E-04,
         0.48377791E-04, 0.51385869E-04, 0.60084032E-04, 0.66807632E-04,
         0.15151096E-03, 0.58899375E-04, 0.53492677E-04, 0.97616203E-03,
        -0.78919766E-04, 0.10912847E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_200                                =v_t_e_q3en_200                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_200                                =v_t_e_q3en_200                                
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3en_200                                =v_t_e_q3en_200                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23            
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]                *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_q3en_200                                =v_t_e_q3en_200                                
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x12                
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]*x11        *x42    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_q3en_200                                =v_t_e_q3en_200                                
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x23        *x52
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_q3en_200                                ;
}
float y_e_q3en_200                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6392651E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.57690416E-03, 0.11069963E+00,-0.91791220E-01,-0.68339862E-01,
        -0.25474276E-01, 0.47643404E-01, 0.28134672E-01,-0.33240408E-01,
        -0.14788089E-01,-0.72103525E-02, 0.23048946E-02,-0.10240528E-01,
        -0.24417639E-02,-0.30674406E-02,-0.25337969E-03,-0.79360220E-03,
         0.90880482E-03,-0.65643075E-02,-0.15441873E-02, 0.16510078E-02,
        -0.19091530E-02, 0.58428207E-02,-0.53161010E-02, 0.56532887E-03,
         0.21381972E-02,-0.23952157E-02,-0.14866088E-02, 0.18597066E-02,
         0.34719624E-02, 0.23279816E-02,-0.46527036E-02,-0.57480568E-02,
        -0.41295355E-02,-0.35963531E-03,-0.33243366E-04, 0.12348001E-02,
         0.62976260E-03,-0.36423380E-03, 0.86344586E-03,-0.18526550E-02,
        -0.18229022E-02, 0.13737862E-02,-0.49245449E-04, 0.41648953E-04,
        -0.46701092E-03, 0.23458571E-03, 0.35832141E-03,-0.11798076E-02,
         0.57944842E-03, 0.35099252E-04,-0.46253273E-04,-0.40060200E-04,
         0.57480228E-03,-0.87868393E-04,-0.44110132E-03, 0.44217517E-03,
        -0.19523010E-03, 0.13382084E-03, 0.67493255E-03, 0.28823450E-03,
        -0.84049119E-04,-0.93170991E-02,-0.10758100E-01, 0.19744528E-02,
         0.60060425E-02, 0.85644228E-02, 0.59582936E-02,-0.10580706E-02,
         0.16555176E-02,-0.92324911E-03,-0.22499175E-02, 0.21717402E-04,
        -0.21438233E-04,-0.31104431E-04, 0.47126458E-04,-0.45195233E-04,
         0.57415113E-04, 0.11689382E-02,-0.12111742E-03, 0.26254807E-03,
        -0.67808556E-04, 0.13382842E-03,-0.13666747E-02,-0.35778529E-02,
        -0.62582698E-02,-0.20461723E-02,-0.16309385E-02, 0.75095892E-03,
         0.11282699E-04, 0.11667862E-04, 0.56614899E-05,-0.23793757E-04,
        -0.18528710E-04, 0.28359736E-03,-0.73133895E-04, 0.38979786E-04,
        -0.63906911E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]                *x51
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 53]*x11*x21    *x41*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]            *x41*x53
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x23    *x45    
        +coeff[ 64]    *x23*x31*x44    
        +coeff[ 65]    *x23*x32*x43    
        +coeff[ 66]    *x23*x33*x42    
        +coeff[ 67]    *x24    *x43*x51
        +coeff[ 68]    *x23*x34*x41    
        +coeff[ 69]    *x24*x31*x42*x51
        +coeff[ 70]    *x24    *x45    
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 71]        *x31*x41    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x11    *x32*x41    
        +coeff[ 75]*x11*x22*x31        
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]    *x21*x31*x44    
        +coeff[ 78]*x11*x22    *x41*x51
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 80]*x11*x21    *x41*x52
        +coeff[ 81]    *x21    *x41*x53
        +coeff[ 82]    *x22    *x45    
        +coeff[ 83]    *x24*x31*x42    
        +coeff[ 84]    *x22*x33*x42    
        +coeff[ 85]    *x24*x32*x41    
        +coeff[ 86]    *x22*x34*x41    
        +coeff[ 87]    *x21*x32*x45    
        +coeff[ 88]            *x42    
    ;
    v_y_e_q3en_200                                =v_y_e_q3en_200                                
        +coeff[ 89]        *x32        
        +coeff[ 90]    *x21        *x51
        +coeff[ 91]    *x22*x32        
        +coeff[ 92]*x12        *x42    
        +coeff[ 93]    *x21    *x45    
        +coeff[ 94]    *x21*x32*x41*x51
        +coeff[ 95]        *x32*x44    
        +coeff[ 96]    *x23*x31    *x51
        ;

    return v_y_e_q3en_200                                ;
}
float p_e_q3en_200                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1477899E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.14668799E-03,-0.24272844E-01, 0.97312219E-02,-0.63898182E-02,
        -0.12758323E-01, 0.47219321E-02, 0.99381888E-02,-0.84975706E-02,
        -0.54503803E-03,-0.16764157E-02,-0.28063215E-02, 0.40603010E-03,
        -0.19880212E-02,-0.11267372E-02, 0.14124779E-02,-0.51692400E-05,
        -0.92984311E-03, 0.43245018E-04, 0.26508939E-03,-0.32448265E-03,
        -0.13699790E-02,-0.17509532E-03, 0.49996347E-03,-0.99976733E-03,
        -0.83364360E-03, 0.12791737E-03,-0.31446115E-03, 0.72785025E-03,
         0.45934581E-03,-0.96353957E-04, 0.42657065E-03, 0.18783794E-03,
        -0.11913006E-03, 0.25196608E-04,-0.23995756E-03,-0.16687425E-02,
        -0.86714100E-03,-0.34195257E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_200                                =v_p_e_q3en_200                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_200                                =v_p_e_q3en_200                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_200                                =v_p_e_q3en_200                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x22    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x24    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_200                                =v_p_e_q3en_200                                
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x23*x31    *x51
        ;

    return v_p_e_q3en_200                                ;
}
float l_e_q3en_200                                (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.1792906E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.16006147E-01,-0.32554239E+00,-0.25522534E-01, 0.11940372E-01,
        -0.30720016E-01,-0.16116623E-01,-0.55477037E-02,-0.50698952E-02,
         0.20594471E-02, 0.11279244E-01, 0.91687255E-02, 0.24072010E-03,
         0.30559881E-03, 0.12399356E-02,-0.32320109E-02,-0.81731658E-03,
         0.43027201E-02,-0.20753297E-02, 0.92289504E-03, 0.62358461E-03,
         0.74519240E-03, 0.11924804E-02, 0.51714154E-03, 0.46062781E-02,
         0.31144177E-02,-0.53396501E-03, 0.15661822E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_200                                =v_l_e_q3en_200                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_200                                =v_l_e_q3en_200                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]*x11*x22            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x21*x33    *x51
    ;
    v_l_e_q3en_200                                =v_l_e_q3en_200                                
        +coeff[ 26]*x11*x21*x34*x41    
        ;

    return v_l_e_q3en_200                                ;
}
float x_e_q3en_175                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5652945E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.19903569E-02,-0.10403499E-01, 0.11127220E+00, 0.23139474E+00,
         0.28321706E-01, 0.18297024E-01,-0.38019198E-02,-0.37627530E-02,
        -0.11590431E-01,-0.94485236E-02,-0.11587073E-02,-0.22977216E-02,
         0.23303914E-02,-0.52410942E-02,-0.11508896E-02,-0.64559228E-03,
         0.90044428E-04,-0.45204343E-03,-0.91250025E-03, 0.69255201E-03,
        -0.38240533E-02,-0.23445641E-02, 0.92818192E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_175                                =v_x_e_q3en_175                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3en_175                                =v_x_e_q3en_175                                
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23*x32    *x51
        ;

    return v_x_e_q3en_175                                ;
}
float t_e_q3en_175                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5715603E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.43352949E-02, 0.25616956E-02,-0.79697698E-01, 0.18204369E-01,
        -0.56859367E-02,-0.39520715E-02,-0.92996279E-03, 0.48745435E-03,
        -0.85438881E-03, 0.18739089E-02, 0.83371578E-03, 0.96329913E-03,
         0.32662568E-03,-0.16737069E-03, 0.63520856E-03, 0.31360119E-03,
         0.33628807E-03, 0.49733877E-03, 0.12826343E-03, 0.77639637E-03,
         0.12568040E-03, 0.12266474E-03,-0.18015398E-03,-0.25426259E-03,
        -0.16267730E-03, 0.63523295E-03,-0.29751690E-03,-0.49983105E-03,
        -0.27639733E-03, 0.21546904E-02, 0.15595057E-02,-0.47687739E-04,
         0.68555564E-04, 0.18051616E-02, 0.17098733E-02,-0.30016818E-03,
         0.26163430E-03,-0.10544758E-04, 0.50392235E-04, 0.57105950E-04,
         0.73214847E-04, 0.61338986E-04, 0.72740506E-04, 0.52917490E-04,
         0.97369927E-03, 0.12157662E-03,-0.11001644E-03,-0.18125429E-03,
         0.40383200E-03,-0.23855567E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_t_e_q3en_175                                =v_t_e_q3en_175                                
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]            *x42    
        +coeff[ 13]*x11            *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q3en_175                                =v_t_e_q3en_175                                
        +coeff[ 17]    *x22*x32        
        +coeff[ 18]        *x32        
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x22        *x51
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_175                                =v_t_e_q3en_175                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]                *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_q3en_175                                =v_t_e_q3en_175                                
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]*x12                
        +coeff[ 38]*x11    *x31*x41    
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]        *x33*x41    
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_175                                =v_t_e_q3en_175                                
        +coeff[ 44]    *x21*x33*x41    
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]    *x23        *x52
        +coeff[ 47]    *x21*x31*x41*x52
        +coeff[ 48]    *x22*x32*x42    
        +coeff[ 49]    *x23    *x42*x51
        ;

    return v_t_e_q3en_175                                ;
}
float y_e_q3en_175                                (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3732451E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.40455058E-03, 0.10880549E+00,-0.93159162E-01,-0.68164997E-01,
        -0.25272954E-01, 0.47510311E-01, 0.28078414E-01,-0.32813977E-01,
        -0.14609023E-01,-0.71091033E-02, 0.22940410E-02,-0.99017462E-02,
        -0.24140426E-02,-0.40123274E-03,-0.11476579E-02, 0.89511852E-03,
        -0.62559643E-02,-0.15001987E-02, 0.16025992E-02,-0.30405475E-02,
        -0.18995620E-02, 0.53087762E-02,-0.51071425E-02, 0.55932609E-03,
         0.17676480E-02,-0.19036524E-02,-0.13433350E-02, 0.36367489E-03,
         0.13883749E-02, 0.78034768E-03,-0.61746426E-02,-0.78298049E-02,
        -0.60962657E-02,-0.14907318E-03, 0.12934798E-02, 0.33821620E-03,
        -0.35317324E-03, 0.85514423E-03,-0.20665419E-02,-0.18836254E-02,
         0.13551144E-02,-0.80984013E-04, 0.27109325E-04,-0.22033055E-04,
        -0.79535774E-03, 0.24410449E-03, 0.32103178E-03,-0.13985694E-02,
         0.54192333E-03,-0.49316768E-04, 0.66354964E-03,-0.92369126E-03,
         0.44603617E-03,-0.44681548E-03, 0.10438161E-03, 0.65029756E-03,
         0.30358194E-03, 0.22784362E-02, 0.38088188E-02, 0.25640666E-02,
        -0.15878389E-03,-0.56602934E-03,-0.35873913E-04, 0.16323309E-04,
        -0.35475008E-04,-0.24412609E-04,-0.40849034E-04, 0.15170148E-02,
         0.58373476E-04, 0.38820892E-02,-0.93341419E-04, 0.49818689E-02,
        -0.10826161E-03, 0.29532257E-02,-0.49075257E-03, 0.85176458E-03,
        -0.45164349E-03, 0.52689837E-03,-0.64631146E-02, 0.40093399E-03,
        -0.21423335E-03, 0.97408879E-03,-0.61156815E-02,-0.26374750E-02,
        -0.27544361E-02,-0.12380847E-02, 0.19201978E-03,-0.20410940E-03,
         0.77928336E-04,-0.29339297E-02, 0.10301762E-04, 0.15331107E-04,
        -0.31721875E-05,-0.73116062E-05, 0.23600578E-04, 0.25831234E-04,
        -0.15489124E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]*x11*x21*x32*x41    
        +coeff[ 57]    *x23    *x43    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]*x11*x21    *x43*x51
        +coeff[ 61]    *x24    *x41*x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 62]*x13    *x31    *x51
        +coeff[ 63]    *x22            
        +coeff[ 64]*x12        *x41    
        +coeff[ 65]*x12    *x31        
        +coeff[ 66]*x11*x22*x31        
        +coeff[ 67]    *x21    *x45    
        +coeff[ 68]        *x31    *x53
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x23*x31    *x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 71]    *x21*x32*x43    
        +coeff[ 72]*x11*x22    *x41*x51
        +coeff[ 73]    *x21*x33*x42    
        +coeff[ 74]    *x22    *x43*x51
        +coeff[ 75]    *x21*x34*x41    
        +coeff[ 76]    *x22*x31*x42*x51
        +coeff[ 77]    *x23*x33        
        +coeff[ 78]    *x22*x31*x44    
        +coeff[ 79]        *x33*x44    
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 80]    *x24*x31    *x51
        +coeff[ 81]    *x24    *x43    
        +coeff[ 82]    *x22*x32*x43    
        +coeff[ 83]    *x24*x31*x42    
        +coeff[ 84]    *x22*x33*x42    
        +coeff[ 85]    *x24*x32*x41    
        +coeff[ 86]    *x23*x31*x44    
        +coeff[ 87]*x11    *x33*x42*x51
        +coeff[ 88]    *x22    *x45*x51
    ;
    v_y_e_q3en_175                                =v_y_e_q3en_175                                
        +coeff[ 89]    *x24    *x45    
        +coeff[ 90]            *x42    
        +coeff[ 91]        *x31*x41    
        +coeff[ 92]*x11*x21            
        +coeff[ 93]*x11        *x42    
        +coeff[ 94]*x11*x21*x31*x41    
        +coeff[ 95]*x11    *x32*x41    
        +coeff[ 96]*x11        *x42*x51
        ;

    return v_y_e_q3en_175                                ;
}
float p_e_q3en_175                                (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1237194E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.12228169E-03,-0.24521552E-01, 0.94111692E-02,-0.64111780E-02,
        -0.12807224E-01, 0.46820715E-02, 0.99070072E-02,-0.84238546E-02,
        -0.53556304E-03,-0.16597165E-02,-0.27974655E-02, 0.37911561E-03,
        -0.19473728E-02,-0.11298545E-02, 0.13976988E-02,-0.11014250E-04,
        -0.92488551E-03, 0.87834764E-04, 0.27080326E-03,-0.31236722E-03,
        -0.13944973E-02,-0.16838816E-03, 0.48157459E-03,-0.10023424E-02,
        -0.78056537E-03, 0.12958438E-03,-0.26273055E-03, 0.74227387E-03,
         0.49833854E-03,-0.92026203E-04, 0.42088210E-03, 0.18822122E-03,
         0.16918317E-03,-0.24061624E-03,-0.16405445E-02,-0.85841137E-03,
        -0.34680753E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_175                                =v_p_e_q3en_175                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_175                                =v_p_e_q3en_175                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_175                                =v_p_e_q3en_175                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]    *x24    *x41*x52
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3en_175                                =v_p_e_q3en_175                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x23*x31    *x51
        ;

    return v_p_e_q3en_175                                ;
}
float l_e_q3en_175                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1916899E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.16014824E-01,-0.32665953E+00,-0.25707668E-01, 0.11886037E-01,
        -0.31147899E-01,-0.15945872E-01,-0.59049870E-02,-0.53916932E-02,
         0.20478938E-02, 0.11516874E-01, 0.82552833E-02, 0.49224950E-03,
        -0.57167839E-03, 0.92278956E-03,-0.74596237E-03, 0.41847280E-02,
        -0.17155856E-02,-0.25588786E-02, 0.14503733E-02, 0.64466946E-03,
         0.45500015E-03, 0.47322601E-03, 0.52284769E-03, 0.17416318E-02,
         0.10766978E-02, 0.21741728E-02, 0.12761320E-01, 0.55014994E-02,
         0.54798927E-02, 0.20993224E-02, 0.29583315E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_175                                =v_l_e_q3en_175                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3en_175                                =v_l_e_q3en_175                                
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]*x11*x22            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3en_175                                =v_l_e_q3en_175                                
        +coeff[ 26]    *x23*x31*x43    
        +coeff[ 27]    *x23    *x44    
        +coeff[ 28]    *x21*x32*x44    
        +coeff[ 29]*x11*x24*x31    *x51
        +coeff[ 30]*x11*x22*x32*x41*x51
        ;

    return v_l_e_q3en_175                                ;
}
float x_e_q3en_150                                (float *x,int m){
    int ncoeff= 20;
    float avdat=  0.5577217E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
         0.11756958E-02,-0.10339926E-01, 0.11166138E+00, 0.23137015E+00,
         0.28159305E-01, 0.18231137E-01,-0.37986916E-02,-0.37188020E-02,
        -0.11382028E-01,-0.93110353E-02,-0.11642798E-02,-0.23842866E-02,
         0.22370594E-02,-0.51521850E-02,-0.12142244E-02,-0.61907788E-03,
        -0.43927261E-03,-0.11960663E-02,-0.36911606E-02,-0.22575092E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_150                                =v_x_e_q3en_150                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_q3en_150                                =v_x_e_q3en_150                                
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        ;

    return v_x_e_q3en_150                                ;
}
float t_e_q3en_150                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5538996E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.31050327E-02,-0.79464585E-01, 0.18153990E-01,-0.56749573E-02,
        -0.38674639E-02, 0.36003785E-05, 0.25423525E-02,-0.91922859E-03,
         0.46290830E-03,-0.78367098E-03, 0.19769364E-02, 0.77021081E-03,
         0.91502495E-03, 0.32466368E-03,-0.16561142E-03, 0.62133285E-03,
         0.31807608E-03, 0.25119932E-03, 0.54224895E-03, 0.14589864E-03,
         0.89378597E-03, 0.12729452E-03, 0.11798730E-03,-0.23974694E-03,
        -0.25051742E-03,-0.16071329E-03, 0.59066288E-03,-0.29063740E-03,
        -0.52616006E-03,-0.40754408E-03, 0.19853099E-02, 0.15556382E-02,
        -0.48225058E-04, 0.67376634E-04, 0.17628189E-02, 0.15421602E-02,
         0.14106503E-03,-0.33009451E-03, 0.25926760E-03, 0.54119104E-04,
         0.46613834E-04, 0.46411256E-04, 0.63838372E-04, 0.35513516E-04,
         0.24253322E-04, 0.98644501E-04, 0.81478333E-03,-0.11517962E-03,
        -0.14775558E-03, 0.10519848E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_150                                =v_t_e_q3en_150                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_150                                =v_t_e_q3en_150                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x32        
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_q3en_150                                =v_t_e_q3en_150                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_150                                =v_t_e_q3en_150                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x32    *x51
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_150                                =v_t_e_q3en_150                                
        +coeff[ 44]*x13    *x31*x41    
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]        *x32*x42*x51
        +coeff[ 48]    *x21*x31*x41*x52
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_q3en_150                                ;
}
float y_e_q3en_150                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1761845E-03;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.13316445E-03, 0.10616180E+00,-0.95139049E-01,-0.68498217E-01,
        -0.25269583E-01, 0.47465388E-01, 0.28041072E-01,-0.32929629E-01,
        -0.14441316E-01,-0.70870882E-02, 0.22976671E-02,-0.10105687E-01,
        -0.29911413E-02,-0.89318615E-04,-0.67549432E-03, 0.88640628E-03,
        -0.65127523E-02,-0.15179774E-02, 0.15425388E-02,-0.24161814E-02,
        -0.18869268E-02, 0.51367874E-02,-0.53897761E-02, 0.54486154E-03,
         0.16164922E-02,-0.18752320E-02,-0.13253720E-02, 0.12364495E-02,
        -0.49306396E-02, 0.13535537E-02, 0.24806578E-02,-0.31378274E-02,
        -0.10655733E-01,-0.26528002E-02,-0.47063758E-03, 0.53293328E-03,
         0.12790317E-02, 0.29891651E-03,-0.35332559E-03, 0.84587076E-03,
        -0.23880068E-02,-0.33509387E-02,-0.15911169E-02, 0.12728155E-02,
        -0.30779043E-04, 0.76269280E-04,-0.26500160E-02, 0.27913544E-04,
        -0.18837865E-04, 0.34355160E-03,-0.39428339E-03, 0.22914715E-03,
        -0.11889982E-02,-0.10148197E-02, 0.52631728E-03,-0.50975261E-04,
        -0.48762769E-04, 0.58881589E-03,-0.80059086E-04, 0.35358212E-03,
        -0.45216014E-03, 0.44111570E-03,-0.16581015E-03, 0.10342208E-03,
         0.68019831E-03, 0.29220429E-03, 0.19220586E-02, 0.35237279E-02,
        -0.49514492E-03,-0.90923868E-02,-0.42958790E-02,-0.59453603E-02,
        -0.16097378E-02, 0.16671222E-04,-0.19944226E-04,-0.30092517E-04,
        -0.39343991E-04, 0.49625920E-04, 0.34776605E-02, 0.45002503E-02,
        -0.73760923E-04, 0.27285330E-02,-0.42241602E-03, 0.88632887E-03,
         0.99374796E-04,-0.41389599E-03, 0.49676839E-03,-0.20210879E-03,
        -0.81197213E-05, 0.10347470E-04, 0.24215535E-04, 0.25699157E-04,
        -0.67093672E-04, 0.61058694E-04,-0.98002864E-04,-0.82752289E-04,
        -0.12109587E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 44]            *x45*x51
        +coeff[ 45]    *x21    *x43*x52
        +coeff[ 46]    *x24    *x43    
        +coeff[ 47]    *x21            
        +coeff[ 48]                *x51
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x22    *x41*x52
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11        *x41*x51
        +coeff[ 57]            *x43*x51
        +coeff[ 58]*x11*x21    *x41*x51
        +coeff[ 59]    *x21    *x41*x52
        +coeff[ 60]        *x33*x42    
        +coeff[ 61]*x11*x21    *x43    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 62]        *x34*x41    
        +coeff[ 63]            *x41*x53
        +coeff[ 64]*x11*x21*x31*x42    
        +coeff[ 65]*x11*x21*x32*x41    
        +coeff[ 66]    *x23    *x43    
        +coeff[ 67]    *x23*x31*x42    
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]    *x24*x31*x42    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]    *x22*x34*x41    
        +coeff[ 73]    *x22            
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11*x22*x31        
        +coeff[ 77]        *x31    *x53
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]    *x21*x32*x43    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 80]*x11*x22    *x41*x51
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x21*x34*x41    
        +coeff[ 84]    *x21    *x41*x53
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]    *x24*x31    *x51
        +coeff[ 88]            *x42    
    ;
    v_y_e_q3en_150                                =v_y_e_q3en_150                                
        +coeff[ 89]        *x31*x41    
        +coeff[ 90]            *x44    
        +coeff[ 91]*x12*x21    *x41    
        +coeff[ 92]            *x43*x52
        +coeff[ 93]*x11*x23    *x41    
        +coeff[ 94]    *x21*x32*x41*x51
        +coeff[ 95]    *x23*x31    *x51
        +coeff[ 96]        *x31*x44*x51
        ;

    return v_y_e_q3en_150                                ;
}
float p_e_q3en_150                                (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.2059853E-04;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.22089809E-04,-0.24891781E-01, 0.89107426E-02,-0.63906102E-02,
        -0.12936251E-01, 0.46864301E-02, 0.98713841E-02,-0.82317041E-02,
        -0.54732559E-03,-0.16546908E-02,-0.27167830E-02, 0.41222011E-03,
        -0.19469729E-02,-0.11045990E-02, 0.13324820E-02,-0.10446266E-02,
         0.55367407E-04, 0.97812119E-06,-0.31982805E-03,-0.13479157E-02,
        -0.14486001E-03, 0.48141342E-03,-0.10310834E-02,-0.76860859E-03,
         0.26088004E-03, 0.12450335E-03,-0.29896738E-03,-0.15920878E-02,
        -0.91289362E-03, 0.68272755E-03,-0.39094730E-03,-0.87674998E-04,
         0.33380216E-03, 0.16927003E-03,-0.11952429E-03,-0.24358834E-03,
        -0.43853949E-03,-0.16374361E-03, 0.37858714E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_150                                =v_p_e_q3en_150                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_q3en_150                                =v_p_e_q3en_150                                
        +coeff[ 17]*x13    *x31        
        +coeff[ 18]        *x33        
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]*x11    *x31        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_150                                =v_p_e_q3en_150                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x23    *x41*x51
        +coeff[ 30]    *x22*x31*x42*x52
        +coeff[ 31]        *x31    *x52
        +coeff[ 32]    *x21*x31*x42    
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_q3en_150                                =v_p_e_q3en_150                                
        +coeff[ 35]    *x21*x31    *x52
        +coeff[ 36]    *x23*x31    *x51
        +coeff[ 37]    *x25*x31        
        +coeff[ 38]    *x22    *x41*x52
        ;

    return v_p_e_q3en_150                                ;
}
float l_e_q3en_150                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1868282E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.11344035E-01,-0.32625705E+00,-0.25727907E-01, 0.11868410E-01,
        -0.31030156E-01,-0.15660793E-01,-0.59283441E-02,-0.53159771E-02,
         0.20475090E-02, 0.10399698E-01, 0.87882346E-02, 0.98062516E-03,
         0.39728521E-03, 0.87195935E-04,-0.35515861E-02,-0.81887614E-03,
         0.47558201E-02,-0.22072885E-02, 0.15554421E-02, 0.32035806E-02,
         0.62393252E-03, 0.78284449E-03, 0.45736239E-03,-0.29744973E-03,
         0.19136342E-02, 0.12382488E-02, 0.46875142E-02, 0.20690057E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_150                                =v_l_e_q3en_150                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3en_150                                =v_l_e_q3en_150                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x23    *x42*x52
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_q3en_150                                =v_l_e_q3en_150                                
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        ;

    return v_l_e_q3en_150                                ;
}
float x_e_q3en_125                                (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.4490049E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
         0.19319978E-02,-0.10225371E-01, 0.11162470E+00, 0.23553702E+00,
         0.28133325E-01, 0.18770268E-01,-0.37836863E-02,-0.36924768E-02,
        -0.93344944E-02,-0.94202748E-02,-0.11530583E-02,-0.22874898E-02,
         0.22697784E-02,-0.48333723E-02,-0.12004952E-02,-0.63817715E-03,
        -0.20718972E-03,-0.45965839E-03,-0.14461960E-02,-0.68857507E-02,
        -0.20542128E-02,-0.32556520E-02,-0.62100012E-02, 0.88009663E-03,
         0.58270479E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_125                                =v_x_e_q3en_125                                
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_q3en_125                                =v_x_e_q3en_125                                
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x23*x31*x43    
        ;

    return v_x_e_q3en_125                                ;
}
float t_e_q3en_125                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4740986E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.24204871E-02,-0.80684759E-01, 0.18157003E-01,-0.58455174E-02,
        -0.38143715E-02,-0.51975366E-06, 0.25161970E-02,-0.91086770E-03,
         0.49705955E-03,-0.79077634E-03, 0.19150801E-02, 0.86014770E-03,
         0.89205720E-03, 0.30629788E-03,-0.16304782E-03, 0.63271087E-03,
         0.31561591E-03, 0.32095041E-03, 0.49877766E-03, 0.16322646E-03,
         0.82278263E-03, 0.11167565E-03, 0.10900269E-03,-0.19670736E-03,
        -0.14725950E-03, 0.62932726E-03,-0.34567123E-03,-0.49596169E-03,
        -0.39772777E-03, 0.19892845E-02, 0.14840170E-02,-0.22718377E-03,
        -0.46794092E-04, 0.65832995E-04, 0.17779619E-02, 0.17038648E-02,
        -0.37970021E-03,-0.12000121E-04, 0.48158166E-04, 0.54387459E-04,
        -0.37810787E-04,-0.51481355E-04, 0.62777661E-04, 0.53386022E-04,
         0.52742012E-04, 0.94939175E-03, 0.15591877E-03,-0.11001874E-03,
        -0.19214730E-03, 0.33236900E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_125                                =v_t_e_q3en_125                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]            *x42    
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]            *x42*x51
    ;
    v_t_e_q3en_125                                =v_t_e_q3en_125                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x32        
        +coeff[ 19]        *x32        
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x22        *x51
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_125                                =v_t_e_q3en_125                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x23            
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_q3en_125                                =v_t_e_q3en_125                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]*x12                
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11*x21    *x42    
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3en_125                                =v_t_e_q3en_125                                
        +coeff[ 44]*x13    *x31*x41    
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x22    *x42*x51
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]    *x21*x31*x41*x52
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_q3en_125                                ;
}
float y_e_q3en_125                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2245650E-03;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.13272646E-03, 0.10351017E+00,-0.97389758E-01,-0.68925358E-01,
        -0.25158634E-01, 0.47412142E-01, 0.28000860E-01,-0.33648279E-01,
        -0.14687344E-01,-0.70543136E-02, 0.22550465E-02,-0.10157204E-01,
        -0.30373677E-02,-0.50179828E-04,-0.33116169E-03,-0.11015214E-03,
         0.85918186E-03,-0.65166280E-02,-0.15260880E-02, 0.15650347E-02,
        -0.24362670E-02,-0.18800525E-02, 0.52974247E-02,-0.53935512E-02,
         0.54032129E-03, 0.16214747E-02,-0.19150377E-02,-0.13663606E-02,
         0.96033892E-03,-0.39213952E-02, 0.13489916E-02, 0.24370134E-02,
        -0.36810655E-02,-0.12743833E-01,-0.29106813E-02,-0.58018934E-03,
         0.53092375E-04, 0.48975198E-03, 0.12541056E-02, 0.28754844E-03,
        -0.36197645E-03, 0.83731348E-03,-0.20797816E-02,-0.29831193E-02,
        -0.15847601E-02, 0.13607026E-02, 0.13678875E-05, 0.49755137E-04,
        -0.28025736E-02,-0.14084329E-03,-0.38363629E-04, 0.33828971E-03,
        -0.13722810E-03, 0.21970863E-03,-0.12805318E-02,-0.89608005E-03,
         0.58038183E-03,-0.11125045E-01,-0.71745263E-02, 0.29367891E-04,
        -0.46929817E-04, 0.58284821E-03, 0.37034857E-03,-0.18049590E-03,
         0.44369313E-03, 0.12738269E-03, 0.64159645E-03, 0.31716845E-03,
         0.20389226E-02, 0.37285292E-02, 0.31293217E-04,-0.44013083E-03,
        -0.49559902E-02,-0.18209002E-02, 0.25752903E-04, 0.86443952E-05,
        -0.33460357E-04,-0.19810182E-04,-0.27547396E-04,-0.30645431E-04,
        -0.97893761E-04, 0.49950304E-04, 0.39117858E-02, 0.49133752E-02,
        -0.92374445E-04, 0.29586924E-02,-0.42856851E-03, 0.92185952E-03,
        -0.41308755E-03, 0.50610176E-03,-0.18689019E-03, 0.19348630E-03,
         0.33120377E-04, 0.10251494E-04, 0.83377545E-05,-0.26179247E-04,
        -0.36822271E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]        *x34*x41    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]        *x31    *x52
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]            *x43    
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]    *x23*x31        
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x22*x31    *x51
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x21    *x45    
        +coeff[ 31]    *x23*x32*x41    
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x22*x32*x43    
        +coeff[ 34]    *x24*x32*x41    
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]    *x21            
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]    *x22    *x43    
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 44]    *x24*x31        
        +coeff[ 45]    *x23    *x41*x51
        +coeff[ 46]            *x45*x51
        +coeff[ 47]    *x21    *x43*x52
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]    *x21    *x45*x52
        +coeff[ 50]                *x51
        +coeff[ 51]    *x21    *x43    
        +coeff[ 52]        *x31*x44    
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 53]        *x33    *x51
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]    *x22*x33        
        +coeff[ 56]    *x22    *x41*x52
        +coeff[ 57]    *x22*x31*x44    
        +coeff[ 58]    *x22*x33*x42    
        +coeff[ 59]    *x22            
        +coeff[ 60]*x11        *x41*x51
        +coeff[ 61]            *x43*x51
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 62]    *x21    *x41*x52
        +coeff[ 63]        *x33*x42    
        +coeff[ 64]*x11*x21    *x43    
        +coeff[ 65]            *x41*x53
        +coeff[ 66]*x11*x21*x31*x42    
        +coeff[ 67]*x11*x21*x32*x41    
        +coeff[ 68]    *x23    *x43    
        +coeff[ 69]    *x23*x31*x42    
        +coeff[ 70]*x11*x21    *x43*x51
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 71]    *x24    *x41*x51
        +coeff[ 72]    *x24*x31*x42    
        +coeff[ 73]    *x22*x34*x41    
        +coeff[ 74]        *x31*x41    
        +coeff[ 75]    *x21        *x51
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11    *x31    *x51
        +coeff[ 79]*x11*x22*x31        
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 80]*x11*x21    *x41*x51
        +coeff[ 81]        *x31    *x53
        +coeff[ 82]    *x21*x31*x44    
        +coeff[ 83]    *x21*x32*x43    
        +coeff[ 84]*x11*x22    *x41*x51
        +coeff[ 85]    *x21*x33*x42    
        +coeff[ 86]    *x22    *x43*x51
        +coeff[ 87]    *x21*x34*x41    
        +coeff[ 88]    *x22*x31*x42*x51
    ;
    v_y_e_q3en_125                                =v_y_e_q3en_125                                
        +coeff[ 89]    *x23*x33        
        +coeff[ 90]    *x24*x31    *x51
        +coeff[ 91]*x11    *x33*x44    
        +coeff[ 92]            *x42    
        +coeff[ 93]        *x32        
        +coeff[ 94]            *x42*x51
        +coeff[ 95]            *x44    
        +coeff[ 96]*x13                
        ;

    return v_y_e_q3en_125                                ;
}
float p_e_q3en_125                                (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1183416E-05;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.41272679E-05,-0.25283923E-01, 0.84479479E-02,-0.64222822E-02,
        -0.13019719E-01, 0.46647242E-02, 0.98455809E-02,-0.84151216E-02,
        -0.60428830E-03,-0.16465045E-02,-0.26628941E-02, 0.40353584E-03,
        -0.19286391E-02,-0.10870297E-02, 0.13690905E-02,-0.58047603E-05,
        -0.10920530E-02, 0.53124786E-04, 0.26068275E-03,-0.31929283E-03,
        -0.13191975E-02,-0.13063768E-03, 0.47752706E-03,-0.11020671E-02,
        -0.80237188E-03, 0.11869548E-03,-0.29948185E-03,-0.17798936E-02,
        -0.95157156E-03, 0.71543519E-03,-0.63410946E-04, 0.35260056E-03,
         0.14958512E-03,-0.12226999E-03,-0.26799584E-03,-0.46626333E-03,
        -0.20916981E-03,-0.15211634E-03, 0.42068784E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_125                                =v_p_e_q3en_125                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_q3en_125                                =v_p_e_q3en_125                                
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_q3en_125                                =v_p_e_q3en_125                                
        +coeff[ 26]            *x41*x52
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]    *x23    *x41*x51
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21*x31    *x52
    ;
    v_p_e_q3en_125                                =v_p_e_q3en_125                                
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x25*x31        
        +coeff[ 37]    *x22*x31    *x52
        +coeff[ 38]    *x22    *x41*x52
        ;

    return v_p_e_q3en_125                                ;
}
float l_e_q3en_125                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1629525E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.93638804E-02,-0.33113918E+00,-0.25789686E-01, 0.11740226E-01,
        -0.31761963E-01,-0.15664373E-01,-0.56655346E-02,-0.49883798E-02,
         0.20294634E-02, 0.69015794E-02, 0.74373842E-02, 0.20942478E-03,
         0.42915704E-04, 0.15460699E-02,-0.31262077E-02,-0.85041812E-03,
        -0.12684758E-02, 0.39665489E-02,-0.19419376E-02, 0.15058429E-02,
         0.77098212E-03,-0.19624144E-03, 0.63563016E-03, 0.60124299E-03,
         0.55038923E-03, 0.49095071E-03,-0.11410804E-02, 0.85281935E-02,
         0.99787768E-03, 0.67314473E-02, 0.10660702E-02, 0.75917533E-02,
         0.10374635E-02, 0.10847674E-02, 0.89095253E-03, 0.68004127E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_125                                =v_l_e_q3en_125                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3en_125                                =v_l_e_q3en_125                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x52
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3en_125                                =v_l_e_q3en_125                                
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]        *x34*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]        *x33*x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]    *x24    *x42    
        +coeff[ 34]*x12*x23    *x41    
    ;
    v_l_e_q3en_125                                =v_l_e_q3en_125                                
        +coeff[ 35]    *x21*x32*x44    
        ;

    return v_l_e_q3en_125                                ;
}
float x_e_q3en_100                                (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.4558034E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.25916849E-02,-0.10059288E-01, 0.11169468E+00, 0.23869103E+00,
         0.28071690E-01, 0.19217525E-01,-0.37720760E-02,-0.36608332E-02,
        -0.11598098E-02,-0.22356545E-02,-0.94512021E-02,-0.84487135E-02,
        -0.60921695E-04,-0.11773305E-02,-0.12369383E-02, 0.22926205E-02,
         0.12252080E-02,-0.42604902E-02,-0.63873746E-03, 0.99297133E-04,
        -0.45923737E-03,-0.16095939E-02,-0.63660899E-02,-0.40729297E-02,
        -0.30557141E-02, 0.75114373E-03,-0.14464461E-02,-0.40227841E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q3en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_q3en_100                                =v_x_e_q3en_100                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23            
    ;
    v_x_e_q3en_100                                =v_x_e_q3en_100                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x42*x51
        +coeff[ 19]*x11            *x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_x_e_q3en_100                                =v_x_e_q3en_100                                
        +coeff[ 26]    *x21*x32*x42*x52
        +coeff[ 27]    *x23*x32*x42    
        ;

    return v_x_e_q3en_100                                ;
}
float t_e_q3en_100                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4895522E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.23336259E-02,-0.81634715E-01, 0.18167702E-01,-0.60459063E-02,
        -0.37521846E-02, 0.72950803E-06, 0.24672151E-02,-0.91392704E-03,
         0.46069850E-03,-0.78602356E-03, 0.18899828E-02, 0.81975537E-03,
         0.98175218E-03, 0.17738136E-03, 0.28063060E-03,-0.16271159E-03,
         0.56432205E-03, 0.28637476E-03, 0.33305725E-03, 0.85020997E-03,
        -0.24907131E-03, 0.11940274E-03, 0.11098875E-03,-0.19331033E-03,
         0.58799487E-03, 0.63297164E-03, 0.14110778E-03,-0.54749456E-03,
         0.20275025E-02, 0.15372772E-02,-0.30068253E-03,-0.46641489E-04,
        -0.23391710E-03, 0.58727128E-04,-0.30000208E-03,-0.29201465E-03,
         0.17690662E-02, 0.15226544E-02, 0.19099681E-03,-0.24720683E-03,
         0.45428584E-04, 0.59530783E-04,-0.68780333E-04, 0.72849012E-04,
         0.58398749E-04, 0.52760523E-04, 0.82313054E-03, 0.37081634E-04,
         0.22049939E-03,-0.12459989E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]                *x52
    ;
    v_t_e_q3en_100                                =v_t_e_q3en_100                                
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]            *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_q3en_100                                =v_t_e_q3en_100                                
        +coeff[ 17]            *x42*x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]    *x21*x31*x41*x53
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_q3en_100                                =v_t_e_q3en_100                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x23            
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]    *x22        *x51
        +coeff[ 33]                *x53
        +coeff[ 34]    *x23        *x51
    ;
    v_t_e_q3en_100                                =v_t_e_q3en_100                                
        +coeff[ 35]    *x21    *x42*x51
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]    *x22*x31*x41*x51
        +coeff[ 40]*x11        *x42    
        +coeff[ 41]*x11*x23            
        +coeff[ 42]    *x22        *x52
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_q3en_100                                =v_t_e_q3en_100                                
        +coeff[ 44]            *x42*x52
        +coeff[ 45]*x13    *x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]        *x33*x41*x51
        +coeff[ 48]    *x22    *x42*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_q3en_100                                ;
}
float y_e_q3en_100                                (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1063473E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.10021045E-02, 0.99400893E-01,-0.10092130E+00,-0.69495946E-01,
        -0.24969162E-01, 0.47359060E-01, 0.27940195E-01,-0.34116935E-01,
        -0.14693631E-01,-0.70685707E-02, 0.21974302E-02,-0.10022705E-01,
        -0.31553049E-04,-0.36974926E-03, 0.82604366E-03,-0.64702691E-02,
        -0.15037322E-02, 0.15346480E-02,-0.23332646E-02,-0.29903448E-02,
        -0.18613323E-02, 0.57306197E-02,-0.53775613E-02, 0.52854372E-03,
         0.16846013E-02,-0.17493883E-02,-0.14784326E-02, 0.16393014E-02,
         0.23862214E-02, 0.96693414E-03,-0.15469965E-02,-0.33864775E-02,
        -0.22308405E-02,-0.81622816E-03, 0.12349233E-02, 0.33693053E-03,
        -0.37503368E-03, 0.83865691E-03,-0.10376305E-02,-0.14679084E-02,
         0.13718415E-02,-0.27373571E-05, 0.39153208E-04,-0.29368881E-04,
        -0.12412696E-03, 0.22038519E-03, 0.37049633E-03,-0.72720548E-03,
         0.53475238E-03, 0.19439509E-04,-0.36239540E-04,-0.43568954E-04,
         0.62111311E-03,-0.89503847E-04,-0.21629364E-03, 0.46892563E-03,
        -0.66368651E-04, 0.11063518E-03, 0.66237524E-03, 0.28958617E-03,
        -0.35406233E-03, 0.97318017E-03, 0.16020819E-02, 0.36610776E-03,
        -0.52674790E-03,-0.11212967E-01,-0.35511469E-02,-0.13144380E-01,
        -0.69962072E-04,-0.22163444E-04,-0.27346094E-04, 0.44363973E-04,
        -0.43293225E-04, 0.53105861E-04, 0.59588008E-04, 0.14824899E-02,
        -0.13726254E-03, 0.22204560E-02,-0.99635014E-04, 0.12422338E-02,
        -0.54197054E-03, 0.74066548E-03, 0.10497234E-03,-0.48399591E-03,
        -0.37228970E-02,-0.57551675E-02,-0.74308240E-02,-0.36644826E-02,
        -0.20907274E-02, 0.22957784E-02, 0.38970902E-02, 0.40335418E-02,
         0.23232671E-02,-0.29475639E-05, 0.16710990E-04, 0.86461605E-05,
        -0.57506845E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]            *x45*x51
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 53]*x11*x21    *x41*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]            *x41*x53
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x43    
        +coeff[ 61]    *x23*x31*x42    
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x23*x33        
        +coeff[ 64]    *x24    *x41*x51
        +coeff[ 65]    *x22*x31*x44    
        +coeff[ 66]    *x24    *x43    
        +coeff[ 67]    *x22*x32*x43    
        +coeff[ 68]    *x24    *x45    
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x11    *x31    *x51
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 71]*x11    *x31*x42    
        +coeff[ 72]*x11*x22*x31        
        +coeff[ 73]        *x31    *x53
        +coeff[ 74]    *x22    *x44    
        +coeff[ 75]    *x21*x31*x44    
        +coeff[ 76]    *x23*x31    *x51
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]*x11*x22    *x41*x51
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 80]    *x22    *x43*x51
        +coeff[ 81]    *x21*x34*x41    
        +coeff[ 82]    *x21    *x41*x53
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x22    *x45    
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_q3en_100                                =v_y_e_q3en_100                                
        +coeff[ 89]    *x23    *x45    
        +coeff[ 90]    *x23*x31*x44    
        +coeff[ 91]    *x23*x32*x43    
        +coeff[ 92]    *x23*x33*x42    
        +coeff[ 93]*x11                
        +coeff[ 94]        *x31*x41    
        +coeff[ 95]        *x32        
        +coeff[ 96]*x11*x21            
        ;

    return v_y_e_q3en_100                                ;
}
float p_e_q3en_100                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1148561E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.11182431E-03,-0.25910974E-01, 0.77138604E-02,-0.64036301E-02,
        -0.13153346E-01, 0.46212561E-02, 0.98030744E-02,-0.84752496E-02,
        -0.65069471E-03,-0.16091863E-02,-0.26289639E-02, 0.39378475E-03,
        -0.18781468E-02,-0.10720367E-02, 0.14050470E-02,-0.10726183E-02,
         0.75639364E-04, 0.25165910E-03,-0.29940647E-03,-0.13121839E-02,
         0.47727878E-03,-0.11185011E-02,-0.78423729E-03,-0.67275978E-03,
         0.11528943E-03,-0.28647750E-03,-0.94447743E-04,-0.37542661E-03,
        -0.24500539E-03,-0.80675927E-04, 0.33555838E-03, 0.16381587E-03,
        -0.11895601E-03,-0.22728954E-03,-0.17147660E-02,-0.96127833E-03,
         0.69104007E-03, 0.38373543E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_q3en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3en_100                                =v_p_e_q3en_100                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_q3en_100                                =v_p_e_q3en_100                                
        +coeff[ 17]*x11    *x31        
        +coeff[ 18]        *x33        
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x23*x31    *x51
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]            *x41*x52
    ;
    v_p_e_q3en_100                                =v_p_e_q3en_100                                
        +coeff[ 26]    *x23*x31    *x52
        +coeff[ 27]    *x22*x31*x42*x52
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x21*x31    *x52
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3en_100                                =v_p_e_q3en_100                                
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x23    *x41*x51
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_q3en_100                                ;
}
float l_e_q3en_100                                (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1661577E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.86528491E-02,-0.33559921E+00,-0.25726028E-01, 0.11473233E-01,
        -0.32336589E-01,-0.15277136E-01,-0.56179329E-02,-0.50035981E-02,
         0.21734503E-02, 0.87945368E-02, 0.84761605E-02, 0.36128738E-04,
         0.32965097E-03, 0.14629014E-02,-0.33352568E-02,-0.39346208E-03,
        -0.12318204E-02, 0.36891748E-02,-0.21581498E-02, 0.14451522E-02,
         0.86306385E-03, 0.76979928E-03, 0.17827398E-02,-0.41415027E-03,
        -0.67846145E-03,-0.72856783E-03, 0.34645670E-02, 0.54791346E-02,
        -0.58537553E-03, 0.13430213E-02,-0.18187537E-02, 0.78354432E-03,
         0.10705343E-01, 0.44805701E-02, 0.58355280E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3en_100                                =v_l_e_q3en_100                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3en_100                                =v_l_e_q3en_100                                
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q3en_100                                =v_l_e_q3en_100                                
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x13            *x51
        +coeff[ 29]    *x22*x32    *x51
        +coeff[ 30]*x11*x24            
        +coeff[ 31]*x11    *x31*x43    
        +coeff[ 32]    *x23*x33*x41    
        +coeff[ 33]    *x23*x32*x42    
        +coeff[ 34]    *x21*x34*x42    
        ;

    return v_l_e_q3en_100                                ;
}
