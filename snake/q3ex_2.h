float x_e_q3ex_2_1200                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1403023E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.14186012E-01, 0.24988584E+00, 0.69441676E-01,-0.13343049E-01,
         0.26774786E-01, 0.82431613E-02,-0.88568713E-05,-0.75077303E-02,
        -0.62560276E-02,-0.48715654E-02,-0.11857311E-01,-0.64830733E-02,
        -0.56402129E-03,-0.10046498E-02, 0.14631102E-02,-0.35159371E-02,
         0.43388573E-02,-0.31251568E-03, 0.11298106E-02,-0.42351405E-03,
         0.80816069E-03,-0.30029067E-03, 0.77249523E-03, 0.58985886E-04,
         0.41006418E-03,-0.11974474E-02, 0.78727322E-03, 0.20925370E-02,
        -0.28737986E-02,-0.12544558E-02,-0.25267613E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_1200                              =v_x_e_q3ex_2_1200                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_1200                              =v_x_e_q3ex_2_1200                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_1200                              =v_x_e_q3ex_2_1200                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x21*x31*x41*x53
        +coeff[ 30]    *x23*x32*x42    
        ;

    return v_x_e_q3ex_2_1200                              ;
}
float t_e_q3ex_2_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6811322E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64573418E-02,-0.28004406E-01, 0.78749165E-01, 0.36704945E-02,
        -0.59035434E-02,-0.41306173E-03,-0.21160508E-02,-0.25079390E-02,
        -0.88634697E-03,-0.11139987E-02, 0.24995950E-03, 0.23569957E-03,
        -0.45877122E-03,-0.23443258E-03,-0.34736708E-03, 0.48706282E-03,
         0.17484380E-02,-0.10647255E-03, 0.49389713E-03, 0.81244754E-04,
         0.24020953E-03, 0.85314148E-03,-0.27568411E-03, 0.39470257E-03,
        -0.78752800E-03,-0.54360612E-03,-0.41510703E-03, 0.21042956E-03,
         0.70027960E-03, 0.10092932E-03, 0.10255784E-03, 0.23704428E-03,
        -0.13840791E-03,-0.12028704E-03,-0.11273131E-03,-0.29108836E-03,
         0.95150441E-04, 0.41610692E-03,-0.31431613E-03,-0.18210741E-03,
        -0.15338128E-04, 0.30596395E-04,-0.21318168E-04,-0.65148648E-04,
         0.30735682E-04, 0.12139472E-03, 0.37936941E-04, 0.11520198E-03,
        -0.31432096E-03, 0.13461505E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_1200                              =v_t_e_q3ex_2_1200                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_1200                              =v_t_e_q3ex_2_1200                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_1200                              =v_t_e_q3ex_2_1200                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_1200                              =v_t_e_q3ex_2_1200                              
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]    *x21*x33*x41    
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]    *x22        *x51
    ;
    v_t_e_q3ex_2_1200                              =v_t_e_q3ex_2_1200                              
        +coeff[ 44]*x11            *x52
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_q3ex_2_1200                              ;
}
float y_e_q3ex_2_1200                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2851138E-03;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.24881065E-03, 0.77868477E-01,-0.84296599E-01,-0.59213053E-01,
        -0.23689708E-01, 0.47776137E-01, 0.19729871E-01,-0.33057734E-01,
        -0.11067134E-01,-0.88950815E-02, 0.21179740E-02,-0.80503235E-02,
        -0.49009169E-02, 0.53392588E-02,-0.26228698E-03, 0.94062119E-03,
        -0.50216527E-02, 0.19626380E-02,-0.20470433E-02,-0.14161747E-02,
        -0.34376467E-02,-0.22806160E-02,-0.94446592E-03, 0.41266004E-03,
         0.11675014E-02, 0.34176502E-02, 0.11632402E-03, 0.10639721E-02,
         0.90569368E-03, 0.11759944E-02, 0.41195168E-03,-0.47162166E-03,
        -0.43040626E-02,-0.72606453E-02,-0.49305903E-02, 0.20757010E-02,
         0.17448614E-03, 0.42952897E-03,-0.21466117E-03,-0.14835397E-02,
        -0.36110100E-03,-0.57565328E-03, 0.87351742E-03, 0.30763807E-04,
        -0.26165058E-04,-0.89940913E-04, 0.30711395E-03, 0.11284743E-03,
        -0.89370401E-03,-0.25955070E-03, 0.28104938E-03,-0.75058437E-04,
         0.18658700E-04,-0.53023054E-04,-0.64963661E-03,-0.11086261E-02,
        -0.83407381E-03,-0.21864557E-03, 0.53019810E-03, 0.38133943E-03,
        -0.89819520E-03, 0.17840798E-02, 0.20074847E-02,-0.13198403E-02,
         0.54123986E-03, 0.23860027E-03,-0.32553429E-03,-0.68305124E-03,
         0.75302116E-04,-0.33734753E-02, 0.32566284E-03,-0.32114505E-03,
        -0.23277926E-02, 0.21185125E-03, 0.22282857E-02, 0.45240292E-03,
         0.21411803E-03,-0.44301385E-03,-0.30142081E-03, 0.15109191E-05,
         0.66583954E-04, 0.34977820E-04, 0.61125116E-03, 0.30541843E-04,
        -0.99986079E-04, 0.19814591E-03, 0.46474852E-04, 0.16537518E-02,
        -0.10423698E-03, 0.67613978E-03, 0.67273452E-03,-0.63850843E-04,
         0.63394931E-04,-0.55297243E-03, 0.24761603E-03,-0.10944322E-02,
        -0.46424076E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]        *x31    *x53
        +coeff[ 41]    *x21*x32*x41*x51
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 44]                *x51
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]            *x41*x53
        +coeff[ 50]    *x22*x31    *x52
        +coeff[ 51]        *x32*x43*x52
        +coeff[ 52]    *x22            
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 53]*x12        *x41    
        +coeff[ 54]        *x31*x44    
        +coeff[ 55]        *x32*x43    
        +coeff[ 56]        *x33*x42    
        +coeff[ 57]        *x34*x41    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]    *x21*x31*x42*x51
        +coeff[ 60]    *x22*x33        
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x22*x31*x42*x51
        +coeff[ 64]    *x23*x33        
        +coeff[ 65]    *x21*x31    *x53
        +coeff[ 66]    *x21    *x43*x52
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]*x11        *x41*x53
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]*x11*x21    *x45    
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 71]    *x24*x31    *x51
        +coeff[ 72]    *x22*x32*x43    
        +coeff[ 73]*x11*x21*x32*x43    
        +coeff[ 74]    *x23*x31*x44    
        +coeff[ 75]        *x33*x42*x52
        +coeff[ 76]    *x23*x33    *x51
        +coeff[ 77]    *x22*x32*x43*x51
        +coeff[ 78]    *x24    *x45    
        +coeff[ 79]*x12    *x31        
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 80]*x11    *x31*x42    
        +coeff[ 81]*x11*x22*x31        
        +coeff[ 82]    *x21    *x45    
        +coeff[ 83]*x11    *x31    *x52
        +coeff[ 84]            *x43*x52
        +coeff[ 85]*x11*x21*x32*x41    
        +coeff[ 86]            *x45*x51
        +coeff[ 87]    *x21*x32*x43    
        +coeff[ 88]*x11*x22    *x41*x51
    ;
    v_y_e_q3ex_2_1200                              =v_y_e_q3ex_2_1200                              
        +coeff[ 89]    *x23*x31*x42    
        +coeff[ 90]    *x21*x33*x42    
        +coeff[ 91]*x12*x22*x31        
        +coeff[ 92]        *x33    *x52
        +coeff[ 93]    *x22    *x43*x51
        +coeff[ 94]    *x21*x34*x41    
        +coeff[ 95]    *x22    *x45    
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_1200                              ;
}
float p_e_q3ex_2_1200                              (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.7452264E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.58740952E-04, 0.16863964E-01,-0.35099067E-01, 0.17091697E-01,
        -0.82115224E-02,-0.10492438E-01, 0.78652501E-02,-0.62096870E-03,
        -0.10577452E-03, 0.15602489E-03,-0.31782511E-05, 0.52942336E-02,
         0.15767561E-02, 0.40717931E-02,-0.63397718E-03, 0.30352580E-02,
         0.15682505E-02, 0.11538838E-02, 0.52870470E-04,-0.18489336E-03,
         0.42970254E-03, 0.16302360E-02, 0.58941130E-03, 0.38000609E-03,
        -0.37082672E-03,-0.12161401E-02,-0.18382349E-03,-0.15510637E-02,
        -0.77182386E-03,-0.36924472E-03, 0.78980411E-04,-0.73810859E-03,
         0.86257984E-04, 0.11720117E-02, 0.95833675E-03,-0.28236510E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_1200                              =v_p_e_q3ex_2_1200                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_1200                              =v_p_e_q3ex_2_1200                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_q3ex_2_1200                              =v_p_e_q3ex_2_1200                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_q3ex_2_1200                              =v_p_e_q3ex_2_1200                              
        +coeff[ 35]            *x41*x53
        ;

    return v_p_e_q3ex_2_1200                              ;
}
float l_e_q3ex_2_1200                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1547277E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.15271469E-01,-0.31178191E+00,-0.25354849E-01, 0.12495459E-01,
        -0.32825146E-01,-0.10700017E-01,-0.60551018E-02,-0.42369715E-02,
         0.21495486E-02,-0.29322871E-02,-0.47494923E-02, 0.93950648E-02,
         0.96776634E-02, 0.65673102E-03,-0.77863410E-03, 0.36448534E-02,
        -0.20658907E-02, 0.22157962E-02, 0.73705608E-03, 0.66376122E-03,
         0.47998704E-03, 0.41970942E-03,-0.43088992E-03,-0.64647803E-03,
         0.90953533E-03,-0.64122758E-03, 0.48046368E-02, 0.33821266E-02,
        -0.14967734E-02, 0.97871036E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_1200                              =v_l_e_q3ex_2_1200                              
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_1200                              =v_l_e_q3ex_2_1200                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x34    *x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]*x11    *x31*x41    
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x21        *x53
    ;
    v_l_e_q3ex_2_1200                              =v_l_e_q3ex_2_1200                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]        *x33*x41*x51
        +coeff[ 29]*x11*x23*x32        
        ;

    return v_l_e_q3ex_2_1200                              ;
}
float x_e_q3ex_2_1100                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1212066E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.12625704E-01,-0.75083440E-02, 0.24989264E+00, 0.69505960E-01,
        -0.13338737E-01, 0.26739243E-01, 0.83086882E-02,-0.61756233E-02,
        -0.48712296E-02,-0.11289730E-01,-0.64465767E-02,-0.62184571E-03,
        -0.98633301E-03, 0.14979606E-02,-0.36781922E-02, 0.42361184E-02,
        -0.29501942E-03, 0.11277546E-02,-0.41277229E-03, 0.10157182E-02,
        -0.29935926E-03, 0.75580378E-03,-0.43135959E-04, 0.60976570E-03,
        -0.93778525E-03, 0.69222256E-03, 0.22524272E-02,-0.23759550E-02,
        -0.14753818E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_1100                              =v_x_e_q3ex_2_1100                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3ex_2_1100                              =v_x_e_q3ex_2_1100                              
        +coeff[ 17]                *x53
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_2_1100                              =v_x_e_q3ex_2_1100                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        ;

    return v_x_e_q3ex_2_1100                              ;
}
float t_e_q3ex_2_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6212603E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59872726E-02,-0.28010722E-01, 0.78743234E-01, 0.36621175E-02,
        -0.58991425E-02,-0.40288898E-03,-0.21204664E-02,-0.24705268E-02,
        -0.11011158E-02, 0.23032942E-03,-0.88441093E-03, 0.26557079E-03,
        -0.42230909E-03,-0.19468821E-03,-0.34886916E-03, 0.48797851E-03,
         0.17597144E-02,-0.10915698E-03, 0.51154330E-03, 0.10273566E-03,
         0.22739229E-03, 0.85228460E-03,-0.25948387E-03, 0.39835309E-03,
        -0.74520975E-03,-0.51565858E-03,-0.44597339E-03, 0.20931798E-03,
         0.19872795E-03, 0.98012591E-04, 0.13473888E-03, 0.71171889E-04,
         0.26840589E-03,-0.12899317E-03,-0.12209779E-03, 0.35948560E-03,
        -0.27939098E-03,-0.21310826E-03, 0.60664734E-03,-0.13914309E-04,
         0.32989097E-04,-0.25594623E-04,-0.52108207E-04,-0.89941183E-04,
         0.50152346E-04,-0.24630444E-03, 0.14228160E-04,-0.25399981E-03,
         0.54653239E-04,-0.21796886E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_1100                              =v_t_e_q3ex_2_1100                              
        +coeff[  8]            *x42    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_1100                              =v_t_e_q3ex_2_1100                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_1100                              =v_t_e_q3ex_2_1100                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41*x52
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]*x11*x21*x31*x41    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_1100                              =v_t_e_q3ex_2_1100                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]    *x22        *x51
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_q3ex_2_1100                              =v_t_e_q3ex_2_1100                              
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x21*x32*x42    
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_q3ex_2_1100                              ;
}
float y_e_q3ex_2_1100                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.8632585E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.77129173E-03, 0.77557728E-01,-0.84490761E-01,-0.59414215E-01,
        -0.23707684E-01, 0.47725238E-01, 0.19747650E-01,-0.33066474E-01,
        -0.11123998E-01,-0.89084506E-02, 0.21024623E-02,-0.79296818E-02,
        -0.49610212E-02, 0.56202146E-02,-0.36248725E-03, 0.94394555E-03,
        -0.49938476E-02, 0.18730531E-02,-0.21180485E-02,-0.14614897E-02,
        -0.33279315E-02,-0.25425432E-02,-0.95850369E-03, 0.42651431E-03,
         0.21136468E-02, 0.36119451E-02, 0.18747669E-02, 0.12045600E-02,
         0.50013972E-03,-0.86348485E-02,-0.50707916E-02, 0.21342800E-02,
        -0.28949569E-03, 0.10012631E-03, 0.54015603E-04,-0.46978319E-04,
        -0.94192728E-04, 0.90588612E-04, 0.74041093E-03,-0.44103106E-03,
        -0.49101794E-02, 0.42486275E-03,-0.21723130E-03,-0.14027451E-02,
        -0.34996972E-03,-0.57432934E-03, 0.91431831E-03, 0.37736911E-04,
        -0.57828907E-04, 0.31241111E-03, 0.11465941E-03,-0.84268110E-03,
        -0.24805081E-03, 0.35612486E-03, 0.34300427E-03,-0.11311197E-02,
        -0.11530394E-02,-0.68422355E-03, 0.53711788E-03, 0.40650283E-03,
        -0.81386487E-03, 0.15337228E-03, 0.10189944E-02, 0.15760484E-02,
         0.23024180E-03,-0.29600927E-03,-0.78463921E-03, 0.55089691E-04,
        -0.20737164E-02, 0.16417642E-03,-0.10354526E-04,-0.13212217E-02,
         0.16295862E-02, 0.74129462E-05, 0.97348184E-05,-0.21318619E-04,
         0.45684792E-04, 0.21770808E-04, 0.28989998E-04, 0.52058876E-04,
         0.32937700E-04, 0.33946719E-04, 0.36167316E-04, 0.36167650E-03,
        -0.13962635E-03, 0.28047143E-03, 0.10788369E-03,-0.14451928E-03,
         0.55353481E-04, 0.12990993E-02, 0.69981725E-04,-0.59432356E-03,
        -0.14517565E-03, 0.43330266E-03,-0.53007800E-04, 0.46975107E-04,
        -0.51685626E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x21            
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 35]                *x51
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]        *x31    *x52
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 44]        *x31    *x53
        +coeff[ 45]    *x21*x32*x41*x51
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]    *x22            
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]        *x32*x41*x51
        +coeff[ 50]*x11*x21*x31    *x51
        +coeff[ 51]    *x24    *x41    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 53]        *x31*x42*x52
        +coeff[ 54]    *x22*x31    *x52
        +coeff[ 55]        *x31*x44    
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]    *x21*x31*x42*x51
        +coeff[ 60]    *x22*x33        
        +coeff[ 61]    *x23*x31    *x51
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x21*x31    *x53
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]*x11*x21    *x45    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]*x11    *x31*x42*x52
        +coeff[ 70]*x11*x21*x34*x41    
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 71]    *x24*x31*x42*x51
        +coeff[ 72]    *x22*x32*x45    
        +coeff[ 73]        *x31*x41    
        +coeff[ 74]    *x21        *x51
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]            *x43*x51
        +coeff[ 77]*x11        *x42*x51
        +coeff[ 78]            *x42*x52
        +coeff[ 79]*x11*x22*x31        
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 80]        *x33    *x51
        +coeff[ 81]*x11*x21    *x41*x51
        +coeff[ 82]        *x31*x41*x52
        +coeff[ 83]*x11*x21    *x43    
        +coeff[ 84]        *x34*x41    
        +coeff[ 85]*x11*x21*x32*x41    
        +coeff[ 86]    *x21*x33    *x51
        +coeff[ 87]*x11*x22    *x41*x51
        +coeff[ 88]*x11    *x32*x41*x51
    ;
    v_y_e_q3ex_2_1100                              =v_y_e_q3ex_2_1100                              
        +coeff[ 89]    *x23*x31*x42    
        +coeff[ 90]        *x33    *x52
        +coeff[ 91]    *x22    *x43*x51
        +coeff[ 92]    *x21*x34*x41    
        +coeff[ 93]    *x23*x33        
        +coeff[ 94]*x11*x21*x31    *x52
        +coeff[ 95]*x13        *x41*x51
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_1100                              ;
}
float p_e_q3ex_2_1100                              (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.2446832E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.20982661E-03, 0.16924048E-01,-0.35005346E-01, 0.17075991E-01,
        -0.82355021E-02,-0.10509941E-01, 0.78757666E-02,-0.61908667E-03,
        -0.95220537E-04, 0.13091233E-03,-0.43679168E-06, 0.52745636E-02,
         0.15746946E-02, 0.40946254E-02,-0.62872493E-03, 0.30370029E-02,
         0.15812659E-02, 0.11625784E-02, 0.12203408E-04,-0.18457648E-03,
         0.42402555E-03, 0.16538267E-02, 0.60034345E-03, 0.38175302E-03,
        -0.12090714E-02,-0.38162948E-03,-0.15215416E-02,-0.77009987E-03,
        -0.33771506E-03, 0.48106463E-05, 0.16457445E-03,-0.19416556E-03,
        -0.79926470E-03, 0.95347474E-04, 0.11544145E-02, 0.94862579E-03,
        -0.26353309E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_1100                              =v_p_e_q3ex_2_1100                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_1100                              =v_p_e_q3ex_2_1100                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_1100                              =v_p_e_q3ex_2_1100                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_2_1100                              =v_p_e_q3ex_2_1100                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        ;

    return v_p_e_q3ex_2_1100                              ;
}
float l_e_q3ex_2_1100                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1571763E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.15643938E-01,-0.31200135E+00,-0.25242150E-01, 0.12247193E-01,
        -0.33573039E-01,-0.10683443E-01,-0.58084107E-02,-0.39095525E-02,
         0.95545156E-02, 0.63599688E-02,-0.26287648E-02,-0.46281861E-02,
         0.23813385E-02, 0.38741862E-02, 0.91223260E-02,-0.10592324E-02,
        -0.17642396E-02, 0.17853215E-02, 0.10328784E-02, 0.43302686E-02,
         0.69303432E-03,-0.19577502E-03, 0.41541920E-03, 0.92828937E-03,
         0.30946004E-03, 0.91785379E-03,-0.55564870E-03,-0.11841647E-02,
        -0.89652091E-03, 0.57786977E-03,-0.11336610E-02, 0.41230355E-03,
        -0.17805896E-02,-0.68920205E-03,-0.68520254E-03,-0.10224236E-02,
         0.93625410E-03,-0.85621316E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_1100                              =v_l_e_q3ex_2_1100                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]        *x32        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]*x11*x21            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_1100                              =v_l_e_q3ex_2_1100                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]        *x32    *x53
        +coeff[ 21]*x12                
        +coeff[ 22]        *x31*x42    
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]                *x53
        +coeff[ 25]    *x24            
    ;
    v_l_e_q3ex_2_1100                              =v_l_e_q3ex_2_1100                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]            *x42*x52
        +coeff[ 29]*x11    *x32    *x51
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]*x11    *x34        
        +coeff[ 32]    *x23*x32    *x51
        +coeff[ 33]        *x34    *x52
        +coeff[ 34]*x11*x24*x31        
    ;
    v_l_e_q3ex_2_1100                              =v_l_e_q3ex_2_1100                              
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]*x11        *x43*x53
        +coeff[ 37]*x12*x22*x33        
        ;

    return v_l_e_q3ex_2_1100                              ;
}
float x_e_q3ex_2_1000                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1429071E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.15167888E-01,-0.75120381E-02, 0.24988085E+00, 0.69734685E-01,
        -0.13333634E-01, 0.26712487E-01, 0.83564436E-02,-0.61956136E-02,
        -0.48511736E-02,-0.11965276E-01,-0.67199743E-02,-0.60275599E-03,
        -0.96064474E-03, 0.14705842E-02,-0.36661052E-02, 0.40678745E-02,
        -0.28852050E-03, 0.11612164E-02,-0.43527054E-03, 0.10138736E-02,
        -0.28304223E-03, 0.75824861E-03, 0.27126987E-05, 0.36551355E-03,
        -0.88159117E-03, 0.59919123E-03, 0.21370405E-02,-0.16555025E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_1000                              =v_x_e_q3ex_2_1000                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3ex_2_1000                              =v_x_e_q3ex_2_1000                              
        +coeff[ 17]                *x53
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_2_1000                              =v_x_e_q3ex_2_1000                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_2_1000                              ;
}
float t_e_q3ex_2_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6853039E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.67400564E-02,-0.27961746E-01, 0.78731380E-01, 0.36532045E-02,
        -0.59024701E-02,-0.21159537E-02,-0.25181952E-02,-0.41369090E-03,
        -0.11161725E-02, 0.25040261E-03,-0.88307529E-03, 0.23970785E-03,
        -0.48199046E-03,-0.19143318E-03,-0.37153464E-03, 0.50015841E-03,
         0.17313934E-02,-0.95101474E-04, 0.52692957E-03, 0.11493745E-03,
         0.23887484E-03, 0.81747252E-03,-0.26783493E-03, 0.38026430E-03,
        -0.73755282E-03,-0.53745112E-03,-0.42827387E-03, 0.21225766E-03,
         0.10296961E-03, 0.10089424E-03, 0.28543139E-03,-0.12437842E-03,
        -0.10779240E-03,-0.11685307E-03, 0.39906241E-03,-0.26775306E-03,
        -0.25246854E-03,-0.25556728E-03, 0.21291440E-03,-0.21573236E-03,
         0.58610510E-03, 0.26412801E-04, 0.19373074E-04,-0.21363066E-04,
        -0.48891834E-04, 0.28816850E-04, 0.82613362E-04,-0.23992502E-03,
         0.97231357E-04, 0.11591618E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_1000                              =v_t_e_q3ex_2_1000                              
        +coeff[  8]            *x42    
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x22            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_1000                              =v_t_e_q3ex_2_1000                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_1000                              =v_t_e_q3ex_2_1000                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x23            
        +coeff[ 30]    *x22*x31*x41    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]            *x42*x52
        +coeff[ 34]    *x23    *x42    
    ;
    v_t_e_q3ex_2_1000                              =v_t_e_q3ex_2_1000                              
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]        *x32*x42*x51
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]    *x23        *x53
        +coeff[ 41]*x11    *x32        
        +coeff[ 42]    *x22    *x41    
        +coeff[ 43]*x11*x21        *x51
    ;
    v_t_e_q3ex_2_1000                              =v_t_e_q3ex_2_1000                              
        +coeff[ 44]    *x22        *x51
        +coeff[ 45]*x11            *x52
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_q3ex_2_1000                              ;
}
float y_e_q3ex_2_1000                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.5535322E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.51203492E-03, 0.77143744E-01,-0.84710635E-01,-0.59170540E-01,
        -0.23801943E-01, 0.47803447E-01, 0.19768549E-01,-0.33084169E-01,
        -0.11250104E-01,-0.88470774E-02, 0.21192366E-02, 0.95647865E-03,
        -0.82109673E-02,-0.49144584E-02, 0.54407087E-02,-0.21173162E-03,
        -0.50999257E-02, 0.18456680E-02,-0.20335985E-02,-0.14353839E-02,
        -0.38036264E-02,-0.25889603E-02,-0.98252203E-03, 0.41363225E-03,
         0.11032361E-02, 0.37110487E-02, 0.10712005E-03, 0.15519847E-02,
         0.74788381E-03, 0.14515860E-02, 0.70141017E-03,-0.36907049E-02,
        -0.59957239E-02,-0.44934372E-02, 0.20308322E-02,-0.96739706E-04,
        -0.47682581E-03, 0.28804023E-03,-0.23340780E-03,-0.13807131E-02,
        -0.59164013E-03,-0.33909321E-03,-0.62827580E-03, 0.83233282E-03,
        -0.11706739E-03, 0.27950216E-04,-0.22106527E-04, 0.22052298E-03,
         0.12432941E-03,-0.87584980E-03,-0.23644538E-03, 0.28201932E-03,
         0.28730987E-03,-0.50303568E-02,-0.44054892E-02, 0.19821460E-04,
        -0.53857842E-04,-0.80297736E-03, 0.45360171E-03, 0.57507778E-03,
         0.30474158E-03, 0.17046032E-02, 0.17997950E-02,-0.17424439E-02,
         0.21758939E-03,-0.55948412E-03, 0.29492076E-03,-0.22531622E-03,
        -0.21507179E-02,-0.39399837E-03, 0.46771063E-03,-0.23829348E-03,
         0.20900180E-02, 0.92412898E-04,-0.61813224E-03,-0.58133970E-03,
         0.21514626E-04, 0.24586632E-05, 0.54703731E-04,-0.18639013E-04,
        -0.38445476E-04, 0.16954662E-04,-0.59128786E-03, 0.20346779E-04,
         0.31127933E-04,-0.59648376E-03,-0.22616764E-03, 0.11173979E-03,
         0.63495524E-03, 0.35464916E-04,-0.71125985E-04, 0.13348954E-03,
         0.11533491E-02,-0.23810012E-03, 0.13892452E-03, 0.43007729E-03,
         0.80589380E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]    *x21*x32*x41*x51
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 44]            *x43*x53
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]        *x31*x42*x52
        +coeff[ 52]    *x22*x31    *x52
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 53]    *x22*x31*x44    
        +coeff[ 54]    *x22*x32*x43    
        +coeff[ 55]    *x22            
        +coeff[ 56]*x12        *x41    
        +coeff[ 57]        *x32*x43    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x22    *x45    
        +coeff[ 64]    *x21*x31    *x53
        +coeff[ 65]    *x24    *x41*x51
        +coeff[ 66]        *x33*x44    
        +coeff[ 67]    *x24    *x43    
        +coeff[ 68]    *x22*x33*x42    
        +coeff[ 69]    *x22*x34*x41    
        +coeff[ 70]    *x21*x33*x42*x51
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 71]        *x32*x43*x52
        +coeff[ 72]    *x23*x31*x44    
        +coeff[ 73]*x11*x24    *x41*x51
        +coeff[ 74]    *x23*x34*x41    
        +coeff[ 75]    *x24*x31*x42*x51
        +coeff[ 76]            *x42    
        +coeff[ 77]        *x31*x41    
        +coeff[ 78]*x11        *x41*x51
        +coeff[ 79]*x12    *x31        
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 80]*x11    *x31*x42    
        +coeff[ 81]    *x21    *x42*x51
        +coeff[ 82]        *x31*x44    
        +coeff[ 83]*x11        *x42*x51
        +coeff[ 84]*x12*x21    *x41    
        +coeff[ 85]        *x33*x42    
        +coeff[ 86]        *x34*x41    
        +coeff[ 87]    *x21*x31*x42*x51
        +coeff[ 88]    *x21    *x45    
    ;
    v_y_e_q3ex_2_1000                              =v_y_e_q3ex_2_1000                              
        +coeff[ 89]*x11    *x31    *x52
        +coeff[ 90]        *x32*x44    
        +coeff[ 91]    *x23*x31    *x51
        +coeff[ 92]    *x21*x32*x43    
        +coeff[ 93]*x11*x22    *x41*x51
        +coeff[ 94]*x11    *x31*x44    
        +coeff[ 95]    *x23*x31*x42    
        +coeff[ 96]        *x33    *x52
        ;

    return v_y_e_q3ex_2_1000                              ;
}
float p_e_q3ex_2_1000                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.1107946E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.94373427E-04, 0.16991314E-01,-0.34876503E-01, 0.16946500E-01,
        -0.82319658E-02,-0.10517438E-01, 0.78614447E-02,-0.54899004E-03,
        -0.75957374E-04, 0.12723949E-03,-0.61099090E-04, 0.52242903E-02,
         0.15826582E-02, 0.40905224E-02,-0.63228724E-03, 0.30175932E-02,
         0.15746304E-02, 0.60630834E-03, 0.11621044E-02, 0.10988742E-03,
        -0.18467128E-03, 0.42696303E-03, 0.15855739E-02, 0.38223877E-03,
        -0.37379481E-03,-0.11807970E-02,-0.18159620E-03,-0.13065104E-02,
        -0.65652281E-03,-0.36180342E-03,-0.66533673E-03, 0.86937638E-04,
         0.11515381E-02, 0.93514303E-03,-0.27689047E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_1000                              =v_p_e_q3ex_2_1000                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_1000                              =v_p_e_q3ex_2_1000                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_q3ex_2_1000                              =v_p_e_q3ex_2_1000                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_1000                              ;
}
float l_e_q3ex_2_1000                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1521959E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.15132034E-01,-0.31217420E+00,-0.25284696E-01, 0.12482937E-01,
        -0.33180743E-01,-0.10550617E-01,-0.60248426E-02,-0.42189728E-02,
         0.22923481E-02, 0.93025817E-02, 0.59724469E-02,-0.28036362E-02,
        -0.48621008E-02, 0.42228331E-02, 0.94147362E-02,-0.92073332E-03,
        -0.21659068E-02, 0.49802735E-02, 0.18769077E-02, 0.58376050E-03,
         0.18145335E-02, 0.37840244E-03, 0.30136577E-03, 0.39098025E-03,
         0.64562057E-03,-0.63371356E-03,-0.10563798E-02,-0.65336778E-03,
         0.44363894E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_1000                              =v_l_e_q3ex_2_1000                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]        *x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_1000                              =v_l_e_q3ex_2_1000                              
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]*x11*x22            
        +coeff[ 20]        *x32*x42*x51
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]                *x53
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x32        
    ;
    v_l_e_q3ex_2_1000                              =v_l_e_q3ex_2_1000                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]*x12*x21*x31    *x51
        +coeff[ 28]*x12            *x53
        ;

    return v_l_e_q3ex_2_1000                              ;
}
float x_e_q3ex_2_900                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1104113E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.12175964E-01, 0.24985078E+00, 0.69859989E-01,-0.13362062E-01,
         0.26729019E-01, 0.82795322E-02,-0.32117699E-04,-0.75683687E-02,
        -0.62186709E-02,-0.48845205E-02,-0.11747530E-01,-0.64353901E-02,
        -0.46051200E-03,-0.98828820E-03,-0.63185864E-04, 0.14332214E-02,
        -0.36093411E-02, 0.43897755E-02,-0.28786919E-03, 0.11584937E-02,
        -0.46293734E-03, 0.97059389E-03, 0.77067531E-03, 0.27695873E-04,
         0.37739417E-03,-0.92132774E-03, 0.72255410E-03, 0.22602959E-02,
        -0.28550113E-02,-0.27830817E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_900                              =v_x_e_q3ex_2_900                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_900                              =v_x_e_q3ex_2_900                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_900                              =v_x_e_q3ex_2_900                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23*x32*x42    
        ;

    return v_x_e_q3ex_2_900                              ;
}
float t_e_q3ex_2_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5998383E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59824949E-02,-0.27978541E-01, 0.78733377E-01, 0.36648253E-02,
        -0.59113461E-02,-0.21406908E-02,-0.24781593E-02,-0.40262518E-03,
        -0.89087215E-03,-0.11338301E-02, 0.24002240E-03, 0.24535996E-03,
        -0.42155787E-03,-0.23159513E-03,-0.34260948E-03, 0.48617111E-03,
         0.17533096E-02,-0.11079664E-03, 0.48362426E-03, 0.80639642E-04,
         0.22041590E-03, 0.85006899E-03,-0.27255688E-03, 0.37465818E-03,
        -0.77107071E-03,-0.53229881E-03,-0.42234507E-03, 0.21547837E-03,
         0.62826130E-03, 0.10352986E-03, 0.13844567E-03, 0.44886194E-04,
         0.26156410E-03,-0.12811992E-03,-0.11532372E-03,-0.76326374E-04,
        -0.25266301E-03, 0.86942877E-04, 0.42652275E-03, 0.85627253E-04,
        -0.31437643E-03,-0.19187428E-03,-0.38184538E-04, 0.10552651E-03,
         0.63785265E-04, 0.77150646E-04,-0.31301368E-03,-0.80742211E-04,
         0.14497631E-03,-0.74029915E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_900                              =v_t_e_q3ex_2_900                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_900                              =v_t_e_q3ex_2_900                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_900                              =v_t_e_q3ex_2_900                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]*x11*x21*x31*x41    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_q3ex_2_900                              =v_t_e_q3ex_2_900                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x21        *x53
        +coeff[ 37]    *x21*x33*x41    
        +coeff[ 38]    *x23    *x42    
        +coeff[ 39]*x11    *x32    *x52
        +coeff[ 40]    *x21*x32    *x52
        +coeff[ 41]        *x32    *x53
        +coeff[ 42]    *x22        *x51
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_q3ex_2_900                              =v_t_e_q3ex_2_900                              
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]        *x31*x41*x52
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        ;

    return v_t_e_q3ex_2_900                              ;
}
float y_e_q3ex_2_900                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1155037E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.52472526E-04, 0.76666348E-01,-0.84990986E-01,-0.58954433E-01,
        -0.23488477E-01, 0.47760226E-01, 0.19735603E-01,-0.32992482E-01,
        -0.11170307E-01,-0.89498404E-02, 0.21010621E-02, 0.94931276E-03,
        -0.78122350E-02, 0.52910470E-02,-0.34420469E-03,-0.97717496E-03,
        -0.49746665E-02, 0.18610033E-02,-0.20305468E-02,-0.14389611E-02,
        -0.37370815E-02,-0.25365772E-02,-0.49865851E-02,-0.96468051E-03,
         0.40465486E-03, 0.21404272E-03, 0.11688004E-02, 0.33939714E-03,
         0.98250539E-03, 0.23459486E-03,-0.44680588E-03,-0.46366453E-02,
        -0.86873826E-02,-0.50281314E-02, 0.21080945E-02, 0.92033159E-04,
        -0.10175211E-03, 0.76099433E-03, 0.37225510E-03,-0.12986670E-02,
        -0.74052729E-03,-0.34871645E-03,-0.51133224E-03, 0.90538064E-03,
         0.36112897E-03, 0.26181298E-04,-0.23183797E-04, 0.79739591E-04,
         0.16605912E-03,-0.12664497E-02, 0.12239860E-03,-0.20722076E-03,
        -0.93228946E-03,-0.27197140E-03, 0.34437890E-03,-0.55201541E-04,
         0.59381828E-05,-0.79456589E-03, 0.42458536E-03, 0.58615743E-03,
         0.50662487E-03, 0.29258331E-03, 0.22683535E-02, 0.22930063E-02,
         0.21748374E-03,-0.20553969E-03,-0.43388031E-03,-0.29093174E-02,
         0.16447909E-03, 0.12303863E-02,-0.57519705E-03, 0.26294377E-03,
        -0.49884664E-03, 0.26349691E-02,-0.80737303E-03, 0.14236212E-04,
        -0.25048450E-05, 0.45936147E-04, 0.53595708E-04, 0.17548715E-03,
        -0.13193593E-03, 0.30134675E-04, 0.14623214E-02, 0.32285037E-02,
         0.43159444E-02,-0.84850632E-04, 0.76279852E-04, 0.24846098E-02,
         0.27442148E-02,-0.57403347E-04,-0.44183031E-03, 0.15631549E-03,
         0.66546537E-03,-0.57221868E-03, 0.73087774E-03,-0.66548091E-03,
         0.48119258E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]    *x21    *x43    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 35]        *x33    *x52
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]    *x21*x32*x41*x51
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 44]    *x22*x31    *x52
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]        *x31    *x52
        +coeff[ 48]        *x32*x41*x51
        +coeff[ 49]        *x31*x44    
        +coeff[ 50]*x11*x21*x31    *x51
        +coeff[ 51]    *x21*x31    *x52
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 53]            *x41*x53
        +coeff[ 54]        *x31*x42*x52
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]            *x43*x51
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x21*x31    *x53
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x32*x43    
        +coeff[ 68]*x11    *x31*x42*x52
        +coeff[ 69]    *x23*x31*x44    
        +coeff[ 70]    *x21*x33*x44    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 71]    *x23*x33    *x51
        +coeff[ 72]    *x23*x33*x42    
        +coeff[ 73]    *x22*x32*x45    
        +coeff[ 74]        *x34*x45    
        +coeff[ 75]    *x22            
        +coeff[ 76]*x12    *x31        
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]        *x33    *x51
        +coeff[ 79]    *x21    *x43*x51
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 80]        *x34*x41    
        +coeff[ 81]*x12        *x41*x51
        +coeff[ 82]    *x21    *x45    
        +coeff[ 83]    *x21*x31*x44    
        +coeff[ 84]    *x21*x32*x43    
        +coeff[ 85]*x11*x22    *x41*x51
        +coeff[ 86]*x11    *x32*x41*x51
        +coeff[ 87]    *x23*x31*x42    
        +coeff[ 88]    *x21*x33*x42    
    ;
    v_y_e_q3ex_2_900                              =v_y_e_q3ex_2_900                              
        +coeff[ 89]*x12*x22*x31        
        +coeff[ 90]    *x22    *x43*x51
        +coeff[ 91]        *x32*x43*x51
        +coeff[ 92]    *x21*x34*x41    
        +coeff[ 93]    *x22*x31*x42*x51
        +coeff[ 94]    *x23*x33        
        +coeff[ 95]    *x22    *x45    
        +coeff[ 96]*x11    *x34*x41    
        ;

    return v_y_e_q3ex_2_900                              ;
}
float p_e_q3ex_2_900                              (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1622689E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.66322318E-05, 0.17064717E-01,-0.34732267E-01, 0.17042300E-01,
        -0.82399659E-02,-0.10535150E-01, 0.78646047E-02,-0.62098249E-03,
        -0.94335999E-04, 0.13287667E-03, 0.32306896E-04, 0.52499780E-02,
         0.15823973E-02, 0.41013109E-02,-0.62692695E-03, 0.30195937E-02,
         0.15707032E-02, 0.11682935E-02, 0.37381626E-04,-0.18348287E-03,
         0.43071786E-03, 0.16346393E-02, 0.60424104E-03, 0.37964061E-03,
        -0.12177273E-02,-0.37503353E-03,-0.14911692E-02,-0.76511357E-03,
        -0.36074792E-03, 0.10806992E-04, 0.10274037E-03,-0.19750945E-03,
        -0.72966114E-03, 0.91378846E-04, 0.11382475E-02, 0.96710405E-03,
        -0.26268908E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_900                              =v_p_e_q3ex_2_900                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_900                              =v_p_e_q3ex_2_900                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_900                              =v_p_e_q3ex_2_900                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_2_900                              =v_p_e_q3ex_2_900                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        ;

    return v_p_e_q3ex_2_900                              ;
}
float l_e_q3ex_2_900                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1649501E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.16531279E-01,-0.31214067E+00,-0.25343334E-01, 0.12357939E-01,
        -0.33091057E-01,-0.10813577E-01,-0.59561729E-02,-0.41368804E-02,
         0.23403023E-02, 0.93998928E-02, 0.94884578E-02,-0.28604562E-02,
        -0.47751488E-02, 0.35751290E-02, 0.66271359E-02,-0.95560250E-03,
        -0.21687630E-02,-0.60167862E-03, 0.17073221E-02, 0.63412904E-03,
         0.72804152E-03, 0.58385073E-02, 0.87607733E-03,-0.22156283E-03,
         0.48551257E-03,-0.89270226E-03,-0.69086207E-03, 0.69761078E-03,
         0.13761995E-02, 0.44566379E-02, 0.35321398E-03,-0.11038177E-02,
         0.10157247E-02,-0.15505744E-02,-0.18961261E-02, 0.39128228E-02,
         0.18729602E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_900                              =v_l_e_q3ex_2_900                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]        *x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_900                              =v_l_e_q3ex_2_900                              
        +coeff[ 17]    *x23            
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x34    *x51
        +coeff[ 23]        *x31    *x51
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_q3ex_2_900                              =v_l_e_q3ex_2_900                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]*x12    *x31*x41    
        +coeff[ 28]    *x23*x32        
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]*x11    *x34        
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]    *x21*x31*x42*x52
        +coeff[ 34]        *x32*x42*x52
    ;
    v_l_e_q3ex_2_900                              =v_l_e_q3ex_2_900                              
        +coeff[ 35]    *x23*x32*x42    
        +coeff[ 36]*x11*x23    *x41*x52
        ;

    return v_l_e_q3ex_2_900                              ;
}
float x_e_q3ex_2_800                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1175214E-01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.12973054E-01, 0.24984694E+00, 0.70128001E-01,-0.13353827E-01,
         0.27170818E-01, 0.82858289E-02,-0.91253632E-05,-0.75792870E-02,
        -0.61629135E-02,-0.48670736E-02,-0.11983314E-01,-0.67532738E-02,
        -0.39324540E-03,-0.97098504E-03,-0.14194284E-03, 0.14538170E-02,
        -0.37748595E-02, 0.42134286E-02,-0.29307639E-03, 0.11356600E-02,
        -0.44752998E-03, 0.81550429E-03, 0.78479643E-03, 0.30691506E-04,
         0.14951620E-03,-0.74583053E-03,-0.12759375E-02, 0.69162116E-03,
         0.21521109E-02,-0.18333886E-02,-0.13088613E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_800                              =v_x_e_q3ex_2_800                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_800                              =v_x_e_q3ex_2_800                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_q3ex_2_800                              =v_x_e_q3ex_2_800                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_q3ex_2_800                              ;
}
float t_e_q3ex_2_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6407385E-02;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64038844E-02,-0.27944053E-01, 0.78716666E-01, 0.36830250E-02,
        -0.59097675E-02,-0.41403685E-03,-0.21855424E-02,-0.24427089E-02,
        -0.89521875E-03,-0.11267896E-02, 0.25308455E-03, 0.22701020E-03,
        -0.41792850E-03,-0.21348908E-03,-0.34310212E-03, 0.49548695E-03,
         0.17133686E-02,-0.10840862E-03, 0.52529364E-03, 0.11596798E-03,
         0.23171167E-03, 0.80971100E-03,-0.25410522E-03, 0.30008322E-03,
        -0.81356138E-03,-0.54319663E-03,-0.42171119E-03, 0.21508599E-03,
         0.66105468E-03, 0.10663032E-03, 0.10355924E-03, 0.27998068E-03,
        -0.12536111E-03,-0.11112935E-03,-0.88335517E-04,-0.26908575E-03,
         0.37343023E-03,-0.44833840E-03,-0.21776516E-03,-0.32751274E-03,
        -0.20695000E-03, 0.10927023E-02, 0.60693733E-03,-0.15570187E-04,
         0.39204067E-04,-0.50235358E-04, 0.23615381E-04, 0.91817921E-04,
         0.49124530E-04, 0.69850474E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_800                              =v_t_e_q3ex_2_800                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_800                              =v_t_e_q3ex_2_800                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_800                              =v_t_e_q3ex_2_800                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_800                              =v_t_e_q3ex_2_800                              
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]        *x32*x42*x51
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]    *x22*x32*x42    
        +coeff[ 42]    *x22*x31*x43    
        +coeff[ 43]*x12                
    ;
    v_t_e_q3ex_2_800                              =v_t_e_q3ex_2_800                              
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]*x11            *x52
        +coeff[ 47]        *x33*x41    
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]        *x31*x41*x52
        ;

    return v_t_e_q3ex_2_800                              ;
}
float y_e_q3ex_2_800                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4695880E-03;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.47292290E-03, 0.76077640E-01,-0.85364141E-01,-0.59002109E-01,
        -0.23414375E-01, 0.47771864E-01, 0.19772356E-01,-0.32868519E-01,
        -0.11122923E-01,-0.88616088E-02, 0.20967347E-02,-0.78451382E-02,
         0.53664390E-02,-0.50851877E-03,-0.17170887E-02,-0.27206712E-03,
         0.94689231E-03,-0.47355490E-02, 0.18500552E-02,-0.20055932E-02,
        -0.14544766E-02,-0.36023757E-02,-0.25810613E-02,-0.48064655E-02,
        -0.96406846E-03, 0.41401599E-03, 0.37887492E-03, 0.81322459E-03,
         0.10613911E-03, 0.32448961E-03, 0.90758444E-03, 0.19574029E-03,
        -0.53145345E-02,-0.86313514E-02,-0.54056966E-02,-0.13417379E-02,
         0.20633193E-02,-0.99261866E-04, 0.74405008E-03,-0.44533497E-03,
         0.40202824E-03,-0.89036953E-03,-0.72598463E-03,-0.34676126E-03,
        -0.57737995E-03, 0.87987870E-03, 0.28521760E-03,-0.12666786E-02,
        -0.75761188E-03, 0.11560570E-03,-0.24041651E-03,-0.26608005E-03,
         0.31008612E-03, 0.16241270E-03, 0.13960015E-02,-0.58252052E-04,
         0.50093723E-03, 0.64577168E-03, 0.36605739E-03, 0.18147067E-02,
         0.31099636E-02, 0.21954642E-02, 0.77545911E-03, 0.20418716E-03,
        -0.43099042E-03, 0.49322254E-04,-0.65839372E-03,-0.14141230E-03,
         0.66425989E-03,-0.90136011E-04,-0.98408805E-03, 0.23540544E-03,
        -0.15181722E-02,-0.64773449E-05, 0.51768097E-05, 0.12930928E-04,
        -0.19335208E-04, 0.26680013E-04, 0.14411852E-02, 0.35519650E-04,
         0.18855947E-03, 0.34855441E-02, 0.23553142E-03, 0.13098474E-03,
         0.38673929E-02,-0.18174338E-03, 0.53059292E-04, 0.26650738E-02,
         0.79285513E-04, 0.73079305E-03,-0.90780712E-04, 0.19597677E-03,
         0.37724254E-03,-0.56657100E-04,-0.22234840E-03, 0.51065905E-04,
        -0.52074797E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]        *x34*x41    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 35]    *x24*x31        
        +coeff[ 36]    *x23    *x41*x51
        +coeff[ 37]*x11    *x31    *x51
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x24    *x41    
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]        *x31    *x53
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 44]    *x21*x32*x41*x51
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]        *x31*x44    
        +coeff[ 48]        *x33*x42    
        +coeff[ 49]*x11*x21*x31    *x51
        +coeff[ 50]    *x21*x31    *x52
        +coeff[ 51]            *x41*x53
        +coeff[ 52]    *x22*x31    *x52
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 53]        *x31*x44*x52
        +coeff[ 54]    *x23*x32*x43    
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11*x21    *x43    
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 62]    *x23*x33        
        +coeff[ 63]    *x21*x31    *x53
        +coeff[ 64]    *x24    *x41*x51
        +coeff[ 65]    *x22*x33    *x51
        +coeff[ 66]    *x22*x32*x43    
        +coeff[ 67]*x11        *x45*x51
        +coeff[ 68]    *x21*x33*x42*x51
        +coeff[ 69]*x11*x22    *x43*x51
        +coeff[ 70]    *x24    *x43*x51
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 71]    *x21*x31*x44*x52
        +coeff[ 72]    *x24*x31*x42*x51
        +coeff[ 73]    *x21            
        +coeff[ 74]                *x51
        +coeff[ 75]*x11        *x41*x51
        +coeff[ 76]*x12    *x31        
        +coeff[ 77]*x11*x21    *x41*x51
        +coeff[ 78]    *x21    *x45    
        +coeff[ 79]*x11    *x31    *x52
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 80]*x11        *x43*x51
        +coeff[ 81]    *x21*x31*x44    
        +coeff[ 82]        *x31*x42*x52
        +coeff[ 83]    *x23*x31    *x51
        +coeff[ 84]    *x21*x32*x43    
        +coeff[ 85]*x11*x22    *x41*x51
        +coeff[ 86]*x11    *x32*x41*x51
        +coeff[ 87]    *x21*x33*x42    
        +coeff[ 88]        *x33    *x52
    ;
    v_y_e_q3ex_2_800                              =v_y_e_q3ex_2_800                              
        +coeff[ 89]    *x21*x34*x41    
        +coeff[ 90]*x11*x21    *x41*x52
        +coeff[ 91]*x11*x22*x31*x42    
        +coeff[ 92]        *x32*x45    
        +coeff[ 93]*x11*x21*x31    *x52
        +coeff[ 94]    *x21    *x43*x52
        +coeff[ 95]*x11    *x34*x41    
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_800                              ;
}
float p_e_q3ex_2_800                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2256724E-03;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.22452127E-03, 0.17175743E-01,-0.34573320E-01, 0.16908338E-01,
        -0.82367295E-02,-0.10545259E-01, 0.78601688E-02,-0.55818399E-03,
        -0.93785740E-04, 0.16711264E-03, 0.24817907E-05, 0.52036815E-02,
         0.15780544E-02, 0.40567471E-02,-0.63068315E-03, 0.30295388E-02,
         0.15793005E-02, 0.11721054E-02, 0.90647161E-04,-0.18667879E-03,
         0.43066780E-03, 0.16040043E-02, 0.59654820E-03, 0.38454836E-03,
        -0.37519599E-03,-0.11554222E-02,-0.18888857E-03,-0.13079882E-02,
        -0.66258450E-03,-0.38536088E-03,-0.63919480E-03, 0.87573600E-04,
         0.11637200E-02, 0.94350585E-03,-0.29261663E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_800                              =v_p_e_q3ex_2_800                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_800                              =v_p_e_q3ex_2_800                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_q3ex_2_800                              =v_p_e_q3ex_2_800                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_800                              ;
}
float l_e_q3ex_2_800                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1757193E-01;
    float xmin[10]={
        -0.39991E-02,-0.60002E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.17500419E-01,-0.31232151E+00,-0.25461720E-01, 0.12400416E-01,
        -0.33942882E-01,-0.10752442E-01,-0.60321684E-02,-0.42245067E-02,
         0.23080835E-02, 0.99550597E-02,-0.27663235E-02,-0.48590279E-02,
         0.94695957E-02,-0.18721250E-03,-0.84230257E-03, 0.41767065E-02,
        -0.20745937E-02, 0.18925084E-02, 0.77037886E-03, 0.56459336E-03,
         0.65465999E-03, 0.54933917E-03, 0.52397471E-03, 0.11718010E-02,
         0.13781990E-02, 0.84656297E-03,-0.59219875E-03,-0.10711275E-02,
         0.44976934E-02, 0.33963223E-02,-0.13383342E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_800                              =v_l_e_q3ex_2_800                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]        *x32        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_800                              =v_l_e_q3ex_2_800                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x22*x32    *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]                *x53
        +coeff[ 23]    *x24            
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_q3ex_2_800                              =v_l_e_q3ex_2_800                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x23*x32    *x51
        ;

    return v_l_e_q3ex_2_800                              ;
}
float x_e_q3ex_2_700                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1176531E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.12743004E-01, 0.24982876E+00, 0.70028305E-01,-0.13335189E-01,
         0.26752625E-01, 0.82845790E-02,-0.16417691E-04,-0.74886857E-02,
        -0.61801421E-02,-0.48882146E-02,-0.11148264E-01,-0.63817147E-02,
        -0.65690605E-03,-0.96027955E-03, 0.14760061E-02,-0.37009793E-02,
         0.43023052E-02,-0.28690643E-03, 0.11324339E-02,-0.43439036E-03,
         0.10526894E-02,-0.31359185E-03, 0.74741355E-03, 0.55730136E-04,
         0.61228889E-03,-0.88581932E-03, 0.67677541E-03, 0.21116720E-02,
        -0.25214434E-02,-0.15350406E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_700                              =v_x_e_q3ex_2_700                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_700                              =v_x_e_q3ex_2_700                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_700                              =v_x_e_q3ex_2_700                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        ;

    return v_x_e_q3ex_2_700                              ;
}
float t_e_q3ex_2_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6211349E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.61333929E-02,-0.27977340E-01, 0.78711055E-01, 0.35183311E-02,
        -0.59336219E-02,-0.40700083E-03,-0.22150441E-02,-0.24134505E-02,
        -0.91226865E-03,-0.11561349E-02, 0.24508548E-03, 0.26017983E-03,
        -0.42621436E-03,-0.22423048E-03,-0.34359959E-03, 0.49689785E-03,
         0.17354315E-02,-0.10691588E-03, 0.37642600E-03, 0.13088764E-03,
         0.23353135E-03, 0.80502540E-03,-0.26060382E-03, 0.31385256E-03,
        -0.52987674E-03,-0.53071952E-03,-0.44198072E-03, 0.24865931E-03,
        -0.30549226E-03,-0.20081565E-04, 0.88676243E-04, 0.12360880E-03,
         0.22390408E-03,-0.11288485E-03,-0.10614206E-03, 0.13692450E-03,
         0.36842300E-03,-0.36126323E-03,-0.24350001E-03, 0.22366564E-03,
        -0.19173765E-03, 0.23786690E-03, 0.95425016E-03, 0.68736175E-03,
         0.23064685E-03, 0.24962570E-04,-0.60823317E-04, 0.27954589E-04,
         0.10622774E-03, 0.56862220E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]*x11                
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_700                              =v_t_e_q3ex_2_700                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_700                              =v_t_e_q3ex_2_700                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_700                              =v_t_e_q3ex_2_700                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x21*x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_q3ex_2_700                              =v_t_e_q3ex_2_700                              
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]        *x32*x42*x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]        *x31*x41*x53
        +coeff[ 42]    *x22*x32*x42    
        +coeff[ 43]    *x22*x31*x43    
    ;
    v_t_e_q3ex_2_700                              =v_t_e_q3ex_2_700                              
        +coeff[ 44]    *x23        *x53
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]    *x22        *x51
        +coeff[ 47]*x11            *x52
        +coeff[ 48]        *x33*x41    
        +coeff[ 49]*x11*x22        *x51
        ;

    return v_t_e_q3ex_2_700                              ;
}
float y_e_q3ex_2_700                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2651895E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.27585478E-03, 0.75323641E-01,-0.85849106E-01,-0.59045803E-01,
        -0.23649013E-01, 0.47754891E-01, 0.19742129E-01,-0.32897327E-01,
        -0.11049054E-01,-0.88920221E-02, 0.21032854E-02,-0.79243286E-02,
         0.53596245E-02,-0.31801156E-03,-0.11571484E-02,-0.21835155E-03,
         0.94221649E-03,-0.49232133E-02, 0.18643126E-02,-0.20374304E-02,
        -0.13755143E-02,-0.34295667E-02,-0.23194489E-02,-0.49332874E-02,
        -0.94324665E-03, 0.41288379E-03, 0.64176187E-03, 0.28415425E-02,
         0.11146429E-03, 0.90514339E-03, 0.12887515E-02, 0.56747266E-03,
        -0.46596597E-02,-0.74971141E-02,-0.49524433E-02, 0.20581542E-02,
         0.92758943E-03,-0.43886475E-03, 0.37963653E-03,-0.22114353E-03,
        -0.14631454E-02,-0.74610760E-03,-0.34390282E-03,-0.56936027E-03,
         0.74224989E-03,-0.17647182E-04, 0.15469303E-04,-0.96917087E-04,
         0.35679306E-04, 0.28837362E-03,-0.98498515E-03,-0.10577388E-02,
         0.11625632E-03,-0.10181462E-02,-0.26967778E-03, 0.37024569E-03,
         0.41542233E-04, 0.24412280E-03,-0.39830687E-04, 0.38928931E-03,
         0.54639188E-03, 0.29712863E-03, 0.18449212E-03, 0.17111109E-02,
         0.15309122E-02,-0.13886469E-02,-0.28635451E-03,-0.72839687E-03,
        -0.30831217E-02,-0.23634457E-02, 0.30553999E-03, 0.41434949E-03,
         0.39385431E-03, 0.36451649E-02, 0.46578194E-02, 0.34960371E-02,
         0.37296786E-03,-0.10959508E-04, 0.52400766E-04, 0.13032622E-02,
         0.32460808E-04,-0.15238492E-03, 0.14219473E-02, 0.16271144E-02,
        -0.16273442E-03,-0.38127280E-04, 0.11166441E-03,-0.61422266E-03,
         0.48259090E-03, 0.12663212E-03, 0.18137872E-03,-0.10469219E-02,
        -0.54969662E-03, 0.43062729E-03, 0.35763445E-04,-0.28419652E-03,
         0.16979659E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]        *x34*x41    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]*x11    *x31    *x51
        +coeff[ 48]            *x43*x51
        +coeff[ 49]        *x32*x41*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21*x31    *x51
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 53]    *x24    *x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]        *x31*x42*x52
        +coeff[ 56]    *x22*x31    *x52
        +coeff[ 57]    *x21*x31    *x53
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 62]    *x23*x31    *x51
        +coeff[ 63]    *x23    *x43    
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x22*x31*x42*x51
        +coeff[ 66]    *x21    *x43*x52
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]    *x22*x32*x43    
        +coeff[ 70]    *x21*x31*x44*x51
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 71]    *x21*x33*x42*x51
        +coeff[ 72]    *x22    *x43*x52
        +coeff[ 73]    *x23*x31*x44    
        +coeff[ 74]    *x23*x32*x43    
        +coeff[ 75]    *x23*x33*x42    
        +coeff[ 76]    *x24*x31    *x52
        +coeff[ 77]    *x22            
        +coeff[ 78]*x11        *x41*x51
        +coeff[ 79]    *x21    *x45    
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 80]*x11    *x31    *x52
        +coeff[ 81]            *x43*x52
        +coeff[ 82]    *x21*x31*x44    
        +coeff[ 83]    *x21*x32*x43    
        +coeff[ 84]*x11*x22    *x41*x51
        +coeff[ 85]*x12*x22*x31        
        +coeff[ 86]        *x33    *x52
        +coeff[ 87]    *x22    *x43*x51
        +coeff[ 88]    *x21*x34*x41    
    ;
    v_y_e_q3ex_2_700                              =v_y_e_q3ex_2_700                              
        +coeff[ 89]*x11*x22*x31*x42    
        +coeff[ 90]    *x23*x33        
        +coeff[ 91]    *x22    *x45    
        +coeff[ 92]    *x22*x32*x41*x51
        +coeff[ 93]        *x33*x44    
        +coeff[ 94]*x11*x24*x31        
        +coeff[ 95]    *x24*x31    *x51
        +coeff[ 96]    *x21    *x45*x51
        ;

    return v_y_e_q3ex_2_700                              ;
}
float p_e_q3ex_2_700                              (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.5597421E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.61268343E-04, 0.17296268E-01,-0.34348190E-01, 0.17021481E-01,
        -0.82627470E-02,-0.10573352E-01, 0.78865727E-02,-0.63086103E-03,
        -0.10227763E-03, 0.14720971E-03, 0.37509479E-04, 0.52362941E-02,
         0.15969970E-02, 0.40846518E-02,-0.62951294E-03, 0.30270044E-02,
         0.15720786E-02, 0.11714746E-02, 0.39836716E-04,-0.18410943E-03,
         0.44002611E-03, 0.16380225E-02, 0.57241868E-03, 0.46126844E-03,
        -0.37236582E-03,-0.12176064E-02,-0.18541657E-03,-0.15056283E-02,
        -0.75013970E-03,-0.37239320E-03,-0.69404277E-03, 0.92095863E-04,
         0.11921033E-02, 0.93723001E-03,-0.28412117E-03,-0.38302343E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_700                              =v_p_e_q3ex_2_700                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_700                              =v_p_e_q3ex_2_700                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_q3ex_2_700                              =v_p_e_q3ex_2_700                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_q3ex_2_700                              =v_p_e_q3ex_2_700                              
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_700                              ;
}
float l_e_q3ex_2_700                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1628193E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.16072458E-01,-0.31201008E+00,-0.25322158E-01, 0.12441633E-01,
        -0.32860015E-01,-0.10928296E-01,-0.57601980E-02,-0.41162441E-02,
         0.40414855E-02,-0.28818052E-02,-0.44680522E-02, 0.21971390E-02,
         0.48386739E-02, 0.32523903E-02,-0.95122552E-03, 0.14952897E-02,
        -0.21299834E-02, 0.20677857E-02, 0.61916903E-03,-0.11043451E-02,
         0.48747909E-03, 0.56357798E-03,-0.88874996E-03,-0.21959540E-03,
         0.78811432E-03, 0.63570631E-02, 0.77958168E-02, 0.13489745E-01,
         0.14187329E-01, 0.56323148E-02,-0.18500076E-02,-0.47270086E-03,
         0.73763812E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_2_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_700                              =v_l_e_q3ex_2_700                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_700                              =v_l_e_q3ex_2_700                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]    *x23            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3ex_2_700                              =v_l_e_q3ex_2_700                              
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21    *x44    
        +coeff[ 30]    *x22*x31*x41*x51
        +coeff[ 31]*x12            *x54
        +coeff[ 32]    *x23*x33*x41    
        ;

    return v_l_e_q3ex_2_700                              ;
}
float x_e_q3ex_2_600                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1343118E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.13645924E-01, 0.24986534E+00, 0.70284262E-01,-0.13314134E-01,
         0.26861474E-01, 0.83225379E-02, 0.16977250E-04,-0.75134858E-02,
        -0.61836564E-02,-0.48481291E-02,-0.11869144E-01,-0.66920291E-02,
        -0.56852232E-03,-0.95446955E-03, 0.15440398E-02,-0.36961089E-02,
         0.41249539E-02,-0.29398073E-03, 0.11556145E-02,-0.46564004E-03,
         0.82610769E-03,-0.19341233E-02,-0.27619681E-03, 0.75477350E-03,
         0.60525188E-04, 0.37535766E-03,-0.12339166E-02, 0.67493063E-03,
         0.21187461E-02,-0.13069024E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_600                              =v_x_e_q3ex_2_600                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_600                              =v_x_e_q3ex_2_600                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x23            
    ;
    v_x_e_q3ex_2_600                              =v_x_e_q3ex_2_600                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x21*x31*x41*x53
        ;

    return v_x_e_q3ex_2_600                              ;
}
float t_e_q3ex_2_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6870272E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.65591442E-02,-0.28000239E-01, 0.78728244E-01, 0.36598113E-02,
        -0.59310193E-02,-0.21038684E-02,-0.23725675E-02,-0.41106151E-03,
        -0.93277701E-03,-0.11128756E-02, 0.26792413E-03, 0.27625976E-03,
        -0.41067580E-03,-0.23922615E-03,-0.34168412E-03, 0.49367687E-03,
         0.16605997E-02,-0.10880460E-03, 0.51068631E-03, 0.10958914E-03,
         0.24891799E-03, 0.88608911E-03,-0.24510993E-03, 0.33440581E-03,
        -0.75452524E-03,-0.54933218E-03,-0.43745514E-03, 0.22262026E-03,
         0.78737787E-04, 0.10032268E-03, 0.88985929E-04, 0.28165194E-03,
        -0.12188777E-03,-0.11141831E-03,-0.24746748E-03,-0.93314644E-04,
         0.30381407E-03,-0.44643204E-03,-0.18506103E-03,-0.32709504E-03,
        -0.19119192E-03, 0.46248661E-03, 0.65220712E-03,-0.12557165E-04,
         0.30597581E-04,-0.17881646E-04,-0.36686080E-04, 0.26680393E-04,
        -0.38299888E-04, 0.48748516E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_600                              =v_t_e_q3ex_2_600                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_600                              =v_t_e_q3ex_2_600                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_600                              =v_t_e_q3ex_2_600                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]            *x42*x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q3ex_2_600                              =v_t_e_q3ex_2_600                              
        +coeff[ 35]    *x23*x31*x41    
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]        *x32*x42*x51
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]    *x22*x32*x42    
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]*x12                
    ;
    v_t_e_q3ex_2_600                              =v_t_e_q3ex_2_600                              
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]*x11*x21        *x51
        +coeff[ 46]    *x22        *x51
        +coeff[ 47]*x11            *x52
        +coeff[ 48]*x11*x21*x32        
        +coeff[ 49]*x11*x22        *x51
        ;

    return v_t_e_q3ex_2_600                              ;
}
float y_e_q3ex_2_600                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1551710E-03;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.15893491E-03, 0.74279368E-01,-0.86475953E-01,-0.58946948E-01,
        -0.23641866E-01, 0.47749653E-01, 0.19769819E-01,-0.33037223E-01,
        -0.11031497E-01,-0.88679576E-02, 0.21012109E-02, 0.94339420E-03,
        -0.81680771E-02, 0.52043367E-02,-0.21770188E-03,-0.85094123E-03,
        -0.16271949E-03,-0.49771708E-02, 0.18389695E-02,-0.19807077E-02,
        -0.13691485E-02,-0.38328147E-02,-0.25344491E-02,-0.50059948E-02,
        -0.94831764E-03, 0.40983158E-03, 0.75239863E-03, 0.34812205E-02,
         0.15235742E-03, 0.73773920E-03, 0.12684874E-02, 0.44726313E-03,
        -0.44829170E-02,-0.71298839E-02,-0.49718963E-02, 0.19607423E-02,
         0.37424051E-03,-0.96885524E-04,-0.44810274E-03, 0.36959382E-03,
        -0.14923436E-02,-0.74251054E-03,-0.34169885E-03,-0.49262075E-03,
         0.91503444E-03, 0.27100739E-03,-0.54360722E-03, 0.10823664E-03,
        -0.20414649E-03,-0.86395734E-03,-0.26712407E-03, 0.30575105E-03,
        -0.10473657E-03, 0.39748318E-03, 0.47015133E-05,-0.44837190E-04,
         0.68798813E-03,-0.73718623E-03, 0.42631183E-03, 0.53780421E-03,
         0.48995635E-03, 0.34518054E-03, 0.21325231E-02, 0.24732323E-02,
        -0.12752583E-02,-0.13272047E-03,-0.19159843E-03,-0.36928928E-02,
        -0.20853421E-02, 0.49812015E-03, 0.34079428E-02, 0.60684019E-03,
        -0.10895681E-02,-0.67142688E-03, 0.29249545E-03,-0.56546944E-03,
         0.43793170E-04,-0.14136112E-04, 0.23358712E-04, 0.67901383E-05,
         0.43836244E-04, 0.85076771E-03, 0.33500492E-04,-0.13637909E-03,
         0.10538055E-03, 0.23686239E-02,-0.13624161E-03,-0.58875943E-04,
         0.94517536E-03, 0.79373021E-04, 0.15408620E-03, 0.55626809E-03,
         0.18736493E-03, 0.43936938E-03,-0.88605646E-03,-0.44511232E-03,
        -0.42996713E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]        *x34*x41    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]        *x31*x44*x51
        +coeff[ 37]*x11    *x31    *x51
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]        *x32*x41*x51
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x21*x31    *x52
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]        *x31*x42*x52
        +coeff[ 52]    *x22*x31    *x52
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 53]    *x23*x31    *x53
        +coeff[ 54]    *x21            
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]        *x31*x42*x51
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x31*x44    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]    *x23    *x43*x51
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 71]    *x24*x31    *x52
        +coeff[ 72]    *x24    *x43*x51
        +coeff[ 73]    *x23*x34*x41    
        +coeff[ 74]    *x22*x33*x42*x51
        +coeff[ 75]    *x24    *x45    
        +coeff[ 76]*x11        *x41*x51
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]    *x21    *x44    
        +coeff[ 79]    *x21    *x43*x51
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 80]*x12        *x41*x51
        +coeff[ 81]    *x21    *x45    
        +coeff[ 82]*x11    *x31    *x52
        +coeff[ 83]            *x43*x52
        +coeff[ 84]    *x21*x33    *x51
        +coeff[ 85]    *x21*x32*x43    
        +coeff[ 86]*x11*x22    *x41*x51
        +coeff[ 87]        *x32*x41*x52
        +coeff[ 88]    *x21*x33*x42    
    ;
    v_y_e_q3ex_2_600                              =v_y_e_q3ex_2_600                              
        +coeff[ 89]        *x33    *x52
        +coeff[ 90]    *x22    *x43*x51
        +coeff[ 91]    *x21*x34*x41    
        +coeff[ 92]*x11*x22*x31*x42    
        +coeff[ 93]    *x23*x33        
        +coeff[ 94]    *x22    *x45    
        +coeff[ 95]        *x32*x45    
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_600                              ;
}
float p_e_q3ex_2_600                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.4799719E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.50391143E-04, 0.17483117E-01,-0.34025762E-01, 0.16977465E-01,
        -0.83051333E-02,-0.10598943E-01, 0.78701237E-02,-0.63348259E-03,
        -0.10202174E-03, 0.14890585E-03, 0.55881850E-04, 0.52138725E-02,
         0.15964637E-02, 0.40804967E-02,-0.62596187E-03, 0.29962177E-02,
         0.15654238E-02, 0.11790148E-02, 0.32593455E-05,-0.18154708E-03,
         0.43129394E-03, 0.16534925E-02, 0.56525832E-03, 0.45927448E-03,
        -0.11904171E-02,-0.37659900E-03,-0.14602530E-02,-0.75764849E-03,
        -0.35099639E-03, 0.19145895E-04,-0.19945660E-03,-0.64961740E-03,
         0.87453343E-04, 0.12390182E-02, 0.99048286E-03,-0.28314636E-03,
        -0.41082234E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_600                              =v_p_e_q3ex_2_600                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_600                              =v_p_e_q3ex_2_600                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_600                              =v_p_e_q3ex_2_600                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_q3ex_2_600                              =v_p_e_q3ex_2_600                              
        +coeff[ 35]            *x41*x53
        +coeff[ 36]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_600                              ;
}
float l_e_q3ex_2_600                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1703346E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.16950242E-01,-0.31309295E+00,-0.25237758E-01, 0.12428674E-01,
        -0.33063952E-01,-0.10566192E-01,-0.60065980E-02,-0.43080486E-02,
         0.23237984E-02, 0.56662629E-02,-0.28368623E-02,-0.47406401E-02,
         0.51663299E-02, 0.21407390E-02,-0.97174861E-03, 0.21774066E-02,
        -0.18457059E-02, 0.14538368E-02, 0.90547663E-03,-0.49855622E-04,
         0.10155045E-02, 0.34835888E-03, 0.52040297E-03,-0.86585031E-03,
        -0.25168536E-03, 0.80046272E-02, 0.59113586E-02, 0.11406682E-01,
         0.11127142E-01, 0.43660309E-02,-0.13836429E-02, 0.86025882E-03,
         0.35463877E-02,-0.17660740E-02, 0.69217030E-02,-0.15024875E-02,
        -0.31905530E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_2_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_600                              =v_l_e_q3ex_2_600                              
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]        *x32        
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_600                              =v_l_e_q3ex_2_600                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]        *x32    *x53
        +coeff[ 19]        *x31        
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x53
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]*x11    *x33        
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3ex_2_600                              =v_l_e_q3ex_2_600                              
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21    *x44    
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]*x12        *x42*x51
        +coeff[ 32]    *x23*x33*x41    
        +coeff[ 33]    *x24    *x42*x51
        +coeff[ 34]    *x21*x33*x41*x52
    ;
    v_l_e_q3ex_2_600                              =v_l_e_q3ex_2_600                              
        +coeff[ 35]        *x33*x42*x52
        +coeff[ 36]    *x21*x31*x41*x54
        ;

    return v_l_e_q3ex_2_600                              ;
}
float x_e_q3ex_2_500                              (float *x,int m){
    int ncoeff= 26;
    float avdat= -0.1346343E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
         0.12015654E-01,-0.75785462E-02, 0.24996927E+00, 0.70639804E-01,
        -0.13356027E-01, 0.26732598E-01, 0.85570747E-02,-0.61768168E-02,
        -0.47980179E-02,-0.11864066E-01,-0.64186831E-02,-0.60334260E-03,
        -0.75560145E-03,-0.10538651E-03, 0.14815747E-02,-0.37748863E-02,
         0.37919090E-02,-0.29495455E-03, 0.11349298E-02,-0.51040173E-03,
         0.10306868E-02,-0.26323195E-02, 0.74319536E-03, 0.60077728E-04,
        -0.83519146E-03, 0.21221647E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_500                              =v_x_e_q3ex_2_500                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_500                              =v_x_e_q3ex_2_500                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22    *x42*x51
        ;

    return v_x_e_q3ex_2_500                              ;
}
float t_e_q3ex_2_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6636517E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.58139851E-02,-0.28015068E-01, 0.78762032E-01, 0.36550257E-02,
        -0.59043868E-02,-0.20911542E-02,-0.23969954E-02,-0.41070420E-03,
        -0.91049587E-03,-0.11307781E-02, 0.25475284E-03, 0.24499223E-03,
        -0.51962183E-03,-0.25689995E-03,-0.34728830E-03, 0.49649901E-03,
         0.16427421E-02,-0.99196914E-04, 0.53366588E-03, 0.85219574E-04,
         0.23928845E-03, 0.85559965E-03,-0.23622370E-03, 0.35554840E-03,
        -0.73095475E-03,-0.56547730E-03,-0.23832136E-03, 0.20553464E-03,
        -0.35466408E-03, 0.71837089E-03, 0.39467974E-04, 0.99863268E-04,
         0.86331544E-04, 0.20915231E-03,-0.11458485E-03,-0.10619445E-03,
        -0.10817784E-03,-0.29855024E-03,-0.98954229E-06, 0.29271175E-03,
        -0.22958448E-03,-0.41709901E-03,-0.11462329E-04, 0.33813576E-04,
        -0.21797978E-04,-0.49780316E-04, 0.22524224E-04, 0.90754416E-04,
         0.11312906E-03, 0.13098364E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_500                              =v_t_e_q3ex_2_500                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_500                              =v_t_e_q3ex_2_500                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_500                              =v_t_e_q3ex_2_500                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]    *x21*x31*x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]*x11*x21    *x42    
    ;
    v_t_e_q3ex_2_500                              =v_t_e_q3ex_2_500                              
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]    *x23*x31*x41    
        +coeff[ 39]    *x23    *x42    
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]    *x23    *x42*x51
        +coeff[ 42]*x12                
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_q3ex_2_500                              =v_t_e_q3ex_2_500                              
        +coeff[ 44]*x11*x21        *x51
        +coeff[ 45]    *x22        *x51
        +coeff[ 46]*x11            *x52
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_q3ex_2_500                              ;
}
float y_e_q3ex_2_500                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2724022E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.29592909E-03, 0.72884336E-01,-0.87326467E-01,-0.58996774E-01,
        -0.23607731E-01, 0.47804683E-01, 0.19743506E-01,-0.33158936E-01,
        -0.11170951E-01,-0.88733258E-02, 0.20901670E-02, 0.94070472E-03,
        -0.84023420E-02,-0.51597171E-02, 0.53565246E-02,-0.13933356E-03,
        -0.51392582E-02, 0.18621900E-02,-0.20298066E-02,-0.13470618E-02,
        -0.39292108E-02,-0.25734380E-02,-0.96879544E-03, 0.39983288E-03,
         0.67962933E-03, 0.24255547E-02, 0.17937053E-03, 0.13535646E-02,
         0.13426854E-02, 0.63547574E-03,-0.56878407E-02,-0.39612055E-02,
         0.20348369E-02,-0.19453634E-02,-0.99418721E-04, 0.72951865E-03,
        -0.43218580E-03,-0.34682886E-02, 0.38057621E-03,-0.22854560E-03,
        -0.13918016E-02,-0.54862374E-03,-0.35951418E-03,-0.37635595E-03,
         0.66168053E-03,-0.46666470E-03, 0.19993272E-03, 0.12426029E-03,
        -0.87322929E-03,-0.25880628E-03, 0.31715821E-03,-0.23506528E-03,
         0.13876050E-04,-0.90582216E-05,-0.62551633E-04,-0.29972839E-03,
        -0.55127928E-03,-0.24909122E-03, 0.36665899E-03, 0.48471673E-03,
         0.51617867E-03, 0.25101993E-03, 0.25547788E-03, 0.18917416E-02,
         0.13804603E-02,-0.68472628E-03, 0.22819998E-03,-0.26014616E-03,
        -0.48279393E-03,-0.59061381E-02,-0.58007017E-02,-0.30217071E-02,
         0.14461983E-03, 0.35646034E-03,-0.88691042E-03, 0.57828025E-03,
        -0.43852109E-03, 0.24568751E-02, 0.29492055E-02, 0.18752319E-02,
         0.97617440E-05,-0.15757194E-04, 0.43198714E-04, 0.44482404E-04,
         0.34963501E-04, 0.11755461E-02,-0.14402735E-03, 0.19369484E-02,
         0.11913470E-03, 0.64180691E-04, 0.19188301E-02,-0.87872868E-04,
         0.80994731E-04, 0.20362274E-03, 0.88625809E-03, 0.53403690E-03,
        -0.22805410E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]*x11    *x31    *x51
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x24    *x43    
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]            *x41*x53
        +coeff[ 50]    *x22*x31    *x52
        +coeff[ 51]        *x32*x43*x52
        +coeff[ 52]    *x21            
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 53]                *x51
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]        *x31*x44    
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 62]        *x31*x42*x52
        +coeff[ 63]    *x23    *x43    
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x22*x31*x42*x51
        +coeff[ 66]    *x21*x31    *x53
        +coeff[ 67]    *x21    *x43*x52
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]*x11    *x31*x42*x52
        +coeff[ 73]    *x23    *x43*x51
        +coeff[ 74]    *x22*x34*x41    
        +coeff[ 75]    *x22    *x43*x52
        +coeff[ 76]    *x23*x32*x41*x51
        +coeff[ 77]    *x23*x31*x44    
        +coeff[ 78]    *x23*x32*x43    
        +coeff[ 79]    *x23*x33*x42    
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 80]    *x22            
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]*x11*x22*x31        
        +coeff[ 83]        *x33    *x51
        +coeff[ 84]*x12*x21    *x41    
        +coeff[ 85]    *x21    *x45    
        +coeff[ 86]            *x43*x52
        +coeff[ 87]    *x21*x31*x44    
        +coeff[ 88]    *x23*x31    *x51
    ;
    v_y_e_q3ex_2_500                              =v_y_e_q3ex_2_500                              
        +coeff[ 89]    *x21*x33    *x51
        +coeff[ 90]    *x21*x32*x43    
        +coeff[ 91]*x11*x22    *x41*x51
        +coeff[ 92]*x11    *x32*x41*x51
        +coeff[ 93]        *x31*x44*x51
        +coeff[ 94]    *x23*x31*x42    
        +coeff[ 95]    *x21*x33*x42    
        +coeff[ 96]            *x42*x53
        ;

    return v_y_e_q3ex_2_500                              ;
}
float p_e_q3ex_2_500                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.6381333E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.72956689E-04, 0.17716706E-01,-0.33609249E-01, 0.16969774E-01,
        -0.83264858E-02,-0.10638150E-01, 0.78875544E-02,-0.61069848E-03,
        -0.72188523E-04, 0.12293181E-03, 0.15198674E-04, 0.51629981E-02,
         0.16181639E-02, 0.41200332E-02,-0.62454597E-03, 0.30244573E-02,
         0.15766078E-02, 0.12020366E-02, 0.17566188E-04,-0.18173625E-03,
         0.44406697E-03, 0.16538369E-02, 0.58208290E-03, 0.45624663E-03,
        -0.12173438E-02,-0.37152111E-03,-0.14796543E-02,-0.75365161E-03,
        -0.37914599E-03, 0.15881536E-04, 0.12809969E-03,-0.18813083E-03,
        -0.77349256E-03, 0.85458247E-04, 0.11680230E-02, 0.95407612E-03,
        -0.29270430E-03,-0.36204874E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_500                              =v_p_e_q3ex_2_500                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_500                              =v_p_e_q3ex_2_500                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_500                              =v_p_e_q3ex_2_500                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21*x34*x41    
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_2_500                              =v_p_e_q3ex_2_500                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_500                              ;
}
float l_e_q3ex_2_500                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1572669E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.15603948E-01,-0.31377199E+00,-0.25303090E-01, 0.12308390E-01,
        -0.33260755E-01,-0.10911406E-01,-0.57457350E-02,-0.42001195E-02,
         0.24020683E-02,-0.29872889E-02,-0.46562147E-02, 0.69729397E-02,
         0.91740415E-02, 0.42414793E-03,-0.93000423E-03, 0.37775978E-02,
        -0.22846826E-02, 0.17079517E-02, 0.66357612E-03, 0.65925455E-03,
         0.32012674E-02, 0.10358612E-02, 0.42442812E-03, 0.42480786E-03,
        -0.35941889E-03,-0.38597826E-03, 0.71068718E-02, 0.10703270E-02,
         0.66461661E-02, 0.47102341E-03, 0.68638759E-03, 0.79180277E-03,
        -0.12006504E-02,-0.13779275E-02,-0.11514039E-02,-0.97711803E-03,
         0.33559392E-02, 0.41646855E-02, 0.14215837E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_500                              =v_l_e_q3ex_2_500                              
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_500                              =v_l_e_q3ex_2_500                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]*x11*x22            
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]*x12        *x41    
        +coeff[ 25]            *x43*x51
    ;
    v_l_e_q3ex_2_500                              =v_l_e_q3ex_2_500                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x13    *x32        
        +coeff[ 30]    *x24*x32        
        +coeff[ 31]    *x24    *x42    
        +coeff[ 32]    *x23*x32    *x51
        +coeff[ 33]    *x21*x31*x41*x53
        +coeff[ 34]*x11*x21    *x42*x52
    ;
    v_l_e_q3ex_2_500                              =v_l_e_q3ex_2_500                              
        +coeff[ 35]*x12        *x42*x52
        +coeff[ 36]    *x23    *x44    
        +coeff[ 37]    *x21*x32*x44    
        +coeff[ 38]    *x21*x34*x41*x51
        ;

    return v_l_e_q3ex_2_500                              ;
}
float x_e_q3ex_2_450                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1542179E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.12673098E-01,-0.75686891E-02, 0.24988441E+00, 0.70459157E-01,
        -0.13346078E-01, 0.26821792E-01, 0.82807131E-02,-0.61283140E-02,
        -0.48871553E-02,-0.11567398E-01,-0.55695157E-02,-0.47518691E-03,
        -0.95524732E-03,-0.10175226E-03, 0.15175354E-02,-0.36152327E-02,
         0.40818043E-02,-0.28495819E-03, 0.11537645E-02,-0.48123501E-03,
         0.10058919E-02,-0.43548672E-02, 0.70101366E-03, 0.67732210E-04,
         0.42568607E-03,-0.10332731E-02, 0.81123266E-03, 0.21112454E-02,
        -0.65740482E-02,-0.36241957E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_450                              =v_x_e_q3ex_2_450                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_450                              =v_x_e_q3ex_2_450                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_450                              =v_x_e_q3ex_2_450                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x32*x42    
        +coeff[ 29]    *x21*x33*x43    
        ;

    return v_x_e_q3ex_2_450                              ;
}
float t_e_q3ex_2_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7523194E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62613674E-02,-0.28085006E-01, 0.78748189E-01, 0.36316260E-02,
        -0.59128678E-02,-0.21275692E-02,-0.23244838E-02,-0.40839970E-03,
        -0.94955636E-03,-0.11346531E-02, 0.23641759E-03, 0.28025542E-03,
        -0.46372859E-03,-0.26796563E-03,-0.34200415E-03, 0.49314846E-03,
         0.16810079E-02,-0.10870399E-03, 0.53255941E-03, 0.85324085E-04,
         0.24621395E-03, 0.86206995E-03,-0.23639775E-03, 0.39605054E-03,
        -0.65509556E-03,-0.49989886E-03,-0.21811531E-03, 0.23869878E-03,
         0.22863098E-03,-0.56796143E-03, 0.93511866E-04, 0.12722195E-03,
         0.22044493E-03,-0.78893965E-04,-0.97741606E-04, 0.79169498E-04,
        -0.28402879E-03,-0.21252899E-03, 0.65158476E-03, 0.36542329E-04,
        -0.25844618E-04,-0.41353826E-04, 0.29066216E-04, 0.66326334E-04,
         0.51903775E-04,-0.91401540E-04,-0.26351848E-03,-0.86781392E-05,
         0.22574890E-03,-0.25988446E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_450                              =v_t_e_q3ex_2_450                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_450                              =v_t_e_q3ex_2_450                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_2_450                              =v_t_e_q3ex_2_450                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]    *x21*x31*x41*x52
        +coeff[ 29]    *x23    *x42*x51
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_q3ex_2_450                              =v_t_e_q3ex_2_450                              
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]    *x22        *x51
        +coeff[ 42]*x11            *x52
        +coeff[ 43]*x11*x21*x31*x41    
    ;
    v_t_e_q3ex_2_450                              =v_t_e_q3ex_2_450                              
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]            *x42*x52
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x23    *x42    
        +coeff[ 49]    *x21*x32*x42    
        ;

    return v_t_e_q3ex_2_450                              ;
}
float y_e_q3ex_2_450                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1765180E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.25424309E-03, 0.71810558E-01,-0.87924235E-01,-0.58999967E-01,
        -0.23618607E-01, 0.47782194E-01, 0.19761350E-01,-0.33184778E-01,
        -0.11110652E-01,-0.88208700E-02, 0.20976705E-02, 0.92561264E-03,
        -0.81650363E-02, 0.53381165E-02,-0.32296838E-03,-0.12667532E-02,
        -0.48649791E-02, 0.18186552E-02,-0.19993898E-02,-0.14248944E-02,
        -0.38089384E-02,-0.25974221E-02,-0.49261861E-02,-0.95697865E-03,
         0.39039049E-03, 0.11218856E-02, 0.37939760E-02, 0.14510543E-03,
         0.14465166E-02, 0.12814156E-02, 0.57701161E-03,-0.46616648E-02,
        -0.72575454E-02,-0.50982693E-02, 0.20938150E-02, 0.19223781E-03,
         0.54897951E-04,-0.47551293E-04,-0.12491712E-03,-0.54608745E-03,
         0.46312247E-03,-0.13221322E-02,-0.73894329E-03,-0.36669499E-03,
        -0.59900567E-03, 0.13630323E-02, 0.40182560E-04, 0.25714238E-03,
        -0.66062174E-03, 0.11361390E-03,-0.22420415E-03,-0.67495601E-03,
        -0.29463341E-03, 0.28908250E-03, 0.33009806E-03,-0.59227415E-04,
         0.73222420E-03,-0.65281719E-03, 0.47485673E-03, 0.65351214E-03,
         0.44903660E-04, 0.36107344E-03, 0.17281304E-02, 0.18245348E-02,
         0.24231982E-03,-0.40989081E-03,-0.32174279E-03,-0.33977232E-02,
        -0.18473265E-02, 0.37327834E-03, 0.25817482E-02,-0.63110149E-03,
        -0.53997309E-03,-0.82101912E-03,-0.35661738E-03,-0.46079842E-03,
         0.11481805E-04,-0.21822201E-04, 0.66567874E-04, 0.46131117E-04,
        -0.19744235E-03, 0.24508988E-03, 0.72589924E-03, 0.43593504E-04,
         0.15847002E-03, 0.12324434E-02,-0.12159493E-03, 0.39950413E-04,
         0.40616462E-04, 0.73159696E-04,-0.87398349E-03, 0.28575867E-03,
        -0.71499951E-03, 0.12837075E-03, 0.40749375E-04, 0.42347801E-04,
        -0.46744003E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]    *x21    *x43    
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x52
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 35]        *x31*x44*x51
        +coeff[ 36]    *x21            
        +coeff[ 37]                *x51
        +coeff[ 38]*x11    *x31    *x51
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]        *x31    *x53
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 44]    *x21*x32*x41*x51
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]    *x22            
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]*x11*x21*x31    *x51
        +coeff[ 50]    *x21*x31    *x52
        +coeff[ 51]    *x24    *x41    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 53]        *x31*x42*x52
        +coeff[ 54]    *x22*x31    *x52
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]        *x31*x42*x51
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x21*x31    *x53
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x31*x44    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]    *x21*x33*x42*x51
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 71]    *x24    *x41*x52
        +coeff[ 72]    *x22*x31*x44*x51
        +coeff[ 73]    *x24    *x43*x51
        +coeff[ 74]    *x23*x34*x41    
        +coeff[ 75]    *x24    *x45    
        +coeff[ 76]    *x21        *x51
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11    *x31*x42    
        +coeff[ 79]*x11*x22*x31        
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 80]        *x34*x41    
        +coeff[ 81]    *x21*x31*x42*x51
        +coeff[ 82]    *x21    *x45    
        +coeff[ 83]*x11*x21    *x42*x51
        +coeff[ 84]    *x23*x31    *x51
        +coeff[ 85]    *x21*x32*x43    
        +coeff[ 86]*x11*x22    *x41*x51
        +coeff[ 87]*x11    *x32*x41*x51
        +coeff[ 88]*x11    *x33    *x51
    ;
    v_y_e_q3ex_2_450                              =v_y_e_q3ex_2_450                              
        +coeff[ 89]        *x33    *x52
        +coeff[ 90]    *x22*x31*x42*x51
        +coeff[ 91]    *x23*x33        
        +coeff[ 92]    *x22    *x45    
        +coeff[ 93]*x11*x24    *x41    
        +coeff[ 94]*x11    *x34*x41    
        +coeff[ 95]*x13        *x41*x51
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_450                              ;
}
float p_e_q3ex_2_450                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.2608863E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.58267473E-04, 0.17895272E-01,-0.33295687E-01, 0.16933784E-01,
        -0.83436444E-02,-0.10666321E-01, 0.78870151E-02,-0.59647812E-03,
        -0.87220149E-04, 0.11548478E-03, 0.16770464E-04, 0.51486040E-02,
         0.16215099E-02, 0.41154968E-02,-0.62046031E-03, 0.29993283E-02,
         0.15638636E-02, 0.12084951E-02, 0.59560127E-04,-0.18067622E-03,
         0.43863588E-03, 0.16208818E-02, 0.57272770E-03, 0.46025368E-03,
        -0.37700517E-03,-0.11943040E-02,-0.18493776E-03,-0.14563900E-02,
        -0.75406703E-03,-0.39541782E-03,-0.65097748E-03, 0.91374743E-04,
         0.12049635E-02, 0.98206790E-03,-0.28745897E-03,-0.38817196E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_450                              =v_p_e_q3ex_2_450                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_450                              =v_p_e_q3ex_2_450                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_q3ex_2_450                              =v_p_e_q3ex_2_450                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21    *x43    
        +coeff[ 29]        *x31    *x53
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_q3ex_2_450                              =v_p_e_q3ex_2_450                              
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_450                              ;
}
float l_e_q3ex_2_450                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1741064E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.17194809E-01,-0.31375098E+00,-0.24854073E-01, 0.12331904E-01,
        -0.33215381E-01,-0.10768849E-01,-0.60255444E-02,-0.42688590E-02,
        -0.27585481E-02,-0.52186316E-02, 0.22644727E-02, 0.81508085E-02,
         0.88228993E-02, 0.20928036E-02,-0.87177503E-03, 0.32009450E-02,
        -0.17858078E-02, 0.14127283E-02, 0.87892683E-03, 0.11723757E-02,
        -0.76460792E-03,-0.14277096E-03, 0.15272561E-02,-0.72866725E-03,
         0.74153012E-02, 0.57909773E-02, 0.24334393E-02, 0.16509313E-02,
        -0.12618058E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]                *x52
    ;
    v_l_e_q3ex_2_450                              =v_l_e_q3ex_2_450                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_2_450                              =v_l_e_q3ex_2_450                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]        *x32    *x53
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x22*x32        
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_2_450                              =v_l_e_q3ex_2_450                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x24    *x42    
        +coeff[ 28]    *x22*x34    *x51
        ;

    return v_l_e_q3ex_2_450                              ;
}
float x_e_q3ex_2_449                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1249648E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.12610354E-01, 0.24775206E+00, 0.69611058E-01,-0.13135864E-01,
         0.26629839E-01, 0.82470831E-02,-0.23485018E-04,-0.72777793E-02,
        -0.68145352E-02,-0.47612553E-02,-0.11879859E-01,-0.72706221E-02,
        -0.49722270E-03,-0.11784639E-02,-0.50850613E-04, 0.15457624E-02,
        -0.44206874E-02, 0.42770035E-02,-0.30572995E-03, 0.11053496E-02,
        -0.53374178E-03, 0.10376455E-02,-0.29684973E-03, 0.86356985E-03,
         0.25026595E-04, 0.41748642E-03,-0.11501970E-02, 0.80784364E-03,
         0.21621750E-02,-0.17172315E-02,-0.13747250E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_449                              =v_x_e_q3ex_2_449                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_449                              =v_x_e_q3ex_2_449                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x23            
    ;
    v_x_e_q3ex_2_449                              =v_x_e_q3ex_2_449                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x21*x31*x41*x53
        ;

    return v_x_e_q3ex_2_449                              ;
}
float t_e_q3ex_2_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6665093E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62338705E-02,-0.29497623E-01, 0.77152990E-01, 0.35032928E-02,
        -0.57754321E-02,-0.23251788E-02,-0.24440773E-02,-0.31928439E-03,
        -0.10869575E-02,-0.10539072E-02, 0.25660946E-03, 0.21478196E-03,
        -0.58201916E-03,-0.27657434E-03,-0.32905734E-03, 0.48056184E-03,
         0.17779568E-02,-0.97646182E-04, 0.54735667E-03, 0.71665236E-04,
         0.30110849E-03, 0.85286179E-03,-0.29957719E-03, 0.14094794E-03,
         0.44993460E-03,-0.77688094E-03,-0.60506631E-03,-0.41763685E-03,
         0.27115026E-03,-0.38167409E-03, 0.67718013E-03, 0.96350319E-04,
         0.11792889E-03, 0.58220874E-04, 0.26989158E-03,-0.12146568E-03,
        -0.10421752E-03,-0.25485244E-03, 0.42260319E-03, 0.79228077E-04,
        -0.24916854E-03,-0.11528336E-04,-0.19959965E-04,-0.37087044E-04,
         0.49121991E-04,-0.77651181E-04, 0.21571093E-03, 0.13878860E-03,
        -0.22466856E-03, 0.18251200E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_449                              =v_t_e_q3ex_2_449                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_449                              =v_t_e_q3ex_2_449                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_449                              =v_t_e_q3ex_2_449                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]*x11*x21*x31*x41    
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_q3ex_2_449                              =v_t_e_q3ex_2_449                              
        +coeff[ 35]*x11*x21    *x42    
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]    *x23    *x42    
        +coeff[ 39]*x11    *x32    *x52
        +coeff[ 40]        *x32    *x53
        +coeff[ 41]*x12                
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]    *x22        *x51
    ;
    v_t_e_q3ex_2_449                              =v_t_e_q3ex_2_449                              
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]            *x42*x52
        +coeff[ 46]    *x23*x32        
        +coeff[ 47]    *x23*x31*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_q3ex_2_449                              ;
}
float y_e_q3ex_2_449                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7358158E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.72850997E-03, 0.80204859E-01,-0.95171817E-01,-0.60905717E-01,
        -0.26808156E-01, 0.48168220E-01, 0.22124976E-01,-0.34210365E-01,
        -0.12699792E-01,-0.89714909E-02, 0.21013997E-02, 0.10372847E-02,
        -0.88717379E-02,-0.59703533E-02, 0.55681081E-02,-0.37653974E-03,
        -0.49389126E-02,-0.13331873E-02, 0.19049662E-02,-0.22885713E-02,
        -0.14899689E-02,-0.33438283E-02,-0.25900539E-02, 0.46376957E-03,
         0.33941673E-03, 0.15808332E-02, 0.46051721E-03, 0.11336956E-02,
         0.36577473E-03,-0.89774793E-02,-0.68175350E-02, 0.21297291E-02,
        -0.35027819E-03, 0.15134405E-03,-0.11249154E-03, 0.88679335E-04,
         0.98986854E-03,-0.44555927E-03,-0.52846130E-02, 0.42628820E-03,
        -0.25336104E-03,-0.17385641E-02,-0.38351695E-03,-0.78364281E-03,
         0.90919295E-03, 0.98132790E-04, 0.45882701E-03, 0.13376071E-03,
        -0.97357790E-03,-0.26238066E-03,-0.10778987E-02,-0.90255207E-06,
        -0.28403118E-03,-0.57493115E-04,-0.11165177E-02,-0.17289952E-02,
        -0.92625391E-03,-0.29067966E-03, 0.62475697E-03, 0.54950215E-03,
         0.59065525E-04,-0.10311043E-05, 0.20691247E-02, 0.28414135E-02,
        -0.15080972E-02, 0.95787499E-03, 0.23909159E-03,-0.28490138E-03,
        -0.73320424E-03,-0.11652408E-03,-0.34168502E-03, 0.21905093E-04,
         0.15832800E-02, 0.37210129E-03, 0.44028781E-03, 0.31686528E-03,
         0.57601817E-04,-0.21348234E-04, 0.29435603E-04, 0.37682370E-04,
         0.53427147E-03, 0.13709881E-02,-0.59936043E-04, 0.34336126E-03,
         0.31122714E-02, 0.28627919E-03, 0.50256327E-02,-0.15822100E-03,
         0.22152308E-02, 0.32103471E-02, 0.12107929E-03,-0.71304914E-03,
        -0.19748582E-03, 0.91047696E-03, 0.15863634E-03,-0.84738387E-03,
        -0.14230568E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]*x11    *x31    *x51
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 35]        *x31    *x52
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]            *x43*x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]            *x41*x53
        +coeff[ 50]    *x22*x33        
        +coeff[ 51]    *x22*x31    *x52
        +coeff[ 52]        *x32*x43*x52
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 53]*x12        *x41    
        +coeff[ 54]        *x31*x44    
        +coeff[ 55]        *x32*x43    
        +coeff[ 56]        *x33*x42    
        +coeff[ 57]        *x34*x41    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]    *x21*x31*x42*x51
        +coeff[ 60]*x11    *x31    *x52
        +coeff[ 61]    *x23*x31    *x51
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]    *x21*x31    *x53
        +coeff[ 67]    *x21    *x43*x52
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]*x11*x21    *x45    
        +coeff[ 70]    *x24*x31    *x51
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 71]*x11*x21*x34*x41    
        +coeff[ 72]    *x23*x31*x44    
        +coeff[ 73]    *x23*x33    *x51
        +coeff[ 74]    *x24*x31    *x52
        +coeff[ 75]    *x22*x32*x43*x51
        +coeff[ 76]*x11        *x41*x51
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11*x22*x31        
        +coeff[ 79]        *x33    *x51
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 80]*x11*x21    *x43    
        +coeff[ 81]    *x21    *x45    
        +coeff[ 82]*x11*x23    *x41    
        +coeff[ 83]*x11*x21*x32*x41    
        +coeff[ 84]    *x21*x31*x44    
        +coeff[ 85]        *x31*x42*x52
        +coeff[ 86]    *x21*x32*x43    
        +coeff[ 87]*x11*x22    *x41*x51
        +coeff[ 88]    *x23*x31*x42    
    ;
    v_y_e_q3ex_2_449                              =v_y_e_q3ex_2_449                              
        +coeff[ 89]    *x21*x33*x42    
        +coeff[ 90]        *x33    *x52
        +coeff[ 91]    *x22    *x43*x51
        +coeff[ 92]        *x32*x43*x51
        +coeff[ 93]    *x21*x34*x41    
        +coeff[ 94]*x11*x22*x31*x42    
        +coeff[ 95]    *x22*x32*x41*x51
        +coeff[ 96]    *x22*x31*x44    
        ;

    return v_y_e_q3ex_2_449                              ;
}
float p_e_q3ex_2_449                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.2583481E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.25677646E-03, 0.17924782E-01,-0.34320112E-01, 0.16441328E-01,
        -0.87926174E-02,-0.99829454E-02, 0.77011008E-02,-0.38843494E-03,
         0.95503980E-04, 0.19189660E-03,-0.15834006E-03, 0.54156911E-02,
         0.16971213E-02, 0.44355053E-02,-0.60034252E-03, 0.32045443E-02,
         0.15041487E-02, 0.11243414E-02, 0.27789851E-04,-0.19013439E-03,
         0.57003286E-03, 0.19014858E-02, 0.62347797E-03,-0.10480793E-02,
         0.29840562E-03,-0.35872305E-03,-0.92000392E-03,-0.39847798E-03,
         0.66400121E-05,-0.20128576E-03,-0.40170047E-03, 0.85207692E-04,
         0.12332866E-02, 0.87591395E-03,-0.24371069E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_449                              =v_p_e_q3ex_2_449                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_449                              =v_p_e_q3ex_2_449                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_449                              =v_p_e_q3ex_2_449                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_449                              ;
}
float l_e_q3ex_2_449                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1814326E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.17527856E-01,-0.31602752E+00,-0.25377961E-01, 0.12219779E-01,
        -0.33913594E-01,-0.10870774E-01,-0.40288270E-02,-0.33760166E-02,
        -0.49928860E-02,-0.58528152E-02, 0.22063470E-02, 0.77056056E-02,
         0.53598471E-02, 0.44782022E-02,-0.85772580E-03,-0.17810643E-02,
         0.19431722E-02,-0.19019552E-02, 0.10214250E-02, 0.16108360E-02,
         0.69687545E-03, 0.47482125E-03,-0.31288690E-03, 0.62051322E-02,
         0.69121150E-02, 0.11522797E-01, 0.87087015E-02, 0.32130415E-02,
         0.55498100E-03, 0.77021681E-02,-0.14560912E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_449                              =v_l_e_q3ex_2_449                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_449                              =v_l_e_q3ex_2_449                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]                *x53
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_l_e_q3ex_2_449                              =v_l_e_q3ex_2_449                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x21    *x44    
        +coeff[ 28]            *x44*x51
        +coeff[ 29]    *x23*x33*x41    
        +coeff[ 30]*x11    *x34*x41*x51
        ;

    return v_l_e_q3ex_2_449                              ;
}
float x_e_q3ex_2_400                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1362023E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.13718329E-01,-0.72690649E-02, 0.24759899E+00, 0.69931664E-01,
        -0.13152434E-01, 0.26633512E-01, 0.83149821E-02,-0.67881579E-02,
        -0.47352011E-02,-0.11838752E-01,-0.72218534E-02, 0.52426767E-04,
        -0.11567457E-02,-0.10916766E-04, 0.15920718E-02,-0.46752868E-02,
         0.42030374E-02,-0.30675533E-03, 0.11317286E-02,-0.54382865E-03,
         0.11484143E-02,-0.19662820E-02,-0.33396267E-03, 0.84750465E-03,
         0.24047531E-04, 0.41109993E-03,-0.11311563E-02, 0.74591005E-03,
         0.20837022E-02,-0.14371492E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_400                              =v_x_e_q3ex_2_400                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]        *x32        
        +coeff[ 13]*x12*x21            
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_400                              =v_x_e_q3ex_2_400                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x23            
    ;
    v_x_e_q3ex_2_400                              =v_x_e_q3ex_2_400                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x21*x31*x41*x53
        ;

    return v_x_e_q3ex_2_400                              ;
}
float t_e_q3ex_2_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7546667E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.71183564E-02,-0.29586155E-01, 0.77071287E-01, 0.35172617E-02,
        -0.57651978E-02,-0.23195702E-02,-0.24375063E-02,-0.32945356E-03,
        -0.10955337E-02,-0.10408207E-02, 0.24674044E-03, 0.24310287E-03,
        -0.50958357E-03,-0.26762325E-03,-0.34050443E-03, 0.47271774E-03,
         0.17434867E-02,-0.96932483E-04, 0.53795637E-03, 0.11296642E-03,
         0.28929513E-03, 0.84763078E-03,-0.29538179E-03, 0.11605387E-03,
         0.44098607E-03,-0.74635539E-03,-0.61553321E-03,-0.44754957E-03,
         0.27391955E-03,-0.32560775E-03, 0.10307245E-03, 0.10782408E-03,
         0.24588461E-03,-0.13210387E-03,-0.90091831E-04, 0.43156397E-03,
        -0.22496436E-03, 0.57576923E-03,-0.17064407E-04, 0.44746044E-04,
        -0.47393758E-04, 0.26091386E-04, 0.53107527E-04,-0.66701810E-04,
         0.83240651E-04,-0.24410503E-03,-0.30410910E-03, 0.12779131E-03,
        -0.28891902E-03, 0.20546340E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_400                              =v_t_e_q3ex_2_400                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_400                              =v_t_e_q3ex_2_400                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_400                              =v_t_e_q3ex_2_400                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_400                              =v_t_e_q3ex_2_400                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]        *x32    *x53
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]*x11*x22            
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]    *x22        *x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_q3ex_2_400                              =v_t_e_q3ex_2_400                              
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]        *x33*x41*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q3ex_2_400                              ;
}
float y_e_q3ex_2_400                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9340986E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.83289604E-03, 0.79807349E-01,-0.95585234E-01,-0.61022040E-01,
        -0.26845628E-01, 0.48194528E-01, 0.22119939E-01,-0.34514237E-01,
        -0.12858373E-01,-0.89501794E-02, 0.21060933E-02, 0.10472017E-02,
        -0.95830020E-02,-0.63133747E-02, 0.57098842E-02,-0.22982291E-04,
        -0.52384655E-02,-0.13653507E-02, 0.19466121E-02,-0.22625113E-02,
        -0.14512204E-02,-0.37340231E-02,-0.29019818E-02, 0.46752309E-03,
         0.49926247E-03, 0.16386041E-02, 0.48170472E-03, 0.11557189E-02,
         0.41515837E-03,-0.63049812E-02,-0.50832080E-02, 0.21562260E-02,
        -0.21643210E-02, 0.27768852E-03, 0.38846702E-04,-0.10416464E-03,
         0.54242988E-04, 0.85416966E-03,-0.48587748E-03,-0.36799321E-02,
         0.30681270E-03,-0.26515214E-03,-0.17198008E-02,-0.37605874E-03,
        -0.78473886E-03, 0.94683946E-03,-0.30532268E-04, 0.27252611E-03,
         0.14344329E-03,-0.11016581E-02,-0.24856627E-03,-0.98367326E-03,
         0.38853279E-03, 0.25687077E-04,-0.53472984E-04,-0.63362630E-03,
         0.57410391E-03, 0.50307286E-03,-0.11262650E-03,-0.80411664E-04,
         0.39675529E-03, 0.12748755E-04, 0.17777862E-02, 0.48612370E-02,
         0.15019423E-03, 0.14227359E-03, 0.26866526E-02,-0.71691378E-03,
         0.91425481E-03, 0.20581596E-03, 0.23341500E-03,-0.55578869E-03,
        -0.66889077E-02,-0.55888408E-05,-0.72066113E-02,-0.44367854E-02,
        -0.91863243E-04,-0.13268690E-02,-0.24640916E-04, 0.69500133E-03,
         0.35793605E-03,-0.17269624E-04, 0.59056034E-04, 0.41836157E-03,
         0.12793023E-02, 0.32779633E-04, 0.43113387E-03, 0.34080204E-02,
        -0.10564380E-03,-0.55934837E-04, 0.87292552E-04, 0.23439894E-02,
         0.28930341E-02, 0.10364275E-03,-0.21503808E-03, 0.96975698E-03,
        -0.74492775E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x21            
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31    *x52
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]        *x31    *x53
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 44]    *x21*x32*x41*x51
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]                *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]    *x22*x33        
        +coeff[ 52]    *x22*x31    *x52
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 53]    *x22            
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]        *x32*x43    
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]    *x21*x31*x42*x51
        +coeff[ 58]            *x43*x52
        +coeff[ 59]*x11*x23    *x41    
        +coeff[ 60]        *x31*x42*x52
        +coeff[ 61]    *x23*x31    *x51
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x21*x32*x43    
        +coeff[ 64]*x11    *x32*x41*x51
        +coeff[ 65]        *x31*x44*x51
        +coeff[ 66]    *x23*x32*x41    
        +coeff[ 67]    *x22*x31*x42*x51
        +coeff[ 68]    *x23*x33        
        +coeff[ 69]        *x32*x45    
        +coeff[ 70]    *x21*x31    *x53
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 71]    *x24    *x41*x51
        +coeff[ 72]    *x22*x31*x44    
        +coeff[ 73]*x11*x21    *x45    
        +coeff[ 74]    *x22*x32*x43    
        +coeff[ 75]    *x22*x33*x42    
        +coeff[ 76]*x11*x21*x32*x43    
        +coeff[ 77]    *x22*x34*x41    
        +coeff[ 78]*x11*x21*x34*x41    
        +coeff[ 79]    *x23*x31*x44    
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 80]    *x23*x33    *x51
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]        *x33    *x51
        +coeff[ 83]*x11*x21    *x43    
        +coeff[ 84]    *x21    *x45    
        +coeff[ 85]*x11    *x31    *x52
        +coeff[ 86]*x11*x21*x32*x41    
        +coeff[ 87]    *x21*x31*x44    
        +coeff[ 88]*x11*x22    *x41*x51
    ;
    v_y_e_q3ex_2_400                              =v_y_e_q3ex_2_400                              
        +coeff[ 89]        *x32*x41*x52
        +coeff[ 90]*x11    *x31*x44    
        +coeff[ 91]    *x23*x31*x42    
        +coeff[ 92]    *x21*x33*x42    
        +coeff[ 93]        *x33    *x52
        +coeff[ 94]    *x22    *x43*x51
        +coeff[ 95]    *x21*x34*x41    
        +coeff[ 96]*x11*x21    *x41*x52
        ;

    return v_y_e_q3ex_2_400                              ;
}
float p_e_q3ex_2_400                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.2828875E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.24895469E-03, 0.17986778E-01,-0.34120400E-01, 0.16432256E-01,
        -0.87799625E-02,-0.99444697E-02, 0.45725321E-02, 0.76862550E-02,
        -0.57759846E-03, 0.41757907E-04, 0.12639859E-03, 0.54376959E-02,
         0.17015666E-02,-0.59089519E-03, 0.31854648E-02, 0.14738600E-02,
         0.59014169E-03, 0.11318057E-02,-0.21396771E-04,-0.18921332E-03,
         0.56800980E-03, 0.19235368E-02, 0.37363116E-03,-0.10542505E-02,
        -0.35858172E-03,-0.90501405E-03,-0.40809062E-03, 0.24922396E-04,
        -0.20768652E-03,-0.40936706E-03, 0.85644519E-04, 0.13297769E-02,
         0.95837488E-03,-0.28907816E-03,-0.39995095E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_2_400                              =v_p_e_q3ex_2_400                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_2_400                              =v_p_e_q3ex_2_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21    *x41*x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_2_400                              =v_p_e_q3ex_2_400                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11        *x41*x51
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]            *x41*x53
        +coeff[ 34]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_400                              ;
}
float l_e_q3ex_2_400                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2119937E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.20781871E-01,-0.31770280E+00,-0.25421811E-01, 0.12238489E-01,
        -0.34532126E-01,-0.10705140E-01,-0.41595129E-02,-0.35495237E-02,
        -0.56844298E-02,-0.61439797E-02, 0.23473885E-02, 0.76781739E-02,
         0.98346025E-02, 0.82943158E-03,-0.84046187E-03,-0.40138283E-03,
         0.38780116E-02,-0.21581431E-02, 0.10416554E-02, 0.17274093E-02,
         0.64645050E-03, 0.14527485E-02, 0.28319971E-02, 0.14854278E-02,
         0.86909346E-02, 0.20147897E-02, 0.65927310E-02,-0.58952934E-03,
         0.15925462E-02, 0.11212551E-02, 0.70380373E-02, 0.32558113E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_400                              =v_l_e_q3ex_2_400                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_400                              =v_l_e_q3ex_2_400                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]                *x53
        +coeff[ 21]*x11*x22        *x52
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_2_400                              =v_l_e_q3ex_2_400                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]*x11        *x43*x51
        +coeff[ 28]    *x24*x32        
        +coeff[ 29]    *x22*x32*x42    
        +coeff[ 30]    *x23*x32*x42    
        +coeff[ 31]    *x23    *x44    
        ;

    return v_l_e_q3ex_2_400                              ;
}
float x_e_q3ex_2_350                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1383569E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.14052601E-01, 0.24757130E+00, 0.70300326E-01,-0.13144561E-01,
         0.26544884E-01, 0.82448684E-02,-0.54381758E-05,-0.73301033E-02,
        -0.67924936E-02,-0.47972016E-02,-0.11729221E-01,-0.73087537E-02,
         0.77165052E-04,-0.11809426E-02,-0.36696969E-04, 0.15102035E-02,
        -0.43708980E-02, 0.42950287E-02,-0.29015084E-03, 0.11057913E-02,
        -0.56589098E-03, 0.12462205E-02, 0.83222217E-03,-0.47132959E-04,
         0.36310134E-03, 0.83300855E-03,-0.83990750E-03, 0.23394623E-02,
        -0.12599637E-02,-0.15019984E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_350                              =v_x_e_q3ex_2_350                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x12*x21            
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_350                              =v_x_e_q3ex_2_350                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_2_350                              =v_x_e_q3ex_2_350                              
        +coeff[ 26]    *x21*x32    *x52
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42*x51
        ;

    return v_x_e_q3ex_2_350                              ;
}
float t_e_q3ex_2_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7354086E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.69103315E-02,-0.29659018E-01, 0.77000938E-01, 0.34489844E-02,
        -0.57532080E-02,-0.22840374E-02,-0.24018541E-02,-0.31269432E-03,
        -0.11426866E-02,-0.10675888E-02, 0.24702802E-03, 0.21485564E-03,
        -0.60176692E-03,-0.30868454E-03,-0.32731262E-03, 0.47483889E-03,
         0.17951623E-02,-0.11032511E-03, 0.59932604E-03, 0.54384331E-04,
         0.29617053E-03, 0.93626633E-03,-0.30783875E-03, 0.89648136E-04,
         0.47325491E-03,-0.66748052E-03,-0.59653330E-03,-0.20199212E-03,
         0.26019421E-03,-0.41106276E-03, 0.92009774E-04, 0.11374708E-03,
         0.26363353E-03,-0.11937001E-03,-0.11041239E-03, 0.32775616E-03,
         0.87719505E-04,-0.23322880E-03,-0.43380336E-03, 0.60499104E-03,
        -0.21608603E-04,-0.51247800E-04, 0.46441663E-04,-0.78604629E-04,
         0.12205695E-03, 0.43612916E-04,-0.22962819E-03, 0.23142606E-03,
         0.21924723E-03, 0.16653253E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_350                              =v_t_e_q3ex_2_350                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_350                              =v_t_e_q3ex_2_350                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x22    *x42*x51
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x21*x31*x41    
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_350                              =v_t_e_q3ex_2_350                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x23            
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_350                              =v_t_e_q3ex_2_350                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]    *x22    *x41    
        +coeff[ 41]    *x22        *x51
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_q3ex_2_350                              =v_t_e_q3ex_2_350                              
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x23*x31*x41    
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_q3ex_2_350                              ;
}
float y_e_q3ex_2_350                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.2160718E-03;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.18204191E-03, 0.79154134E-01,-0.96260019E-01,-0.61403394E-01,
        -0.26976962E-01, 0.48160799E-01, 0.22153568E-01,-0.34693204E-01,
        -0.12727613E-01,-0.90212040E-02, 0.20942772E-02, 0.10271898E-02,
        -0.89596370E-02,-0.60244389E-02, 0.60667680E-02,-0.12410221E-03,
        -0.51567541E-02,-0.13043358E-02, 0.18983125E-02,-0.22182649E-02,
        -0.14952372E-02,-0.36416776E-02,-0.30437179E-02, 0.45815937E-03,
         0.13201566E-02,-0.47542303E-03,-0.74202023E-02,-0.60640872E-02,
         0.22953562E-02,-0.18812729E-02, 0.43201374E-03, 0.17607950E-02,
         0.69745933E-02, 0.10432965E-01, 0.64070211E-02, 0.18742649E-02,
        -0.10903416E-03, 0.58280624E-04, 0.83324086E-03, 0.61052520E-03,
        -0.35642902E-02, 0.45371091E-03,-0.26445015E-03,-0.18030043E-02,
        -0.38961909E-03,-0.57390181E-03, 0.40706262E-03, 0.97660359E-03,
        -0.61752147E-03, 0.14808581E-02, 0.33880528E-02, 0.18985366E-02,
         0.26257662E-03, 0.13985252E-03,-0.86611835E-03,-0.24548909E-03,
        -0.11727666E-02, 0.37602970E-03,-0.20818277E-04, 0.17690571E-04,
        -0.13634549E-04,-0.53792468E-04,-0.62900124E-03,-0.11088015E-02,
        -0.94698049E-03, 0.42907521E-03,-0.32967879E-03, 0.60033612E-03,
         0.48543772E-03, 0.34480658E-03, 0.22126488E-03,-0.11490422E-02,
         0.52151259E-03, 0.21351040E-03,-0.33879129E-03,-0.63047453E-03,
        -0.55029909E-02, 0.20892588E-03,-0.52445885E-02,-0.17709356E-02,
        -0.47775987E-03, 0.35469976E-03, 0.26965872E-03,-0.80213259E-03,
        -0.25773597E-04, 0.67799541E-04, 0.43472159E-04, 0.40572562E-03,
         0.40116527E-04,-0.13878739E-03, 0.39779119E-04,-0.53374810E-04,
         0.87976783E-04,-0.23315742E-03,-0.67066219E-04, 0.54619079E-04,
         0.46652392E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]*x11*x22    *x41    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x23    *x41*x51
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x24*x33        
        +coeff[ 31]    *x23    *x45    
        +coeff[ 32]    *x23*x31*x44    
        +coeff[ 33]    *x23*x32*x43    
        +coeff[ 34]    *x23*x33*x42    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 35]    *x23*x34*x41    
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]        *x31    *x52
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 44]        *x31    *x53
        +coeff[ 45]    *x21*x32*x41*x51
        +coeff[ 46]    *x21*x31*x44    
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]    *x21*x31*x42    
        +coeff[ 51]    *x21*x32*x41    
        +coeff[ 52]        *x32*x41*x51
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 53]*x11*x21*x31    *x51
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]    *x22*x33        
        +coeff[ 57]    *x22*x31    *x52
        +coeff[ 58]        *x32*x43*x52
        +coeff[ 59]    *x21            
        +coeff[ 60]                *x51
        +coeff[ 61]*x12        *x41    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 62]        *x31*x44    
        +coeff[ 63]        *x32*x43    
        +coeff[ 64]        *x33*x42    
        +coeff[ 65]*x11*x21    *x43    
        +coeff[ 66]        *x34*x41    
        +coeff[ 67]*x11*x21*x31*x42    
        +coeff[ 68]    *x21*x31*x42*x51
        +coeff[ 69]*x11*x21*x32*x41    
        +coeff[ 70]        *x31*x42*x52
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 71]    *x23*x31*x42    
        +coeff[ 72]    *x23*x33        
        +coeff[ 73]    *x21*x31    *x53
        +coeff[ 74]    *x21    *x43*x52
        +coeff[ 75]    *x24    *x41*x51
        +coeff[ 76]    *x22*x31*x44    
        +coeff[ 77]    *x22*x33    *x51
        +coeff[ 78]    *x22*x32*x43    
        +coeff[ 79]    *x22*x33*x42    
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 80]    *x23*x32*x41*x51
        +coeff[ 81]        *x33*x42*x52
        +coeff[ 82]    *x23*x33    *x51
        +coeff[ 83]    *x24*x31*x42*x51
        +coeff[ 84]*x11*x21    *x42    
        +coeff[ 85]*x11    *x31*x42    
        +coeff[ 86]*x11*x22*x31        
        +coeff[ 87]    *x21    *x45    
        +coeff[ 88]*x11    *x31    *x52
    ;
    v_y_e_q3ex_2_350                              =v_y_e_q3ex_2_350                              
        +coeff[ 89]*x11*x22    *x41*x51
        +coeff[ 90]*x11    *x32*x41*x51
        +coeff[ 91]*x12*x22*x31        
        +coeff[ 92]        *x33    *x52
        +coeff[ 93]    *x22    *x43*x51
        +coeff[ 94]*x11*x21    *x41*x52
        +coeff[ 95]*x11    *x34*x41    
        +coeff[ 96]*x13        *x41*x51
        ;

    return v_y_e_q3ex_2_350                              ;
}
float p_e_q3ex_2_350                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.3574792E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.48369257E-04, 0.18106308E-01,-0.33850178E-01, 0.16407209E-01,
        -0.87700356E-02,-0.99334391E-02, 0.76877917E-02,-0.58215600E-03,
         0.43507818E-04, 0.15647692E-03, 0.72455383E-04, 0.54351403E-02,
         0.16919463E-02, 0.44523138E-02,-0.59429702E-03, 0.31629663E-02,
         0.14723006E-02, 0.62257278E-03, 0.11375513E-02, 0.16908905E-04,
        -0.18484403E-03, 0.56617963E-03, 0.18915477E-02,-0.10508889E-02,
         0.29689402E-03,-0.35000889E-03,-0.96803735E-03,-0.40626273E-03,
         0.26728163E-04,-0.20694132E-03,-0.40223630E-03, 0.91807073E-04,
         0.12911631E-02, 0.93090709E-03,-0.26274094E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_350                              =v_p_e_q3ex_2_350                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_350                              =v_p_e_q3ex_2_350                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_350                              =v_p_e_q3ex_2_350                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_350                              ;
}
float l_e_q3ex_2_350                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.1956321E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.18750802E-01,-0.31774715E+00,-0.25684070E-01, 0.12236216E-01,
        -0.34289204E-01,-0.10302751E-01,-0.41743503E-02,-0.34853865E-02,
        -0.54257461E-02,-0.62203342E-02, 0.22636754E-02, 0.67935754E-02,
         0.48159147E-02, 0.41340939E-02,-0.88129903E-03,-0.18935001E-02,
         0.17134470E-02,-0.12105503E-02, 0.98506780E-03, 0.18835113E-02,
         0.88432460E-03, 0.58190961E-03,-0.22240929E-03, 0.19801417E-02,
         0.14450235E-02,-0.85766043E-03, 0.74248221E-02, 0.74889218E-02,
         0.11450649E-01, 0.10334346E-01, 0.36669793E-02,-0.12860101E-02,
        -0.12961953E-02, 0.55024824E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_350                              =v_l_e_q3ex_2_350                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_350                              =v_l_e_q3ex_2_350                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]                *x53
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x12    *x31        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_q3ex_2_350                              =v_l_e_q3ex_2_350                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]    *x21    *x44    
        +coeff[ 31]    *x22        *x53
        +coeff[ 32]    *x21*x31*x41*x53
        +coeff[ 33]    *x23*x33*x41    
        ;

    return v_l_e_q3ex_2_350                              ;
}
float x_e_q3ex_2_300                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1306323E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.13183312E-01, 0.24733391E+00, 0.70978135E-01,-0.13115869E-01,
         0.27095845E-01, 0.82531208E-02, 0.80249461E-04,-0.72827004E-02,
        -0.67678350E-02,-0.47762222E-02,-0.11746618E-01,-0.76270625E-02,
        -0.71339949E-03,-0.11581394E-02,-0.15266522E-03, 0.15476020E-02,
        -0.43538441E-02, 0.44917041E-02,-0.29375733E-03, 0.11225254E-02,
        -0.52439637E-03, 0.10462127E-02, 0.82882529E-03,-0.13307002E-04,
         0.41737233E-03,-0.72834024E-03,-0.11509564E-02, 0.99054759E-03,
        -0.41107627E-03, 0.23808398E-02,-0.18912035E-02,-0.27232317E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_2_300                              =v_x_e_q3ex_2_300                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_300                              =v_x_e_q3ex_2_300                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x23        *x51
    ;
    v_x_e_q3ex_2_300                              =v_x_e_q3ex_2_300                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x13*x22            
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x21*x31*x41*x53
        +coeff[ 31]    *x23*x31*x41*x52
        ;

    return v_x_e_q3ex_2_300                              ;
}
float t_e_q3ex_2_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7035423E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.66408957E-02,-0.29867772E-01, 0.76840460E-01, 0.34800638E-02,
        -0.57498789E-02,-0.23047344E-02,-0.23647838E-02,-0.31802306E-03,
        -0.11934356E-02,-0.10473540E-02, 0.25997596E-03, 0.22557421E-03,
        -0.59434277E-03,-0.24169021E-03,-0.33032536E-03, 0.49377984E-03,
         0.17795763E-02,-0.10222833E-03, 0.57393441E-03, 0.12474414E-03,
        -0.73868159E-03, 0.30918149E-03,-0.69416797E-03, 0.82241965E-03,
        -0.48713153E-03,-0.28116730E-03, 0.14992060E-03, 0.48149467E-03,
         0.26594294E-03,-0.36745690E-03, 0.69118058E-03, 0.88610999E-04,
         0.96230870E-04, 0.24714516E-03,-0.97831733E-04,-0.25603821E-03,
        -0.32594186E-03,-0.27738634E-03, 0.37488837E-04, 0.22478069E-04,
         0.66832647E-04,-0.72826158E-04, 0.53686137E-04,-0.36823614E-04,
        -0.20170248E-03,-0.81245133E-04, 0.72479124E-04, 0.17436236E-03,
         0.23646302E-03, 0.21906484E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_300                              =v_t_e_q3ex_2_300                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x23            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]                *x53
        +coeff[ 16]    *x22    *x42    
    ;
    v_t_e_q3ex_2_300                              =v_t_e_q3ex_2_300                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21*x31*x41*x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x23    *x42*x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_q3ex_2_300                              =v_t_e_q3ex_2_300                              
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]        *x32    *x52
        +coeff[ 29]    *x21*x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x23            
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_2_300                              =v_t_e_q3ex_2_300                              
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]        *x32*x42*x51
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11            *x52
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]*x11*x21    *x42    
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]*x11    *x32    *x51
    ;
    v_t_e_q3ex_2_300                              =v_t_e_q3ex_2_300                              
        +coeff[ 44]    *x21    *x42*x51
        +coeff[ 45]    *x22        *x52
        +coeff[ 46]        *x31*x41*x52
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x23    *x42    
        ;

    return v_t_e_q3ex_2_300                              ;
}
float y_e_q3ex_2_300                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3262554E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.35149136E-03, 0.78564242E-01,-0.97065322E-01,-0.61531313E-01,
        -0.27074456E-01, 0.48218533E-01, 0.22093194E-01,-0.34894899E-01,
        -0.13007772E-01,-0.90244608E-02, 0.20878457E-02, 0.10373747E-02,
        -0.91927554E-02,-0.62945667E-02, 0.57189213E-02,-0.14376383E-03,
        -0.51086890E-02,-0.13481870E-02, 0.20131920E-02,-0.22217133E-02,
        -0.14915284E-02,-0.34960411E-02,-0.27297367E-02, 0.46646764E-03,
         0.78498753E-03, 0.38205977E-02, 0.13377125E-02, 0.10139209E-02,
         0.12766301E-02, 0.55953918E-03,-0.67215017E-02,-0.51811519E-02,
         0.21905678E-02,-0.19049323E-02, 0.65643952E-04,-0.47240753E-03,
        -0.41575255E-02, 0.44158165E-03,-0.28404026E-03,-0.16526922E-02,
        -0.79445308E-03,-0.38485017E-03,-0.67985622E-03, 0.90725202E-03,
        -0.10421885E-03, 0.32817954E-03, 0.13773382E-03,-0.11803075E-02,
        -0.25008403E-03, 0.34347078E-03,-0.10685108E-03,-0.40096769E-03,
        -0.56897482E-03,-0.43418715E-03, 0.47632685E-03, 0.58678328E-03,
         0.20407774E-02,-0.95847143E-04, 0.23887870E-02,-0.13221075E-02,
         0.79806469E-03, 0.22514039E-03,-0.27647437E-03,-0.75394992E-03,
        -0.67298631E-02, 0.32599623E-03,-0.69596977E-02,-0.38381761E-02,
        -0.14151941E-02,-0.27499771E-04, 0.32422883E-02,-0.12806038E-03,
         0.32766766E-03,-0.22640055E-04, 0.39217393E-05, 0.41334988E-04,
        -0.27163593E-04, 0.89061470E-03, 0.27630978E-04, 0.32839630E-03,
         0.29734828E-03, 0.24321324E-02,-0.16401334E-03, 0.46320096E-04,
         0.41476413E-03, 0.10074540E-02, 0.11428044E-03,-0.54026401E-03,
         0.28089253E-03,-0.90720707E-04, 0.25042889E-03,-0.50079619E-03,
         0.54455915E-04, 0.52455107E-04,-0.52753265E-03,-0.27967998E-03,
         0.22943872E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]            *x45    
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]        *x31*x42*x51
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]        *x31    *x52
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]    *x21*x32*x41*x51
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 44]*x11    *x31    *x51
        +coeff[ 45]        *x32*x41*x51
        +coeff[ 46]*x11*x21*x31    *x51
        +coeff[ 47]    *x24    *x41    
        +coeff[ 48]            *x41*x53
        +coeff[ 49]    *x22*x31    *x52
        +coeff[ 50]        *x32*x43*x52
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x32*x43    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21*x31*x42    
        +coeff[ 55]    *x21*x31*x42*x51
        +coeff[ 56]    *x23    *x43    
        +coeff[ 57]*x12*x22    *x41    
        +coeff[ 58]    *x23*x32*x41    
        +coeff[ 59]    *x22*x31*x42*x51
        +coeff[ 60]    *x23*x33        
        +coeff[ 61]    *x21*x31    *x53
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 62]    *x21    *x43*x52
        +coeff[ 63]    *x24    *x41*x51
        +coeff[ 64]    *x22*x31*x44    
        +coeff[ 65]*x11*x21    *x45    
        +coeff[ 66]    *x22*x32*x43    
        +coeff[ 67]    *x22*x33*x42    
        +coeff[ 68]    *x22*x34*x41    
        +coeff[ 69]*x11*x21*x34*x41    
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 71]    *x21*x33*x44    
        +coeff[ 72]    *x23*x33    *x51
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x11    *x31*x42    
        +coeff[ 75]        *x33    *x51
        +coeff[ 76]*x11        *x41*x52
        +coeff[ 77]    *x21    *x45    
        +coeff[ 78]*x11    *x31    *x52
        +coeff[ 79]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 80]        *x31*x42*x52
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]*x11*x22    *x41*x51
        +coeff[ 83]*x11    *x32*x41*x51
        +coeff[ 84]    *x23*x31*x42    
        +coeff[ 85]    *x21*x33*x42    
        +coeff[ 86]        *x33    *x52
        +coeff[ 87]    *x22    *x43*x51
        +coeff[ 88]    *x21*x34*x41    
    ;
    v_y_e_q3ex_2_300                              =v_y_e_q3ex_2_300                              
        +coeff[ 89]*x11*x21    *x41*x52
        +coeff[ 90]*x11*x22*x31*x42    
        +coeff[ 91]        *x32*x45    
        +coeff[ 92]*x11    *x34*x41    
        +coeff[ 93]*x13        *x41*x51
        +coeff[ 94]    *x22*x32*x41*x51
        +coeff[ 95]    *x24*x31    *x51
        +coeff[ 96]    *x21    *x45*x51
        ;

    return v_y_e_q3ex_2_300                              ;
}
float p_e_q3ex_2_300                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1182455E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.12392053E-03, 0.18249348E-01,-0.33588067E-01, 0.16409190E-01,
        -0.87665170E-02,-0.99302819E-02, 0.77535128E-02,-0.58201054E-03,
         0.46835601E-04, 0.16667361E-03, 0.76230834E-04, 0.53963512E-02,
         0.16966884E-02, 0.44723041E-02,-0.58616069E-03, 0.31337989E-02,
         0.14745643E-02, 0.11384768E-02,-0.15621554E-04,-0.18348623E-03,
         0.55393489E-03, 0.19006917E-02, 0.62871707E-03,-0.10370951E-02,
         0.29995965E-03,-0.35059307E-03,-0.95395022E-03,-0.39741542E-03,
         0.94262095E-05,-0.19249674E-03,-0.39445321E-03, 0.78613077E-04,
         0.13505506E-02, 0.91487053E-03,-0.26280773E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_300                              =v_p_e_q3ex_2_300                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_300                              =v_p_e_q3ex_2_300                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_300                              =v_p_e_q3ex_2_300                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_300                              ;
}
float l_e_q3ex_2_300                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1923252E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.18899152E-01,-0.32060000E+00,-0.25382480E-01, 0.12105697E-01,
        -0.34636021E-01,-0.10245874E-01,-0.41880924E-02,-0.35010383E-02,
        -0.47530793E-02,-0.57878909E-02, 0.23178975E-02, 0.62210113E-02,
         0.79364078E-02, 0.32867903E-02,-0.93108596E-03,-0.12640121E-02,
         0.31556569E-02,-0.20351340E-02, 0.20973939E-02, 0.74670586E-03,
         0.10175811E-01, 0.67568510E-02, 0.71022561E-03, 0.30532505E-03,
         0.72171720E-03, 0.12330568E-02, 0.57689189E-02, 0.23759527E-02,
        -0.16558375E-02,-0.18290152E-02,-0.94647746E-03, 0.73680747E-03,
         0.59214318E-02, 0.30745296E-02, 0.86441584E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_300                              =v_l_e_q3ex_2_300                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_300                              =v_l_e_q3ex_2_300                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x34    *x51
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3ex_2_300                              =v_l_e_q3ex_2_300                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]        *x32*x42*x51
        +coeff[ 28]*x11    *x31*x43    
        +coeff[ 29]        *x33*x41*x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]    *x22*x31    *x53
        +coeff[ 32]    *x21*x32*x44    
        +coeff[ 33]    *x21*x33*x41*x52
        +coeff[ 34]*x11    *x34    *x52
        ;

    return v_l_e_q3ex_2_300                              ;
}
float x_e_q3ex_2_250                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1402014E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.14118630E-01,-0.71775727E-02, 0.24713780E+00, 0.71756050E-01,
        -0.13096441E-01, 0.26778582E-01, 0.83304988E-02,-0.67054317E-02,
        -0.47394605E-02,-0.11743218E-01,-0.71265730E-02,-0.66603629E-05,
        -0.11269676E-02, 0.12709069E-04, 0.16521323E-02,-0.44762022E-02,
         0.42287554E-02,-0.28521335E-03, 0.11109798E-02,-0.59353572E-03,
         0.40081484E-03, 0.10910711E-02,-0.31078051E-03, 0.85002847E-03,
         0.98828195E-05, 0.89465122E-03,-0.62976161E-03, 0.21482415E-02,
        -0.19147083E-02,-0.22378210E-02,-0.26389603E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_250                              =v_x_e_q3ex_2_250                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]        *x32        
        +coeff[ 13]*x12*x21            
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_250                              =v_x_e_q3ex_2_250                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_2_250                              =v_x_e_q3ex_2_250                              
        +coeff[ 26]    *x21*x32    *x52
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42*x51
        +coeff[ 30]    *x23*x31*x41*x53
        ;

    return v_x_e_q3ex_2_250                              ;
}
float t_e_q3ex_2_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7261183E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.68722623E-02,-0.30062707E-01, 0.76693535E-01, 0.35133786E-02,
        -0.57459236E-02,-0.22764243E-02,-0.23957635E-02,-0.31133549E-03,
        -0.12412696E-02,-0.10677285E-02,-0.38063349E-03, 0.27948857E-03,
        -0.30954584E-03, 0.47866389E-03, 0.17387358E-02,-0.95126998E-04,
         0.19152883E-03,-0.60076715E-03, 0.60164678E-03, 0.93236762E-04,
        -0.33970494E-03, 0.30844557E-03, 0.81666256E-03,-0.29112416E-03,
         0.20387825E-03, 0.46894283E-03,-0.80141705E-03,-0.61131618E-03,
        -0.44143299E-03, 0.28242008E-03, 0.68290357E-03, 0.94785340E-04,
         0.23450388E-03,-0.10207164E-03,-0.24759283E-03, 0.42270837E-03,
        -0.53476440E-04,-0.25562174E-03,-0.12967716E-04, 0.38460217E-04,
         0.27397045E-04, 0.59586440E-04, 0.14401830E-03,-0.85995824E-04,
         0.68241425E-04,-0.65015862E-04,-0.30420282E-04, 0.17975539E-03,
         0.10301811E-03,-0.20477001E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_250                              =v_t_e_q3ex_2_250                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_q3ex_2_250                              =v_t_e_q3ex_2_250                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]        *x32        
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_q3ex_2_250                              =v_t_e_q3ex_2_250                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q3ex_2_250                              =v_t_e_q3ex_2_250                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11*x23        *x51
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]*x12                
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11            *x52
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]    *x22        *x52
    ;
    v_t_e_q3ex_2_250                              =v_t_e_q3ex_2_250                              
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]            *x42*x52
        +coeff[ 46]*x13*x22            
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x21*x32*x42    
        ;

    return v_t_e_q3ex_2_250                              ;
}
float y_e_q3ex_2_250                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1000055E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.10673964E-02, 0.77452227E-01,-0.98263830E-01,-0.61637700E-01,
        -0.26983792E-01, 0.48194833E-01, 0.22091433E-01,-0.35308022E-01,
        -0.13023901E-01,-0.90076793E-02, 0.20601926E-02,-0.87023182E-02,
        -0.59380829E-02, 0.56597232E-02,-0.16767673E-03, 0.10159819E-02,
        -0.50376072E-02,-0.13036025E-02, 0.18639592E-02,-0.22500358E-02,
        -0.14690828E-02,-0.39261370E-02,-0.29983411E-02, 0.45350686E-03,
        -0.13581985E-03, 0.97972457E-03, 0.47615147E-04, 0.10925459E-02,
         0.38346648E-03,-0.92012389E-02,-0.63659288E-02, 0.23268627E-02,
        -0.13070338E-02,-0.11479515E-03, 0.73727577E-04, 0.85470709E-03,
        -0.44104061E-03,-0.45667868E-02, 0.39085647E-03,-0.29233994E-03,
        -0.15696911E-02,-0.93786267E-03,-0.38957808E-03,-0.47309301E-03,
         0.99652272E-03, 0.30847368E-04,-0.26147734E-04, 0.28613620E-03,
         0.12533418E-03,-0.10668632E-02,-0.24360792E-03, 0.37320706E-03,
         0.31659345E-02,-0.88037166E-03, 0.20378133E-04,-0.56031404E-04,
        -0.10350515E-02,-0.10929839E-02,-0.10574590E-02, 0.45641270E-03,
        -0.35285566E-03, 0.60583436E-03, 0.39342657E-03, 0.27733357E-03,
         0.28694938E-02, 0.58459984E-02, 0.29979344E-02, 0.94837777E-03,
         0.24955597E-03,-0.45566424E-03,-0.36939059E-02,-0.73151587E-03,
         0.30126431E-03, 0.36836980E-03, 0.18933641E-02,-0.13918007E-04,
         0.51087263E-04,-0.22539733E-04, 0.27695698E-04, 0.24378689E-04,
         0.61954233E-04, 0.43169517E-03, 0.17867130E-02, 0.39986746E-04,
         0.41072103E-02, 0.12384154E-03, 0.15054930E-03,-0.15982421E-03,
        -0.82062172E-04, 0.35780964E-02, 0.98748031E-04, 0.11577475E-02,
         0.19518557E-03,-0.41259883E-03,-0.52479567E-03,-0.18011218E-03,
        -0.24667422E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]        *x31    *x52
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]    *x22*x31    *x52
        +coeff[ 52]    *x23*x32*x41    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 53]    *x23    *x45    
        +coeff[ 54]    *x22            
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]        *x31*x44    
        +coeff[ 57]        *x32*x43    
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]        *x34*x41    
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 62]*x11*x21*x32*x41    
        +coeff[ 63]        *x31*x42*x52
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]    *x21*x32*x43    
        +coeff[ 66]    *x23*x31*x42    
        +coeff[ 67]    *x23*x33        
        +coeff[ 68]    *x21*x31    *x53
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 71]    *x23*x32*x41*x51
        +coeff[ 72]    *x23*x31*x44    
        +coeff[ 73]    *x22*x31*x42*x52
        +coeff[ 74]    *x22*x32*x45    
        +coeff[ 75]*x11        *x42    
        +coeff[ 76]*x11        *x41*x51
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11    *x31*x42    
        +coeff[ 79]*x11*x22*x31        
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 80]        *x33    *x51
        +coeff[ 81]    *x21*x31*x42*x51
        +coeff[ 82]    *x21    *x45    
        +coeff[ 83]*x11    *x31    *x52
        +coeff[ 84]    *x21*x31*x44    
        +coeff[ 85]    *x23*x31    *x51
        +coeff[ 86]    *x21*x33    *x51
        +coeff[ 87]*x11*x22    *x41*x51
        +coeff[ 88]        *x32*x41*x52
    ;
    v_y_e_q3ex_2_250                              =v_y_e_q3ex_2_250                              
        +coeff[ 89]    *x21*x33*x42    
        +coeff[ 90]        *x33    *x52
        +coeff[ 91]    *x21*x34*x41    
        +coeff[ 92]*x11*x22*x31*x42    
        +coeff[ 93]    *x22*x31*x42*x51
        +coeff[ 94]        *x32*x45    
        +coeff[ 95]    *x21    *x43*x52
        +coeff[ 96]    *x22*x31*x44    
        ;

    return v_y_e_q3ex_2_250                              ;
}
float p_e_q3ex_2_250                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1943730E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.21665181E-03, 0.18425634E-01,-0.33115458E-01, 0.16387919E-01,
        -0.87513272E-02,-0.98947706E-02, 0.77749360E-02,-0.38989936E-03,
         0.69403228E-04, 0.22535938E-03,-0.16511747E-03, 0.53039766E-02,
         0.17020568E-02, 0.44727973E-02,-0.57536550E-03, 0.31246629E-02,
         0.14692795E-02, 0.64241030E-03, 0.11422293E-02,-0.29587709E-04,
        -0.17614313E-03, 0.57478063E-03, 0.18960685E-02,-0.10579808E-02,
         0.28358219E-03,-0.35303715E-03,-0.93969115E-03,-0.40371806E-03,
        -0.56574554E-05,-0.20114314E-03,-0.40671614E-03, 0.85356129E-04,
         0.12821318E-02, 0.92678302E-03,-0.26370783E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_250                              =v_p_e_q3ex_2_250                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_250                              =v_p_e_q3ex_2_250                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_250                              =v_p_e_q3ex_2_250                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
        ;

    return v_p_e_q3ex_2_250                              ;
}
float l_e_q3ex_2_250                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1870552E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.18275918E-01,-0.32256472E+00,-0.25458967E-01, 0.12108503E-01,
        -0.35036236E-01,-0.10326917E-01,-0.39974679E-02,-0.34093459E-02,
        -0.50832774E-02,-0.55355690E-02, 0.21834206E-02, 0.95583079E-02,
         0.78293439E-02, 0.41242167E-02,-0.87495643E-03,-0.18317538E-02,
         0.26672469E-02,-0.20837714E-02, 0.12373031E-02, 0.17510551E-02,
         0.79840946E-03, 0.63816848E-03, 0.46481504E-03,-0.24434133E-03,
        -0.29818367E-03, 0.43552680E-03,-0.27364027E-03, 0.52631501E-03,
         0.76558227E-02, 0.64077978E-02, 0.10420312E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_2_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_250                              =v_l_e_q3ex_2_250                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_250                              =v_l_e_q3ex_2_250                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]            *x42*x51
        +coeff[ 22]                *x53
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3ex_2_250                              =v_l_e_q3ex_2_250                              
        +coeff[ 26]*x11        *x43    
        +coeff[ 27]*x11*x21        *x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]        *x32*x41*x54
        ;

    return v_l_e_q3ex_2_250                              ;
}
float x_e_q3ex_2_200                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1229471E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.12716230E-01,-0.71200444E-02, 0.24688751E+00, 0.72617292E-01,
        -0.13067313E-01, 0.26726106E-01, 0.82866633E-02,-0.67202090E-02,
        -0.47425232E-02,-0.11582109E-01,-0.69871405E-02, 0.37028120E-04,
        -0.10917454E-02, 0.11016130E-02,-0.19796528E-05, 0.16677290E-02,
        -0.44194018E-02, 0.42657820E-02,-0.29681198E-03,-0.58621337E-03,
         0.50502701E-03, 0.13212461E-02,-0.30029201E-03, 0.75797457E-03,
         0.69228925E-04,-0.92798454E-03, 0.82494447E-03,-0.85599750E-03,
         0.20041098E-02,-0.16505474E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_200                              =v_x_e_q3ex_2_200                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]        *x32        
        +coeff[ 13]                *x53
        +coeff[ 14]*x12*x21            
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_200                              =v_x_e_q3ex_2_200                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_200                              =v_x_e_q3ex_2_200                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x32    *x52
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_2_200                              ;
}
float t_e_q3ex_2_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6974750E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64428099E-02,-0.30248286E-01, 0.76446056E-01, 0.34869814E-02,
        -0.57265945E-02,-0.22732571E-02,-0.23603700E-02,-0.29119223E-03,
        -0.13133376E-02,-0.10766212E-02,-0.42893118E-03, 0.29064351E-03,
        -0.32137593E-03, 0.47071662E-03, 0.17342319E-02,-0.99613309E-04,
         0.18495602E-03,-0.58797322E-03, 0.48219183E-03, 0.98829834E-04,
        -0.32643133E-03, 0.31101040E-03, 0.77125948E-03,-0.27863367E-03,
         0.17149965E-03, 0.46571167E-03,-0.76608732E-03,-0.61540323E-03,
        -0.44252051E-03, 0.29280959E-03, 0.65600267E-03, 0.83187828E-04,
        -0.13746608E-03,-0.91932998E-04,-0.23724776E-03, 0.49286237E-03,
        -0.59303613E-04, 0.86014676E-04,-0.24260901E-03,-0.11458149E-04,
         0.52262352E-04, 0.19516109E-03, 0.15503886E-03, 0.88292189E-04,
        -0.61623919E-04, 0.17829638E-03, 0.16856681E-03, 0.94437703E-04,
        -0.30499531E-03, 0.16979531E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_200                              =v_t_e_q3ex_2_200                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_q3ex_2_200                              =v_t_e_q3ex_2_200                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]            *x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x22    *x42*x51
        +coeff[ 23]        *x32        
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_q3ex_2_200                              =v_t_e_q3ex_2_200                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]        *x32    *x52
        +coeff[ 30]    *x23        *x53
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q3ex_2_200                              =v_t_e_q3ex_2_200                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11*x23        *x51
        +coeff[ 37]*x11    *x32    *x52
        +coeff[ 38]        *x32    *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11*x23            
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]        *x31*x41*x52
    ;
    v_t_e_q3ex_2_200                              =v_t_e_q3ex_2_200                              
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_q3ex_2_200                              ;
}
float y_e_q3ex_2_200                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6712544E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.63617522E-03, 0.75680129E-01,-0.10017356E+00,-0.62712379E-01,
        -0.27118716E-01, 0.48223831E-01, 0.22079676E-01,-0.35295889E-01,
        -0.12855274E-01,-0.90392483E-02, 0.20353876E-02,-0.88521438E-02,
        -0.21263129E-03,-0.13705567E-02,-0.39966410E-03, 0.10223639E-02,
        -0.58162422E-02, 0.18957171E-02,-0.21806245E-02,-0.14704892E-02,
         0.61476780E-02,-0.37218886E-02,-0.30923805E-02,-0.49666693E-02,
        -0.13195951E-02, 0.45302030E-03, 0.19571979E-02, 0.23481927E-02,
         0.12822259E-02, 0.10218973E-02, 0.43576380E-03,-0.46482059E-03,
        -0.79288054E-02,-0.64634867E-02, 0.22794046E-02,-0.11492368E-02,
         0.37828362E-04,-0.32197491E-04, 0.94899529E-04, 0.89323020E-03,
        -0.48359958E-02, 0.42370165E-03,-0.30926452E-03,-0.17711832E-02,
        -0.80451468E-03,-0.38199039E-03,-0.75771223E-03, 0.10099788E-02,
        -0.10838603E-03, 0.21948514E-03,-0.80105243E-03,-0.72375085E-03,
         0.13069199E-03,-0.11991068E-02,-0.22730909E-03,-0.88144683E-04,
         0.86896744E-03, 0.27813074E-04,-0.46533220E-04, 0.43270885E-03,
         0.55696286E-03, 0.35056775E-03, 0.35753517E-03, 0.22619329E-02,
        -0.85281715E-03, 0.96625485E-03, 0.24064306E-03,-0.28277410E-03,
        -0.61319809E-03,-0.45967377E-02, 0.21754403E-03,-0.43200883E-02,
        -0.22403880E-02, 0.42970161E-04, 0.38582468E-03, 0.16370897E-03,
         0.52114035E-03, 0.18279982E-03, 0.12114177E-04,-0.17510098E-04,
         0.81381892E-04, 0.22889792E-04, 0.47599833E-03, 0.17848995E-02,
         0.11257522E-02, 0.21537456E-02,-0.16772063E-03, 0.22536109E-04,
         0.22915544E-02, 0.19955065E-02, 0.11545124E-03,-0.41238734E-03,
         0.54398633E-03,-0.82009858E-04, 0.77580422E-04, 0.25916653E-03,
         0.46577239E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]        *x34*x41    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 35]    *x22    *x45    
        +coeff[ 36]    *x21            
        +coeff[ 37]                *x51
        +coeff[ 38]        *x31    *x52
        +coeff[ 39]        *x31*x42*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]        *x31    *x53
        +coeff[ 46]    *x21*x32*x41*x51
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x11    *x31    *x51
        +coeff[ 49]        *x32*x41*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21*x31    *x51
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 53]    *x24    *x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]    *x22*x31    *x52
        +coeff[ 56]    *x23*x32*x43    
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 62]        *x31*x42*x52
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]    *x21*x31    *x53
        +coeff[ 67]    *x21    *x43*x52
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]    *x22*x33    *x51
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 71]    *x22*x32*x43    
        +coeff[ 72]    *x22*x33*x42    
        +coeff[ 73]    *x23*x31*x42*x51
        +coeff[ 74]    *x23*x33    *x51
        +coeff[ 75]    *x22    *x45*x51
        +coeff[ 76]    *x24*x31    *x52
        +coeff[ 77]    *x22*x33    *x52
        +coeff[ 78]        *x31*x41    
        +coeff[ 79]*x12    *x31        
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 80]*x11    *x32*x41    
        +coeff[ 81]*x11*x22*x31        
        +coeff[ 82]    *x21*x31*x42*x51
        +coeff[ 83]    *x21*x31*x44    
        +coeff[ 84]    *x23    *x43    
        +coeff[ 85]    *x21*x32*x43    
        +coeff[ 86]*x11*x22    *x41*x51
        +coeff[ 87]*x11    *x32*x41*x51
        +coeff[ 88]    *x23*x31*x42    
    ;
    v_y_e_q3ex_2_200                              =v_y_e_q3ex_2_200                              
        +coeff[ 89]    *x21*x33*x42    
        +coeff[ 90]        *x33    *x52
        +coeff[ 91]    *x22    *x43*x51
        +coeff[ 92]    *x21*x34*x41    
        +coeff[ 93]*x11*x21    *x41*x52
        +coeff[ 94]    *x21    *x41*x53
        +coeff[ 95]*x11*x22*x31*x42    
        +coeff[ 96]*x13        *x41*x51
        ;

    return v_y_e_q3ex_2_200                              ;
}
float p_e_q3ex_2_200                              (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1621163E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.14682271E-03, 0.18738940E-01,-0.32416210E-01, 0.16458489E-01,
        -0.87349657E-02,-0.98668225E-02, 0.77389479E-02,-0.68545825E-03,
        -0.10597450E-03, 0.17667818E-03, 0.98320590E-04, 0.54320642E-02,
         0.17055047E-02, 0.44729039E-02,-0.56334207E-03, 0.30833790E-02,
         0.14453281E-02, 0.60606247E-03, 0.11515412E-02, 0.27431224E-04,
        -0.17411041E-03, 0.56332239E-03, 0.18275789E-02,-0.11513998E-02,
         0.35624779E-03,-0.35261060E-03,-0.13904650E-02,-0.39828042E-03,
         0.76338911E-05,-0.61177113E-03,-0.19717892E-03,-0.60623890E-03,
         0.77496865E-04, 0.12720821E-02, 0.96329534E-03,-0.27236369E-03,
        -0.50502655E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_200                              =v_p_e_q3ex_2_200                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_200                              =v_p_e_q3ex_2_200                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_200                              =v_p_e_q3ex_2_200                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_q3ex_2_200                              =v_p_e_q3ex_2_200                              
        +coeff[ 35]            *x41*x53
        +coeff[ 36]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_200                              ;
}
float l_e_q3ex_2_200                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.2067242E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.18863251E-01,-0.32536978E+00,-0.25416415E-01, 0.11948148E-01,
        -0.35713457E-01,-0.10142448E-01,-0.40450711E-02, 0.23098930E-02,
        -0.45393859E-02,-0.57611517E-02, 0.10450620E-01, 0.94663929E-02,
         0.65527408E-03, 0.31335041E-03, 0.19973330E-02,-0.37672825E-02,
        -0.90734148E-03,-0.98037138E-03, 0.32937217E-02,-0.20545749E-02,
         0.10800401E-02, 0.15183161E-02, 0.70594728E-03,-0.32068620E-03,
         0.68230685E-04, 0.53455733E-03,-0.66309655E-03,-0.17204349E-02,
         0.50705164E-02, 0.34186363E-02, 0.13708182E-02, 0.15789925E-02,
         0.33024559E-02, 0.75955468E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q3ex_2_200                              =v_l_e_q3ex_2_200                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3ex_2_200                              =v_l_e_q3ex_2_200                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]                *x53
    ;
    v_l_e_q3ex_2_200                              =v_l_e_q3ex_2_200                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]        *x31*x41*x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x32    *x52
        +coeff[ 31]    *x24    *x42    
        +coeff[ 32]    *x22*x31*x41*x52
        +coeff[ 33]            *x44*x53
        ;

    return v_l_e_q3ex_2_200                              ;
}
float x_e_q3ex_2_175                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1361698E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.14246412E-01,-0.70737917E-02, 0.24668527E+00, 0.73171526E-01,
        -0.13071782E-01, 0.26613448E-01, 0.82183396E-02,-0.67073908E-02,
        -0.47888076E-02,-0.11379580E-01,-0.45108311E-02,-0.70200637E-02,
        -0.10805157E-02, 0.10897885E-02, 0.16427916E-02, 0.43603708E-02,
        -0.28898692E-03,-0.58237993E-03, 0.48383238E-03, 0.13719446E-02,
        -0.31041342E-03, 0.75992133E-03,-0.16511318E-04, 0.91343053E-03,
         0.23015549E-02,-0.13958736E-02,-0.13878712E-02,-0.87781751E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_q3ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_175                              =v_x_e_q3ex_2_175                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x32        
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_x_e_q3ex_2_175                              =v_x_e_q3ex_2_175                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x22    *x42*x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_x_e_q3ex_2_175                              =v_x_e_q3ex_2_175                              
        +coeff[ 26]    *x23    *x42*x51
        +coeff[ 27]    *x21*x32    *x54
        ;

    return v_x_e_q3ex_2_175                              ;
}
float t_e_q3ex_2_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7637659E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.69492953E-02,-0.30372646E-01, 0.76242670E-01, 0.34654608E-02,
        -0.56856913E-02,-0.22278407E-02,-0.22711286E-02,-0.28293420E-03,
        -0.13535968E-02,-0.10594246E-02,-0.43162759E-03, 0.28569542E-03,
         0.18268946E-03,-0.27020564E-03, 0.47905053E-03, 0.17354606E-02,
        -0.97486300E-04,-0.55437704E-03, 0.23413461E-03, 0.48644893E-03,
         0.11271152E-03,-0.31859559E-03, 0.33541746E-03, 0.83319086E-03,
         0.48213682E-03,-0.79338753E-03,-0.61275484E-03,-0.35793259E-03,
         0.43095532E-03, 0.67838794E-03,-0.24733084E-03, 0.85524771E-04,
         0.20556644E-03,-0.13462496E-03,-0.12698774E-03, 0.25000924E-03,
        -0.11337231E-03,-0.24249971E-03, 0.82107057E-04,-0.27343066E-03,
        -0.11074047E-04,-0.20155689E-04, 0.70235677E-04, 0.11394013E-03,
         0.10646007E-03, 0.12819676E-03,-0.28214048E-03,-0.74101044E-04,
         0.88957080E-04,-0.18849294E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_2_175                              =v_t_e_q3ex_2_175                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]                *x53
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_2_175                              =v_t_e_q3ex_2_175                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_175                              =v_t_e_q3ex_2_175                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]        *x32        
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_q3ex_2_175                              =v_t_e_q3ex_2_175                              
        +coeff[ 35]        *x32    *x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]*x11    *x32    *x52
        +coeff[ 39]        *x32    *x53
        +coeff[ 40]*x12                
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]*x11*x23            
        +coeff[ 43]        *x32*x42    
    ;
    v_t_e_q3ex_2_175                              =v_t_e_q3ex_2_175                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x32    *x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_q3ex_2_175                              ;
}
float y_e_q3ex_2_175                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4720230E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.48708811E-03, 0.74229009E-01,-0.10157182E+00,-0.62954292E-01,
        -0.27321694E-01, 0.48109509E-01, 0.22021024E-01,-0.35327490E-01,
        -0.12849604E-01,-0.90032900E-02, 0.20537744E-02,-0.85675986E-02,
        -0.58780899E-02, 0.60403016E-02,-0.24509942E-03, 0.99853147E-03,
        -0.49270242E-02,-0.12679350E-02, 0.18477821E-02,-0.21288835E-02,
        -0.14338801E-02,-0.33360794E-02,-0.30681244E-02, 0.43389687E-03,
         0.18599993E-02, 0.38335591E-02, 0.17200953E-02, 0.11312318E-02,
         0.66548941E-03,-0.92368340E-02,-0.65267351E-02, 0.23090590E-02,
        -0.97138894E-03,-0.10890784E-03, 0.66212946E-04, 0.88641478E-03,
        -0.46406651E-03,-0.49576252E-02, 0.28753807E-03,-0.29692211E-03,
        -0.15898050E-02,-0.87238802E-03,-0.38051687E-03,-0.73191785E-03,
         0.96199481E-03, 0.24703451E-04,-0.21102829E-04, 0.32975865E-03,
         0.13190262E-03,-0.10984974E-02,-0.29815407E-03, 0.40536313E-03,
         0.21751202E-02,-0.40197938E-04,-0.11037211E-02,-0.13100860E-02,
        -0.11385534E-02, 0.42307598E-03,-0.31124701E-03, 0.54887187E-03,
         0.34806161E-03, 0.29995805E-03, 0.11136449E-02, 0.13370630E-02,
         0.69783477E-03, 0.21296262E-03,-0.83167362E-03, 0.73908712E-04,
         0.33955273E-03,-0.12508844E-02, 0.14550440E-04,-0.17198012E-04,
         0.29954976E-04, 0.49644550E-04,-0.49195482E-04, 0.42077771E-03,
         0.31720065E-04, 0.79660269E-03,-0.17410616E-03, 0.45794288E-04,
        -0.43705291E-04, 0.29164632E-05, 0.12162924E-03,-0.49587805E-03,
        -0.28246808E-04, 0.18623901E-03,-0.17897502E-03, 0.11789546E-04,
        -0.48749280E-03, 0.37999849E-04,-0.21824893E-02, 0.51828113E-03,
         0.13301655E-03,-0.20640125E-02,-0.57746882E-04, 0.96878975E-04,
         0.17787087E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x21*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x22    *x45    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]        *x31    *x52
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]    *x22*x31    *x52
        +coeff[ 52]    *x23*x32*x41    
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 53]*x12        *x41    
        +coeff[ 54]        *x31*x44    
        +coeff[ 55]        *x32*x43    
        +coeff[ 56]        *x33*x42    
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]        *x34*x41    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]        *x31*x42*x52
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x23*x31*x42    
        +coeff[ 64]    *x23*x33        
        +coeff[ 65]    *x21*x31    *x53
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x33    *x51
        +coeff[ 68]    *x23*x33    *x51
        +coeff[ 69]    *x24*x31*x42*x51
        +coeff[ 70]    *x22            
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11*x22*x31        
        +coeff[ 73]        *x33    *x51
        +coeff[ 74]*x11*x22    *x42    
        +coeff[ 75]    *x21*x31*x42*x51
        +coeff[ 76]*x11    *x31    *x52
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]*x11*x22    *x41*x51
        +coeff[ 79]*x11    *x32*x41*x51
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 80]        *x32*x41*x52
        +coeff[ 81]*x11    *x31*x44    
        +coeff[ 82]        *x33    *x52
        +coeff[ 83]    *x22    *x43*x51
        +coeff[ 84]*x11        *x44*x51
        +coeff[ 85]*x11*x22*x31*x42    
        +coeff[ 86]    *x21    *x43*x52
        +coeff[ 87]*x13        *x41*x51
        +coeff[ 88]    *x22*x32*x41*x51
    ;
    v_y_e_q3ex_2_175                              =v_y_e_q3ex_2_175                              
        +coeff[ 89]*x11        *x41*x53
        +coeff[ 90]    *x22*x31*x44    
        +coeff[ 91]        *x33*x44    
        +coeff[ 92]    *x21    *x45*x51
        +coeff[ 93]    *x22*x32*x43    
        +coeff[ 94]*x11        *x43*x52
        +coeff[ 95]            *x43*x53
        +coeff[ 96]    *x23    *x41*x52
        ;

    return v_y_e_q3ex_2_175                              ;
}
float p_e_q3ex_2_175                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.5990704E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.67554211E-04, 0.18954586E-01,-0.31843301E-01, 0.16584557E-01,
        -0.87219728E-02,-0.98345960E-02, 0.45847967E-02, 0.76720915E-02,
        -0.66458120E-03,-0.10549588E-03, 0.10706737E-03, 0.53897286E-02,
         0.16964413E-02,-0.55848272E-03, 0.30270054E-02, 0.14248953E-02,
         0.61269855E-03, 0.11531052E-02, 0.69712905E-05,-0.17060278E-03,
         0.55371621E-03, 0.18049434E-02,-0.11589150E-02, 0.34928881E-03,
        -0.34307645E-03,-0.14609568E-02,-0.69630903E-03,-0.38380077E-03,
         0.12113052E-04,-0.20085758E-03,-0.70836744E-03, 0.83416518E-04,
         0.11847563E-02, 0.93647576E-03,-0.25070511E-03,-0.48310880E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_2_175                              =v_p_e_q3ex_2_175                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_2_175                              =v_p_e_q3ex_2_175                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_2_175                              =v_p_e_q3ex_2_175                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11        *x41*x51
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_p_e_q3ex_2_175                              =v_p_e_q3ex_2_175                              
        +coeff[ 35]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_175                              ;
}
float l_e_q3ex_2_175                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2194978E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.18847458E-01,-0.32628959E+00,-0.25556507E-01, 0.11887850E-01,
        -0.35774931E-01,-0.97186984E-02,-0.39675264E-02,-0.33911483E-02,
        -0.52587404E-02,-0.59421975E-02, 0.23021172E-02, 0.10770428E-01,
         0.78517720E-02, 0.24859344E-02,-0.83023723E-03,-0.16639538E-02,
         0.31216620E-02,-0.17022566E-02, 0.16983749E-02, 0.72234368E-03,
         0.75090211E-03, 0.75799366E-03, 0.16144632E-02, 0.94173651E-03,
         0.43196343E-02,-0.13449593E-02,-0.85271022E-03, 0.14384191E-01,
         0.50884741E-02, 0.68419636E-02, 0.21021061E-02, 0.31238121E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_175                              =v_l_e_q3ex_2_175                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_175                              =v_l_e_q3ex_2_175                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]    *x22*x31*x41    
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x23*x31*x41*x51
    ;
    v_l_e_q3ex_2_175                              =v_l_e_q3ex_2_175                              
        +coeff[ 26]    *x23        *x53
        +coeff[ 27]    *x23*x31*x43    
        +coeff[ 28]    *x23    *x44    
        +coeff[ 29]    *x21*x32*x44    
        +coeff[ 30]*x11*x24*x31    *x51
        +coeff[ 31]*x11*x22*x32*x41*x51
        ;

    return v_l_e_q3ex_2_175                              ;
}
float x_e_q3ex_2_150                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1308332E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.14776731E-01,-0.70180660E-02, 0.24671699E+00, 0.73470518E-01,
        -0.13017012E-01, 0.26449144E-01, 0.80784261E-02,-0.67470758E-02,
        -0.48850523E-02,-0.11127185E-01,-0.67030936E-02,-0.81523840E-03,
        -0.11027481E-02,-0.35501103E-05, 0.16518703E-02,-0.43302882E-02,
         0.41101067E-02,-0.29895775E-03, 0.10938188E-02,-0.60031231E-03,
         0.53496513E-03, 0.13023763E-02,-0.29694993E-03, 0.82342606E-03,
         0.70287468E-04,-0.81075326E-03, 0.93447807E-03, 0.20481341E-02,
        -0.15691067E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_150                              =v_x_e_q3ex_2_150                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_2_150                              =v_x_e_q3ex_2_150                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_2_150                              =v_x_e_q3ex_2_150                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_2_150                              ;
}
float t_e_q3ex_2_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7401008E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.63419165E-02,-0.30299446E-01, 0.76040395E-01, 0.33450772E-02,
        -0.56917602E-02,-0.13998550E-02,-0.22351451E-02,-0.21467896E-02,
        -0.27166220E-03,-0.10911193E-02,-0.34921395E-03, 0.30880890E-03,
         0.21802601E-03,-0.28149338E-03, 0.47978637E-03, 0.16930612E-02,
        -0.10140669E-03,-0.60091185E-03, 0.22525096E-03, 0.44710061E-03,
         0.12505063E-03,-0.35455899E-03, 0.35176377E-03, 0.80319820E-03,
         0.47980610E-03,-0.58161677E-03,-0.60569821E-03,-0.19726178E-03,
        -0.24131231E-03, 0.80774655E-04,-0.39023031E-04,-0.87007880E-04,
        -0.77794255E-04, 0.28878229E-03,-0.17331228E-03, 0.17458570E-03,
         0.95978459E-04,-0.27911965E-03,-0.47194958E-03, 0.51724893E-03,
         0.55692177E-04, 0.12550270E-03, 0.10494755E-03,-0.73691554E-04,
         0.12824709E-03,-0.26661661E-03, 0.16153711E-03,-0.27011210E-03,
         0.28616365E-03, 0.14020986E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_150                              =v_t_e_q3ex_2_150                              
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]                *x53
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_2_150                              =v_t_e_q3ex_2_150                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_150                              =v_t_e_q3ex_2_150                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]        *x32        
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q3ex_2_150                              =v_t_e_q3ex_2_150                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x31*x41*x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3ex_2_150                              =v_t_e_q3ex_2_150                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x21*x32*x42    
        +coeff[ 46]        *x33*x41*x51
        +coeff[ 47]        *x32*x42*x51
        +coeff[ 48]    *x21*x31*x41*x52
        +coeff[ 49]    *x21    *x42*x52
        ;

    return v_t_e_q3ex_2_150                              ;
}
float y_e_q3ex_2_150                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1366610E-05;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.26072998E-04, 0.72028257E-01,-0.10359776E+00,-0.63294664E-01,
        -0.27194502E-01, 0.48062783E-01, 0.21982027E-01,-0.34894265E-01,
        -0.12508603E-01,-0.89461375E-02, 0.20587826E-02,-0.92847170E-02,
        -0.61524590E-02, 0.57321144E-02,-0.93641480E-04, 0.99382258E-03,
        -0.50551072E-02,-0.13369095E-02, 0.18192686E-02,-0.22366268E-02,
        -0.13718592E-02,-0.33131037E-02,-0.29566931E-02, 0.45130370E-03,
         0.30320213E-03, 0.90577424E-03, 0.72360344E-04, 0.27853626E-04,
         0.80845220E-03, 0.36291481E-03,-0.66644512E-02,-0.49464935E-02,
         0.22049325E-02,-0.13811780E-02,-0.50014129E-03,-0.10081205E-03,
         0.94569102E-03,-0.45971724E-03,-0.39182147E-02, 0.43795761E-03,
        -0.30549266E-03,-0.17405165E-02,-0.38498850E-03, 0.95170137E-03,
         0.25628829E-04,-0.19022662E-04, 0.27569893E-03, 0.12855425E-03,
        -0.99780515E-03,-0.26433519E-03,-0.59359224E-03,-0.67679206E-03,
         0.43818637E-03,-0.55679480E-04, 0.29487878E-02,-0.52863379E-04,
        -0.79596357E-03, 0.41071014E-03, 0.56390482E-03, 0.33457219E-03,
         0.16094978E-02, 0.45574545E-02, 0.26855713E-02, 0.18088397E-03,
        -0.11297315E-02, 0.94417064E-03, 0.28239860E-03, 0.26490001E-03,
        -0.74676715E-03,-0.59956172E-02,-0.62660226E-02,-0.34840910E-02,
        -0.12531631E-02, 0.61913504E-03,-0.76751277E-03, 0.14947509E-04,
         0.24491599E-04, 0.40916904E-04,-0.20966012E-03, 0.32896415E-03,
         0.11810507E-02, 0.40599345E-04,-0.10292425E-03, 0.33098527E-02,
         0.12058884E-03, 0.18053455E-03,-0.97766970E-04, 0.80781552E-04,
         0.28914013E-02,-0.40485372E-03, 0.93048712E-03,-0.51361032E-04,
         0.59887851E-04, 0.14514964E-03,-0.63103333E-04,-0.17806418E-03,
        -0.47623104E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]    *x23*x32*x41*x51
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 35]*x11    *x31    *x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]*x11*x21*x31    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]            *x41*x53
        +coeff[ 50]    *x22*x33        
        +coeff[ 51]    *x21*x32*x41*x51
        +coeff[ 52]        *x31*x42*x52
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 53]    *x22*x31    *x52
        +coeff[ 54]    *x23*x32*x41    
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x43    
        +coeff[ 61]    *x21*x32*x43    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 62]    *x23*x31*x42    
        +coeff[ 63]        *x33    *x52
        +coeff[ 64]    *x22*x31*x42*x51
        +coeff[ 65]    *x23*x33        
        +coeff[ 66]        *x32*x45    
        +coeff[ 67]    *x21*x31    *x53
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]    *x22*x34*x41    
        +coeff[ 73]    *x24*x31    *x52
        +coeff[ 74]    *x24    *x45    
        +coeff[ 75]    *x22            
        +coeff[ 76]*x11*x22*x31        
        +coeff[ 77]        *x33    *x51
        +coeff[ 78]        *x33*x42    
        +coeff[ 79]    *x21*x31*x42*x51
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 80]    *x21    *x45    
        +coeff[ 81]*x11    *x31    *x52
        +coeff[ 82]            *x43*x52
        +coeff[ 83]    *x21*x31*x44    
        +coeff[ 84]    *x23*x31    *x51
        +coeff[ 85]    *x21*x33    *x51
        +coeff[ 86]*x11*x22    *x41*x51
        +coeff[ 87]*x11    *x32*x41*x51
        +coeff[ 88]    *x21*x33*x42    
    ;
    v_y_e_q3ex_2_150                              =v_y_e_q3ex_2_150                              
        +coeff[ 89]    *x22    *x43*x51
        +coeff[ 90]    *x21*x34*x41    
        +coeff[ 91]*x12*x21    *x41*x51
        +coeff[ 92]    *x21    *x41*x53
        +coeff[ 93]*x11*x22*x31*x42    
        +coeff[ 94]*x11*x21*x31    *x52
        +coeff[ 95]    *x21    *x43*x52
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_150                              ;
}
float p_e_q3ex_2_150                              (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.7377570E-04;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.62582651E-04, 0.19291751E-01,-0.31024337E-01, 0.16539635E-01,
        -0.86896755E-02,-0.97298156E-02, 0.74536474E-02,-0.69048791E-03,
        -0.15471979E-03, 0.16912000E-03, 0.14090349E-03, 0.54040565E-02,
         0.17035357E-02, 0.43176403E-02,-0.55177585E-03, 0.29885692E-02,
         0.13733292E-02, 0.62080991E-03, 0.11432010E-02, 0.79934498E-05,
        -0.16852356E-03, 0.55833819E-03, 0.17680002E-02,-0.11323603E-02,
         0.34488621E-03,-0.33559481E-03,-0.14065830E-02,-0.63372002E-03,
        -0.39620392E-03, 0.18727596E-05,-0.18883450E-03,-0.69639308E-03,
         0.75323536E-04,-0.14159914E-03, 0.12002769E-02, 0.95393060E-03,
        -0.27778689E-03,-0.46396273E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_150                              =v_p_e_q3ex_2_150                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_150                              =v_p_e_q3ex_2_150                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_150                              =v_p_e_q3ex_2_150                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_2_150                              =v_p_e_q3ex_2_150                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_150                              ;
}
float l_e_q3ex_2_150                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.2153942E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.14071302E-01,-0.32616901E+00,-0.25485802E-01, 0.11875510E-01,
        -0.35500202E-01,-0.93889777E-02,-0.38513900E-02,-0.33350636E-02,
        -0.53437427E-02,-0.55273459E-02, 0.22881255E-02, 0.72455639E-02,
         0.58256253E-02, 0.15965039E-02,-0.89345674E-03,-0.14273685E-02,
         0.32273293E-02,-0.21356093E-02, 0.19114617E-02, 0.65758871E-03,
         0.99359488E-03, 0.49218017E-03, 0.54137356E-03,-0.38895663E-03,
         0.16196298E-02,-0.12201105E-02, 0.75273053E-02, 0.55784048E-02,
         0.11137486E-01, 0.75660069E-02, 0.30242980E-02, 0.12199956E-02,
        -0.97996008E-03,-0.53956122E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_150                              =v_l_e_q3ex_2_150                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_150                              =v_l_e_q3ex_2_150                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_q3ex_2_150                              =v_l_e_q3ex_2_150                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]    *x21    *x44    
        +coeff[ 31]    *x24    *x42    
        +coeff[ 32]    *x23        *x53
        +coeff[ 33]    *x21*x34*x42    
        ;

    return v_l_e_q3ex_2_150                              ;
}
float x_e_q3ex_2_125                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1168650E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.13272496E-01, 0.24622397E+00, 0.74949779E-01,-0.12956142E-01,
         0.26605427E-01, 0.28270846E-04,-0.69449604E-02, 0.80888337E-02,
        -0.66321082E-02,-0.48935818E-02,-0.10900802E-01,-0.72984579E-02,
         0.47446852E-05,-0.10885959E-02,-0.44620487E-04, 0.16815292E-02,
        -0.42552655E-02, 0.43891170E-02,-0.29698780E-03, 0.10778931E-02,
        -0.62833377E-03, 0.80896029E-03, 0.13761487E-02,-0.31499079E-03,
         0.69732632E-03, 0.28128543E-04,-0.91085501E-03, 0.11527422E-02,
        -0.89018844E-03, 0.22817678E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]    *x22            
    ;
    v_x_e_q3ex_2_125                              =v_x_e_q3ex_2_125                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x12*x21            
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_2_125                              =v_x_e_q3ex_2_125                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x23            
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_2_125                              =v_x_e_q3ex_2_125                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x22    *x42*x51
        ;

    return v_x_e_q3ex_2_125                              ;
}
float t_e_q3ex_2_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6516714E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54947557E-02,-0.30811865E-01, 0.75705655E-01, 0.33590181E-02,
        -0.56506577E-02,-0.15119285E-02,-0.22037891E-02,-0.21025436E-02,
        -0.26108182E-03,-0.10825313E-02,-0.43288234E-03, 0.32551101E-03,
        -0.33214974E-03, 0.47118359E-03, 0.16966672E-02,-0.97442673E-04,
         0.20282615E-03,-0.64550480E-03, 0.26284796E-03, 0.42215292E-03,
         0.86254258E-04,-0.33116800E-03, 0.38848474E-03, 0.84202207E-03,
         0.49272215E-03,-0.65191218E-03,-0.57141203E-03,-0.19533373E-03,
         0.60715096E-03,-0.21867351E-03, 0.89423724E-04,-0.12986800E-03,
        -0.10738806E-03, 0.28735984E-03,-0.19664464E-03, 0.26052518E-03,
         0.95968913E-04,-0.26385163E-03,-0.45639757E-03,-0.14105707E-04,
        -0.25312778E-04, 0.13559371E-03, 0.85572072E-04,-0.73330877E-04,
         0.24291374E-03,-0.41462448E-04, 0.17534048E-03, 0.17506479E-03,
        -0.68306726E-04, 0.18929328E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_125                              =v_t_e_q3ex_2_125                              
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_q3ex_2_125                              =v_t_e_q3ex_2_125                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_125                              =v_t_e_q3ex_2_125                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]        *x32        
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_q3ex_2_125                              =v_t_e_q3ex_2_125                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]*x11    *x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]*x12                
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x31*x41*x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_q3ex_2_125                              =v_t_e_q3ex_2_125                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]*x11*x23    *x41    
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_q3ex_2_125                              ;
}
float y_e_q3ex_2_125                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1183424E-03;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.59002050E-04, 0.69992796E-01,-0.10595442E+00,-0.63859783E-01,
        -0.27390704E-01, 0.48076704E-01, 0.21944117E-01,-0.36121119E-01,
        -0.12903176E-01,-0.89117885E-02, 0.20336274E-02,-0.91722161E-02,
        -0.62717265E-02, 0.57990430E-02,-0.43740587E-04, 0.96693321E-03,
        -0.51179514E-02,-0.12629317E-02, 0.18103472E-02,-0.20921815E-02,
        -0.13803405E-02,-0.35685645E-02,-0.30611071E-02, 0.41506076E-03,
         0.80685146E-04, 0.18514942E-02, 0.16039234E-03, 0.12969400E-03,
         0.82700944E-03, 0.94123941E-03, 0.48710368E-03,-0.27657470E-02,
        -0.29715199E-02, 0.22161542E-02,-0.30027556E-02, 0.48381811E-04,
        -0.36246587E-04,-0.99148448E-04,-0.48819149E-03,-0.19559965E-02,
         0.45126062E-03,-0.32339856E-03,-0.14507173E-02,-0.51719137E-03,
        -0.38858815E-03,-0.79949084E-03, 0.96736976E-03, 0.17482135E-03,
         0.14649246E-03,-0.61542465E-03,-0.24029757E-03, 0.22780630E-02,
        -0.99119181E-02,-0.11911850E-01,-0.66965409E-02,-0.35132561E-03,
         0.29805262E-04,-0.41137890E-04, 0.41781241E-03, 0.59279898E-03,
         0.34796642E-03, 0.18612854E-02, 0.41972313E-03, 0.71327511E-03,
         0.23299460E-03,-0.29830239E-03,-0.65991218E-03,-0.20171544E-02,
        -0.39829598E-02,-0.18530828E-02,-0.19318949E-02, 0.53726125E-03,
         0.47087041E-02, 0.74440124E-03, 0.35771195E-03, 0.51276549E-02,
         0.32603806E-02,-0.11867998E-02, 0.25863641E-04, 0.10509000E-04,
         0.48196696E-04,-0.15174841E-04, 0.76459168E-04, 0.64642598E-04,
         0.46678462E-04,-0.17148176E-03, 0.12754514E-03, 0.13458509E-02,
         0.34284887E-04,-0.97810458E-04, 0.19233276E-02, 0.31903377E-02,
        -0.11833762E-03, 0.86182074E-04, 0.18861219E-02,-0.24406180E-03,
         0.11259350E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x23    *x41    
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]            *x43    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x21    *x43    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x23    *x41*x51
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 35]    *x21            
        +coeff[ 36]                *x51
        +coeff[ 37]*x11    *x31    *x51
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x22*x33        
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 44]        *x31    *x53
        +coeff[ 45]    *x21*x32*x41*x51
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x24    *x41    
        +coeff[ 50]            *x41*x53
        +coeff[ 51]    *x23*x32*x41    
        +coeff[ 52]    *x22*x31*x44    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 53]    *x22*x32*x43    
        +coeff[ 54]    *x22*x33*x42    
        +coeff[ 55]    *x22*x31*x42*x52
        +coeff[ 56]    *x22            
        +coeff[ 57]*x12        *x41    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 62]    *x22*x31    *x52
        +coeff[ 63]    *x23*x33        
        +coeff[ 64]    *x21*x31    *x53
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x24    *x43    
        +coeff[ 68]    *x24*x31*x42    
        +coeff[ 69]    *x24*x32*x41    
        +coeff[ 70]    *x22*x34*x41    
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 71]    *x21*x33*x42*x51
        +coeff[ 72]    *x23*x31*x44    
        +coeff[ 73]        *x33*x42*x52
        +coeff[ 74]    *x23*x33    *x51
        +coeff[ 75]    *x23*x32*x43    
        +coeff[ 76]    *x23*x33*x42    
        +coeff[ 77]    *x24*x31*x42*x51
        +coeff[ 78]        *x31*x41    
        +coeff[ 79]    *x21        *x51
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 80]        *x32*x42    
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]*x11    *x31*x42    
        +coeff[ 83]*x11*x22*x31        
        +coeff[ 84]        *x33    *x51
        +coeff[ 85]        *x33*x42    
        +coeff[ 86]    *x21*x31*x42*x51
        +coeff[ 87]    *x21    *x45    
        +coeff[ 88]*x11    *x31    *x52
    ;
    v_y_e_q3ex_2_125                              =v_y_e_q3ex_2_125                              
        +coeff[ 89]            *x43*x52
        +coeff[ 90]    *x21*x31*x44    
        +coeff[ 91]    *x21*x32*x43    
        +coeff[ 92]*x11*x22    *x41*x51
        +coeff[ 93]*x11    *x32*x41*x51
        +coeff[ 94]    *x21*x33*x42    
        +coeff[ 95]    *x22    *x43*x51
        +coeff[ 96]    *x21*x34*x41    
        ;

    return v_y_e_q3ex_2_125                              ;
}
float p_e_q3ex_2_125                              (float *x,int m){
    int ncoeff= 35;
    float avdat=  0.7507815E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
        -0.52420473E-04, 0.19632909E-01,-0.30205693E-01, 0.16236793E-01,
        -0.86686257E-02,-0.97428262E-02, 0.75561157E-02,-0.49994816E-03,
         0.10306304E-03, 0.13913059E-03, 0.50853984E-02, 0.16976177E-02,
         0.43805065E-02,-0.53899531E-03, 0.29170383E-02, 0.13889056E-02,
         0.61931869E-03, 0.11720051E-02,-0.38458409E-04,-0.15865269E-03,
         0.55661728E-03, 0.17827435E-02,-0.99425437E-03, 0.30753043E-03,
        -0.32611482E-03,-0.83129614E-03,-0.38727507E-03,-0.25602885E-05,
        -0.18684089E-03,-0.35031469E-03, 0.76132506E-04, 0.13900358E-02,
         0.96095243E-03,-0.27325927E-03,-0.43006451E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_2_125                              =v_p_e_q3ex_2_125                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_2_125                              =v_p_e_q3ex_2_125                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]    *x21    *x41*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_2_125                              =v_p_e_q3ex_2_125                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11        *x41*x51
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]            *x41*x53
        +coeff[ 34]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_125                              ;
}
float l_e_q3ex_2_125                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1918389E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.12265782E-01,-0.33133909E+00,-0.25540462E-01, 0.11758480E-01,
        -0.36621545E-01,-0.93792276E-02,-0.40477547E-02,-0.35654667E-02,
        -0.50591896E-02,-0.55372724E-02, 0.22531617E-02, 0.62660640E-02,
         0.79145646E-02, 0.14614768E-02,-0.94513636E-03,-0.20651754E-02,
         0.37472339E-02,-0.19831206E-02, 0.20181560E-02, 0.72277122E-03,
         0.19635976E-03, 0.62711525E-03, 0.59698254E-03, 0.51521364E-03,
        -0.32731905E-03,-0.15111916E-02, 0.83870059E-02, 0.51411969E-03,
         0.66543180E-02, 0.77117686E-02, 0.57236134E-03, 0.10443287E-02,
         0.12078915E-02,-0.14689114E-02, 0.79755223E-03, 0.73055704E-02,
        -0.33247699E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_2_125                              =v_l_e_q3ex_2_125                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_2_125                              =v_l_e_q3ex_2_125                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]                *x53
        +coeff[ 20]    *x22*x32    *x51
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11    *x31*x41    
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q3ex_2_125                              =v_l_e_q3ex_2_125                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]        *x34*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]*x12*x21    *x41*x51
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]*x12*x23    *x41    
    ;
    v_l_e_q3ex_2_125                              =v_l_e_q3ex_2_125                              
        +coeff[ 35]    *x21*x32*x44    
        +coeff[ 36]    *x22*x33*x41*x51
        ;

    return v_l_e_q3ex_2_125                              ;
}
float x_e_q3ex_2_100                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.1203649E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.13860990E-01,-0.68860329E-02, 0.24555825E+00, 0.76752558E-01,
        -0.12919933E-01, 0.26536696E-01, 0.80878744E-02,-0.65923366E-02,
        -0.48455196E-02,-0.10699016E-01,-0.45992448E-02,-0.63675772E-02,
        -0.10174393E-02, 0.17176581E-02, 0.41710739E-02,-0.25779131E-03,
         0.10838480E-02,-0.60343678E-03, 0.74427569E-03, 0.15167545E-02,
        -0.28497478E-03, 0.67059643E-03, 0.65277323E-04,-0.78800484E-03,
         0.11310357E-02, 0.21777989E-02,-0.16624469E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_2_100                              =v_x_e_q3ex_2_100                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]                *x53
    ;
    v_x_e_q3ex_2_100                              =v_x_e_q3ex_2_100                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22    *x42*x51
    ;
    v_x_e_q3ex_2_100                              =v_x_e_q3ex_2_100                              
        +coeff[ 26]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_2_100                              ;
}
float t_e_q3ex_2_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6701508E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55862712E-02,-0.31252217E-01, 0.75183436E-01, 0.33660259E-02,
        -0.56347316E-02,-0.16544268E-02,-0.21822019E-02,-0.21121323E-02,
        -0.23944922E-03,-0.10947613E-02,-0.45809484E-03, 0.33739049E-03,
        -0.34645663E-03, 0.47466802E-03, 0.15548309E-02,-0.10951258E-03,
         0.17727324E-03,-0.66730171E-03, 0.39242586E-03, 0.30090663E-03,
         0.11559053E-03,-0.34399913E-03, 0.39954900E-03, 0.83126972E-03,
         0.49331633E-03,-0.72931132E-03,-0.57261274E-03,-0.39064256E-03,
         0.61972142E-03,-0.20021203E-03, 0.80234036E-04,-0.11660855E-03,
        -0.10009229E-03, 0.32253453E-03, 0.16005678E-03,-0.19309818E-03,
         0.40139788E-03, 0.99129989E-04,-0.27325525E-03,-0.21456861E-04,
        -0.44483517E-04, 0.62986401E-04, 0.20696824E-03, 0.13823480E-03,
        -0.10014873E-03, 0.21893188E-03, 0.12221029E-03, 0.16563822E-03,
         0.35858236E-03, 0.45848559E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_2_100                              =v_t_e_q3ex_2_100                              
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]                *x53
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_q3ex_2_100                              =v_t_e_q3ex_2_100                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21*x31*x41    
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x42*x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q3ex_2_100                              =v_t_e_q3ex_2_100                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]        *x32        
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_q3ex_2_100                              =v_t_e_q3ex_2_100                              
        +coeff[ 35]    *x21        *x53
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]*x11    *x32    *x52
        +coeff[ 38]        *x32    *x53
        +coeff[ 39]*x11*x21        *x51
        +coeff[ 40]*x11*x21*x32        
        +coeff[ 41]*x11*x22        *x51
        +coeff[ 42]    *x23*x32        
        +coeff[ 43]        *x33*x41*x51
    ;
    v_t_e_q3ex_2_100                              =v_t_e_q3ex_2_100                              
        +coeff[ 44]        *x32*x42*x51
        +coeff[ 45]    *x21*x31*x41*x52
        +coeff[ 46]    *x21    *x42*x52
        +coeff[ 47]        *x31*x41*x53
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_q3ex_2_100                              ;
}
float y_e_q3ex_2_100                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7336769E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.69334073E-03, 0.66672176E-01,-0.10965730E+00,-0.64915836E-01,
        -0.27368171E-01, 0.48012275E-01, 0.21887461E-01,-0.36349781E-01,
        -0.12572373E-01,-0.89493226E-02, 0.19980278E-02,-0.84672971E-02,
         0.60883448E-02,-0.25534214E-03,-0.13627320E-02,-0.24680322E-03,
         0.94339013E-03,-0.57691233E-02, 0.17871860E-02,-0.20906245E-02,
        -0.14039769E-02,-0.33767438E-02,-0.31078258E-02,-0.48246928E-02,
        -0.12245108E-02, 0.39759860E-03, 0.15019691E-02, 0.21061657E-02,
         0.10608802E-03, 0.76081377E-03, 0.65304840E-03, 0.42915894E-03,
        -0.50233107E-03,-0.42561516E-02,-0.86567746E-02,-0.61679296E-02,
         0.23610953E-02,-0.57623419E-03, 0.98612264E-03, 0.44072233E-03,
        -0.41847143E-03,-0.16524547E-02,-0.39232959E-03, 0.15260939E-02,
        -0.68909302E-03, 0.36849990E-04,-0.28927499E-04,-0.10271848E-03,
         0.29891892E-03,-0.77077979E-03, 0.12947636E-03,-0.64366689E-03,
        -0.26053158E-03,-0.65189489E-03, 0.37342909E-03, 0.24839786E-04,
        -0.41223444E-04,-0.98751998E-03, 0.45994183E-03, 0.64099731E-04,
         0.64123649E-03, 0.51775679E-03, 0.32060099E-03,-0.59476757E-03,
         0.30139540E-03, 0.26197793E-03, 0.12452183E-02, 0.18211165E-02,
         0.14199706E-03, 0.27230196E-02,-0.13977637E-02, 0.10332217E-02,
         0.25581682E-03,-0.50929561E-03,-0.42625507E-02, 0.56484248E-03,
        -0.31259307E-02,-0.79591252E-03,-0.11226643E-02,-0.80816360E-03,
        -0.80591817E-05,-0.19078707E-04, 0.12589716E-03, 0.12349267E-03,
         0.51663876E-04, 0.36758491E-04, 0.51732590E-04, 0.12977982E-02,
         0.20334409E-02,-0.12731527E-03, 0.69147041E-04, 0.12567047E-02,
         0.46115654E-03,-0.15698315E-03,-0.10218516E-02,-0.19554065E-03,
        -0.64994860E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]        *x34*x41    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]    *x23    *x41*x51
        +coeff[ 37]    *x23*x32*x41*x51
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 44]    *x24    *x43    
        +coeff[ 45]    *x21            
        +coeff[ 46]                *x51
        +coeff[ 47]*x11    *x31    *x51
        +coeff[ 48]        *x32*x41*x51
        +coeff[ 49]        *x31*x44    
        +coeff[ 50]*x11*x21*x31    *x51
        +coeff[ 51]    *x24    *x41    
        +coeff[ 52]            *x41*x53
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x22*x31    *x52
        +coeff[ 55]    *x22            
        +coeff[ 56]*x12        *x41    
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]    *x21    *x43*x51
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]    *x21*x31*x42*x51
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 62]*x11*x21*x32*x41    
        +coeff[ 63]    *x21*x32*x41*x51
        +coeff[ 64]        *x31*x42*x52
        +coeff[ 65]    *x21*x33    *x51
        +coeff[ 66]    *x23    *x43    
        +coeff[ 67]    *x23*x31*x42    
        +coeff[ 68]        *x33    *x52
        +coeff[ 69]    *x23*x32*x41    
        +coeff[ 70]    *x22*x31*x42*x51
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 71]    *x23*x33        
        +coeff[ 72]    *x21*x31    *x53
        +coeff[ 73]    *x24    *x41*x51
        +coeff[ 74]    *x22*x31*x44    
        +coeff[ 75]    *x21*x31*x42*x52
        +coeff[ 76]    *x22*x32*x43    
        +coeff[ 77]    *x24    *x41*x52
        +coeff[ 78]    *x24    *x43*x51
        +coeff[ 79]    *x24    *x45    
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 80]*x11*x21            
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]            *x43*x51
        +coeff[ 83]*x11    *x31*x42    
        +coeff[ 84]*x11*x22*x31        
        +coeff[ 85]        *x33    *x51
        +coeff[ 86]*x11    *x31    *x52
        +coeff[ 87]    *x21*x31*x44    
        +coeff[ 88]    *x21*x32*x43    
    ;
    v_y_e_q3ex_2_100                              =v_y_e_q3ex_2_100                              
        +coeff[ 89]*x11*x22    *x41*x51
        +coeff[ 90]*x11    *x32*x41*x51
        +coeff[ 91]    *x21*x33*x42    
        +coeff[ 92]    *x21*x34*x41    
        +coeff[ 93]*x11    *x33*x42    
        +coeff[ 94]    *x22    *x45    
        +coeff[ 95]    *x21    *x43*x52
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_2_100                              ;
}
float p_e_q3ex_2_100                              (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.2734946E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.25728057E-03,-0.10586278E-04, 0.20155197E-01,-0.28921040E-01,
         0.16305741E-01,-0.86227031E-02,-0.96216975E-02, 0.74655628E-02,
        -0.59368403E-03,-0.18654844E-04, 0.20634144E-03, 0.50497996E-02,
         0.17050289E-02, 0.43083983E-02,-0.51563652E-03, 0.28149385E-02,
         0.13440291E-02, 0.63011999E-03, 0.11755767E-02,-0.17690481E-04,
         0.55424514E-03, 0.17139091E-02,-0.11316248E-02,-0.14596016E-03,
         0.29833853E-03,-0.32705787E-03,-0.12568282E-02,-0.57382649E-03,
        -0.39093778E-03,-0.28578166E-04,-0.17488371E-03,-0.56669372E-03,
         0.65613291E-04,-0.20803636E-03, 0.13315150E-02, 0.99947280E-03,
        -0.27620251E-03,-0.47218878E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_2_100                              =v_p_e_q3ex_2_100                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_2_100                              =v_p_e_q3ex_2_100                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_2_100                              =v_p_e_q3ex_2_100                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]        *x31    *x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_2_100                              =v_p_e_q3ex_2_100                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_2_100                              ;
}
float l_e_q3ex_2_100                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1962605E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.11737905E-01,-0.33591312E+00,-0.25745824E-01, 0.11478811E-01,
        -0.37555452E-01,-0.93166549E-02,-0.39751180E-02, 0.24313000E-02,
        -0.35689960E-02,-0.53638108E-02,-0.57096994E-02, 0.74795559E-02,
         0.86798295E-02,-0.15049937E-03,-0.49467885E-03,-0.10108924E-02,
        -0.17954569E-02, 0.43171253E-02, 0.20544652E-02, 0.80338115E-03,
         0.65381831E-03, 0.81874814E-03, 0.18098004E-02,-0.43043494E-03,
         0.18838678E-02, 0.13843328E-02, 0.81179634E-04, 0.65268432E-02,
         0.44322065E-02, 0.38978797E-02,-0.57690527E-03, 0.63317717E-03,
        -0.14322723E-02,-0.15911172E-02,-0.24466712E-02, 0.13232423E-02,
         0.59088427E-02, 0.21567079E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_2_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q3ex_2_100                              =v_l_e_q3ex_2_100                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_2_100                              =v_l_e_q3ex_2_100                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x32*x42*x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]                *x53
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x22*x31*x41    
        +coeff[ 25]    *x22    *x42    
    ;
    v_l_e_q3ex_2_100                              =v_l_e_q3ex_2_100                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]*x13            *x51
        +coeff[ 31]            *x44*x51
        +coeff[ 32]    *x22        *x53
        +coeff[ 33]*x11*x24            
        +coeff[ 34]    *x21*x31*x42*x52
    ;
    v_l_e_q3ex_2_100                              =v_l_e_q3ex_2_100                              
        +coeff[ 35]    *x21    *x42*x53
        +coeff[ 36]    *x23*x32*x42    
        +coeff[ 37]*x11*x22*x31*x43    
        ;

    return v_l_e_q3ex_2_100                              ;
}
