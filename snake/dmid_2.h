float x_e_dmid_2_400                              (float *x,int m){
    int ncoeff= 25;
    float avdat= -0.3077136E+01;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79951E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.39631E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.68323110E-03, 0.21784587E+00, 0.12104883E-01,-0.11763289E-05,
        -0.29660944E-05,-0.81482772E-02, 0.57987641E-02, 0.71434756E-02,
        -0.12819501E-01,-0.94819460E-02, 0.57590667E-04,-0.28475097E-02,
         0.56556973E-03,-0.53909089E-03, 0.24644515E-03,-0.49539106E-02,
        -0.24225590E-03,-0.70062437E-03, 0.51095622E-03,-0.55208377E-03,
        -0.38650437E-03,-0.22137226E-03,-0.50109002E-03,-0.84959017E-02,
        -0.61604660E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dmid_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x13                
        +coeff[  4]*x12            *x51
        +coeff[  5]*x11                
        +coeff[  6]                *x51
        +coeff[  7]    *x22            
    ;
    v_x_e_dmid_2_400                              =v_x_e_dmid_2_400                              
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32    *x52
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x21            
        +coeff[ 14]    *x23            
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]                *x52
    ;
    v_x_e_dmid_2_400                              =v_x_e_dmid_2_400                              
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]            *x42    
        +coeff[ 20]*x11*x22            
        +coeff[ 21]*x12    *x31*x41    
        +coeff[ 22]        *x31*x43    
        +coeff[ 23]    *x23*x31*x41    
        +coeff[ 24]    *x23    *x42    
        ;

    return v_x_e_dmid_2_400                              ;
}
float t_e_dmid_2_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1732666E+01;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79951E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.39631E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.39336616E-02,-0.17884691E-02,-0.10238276E-01, 0.40759452E-01,
         0.24552852E-01,-0.56768409E-02, 0.73258542E-02,-0.67016981E-05,
        -0.38522757E-02,-0.88903896E-03, 0.20270995E-02,-0.16602487E-02,
        -0.87208214E-03,-0.54222331E-02,-0.42967326E-02,-0.28006244E-03,
         0.51956641E-03,-0.16895704E-02,-0.88873779E-03, 0.28873031E-03,
        -0.22486935E-04,-0.94757968E-04,-0.62436727E-03,-0.24600215E-02,
        -0.15695258E-02, 0.30097182E-03,-0.41191925E-04,-0.60328653E-04,
        -0.70093709E-04, 0.32269982E-04, 0.57748068E-04,-0.10762648E-02,
         0.12818459E-04,-0.72531329E-04, 0.25191079E-04,-0.85392159E-04,
        -0.44770750E-04,-0.45609876E-03,-0.46609872E-03,-0.74086034E-04,
        -0.40227908E-03,-0.31261452E-03,-0.14742437E-04,-0.21426010E-03,
        -0.23162909E-03,-0.11751096E-03, 0.49658206E-05, 0.20468824E-04,
         0.93140945E-04, 0.80633603E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dmid_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x22            
        +coeff[  5]            *x42    
        +coeff[  6]    *x21        *x51
        +coeff[  7]*x13*x21            
    ;
    v_t_e_dmid_2_400                              =v_t_e_dmid_2_400                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]                *x52
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]*x11*x21            
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_t_e_dmid_2_400                              =v_t_e_dmid_2_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x42*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x33*x41*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_dmid_2_400                              =v_t_e_dmid_2_400                              
        +coeff[ 26]    *x22*x33*x41    
        +coeff[ 27]    *x22*x32    *x52
        +coeff[ 28]    *x22*x31*x41*x52
        +coeff[ 29]*x12                
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]            *x41    
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11            *x52
    ;
    v_t_e_dmid_2_400                              =v_t_e_dmid_2_400                              
        +coeff[ 35]*x11*x23            
        +coeff[ 36]*x11*x22        *x51
        +coeff[ 37]    *x21*x31*x41*x51
        +coeff[ 38]    *x21    *x42*x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]    *x23*x31*x41    
        +coeff[ 41]    *x23    *x42    
        +coeff[ 42]*x12*x21*x32    *x51
        +coeff[ 43]    *x23*x32    *x51
    ;
    v_t_e_dmid_2_400                              =v_t_e_dmid_2_400                              
        +coeff[ 44]    *x23*x31*x41*x51
        +coeff[ 45]    *x21*x32    *x53
        +coeff[ 46]        *x31        
        +coeff[ 47]*x11    *x32        
        +coeff[ 48]*x11    *x31*x41    
        +coeff[ 49]*x11        *x42    
        ;

    return v_t_e_dmid_2_400                              ;
}
float y_e_dmid_2_400                              (float *x,int m){
    int ncoeff= 79;
    float avdat= -0.1359911E-02;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79951E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.39631E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 80]={
         0.12121408E-02, 0.15369800E+00, 0.88694096E-02,-0.20040946E-01,
        -0.79698069E-02, 0.26273841E-01, 0.15329545E-01,-0.14328651E-01,
        -0.71959994E-02, 0.72879653E-03, 0.28771890E-03,-0.94903940E-02,
        -0.65872511E-02,-0.15994178E-02, 0.66429790E-03,-0.15964721E-02,
        -0.88838034E-03,-0.16473203E-02,-0.11901723E-02, 0.55195391E-03,
        -0.12928458E-02,-0.32801509E-02,-0.36102892E-02,-0.46742805E-02,
         0.17196045E-02,-0.11799192E-02, 0.28306921E-03, 0.71986939E-03,
        -0.50350861E-03,-0.12294395E-01, 0.66229829E-03,-0.76242868E-04,
         0.18229284E-04,-0.25732430E-04, 0.27267176E-02,-0.70972266E-02,
        -0.78868298E-02,-0.12829242E-02,-0.18327748E-02,-0.26231266E-04,
        -0.89900714E-04,-0.44410648E-04, 0.36383962E-03,-0.46926894E-03,
         0.14976392E-02,-0.31840485E-04, 0.17650098E-02, 0.12698998E-02,
        -0.69747898E-06, 0.37614108E-03,-0.94145864E-04, 0.94000291E-03,
         0.22650910E-03, 0.15717989E-03,-0.19454906E-02, 0.86708162E-04,
        -0.15490148E-02,-0.47010780E-03, 0.89316112E-04, 0.58067840E-03,
         0.71873721E-04, 0.20661083E-03,-0.89901965E-04, 0.93174189E-04,
        -0.18797891E-04, 0.20717871E-04, 0.19172759E-04, 0.21325219E-04,
         0.17809549E-04, 0.24226039E-04,-0.26088421E-04,-0.15318517E-04,
         0.75223616E-05,-0.21148864E-04, 0.37259425E-03, 0.20225551E-03,
         0.10929167E-03, 0.12035913E-03, 0.69532354E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dmid_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]*x11        *x41    
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]        *x33        
        +coeff[ 14]*x11*x21    *x41    
        +coeff[ 15]    *x21    *x41*x51
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]            *x43*x51
        +coeff[ 20]            *x45    
        +coeff[ 21]        *x31*x44    
        +coeff[ 22]        *x32*x43    
        +coeff[ 23]            *x43    
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x23*x31        
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]    *x22*x34*x41    
        +coeff[ 32]    *x21            
        +coeff[ 33]                *x51
        +coeff[ 34]    *x21*x31*x42    
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22*x32*x41    
        +coeff[ 37]    *x24*x31        
        +coeff[ 38]    *x22*x33        
        +coeff[ 39]    *x21    *x45    
        +coeff[ 40]    *x21*x33*x42    
        +coeff[ 41]    *x21*x34*x41    
        +coeff[ 42]    *x24    *x43    
        +coeff[ 43]    *x24    *x45    
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 44]    *x21    *x43    
        +coeff[ 45]*x11        *x41*x51
        +coeff[ 46]    *x21*x32*x41    
        +coeff[ 47]        *x31*x42*x51
        +coeff[ 48]    *x24            
        +coeff[ 49]    *x21*x33        
        +coeff[ 50]*x11*x22    *x41    
        +coeff[ 51]        *x32*x41*x51
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]    *x21*x31    *x52
        +coeff[ 56]    *x24    *x41    
        +coeff[ 57]        *x34*x41    
        +coeff[ 58]            *x41*x53
        +coeff[ 59]*x11*x21*x31*x42    
        +coeff[ 60]        *x31    *x53
        +coeff[ 61]    *x23    *x41*x51
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 62]*x11*x23*x31        
        +coeff[ 63]    *x23*x31    *x51
        +coeff[ 64]*x11*x21    *x43*x51
        +coeff[ 65]*x11*x21    *x45    
        +coeff[ 66]*x11*x21*x34*x41    
        +coeff[ 67]        *x31*x41    
        +coeff[ 68]    *x22            
        +coeff[ 69]            *x44    
        +coeff[ 70]*x12        *x41    
    ;
    v_y_e_dmid_2_400                              =v_y_e_dmid_2_400                              
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]        *x34        
        +coeff[ 73]*x11*x22*x31        
        +coeff[ 74]*x11*x21    *x43    
        +coeff[ 75]*x11*x21*x32*x41    
        +coeff[ 76]    *x22    *x41*x52
        +coeff[ 77]    *x23*x31*x42    
        +coeff[ 78]    *x22*x31    *x52
        ;

    return v_y_e_dmid_2_400                              ;
}
float p_e_dmid_2_400                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.1199591E-03;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79951E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.39631E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.13281353E-03,-0.41727610E-01,-0.10121230E-01,-0.54661189E-02,
        -0.20530861E-01, 0.59824148E-02, 0.10005199E-01,-0.66547794E-02,
         0.30867584E-03,-0.26962918E-02,-0.44465596E-02, 0.75940666E-03,
        -0.14689199E-02, 0.24395484E-03,-0.40904786E-02,-0.22194979E-02,
         0.35792970E-03, 0.12904530E-02,-0.41268283E-03,-0.55419782E-03,
        -0.18579420E-02,-0.32548422E-02,-0.18925269E-02,-0.15192390E-04,
         0.72689878E-03, 0.31758333E-04, 0.50632651E-04, 0.42478565E-04,
         0.24908006E-04,-0.48926129E-03,-0.65019989E-03,-0.26989372E-02,
         0.20933669E-03,-0.29349080E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dmid_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dmid_2_400                              =v_p_e_dmid_2_400                              
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_p_e_dmid_2_400                              =v_p_e_dmid_2_400                              
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22    *x43    
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x25*x31        
        +coeff[ 25]        *x33    *x52
    ;
    v_p_e_dmid_2_400                              =v_p_e_dmid_2_400                              
        +coeff[ 26]        *x32*x41*x52
        +coeff[ 27]        *x31*x42*x52
        +coeff[ 28]            *x43*x52
        +coeff[ 29]    *x24*x33        
        +coeff[ 30]        *x33        
        +coeff[ 31]        *x32*x41    
        +coeff[ 32]*x11*x21*x31        
        +coeff[ 33]    *x22    *x41*x51
        ;

    return v_p_e_dmid_2_400                              ;
}
float l_e_dmid_2_400                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.4848083E-02;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79951E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.39631E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.47691171E-02,-0.11172853E+00, 0.40258113E-02,-0.12546107E-01,
        -0.57176370E-02,-0.10179696E-02,-0.11096729E-01, 0.15089318E-04,
        -0.42893938E-02,-0.11695039E-01, 0.87808224E-03,-0.98355906E-03,
        -0.29505973E-03, 0.56181578E-02, 0.42601870E-02, 0.15585554E-02,
         0.12431517E-02,-0.87110835E-04, 0.23822491E-03, 0.23179338E-02,
         0.63718541E-03, 0.84100681E-03, 0.32229323E-03,-0.44338799E-05,
         0.18839154E-03, 0.42639221E-02, 0.30587709E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dmid_2_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x11                
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]                *x51
        +coeff[  6]            *x42    
        +coeff[  7]        *x33*x41    
    ;
    v_l_e_dmid_2_400                              =v_l_e_dmid_2_400                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]*x11            *x51
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]    *x21    *x42    
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]    *x23*x32        
    ;
    v_l_e_dmid_2_400                              =v_l_e_dmid_2_400                              
        +coeff[ 17]    *x21*x34        
        +coeff[ 18]    *x23*x34        
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x23            
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dmid_2_400                              =v_l_e_dmid_2_400                              
        +coeff[ 26]    *x23    *x42    
        ;

    return v_l_e_dmid_2_400                              ;
}
