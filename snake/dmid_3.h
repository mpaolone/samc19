float x_e_dmid_3_400                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1875742E+01;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.35937342E-02, 0.19943697E-01, 0.26814580E+00, 0.17769216E-01,
         0.13253208E-01, 0.12558558E-04,-0.10541963E-01,-0.99957944E-03,
        -0.16928570E-01,-0.12368505E-01,-0.40215593E-04, 0.78492951E-04,
        -0.36326780E-02, 0.61283825E-03,-0.76603016E-03,-0.20062709E-02,
         0.61040232E-03,-0.64947628E-02, 0.68180769E-03,-0.16408510E-02,
        -0.84617536E-03, 0.95525500E-03, 0.36228274E-04, 0.43530570E-03,
        -0.46479760E-03,-0.50688011E-03,-0.10458823E-01,-0.74659619E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dmid_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]*x13                
        +coeff[  6]*x11                
        +coeff[  7]*x11*x21            
    ;
    v_x_e_dmid_3_400                              =v_x_e_dmid_3_400                              
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x12*x21*x32        
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11            *x51
        +coeff[ 14]                *x52
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x23            
    ;
    v_x_e_dmid_3_400                              =v_x_e_dmid_3_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21    *x41    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]    *x21*x31        
        +coeff[ 24]        *x32        
        +coeff[ 25]*x11*x22            
    ;
    v_x_e_dmid_3_400                              =v_x_e_dmid_3_400                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        ;

    return v_x_e_dmid_3_400                              ;
}
float t_e_dmid_3_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1193758E+01;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35985152E-03,-0.54701958E-01, 0.39213184E-01, 0.11066142E-01,
         0.76309720E-03, 0.13941362E-02,-0.68766926E-03,-0.27894606E-02,
        -0.75707829E-03,-0.20492803E-02,-0.28806916E-03, 0.66855276E-03,
        -0.40268101E-03,-0.59957540E-03,-0.59158198E-03, 0.31732209E-03,
         0.11981061E-03, 0.14075011E-03,-0.40047569E-03, 0.46742472E-04,
         0.14791018E-03, 0.10938288E-04,-0.22522180E-03,-0.78079448E-03,
        -0.42515114E-03, 0.16251954E-02, 0.88657640E-06, 0.10298475E-02,
         0.54256223E-04,-0.54049614E-03,-0.26986375E-03, 0.53926627E-03,
         0.15589054E-04,-0.53986681E-04, 0.94813622E-05, 0.13010735E-04,
         0.10490555E-03,-0.24961977E-03, 0.72218834E-04,-0.10484671E-03,
         0.17614429E-04, 0.19829311E-04,-0.20064597E-03,-0.51814277E-04,
         0.44948451E-04, 0.35736576E-03, 0.31963963E-03,-0.15952514E-03,
        -0.69908645E-04,-0.91552129E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dmid_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x22            
        +coeff[  4]*x11                
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42    
    ;
    v_t_e_dmid_3_400                              =v_t_e_dmid_3_400                              
        +coeff[  8]                *x52
        +coeff[  9]        *x31*x41    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x23            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]            *x41    
    ;
    v_t_e_dmid_3_400                              =v_t_e_dmid_3_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x42*x51
        +coeff[ 19]        *x31        
        +coeff[ 20]*x11*x22            
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_t_e_dmid_3_400                              =v_t_e_dmid_3_400                              
        +coeff[ 26]*x12*x21    *x42    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x23*x32        
        +coeff[ 32]*x12*x21*x32    *x51
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]*x12                
    ;
    v_t_e_dmid_3_400                              =v_t_e_dmid_3_400                              
        +coeff[ 35]            *x41*x51
        +coeff[ 36]*x11    *x31*x41    
        +coeff[ 37]    *x21*x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21    *x42    
        +coeff[ 40]*x11            *x52
        +coeff[ 41]                *x53
        +coeff[ 42]    *x21*x32    *x51
        +coeff[ 43]    *x22        *x52
    ;
    v_t_e_dmid_3_400                              =v_t_e_dmid_3_400                              
        +coeff[ 44]*x13    *x32        
        +coeff[ 45]    *x21*x32*x42    
        +coeff[ 46]    *x21*x31*x43    
        +coeff[ 47]    *x23    *x42*x51
        +coeff[ 48]    *x23        *x53
        +coeff[ 49]    *x21    *x41    
        ;

    return v_t_e_dmid_3_400                              ;
}
float y_e_dmid_3_400                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1117653E-02;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.18583473E-02, 0.14422880E+00,-0.21756846E-01,-0.35292551E-01,
        -0.14143845E-01, 0.33199869E-01, 0.20255312E-01,-0.19577323E-01,
        -0.10206438E-01, 0.72436169E-03,-0.68530947E-03, 0.12785516E-02,
         0.51176548E-03,-0.13068150E-01,-0.89586508E-02,-0.28484005E-02,
        -0.16230653E-02,-0.21456585E-02,-0.15833055E-02,-0.28301580E-04,
         0.40407167E-03,-0.66371043E-02,-0.21002048E-02, 0.99716673E-03,
         0.26506442E-02, 0.54757920E-03,-0.35910857E-05, 0.44223649E-03,
         0.12968412E-02,-0.15594037E-02,-0.68491016E-03, 0.41897647E-03,
         0.18784973E-03,-0.10708454E-01,-0.47554872E-02,-0.82781967E-02,
        -0.18074382E-02,-0.41308380E-04,-0.25903211E-04, 0.57258738E-04,
         0.43617303E-04, 0.25918775E-02, 0.48257289E-02, 0.31220347E-02,
         0.67788031E-03,-0.54522627E-02, 0.27367755E-03,-0.18664795E-02,
        -0.74897329E-02,-0.19771610E-02,-0.14141536E-01,-0.21158185E-04,
         0.33000444E-04,-0.52688039E-04, 0.50461199E-03, 0.61471196E-03,
         0.16133714E-02, 0.19289595E-03,-0.13030221E-03, 0.12096023E-02,
         0.29515056E-03,-0.16760967E-04, 0.14193832E-04, 0.16032766E-03,
        -0.15073016E-02, 0.21214129E-04, 0.26623131E-03, 0.96434842E-05,
        -0.15916079E-01,-0.11820919E-04,-0.33654844E-04,-0.54551742E-05,
        -0.20279757E-03,-0.51384963E-04,-0.65939275E-04,-0.66036970E-04,
        -0.33789496E-04,-0.21159842E-04, 0.68123144E-03, 0.72832183E-04,
         0.59243431E-03, 0.10139240E-03,-0.11772173E-03, 0.11137449E-03,
         0.14373273E-03, 0.86507775E-04, 0.97543852E-04,-0.46002267E-04,
         0.68717032E-04,-0.30481233E-03,-0.17689585E-03,-0.41231371E-04,
        -0.23758292E-04, 0.28764871E-04,-0.30646173E-04, 0.29469395E-03,
         0.12037998E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dmid_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21            
        +coeff[ 10]                *x51
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x31    *x52
        +coeff[ 19]            *x45    
        +coeff[ 20]    *x22            
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]        *x31*x41    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 26]            *x44    
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]    *x22*x31    *x51
        +coeff[ 31]            *x42    
        +coeff[ 32]        *x32        
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 35]    *x22*x33*x42    
        +coeff[ 36]    *x22*x34*x41    
        +coeff[ 37]    *x24*x33        
        +coeff[ 38]*x11                
        +coeff[ 39]    *x21        *x51
        +coeff[ 40]                *x52
        +coeff[ 41]    *x21    *x43    
        +coeff[ 42]    *x21*x31*x42    
        +coeff[ 43]    *x21*x32*x41    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 44]    *x21*x33        
        +coeff[ 45]    *x22    *x43    
        +coeff[ 46]    *x21    *x41*x52
        +coeff[ 47]    *x24    *x41    
        +coeff[ 48]    *x22*x32*x41    
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]    *x22*x31*x44    
        +coeff[ 51]*x11*x21            
        +coeff[ 52]    *x22        *x51
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 53]*x11        *x41*x51
        +coeff[ 54]    *x22    *x42    
        +coeff[ 55]    *x22*x31*x41    
        +coeff[ 56]        *x31*x42*x51
        +coeff[ 57]    *x22*x32        
        +coeff[ 58]*x11*x22    *x41    
        +coeff[ 59]        *x32*x41*x51
        +coeff[ 60]        *x33    *x51
        +coeff[ 61]    *x23    *x42    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 62]    *x21*x32*x42    
        +coeff[ 63]    *x21*x31    *x52
        +coeff[ 64]    *x24*x31        
        +coeff[ 65]            *x43*x52
        +coeff[ 66]    *x23    *x41*x51
        +coeff[ 67]            *x45*x51
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]            *x43*x53
        +coeff[ 70]        *x31*x42*x53
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 71]            *x45*x53
        +coeff[ 72]    *x21*x31*x41    
        +coeff[ 73]    *x23            
        +coeff[ 74]    *x21*x32        
        +coeff[ 75]        *x31*x41*x51
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]            *x43*x51
        +coeff[ 79]    *x21    *x44    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 80]*x11*x21*x31*x42    
        +coeff[ 81]        *x31    *x53
        +coeff[ 82]*x11*x23*x31        
        +coeff[ 83]    *x23*x31    *x51
        +coeff[ 84]    *x22    *x41*x52
        +coeff[ 85]    *x22*x31    *x52
        +coeff[ 86]    *x21*x31*x45    
        +coeff[ 87]*x11*x21    *x43*x51
        +coeff[ 88]*x11*x21    *x45    
    ;
    v_y_e_dmid_3_400                              =v_y_e_dmid_3_400                              
        +coeff[ 89]*x11*x21*x33*x42    
        +coeff[ 90]    *x21    *x42    
        +coeff[ 91]            *x42*x51
        +coeff[ 92]        *x32    *x51
        +coeff[ 93]    *x24            
        +coeff[ 94]*x11*x22*x31        
        +coeff[ 95]*x11*x21    *x43    
        +coeff[ 96]            *x41*x53
        ;

    return v_y_e_dmid_3_400                              ;
}
float p_e_dmid_3_400                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.6481507E-04;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.21313375E-03,-0.32575052E-01,-0.61391466E-02,-0.36407895E-02,
        -0.15633496E-01, 0.47924914E-02, 0.80078123E-02, 0.31148503E-03,
        -0.52059209E-02,-0.21948379E-02, 0.18730037E-04,-0.16464823E-03,
        -0.33598030E-02, 0.56505454E-03,-0.11717951E-02, 0.15398314E-03,
        -0.36035895E-02,-0.19379093E-02,-0.55544493E-04, 0.16355563E-04,
         0.18487504E-04, 0.96539479E-04,-0.54342649E-03,-0.23287581E-02,
         0.24952760E-03, 0.11302561E-02,-0.31885816E-03,-0.41647381E-03,
         0.65920065E-03, 0.43982567E-04, 0.14876951E-03,-0.20961375E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x35 = x34*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_p_e_dmid_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x21            
    ;
    v_p_e_dmid_3_400                              =v_p_e_dmid_3_400                              
        +coeff[  8]    *x22    *x41    
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]                *x51
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x31*x42    
    ;
    v_p_e_dmid_3_400                              =v_p_e_dmid_3_400                              
        +coeff[ 17]            *x43    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x35    *x52
        +coeff[ 20]        *x32*x41*x54
        +coeff[ 21]    *x22            
        +coeff[ 22]        *x33        
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23    *x41    
    ;
    v_p_e_dmid_3_400                              =v_p_e_dmid_3_400                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x25*x31        
        +coeff[ 29]    *x21        *x51
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x22    *x41*x51
        ;

    return v_p_e_dmid_3_400                              ;
}
float l_e_dmid_3_400                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.7020498E-02;
    float xmin[10]={
        -0.39990E-02,-0.59288E-01,-0.79939E-01,-0.39708E-01,-0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39990E-02, 0.59327E-01, 0.79951E-01, 0.38104E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.69234055E-02,-0.18657932E+00,-0.46084104E-02, 0.68983422E-02,
        -0.15895968E-01,-0.99875471E-02,-0.10419012E-01, 0.11236644E-02,
         0.22575681E-04, 0.47799971E-03, 0.32506473E-03,-0.46075010E-02,
        -0.11575361E-01, 0.11104738E-02, 0.96473824E-02, 0.73685907E-02,
         0.24507511E-02,-0.56082339E-04, 0.42719237E-03,-0.47734784E-03,
         0.39071511E-02,-0.12455630E-02, 0.23052556E-03,-0.62904764E-04,
         0.17444290E-02, 0.51063806E-03, 0.47467515E-05, 0.22081729E-04,
        -0.37881438E-03, 0.73753315E-03, 0.89773419E-03, 0.31178675E-03,
        -0.41817647E-03, 0.70770187E-02, 0.48754686E-02, 0.12446715E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dmid_3_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]    *x22*x31*x41    
    ;
    v_l_e_dmid_3_400                              =v_l_e_dmid_3_400                              
        +coeff[  8]        *x31*x43    
        +coeff[  9]    *x22*x34        
        +coeff[ 10]    *x22*x31*x43    
        +coeff[ 11]        *x32        
        +coeff[ 12]        *x31*x41    
        +coeff[ 13]*x11*x21            
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x21    *x42    
        +coeff[ 16]    *x23*x32        
    ;
    v_l_e_dmid_3_400                              =v_l_e_dmid_3_400                              
        +coeff[ 17]    *x21*x34        
        +coeff[ 18]            *x41    
        +coeff[ 19]*x11            *x51
        +coeff[ 20]    *x21*x32        
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]        *x31        
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x21        *x52
    ;
    v_l_e_dmid_3_400                              =v_l_e_dmid_3_400                              
        +coeff[ 26]        *x34    *x51
        +coeff[ 27]            *x44*x51
        +coeff[ 28]    *x21    *x41    
        +coeff[ 29]        *x32    *x51
        +coeff[ 30]            *x42*x51
        +coeff[ 31]*x11*x22            
        +coeff[ 32]    *x23*x31        
        +coeff[ 33]    *x23*x31*x41    
        +coeff[ 34]    *x23    *x42    
    ;
    v_l_e_dmid_3_400                              =v_l_e_dmid_3_400                              
        +coeff[ 35]    *x24    *x42    
        ;

    return v_l_e_dmid_3_400                              ;
}
