float x_e_q3ex_1_1200                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.9800682E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.10171688E-01, 0.20490529E+00, 0.85638635E-01,-0.99789603E-02,
         0.24830746E-01, 0.87878080E-02,-0.89234254E-05,-0.73016360E-02,
        -0.50470200E-02,-0.42585363E-02,-0.98774238E-02,-0.64289155E-02,
        -0.31378638E-03,-0.88748627E-03, 0.48044931E-05, 0.14245738E-02,
        -0.34737224E-02, 0.33661823E-02,-0.24966086E-03,-0.22387030E-03,
         0.78407879E-03,-0.26837812E-03,-0.28259010E-03, 0.51877595E-03,
        -0.67315188E-04, 0.42726297E-03, 0.80792874E-03,-0.72859076E-03,
         0.51443954E-03, 0.17485396E-02,-0.25395541E-02,-0.16747451E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_1200                              =v_x_e_q3ex_1_1200                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_1200                              =v_x_e_q3ex_1_1200                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x23            
    ;
    v_x_e_q3ex_1_1200                              =v_x_e_q3ex_1_1200                              
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_1200                              ;
}
float t_e_q3ex_1_1200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6592905E-02;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62354407E-02,-0.29083695E-01, 0.75932235E-01, 0.33499035E-02,
        -0.57142447E-02,-0.21137707E-02,-0.23510680E-02,-0.30910663E-03,
        -0.97493111E-03,-0.10343837E-02, 0.94973505E-03, 0.24886447E-03,
         0.25935259E-03, 0.46804818E-03, 0.15507453E-02, 0.40264276E-03,
        -0.25087141E-03,-0.94675495E-04,-0.35777473E-03, 0.45579469E-04,
         0.46376506E-03,-0.63463504E-03, 0.21389590E-03, 0.45899642E-03,
        -0.66203059E-03,-0.37896220E-03,-0.17422304E-03, 0.59888390E-03,
        -0.65398932E-03, 0.65858866E-03,-0.22489112E-03, 0.77183795E-04,
        -0.58809841E-04, 0.41515166E-04, 0.96941883E-04,-0.11597772E-03,
         0.14947663E-03, 0.40271779E-03, 0.14772605E-03,-0.22216406E-03,
         0.17540752E-03,-0.15131570E-04,-0.91115486E-04,-0.25222991E-04,
         0.19054180E-03, 0.15343816E-03, 0.32744934E-04,-0.21774726E-03,
         0.95483549E-04,-0.14533233E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_1200                              =v_t_e_q3ex_1_1200                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_1200                              =v_t_e_q3ex_1_1200                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_1200                              =v_t_e_q3ex_1_1200                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x22*x31*x41*x52
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]    *x21*x31*x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]        *x32    *x51
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_q3ex_1_1200                              =v_t_e_q3ex_1_1200                              
        +coeff[ 35]*x11*x21    *x42    
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]    *x21*x32    *x52
        +coeff[ 40]    *x22        *x53
        +coeff[ 41]*x12                
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11            *x52
    ;
    v_t_e_q3ex_1_1200                              =v_t_e_q3ex_1_1200                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x23*x31*x41    
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x21*x32*x42    
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_q3ex_1_1200                              ;
}
float y_e_q3ex_1_1200                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.3186549E-03;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.27226759E-03, 0.97776555E-01,-0.93930878E-01,-0.68741694E-01,
        -0.26456181E-01, 0.53616457E-01, 0.24350289E-01,-0.36705129E-01,
        -0.13089042E-01,-0.91571948E-02, 0.24940642E-02,-0.93999300E-02,
         0.58517596E-02,-0.60975982E-03,-0.16898579E-02, 0.10679604E-02,
        -0.55234823E-02, 0.21604579E-02,-0.23663128E-02,-0.20153027E-02,
        -0.35475984E-02,-0.23639970E-02, 0.46574682E-04,-0.54590381E-02,
        -0.11629653E-02, 0.50552079E-03, 0.44184265E-03, 0.14380291E-02,
        -0.80559740E-03, 0.29864753E-03, 0.12495674E-02, 0.25244360E-03,
        -0.65117409E-02,-0.95445523E-02,-0.63389200E-02,-0.17236575E-02,
         0.20410114E-02, 0.94899989E-03,-0.60551497E-03, 0.43042732E-03,
        -0.26859620E-03,-0.13463792E-02,-0.99453365E-03, 0.82140032E-03,
         0.36004294E-04,-0.29043758E-04,-0.70656075E-04, 0.37989399E-03,
        -0.11834049E-02, 0.38687084E-03,-0.14230468E-03,-0.42167239E-03,
         0.21997001E-04,-0.58703052E-04, 0.21275159E-03, 0.11768899E-03,
        -0.10608304E-04,-0.80240471E-03, 0.10588082E-03,-0.24870102E-03,
         0.65467955E-03, 0.24203502E-02,-0.12309494E-03, 0.15476767E-03,
         0.28252134E-02,-0.28823857E-03,-0.63425180E-03, 0.38549051E-03,
         0.12393227E-03, 0.36684348E-03, 0.98811928E-03, 0.27997501E-03,
        -0.76358527E-04,-0.12866968E-05, 0.17108803E-04,-0.41627205E-04,
         0.16098478E-02,-0.10453361E-03, 0.19438090E-03, 0.36254106E-02,
         0.89590416E-04, 0.48457845E-02, 0.23639559E-03, 0.31053878E-02,
         0.28021273E-02,-0.66727334E-04,-0.34303550E-03, 0.20497822E-03,
         0.83098834E-03, 0.14534098E-03, 0.19602178E-03,-0.76002337E-03,
         0.72461995E-03, 0.10255392E-03, 0.15992559E-03,-0.14354559E-02,
        -0.28055432E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33    *x52
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 35]    *x24*x31        
        +coeff[ 36]    *x23    *x41*x51
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24    *x41    
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]    *x21*x31*x42*x51
        +coeff[ 50]        *x31    *x53
        +coeff[ 51]    *x21*x32*x41*x51
        +coeff[ 52]    *x22            
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 53]*x12        *x41    
        +coeff[ 54]            *x43*x51
        +coeff[ 55]        *x33    *x51
        +coeff[ 56]*x11*x21    *x41*x51
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21*x31    *x51
        +coeff[ 59]        *x34*x41    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 62]*x11*x22    *x41*x51
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]*x11*x21    *x45    
        +coeff[ 68]    *x22*x33*x42    
        +coeff[ 69]*x11*x21*x32*x43    
        +coeff[ 70]    *x23*x31*x44    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 71]    *x21*x33*x44    
        +coeff[ 72]    *x23*x33*x42    
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x11    *x31*x42    
        +coeff[ 75]            *x41*x53
        +coeff[ 76]    *x21    *x45    
        +coeff[ 77]            *x43*x52
        +coeff[ 78]*x11*x21*x32*x41    
        +coeff[ 79]    *x21*x31*x44    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 80]    *x21*x33    *x51
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]        *x31*x44*x51
        +coeff[ 83]    *x23*x31*x42    
        +coeff[ 84]    *x21*x33*x42    
        +coeff[ 85]*x12*x22*x31        
        +coeff[ 86]    *x22    *x43*x51
        +coeff[ 87]        *x32*x43*x51
        +coeff[ 88]    *x21*x34*x41    
    ;
    v_y_e_q3ex_1_1200                              =v_y_e_q3ex_1_1200                              
        +coeff[ 89]    *x21    *x41*x53
        +coeff[ 90]*x11*x22*x31*x42    
        +coeff[ 91]    *x22*x31*x42*x51
        +coeff[ 92]    *x23*x33        
        +coeff[ 93]    *x21*x31    *x53
        +coeff[ 94]*x11*x24    *x41    
        +coeff[ 95]    *x22*x31*x44    
        +coeff[ 96]    *x24*x31    *x51
        ;

    return v_y_e_q3ex_1_1200                              ;
}
float p_e_q3ex_1_1200                              (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.6773190E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.52278134E-04, 0.15624677E-01,-0.33866040E-01, 0.16226392E-01,
        -0.78584170E-02,-0.97310273E-02, 0.73597739E-02,-0.54131815E-03,
        -0.45268080E-04, 0.15588902E-03,-0.38699083E-04, 0.49737287E-02,
         0.20796289E-02, 0.40535834E-02,-0.60385402E-03, 0.30101887E-02,
         0.14522987E-02, 0.10500132E-02, 0.70754561E-03, 0.44095203E-04,
        -0.17331903E-03, 0.46308723E-03, 0.14690865E-02,-0.11245881E-02,
        -0.33048552E-03,-0.15592906E-02,-0.68555179E-03,-0.53416262E-03,
        -0.53749589E-03, 0.47780004E-05,-0.20316288E-03,-0.64566592E-03,
        -0.69122791E-04, 0.16389014E-03, 0.11103399E-03,-0.19360257E-03,
         0.11126070E-02, 0.90088317E-03,-0.30915532E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_1200                              =v_p_e_q3ex_1_1200                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_1200                              =v_p_e_q3ex_1_1200                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_1200                              =v_p_e_q3ex_1_1200                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_1200                              =v_p_e_q3ex_1_1200                              
        +coeff[ 35]            *x43*x51
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_1200                              ;
}
float l_e_q3ex_1_1200                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1493351E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.14766037E-01,-0.31158462E+00,-0.25124257E-01, 0.12408130E-01,
        -0.32900322E-01,-0.11705351E-01, 0.21713062E-02,-0.32918717E-03,
        -0.28671843E-02,-0.54592076E-02,-0.58500427E-02,-0.23733196E-02,
         0.70993067E-02, 0.75273919E-02, 0.19318712E-03,-0.76513196E-03,
         0.37597623E-02,-0.21607166E-02, 0.21076857E-02, 0.80855930E-03,
        -0.35996622E-03, 0.48247518E-03, 0.42250360E-03,-0.38798747E-03,
         0.27862930E-03, 0.18472505E-02, 0.12601692E-02,-0.80857443E-03,
        -0.76899707E-03, 0.72433683E-02, 0.37829387E-02, 0.58558430E-02,
         0.29309427E-02,-0.15196402E-02, 0.54556690E-03, 0.75410138E-03,
         0.99915243E-03, 0.57729743E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_1200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_1200                              =v_l_e_q3ex_1_1200                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_1_1200                              =v_l_e_q3ex_1_1200                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x32    *x53
        +coeff[ 20]    *x21    *x41*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11    *x31*x41    
        +coeff[ 23]*x11*x21        *x51
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_q3ex_1_1200                              =v_l_e_q3ex_1_1200                              
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21        *x53
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x21    *x44    
        +coeff[ 33]        *x33*x41*x51
        +coeff[ 34]    *x22*x31    *x52
    ;
    v_l_e_q3ex_1_1200                              =v_l_e_q3ex_1_1200                              
        +coeff[ 35]    *x24*x32        
        +coeff[ 36]*x11*x23*x32        
        +coeff[ 37]    *x23*x32*x42    
        ;

    return v_l_e_q3ex_1_1200                              ;
}
float x_e_q3ex_1_1100                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.8275431E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.89375088E-02,-0.73016472E-02, 0.20490195E+00, 0.85761644E-01,
        -0.99727539E-02, 0.24870381E-01, 0.88237515E-02,-0.49934965E-02,
        -0.42322953E-02,-0.98729497E-02,-0.64652911E-02,-0.30365167E-03,
        -0.87353389E-03, 0.21834270E-04, 0.15292850E-02,-0.34927407E-02,
         0.32992640E-02,-0.23616248E-03, 0.77346480E-03,-0.25951644E-03,
        -0.20103992E-03,-0.28843043E-03, 0.53483754E-03, 0.42481290E-03,
         0.79468347E-03,-0.78383222E-03, 0.50804456E-03, 0.15551902E-02,
        -0.24517248E-02,-0.16913005E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_1100                              =v_x_e_q3ex_1_1100                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_1_1100                              =v_x_e_q3ex_1_1100                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]*x11*x21            
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_1_1100                              =v_x_e_q3ex_1_1100                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_1100                              ;
}
float t_e_q3ex_1_1100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6022762E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.58015129E-02,-0.29076124E-01, 0.75922787E-01, 0.33366326E-02,
        -0.57128053E-02,-0.20599295E-02,-0.23220123E-02,-0.30590038E-03,
        -0.97322185E-03,-0.10405929E-02, 0.95711194E-03, 0.22721710E-03,
         0.26246250E-03, 0.47100411E-03, 0.15576371E-02, 0.37839057E-03,
        -0.24727831E-03,-0.10628153E-03,-0.35171708E-03, 0.14383337E-03,
         0.21230815E-03, 0.45807942E-03,-0.64960757E-03, 0.32470940E-03,
        -0.61065832E-03,-0.34808237E-03,-0.10817246E-03, 0.54468442E-03,
        -0.22879058E-03, 0.77955643E-04,-0.63120875E-04, 0.73338393E-04,
        -0.34695244E-04, 0.13212266E-03, 0.59149192E-04,-0.18225746E-03,
         0.14865820E-03, 0.36537880E-03,-0.22302550E-03, 0.52614365E-03,
        -0.14082498E-04,-0.70458787E-04,-0.68682748E-04, 0.46640565E-04,
         0.15826707E-03, 0.22504119E-03,-0.20668205E-03, 0.10621908E-03,
        -0.14156125E-03, 0.10507581E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_1100                              =v_t_e_q3ex_1_1100                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_1100                              =v_t_e_q3ex_1_1100                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22*x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_1100                              =v_t_e_q3ex_1_1100                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]*x11            *x52
        +coeff[ 33]*x11*x23            
        +coeff[ 34]*x11*x21*x31*x41    
    ;
    v_t_e_q3ex_1_1100                              =v_t_e_q3ex_1_1100                              
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]            *x42*x52
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]*x12                
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]*x11*x21    *x42    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_q3ex_1_1100                              =v_t_e_q3ex_1_1100                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x23*x31*x41    
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x31*x41*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_q3ex_1_1100                              ;
}
float y_e_q3ex_1_1100                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9534678E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.83944760E-03, 0.97445264E-01,-0.94178565E-01,-0.69020949E-01,
        -0.26674332E-01, 0.53553384E-01, 0.24364777E-01,-0.36905713E-01,
        -0.13072971E-01,-0.92101768E-02, 0.24666067E-02,-0.92396792E-02,
        -0.53141994E-03,-0.13587187E-02,-0.13533370E-03, 0.10500902E-02,
        -0.57069585E-02, 0.20724062E-02,-0.22971476E-02,-0.20213649E-02,
         0.62153004E-02,-0.33135959E-02,-0.23540172E-02,-0.56031947E-02,
        -0.11254444E-02, 0.50678663E-03,-0.80273562E-03, 0.20052614E-02,
         0.43745255E-02, 0.18648822E-02, 0.14755580E-02, 0.57110901E-03,
        -0.62692226E-02,-0.10395963E-01,-0.61224084E-02, 0.21258993E-02,
         0.74250907E-04, 0.63244588E-04,-0.53179578E-04, 0.10317804E-02,
        -0.47130804E-03, 0.43946810E-03,-0.27968845E-03,-0.12379063E-02,
        -0.17427249E-02, 0.85882226E-03, 0.43176195E-04,-0.62251187E-04,
         0.48687586E-03,-0.14778731E-02, 0.11593958E-03, 0.21603788E-03,
        -0.13861511E-03,-0.45182381E-03,-0.65321190E-04, 0.28110886E-03,
         0.11936181E-03,-0.10367549E-02, 0.65698859E-03,-0.10655484E-02,
         0.13472940E-02,-0.18221921E-03, 0.19146972E-03, 0.18345283E-02,
         0.19078473E-03,-0.25968096E-03,-0.71575359E-03, 0.41897350E-04,
         0.54532796E-03,-0.48052494E-04, 0.87909903E-05, 0.93209437E-05,
        -0.26548716E-04, 0.58781559E-04, 0.35464065E-04, 0.45587221E-04,
         0.44842644E-04, 0.44202400E-03, 0.32790253E-03, 0.27939644E-04,
        -0.82484461E-04, 0.33003694E-03,-0.87226959E-04, 0.15377380E-03,
         0.88948791E-03, 0.18479103E-02,-0.62575337E-03,-0.62431027E-04,
        -0.85343921E-03, 0.41209694E-03, 0.87365894E-04,-0.41280096E-03,
         0.43291232E-03,-0.10529786E-03,-0.30493364E-03,-0.59346115E-03,
        -0.91197406E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]        *x34*x41    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21            
        +coeff[ 38]                *x51
        +coeff[ 39]        *x31*x42*x51
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 44]    *x24*x31        
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]    *x22            
        +coeff[ 47]*x11    *x31    *x51
        +coeff[ 48]        *x32*x41*x51
        +coeff[ 49]        *x31*x44    
        +coeff[ 50]*x11*x21*x31    *x51
        +coeff[ 51]    *x21*x31*x42*x51
        +coeff[ 52]        *x31    *x53
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 53]    *x21*x32*x41*x51
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]            *x43*x51
        +coeff[ 56]        *x33    *x51
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]    *x22*x33        
        +coeff[ 60]    *x23    *x43    
        +coeff[ 61]*x11*x22    *x41*x51
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 62]    *x22*x31    *x52
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x21    *x41*x53
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]*x11*x21    *x45    
        +coeff[ 68]    *x22*x33*x42    
        +coeff[ 69]*x11*x21*x34*x41    
        +coeff[ 70]        *x31*x41    
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 71]    *x21        *x51
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11        *x42*x51
        +coeff[ 74]            *x42*x52
        +coeff[ 75]*x11*x22*x31        
        +coeff[ 76]        *x31*x41*x52
        +coeff[ 77]*x11*x21    *x43    
        +coeff[ 78]    *x21    *x45    
        +coeff[ 79]*x11    *x31    *x52
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 80]            *x43*x52
        +coeff[ 81]*x11*x21*x32*x41    
        +coeff[ 82]*x11    *x31*x42*x51
        +coeff[ 83]        *x31*x42*x52
        +coeff[ 84]    *x21*x32*x43    
        +coeff[ 85]    *x23*x31*x42    
        +coeff[ 86]    *x22    *x43*x51
        +coeff[ 87]*x11*x21    *x41*x52
        +coeff[ 88]    *x22*x31*x42*x51
    ;
    v_y_e_q3ex_1_1100                              =v_y_e_q3ex_1_1100                              
        +coeff[ 89]    *x23*x33        
        +coeff[ 90]    *x21*x31    *x53
        +coeff[ 91]    *x22*x32*x41*x51
        +coeff[ 92]        *x33*x44    
        +coeff[ 93]*x11*x22    *x42*x51
        +coeff[ 94]    *x24*x31    *x51
        +coeff[ 95]    *x22*x32*x43    
        +coeff[ 96]            *x43*x53
        ;

    return v_y_e_q3ex_1_1100                              ;
}
float p_e_q3ex_1_1100                              (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.2148448E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.18079336E-03, 0.15679432E-01,-0.33781469E-01, 0.15957110E-01,
        -0.78900671E-02,-0.97585237E-02, 0.73752026E-02,-0.43583993E-03,
         0.98545526E-04, 0.13636742E-03,-0.23666307E-04, 0.48073730E-02,
         0.20799586E-02, 0.40669818E-02,-0.60003903E-03, 0.30118397E-02,
         0.14644088E-02, 0.10950390E-02, 0.72576571E-03,-0.10134201E-04,
        -0.17081517E-03, 0.46093753E-03, 0.15019720E-02,-0.99340826E-03,
        -0.33549702E-03,-0.10200868E-02,-0.50091743E-03,-0.50480536E-03,
         0.53122430E-05,-0.20989716E-03,-0.39133575E-03,-0.71174261E-04,
         0.20078418E-03, 0.11646113E-03,-0.19787415E-03, 0.11816530E-02,
         0.89574483E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_1100                              =v_p_e_q3ex_1_1100                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_1100                              =v_p_e_q3ex_1_1100                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_1100                              =v_p_e_q3ex_1_1100                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]            *x43*x51
    ;
    v_p_e_q3ex_1_1100                              =v_p_e_q3ex_1_1100                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_1100                              ;
}
float l_e_q3ex_1_1100                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1516100E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.15142914E-01,-0.31135049E+00,-0.25221249E-01, 0.12213612E-01,
        -0.33367053E-01,-0.12039743E-01,-0.55325818E-02,-0.27099289E-02,
        -0.49577989E-02,-0.22283092E-02, 0.23721738E-02, 0.85780928E-02,
         0.87116957E-02, 0.24771327E-02,-0.10251552E-02, 0.26932561E-02,
        -0.18400424E-02, 0.17246674E-02, 0.10283446E-02, 0.79593137E-02,
         0.56263865E-02,-0.19250206E-03,-0.12171770E-02, 0.41925369E-03,
         0.39693312E-03, 0.55667269E-03, 0.96794445E-03,-0.56327292E-03,
        -0.12331586E-02,-0.85899915E-03, 0.57993794E-03,-0.13809555E-02,
        -0.71775471E-03,-0.99611224E-03, 0.69176301E-03, 0.93116832E-03,
        -0.86759822E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_1100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_1_1100                              =v_l_e_q3ex_1_1100                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_1_1100                              =v_l_e_q3ex_1_1100                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x12                
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x31*x42    
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x21        *x52
    ;
    v_l_e_q3ex_1_1100                              =v_l_e_q3ex_1_1100                              
        +coeff[ 26]    *x24            
        +coeff[ 27]    *x23        *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]            *x42*x52
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]    *x23*x32    *x51
        +coeff[ 32]*x11*x24*x31        
        +coeff[ 33]*x11*x23*x32        
        +coeff[ 34]*x11    *x34    *x51
    ;
    v_l_e_q3ex_1_1100                              =v_l_e_q3ex_1_1100                              
        +coeff[ 35]*x11        *x43*x53
        +coeff[ 36]*x12*x22*x33        
        ;

    return v_l_e_q3ex_1_1100                              ;
}
float x_e_q3ex_1_1000                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1016508E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.11121925E-01,-0.73064221E-02, 0.20487256E+00, 0.85864268E-01,
        -0.99648638E-02, 0.24850542E-01, 0.88363886E-02,-0.50047785E-02,
        -0.42482191E-02,-0.98694814E-02,-0.64570294E-02,-0.17622126E-03,
        -0.86085865E-03,-0.15786831E-04, 0.14705866E-02,-0.35393226E-02,
         0.32841617E-02,-0.23270707E-03,-0.20948477E-03, 0.84122631E-03,
        -0.27273796E-03, 0.81942836E-03,-0.27557556E-03, 0.51963731E-03,
         0.41885133E-03,-0.78104832E-03, 0.46243501E-03,-0.11684307E-03,
         0.16573627E-02,-0.24419732E-02,-0.16318178E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_1000                              =v_x_e_q3ex_1_1000                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_1_1000                              =v_x_e_q3ex_1_1000                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_1_1000                              =v_x_e_q3ex_1_1000                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]            *x42*x53
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_1000                              ;
}
float t_e_q3ex_1_1000                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6642242E-02;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.65194024E-02,-0.29053086E-01, 0.75927593E-01, 0.33333926E-02,
        -0.57039666E-02,-0.21121367E-02,-0.23265751E-02,-0.31175188E-03,
        -0.95799763E-03,-0.10555525E-02, 0.95206901E-03, 0.25969965E-03,
         0.26845891E-03, 0.44112484E-03, 0.15376403E-02, 0.39796322E-03,
        -0.26026150E-03,-0.83247534E-04,-0.36307349E-03, 0.76754091E-04,
         0.21872949E-03,-0.64618536E-03, 0.45687583E-03, 0.53474016E-03,
        -0.61462057E-03,-0.35521013E-03,-0.11794196E-03, 0.55541506E-03,
        -0.67366171E-03,-0.20659005E-03, 0.84933752E-04,-0.59913804E-04,
         0.26598351E-04, 0.10007100E-03,-0.11011923E-03, 0.13680836E-03,
         0.36733152E-03,-0.20814533E-03, 0.18218945E-03, 0.55744947E-03,
        -0.35079876E-04,-0.28173192E-04,-0.29716555E-04,-0.43169013E-04,
         0.10995685E-03,-0.36388894E-04, 0.16744446E-03, 0.14607359E-03,
        -0.20896265E-03, 0.10090242E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_1000                              =v_t_e_q3ex_1_1000                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_1000                              =v_t_e_q3ex_1_1000                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_1000                              =v_t_e_q3ex_1_1000                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x22*x31*x41*x52
        +coeff[ 29]    *x21*x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]        *x32    *x51
        +coeff[ 33]*x11*x23            
        +coeff[ 34]*x11*x21    *x42    
    ;
    v_t_e_q3ex_1_1000                              =v_t_e_q3ex_1_1000                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x22        *x53
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x12*x22            
        +coeff[ 43]*x11*x21*x32        
    ;
    v_t_e_q3ex_1_1000                              =v_t_e_q3ex_1_1000                              
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]*x11    *x32    *x51
        +coeff[ 46]    *x23*x32        
        +coeff[ 47]    *x23*x31*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_q3ex_1_1000                              ;
}
float y_e_q3ex_1_1000                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6386288E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.58716413E-03, 0.96976794E-01,-0.94457015E-01,-0.68624169E-01,
        -0.26392424E-01, 0.53572640E-01, 0.24380907E-01,-0.37007648E-01,
        -0.13152773E-01,-0.91566984E-02, 0.24848164E-02,-0.93694068E-02,
        -0.22861380E-02,-0.22930853E-03,-0.11388824E-02, 0.10613656E-02,
        -0.56147417E-02, 0.20223025E-02,-0.19902342E-02, 0.59146509E-02,
        -0.35331941E-02,-0.25993390E-02, 0.20218713E-05,-0.58356440E-02,
        -0.11277931E-02, 0.51324768E-03, 0.24297573E-03, 0.10044230E-02,
        -0.77597128E-03, 0.33848642E-03, 0.12013833E-02, 0.24275624E-03,
        -0.48938007E-02,-0.86946310E-02,-0.56347158E-02, 0.20738810E-02,
         0.10318238E-02,-0.49519842E-03, 0.32409900E-03,-0.29991631E-03,
        -0.16672126E-02,-0.98849030E-03, 0.74679230E-03, 0.23833095E-03,
         0.31099938E-04,-0.27587215E-04,-0.75681775E-04, 0.45198496E-03,
        -0.94268640E-03, 0.12322003E-03,-0.12481848E-02, 0.23710530E-03,
        -0.12288039E-03,-0.54815109E-03, 0.21635569E-04,-0.63146115E-04,
        -0.59858839E-05, 0.37004892E-03, 0.12065900E-03,-0.97660103E-03,
         0.53719297E-03,-0.28026133E-03, 0.67484262E-03, 0.34413429E-03,
         0.24166661E-02,-0.16107254E-03, 0.14772188E-03, 0.26867162E-02,
         0.18909569E-03,-0.58930024E-03,-0.41840980E-02,-0.31904660E-02,
        -0.19924602E-03,-0.12630396E-03,-0.32379277E-03,-0.40588732E-03,
        -0.21585821E-03,-0.68466878E-03,-0.10397563E-02, 0.17724644E-04,
         0.90502299E-05,-0.21754746E-04, 0.32004828E-04, 0.57125675E-04,
         0.16634763E-02, 0.27680555E-04, 0.43808701E-02, 0.49787820E-02,
         0.84365514E-04, 0.40072319E-02, 0.32295729E-02,-0.48236991E-03,
         0.82851475E-03,-0.56779088E-03, 0.75114460E-03,-0.13528324E-02,
         0.66266177E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x33    *x52
        +coeff[ 23]            *x43    
        +coeff[ 24]        *x33        
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]        *x31    *x52
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]*x11*x21*x31    *x51
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]    *x21*x31*x42*x51
        +coeff[ 52]        *x31    *x53
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 53]    *x21*x32*x41*x51
        +coeff[ 54]    *x22            
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11        *x41*x51
        +coeff[ 57]            *x43*x51
        +coeff[ 58]        *x33    *x51
        +coeff[ 59]        *x33*x42    
        +coeff[ 60]*x11*x21    *x43    
        +coeff[ 61]        *x34*x41    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]*x11*x22    *x41*x51
        +coeff[ 66]    *x22*x31    *x52
        +coeff[ 67]    *x23*x32*x41    
        +coeff[ 68]    *x21    *x41*x53
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]    *x22*x31*x44    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 71]    *x22*x32*x43    
        +coeff[ 72]            *x43*x53
        +coeff[ 73]            *x45*x52
        +coeff[ 74]        *x32*x43*x52
        +coeff[ 75]    *x23*x31*x44    
        +coeff[ 76]    *x21*x33*x44    
        +coeff[ 77]    *x23*x33*x42    
        +coeff[ 78]    *x24    *x45    
        +coeff[ 79]            *x42    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 80]        *x31*x41    
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]*x12*x21    *x41    
        +coeff[ 83]        *x32*x42*x51
        +coeff[ 84]    *x21    *x45    
        +coeff[ 85]*x11    *x31    *x52
        +coeff[ 86]    *x21*x31*x44    
        +coeff[ 87]    *x21*x32*x43    
        +coeff[ 88]*x11    *x31*x44    
    ;
    v_y_e_q3ex_1_1000                              =v_y_e_q3ex_1_1000                              
        +coeff[ 89]    *x23*x31*x42    
        +coeff[ 90]    *x21*x33*x42    
        +coeff[ 91]    *x22    *x43*x51
        +coeff[ 92]    *x21*x34*x41    
        +coeff[ 93]    *x22*x31*x42*x51
        +coeff[ 94]    *x23*x33        
        +coeff[ 95]    *x22    *x45    
        +coeff[ 96]    *x21*x31    *x53
        ;

    return v_y_e_q3ex_1_1000                              ;
}
float p_e_q3ex_1_1000                              (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1127022E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.96636773E-04, 0.15742712E-01,-0.33656538E-01, 0.15962236E-01,
        -0.78310482E-02,-0.97727710E-02, 0.73512588E-02,-0.47459497E-03,
         0.88013956E-04, 0.50873175E-04, 0.41770305E-04, 0.47999430E-02,
         0.20776552E-02, 0.41130902E-02,-0.59976371E-03, 0.30080730E-02,
         0.14557296E-02, 0.10573558E-02, 0.71832247E-03,-0.38425974E-05,
        -0.17054351E-03, 0.46275865E-03, 0.14898974E-02,-0.10209597E-02,
        -0.32855532E-03,-0.93312212E-03,-0.53362461E-03,-0.52680040E-03,
         0.20656393E-04,-0.20642154E-03,-0.39660657E-03,-0.68447487E-04,
         0.11686055E-03,-0.17731814E-03, 0.11008231E-02, 0.90146257E-03,
        -0.31238803E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_1000                              =v_p_e_q3ex_1_1000                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_1000                              =v_p_e_q3ex_1_1000                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_1000                              =v_p_e_q3ex_1_1000                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]*x11        *x41*x51
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_q3ex_1_1000                              =v_p_e_q3ex_1_1000                              
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_1000                              ;
}
float l_e_q3ex_1_1000                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.1446038E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30032E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.14304473E-01,-0.31217903E+00,-0.25161637E-01, 0.12486647E-01,
        -0.32450676E-01,-0.11896579E-01, 0.23009011E-02,-0.24095351E-03,
        -0.27057154E-02,-0.51824613E-02,-0.56450926E-02,-0.23575702E-02,
         0.93331477E-02, 0.90793967E-02,-0.23065270E-02, 0.79215958E-03,
        -0.94856165E-03, 0.38937437E-02, 0.18132464E-02, 0.55988634E-03,
         0.50410293E-02, 0.19876137E-02, 0.56539371E-03,-0.54764596E-03,
        -0.97714970E-03, 0.66182949E-02, 0.35460101E-03, 0.42932827E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_1000                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_1000                              =v_l_e_q3ex_1_1000                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3ex_1_1000                              =v_l_e_q3ex_1_1000                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x32*x42*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3ex_1_1000                              =v_l_e_q3ex_1_1000                              
        +coeff[ 26]        *x32    *x53
        +coeff[ 27]*x12            *x53
        ;

    return v_l_e_q3ex_1_1000                              ;
}
float x_e_q3ex_1_900                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.7197732E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.83479499E-02, 0.20487492E+00, 0.86025469E-01,-0.99892970E-02,
         0.24851697E-01, 0.88259233E-02,-0.29677158E-04,-0.72907158E-02,
        -0.50100707E-02,-0.42546331E-02,-0.99139297E-02,-0.64689852E-02,
        -0.23846440E-03,-0.86721784E-03, 0.32041287E-04, 0.14199170E-02,
        -0.35492158E-02, 0.33964843E-02,-0.23650512E-03, 0.80826099E-03,
        -0.29572574E-03, 0.81080251E-03,-0.20262951E-03,-0.25096856E-03,
         0.51672786E-03,-0.96386888E-04, 0.37272880E-03,-0.76063664E-03,
         0.45022700E-03, 0.18836773E-02,-0.24026195E-02,-0.14534637E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_900                              =v_x_e_q3ex_1_900                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_900                              =v_x_e_q3ex_1_900                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_1_900                              =v_x_e_q3ex_1_900                              
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_900                              ;
}
float t_e_q3ex_1_900                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5782677E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.57501923E-02,-0.29076805E-01, 0.75889245E-01, 0.33447412E-02,
        -0.57117133E-02,-0.20834245E-02,-0.22580510E-02,-0.30847211E-03,
        -0.96715090E-03,-0.10474190E-02, 0.98288374E-03, 0.24102577E-03,
         0.29511863E-03,-0.26801781E-03, 0.46364110E-03, 0.15023568E-02,
        -0.24411963E-03,-0.10999057E-03, 0.12959329E-03, 0.22363968E-03,
         0.37562873E-03,-0.63589192E-03, 0.39784200E-03, 0.38839449E-03,
        -0.63503627E-03,-0.34410786E-03,-0.12221948E-03, 0.58175431E-03,
        -0.15581773E-03, 0.78355260E-04,-0.52578955E-04,-0.29105275E-04,
         0.12699138E-03,-0.11591960E-03,-0.13826882E-03, 0.16853747E-03,
         0.29662723E-03,-0.22827109E-03, 0.56195003E-03, 0.99847523E-04,
        -0.21183871E-03, 0.60648355E-04,-0.41296997E-03, 0.20500059E-03,
         0.95993877E-04,-0.85762891E-04, 0.11965640E-03,-0.67958586E-04,
         0.13342408E-03, 0.36624575E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_900                              =v_t_e_q3ex_1_900                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_900                              =v_t_e_q3ex_1_900                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_900                              =v_t_e_q3ex_1_900                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]*x11            *x52
        +coeff[ 32]*x11*x23            
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_q3ex_1_900                              =v_t_e_q3ex_1_900                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]        *x32    *x51
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11*x22        *x51
        +coeff[ 42]    *x21*x32*x42    
        +coeff[ 43]    *x22*x31*x41*x51
    ;
    v_t_e_q3ex_1_900                              =v_t_e_q3ex_1_900                              
        +coeff[ 44]        *x33*x41*x51
        +coeff[ 45]        *x32*x42*x51
        +coeff[ 46]    *x22        *x53
        +coeff[ 47]        *x32    *x53
        +coeff[ 48]        *x31*x41*x53
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_q3ex_1_900                              ;
}
float y_e_q3ex_1_900                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1527011E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.75937911E-04, 0.96342407E-01,-0.94751753E-01,-0.68968065E-01,
        -0.26482223E-01, 0.53537283E-01, 0.24375658E-01,-0.36776874E-01,
        -0.13151063E-01,-0.90969028E-02, 0.24664034E-02,-0.92639038E-02,
         0.62662847E-02,-0.71663817E-03,-0.24272650E-02, 0.10635747E-02,
        -0.54083425E-02, 0.20395117E-02,-0.23345789E-02,-0.20158701E-02,
        -0.32889864E-02,-0.24068174E-02,-0.53899689E-02,-0.11382326E-02,
         0.50549122E-03,-0.80289744E-03, 0.20926616E-02, 0.24676595E-02,
         0.15323244E-02, 0.13428440E-02, 0.42233433E-03,-0.61399685E-02,
        -0.94727520E-02,-0.61744195E-02, 0.18733711E-02, 0.98335603E-03,
        -0.46453686E-03, 0.40210431E-03,-0.17040814E-02,-0.92456938E-03,
         0.84217096E-03,-0.48661974E-03, 0.30841868E-04,-0.25089343E-04,
        -0.84185005E-04, 0.50826219E-03,-0.14783415E-02,-0.12109869E-02,
         0.11176302E-03,-0.27128510E-03,-0.12326333E-02, 0.40937829E-03,
        -0.12885808E-03,-0.38981813E-03,-0.59302482E-04, 0.30047825E-03,
         0.11950160E-03, 0.50960999E-03, 0.64979371E-03, 0.32713218E-03,
         0.81306108E-03,-0.15815298E-03, 0.28185272E-02, 0.20479191E-03,
         0.21166534E-02, 0.14366253E-03, 0.50845882E-03,-0.25718907E-03,
        -0.77903317E-03, 0.15929138E-04,-0.18918970E-04,-0.41328117E-03,
         0.33305503E-04,-0.65981294E-04, 0.27546969E-02, 0.95409727E-04,
         0.95885422E-04, 0.27799818E-02, 0.13146506E-02,-0.66510576E-03,
         0.17686303E-03,-0.87875716E-03, 0.12042231E-02, 0.69407652E-04,
         0.56598026E-04,-0.44226242E-03,-0.18214697E-02, 0.14632096E-02,
        -0.23346805E-03,-0.29142830E-03,-0.17389188E-02, 0.87253039E-03,
        -0.66945684E-03, 0.10405319E-03, 0.54282759E-03, 0.14013218E-02,
        -0.27696861E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x24*x31        
        +coeff[ 39]    *x22*x33        
        +coeff[ 40]    *x22    *x41*x52
        +coeff[ 41]    *x24    *x43    
        +coeff[ 42]    *x21            
        +coeff[ 43]                *x51
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 44]*x11    *x31    *x51
        +coeff[ 45]        *x32*x41*x51
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]        *x33*x42    
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x21*x31    *x52
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]    *x21*x31*x42*x51
        +coeff[ 52]        *x31    *x53
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 53]    *x21*x32*x41*x51
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]            *x43*x51
        +coeff[ 56]        *x33    *x51
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x21*x32*x41    
        +coeff[ 60]    *x23    *x43    
        +coeff[ 61]*x11*x22    *x41*x51
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 62]    *x23*x31*x42    
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x41*x53
        +coeff[ 66]    *x23*x33        
        +coeff[ 67]    *x21    *x43*x52
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22            
        +coeff[ 70]*x12    *x31        
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 71]        *x34*x41    
        +coeff[ 72]*x12        *x41*x51
        +coeff[ 73]            *x41*x53
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]        *x31*x42*x52
        +coeff[ 76]    *x21*x33    *x51
        +coeff[ 77]    *x21*x32*x43    
        +coeff[ 78]    *x21*x33*x42    
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 80]*x11*x22*x31*x42    
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]        *x32*x45    
        +coeff[ 83]    *x21*x31    *x53
        +coeff[ 84]*x11    *x34*x41    
        +coeff[ 85]    *x22*x32*x41*x51
        +coeff[ 86]    *x22*x31*x44    
        +coeff[ 87]        *x33*x44    
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_q3ex_1_900                              =v_y_e_q3ex_1_900                              
        +coeff[ 89]    *x21    *x45*x51
        +coeff[ 90]    *x22*x32*x43    
        +coeff[ 91]        *x34*x43    
        +coeff[ 92]    *x22*x33*x42    
        +coeff[ 93]*x11    *x31*x42*x52
        +coeff[ 94]    *x23    *x43*x51
        +coeff[ 95]    *x23    *x45    
        +coeff[ 96]        *x32*x43*x52
        ;

    return v_y_e_q3ex_1_900                              ;
}
float p_e_q3ex_1_900                              (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.2169852E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.41656722E-06, 0.15811708E-01,-0.33519477E-01, 0.16021775E-01,
        -0.78815948E-02,-0.97825360E-02, 0.73527996E-02,-0.51149586E-03,
        -0.23447164E-05, 0.10190553E-03,-0.38432259E-04, 0.49052490E-02,
         0.20925384E-02, 0.41110958E-02,-0.59752061E-03, 0.29901941E-02,
         0.14585654E-02, 0.10696960E-02, 0.72659086E-03,-0.72207927E-05,
        -0.17132745E-03, 0.46493291E-03, 0.14962854E-02,-0.11079584E-02,
        -0.33465144E-03,-0.14043638E-02,-0.52023423E-03,-0.52062952E-03,
         0.58223627E-05,-0.62321033E-03,-0.20981114E-03,-0.55345270E-03,
        -0.67774854E-04, 0.15392978E-03, 0.11116068E-03,-0.16994120E-03,
         0.10743964E-02, 0.94034843E-03,-0.33727265E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_900                              =v_p_e_q3ex_1_900                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_900                              =v_p_e_q3ex_1_900                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_900                              =v_p_e_q3ex_1_900                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_900                              =v_p_e_q3ex_1_900                              
        +coeff[ 35]            *x43*x51
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_900                              ;
}
float l_e_q3ex_1_900                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1599159E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.16069183E-01,-0.31230825E+00,-0.25310114E-01, 0.12361838E-01,
        -0.32841671E-01,-0.12156975E-01,-0.57241777E-02, 0.23441759E-02,
        -0.28309259E-02,-0.50390186E-02,-0.24399583E-02, 0.62239137E-02,
         0.96104853E-02, 0.51816471E-03,-0.96964685E-03, 0.40041991E-02,
        -0.23380101E-02, 0.14465809E-02, 0.70945697E-03, 0.94952565E-02,
         0.28284451E-02, 0.74265478E-03,-0.21428366E-03, 0.37962428E-03,
         0.48357411E-03,-0.87528082E-03,-0.88626955E-03, 0.70965744E-03,
         0.63387821E-02, 0.35171502E-03,-0.53938350E-03, 0.76139998E-03,
        -0.13073434E-02, 0.52063679E-02, 0.35216489E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_900                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q3ex_1_900                              =v_l_e_q3ex_1_900                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]                *x52
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_1_900                              =v_l_e_q3ex_1_900                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]        *x31    *x51
        +coeff[ 23]                *x53
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_q3ex_1_900                              =v_l_e_q3ex_1_900                              
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]*x12    *x31*x41    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x11    *x34        
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]    *x24    *x42    
        +coeff[ 32]    *x21*x31*x42*x52
        +coeff[ 33]    *x23*x32*x42    
        +coeff[ 34]    *x23    *x44    
        ;

    return v_l_e_q3ex_1_900                              ;
}
float x_e_q3ex_1_800                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.7752968E-02;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.89712376E-02, 0.20484491E+00, 0.86323328E-01,-0.99764224E-02,
         0.24830215E-01, 0.88253655E-02,-0.79581832E-05,-0.72980281E-02,
        -0.49835271E-02,-0.42234254E-02,-0.10681937E-01,-0.67255725E-02,
        -0.21953687E-04,-0.85877354E-03,-0.23624069E-06, 0.14921878E-02,
        -0.35894960E-02, 0.32600844E-02,-0.24459814E-03,-0.20731111E-03,
         0.81929791E-03,-0.28633288E-03, 0.84836665E-03,-0.25732303E-03,
         0.53641223E-03,-0.34563269E-04,-0.69775723E-03, 0.49937732E-03,
         0.16012518E-02,-0.19055743E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_800                              =v_x_e_q3ex_1_800                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x12*x21            
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_800                              =v_x_e_q3ex_1_800                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_1_800                              =v_x_e_q3ex_1_800                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_1_800                              ;
}
float t_e_q3ex_1_800                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6225714E-02;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62146354E-02,-0.29010415E-01, 0.75900406E-01, 0.33585750E-02,
        -0.57113292E-02,-0.20809376E-02,-0.23510596E-02,-0.31458877E-03,
        -0.98494859E-03,-0.10686757E-02, 0.94660494E-03, 0.26513226E-03,
         0.22652696E-03,-0.40925064E-03, 0.46510305E-03, 0.15491856E-02,
        -0.25567081E-03,-0.10564457E-03, 0.98521647E-04, 0.22471193E-03,
         0.39242179E-03,-0.64935110E-03, 0.45592306E-03, 0.47029051E-03,
        -0.66888449E-03,-0.36199921E-03, 0.16649820E-03,-0.12859944E-03,
         0.50179631E-03,-0.18207431E-03, 0.90590889E-04,-0.52482923E-04,
         0.50974286E-04,-0.35453588E-04, 0.93534967E-04,-0.11322152E-03,
        -0.70010305E-04, 0.36902935E-03,-0.23394381E-03, 0.20198102E-03,
        -0.47147853E-03, 0.58637187E-03,-0.15273748E-04, 0.22940314E-04,
        -0.38726248E-04,-0.35454526E-04, 0.93801929E-04, 0.43597905E-04,
         0.19488510E-03, 0.15381798E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_800                              =v_t_e_q3ex_1_800                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_800                              =v_t_e_q3ex_1_800                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_800                              =v_t_e_q3ex_1_800                              
        +coeff[ 26]            *x42*x52
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x21*x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]        *x32    *x51
        +coeff[ 33]*x11            *x52
        +coeff[ 34]*x11*x23            
    ;
    v_t_e_q3ex_1_800                              =v_t_e_q3ex_1_800                              
        +coeff[ 35]*x11*x21    *x42    
        +coeff[ 36]        *x31*x41*x52
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x22        *x53
        +coeff[ 40]    *x22*x31*x41*x52
        +coeff[ 41]    *x23        *x53
        +coeff[ 42]*x12                
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_q3ex_1_800                              =v_t_e_q3ex_1_800                              
        +coeff[ 44]        *x31*x41*x51
        +coeff[ 45]*x11*x21*x32        
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]*x11*x22        *x51
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]    *x23*x31*x41    
        ;

    return v_t_e_q3ex_1_800                              ;
}
float y_e_q3ex_1_800                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.5768013E-03;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.58001286E-03, 0.95776707E-01,-0.95150530E-01,-0.68610869E-01,
        -0.26619846E-01, 0.53578176E-01, 0.24373643E-01,-0.37192378E-01,
        -0.13347322E-01,-0.91923326E-02, 0.24653911E-02,-0.98756999E-02,
        -0.21848721E-03,-0.11165948E-02, 0.10514857E-02,-0.55301022E-02,
         0.20379403E-02,-0.23359738E-02,-0.20036497E-02, 0.57930225E-02,
        -0.32658509E-02,-0.23893595E-02,-0.58278693E-02,-0.11668648E-02,
         0.49961818E-03,-0.79304946E-03, 0.73377386E-03, 0.44513852E-02,
         0.10189724E-02, 0.15648253E-02, 0.59049058E-03,-0.35873523E-02,
        -0.52976478E-02,-0.51555852E-02,-0.15526890E-02, 0.20942439E-02,
         0.10994499E-02,-0.47312307E-03, 0.32561290E-03,-0.28080793E-03,
        -0.95847034E-03,-0.78497385E-03, 0.80122508E-03,-0.79443264E-04,
         0.49950223E-03,-0.42965278E-03,-0.54744643E-03, 0.28349785E-03,
        -0.12310130E-03,-0.39939606E-03,-0.65381006E-04, 0.32813105E-03,
         0.12204436E-03, 0.11035593E-03, 0.54555153E-03,-0.37740901E-03,
         0.70715474E-03,-0.95055628E-04, 0.39357625E-03, 0.23290999E-02,
        -0.23157381E-03, 0.18232575E-03, 0.26425929E-02, 0.20799894E-03,
        -0.34658084E-03,-0.76792779E-03,-0.75032362E-02,-0.62901052E-02,
         0.38075829E-02, 0.10758471E-02, 0.72954164E-04,-0.13063420E-02,
        -0.68102695E-05, 0.63849138E-05, 0.25538942E-04,-0.23638209E-04,
        -0.50255039E-04, 0.12713553E-02, 0.31455540E-04, 0.84619955E-04,
         0.12465884E-03, 0.26768534E-02,-0.73041196E-03, 0.70002454E-03,
        -0.83275263E-04, 0.16533535E-03,-0.10863009E-02, 0.33932272E-03,
        -0.25567489E-02, 0.66332235E-04,-0.39354380E-03,-0.24930251E-03,
        -0.14238236E-02, 0.25162168E-03,-0.22476905E-02,-0.30149298E-02,
        -0.67092833E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x24*x31        
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]*x11    *x31    *x51
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 44]        *x32*x41*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33*x42    
        +coeff[ 47]    *x21*x31*x42*x51
        +coeff[ 48]        *x31    *x53
        +coeff[ 49]    *x21*x32*x41*x51
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 53]*x11*x21*x31    *x51
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]            *x43*x52
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]*x11*x22    *x41*x51
        +coeff[ 61]    *x22*x31    *x52
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 62]    *x23*x32*x41    
        +coeff[ 63]    *x21    *x41*x53
        +coeff[ 64]    *x21    *x43*x52
        +coeff[ 65]    *x24    *x41*x51
        +coeff[ 66]    *x22*x31*x44    
        +coeff[ 67]    *x22*x32*x43    
        +coeff[ 68]    *x23*x31*x44    
        +coeff[ 69]    *x21*x33*x44    
        +coeff[ 70]    *x21    *x45*x52
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 71]    *x23*x34*x41    
        +coeff[ 72]    *x21            
        +coeff[ 73]                *x51
        +coeff[ 74]*x11        *x41*x51
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]            *x41*x53
        +coeff[ 77]    *x21    *x45    
        +coeff[ 78]*x11    *x31    *x52
        +coeff[ 79]        *x31*x42*x52
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 80]    *x21*x33    *x51
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x21*x34*x41    
        +coeff[ 84]*x11*x21    *x41*x52
        +coeff[ 85]*x11*x22*x31*x42    
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x22    *x45    
    ;
    v_y_e_q3ex_1_800                              =v_y_e_q3ex_1_800                              
        +coeff[ 89]    *x21*x31    *x53
        +coeff[ 90]    *x22*x32*x41*x51
        +coeff[ 91]    *x24*x31    *x51
        +coeff[ 92]    *x24    *x43    
        +coeff[ 93]    *x23    *x41*x52
        +coeff[ 94]    *x24*x31*x42    
        +coeff[ 95]    *x22*x33*x42    
        +coeff[ 96]    *x24*x32*x41    
        ;

    return v_y_e_q3ex_1_800                              ;
}
float p_e_q3ex_1_800                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.2130458E-03;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.21196780E-03, 0.15914481E-01,-0.33370469E-01, 0.16148204E-01,
        -0.78805881E-02,-0.97885970E-02, 0.73513729E-02,-0.56250417E-03,
        -0.67909124E-04, 0.14201262E-03, 0.55267254E-04, 0.49338811E-02,
         0.20902157E-02, 0.40683248E-02,-0.60003938E-03, 0.30081403E-02,
         0.14630944E-02, 0.10648743E-02, 0.73041552E-03,-0.12222590E-05,
        -0.17375223E-03, 0.46656968E-03, 0.14985680E-02,-0.10961848E-02,
        -0.33688557E-03,-0.14793910E-02,-0.66622038E-03,-0.54568209E-03,
        -0.55429555E-03,-0.10003582E-04,-0.20381995E-03,-0.61941211E-03,
        -0.67333189E-04, 0.15677164E-03, 0.11087216E-03,-0.18041930E-03,
         0.10734807E-02, 0.91554481E-03,-0.32858347E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_800                              =v_p_e_q3ex_1_800                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_800                              =v_p_e_q3ex_1_800                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_800                              =v_p_e_q3ex_1_800                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_800                              =v_p_e_q3ex_1_800                              
        +coeff[ 35]            *x43*x51
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_800                              ;
}
float l_e_q3ex_1_800                              (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.1710213E-01;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.17096147E-01,-0.31169116E+00,-0.25272891E-01, 0.12401171E-01,
        -0.33966038E-01,-0.12068621E-01, 0.23145911E-02,-0.17285363E-03,
        -0.52690324E-02,-0.56791701E-02,-0.23788307E-02, 0.89377826E-02,
         0.84572406E-02, 0.60218445E-03, 0.25391020E-04, 0.20530322E-02,
        -0.28145085E-02,-0.84651122E-03,-0.13528016E-02, 0.31382216E-02,
        -0.22611313E-02, 0.18526757E-02, 0.77892427E-03, 0.76467579E-03,
         0.66831434E-03, 0.44318321E-03, 0.13088521E-02, 0.18115639E-02,
         0.93564735E-03,-0.94331300E-03,-0.85248443E-03, 0.60079992E-02,
         0.58568423E-02, 0.99388999E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_800                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_800                              =v_l_e_q3ex_1_800                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x34        
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]        *x32        
    ;
    v_l_e_q3ex_1_800                              =v_l_e_q3ex_1_800                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32        
        +coeff[ 20]    *x22        *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x32    *x53
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x21        *x52
    ;
    v_l_e_q3ex_1_800                              =v_l_e_q3ex_1_800                              
        +coeff[ 26]    *x24            
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        +coeff[ 33]    *x22*x32    *x51
        ;

    return v_l_e_q3ex_1_800                              ;
}
float x_e_q3ex_1_700                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.7920214E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.89571951E-02, 0.20486385E+00, 0.86260505E-01,-0.99704657E-02,
         0.24882458E-01, 0.88091930E-02,-0.15284919E-04,-0.72843432E-02,
        -0.49920324E-02,-0.42593880E-02,-0.97326254E-02,-0.64121815E-02,
        -0.29081808E-03,-0.85577671E-03, 0.13438077E-04, 0.14594286E-02,
        -0.35141886E-02, 0.34093393E-02, 0.78639237E-03,-0.28003997E-03,
         0.83276874E-03,-0.23143756E-03,-0.20313090E-03,-0.30267521E-03,
         0.51781005E-03,-0.53240219E-04, 0.43275594E-03,-0.73150941E-03,
         0.50385448E-03, 0.16725163E-02,-0.25531866E-02,-0.17699588E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_700                              =v_x_e_q3ex_1_700                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_700                              =v_x_e_q3ex_1_700                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]                *x53
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]*x11            *x51
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_1_700                              =v_x_e_q3ex_1_700                              
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_700                              ;
}
float t_e_q3ex_1_700                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6049296E-02;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.59542852E-02,-0.29052466E-01, 0.75886853E-01, 0.33355320E-02,
        -0.57195337E-02,-0.21056312E-02,-0.22696445E-02,-0.30438878E-03,
        -0.10053192E-02,-0.10601629E-02, 0.99356042E-03, 0.23851612E-03,
         0.26186579E-03, 0.44830900E-03, 0.15179866E-02, 0.39514960E-03,
        -0.23953343E-03,-0.10442789E-03,-0.35270880E-03, 0.17799226E-03,
         0.38539525E-03,-0.63843658E-03, 0.21858995E-03,-0.61212102E-03,
        -0.34216774E-03,-0.97822282E-04, 0.46369791E-03,-0.22787577E-03,
         0.75443633E-04,-0.52498061E-04, 0.12222945E-03, 0.27647073E-03,
        -0.79086421E-04,-0.16931057E-03, 0.15906004E-03, 0.37278185E-03,
        -0.31116352E-03, 0.12989349E-03,-0.22189692E-03, 0.92723762E-03,
         0.61623956E-03, 0.52005670E-03,-0.38866943E-04,-0.31017174E-04,
         0.79383346E-04, 0.47618734E-04, 0.46637713E-04, 0.15585107E-03,
         0.68016234E-04, 0.20006216E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_700                              =v_t_e_q3ex_1_700                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_700                              =v_t_e_q3ex_1_700                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x21        *x53
    ;
    v_t_e_q3ex_1_700                              =v_t_e_q3ex_1_700                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x21*x31*x41    
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_1_700                              =v_t_e_q3ex_1_700                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x22*x32    *x51
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x22*x32*x42    
        +coeff[ 40]    *x22*x31*x43    
        +coeff[ 41]    *x23        *x53
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]*x11            *x52
    ;
    v_t_e_q3ex_1_700                              =v_t_e_q3ex_1_700                              
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]    *x22        *x52
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]*x11*x22*x31*x41    
        +coeff[ 49]    *x23*x31*x41    
        ;

    return v_t_e_q3ex_1_700                              ;
}
float y_e_q3ex_1_700                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.3083757E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.32281864E-03, 0.94879299E-01,-0.95750928E-01,-0.68630517E-01,
        -0.26349759E-01, 0.53617809E-01, 0.24415979E-01,-0.36995675E-01,
        -0.13107965E-01,-0.91359792E-02, 0.24687520E-02,-0.92336778E-02,
         0.59258873E-02,-0.33358356E-03,-0.12281680E-02, 0.10593423E-02,
        -0.56858249E-02, 0.20608180E-02,-0.22889229E-02,-0.19701119E-02,
        -0.34285251E-02,-0.26175592E-02,-0.57290103E-02,-0.11089111E-02,
         0.50896546E-03,-0.77376445E-03, 0.41525011E-03, 0.11440854E-02,
         0.45555335E-03, 0.12296947E-02, 0.28837009E-03,-0.51265191E-02,
        -0.90597291E-02,-0.56441072E-02, 0.20533821E-02, 0.10575663E-02,
        -0.47652802E-03, 0.41441154E-03,-0.28917741E-03,-0.16698906E-02,
        -0.98559726E-03, 0.67174732E-03,-0.16970903E-03,-0.21028294E-04,
         0.17517985E-04,-0.72500341E-04, 0.47529393E-03,-0.11510077E-02,
        -0.12740850E-02, 0.10626960E-03,-0.12479532E-02,-0.12981948E-03,
        -0.43853151E-03, 0.64891495E-03,-0.42560579E-04,-0.16808580E-05,
         0.27172748E-03, 0.11388577E-03, 0.43465162E-03,-0.23645669E-03,
         0.65776572E-03, 0.30827257E-03, 0.22922801E-02,-0.15671016E-03,
         0.23511918E-02,-0.32548833E-03,-0.65279374E-03,-0.41231243E-02,
        -0.32960663E-02, 0.24766906E-03, 0.55777619E-03, 0.67047379E-03,
         0.20748428E-02, 0.43527942E-03,-0.56760368E-03,-0.12068278E-04,
        -0.18130422E-04, 0.10361418E-04,-0.56736069E-04, 0.20174608E-03,
         0.17099110E-02,-0.19284156E-03, 0.43033841E-02, 0.44599418E-02,
         0.34033663E-02, 0.28167698E-02, 0.11901877E-03,-0.58198790E-03,
         0.87934808E-03, 0.13233972E-03,-0.87252539E-03, 0.65049744E-03,
        -0.12656860E-02, 0.96824078E-04,-0.28218760E-03, 0.44413560E-03,
         0.51536223E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]    *x22    *x41*x52
        +coeff[ 42]    *x24    *x43    
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 44]                *x51
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]        *x31*x44    
        +coeff[ 48]        *x33*x42    
        +coeff[ 49]*x11*x21*x31    *x51
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]        *x31    *x53
        +coeff[ 52]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 53]    *x22*x31*x42*x52
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]*x11        *x41*x51
        +coeff[ 56]            *x43*x51
        +coeff[ 57]        *x33    *x51
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]        *x34*x41    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]*x11*x22    *x41*x51
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x43*x52
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x31*x44    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]    *x21*x33*x42*x51
        +coeff[ 70]    *x22    *x43*x52
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 71]    *x23*x31*x44    
        +coeff[ 72]    *x23*x32*x43    
        +coeff[ 73]    *x23*x33*x42    
        +coeff[ 74]    *x24    *x45    
        +coeff[ 75]    *x22            
        +coeff[ 76]*x12    *x31        
        +coeff[ 77]    *x21    *x43*x51
        +coeff[ 78]            *x41*x53
        +coeff[ 79]    *x21*x31*x42*x51
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 80]    *x21    *x45    
        +coeff[ 81]            *x43*x52
        +coeff[ 82]    *x21*x31*x44    
        +coeff[ 83]    *x21*x32*x43    
        +coeff[ 84]    *x23*x31*x42    
        +coeff[ 85]    *x21*x33*x42    
        +coeff[ 86]    *x22*x31    *x52
        +coeff[ 87]    *x22    *x43*x51
        +coeff[ 88]    *x21*x34*x41    
    ;
    v_y_e_q3ex_1_700                              =v_y_e_q3ex_1_700                              
        +coeff[ 89]    *x21    *x41*x53
        +coeff[ 90]    *x22*x31*x42*x51
        +coeff[ 91]    *x23*x33        
        +coeff[ 92]    *x22    *x45    
        +coeff[ 93]    *x21*x31    *x53
        +coeff[ 94]    *x22*x32*x41*x51
        +coeff[ 95]        *x33*x44    
        +coeff[ 96]*x11*x24*x31        
        ;

    return v_y_e_q3ex_1_700                              ;
}
float p_e_q3ex_1_700                              (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.5556228E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.60643739E-04, 0.16035169E-01,-0.33156753E-01, 0.16156983E-01,
        -0.78561734E-02,-0.98317862E-02, 0.73743500E-02,-0.55575697E-03,
        -0.48689053E-04, 0.77273777E-04,-0.67790506E-05, 0.49288920E-02,
         0.20939810E-02, 0.41148453E-02,-0.59971894E-03, 0.30171166E-02,
         0.14634633E-02, 0.10590988E-02, 0.74788707E-03, 0.56499692E-04,
        -0.17164648E-03, 0.47443152E-03, 0.14631237E-02,-0.11260259E-02,
        -0.33444984E-03,-0.15333990E-02,-0.67303586E-03,-0.54784410E-03,
        -0.53682923E-03,-0.36720317E-04,-0.18174668E-03,-0.63752697E-03,
        -0.70413917E-04, 0.11132783E-03,-0.16271582E-03, 0.10629146E-02,
         0.89972996E-03,-0.33310193E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_700                              =v_p_e_q3ex_1_700                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_700                              =v_p_e_q3ex_1_700                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_700                              =v_p_e_q3ex_1_700                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21*x32*x41    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]            *x43*x51
    ;
    v_p_e_q3ex_1_700                              =v_p_e_q3ex_1_700                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_700                              ;
}
float l_e_q3ex_1_700                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1582841E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.15597785E-01,-0.31251726E+00,-0.25402306E-01, 0.12444563E-01,
        -0.32661024E-01,-0.12128009E-01,-0.19968495E-03,-0.48469971E-02,
        -0.53255926E-02,-0.22718962E-02, 0.22030957E-02, 0.62231743E-02,
         0.90426896E-02, 0.14320946E-03, 0.66767243E-04, 0.11335609E-02,
        -0.28480552E-02,-0.96548838E-03, 0.31719801E-02,-0.21912444E-02,
         0.21392072E-02,-0.28724797E-03, 0.44703353E-03, 0.36990465E-03,
         0.36150031E-03, 0.53059391E-03,-0.88576204E-03,-0.62357262E-03,
         0.83113089E-02, 0.22022889E-02, 0.60676215E-02,-0.23157655E-02,
        -0.47674280E-03, 0.41407831E-02, 0.49576871E-02, 0.89310445E-02,
         0.55643632E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_1_700                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42*x52
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_q3ex_1_700                              =v_l_e_q3ex_1_700                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x34        
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]        *x32        
    ;
    v_l_e_q3ex_1_700                              =v_l_e_q3ex_1_700                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x23            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]                *x53
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_q3ex_1_700                              =v_l_e_q3ex_1_700                              
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x22*x31*x41*x51
        +coeff[ 32]*x12            *x54
        +coeff[ 33]    *x23*x32*x42    
        +coeff[ 34]    *x23    *x44    
    ;
    v_l_e_q3ex_1_700                              =v_l_e_q3ex_1_700                              
        +coeff[ 35]    *x21*x32*x42*x52
        +coeff[ 36]    *x21*x31*x43*x52
        ;

    return v_l_e_q3ex_1_700                              ;
}
float x_e_q3ex_1_600                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.9168012E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.95834676E-02, 0.20489027E+00, 0.86420551E-01,-0.99544609E-02,
         0.24917716E-01, 0.88227475E-02, 0.19527599E-04,-0.73077749E-02,
        -0.49891672E-02,-0.42437962E-02,-0.97831637E-02,-0.64361165E-02,
        -0.25466236E-03,-0.85661624E-03,-0.22052614E-04, 0.15080472E-02,
        -0.35275877E-02, 0.33493834E-02,-0.23482686E-03,-0.22202595E-03,
         0.80890144E-03,-0.29544861E-03, 0.82010485E-03,-0.26979460E-03,
         0.51429070E-03,-0.63437212E-04, 0.43279005E-03,-0.82215713E-03,
         0.52069646E-03, 0.17432069E-02,-0.26055027E-02,-0.17070457E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_600                              =v_x_e_q3ex_1_600                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21*x32    *x52
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_600                              =v_x_e_q3ex_1_600                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_1_600                              =v_x_e_q3ex_1_600                              
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_600                              ;
}
float t_e_q3ex_1_600                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6641254E-02;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.63198041E-02,-0.29075529E-01, 0.75890064E-01, 0.33522309E-02,
        -0.57106833E-02,-0.20769262E-02,-0.22122909E-02,-0.30734864E-03,
        -0.98910846E-03,-0.10517321E-02, 0.99650433E-03, 0.26675823E-03,
         0.28204665E-03,-0.32661611E-03, 0.43646008E-03, 0.14731629E-02,
        -0.23347982E-03,-0.10622050E-03, 0.19451301E-03, 0.40494782E-03,
        -0.65185729E-03, 0.20752853E-03, 0.40641980E-03,-0.63706900E-03,
        -0.35243537E-03, 0.57369267E-03, 0.55851974E-03,-0.15629530E-03,
         0.78223580E-04,-0.55666773E-04, 0.87881665E-04, 0.47103880E-03,
        -0.10203275E-03,-0.10155029E-03, 0.14062808E-03, 0.24755235E-03,
        -0.39575776E-03, 0.55527798E-04,-0.21407858E-03, 0.44794398E-03,
        -0.40515565E-03,-0.13037421E-04, 0.38660914E-04,-0.95075469E-04,
        -0.28549799E-04,-0.39534127E-04, 0.49710001E-04,-0.12440885E-03,
         0.10668149E-03, 0.11145500E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_600                              =v_t_e_q3ex_1_600                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_600                              =v_t_e_q3ex_1_600                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x22    *x42*x51
    ;
    v_t_e_q3ex_1_600                              =v_t_e_q3ex_1_600                              
        +coeff[ 26]    *x23        *x53
        +coeff[ 27]    *x21*x31*x41    
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_1_600                              =v_t_e_q3ex_1_600                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x22*x32    *x51
        +coeff[ 38]    *x21*x32    *x52
        +coeff[ 39]    *x22*x32*x42    
        +coeff[ 40]    *x22*x31*x41*x52
        +coeff[ 41]*x12                
        +coeff[ 42]        *x32    *x51
        +coeff[ 43]        *x31*x41*x51
    ;
    v_t_e_q3ex_1_600                              =v_t_e_q3ex_1_600                              
        +coeff[ 44]*x11            *x52
        +coeff[ 45]*x11*x21*x32        
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x23*x32        
        +coeff[ 49]        *x33*x41*x51
        ;

    return v_t_e_q3ex_1_600                              ;
}
float y_e_q3ex_1_600                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2019214E-03;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.20722604E-03, 0.93642652E-01,-0.96488141E-01,-0.68658188E-01,
        -0.26522109E-01, 0.53576112E-01, 0.24415145E-01,-0.36952220E-01,
        -0.12898156E-01,-0.91606528E-02, 0.24642274E-02,-0.95748054E-02,
        -0.23138941E-03,-0.10043840E-02, 0.10582274E-02,-0.57432731E-02,
         0.20202589E-02,-0.23675798E-02,-0.19955859E-02, 0.58876565E-02,
        -0.33397602E-02,-0.23934045E-02,-0.57738535E-02,-0.11116555E-02,
         0.50699536E-03,-0.75792766E-03, 0.11502788E-02, 0.41259849E-02,
         0.14836699E-02, 0.14704493E-02, 0.46746444E-03,-0.52845371E-02,
        -0.86641777E-02,-0.57884222E-02, 0.21121115E-02, 0.18305391E-03,
         0.93983242E-03,-0.47486820E-03, 0.42361609E-03,-0.27814295E-03,
        -0.13014351E-02,-0.19438234E-02, 0.86966815E-03,-0.76498254E-04,
         0.50113781E-03,-0.67112286E-03,-0.89559751E-03, 0.28338851E-03,
        -0.11390672E-02,-0.12998476E-03,-0.40439010E-03,-0.51172032E-04,
        -0.76196075E-05, 0.31442015E-03, 0.11609445E-03, 0.10468593E-03,
         0.50452253E-03,-0.17085741E-03, 0.63939847E-03,-0.12206878E-03,
         0.38239689E-03, 0.23165157E-02,-0.13842720E-03,-0.15504265E-03,
         0.22239937E-02, 0.19794809E-03,-0.74127002E-03,-0.42828401E-02,
        -0.24716742E-02, 0.31755294E-02, 0.44677150E-03, 0.12662787E-03,
        -0.44238166E-03, 0.55217129E-05,-0.43760256E-05,-0.17684843E-04,
         0.23732991E-04, 0.46241501E-04,-0.51073173E-04, 0.82004949E-03,
         0.14261789E-03, 0.80524056E-04, 0.91975242E-04, 0.21016283E-02,
         0.35257550E-03, 0.75084786E-03, 0.71005611E-03,-0.66691870E-03,
         0.16094551E-03,-0.12367006E-02, 0.50098402E-03,-0.12846490E-02,
        -0.43553844E-03, 0.10869279E-03,-0.20835023E-03,-0.42570880E-03,
        -0.25772650E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]*x11    *x31    *x51
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 44]        *x32*x41*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33*x42    
        +coeff[ 47]    *x21*x31*x42*x51
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]        *x31    *x53
        +coeff[ 50]    *x21*x32*x41*x51
        +coeff[ 51]*x12        *x41    
        +coeff[ 52]*x11        *x41*x51
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 53]            *x43*x51
        +coeff[ 54]        *x33    *x51
        +coeff[ 55]*x11*x21*x31    *x51
        +coeff[ 56]*x11*x21    *x43    
        +coeff[ 57]        *x34*x41    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]            *x43*x52
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 62]*x11*x22    *x41*x51
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x41*x53
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x31*x44    
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]    *x23*x31*x44    
        +coeff[ 70]    *x24*x31    *x52
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 71]    *x24*x31*x42*x51
        +coeff[ 72]    *x24    *x45    
        +coeff[ 73]    *x21            
        +coeff[ 74]                *x51
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]    *x21    *x44    
        +coeff[ 77]*x12        *x41*x51
        +coeff[ 78]            *x41*x53
        +coeff[ 79]    *x21    *x45    
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 80]        *x31*x42*x52
        +coeff[ 81]    *x23*x31    *x51
        +coeff[ 82]    *x21*x33    *x51
        +coeff[ 83]    *x21*x32*x43    
        +coeff[ 84]        *x31*x44*x51
        +coeff[ 85]    *x23*x31*x42    
        +coeff[ 86]    *x21*x33*x42    
        +coeff[ 87]    *x22    *x43*x51
        +coeff[ 88]*x11*x22*x31*x42    
    ;
    v_y_e_q3ex_1_600                              =v_y_e_q3ex_1_600                              
        +coeff[ 89]    *x22*x31*x42*x51
        +coeff[ 90]    *x23*x33        
        +coeff[ 91]    *x22    *x45    
        +coeff[ 92]        *x32*x45    
        +coeff[ 93]    *x21*x31    *x53
        +coeff[ 94]    *x21    *x43*x52
        +coeff[ 95]    *x22*x32*x41*x51
        +coeff[ 96]    *x24*x31    *x51
        ;

    return v_y_e_q3ex_1_600                              ;
}
float p_e_q3ex_1_600                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.4862239E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.51091421E-04, 0.16211452E-01,-0.32844398E-01, 0.16099108E-01,
        -0.79490235E-02,-0.98427301E-02, 0.73693357E-02,-0.54398028E-03,
        -0.32436561E-04, 0.12848532E-03, 0.48983553E-02, 0.40872619E-02,
         0.21137488E-02,-0.59607194E-03, 0.29678587E-02, 0.14451639E-02,
         0.10719453E-02, 0.75954892E-03, 0.12276803E-04,-0.16961525E-03,
         0.46468500E-03, 0.14782599E-02,-0.10961982E-02,-0.33718062E-03,
        -0.15106254E-02,-0.63232717E-03,-0.51560544E-03,-0.52543113E-03,
         0.23674729E-04,-0.21986419E-03,-0.59089088E-03,-0.73510440E-04,
         0.17513352E-03, 0.10758382E-03,-0.20527866E-03, 0.11624878E-02,
         0.91885292E-03,-0.47417771E-03, 0.20803131E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_600                              =v_p_e_q3ex_1_600                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]    *x22*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_1_600                              =v_p_e_q3ex_1_600                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]    *x21    *x43    
    ;
    v_p_e_q3ex_1_600                              =v_p_e_q3ex_1_600                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]            *x43*x51
    ;
    v_p_e_q3ex_1_600                              =v_p_e_q3ex_1_600                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21*x32*x41*x51
        +coeff[ 38]    *x21    *x43*x51
        ;

    return v_p_e_q3ex_1_600                              ;
}
float l_e_q3ex_1_600                              (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1638937E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.16282616E-01,-0.31302390E+00,-0.25358448E-01, 0.12433159E-01,
        -0.32799095E-01,-0.11892882E-01,-0.56851758E-02, 0.23224743E-02,
         0.52658147E-02,-0.27198310E-02,-0.50558029E-02,-0.24875135E-02,
         0.52619390E-02, 0.22682950E-02,-0.98173309E-03, 0.21552085E-02,
        -0.22012966E-02, 0.14600703E-02,-0.53157219E-04, 0.50187262E-03,
         0.12343485E-02, 0.34135059E-03, 0.52117847E-03,-0.85654552E-03,
        -0.25530136E-03, 0.74424925E-02, 0.55109197E-02, 0.11190357E-01,
         0.10739138E-01, 0.43295939E-02,-0.16842092E-02, 0.82741701E-03,
         0.47309608E-02, 0.37595138E-02, 0.24156838E-02,-0.14430080E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_600                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q3ex_1_600                              =v_l_e_q3ex_1_600                              
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x32        
        +coeff[ 10]        *x31*x41    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_1_600                              =v_l_e_q3ex_1_600                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]        *x31        
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x53
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]*x11    *x33        
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3ex_1_600                              =v_l_e_q3ex_1_600                              
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21    *x44    
        +coeff[ 30]    *x23        *x52
        +coeff[ 31]*x12        *x42*x51
        +coeff[ 32]    *x23*x33*x41    
        +coeff[ 33]    *x21*x33*x41*x52
        +coeff[ 34]    *x23    *x42*x52
    ;
    v_l_e_q3ex_1_600                              =v_l_e_q3ex_1_600                              
        +coeff[ 35]        *x33*x42*x52
        ;

    return v_l_e_q3ex_1_600                              ;
}
float x_e_q3ex_1_500                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.9494314E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.85455310E-02,-0.72975140E-02, 0.20497335E+00, 0.86561851E-01,
        -0.99810818E-02, 0.24901809E-01, 0.90156281E-02,-0.49876561E-02,
        -0.41991659E-02,-0.97532859E-02,-0.63752150E-02,-0.36142580E-03,
        -0.71148522E-03, 0.78718949E-05, 0.14913535E-02,-0.35310599E-02,
         0.30541250E-02,-0.24236107E-03,-0.22869879E-03, 0.79177477E-03,
        -0.34835929E-03,-0.26575016E-03, 0.50392887E-03,-0.68394940E-04,
         0.40074060E-03, 0.80016034E-03,-0.74337062E-03, 0.17097427E-02,
        -0.28012288E-02,-0.16050398E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_500                              =v_x_e_q3ex_1_500                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21*x32    *x52
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_1_500                              =v_x_e_q3ex_1_500                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]*x11*x21            
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23            
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_x_e_q3ex_1_500                              =v_x_e_q3ex_1_500                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_500                              ;
}
float t_e_q3ex_1_500                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6446206E-02;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.56438092E-02,-0.29116984E-01, 0.75927421E-01, 0.33670305E-02,
        -0.57448978E-02,-0.20383943E-02,-0.22178434E-02,-0.30606680E-03,
        -0.10146479E-02,-0.10591169E-02, 0.95702522E-03, 0.24997466E-03,
         0.26204734E-03,-0.35950664E-03, 0.47993148E-03, 0.14420258E-02,
        -0.21921306E-03,-0.97252909E-04, 0.14035287E-03, 0.24987003E-03,
         0.41514711E-03,-0.64465386E-03, 0.41470953E-03,-0.67612744E-03,
        -0.34243485E-03,-0.17841268E-03,-0.16810732E-03, 0.46773278E-03,
        -0.11624659E-03, 0.77717530E-04,-0.59830971E-04, 0.76487566E-04,
         0.87561421E-04, 0.24894485E-03,-0.89164518E-04, 0.16053536E-03,
         0.35532977E-03,-0.30276497E-03, 0.64206542E-03,-0.11411044E-04,
        -0.34468361E-04, 0.67076464E-04, 0.16469145E-03, 0.13342161E-03,
        -0.13763136E-03,-0.22548021E-03,-0.11767803E-03,-0.19082741E-03,
        -0.12413162E-03, 0.15288353E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_500                              =v_t_e_q3ex_1_500                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_500                              =v_t_e_q3ex_1_500                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]        *x31*x41*x52
    ;
    v_t_e_q3ex_1_500                              =v_t_e_q3ex_1_500                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]*x11*x23            
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]*x11*x21    *x42    
    ;
    v_t_e_q3ex_1_500                              =v_t_e_q3ex_1_500                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11            *x52
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]    *x23*x32        
        +coeff[ 43]    *x23*x31*x41    
    ;
    v_t_e_q3ex_1_500                              =v_t_e_q3ex_1_500                              
        +coeff[ 44]    *x21*x32*x42    
        +coeff[ 45]        *x32*x42*x51
        +coeff[ 46]        *x31*x43*x51
        +coeff[ 47]    *x21*x31*x41*x52
        +coeff[ 48]    *x21    *x42*x52
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_q3ex_1_500                              ;
}
float y_e_q3ex_1_500                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3414271E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.37104322E-03, 0.92013381E-01,-0.97495042E-01,-0.68442650E-01,
        -0.26261639E-01, 0.53616822E-01, 0.24428638E-01,-0.37085928E-01,
        -0.12911709E-01,-0.91392687E-02, 0.24504566E-02,-0.94258301E-02,
        -0.26875746E-03,-0.10471466E-02, 0.10448797E-02,-0.57771751E-02,
         0.20675519E-02,-0.23030315E-02,-0.20421813E-02, 0.58816546E-02,
        -0.34662937E-02,-0.25914689E-02,-0.58493330E-02,-0.10939762E-02,
        -0.78538718E-03, 0.50396827E-03, 0.24742493E-03, 0.13560815E-02,
         0.40383707E-03, 0.12134307E-02, 0.27528798E-03,-0.50361622E-02,
        -0.88303974E-02,-0.56616454E-02, 0.21213223E-02, 0.35028352E-03,
         0.10856499E-02,-0.45021725E-03, 0.37557070E-03,-0.29180347E-03,
        -0.12384077E-02,-0.18813984E-02, 0.87119750E-03,-0.79525002E-04,
         0.48043855E-03,-0.94489602E-03,-0.10860071E-02, 0.38527959E-03,
        -0.12212386E-03,-0.45346422E-03, 0.15215204E-04,-0.14400250E-04,
        -0.67471818E-04,-0.90186671E-06, 0.33836678E-03, 0.12114479E-03,
         0.11587791E-03, 0.44640139E-03, 0.55491971E-03,-0.12682592E-02,
         0.29673148E-03, 0.23908403E-02,-0.16068619E-03, 0.18305206E-03,
         0.22495163E-02, 0.15045328E-03,-0.10640321E-02, 0.61711257E-05,
        -0.61661366E-03,-0.43130368E-02,-0.32566187E-02, 0.16398317E-02,
         0.27697950E-02, 0.56247402E-03,-0.35284550E-03,-0.90954435E-03,
         0.97014226E-05,-0.23656694E-04,-0.16036749E-03, 0.17879020E-02,
         0.35218967E-04, 0.25009724E-04, 0.37791212E-02, 0.11760202E-03,
         0.78591758E-04, 0.42163525E-02, 0.30502751E-02, 0.27813842E-02,
        -0.66404039E-03, 0.96460176E-03,-0.93610717E-04, 0.17232873E-03,
         0.67871966E-03,-0.11030607E-02, 0.81144062E-04,-0.35704020E-03,
         0.47142297E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]    *x23    *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 35]    *x24*x33        
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]*x11    *x31    *x51
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 44]        *x32*x41*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33*x42    
        +coeff[ 47]    *x21*x31*x42*x51
        +coeff[ 48]        *x31    *x53
        +coeff[ 49]    *x21*x32*x41*x51
        +coeff[ 50]    *x21            
        +coeff[ 51]                *x51
        +coeff[ 52]*x12        *x41    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 53]*x11        *x41*x51
        +coeff[ 54]            *x43*x51
        +coeff[ 55]        *x33    *x51
        +coeff[ 56]*x11*x21*x31    *x51
        +coeff[ 57]*x11*x21    *x43    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]    *x22*x33        
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 62]*x11*x22    *x41*x51
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x41*x53
        +coeff[ 66]    *x22*x31*x42*x51
        +coeff[ 67]    *x21    *x43*x52
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x22*x31*x44    
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 71]    *x23*x31*x44    
        +coeff[ 72]    *x23*x32*x43    
        +coeff[ 73]    *x23*x33*x42    
        +coeff[ 74]    *x21    *x45*x52
        +coeff[ 75]    *x24    *x45    
        +coeff[ 76]    *x22            
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]        *x34*x41    
        +coeff[ 79]    *x21    *x45    
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 80]*x11    *x31    *x52
        +coeff[ 81]    *x22    *x44    
        +coeff[ 82]    *x21*x31*x44    
        +coeff[ 83]        *x31*x42*x52
        +coeff[ 84]    *x21*x33    *x51
        +coeff[ 85]    *x21*x32*x43    
        +coeff[ 86]    *x23*x31*x42    
        +coeff[ 87]    *x21*x33*x42    
        +coeff[ 88]    *x22    *x43*x51
    ;
    v_y_e_q3ex_1_500                              =v_y_e_q3ex_1_500                              
        +coeff[ 89]    *x21*x34*x41    
        +coeff[ 90]*x11*x21    *x41*x52
        +coeff[ 91]*x11*x22*x31*x42    
        +coeff[ 92]    *x23*x33        
        +coeff[ 93]    *x22    *x45    
        +coeff[ 94]    *x21*x31    *x53
        +coeff[ 95]    *x22*x32*x41*x51
        +coeff[ 96]        *x33*x44    
        ;

    return v_y_e_q3ex_1_500                              ;
}
float p_e_q3ex_1_500                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.6944912E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.78458193E-04, 0.16431745E-01,-0.32452606E-01, 0.16082138E-01,
        -0.79721790E-02,-0.98743206E-02, 0.73921247E-02,-0.53531252E-03,
        -0.30061970E-04, 0.11109824E-03, 0.48558027E-02, 0.41239369E-02,
         0.21394275E-02,-0.59540832E-03, 0.30003027E-02, 0.14606296E-02,
         0.10943592E-02, 0.79350901E-03, 0.41051982E-04,-0.16870080E-03,
         0.48151653E-03, 0.14750148E-02,-0.11164958E-02,-0.33230329E-03,
        -0.15108264E-02,-0.62333839E-03,-0.54815219E-03,-0.53401192E-03,
         0.28688090E-04,-0.21370567E-03,-0.60312549E-03,-0.71156865E-04,
         0.16548534E-03, 0.10475727E-03,-0.21000333E-03, 0.10748601E-02,
         0.87874336E-03,-0.43097811E-03, 0.21201732E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_500                              =v_p_e_q3ex_1_500                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]    *x22*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_1_500                              =v_p_e_q3ex_1_500                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]    *x21    *x43    
    ;
    v_p_e_q3ex_1_500                              =v_p_e_q3ex_1_500                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]            *x43*x51
    ;
    v_p_e_q3ex_1_500                              =v_p_e_q3ex_1_500                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21*x32*x41*x51
        +coeff[ 38]    *x21    *x43*x51
        ;

    return v_p_e_q3ex_1_500                              ;
}
float l_e_q3ex_1_500                              (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.1496384E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.14839216E-01,-0.31368941E+00,-0.25349846E-01, 0.12313318E-01,
        -0.32991860E-01,-0.12248883E-01, 0.23324755E-02, 0.43245137E-03,
        -0.28550108E-02,-0.50003328E-02,-0.54833326E-02,-0.24762556E-02,
         0.59681237E-02, 0.92939800E-02,-0.23972006E-02, 0.40995952E-03,
        -0.93258539E-03, 0.37337663E-02, 0.16657342E-02, 0.67359331E-03,
         0.15738468E-02, 0.87349495E-03, 0.71084796E-03, 0.65126903E-02,
         0.61828346E-03, 0.39664801E-03,-0.36641210E-03,-0.39249618E-03,
         0.69799325E-02, 0.10046653E-02, 0.77160010E-02, 0.94564870E-03,
         0.44036060E-03, 0.70781051E-03, 0.53584023E-03,-0.12246053E-02,
        -0.11902058E-02,-0.13203500E-02, 0.10788506E-01, 0.41610249E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_1_500                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_500                              =v_l_e_q3ex_1_500                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23*x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3ex_1_500                              =v_l_e_q3ex_1_500                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]*x11*x22            
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x34    *x51
        +coeff[ 22]    *x21        *x54
        +coeff[ 23]    *x23*x33*x41    
        +coeff[ 24]            *x42*x51
        +coeff[ 25]                *x53
    ;
    v_l_e_q3ex_1_500                              =v_l_e_q3ex_1_500                              
        +coeff[ 26]*x12        *x41    
        +coeff[ 27]            *x43*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21*x32*x41*x51
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]    *x24*x32        
        +coeff[ 34]    *x24    *x42    
    ;
    v_l_e_q3ex_1_500                              =v_l_e_q3ex_1_500                              
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]    *x21*x31*x41*x53
        +coeff[ 37]*x12        *x42*x52
        +coeff[ 38]    *x23*x32*x42    
        +coeff[ 39]    *x23    *x44    
        ;

    return v_l_e_q3ex_1_500                              ;
}
float x_e_q3ex_1_450                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1104477E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.90215299E-02,-0.72909943E-02, 0.20490193E+00, 0.86677238E-01,
        -0.99784862E-02, 0.24943555E-01, 0.88468892E-02,-0.49532917E-02,
        -0.42411266E-02,-0.97089382E-02,-0.64300220E-02, 0.32422820E-04,
        -0.83998271E-03,-0.49845010E-04, 0.15001788E-02,-0.36931767E-02,
         0.31709576E-02, 0.81093871E-03,-0.31429445E-03, 0.83837169E-03,
        -0.23833697E-03,-0.20271208E-03,-0.24847139E-03, 0.49900747E-03,
        -0.76749340E-04, 0.43723013E-03,-0.85450354E-03, 0.53819956E-03,
         0.17961906E-02,-0.25496066E-02,-0.16943440E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_450                              =v_x_e_q3ex_1_450                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]        *x32        
        +coeff[ 13]*x12*x21            
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22    *x42    
    ;
    v_x_e_q3ex_1_450                              =v_x_e_q3ex_1_450                              
        +coeff[ 17]                *x53
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x11            *x51
        +coeff[ 21]*x11*x21            
        +coeff[ 22]*x11*x22            
        +coeff[ 23]        *x31*x41*x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]    *x23            
    ;
    v_x_e_q3ex_1_450                              =v_x_e_q3ex_1_450                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_450                              ;
}
float t_e_q3ex_1_450                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7362946E-02;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.61356872E-02,-0.29173268E-01, 0.75887993E-01, 0.33099204E-02,
        -0.57503399E-02,-0.20507206E-02,-0.21539801E-02,-0.30437767E-03,
        -0.10478117E-02,-0.10745965E-02, 0.10016571E-02, 0.23489706E-03,
         0.29407995E-03,-0.32961491E-03, 0.44960191E-03, 0.14876337E-02,
        -0.22630647E-03,-0.10475582E-03, 0.22884228E-03, 0.22730851E-03,
         0.42568019E-03,-0.49258344E-03, 0.45688121E-03, 0.28802702E-03,
        -0.57618297E-03,-0.31214955E-03,-0.13841853E-03, 0.53607777E-03,
        -0.12584985E-03, 0.75030352E-04,-0.63411244E-04, 0.75907665E-04,
         0.12047464E-03, 0.57770638E-04,-0.15307928E-03, 0.15170561E-03,
         0.17286401E-03,-0.25753953E-03,-0.34806551E-03, 0.60287083E-03,
        -0.12358373E-03,-0.24905172E-04,-0.58404319E-04, 0.44819419E-04,
         0.57120247E-04, 0.84986365E-04,-0.27964782E-03, 0.98019475E-04,
         0.12105420E-03,-0.15384884E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_450                              =v_t_e_q3ex_1_450                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]        *x32        
    ;
    v_t_e_q3ex_1_450                              =v_t_e_q3ex_1_450                              
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_450                              =v_t_e_q3ex_1_450                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]*x11*x23            
        +coeff[ 33]*x11*x21*x31*x41    
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_q3ex_1_450                              =v_t_e_q3ex_1_450                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x11*x21    *x42    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_q3ex_1_450                              =v_t_e_q3ex_1_450                              
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x31*x41*x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_q3ex_1_450                              ;
}
float y_e_q3ex_1_450                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2077064E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.30527168E-03, 0.90747349E-01,-0.98163933E-01,-0.68602026E-01,
        -0.26440937E-01, 0.53627204E-01, 0.24447577E-01,-0.36971770E-01,
        -0.13043175E-01,-0.91337813E-02, 0.24538573E-02,-0.95504057E-02,
         0.59466902E-02,-0.30794094E-03,-0.14114848E-02, 0.10455424E-02,
        -0.55916924E-02, 0.20060174E-02,-0.23113168E-02,-0.20149979E-02,
        -0.32662074E-02,-0.26422266E-02,-0.57172156E-02,-0.11409051E-02,
         0.49196969E-03,-0.78227656E-03, 0.12966938E-02, 0.42262492E-02,
         0.15629103E-02, 0.14043926E-02, 0.54385787E-03,-0.53681820E-02,
        -0.89183934E-02,-0.59938021E-02, 0.20758889E-02, 0.64318156E-04,
        -0.54360371E-04, 0.90919976E-03,-0.48272448E-03, 0.45482939E-03,
        -0.29141110E-03,-0.17075622E-02,-0.98139630E-03, 0.82523160E-03,
        -0.75081480E-04, 0.39517923E-04,-0.81399077E-04, 0.51750668E-03,
        -0.84810890E-03, 0.11233064E-03,-0.12879517E-02, 0.31342384E-03,
        -0.13278492E-03,-0.44625250E-03,-0.68788751E-04,-0.33492629E-05,
         0.32401655E-03, 0.11986974E-03,-0.80545538E-03, 0.54158462E-03,
        -0.22966589E-03, 0.73322450E-03, 0.39700986E-03, 0.20395736E-02,
        -0.16051027E-03, 0.18507444E-03, 0.20373596E-02, 0.20116953E-03,
        -0.32730072E-03,-0.87405305E-03,-0.35538299E-02,-0.20214703E-02,
         0.16555836E-03, 0.19402414E-02, 0.10978056E-02,-0.32291232E-03,
        -0.13631702E-02,-0.19661942E-03, 0.19083562E-04, 0.10609087E-04,
         0.34806460E-04,-0.24032674E-04, 0.37379836E-04, 0.32824406E-04,
        -0.77685894E-04, 0.76288352E-03, 0.12865319E-03, 0.11013573E-03,
         0.18751922E-02,-0.37503854E-04, 0.12308689E-03, 0.12784378E-02,
        -0.71655685E-03, 0.47529276E-03,-0.12585408E-02, 0.97464253E-04,
        -0.48088847E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x33        
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x23*x31        
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x23    *x41*x51
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 35]    *x21            
        +coeff[ 36]                *x51
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x21*x31    *x52
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 44]    *x24    *x43    
        +coeff[ 45]    *x22            
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]*x11*x21*x31    *x51
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]    *x21*x31*x42*x51
        +coeff[ 52]        *x31    *x53
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 53]    *x21*x32*x41*x51
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]*x11        *x41*x51
        +coeff[ 56]            *x43*x51
        +coeff[ 57]        *x33    *x51
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]        *x34*x41    
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 62]*x11*x21*x32*x41    
        +coeff[ 63]    *x23    *x43    
        +coeff[ 64]*x11*x22    *x41*x51
        +coeff[ 65]    *x22*x31    *x52
        +coeff[ 66]    *x23*x32*x41    
        +coeff[ 67]    *x21    *x41*x53
        +coeff[ 68]    *x21    *x43*x52
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]    *x22*x31*x44    
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 71]    *x22*x32*x43    
        +coeff[ 72]*x11    *x31*x42*x52
        +coeff[ 73]    *x23*x31*x44    
        +coeff[ 74]    *x21*x33*x44    
        +coeff[ 75]    *x23*x33*x42    
        +coeff[ 76]    *x24*x31*x42*x51
        +coeff[ 77]    *x24    *x45    
        +coeff[ 78]        *x31*x41    
        +coeff[ 79]    *x21        *x51
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 80]    *x22    *x42    
        +coeff[ 81]*x12    *x31        
        +coeff[ 82]*x11*x22*x31        
        +coeff[ 83]*x12*x21    *x41    
        +coeff[ 84]            *x41*x53
        +coeff[ 85]    *x21    *x45    
        +coeff[ 86]        *x31*x42*x52
        +coeff[ 87]    *x21*x33    *x51
        +coeff[ 88]    *x21*x32*x43    
    ;
    v_y_e_q3ex_1_450                              =v_y_e_q3ex_1_450                              
        +coeff[ 89]*x11*x21    *x44    
        +coeff[ 90]        *x31*x44*x51
        +coeff[ 91]    *x23*x31*x42    
        +coeff[ 92]    *x22    *x43*x51
        +coeff[ 93]    *x23*x33        
        +coeff[ 94]    *x22    *x45    
        +coeff[ 95]    *x21*x31    *x53
        +coeff[ 96]    *x22*x32*x41*x51
        ;

    return v_y_e_q3ex_1_450                              ;
}
float p_e_q3ex_1_450                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.2962326E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.60746748E-04, 0.16597813E-01,-0.32152038E-01, 0.16057579E-01,
        -0.79967845E-02,-0.98942555E-02, 0.73853214E-02,-0.51983487E-03,
        -0.38779541E-04, 0.98238474E-04, 0.48373081E-02, 0.41227941E-02,
         0.21502841E-02,-0.59228233E-03, 0.29768795E-02, 0.14495958E-02,
         0.10872005E-02, 0.80466142E-03, 0.70045615E-04,-0.16691587E-03,
         0.47973628E-03, 0.14471732E-02,-0.10931207E-02,-0.33661045E-03,
        -0.15168741E-02,-0.63005433E-03,-0.56626264E-03,-0.53659547E-03,
        -0.35676035E-05,-0.19871417E-03,-0.58504893E-03,-0.70301139E-04,
         0.17091206E-03, 0.10990060E-03,-0.20974240E-03, 0.11376477E-02,
         0.91712968E-03,-0.45817895E-03, 0.23423240E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_450                              =v_p_e_q3ex_1_450                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]    *x22*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_q3ex_1_450                              =v_p_e_q3ex_1_450                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]    *x21    *x43    
    ;
    v_p_e_q3ex_1_450                              =v_p_e_q3ex_1_450                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]*x11    *x31    *x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]            *x43*x51
    ;
    v_p_e_q3ex_1_450                              =v_p_e_q3ex_1_450                              
        +coeff[ 35]    *x22*x31*x42    
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21*x32*x41*x51
        +coeff[ 38]    *x21    *x43*x51
        ;

    return v_p_e_q3ex_1_450                              ;
}
float l_e_q3ex_1_450                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1666489E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29970E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.16442962E-01,-0.31382054E+00,-0.25245085E-01, 0.12331023E-01,
        -0.32997839E-01,-0.12483991E-01,-0.57025305E-02,-0.26752031E-02,
        -0.55701453E-02,-0.24575877E-02, 0.22798888E-02, 0.83131464E-02,
         0.85612116E-02, 0.21009394E-02,-0.88143256E-03, 0.32470084E-02,
        -0.16333096E-02, 0.15324845E-02, 0.87541441E-03,-0.66973321E-03,
         0.87834708E-03, 0.55310084E-03, 0.41461884E-03, 0.16597676E-02,
         0.73990282E-02, 0.58334116E-02, 0.23906815E-02,-0.13844620E-02,
         0.16724205E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3ex_1_450                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42    
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_1_450                              =v_l_e_q3ex_1_450                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_q3ex_1_450                              =v_l_e_q3ex_1_450                              
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]*x11*x22            
        +coeff[ 19]    *x23            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_1_450                              =v_l_e_q3ex_1_450                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x22*x32    *x51
        +coeff[ 28]    *x24    *x42    
        ;

    return v_l_e_q3ex_1_450                              ;
}
float x_e_q3ex_1_449                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.8528806E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.89102192E-02, 0.20367131E+00, 0.86520284E-01,-0.98494245E-02,
         0.24846137E-01, 0.88372501E-02,-0.20385691E-04,-0.71200896E-02,
        -0.55142511E-02,-0.42122519E-02,-0.96990298E-02,-0.68847160E-02,
         0.97660406E-04,-0.55670267E-03,-0.10639747E-02, 0.15296943E-02,
         0.63512463E-03,-0.40356056E-02, 0.35192459E-02,-0.24822020E-03,
         0.77582954E-03,-0.34387974E-03, 0.83042524E-03,-0.19178378E-03,
        -0.28724392E-03, 0.58811076E-03,-0.94307674E-04,-0.97636296E-03,
         0.64028380E-03, 0.17416115E-02,-0.29846826E-02,-0.19320649E-02,
        -0.11003724E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_449                              =v_x_e_q3ex_1_449                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23            
    ;
    v_x_e_q3ex_1_449                              =v_x_e_q3ex_1_449                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]*x11            *x51
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x21            
        +coeff[ 24]*x11*x22            
        +coeff[ 25]        *x31*x41*x51
    ;
    v_x_e_q3ex_1_449                              =v_x_e_q3ex_1_449                              
        +coeff[ 26]            *x42*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21*x31*x41*x53
        ;

    return v_x_e_q3ex_1_449                              ;
}
float t_e_q3ex_1_449                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6521862E-02;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.60934830E-02,-0.30571895E-01, 0.74385725E-01, 0.31818263E-02,
        -0.56005502E-02,-0.22495571E-02,-0.22683209E-02,-0.23050609E-03,
        -0.11932399E-02,-0.99226076E-03, 0.95444208E-03, 0.24776338E-03,
         0.25784131E-03,-0.42927760E-03, 0.49755274E-03, 0.16037197E-02,
         0.49553160E-03,-0.28523221E-03,-0.95976364E-04, 0.15010899E-03,
         0.53792284E-03,-0.62609493E-03, 0.22306271E-03, 0.28243847E-03,
        -0.64439379E-03,-0.43195425E-03,-0.12370750E-03, 0.48661415E-03,
         0.75024676E-04,-0.58830985E-04, 0.16875738E-03, 0.12358501E-03,
        -0.90452937E-04,-0.16644280E-03, 0.17408172E-03, 0.26162493E-03,
         0.34976553E-03,-0.31677773E-03, 0.61911100E-03,-0.11823788E-04,
        -0.29804540E-04, 0.67951660E-04, 0.17467888E-03,-0.28271004E-03,
        -0.12763210E-03,-0.17951387E-03, 0.13841760E-03,-0.11966766E-03,
         0.12586704E-03, 0.21421209E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_449                              =v_t_e_q3ex_1_449                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x21*x32    *x51
    ;
    v_t_e_q3ex_1_449                              =v_t_e_q3ex_1_449                              
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_449                              =v_t_e_q3ex_1_449                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]*x11*x23            
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_1_449                              =v_t_e_q3ex_1_449                              
        +coeff[ 35]    *x23*x32        
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x12                
        +coeff[ 40]*x11            *x52
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]    *x23*x31*x41    
        +coeff[ 43]        *x32*x42*x51
    ;
    v_t_e_q3ex_1_449                              =v_t_e_q3ex_1_449                              
        +coeff[ 44]        *x31*x43*x51
        +coeff[ 45]    *x21*x31*x41*x52
        +coeff[ 46]    *x22        *x53
        +coeff[ 47]        *x32    *x53
        +coeff[ 48]*x11*x23*x31*x41    
        +coeff[ 49]    *x22*x31*x43    
        ;

    return v_t_e_q3ex_1_449                              ;
}
float y_e_q3ex_1_449                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.8556150E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.84792153E-03, 0.99812135E-01,-0.10539434E+00,-0.70585214E-01,
        -0.30128108E-01, 0.53720243E-01, 0.27068198E-01,-0.38882192E-01,
        -0.15226061E-01,-0.92814788E-02, 0.24496133E-02,-0.10941489E-01,
        -0.72086649E-02,-0.60717732E-04, 0.11571668E-02,-0.60648760E-02,
        -0.16075838E-02, 0.20421604E-02,-0.25971390E-02,-0.20458291E-02,
         0.62976675E-02,-0.33624712E-02,-0.25797896E-02, 0.56582817E-03,
        -0.88984333E-03, 0.15739587E-02, 0.17130856E-02, 0.48182625E-02,
         0.22254027E-02, 0.68669859E-03,-0.49310126E-02,-0.41367756E-02,
         0.21425548E-02,-0.33315134E-02,-0.29219332E-03, 0.11786026E-02,
        -0.48192809E-03,-0.22793456E-02, 0.31812955E-03,-0.32483251E-03,
        -0.64226956E-03,-0.17054625E-02, 0.88264415E-03,-0.90706031E-04,
         0.63608505E-03, 0.15323490E-03, 0.12916666E-03, 0.45651998E-03,
        -0.90085005E-03,-0.14571857E-03,-0.49425114E-03,-0.63123007E-04,
         0.30445139E-03,-0.41035088E-03,-0.60296920E-03,-0.46650713E-03,
        -0.20342515E-03, 0.71669940E-03, 0.16057898E-02,-0.15487989E-03,
         0.15999870E-03, 0.22810814E-02, 0.17972650E-03, 0.69854577E-03,
        -0.26082605E-03,-0.71063504E-03,-0.10600667E-03,-0.44129149E-03,
        -0.10693081E-01,-0.42444233E-04,-0.12632906E-02,-0.27761045E-04,
         0.59247256E-03,-0.11700849E-03, 0.46740437E-03, 0.37041311E-04,
        -0.75600379E-04, 0.41903331E-03, 0.13130707E-03, 0.14098198E-03,
         0.10772838E-02, 0.19073187E-02,-0.81829814E-03, 0.17562229E-03,
        -0.11235232E-02,-0.10607193E-03, 0.68043264E-04,-0.56091056E-03,
        -0.95530972E-02,-0.23171459E-02, 0.27138204E-03,-0.34646550E-02,
        -0.61654095E-02, 0.24355298E-03,-0.22377088E-02,-0.15956453E-02,
         0.23964122E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]    *x23*x31        
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x32*x41    
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]*x11    *x31    *x51
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 44]        *x32*x41*x51
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]*x11*x21*x31    *x51
        +coeff[ 47]    *x21*x31*x42*x51
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]        *x31    *x53
        +coeff[ 50]    *x21*x32*x41*x51
        +coeff[ 51]*x12        *x41    
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 53]        *x31*x44    
        +coeff[ 54]        *x32*x43    
        +coeff[ 55]        *x33*x42    
        +coeff[ 56]        *x34*x41    
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]    *x23    *x43    
        +coeff[ 59]*x11*x22    *x41*x51
        +coeff[ 60]    *x22*x31    *x52
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 62]    *x21    *x41*x53
        +coeff[ 63]    *x23*x33        
        +coeff[ 64]    *x21    *x43*x52
        +coeff[ 65]    *x24    *x41*x51
        +coeff[ 66]*x11*x21    *x45    
        +coeff[ 67]    *x24*x31    *x51
        +coeff[ 68]    *x22*x32*x43    
        +coeff[ 69]*x11*x21*x34*x41    
        +coeff[ 70]    *x22*x32*x45    
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11*x21    *x43    
        +coeff[ 73]            *x41*x53
        +coeff[ 74]    *x21    *x45    
        +coeff[ 75]*x11    *x31    *x52
        +coeff[ 76]            *x43*x52
        +coeff[ 77]*x11*x21*x32*x41    
        +coeff[ 78]        *x31*x42*x52
        +coeff[ 79]    *x21*x33    *x51
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 80]    *x21*x32*x43    
        +coeff[ 81]    *x23*x31*x42    
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]*x11*x22*x31*x42    
        +coeff[ 84]    *x22*x31*x42*x51
        +coeff[ 85]        *x32*x45    
        +coeff[ 86]    *x21*x31    *x53
        +coeff[ 87]    *x22*x32*x41*x51
        +coeff[ 88]    *x22*x31*x44    
    ;
    v_y_e_q3ex_1_449                              =v_y_e_q3ex_1_449                              
        +coeff[ 89]    *x24    *x43    
        +coeff[ 90]    *x23    *x41*x52
        +coeff[ 91]    *x24*x31*x42    
        +coeff[ 92]    *x22*x33*x42    
        +coeff[ 93]    *x23    *x43*x51
        +coeff[ 94]    *x24*x32*x41    
        +coeff[ 95]    *x22*x34*x41    
        +coeff[ 96]    *x22    *x41*x53
        ;

    return v_y_e_q3ex_1_449                              ;
}
float p_e_q3ex_1_449                              (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.2431298E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.24159785E-03, 0.16558023E-01,-0.33083785E-01, 0.15585406E-01,
        -0.83355289E-02,-0.91901757E-02, 0.71831224E-02,-0.21892702E-03,
         0.15348173E-03, 0.20422232E-03,-0.24580615E-03, 0.49981074E-02,
         0.44145631E-02, 0.22536034E-02, 0.31468368E-03,-0.57013566E-03,
         0.32092971E-02, 0.13721711E-02, 0.67843846E-03,-0.96166686E-05,
        -0.17584325E-03, 0.61886059E-03, 0.17089500E-02, 0.94667624E-03,
        -0.94711024E-03,-0.31529600E-03,-0.95954951E-03,-0.55925566E-03,
        -0.47049974E-03, 0.41902003E-05,-0.19951549E-04,-0.22063543E-03,
        -0.30572628E-03,-0.72806783E-04, 0.18092914E-03, 0.10666138E-03,
        -0.17469208E-03,-0.25554374E-03,-0.22510516E-03, 0.16586404E-03,
         0.12290828E-02, 0.85900060E-03,-0.51073520E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_449                              =v_p_e_q3ex_1_449                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x25*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x21*x31    *x53
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]        *x31*x42    
    ;
    v_p_e_q3ex_1_449                              =v_p_e_q3ex_1_449                              
        +coeff[ 17]            *x43    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x21*x31    *x51
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_1_449                              =v_p_e_q3ex_1_449                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]    *x21    *x41*x51
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_p_e_q3ex_1_449                              =v_p_e_q3ex_1_449                              
        +coeff[ 35]*x11        *x41*x51
        +coeff[ 36]        *x33    *x51
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x22*x31*x42    
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]    *x21*x32*x41*x51
        ;

    return v_p_e_q3ex_1_449                              ;
}
float l_e_q3ex_1_449                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.1752042E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.16927766E-01,-0.31610167E+00,-0.25222402E-01, 0.12220399E-01,
        -0.33692047E-01,-0.12219605E-01,-0.53097205E-02,-0.55499198E-02,
        -0.23088937E-02, 0.22118902E-02, 0.77977008E-02, 0.51551955E-02,
         0.18278239E-03, 0.21411135E-03, 0.44378890E-02,-0.34718567E-02,
        -0.86169096E-03,-0.17434858E-02, 0.20591640E-02,-0.19702266E-02,
         0.15250879E-02, 0.69242984E-03, 0.10806565E-02, 0.56576642E-03,
         0.42599745E-03,-0.30577974E-03, 0.63110259E-02, 0.69280122E-02,
         0.11335859E-01, 0.86558592E-02, 0.31921223E-02, 0.75779730E-02,
        -0.14888742E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_q3ex_1_449                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3ex_1_449                              =v_l_e_q3ex_1_449                              
        +coeff[  8]                *x52
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3ex_1_449                              =v_l_e_q3ex_1_449                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x34    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x21        *x52
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_q3ex_1_449                              =v_l_e_q3ex_1_449                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x32*x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]    *x21    *x44    
        +coeff[ 31]    *x23*x33*x41    
        +coeff[ 32]*x11    *x34*x41*x51
        ;

    return v_l_e_q3ex_1_449                              ;
}
float x_e_q3ex_1_400                              (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.9061660E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.94231926E-02,-0.71102758E-02, 0.20355575E+00, 0.86880438E-01,
        -0.98721441E-02, 0.24867371E-01, 0.89171510E-02,-0.54949666E-02,
        -0.41776588E-02,-0.96225012E-02,-0.68506077E-02, 0.18625356E-04,
        -0.75012801E-03,-0.10449468E-02, 0.15795256E-02, 0.66610344E-03,
        -0.39893868E-02, 0.34067812E-02,-0.24426135E-03,-0.21700280E-03,
         0.78424142E-03,-0.34814380E-03, 0.84586820E-03,-0.31957179E-03,
         0.60795160E-03, 0.56093169E-03, 0.15477084E-02,-0.31843672E-02,
        -0.20914879E-02,-0.25615192E-03,-0.18445117E-02,-0.21915282E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_400                              =v_x_e_q3ex_1_400                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_400                              =v_x_e_q3ex_1_400                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_1_400                              =v_x_e_q3ex_1_400                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21    *x42*x53
        +coeff[ 30]    *x23    *x42*x51
        +coeff[ 31]    *x23*x31*x41*x53
        ;

    return v_x_e_q3ex_1_400                              ;
}
float t_e_q3ex_1_400                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7357392E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.69274576E-02,-0.30650875E-01, 0.74307993E-01, 0.31959678E-02,
        -0.55801608E-02,-0.22444688E-02,-0.22459214E-02,-0.23133366E-03,
        -0.11849216E-02,-0.97507949E-03, 0.95603603E-03, 0.26289796E-03,
         0.26454800E-03,-0.36507795E-03, 0.48438838E-03, 0.15395243E-02,
         0.48786998E-03,-0.27655234E-03,-0.95952695E-04, 0.22231828E-03,
         0.52103616E-03,-0.64471038E-03, 0.20495430E-03, 0.32073009E-03,
        -0.62091841E-03,-0.43255978E-03, 0.51936897E-03, 0.53076883E-03,
         0.83315543E-04,-0.55710614E-04, 0.87634384E-04, 0.10091888E-03,
        -0.15971628E-03, 0.15266403E-03, 0.33546286E-03,-0.28336904E-03,
        -0.20550258E-04,-0.12542124E-03,-0.28922057E-04,-0.49374143E-04,
         0.18020181E-04,-0.77866702E-04,-0.12068210E-03, 0.29743092E-04,
         0.99992198E-04,-0.25366663E-03, 0.11135433E-03, 0.10948921E-03,
        -0.20736571E-03,-0.12130405E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_400                              =v_t_e_q3ex_1_400                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x21*x32    *x51
    ;
    v_t_e_q3ex_1_400                              =v_t_e_q3ex_1_400                              
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_400                              =v_t_e_q3ex_1_400                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23        *x53
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]*x11*x23            
        +coeff[ 32]        *x31*x41*x52
        +coeff[ 33]            *x42*x52
        +coeff[ 34]    *x23    *x42    
    ;
    v_t_e_q3ex_1_400                              =v_t_e_q3ex_1_400                              
        +coeff[ 35]    *x21*x32    *x52
        +coeff[ 36]*x11*x22            
        +coeff[ 37]        *x31*x41*x51
        +coeff[ 38]*x11            *x52
        +coeff[ 39]*x11*x21*x32        
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]*x11*x21    *x42    
        +coeff[ 42]    *x21        *x53
        +coeff[ 43]*x13    *x32        
    ;
    v_t_e_q3ex_1_400                              =v_t_e_q3ex_1_400                              
        +coeff[ 44]    *x23*x32        
        +coeff[ 45]    *x21*x32*x42    
        +coeff[ 46]    *x22*x31*x41*x51
        +coeff[ 47]        *x33*x41*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q3ex_1_400                              ;
}
float y_e_q3ex_1_400                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1116091E-02;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.99496660E-03, 0.99265918E-01,-0.10587140E+00,-0.70474654E-01,
        -0.29927775E-01, 0.53611632E-01, 0.27048497E-01,-0.38674206E-01,
        -0.15177178E-01,-0.92140222E-02, 0.24511009E-02,-0.11165489E-01,
        -0.72490699E-02, 0.53556294E-04, 0.11645752E-02,-0.61350288E-02,
        -0.16328228E-02, 0.20676905E-02,-0.25818758E-02,-0.20887470E-02,
         0.62370207E-02,-0.32801784E-02,-0.26793731E-02, 0.59312279E-03,
        -0.87484205E-03, 0.17177785E-02, 0.13736761E-02,-0.64158658E-02,
        -0.55855783E-02, 0.15154433E-02, 0.22238651E-02, 0.56716846E-02,
         0.29277490E-02, 0.94467856E-03,-0.32016425E-02, 0.45418441E-04,
         0.64692256E-03, 0.11435311E-02, 0.42354898E-03,-0.50116814E-03,
        -0.30155669E-02, 0.33144609E-03,-0.33681290E-03,-0.18614160E-02,
        -0.10304304E-02, 0.44071646E-02, 0.94167195E-03,-0.12841286E-02,
        -0.33884280E-04, 0.61029580E-03,-0.80315076E-04, 0.61595021E-03,
         0.15408681E-03, 0.14162333E-03,-0.11599796E-02,-0.13805299E-03,
        -0.55615488E-03, 0.27683733E-04,-0.58956219E-04, 0.30722152E-03,
        -0.44677494E-03, 0.73974731E-03, 0.21241345E-02,-0.19633961E-03,
         0.34570117E-02, 0.33245373E-02, 0.14307356E-03, 0.11140646E-02,
         0.14495144E-03,-0.78344817E-03,-0.90523949E-02,-0.89509813E-04,
        -0.10001318E-01,-0.56366464E-02,-0.16418250E-02,-0.74435055E-04,
         0.51591697E-03,-0.23127077E-04, 0.58130262E-03,-0.73897580E-04,
         0.45344047E-03, 0.51705807E-03,-0.31019616E-04, 0.13652661E-03,
         0.88871035E-04,-0.61657699E-03,-0.11976991E-03, 0.16772580E-03,
         0.15581472E-03,-0.95457450E-03,-0.70404079E-04, 0.79085890E-04,
        -0.27916080E-03,-0.40562372E-03,-0.35278563E-03, 0.24552504E-03,
        -0.13202884E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 26]    *x23*x31        
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23    *x41*x51
        +coeff[ 31]    *x21*x32*x43    
        +coeff[ 32]    *x23*x32*x41    
        +coeff[ 33]    *x23*x33        
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 35]    *x21            
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]    *x21*x31*x44    
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]    *x24    *x43    
        +coeff[ 48]                *x51
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]*x11    *x31    *x51
        +coeff[ 51]        *x32*x41*x51
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 53]*x11*x21*x31    *x51
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]        *x31    *x53
        +coeff[ 56]    *x21*x32*x41*x51
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]            *x43*x51
        +coeff[ 60]        *x32*x43    
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]*x11*x22    *x41*x51
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]    *x21*x33*x42    
        +coeff[ 66]    *x22*x31    *x52
        +coeff[ 67]    *x21*x34*x41    
        +coeff[ 68]        *x32*x45    
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]    *x22*x31*x44    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 71]*x11*x21    *x45    
        +coeff[ 72]    *x22*x32*x43    
        +coeff[ 73]    *x22*x33*x42    
        +coeff[ 74]    *x22*x34*x41    
        +coeff[ 75]*x11*x21*x34*x41    
        +coeff[ 76]    *x22*x31*x42*x52
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11*x21    *x43    
        +coeff[ 79]    *x21    *x43*x51
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 80]    *x21*x31*x42*x51
        +coeff[ 81]*x11*x21*x32*x41    
        +coeff[ 82]*x11    *x31*x42*x51
        +coeff[ 83]    *x21*x33    *x51
        +coeff[ 84]*x11    *x32*x41*x51
        +coeff[ 85]    *x22    *x43*x51
        +coeff[ 86]*x11*x21    *x41*x52
        +coeff[ 87]    *x21    *x41*x53
        +coeff[ 88]*x11*x22*x31*x42    
    ;
    v_y_e_q3ex_1_400                              =v_y_e_q3ex_1_400                              
        +coeff[ 89]    *x22*x31*x42*x51
        +coeff[ 90]*x11*x21*x31    *x52
        +coeff[ 91]    *x21*x31    *x53
        +coeff[ 92]    *x21    *x43*x52
        +coeff[ 93]    *x22*x32*x41*x51
        +coeff[ 94]    *x24*x31    *x51
        +coeff[ 95]    *x23    *x41*x52
        +coeff[ 96]    *x24*x31*x42    
        ;

    return v_y_e_q3ex_1_400                              ;
}
float p_e_q3ex_1_400                              (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.2746123E-03;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.24168135E-03, 0.16620107E-01,-0.32901254E-01, 0.15582287E-01,
        -0.83243232E-02,-0.92206141E-02, 0.45743100E-02, 0.71771545E-02,
        -0.48134141E-03, 0.10853669E-03, 0.82812301E-04, 0.50431858E-02,
         0.22543247E-02, 0.34742578E-03,-0.56249194E-03, 0.31733534E-02,
         0.13594287E-02, 0.69913000E-03,-0.16756063E-04,-0.17510519E-03,
         0.61234285E-03, 0.17135105E-02, 0.93795947E-03,-0.96305984E-03,
        -0.32015200E-03,-0.98718272E-03,-0.57987781E-03,-0.52163226E-03,
        -0.64283510E-03, 0.24411416E-04,-0.22475803E-03,-0.33455982E-03,
        -0.71041613E-04, 0.18496280E-03, 0.10424553E-03,-0.13281680E-03,
        -0.16898671E-03, 0.15932048E-03, 0.12353564E-02, 0.89010282E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_400                              =v_p_e_q3ex_1_400                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x21*x31    *x53
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_400                              =v_p_e_q3ex_1_400                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_400                              =v_p_e_q3ex_1_400                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]    *x21*x32*x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_400                              =v_p_e_q3ex_1_400                              
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_400                              ;
}
float l_e_q3ex_1_400                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2054956E-01;
    float xmin[10]={
        -0.39994E-02,-0.59863E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.20106960E-01,-0.31714615E+00,-0.25410347E-01, 0.12240378E-01,
        -0.34291986E-01,-0.12060511E-01, 0.23592138E-02,-0.47716507E-03,
        -0.34536677E-02,-0.59634871E-02,-0.55690575E-02,-0.23389636E-02,
         0.75158221E-02, 0.53747995E-02, 0.28116663E-02,-0.97879674E-03,
        -0.10203854E-02, 0.24730437E-02,-0.22412394E-02, 0.90712134E-03,
         0.15620590E-02, 0.14290392E-02, 0.38751768E-03, 0.25714894E-02,
         0.72280052E-02, 0.62968722E-02, 0.11399195E-01, 0.11873660E-01,
         0.42670649E-02,-0.15509165E-02,-0.57386258E-03, 0.17862142E-02,
         0.19828915E-02, 0.79433754E-03, 0.52396161E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_400                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_400                              =v_l_e_q3ex_1_400                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_1_400                              =v_l_e_q3ex_1_400                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]*x11*x22        *x52
        +coeff[ 22]                *x53
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_1_400                              =v_l_e_q3ex_1_400                              
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]    *x21    *x44    
        +coeff[ 29]    *x21*x31*x41*x52
        +coeff[ 30]*x11        *x43*x51
        +coeff[ 31]    *x24*x32        
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]*x11    *x32    *x53
        +coeff[ 34]    *x23*x33*x41    
        ;

    return v_l_e_q3ex_1_400                              ;
}
float x_e_q3ex_1_350                              (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.9492539E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.99754911E-02,-0.70942161E-02, 0.20353810E+00, 0.87268896E-01,
        -0.98620979E-02, 0.24788672E-01, 0.88824145E-02,-0.55042552E-02,
        -0.42150486E-02,-0.94598429E-02,-0.68681585E-02, 0.31503805E-04,
        -0.60173060E-03,-0.10617706E-02, 0.15672216E-02, 0.67487633E-03,
        -0.40265541E-02, 0.34695067E-02,-0.24001098E-03, 0.77389315E-03,
        -0.34330995E-03, 0.10410935E-02,-0.20643849E-03,-0.29001545E-03,
         0.58679126E-03, 0.63993072E-03, 0.16701149E-02,-0.27595148E-02,
        -0.22097882E-02,-0.14164130E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_350                              =v_x_e_q3ex_1_350                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_350                              =v_x_e_q3ex_1_350                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_1_350                              =v_x_e_q3ex_1_350                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x23    *x42*x51
        ;

    return v_x_e_q3ex_1_350                              ;
}
float t_e_q3ex_1_350                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7212161E-02;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.67666359E-02,-0.30743340E-01, 0.74242018E-01, 0.31475073E-02,
        -0.55722543E-02,-0.22252644E-02,-0.21811547E-02,-0.22724127E-03,
        -0.12282924E-02,-0.10067368E-02, 0.95140573E-03, 0.24416324E-03,
         0.25665731E-03,-0.38753974E-03, 0.43853751E-03, 0.15951743E-02,
         0.49169897E-03,-0.29607341E-03,-0.97384305E-04, 0.22122935E-03,
         0.22145663E-03, 0.55658183E-03,-0.25303557E-03, 0.35306517E-03,
        -0.38329727E-03,-0.91316178E-04, 0.57507888E-03, 0.69421978E-04,
        -0.59390230E-04, 0.59878435E-04, 0.10448858E-03,-0.55950560E-03,
        -0.50323922E-03,-0.19890511E-03, 0.14643204E-03,-0.72682917E-04,
        -0.33629086E-03, 0.51399262E-03,-0.21989837E-04,-0.13733603E-03,
         0.43977168E-04,-0.54395034E-04, 0.13936395E-03, 0.21134432E-03,
         0.15131067E-03, 0.25545142E-03,-0.24292947E-03, 0.14539514E-03,
         0.11668273E-03,-0.14569823E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_350                              =v_t_e_q3ex_1_350                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x21*x32    *x51
    ;
    v_t_e_q3ex_1_350                              =v_t_e_q3ex_1_350                              
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22*x32        
        +coeff[ 22]    *x23    *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x21        *x53
    ;
    v_t_e_q3ex_1_350                              =v_t_e_q3ex_1_350                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]        *x32    *x51
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x23        *x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]            *x42*x52
    ;
    v_t_e_q3ex_1_350                              =v_t_e_q3ex_1_350                              
        +coeff[ 35]*x11*x22        *x52
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]    *x22    *x41    
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11*x21*x31*x41    
        +coeff[ 41]*x11*x21    *x42    
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]    *x23*x32        
    ;
    v_t_e_q3ex_1_350                              =v_t_e_q3ex_1_350                              
        +coeff[ 44]    *x23*x31*x41    
        +coeff[ 45]    *x23    *x42    
        +coeff[ 46]    *x21*x32*x42    
        +coeff[ 47]    *x22*x31*x41*x51
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q3ex_1_350                              ;
}
float y_e_q3ex_1_350                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1776379E-03;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.13640217E-03, 0.98462380E-01,-0.10657748E+00,-0.70580073E-01,
        -0.29962355E-01, 0.53619899E-01, 0.27042132E-01,-0.38704678E-01,
        -0.15019637E-01,-0.92778411E-02, 0.24407124E-02,-0.11013861E-01,
        -0.72415392E-02, 0.64698346E-04, 0.11506561E-02,-0.60957051E-02,
        -0.16183503E-02, 0.20891288E-02,-0.25543962E-02,-0.20846808E-02,
         0.61924257E-02,-0.34192998E-02,-0.27455557E-02, 0.56511827E-03,
        -0.86874602E-03, 0.17793240E-02, 0.14185327E-02,-0.53477770E-03,
        -0.73819002E-02,-0.58395634E-02, 0.14768309E-02, 0.22887369E-02,
         0.56599909E-02, 0.31503080E-02, 0.88130089E-03,-0.30935344E-02,
         0.56658586E-03, 0.11717969E-02, 0.44073613E-03,-0.49957295E-03,
        -0.33815058E-02, 0.30413404E-03,-0.36765213E-03,-0.20788657E-02,
        -0.10530439E-02, 0.96811610E-03,-0.80317969E-03, 0.54612290E-03,
        -0.86046566E-04, 0.60363283E-03, 0.15413077E-03, 0.13225012E-03,
        -0.12324464E-02,-0.15057695E-03,-0.61378255E-03,-0.86547341E-02,
         0.40628121E-03, 0.20595584E-04,-0.15507969E-04,-0.58441150E-04,
         0.18850802E-05, 0.26385082E-03, 0.68748102E-03, 0.21159300E-02,
        -0.17664477E-03, 0.32795595E-02, 0.20968048E-03,-0.72953524E-03,
         0.85222739E-04,-0.86067803E-02, 0.33610204E-03,-0.45913109E-02,
        -0.13316032E-02,-0.49586798E-03, 0.11822964E-03,-0.50214469E-03,
         0.22955015E-03,-0.20450068E-04,-0.31218831E-04,-0.26434904E-03,
         0.41383450E-03,-0.50007198E-04, 0.43881210E-03, 0.28723402E-04,
         0.33440685E-03, 0.39180219E-02, 0.97765937E-04, 0.33599234E-02,
         0.10757869E-02,-0.89062523E-04, 0.15540994E-03,-0.84720372E-03,
         0.75155069E-04,-0.21228334E-03,-0.37406580E-03, 0.35715714E-03,
        -0.27596005E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 26]    *x23*x31        
        +coeff[ 27]        *x32*x43    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x21    *x45    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x21*x32*x43    
        +coeff[ 33]    *x23*x32*x41    
        +coeff[ 34]    *x23*x33        
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 35]    *x22    *x45    
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]    *x24    *x43    
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11    *x31    *x51
        +coeff[ 49]        *x32*x41*x51
        +coeff[ 50]        *x33    *x51
        +coeff[ 51]*x11*x21*x31    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 53]        *x31    *x53
        +coeff[ 54]    *x21*x32*x41*x51
        +coeff[ 55]    *x22*x31*x44    
        +coeff[ 56]    *x22*x31*x42*x52
        +coeff[ 57]    *x21            
        +coeff[ 58]                *x51
        +coeff[ 59]*x12        *x41    
        +coeff[ 60]*x11        *x41*x51
        +coeff[ 61]            *x43*x51
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]    *x23    *x43    
        +coeff[ 64]*x11*x22    *x41*x51
        +coeff[ 65]    *x23*x31*x42    
        +coeff[ 66]    *x21    *x41*x53
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]*x11*x21    *x45    
        +coeff[ 69]    *x22*x32*x43    
        +coeff[ 70]    *x23    *x41*x52
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]    *x22*x34*x41    
        +coeff[ 73]    *x21*x32*x45    
        +coeff[ 74]*x11*x23*x32*x41    
        +coeff[ 75]    *x22    *x45*x51
        +coeff[ 76]    *x24*x31    *x52
        +coeff[ 77]*x12    *x31        
        +coeff[ 78]*x11*x21    *x42    
        +coeff[ 79]        *x33*x42    
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 80]*x11*x21    *x43    
        +coeff[ 81]            *x41*x53
        +coeff[ 82]    *x21*x31*x42*x51
        +coeff[ 83]*x11    *x31    *x52
        +coeff[ 84]*x11*x21*x32*x41    
        +coeff[ 85]    *x21*x31*x44    
        +coeff[ 86]    *x21*x33    *x51
        +coeff[ 87]    *x21*x33*x42    
        +coeff[ 88]    *x21*x34*x41    
    ;
    v_y_e_q3ex_1_350                              =v_y_e_q3ex_1_350                              
        +coeff[ 89]*x11*x21    *x41*x52
        +coeff[ 90]*x11*x22*x31*x42    
        +coeff[ 91]    *x22*x31*x42*x51
        +coeff[ 92]    *x21*x31    *x53
        +coeff[ 93]    *x21    *x43*x52
        +coeff[ 94]    *x22*x32*x41*x51
        +coeff[ 95]    *x21*x31*x42*x52
        +coeff[ 96]    *x24*x31    *x51
        ;

    return v_y_e_q3ex_1_350                              ;
}
float p_e_q3ex_1_350                              (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.4270090E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.55072614E-04, 0.16717216E-01,-0.32617025E-01, 0.15546674E-01,
        -0.83073573E-02,-0.91147376E-02, 0.45605684E-02, 0.71702320E-02,
        -0.50795713E-03, 0.90727612E-04, 0.52976025E-04, 0.51126922E-02,
         0.22500753E-02, 0.29667534E-03,-0.56493742E-03, 0.31886580E-02,
         0.13264254E-02, 0.68890664E-03, 0.58841026E-04,-0.17092554E-03,
         0.61706052E-03, 0.16346942E-02, 0.99738105E-03,-0.97747566E-03,
        -0.30770843E-03,-0.11272664E-02,-0.58746233E-03,-0.48356198E-03,
         0.27817572E-04,-0.25443302E-03,-0.21995924E-03,-0.26817113E-03,
        -0.78878838E-04, 0.17067426E-03, 0.10589597E-03,-0.16443104E-03,
        -0.32084854E-03,-0.29828941E-03, 0.11957197E-02, 0.84816426E-03,
         0.38852534E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_350                              =v_p_e_q3ex_1_350                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x21*x31    *x53
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_350                              =v_p_e_q3ex_1_350                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_350                              =v_p_e_q3ex_1_350                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21    *x41*x51
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_350                              =v_p_e_q3ex_1_350                              
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]            *x43*x51
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x43*x51
        ;

    return v_p_e_q3ex_1_350                              ;
}
float l_e_q3ex_1_350                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1895128E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59967E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.18122369E-01,-0.31786290E+00,-0.25612317E-01, 0.12236170E-01,
        -0.33990815E-01,-0.12076030E-01,-0.33558165E-02,-0.57384544E-02,
        -0.58986600E-02,-0.24390586E-02, 0.22603071E-02, 0.69471444E-02,
         0.45021749E-02, 0.40729074E-02,-0.90491527E-03,-0.17652713E-02,
         0.18224822E-02,-0.15062564E-02, 0.17835811E-02, 0.30539685E-03,
         0.75478968E-03, 0.47061301E-03, 0.48616758E-03, 0.57712366E-03,
        -0.23191278E-03, 0.19257682E-02, 0.12978336E-02, 0.74865860E-02,
         0.76635368E-02, 0.11262620E-01, 0.10219385E-01, 0.37211950E-02,
        -0.14994121E-02, 0.52136374E-02,-0.13824666E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_350                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x32        
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_q3ex_1_350                              =v_l_e_q3ex_1_350                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_1_350                              =v_l_e_q3ex_1_350                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x22*x32    *x51
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]                *x53
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x12    *x31        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_q3ex_1_350                              =v_l_e_q3ex_1_350                              
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x32*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]    *x21    *x44    
        +coeff[ 32]    *x21*x31*x41*x53
        +coeff[ 33]    *x23*x33*x41    
        +coeff[ 34]    *x24        *x53
        ;

    return v_l_e_q3ex_1_350                              ;
}
float x_e_q3ex_1_300                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.8711993E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.90674181E-02, 0.20341727E+00, 0.88038504E-01,-0.98366886E-02,
         0.24888979E-01, 0.89546274E-02,-0.28954939E-04,-0.70524546E-02,
        -0.54710996E-02,-0.42055878E-02,-0.92573455E-02,-0.62487097E-02,
         0.13735528E-04,-0.81647671E-03,-0.10379683E-02, 0.15564158E-02,
         0.74375351E-03,-0.36871738E-02, 0.35049240E-02,-0.23855819E-03,
         0.80301426E-03,-0.34388888E-03, 0.10291513E-02,-0.20809651E-03,
        -0.27752150E-03, 0.60802756E-03,-0.13148338E-03, 0.13464343E-03,
         0.76130568E-03, 0.18133228E-02,-0.37413004E-02,-0.28288004E-02,
        -0.28691029E-02,-0.23330534E-02,-0.16592852E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_300                              =v_x_e_q3ex_1_300                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23            
    ;
    v_x_e_q3ex_1_300                              =v_x_e_q3ex_1_300                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]*x11            *x51
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x21            
        +coeff[ 24]*x11*x22            
        +coeff[ 25]        *x31*x41*x51
    ;
    v_x_e_q3ex_1_300                              =v_x_e_q3ex_1_300                              
        +coeff[ 26]            *x42*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x23    *x42    
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x23    *x42*x51
        ;

    return v_x_e_q3ex_1_300                              ;
}
float t_e_q3ex_1_300                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6844934E-02;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64531430E-02,-0.30944789E-01, 0.74087135E-01, 0.32321117E-02,
        -0.55634077E-02,-0.22647474E-02,-0.22116515E-02,-0.21954210E-03,
        -0.12740759E-02,-0.98828610E-03, 0.10013632E-02, 0.27288371E-03,
         0.23440660E-03,-0.40790375E-03, 0.44589365E-03, 0.15847331E-02,
         0.49002038E-03,-0.26823313E-03,-0.10930026E-03, 0.24256410E-03,
         0.22255996E-03, 0.57414052E-03,-0.65629021E-03, 0.48137704E-03,
        -0.72177825E-03,-0.43233242E-03, 0.16444427E-03,-0.14923159E-03,
         0.53115370E-03, 0.69474103E-04,-0.51112387E-04, 0.14946588E-03,
         0.88092696E-04,-0.73751064E-04, 0.21942548E-03, 0.28333053E-03,
        -0.28871442E-03,-0.15392659E-03,-0.53667539E-03, 0.63931604E-03,
        -0.11964580E-04,-0.49091672E-04,-0.29301738E-04,-0.42777730E-04,
         0.32482505E-04,-0.73299714E-04, 0.46792458E-04, 0.94158662E-04,
        -0.26640261E-03,-0.14685304E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_300                              =v_t_e_q3ex_1_300                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x21*x32    *x51
    ;
    v_t_e_q3ex_1_300                              =v_t_e_q3ex_1_300                              
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22*x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_300                              =v_t_e_q3ex_1_300                              
        +coeff[ 26]            *x42*x52
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]        *x32    *x51
        +coeff[ 32]*x11*x23            
        +coeff[ 33]        *x31*x41*x52
        +coeff[ 34]    *x23*x32        
    ;
    v_t_e_q3ex_1_300                              =v_t_e_q3ex_1_300                              
        +coeff[ 35]    *x23    *x42    
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]        *x32    *x53
        +coeff[ 38]    *x22*x31*x41*x52
        +coeff[ 39]    *x23        *x53
        +coeff[ 40]*x12                
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]*x11            *x52
        +coeff[ 43]*x11*x21*x32        
    ;
    v_t_e_q3ex_1_300                              =v_t_e_q3ex_1_300                              
        +coeff[ 44]*x11*x21*x31*x41    
        +coeff[ 45]*x11*x21    *x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x23*x31*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]    *x21*x31*x43    
        ;

    return v_t_e_q3ex_1_300                              ;
}
float y_e_q3ex_1_300                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.4336717E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.46223475E-03, 0.97660013E-01,-0.10752223E+00,-0.70815556E-01,
        -0.29997792E-01, 0.53679433E-01, 0.27019350E-01,-0.39077807E-01,
        -0.15083629E-01,-0.92924703E-02, 0.24344351E-02,-0.10606337E-01,
        -0.10600971E-03,-0.12364964E-02,-0.41488092E-03, 0.11513971E-02,
        -0.67978413E-02,-0.15348297E-02, 0.21926963E-02,-0.24881854E-02,
        -0.20190915E-02, 0.61670882E-02,-0.34408949E-02,-0.27139452E-02,
        -0.58558634E-02, 0.57149545E-03,-0.87315944E-03, 0.17016691E-02,
         0.13762738E-02,-0.74683833E-02,-0.69861100E-02, 0.18454404E-02,
         0.22659225E-02, 0.58288118E-02,-0.30733552E-02,-0.92975519E-04,
         0.47620971E-03, 0.60522277E-03, 0.12079651E-02, 0.40627585E-03,
        -0.50110096E-03,-0.33669365E-02,-0.34816231E-03,-0.11655617E-02,
        -0.21056484E-02, 0.39402051E-02, 0.26847101E-02, 0.85801812E-03,
         0.57774472E-04,-0.82746214E-04, 0.59458253E-03,-0.47391880E-03,
         0.15891726E-03, 0.32197140E-03,-0.14797802E-02,-0.14310722E-03,
        -0.59500727E-03, 0.59126846E-05, 0.27407773E-03,-0.92465547E-03,
         0.13425508E-03, 0.56717667E-03,-0.18916783E-03, 0.18770914E-03,
         0.32304847E-02, 0.20505578E-03,-0.12465909E-02,-0.77226124E-03,
        -0.83366977E-02, 0.38810339E-03,-0.78531085E-02,-0.28051080E-02,
        -0.18541627E-04, 0.11595815E-02,-0.74420538E-03,-0.38554185E-03,
        -0.28858570E-03,-0.39007249E-04,-0.28454224E-04,-0.33283337E-04,
        -0.52759915E-04, 0.42043475E-03,-0.14269099E-03, 0.36522426E-03,
         0.33852141E-02, 0.35836077E-02, 0.10211160E-02,-0.94707713E-04,
         0.19905489E-03, 0.96856529E-03, 0.79775207E-04,-0.41369660E-03,
        -0.32900088E-03, 0.14249726E-03,-0.15790694E-02, 0.25756392E-03,
        -0.16314354E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]        *x34*x41    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]            *x43    
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x21    *x45    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x21*x32*x43    
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 35]    *x21*x32*x41*x52
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24    *x41    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 44]    *x24*x31        
        +coeff[ 45]    *x21*x31*x44    
        +coeff[ 46]    *x23    *x43    
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]*x11    *x31    *x51
        +coeff[ 50]        *x32*x41*x51
        +coeff[ 51]        *x31*x44    
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]    *x22*x33        
        +coeff[ 55]        *x31    *x53
        +coeff[ 56]    *x21*x32*x41*x51
        +coeff[ 57]*x11        *x41*x51
        +coeff[ 58]            *x43*x51
        +coeff[ 59]        *x33*x42    
        +coeff[ 60]*x11*x21*x31    *x51
        +coeff[ 61]*x11*x21*x31*x42    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 62]*x11*x22    *x41*x51
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x41*x53
        +coeff[ 66]    *x22*x31*x42*x51
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]*x11*x21    *x45    
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]*x11*x21*x34*x41    
        +coeff[ 73]    *x23*x31*x44    
        +coeff[ 74]    *x22    *x45*x51
        +coeff[ 75]    *x23*x33*x42    
        +coeff[ 76]    *x21    *x45*x52
        +coeff[ 77]*x12        *x41    
        +coeff[ 78]*x12    *x31        
        +coeff[ 79]*x11        *x41*x52
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 80]            *x41*x53
        +coeff[ 81]    *x21*x31*x42*x51
        +coeff[ 82]            *x43*x52
        +coeff[ 83]*x11*x21*x32*x41    
        +coeff[ 84]    *x23*x31*x42    
        +coeff[ 85]    *x21*x33*x42    
        +coeff[ 86]    *x21*x34*x41    
        +coeff[ 87]*x11*x21    *x41*x52
        +coeff[ 88]*x11*x22*x31*x42    
    ;
    v_y_e_q3ex_1_300                              =v_y_e_q3ex_1_300                              
        +coeff[ 89]    *x23*x33        
        +coeff[ 90]    *x21*x31    *x53
        +coeff[ 91]    *x22*x32*x41*x51
        +coeff[ 92]    *x24*x31    *x51
        +coeff[ 93]    *x21    *x45*x51
        +coeff[ 94]    *x24    *x43    
        +coeff[ 95]    *x23    *x41*x52
        +coeff[ 96]    *x24*x31*x42    
        ;

    return v_y_e_q3ex_1_300                              ;
}
float p_e_q3ex_1_300                              (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.1268839E-03;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.13221345E-03, 0.16869446E-01,-0.32387063E-01, 0.15552063E-01,
        -0.83041890E-02,-0.92135714E-02, 0.72237956E-02,-0.49033592E-03,
         0.11318355E-03, 0.15610628E-03, 0.52472798E-04, 0.49970383E-02,
         0.22527757E-02, 0.44788322E-02,-0.55970642E-03, 0.31199104E-02,
         0.13565416E-02, 0.11632922E-02, 0.70472842E-03,-0.77454781E-04,
        -0.17081015E-03, 0.59750519E-03, 0.17284612E-02,-0.92969870E-03,
        -0.31229274E-03,-0.99182327E-03,-0.58441615E-03,-0.48725682E-03,
         0.96747181E-05,-0.10504799E-02,-0.21366950E-03,-0.31482789E-03,
        -0.66615001E-04, 0.20752806E-03, 0.10012861E-03,-0.15015835E-03,
        -0.15764109E-03, 0.15782584E-03, 0.12638433E-02, 0.86989667E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_300                              =v_p_e_q3ex_1_300                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_300                              =v_p_e_q3ex_1_300                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_300                              =v_p_e_q3ex_1_300                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x32*x41*x53
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]    *x22*x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_300                              =v_p_e_q3ex_1_300                              
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_300                              ;
}
float l_e_q3ex_1_300                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1872549E-01;
    float xmin[10]={
        -0.39999E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.18333632E-01,-0.32060161E+00,-0.25101010E-01, 0.12115847E-01,
        -0.34358136E-01,-0.11601260E-01,-0.62432321E-03,-0.33482767E-02,
        -0.53061671E-02,-0.53305528E-02,-0.23205699E-02, 0.23278499E-02,
         0.77713220E-02, 0.79099294E-02, 0.31187152E-02,-0.92903688E-03,
        -0.12001777E-02, 0.33674946E-02,-0.21534609E-02, 0.16770220E-02,
         0.92420122E-02, 0.63172122E-02, 0.11245081E-02, 0.50973130E-03,
         0.68329374E-03, 0.12133998E-02, 0.21733842E-02,-0.16469168E-02,
        -0.90257282E-03, 0.75479859E-03, 0.80443715E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_300                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42*x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_1_300                              =v_l_e_q3ex_1_300                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_1_300                              =v_l_e_q3ex_1_300                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x32    *x53
        +coeff[ 23]    *x21        *x52
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_q3ex_1_300                              =v_l_e_q3ex_1_300                              
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]    *x22*x31    *x53
        +coeff[ 30]*x11    *x34    *x52
        ;

    return v_l_e_q3ex_1_300                              ;
}
float x_e_q3ex_1_250                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.9621834E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.99624190E-02,-0.70279194E-02, 0.20328912E+00, 0.88981412E-01,
        -0.98296916E-02, 0.25015125E-01, 0.90183318E-02,-0.54237870E-02,
        -0.41720821E-02,-0.94760079E-02,-0.66987807E-02, 0.24413457E-04,
        -0.64196956E-03,-0.10260728E-02, 0.16750010E-02, 0.71421004E-03,
        -0.40665660E-02, 0.34464735E-02,-0.22737781E-03, 0.77971077E-03,
        -0.37744420E-03, 0.86346531E-03,-0.19678680E-03,-0.29975013E-03,
         0.59831829E-03, 0.70537807E-03, 0.15388933E-02,-0.33636317E-02,
        -0.22455552E-02,-0.21001352E-02,-0.23513343E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_250                              =v_x_e_q3ex_1_250                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_250                              =v_x_e_q3ex_1_250                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_1_250                              =v_x_e_q3ex_1_250                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x23    *x42*x51
        +coeff[ 30]    *x23*x31*x41*x53
        ;

    return v_x_e_q3ex_1_250                              ;
}
float t_e_q3ex_1_250                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7069388E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.66757379E-02,-0.31156730E-01, 0.73953420E-01, 0.32015876E-02,
        -0.55515901E-02,-0.22418518E-02,-0.22096687E-02,-0.21218053E-03,
        -0.13195677E-02,-0.99754659E-03, 0.94921293E-03, 0.26882612E-03,
         0.23008064E-03,-0.42897579E-03, 0.46540966E-03, 0.15493406E-02,
         0.50981733E-03,-0.26869719E-03,-0.93936651E-04, 0.25540692E-03,
         0.55356091E-03,-0.64172799E-03, 0.21564565E-03,-0.67504810E-03,
        -0.40668473E-03, 0.16296326E-03,-0.13025235E-03, 0.52023458E-03,
         0.74132076E-04,-0.63655956E-04, 0.80121310E-04, 0.47831758E-03,
        -0.51575171E-04, 0.34512018E-03,-0.29097439E-03,-0.54512388E-03,
         0.62239630E-03,-0.13306966E-04,-0.22166272E-04,-0.10664551E-03,
        -0.22931388E-04, 0.72961571E-04, 0.46955378E-04,-0.57044643E-04,
         0.10543222E-03, 0.22413247E-03, 0.62725463E-04,-0.21444200E-03,
         0.11382959E-03,-0.14860991E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]        *x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x11                
    ;
    v_t_e_q3ex_1_250                              =v_t_e_q3ex_1_250                              
        +coeff[  8]    *x22            
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22    *x42    
        +coeff[ 16]    *x21*x32    *x51
    ;
    v_t_e_q3ex_1_250                              =v_t_e_q3ex_1_250                              
        +coeff[ 17]        *x32        
        +coeff[ 18]*x11            *x51
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x22*x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]            *x42*x52
    ;
    v_t_e_q3ex_1_250                              =v_t_e_q3ex_1_250                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]        *x31*x41*x52
        +coeff[ 33]    *x23    *x42    
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_q3ex_1_250                              =v_t_e_q3ex_1_250                              
        +coeff[ 35]    *x22*x31*x41*x52
        +coeff[ 36]    *x23        *x53
        +coeff[ 37]*x12                
        +coeff[ 38]*x11*x22            
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11            *x52
        +coeff[ 41]*x11*x23            
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_t_e_q3ex_1_250                              =v_t_e_q3ex_1_250                              
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x32*x42    
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_q3ex_1_250                              ;
}
float y_e_q3ex_1_250                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1104983E-02;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.11887328E-02, 0.96301921E-01,-0.10880794E+00,-0.71444936E-01,
        -0.30272767E-01, 0.53588595E-01, 0.27001783E-01,-0.39450835E-01,
        -0.15092853E-01,-0.91317799E-02, 0.23933505E-02,-0.10467567E-01,
        -0.67061014E-02,-0.46889985E-04, 0.11402617E-02,-0.59056939E-02,
        -0.15307243E-02, 0.20407550E-02,-0.25415923E-02,-0.20625147E-02,
         0.65539526E-02,-0.33012233E-02,-0.27582885E-02, 0.56086382E-03,
        -0.87319198E-03, 0.20767536E-02, 0.47883932E-02, 0.23408395E-02,
         0.15708527E-02, 0.75110019E-03,-0.82984120E-02,-0.72147474E-02,
         0.20832042E-02,-0.26436015E-02, 0.48174569E-03, 0.11026146E-02,
        -0.46802554E-03,-0.36175274E-02, 0.25258589E-03,-0.28436026E-03,
        -0.11395791E-02,-0.21735078E-02, 0.92850241E-03, 0.35296045E-04,
        -0.28114124E-04,-0.91424517E-04, 0.63205906E-03, 0.16626500E-03,
         0.12706048E-03,-0.14730779E-02,-0.15341413E-03,-0.55966224E-03,
         0.76177251E-03, 0.24373747E-04,-0.62663028E-04, 0.33735207E-03,
        -0.68800175E-03,-0.11749243E-02,-0.10476949E-02, 0.51152048E-03,
        -0.27879764E-03,-0.48553452E-03, 0.68248162E-03, 0.39743187E-03,
         0.16026273E-02,-0.14899240E-03, 0.22648985E-02, 0.19354814E-03,
        -0.78633177E-03,-0.38044705E-03, 0.10770892E-04,-0.15878097E-04,
        -0.26839347E-04,-0.47364076E-04, 0.58196019E-03, 0.12766427E-03,
         0.81573724E-03, 0.18621903E-02, 0.14306424E-03,-0.67506632E-03,
        -0.61811821E-04, 0.21361363E-03,-0.65951183E-03, 0.67706878E-03,
         0.94074392E-04,-0.53432344E-04,-0.45365159E-03,-0.65877629E-02,
         0.37545248E-03,-0.15378641E-02,-0.65571843E-02, 0.25124138E-03,
        -0.40640519E-03,-0.13944963E-02,-0.24541502E-02,-0.13605115E-03,
         0.51145960E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]    *x21    *x43    
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 44]                *x51
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]        *x31    *x53
        +coeff[ 51]    *x21*x32*x41*x51
        +coeff[ 52]    *x22*x31*x42*x52
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 53]    *x22            
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]            *x43*x51
        +coeff[ 56]        *x31*x44    
        +coeff[ 57]        *x32*x43    
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]    *x21    *x43*x51
        +coeff[ 61]        *x34*x41    
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]*x11*x22    *x41*x51
        +coeff[ 66]    *x23*x32*x41    
        +coeff[ 67]    *x21    *x41*x53
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x24*x31    *x51
        +coeff[ 70]            *x42    
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 71]*x11        *x42    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]            *x41*x53
        +coeff[ 74]    *x21*x31*x42*x51
        +coeff[ 75]    *x21*x33    *x51
        +coeff[ 76]    *x21*x32*x43    
        +coeff[ 77]    *x23*x31*x42    
        +coeff[ 78]    *x22*x31    *x52
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 80]    *x21*x34*x41    
        +coeff[ 81]*x11*x22*x31*x42    
        +coeff[ 82]    *x22*x31*x42*x51
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]    *x21*x31    *x53
        +coeff[ 85]    *x21    *x43*x52
        +coeff[ 86]    *x22*x32*x41*x51
        +coeff[ 87]    *x22*x31*x44    
        +coeff[ 88]        *x33*x44    
    ;
    v_y_e_q3ex_1_250                              =v_y_e_q3ex_1_250                              
        +coeff[ 89]    *x24    *x43    
        +coeff[ 90]    *x22*x32*x43    
        +coeff[ 91]    *x23    *x41*x52
        +coeff[ 92]    *x21*x31*x44*x51
        +coeff[ 93]    *x24*x31*x42    
        +coeff[ 94]    *x22*x33*x42    
        +coeff[ 95]    *x21*x33    *x52
        +coeff[ 96]    *x23    *x43*x51
        ;

    return v_y_e_q3ex_1_250                              ;
}
float p_e_q3ex_1_250                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.1831797E-03;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.20491488E-03, 0.17018182E-01,-0.31921130E-01, 0.15775969E-01,
        -0.82412669E-02,-0.90935994E-02, 0.46218573E-02, 0.72469502E-02,
        -0.61589759E-03,-0.51400333E-04, 0.11110034E-04, 0.51672682E-02,
         0.11741434E-02, 0.22632631E-02,-0.54821750E-03, 0.31545088E-02,
         0.13360527E-02, 0.71148929E-03,-0.15271427E-04,-0.16420904E-03,
         0.62249275E-03, 0.16652991E-02,-0.10746935E-02,-0.31068039E-03,
        -0.15707552E-02,-0.59383770E-03,-0.48067345E-03, 0.11718670E-04,
        -0.89698099E-03,-0.22490077E-03,-0.71556232E-03,-0.60429936E-03,
        -0.76189863E-04, 0.10338340E-03,-0.17694509E-03,-0.30733549E-03,
        -0.24951616E-03, 0.11028862E-02, 0.87141071E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_250                              =v_p_e_q3ex_1_250                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_250                              =v_p_e_q3ex_1_250                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x21*x31*x42    
        +coeff[ 25]        *x31    *x53
    ;
    v_p_e_q3ex_1_250                              =v_p_e_q3ex_1_250                              
        +coeff[ 26]            *x41*x53
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]    *x21*x32*x41*x53
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x21*x32*x41    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_p_e_q3ex_1_250                              =v_p_e_q3ex_1_250                              
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_250                              ;
}
float l_e_q3ex_1_250                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1806889E-01;
    float xmin[10]={
        -0.39971E-02,-0.59978E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.17585464E-01,-0.32260534E+00,-0.25346877E-01, 0.12119669E-01,
        -0.34755781E-01,-0.11707389E-01,-0.32905391E-02,-0.54153362E-02,
        -0.52523715E-02,-0.17803909E-02, 0.21603971E-02, 0.97162584E-02,
         0.75665191E-02, 0.42101545E-02,-0.86932385E-03,-0.18117562E-02,
         0.26924692E-02,-0.21610123E-02, 0.11075867E-02, 0.17080349E-02,
         0.73846756E-03, 0.74008020E-03, 0.36556346E-03,-0.30719268E-03,
        -0.28971588E-03,-0.61378104E-03, 0.57572528E-03, 0.76479767E-02,
         0.64505716E-02, 0.72240614E-03, 0.10016225E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q3ex_1_250                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x32        
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_q3ex_1_250                              =v_l_e_q3ex_1_250                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_1_250                              =v_l_e_q3ex_1_250                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]*x11*x22            
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]                *x54
    ;
    v_l_e_q3ex_1_250                              =v_l_e_q3ex_1_250                              
        +coeff[ 26]*x11*x21        *x52
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]*x11*x22*x33        
        +coeff[ 30]        *x32*x41*x54
        ;

    return v_l_e_q3ex_1_250                              ;
}
float x_e_q3ex_1_200                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.7951588E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.86968699E-02,-0.69748857E-02, 0.20318657E+00, 0.90003543E-01,
        -0.98109469E-02, 0.24954334E-01, 0.90192277E-02,-0.54488438E-02,
        -0.41664373E-02,-0.96268412E-02,-0.67404965E-02, 0.27555301E-04,
        -0.49754704E-03,-0.10006181E-02, 0.16648096E-02, 0.68416039E-03,
        -0.41067526E-02, 0.34359058E-02,-0.23946178E-03,-0.20603984E-03,
         0.78798679E-03,-0.36262913E-03, 0.10907520E-02,-0.29430303E-03,
         0.52044500E-03,-0.77980745E-03, 0.65469783E-03,-0.15029710E-04,
         0.15265588E-02,-0.27251297E-02,-0.15936904E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_200                              =v_x_e_q3ex_1_200                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_200                              =v_x_e_q3ex_1_200                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]*x11*x21            
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_x_e_q3ex_1_200                              =v_x_e_q3ex_1_200                              
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]            *x42*x53
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_200                              ;
}
float t_e_q3ex_1_200                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6777409E-02;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.62372913E-02,-0.31342354E-01, 0.73722824E-01, 0.31813192E-02,
        -0.55288663E-02,-0.13948595E-02,-0.22221780E-02,-0.22252926E-02,
        -0.20740219E-03,-0.10132504E-02, 0.93941553E-03, 0.29354976E-03,
        -0.45112768E-03, 0.44183133E-03, 0.15453404E-02, 0.51863061E-03,
        -0.97288117E-04, 0.20321085E-03, 0.26571369E-03,-0.64828258E-03,
        -0.26145825E-03, 0.22638965E-03, 0.56056917E-03,-0.64470421E-03,
        -0.37706608E-03, 0.15115790E-03,-0.11881053E-03, 0.47569789E-03,
        -0.31466217E-03,-0.25297923E-03, 0.67741217E-04,-0.68178037E-04,
         0.58032201E-04, 0.41348310E-03,-0.12564668E-03,-0.82668048E-04,
         0.51284331E-03,-0.49278728E-03, 0.59655431E-03,-0.12335252E-04,
        -0.74396274E-04,-0.18984314E-04, 0.62926520E-04,-0.38671253E-04,
         0.16415323E-03, 0.28720521E-03, 0.41816148E-03, 0.19628934E-03,
        -0.27489348E-04, 0.24820276E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_1_200                              =v_t_e_q3ex_1_200                              
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_1_200                              =v_t_e_q3ex_1_200                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]        *x32        
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]            *x42*x52
    ;
    v_t_e_q3ex_1_200                              =v_t_e_q3ex_1_200                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]    *x21*x31*x41    
        +coeff[ 30]*x11        *x42    
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]        *x32    *x51
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]*x11*x21    *x42    
    ;
    v_t_e_q3ex_1_200                              =v_t_e_q3ex_1_200                              
        +coeff[ 35]        *x31*x41*x52
        +coeff[ 36]    *x23    *x42    
        +coeff[ 37]    *x22*x31*x41*x52
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x12                
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x11*x23            
        +coeff[ 43]*x11*x21*x32        
    ;
    v_t_e_q3ex_1_200                              =v_t_e_q3ex_1_200                              
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x23*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x21*x32*x42    
        +coeff[ 49]    *x21*x31*x43    
        ;

    return v_t_e_q3ex_1_200                              ;
}
float y_e_q3ex_1_200                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.7177460E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.67271583E-03, 0.94051145E-01,-0.11092436E+00,-0.72089732E-01,
        -0.30331569E-01, 0.53584564E-01, 0.26964737E-01,-0.38920891E-01,
        -0.14789563E-01,-0.91140633E-02, 0.23678013E-02,-0.10111617E-01,
        -0.54969039E-03,-0.18405172E-02,-0.45241718E-03, 0.11259140E-02,
        -0.65269098E-02,-0.15144830E-02, 0.20670334E-02,-0.24320593E-02,
        -0.20512468E-02, 0.66059916E-02,-0.32317655E-02,-0.30195573E-02,
        -0.53424174E-02, 0.55887003E-03,-0.85844955E-03, 0.21910719E-02,
         0.44191005E-02, 0.21691362E-02, 0.15955014E-02, 0.78230625E-03,
        -0.69687553E-02,-0.10472530E-01,-0.77633522E-02, 0.20883323E-02,
         0.31017332E-03, 0.44133663E-04, 0.10478316E-02,-0.50497212E-03,
         0.30149467E-03,-0.27815616E-03,-0.15872346E-02,-0.23621281E-02,
         0.97673433E-03,-0.36711870E-04,-0.86650900E-04, 0.59655221E-03,
        -0.12364294E-02, 0.16015557E-03,-0.96779881E-03, 0.11817247E-03,
        -0.13998779E-02,-0.13895700E-03,-0.72716508E-03, 0.29772476E-04,
        -0.54218082E-04,-0.16101744E-05, 0.30423849E-03, 0.50001848E-03,
         0.64663863E-03, 0.38609499E-03, 0.14811980E-02,-0.18968854E-03,
        -0.13144281E-03, 0.17754480E-02, 0.22718478E-03,-0.88695617E-03,
        -0.14503101E-02,-0.21894828E-04, 0.70783630E-04, 0.34864745E-04,
        -0.49571041E-04,-0.33937197E-03, 0.82457052E-04, 0.54231321E-03,
        -0.60369098E-03, 0.23743577E-03,-0.90932612E-04, 0.25019547E-03,
         0.45468038E-03, 0.10271619E-03,-0.24358252E-03,-0.50429598E-03,
        -0.32894262E-02,-0.91344682E-05,-0.31683927E-02, 0.23907133E-03,
        -0.18567730E-02,-0.17343315E-03, 0.46212247E-03, 0.38668507E-03,
         0.30326056E-02, 0.41818018E-02,-0.37401749E-03, 0.25762867E-02,
         0.46602546E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]        *x34*x41    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]            *x43    
        +coeff[ 25]*x11*x21*x31        
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 26]        *x31    *x52
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21            
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x21*x31    *x52
        +coeff[ 42]    *x24    *x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]                *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]        *x33    *x51
        +coeff[ 50]        *x33*x42    
        +coeff[ 51]*x11*x21*x31    *x51
        +coeff[ 52]    *x22*x33        
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 53]        *x31    *x53
        +coeff[ 54]    *x21*x32*x41*x51
        +coeff[ 55]    *x22            
        +coeff[ 56]*x12        *x41    
        +coeff[ 57]*x11        *x41*x51
        +coeff[ 58]            *x43*x51
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]*x11*x22    *x41*x51
        +coeff[ 64]    *x22*x31    *x52
        +coeff[ 65]    *x23*x32*x41    
        +coeff[ 66]    *x21    *x41*x53
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x24*x31*x42*x51
        +coeff[ 69]*x12    *x31        
        +coeff[ 70]*x11    *x32*x41    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 71]*x12    *x31*x41    
        +coeff[ 72]*x11*x21    *x41*x51
        +coeff[ 73]    *x21    *x43*x51
        +coeff[ 74]        *x31*x42*x52
        +coeff[ 75]    *x23*x31*x42    
        +coeff[ 76]    *x22    *x43*x51
        +coeff[ 77]    *x21*x34*x41    
        +coeff[ 78]*x11*x21    *x41*x52
        +coeff[ 79]*x11*x22*x31*x42    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 80]    *x23*x33        
        +coeff[ 81]    *x21*x31    *x53
        +coeff[ 82]    *x21    *x43*x52
        +coeff[ 83]    *x22*x32*x41*x51
        +coeff[ 84]    *x22*x31*x44    
        +coeff[ 85]    *x21    *x45*x51
        +coeff[ 86]    *x22*x32*x43    
        +coeff[ 87]    *x23    *x41*x52
        +coeff[ 88]    *x22*x33*x42    
    ;
    v_y_e_q3ex_1_200                              =v_y_e_q3ex_1_200                              
        +coeff[ 89]    *x21*x33    *x52
        +coeff[ 90]    *x23    *x43*x51
        +coeff[ 91]    *x23*x31*x42*x51
        +coeff[ 92]    *x23*x31*x44    
        +coeff[ 93]    *x23*x32*x43    
        +coeff[ 94]    *x22*x32*x41*x52
        +coeff[ 95]    *x23*x33*x42    
        +coeff[ 96]    *x24*x31    *x52
        ;

    return v_y_e_q3ex_1_200                              ;
}
float p_e_q3ex_1_200                              (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.1473731E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.13222516E-03, 0.17312042E-01,-0.31258058E-01, 0.15526041E-01,
        -0.81977071E-02,-0.90762023E-02, 0.45858826E-02, 0.72312783E-02,
        -0.52385288E-03, 0.85619940E-04, 0.12522320E-03, 0.49629519E-02,
         0.22652955E-02, 0.13981851E-03,-0.53446600E-03, 0.31010320E-02,
         0.13259245E-02, 0.73047337E-03, 0.29750941E-04,-0.15837277E-03,
         0.62182260E-03, 0.16047213E-02, 0.11209181E-02,-0.96290017E-03,
        -0.31515563E-03,-0.10235806E-02,-0.60986920E-03,-0.50421251E-03,
         0.36304740E-04,-0.89779316E-03,-0.22896922E-03,-0.33408159E-03,
        -0.76707329E-04, 0.99579134E-04,-0.20091575E-03,-0.30076026E-03,
        -0.21872749E-03, 0.12539070E-02, 0.88281016E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_200                              =v_p_e_q3ex_1_200                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x23*x31    *x51
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_200                              =v_p_e_q3ex_1_200                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_200                              =v_p_e_q3ex_1_200                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x32*x41*x53
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_p_e_q3ex_1_200                              =v_p_e_q3ex_1_200                              
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_200                              ;
}
float l_e_q3ex_1_200                              (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2012741E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.18168733E-01,-0.32533383E+00,-0.25340324E-01, 0.11955149E-01,
        -0.35092533E-01,-0.11516788E-01, 0.22950079E-02,-0.24995743E-03,
        -0.31196510E-02,-0.50333585E-02,-0.51400759E-02,-0.22992997E-02,
         0.10624259E-01, 0.90404563E-02, 0.22526348E-02,-0.78679784E-03,
        -0.11254074E-02, 0.33811636E-02,-0.23644876E-02, 0.14810885E-02,
         0.49610279E-03, 0.65265468E-03, 0.90284972E-03, 0.69850148E-03,
         0.21082028E-03,-0.10194279E-02,-0.96382241E-03, 0.47385087E-02,
         0.40168758E-02,-0.48801926E-03, 0.63804694E-03,-0.15056690E-02,
        -0.84108597E-03, 0.13351957E-02, 0.16368103E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_200                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]            *x42*x52
    ;
    v_l_e_q3ex_1_200                              =v_l_e_q3ex_1_200                              
        +coeff[  8]        *x32        
        +coeff[  9]        *x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]                *x52
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_1_200                              =v_l_e_q3ex_1_200                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x22*x32    *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]                *x53
        +coeff[ 25]        *x32*x42    
    ;
    v_l_e_q3ex_1_200                              =v_l_e_q3ex_1_200                              
        +coeff[ 26]        *x31*x41*x52
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x33    *x51
        +coeff[ 30]            *x42*x53
        +coeff[ 31]    *x21*x31*x41*x53
        +coeff[ 32]*x11*x22        *x53
        +coeff[ 33]    *x21*x34    *x52
        +coeff[ 34]*x11*x21*x34*x41    
        ;

    return v_l_e_q3ex_1_200                              ;
}
float x_e_q3ex_1_175                              (float *x,int m){
    int ncoeff= 27;
    float avdat= -0.9059620E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
         0.10103361E-01,-0.69338088E-02, 0.20309503E+00, 0.90813555E-01,
        -0.98282006E-02, 0.24827147E-01, 0.90138586E-02,-0.54501304E-02,
        -0.41767601E-02,-0.10175453E-01,-0.43097814E-02,-0.71707056E-02,
        -0.98494592E-03, 0.16698567E-02, 0.33951681E-02, 0.77167270E-03,
        -0.36589181E-03, 0.11351843E-02,-0.14523492E-02,-0.23158835E-03,
        -0.18657187E-03,-0.30018250E-03, 0.55052404E-03, 0.28485185E-03,
         0.67109737E-03, 0.15942536E-02,-0.97365305E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_175                              =v_x_e_q3ex_1_175                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]                *x53
        +coeff[ 16]        *x32    *x51
    ;
    v_x_e_q3ex_1_175                              =v_x_e_q3ex_1_175                              
        +coeff[ 17]    *x21*x32    *x51
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]*x11            *x51
        +coeff[ 20]*x11*x21            
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22    *x42*x51
    ;
    v_x_e_q3ex_1_175                              =v_x_e_q3ex_1_175                              
        +coeff[ 26]    *x23    *x42*x51
        ;

    return v_x_e_q3ex_1_175                              ;
}
float t_e_q3ex_1_175                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7481726E-02;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.67900899E-02,-0.31486429E-01, 0.73549919E-01, 0.31536140E-02,
        -0.55094762E-02,-0.14402412E-02,-0.21831237E-02,-0.20578788E-02,
        -0.19956328E-03,-0.99451048E-03, 0.91551663E-03, 0.28063889E-03,
        -0.30554144E-03, 0.44456701E-03, 0.15295077E-02, 0.52788586E-03,
        -0.96400319E-04, 0.24026065E-03, 0.22666811E-03, 0.18141011E-03,
        -0.21800424E-03,-0.22864020E-03, 0.58037532E-03, 0.28167100E-03,
        -0.38018462E-03,-0.14456389E-03, 0.50020387E-03, 0.17256662E-04,
         0.70231006E-04,-0.57905661E-04, 0.89375237E-04,-0.11345035E-03,
        -0.62771892E-03,-0.47671117E-03,-0.19909996E-03, 0.14438629E-03,
        -0.30794492E-03, 0.65249053E-03,-0.11298716E-04,-0.15374557E-03,
        -0.24324430E-04, 0.70800394E-04, 0.72092676E-04, 0.23971040E-03,
        -0.25746220E-03, 0.11070741E-03, 0.10201563E-03,-0.18102709E-03,
         0.12628200E-03, 0.84896077E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_1_175                              =v_t_e_q3ex_1_175                              
        +coeff[  8]*x11                
        +coeff[  9]            *x42    
        +coeff[ 10]                *x53
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_1_175                              =v_t_e_q3ex_1_175                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]        *x32        
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x21        *x53
    ;
    v_t_e_q3ex_1_175                              =v_t_e_q3ex_1_175                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x21*x31*x41    
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x23        *x51
        +coeff[ 33]    *x21    *x42*x51
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_q3ex_1_175                              =v_t_e_q3ex_1_175                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x21*x32    *x52
        +coeff[ 37]    *x23        *x53
        +coeff[ 38]*x12                
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]*x11            *x52
        +coeff[ 41]*x11*x23            
        +coeff[ 42]    *x21*x33*x41    
        +coeff[ 43]    *x23    *x42    
    ;
    v_t_e_q3ex_1_175                              =v_t_e_q3ex_1_175                              
        +coeff[ 44]    *x21*x32*x42    
        +coeff[ 45]    *x22*x31*x41*x51
        +coeff[ 46]        *x33*x41*x51
        +coeff[ 47]        *x32*x42*x51
        +coeff[ 48]    *x23        *x52
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_q3ex_1_175                              ;
}
float y_e_q3ex_1_175                              (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.5030383E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.52364205E-03, 0.92303909E-01,-0.11240563E+00,-0.72184034E-01,
        -0.30332822E-01, 0.53504527E-01, 0.26881780E-01,-0.38960718E-01,
        -0.14975266E-01,-0.92050554E-02, 0.23824221E-02,-0.10349019E-01,
        -0.65435544E-02,-0.22177662E-03, 0.11126742E-02,-0.55955774E-02,
        -0.15155310E-02, 0.20199253E-02,-0.24768703E-02,-0.20145797E-02,
         0.65476727E-02,-0.32454608E-02,-0.27824442E-02, 0.54152362E-03,
        -0.88106625E-03, 0.12665178E-02, 0.42844536E-02, 0.17404113E-02,
         0.14981143E-02, 0.67657040E-03,-0.85047018E-02,-0.74685067E-02,
         0.22557594E-02,-0.18634159E-02, 0.20399834E-03, 0.12317691E-02,
        -0.48853987E-03,-0.54218695E-02, 0.28842691E-03,-0.28913631E-03,
        -0.17202315E-02,-0.20823341E-02, 0.90548256E-03, 0.28716928E-04,
        -0.24096771E-04,-0.89578687E-04, 0.57088141E-03, 0.17869237E-03,
         0.12192712E-03,-0.12083453E-02,-0.13983897E-03,-0.54951600E-03,
        -0.44083805E-04,-0.65260028E-05, 0.30591054E-03,-0.75818721E-03,
        -0.13670437E-02,-0.85153966E-03, 0.48471175E-03,-0.48240824E-03,
         0.63274679E-03, 0.36533599E-03, 0.11933619E-02,-0.18644058E-03,
         0.22977268E-03, 0.23227425E-02, 0.18862054E-03,-0.77360880E-03,
         0.45272643E-02, 0.16854739E-04,-0.24030127E-04,-0.40141709E-04,
        -0.58902340E-04, 0.28587380E-03, 0.54496585E-03,-0.39876501E-04,
         0.12879413E-03, 0.13277202E-03, 0.26621553E-02, 0.98397548E-03,
         0.86459149E-04,-0.59319992E-03, 0.21419897E-03,-0.10506405E-02,
         0.63405494E-03, 0.78733246E-04,-0.19045435E-03,-0.32177020E-03,
        -0.58461376E-02, 0.39348914E-03,-0.32192408E-03,-0.51172152E-02,
         0.19933288E-03,-0.95638068E-03,-0.28110598E-02,-0.13801095E-03,
         0.11843303E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31    *x52
        +coeff[ 25]    *x21    *x43    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x21*x33        
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22    *x45    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x21            
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 44]                *x51
        +coeff[ 45]*x11    *x31    *x51
        +coeff[ 46]        *x32*x41*x51
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]        *x31    *x53
        +coeff[ 51]    *x21*x32*x41*x51
        +coeff[ 52]*x12        *x41    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 53]*x11        *x41*x51
        +coeff[ 54]            *x43*x51
        +coeff[ 55]        *x31*x44    
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]        *x33*x42    
        +coeff[ 58]*x11*x21    *x43    
        +coeff[ 59]        *x34*x41    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]*x11*x22    *x41*x51
        +coeff[ 64]    *x22*x31    *x52
        +coeff[ 65]    *x23*x32*x41    
        +coeff[ 66]    *x21    *x41*x53
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x23*x31*x44    
        +coeff[ 69]    *x22            
        +coeff[ 70]*x12    *x31        
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 71]*x11        *x41*x52
        +coeff[ 72]            *x41*x53
        +coeff[ 73]    *x21*x31*x42*x51
        +coeff[ 74]    *x21    *x45    
        +coeff[ 75]            *x43*x52
        +coeff[ 76]        *x31*x42*x52
        +coeff[ 77]    *x21*x33    *x51
        +coeff[ 78]    *x21*x32*x43    
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 80]*x11*x21*x31*x43    
        +coeff[ 81]    *x22    *x43*x51
        +coeff[ 82]*x11*x22*x31*x42    
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x23*x33        
        +coeff[ 85]    *x21*x31    *x53
        +coeff[ 86]    *x21    *x43*x52
        +coeff[ 87]    *x22*x32*x41*x51
        +coeff[ 88]    *x22*x31*x44    
    ;
    v_y_e_q3ex_1_175                              =v_y_e_q3ex_1_175                              
        +coeff[ 89]        *x33*x44    
        +coeff[ 90]    *x24*x31    *x51
        +coeff[ 91]    *x22*x32*x43    
        +coeff[ 92]    *x23    *x41*x52
        +coeff[ 93]    *x24*x31*x42    
        +coeff[ 94]    *x22*x33*x42    
        +coeff[ 95]    *x21*x33    *x52
        +coeff[ 96]    *x23    *x45    
        ;

    return v_y_e_q3ex_1_175                              ;
}
float p_e_q3ex_1_175                              (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.5682578E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.64077401E-04, 0.17519927E-01,-0.30717930E-01, 0.15491760E-01,
        -0.82096830E-02,-0.90386504E-02, 0.45677936E-02, 0.71644969E-02,
        -0.47765215E-03, 0.11449777E-03, 0.73809628E-04, 0.49054734E-02,
         0.22655500E-02, 0.12801705E-03,-0.53129194E-03, 0.30426735E-02,
         0.13095792E-02, 0.74645586E-03,-0.43599033E-04, 0.62153343E-03,
         0.16138968E-02, 0.11361093E-02,-0.95055351E-03,-0.15552915E-03,
        -0.30176443E-03,-0.10182611E-02,-0.56975160E-03,-0.49177004E-03,
        -0.54104417E-03, 0.25742984E-05,-0.21236963E-03,-0.35470704E-03,
        -0.70600829E-04, 0.10419489E-03,-0.17666657E-03,-0.31691178E-03,
        -0.21648510E-03, 0.12221810E-02, 0.89235854E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_175                              =v_p_e_q3ex_1_175                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x23*x31    *x51
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_175                              =v_p_e_q3ex_1_175                              
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]    *x21*x31    *x51
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_175                              =v_p_e_q3ex_1_175                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]    *x21*x32*x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_p_e_q3ex_1_175                              =v_p_e_q3ex_1_175                              
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_175                              ;
}
float l_e_q3ex_1_175                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.2134916E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59983E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.18230649E-01,-0.32621425E+00,-0.25513832E-01, 0.11885959E-01,
        -0.35683356E-01,-0.11343387E-01,-0.56399815E-02,-0.56721680E-02,
        -0.22822041E-02, 0.23022008E-02, 0.10948785E-01, 0.74557052E-02,
         0.46940110E-03,-0.60646510E-03, 0.23045980E-02,-0.28737865E-02,
        -0.84394903E-03,-0.16036095E-02, 0.32940961E-02,-0.59561298E-03,
         0.17450841E-02, 0.78167400E-03, 0.10524396E-02, 0.20532398E-02,
         0.12613722E-02, 0.44030952E-02,-0.16260155E-02, 0.55031118E-03,
        -0.16957298E-02, 0.15368160E-01, 0.53223423E-02, 0.72459481E-02,
         0.38216712E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_175                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_q3ex_1_175                              =v_l_e_q3ex_1_175                              
        +coeff[  8]                *x52
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_l_e_q3ex_1_175                              =v_l_e_q3ex_1_175                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x53
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_1_175                              =v_l_e_q3ex_1_175                              
        +coeff[ 26]    *x24        *x51
        +coeff[ 27]            *x44*x51
        +coeff[ 28]*x11*x22*x31    *x51
        +coeff[ 29]    *x23*x31*x43    
        +coeff[ 30]    *x23    *x44    
        +coeff[ 31]    *x21*x32*x44    
        +coeff[ 32]*x11*x24*x31    *x51
        ;

    return v_l_e_q3ex_1_175                              ;
}
float x_e_q3ex_1_150                              (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.8664118E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.10983374E-01,-0.68842499E-02, 0.20325103E+00, 0.90858854E-01,
        -0.97822445E-02, 0.24705295E-01, 0.88600991E-02,-0.54919384E-02,
        -0.42961645E-02,-0.91483491E-02,-0.64541837E-02, 0.46923829E-04,
        -0.50110073E-03,-0.10208267E-02, 0.16380744E-02, 0.76975068E-03,
        -0.40266854E-02, 0.33140157E-02,-0.24107337E-03, 0.78007532E-03,
        -0.37725249E-03, 0.10611315E-02,-0.20452979E-03,-0.29295721E-03,
         0.58729353E-03,-0.42120708E-04,-0.70624385E-03, 0.74200158E-03,
         0.16344672E-02,-0.27172451E-02,-0.18018482E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_150                              =v_x_e_q3ex_1_150                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]*x12*x21*x32        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]    *x22        *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_q3ex_1_150                              =v_x_e_q3ex_1_150                              
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]*x11            *x51
        +coeff[ 19]                *x53
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]*x11*x21            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]            *x42*x51
    ;
    v_x_e_q3ex_1_150                              =v_x_e_q3ex_1_150                              
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x22    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_150                              ;
}
float t_e_q3ex_1_150                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7233340E-02;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.61529274E-02,-0.31378698E-01, 0.73365904E-01, 0.30770469E-02,
        -0.55095609E-02,-0.14954741E-02,-0.21576993E-02,-0.20283964E-02,
        -0.10316068E-02, 0.89349219E-03,-0.18998113E-03, 0.30483722E-03,
        -0.44698222E-03, 0.43606450E-03, 0.14997404E-02, 0.54575456E-03,
        -0.10029493E-03, 0.19119740E-03, 0.17908342E-03, 0.16102861E-03,
        -0.46439812E-03,-0.22308980E-03, 0.57407725E-03,-0.50556427E-03,
        -0.37158380E-03,-0.88773450E-04, 0.48803803E-03,-0.17012424E-04,
         0.70360518E-04,-0.74157011E-04, 0.96958160E-04, 0.23230503E-03,
        -0.18667971E-03, 0.15658530E-03,-0.30386154E-03,-0.34872061E-03,
         0.56340999E-03,-0.14989349E-03,-0.35966357E-04, 0.44284898E-04,
        -0.71431234E-04, 0.10876137E-03, 0.25250574E-03, 0.34603322E-03,
         0.23969109E-03, 0.13871992E-03,-0.18843341E-03, 0.17571368E-03,
         0.54503704E-04, 0.18933338E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_1_150                              =v_t_e_q3ex_1_150                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x53
        +coeff[ 10]*x11                
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_1_150                              =v_t_e_q3ex_1_150                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]        *x32        
        +coeff[ 22]    *x22*x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]    *x21*x31*x41*x51
        +coeff[ 25]    *x21        *x53
    ;
    v_t_e_q3ex_1_150                              =v_t_e_q3ex_1_150                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x21*x31*x41    
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]        *x31*x41*x52
        +coeff[ 33]            *x42*x52
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_q3ex_1_150                              =v_t_e_q3ex_1_150                              
        +coeff[ 35]    *x23    *x42*x51
        +coeff[ 36]    *x23        *x53
        +coeff[ 37]        *x31*x41*x51
        +coeff[ 38]*x11            *x52
        +coeff[ 39]*x11*x21*x31*x41    
        +coeff[ 40]*x11*x21    *x42    
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]    *x23*x32        
        +coeff[ 43]    *x23*x31*x41    
    ;
    v_t_e_q3ex_1_150                              =v_t_e_q3ex_1_150                              
        +coeff[ 44]    *x23    *x42    
        +coeff[ 45]        *x33*x41*x51
        +coeff[ 46]        *x32*x42*x51
        +coeff[ 47]    *x23        *x52
        +coeff[ 48]*x11    *x32    *x52
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_q3ex_1_150                              ;
}
float y_e_q3ex_1_150                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.6820545E-04;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.33327069E-04, 0.89635544E-01,-0.11466040E+00,-0.72528556E-01,
        -0.30138042E-01, 0.53424641E-01, 0.26843479E-01,-0.38627554E-01,
        -0.14762838E-01,-0.91822231E-02, 0.23809939E-02,-0.10263874E-01,
        -0.65361578E-02,-0.18081392E-03, 0.11086756E-02,-0.56420993E-02,
        -0.15066569E-02, 0.19852875E-02,-0.25422631E-02,-0.19619907E-02,
         0.61283712E-02,-0.30668504E-02,-0.27358790E-02,-0.86697639E-03,
         0.53136575E-03, 0.12875152E-02, 0.10120230E-02,-0.13574574E-02,
        -0.77911853E-02,-0.69746873E-02, 0.14901962E-02, 0.21914844E-02,
         0.52479855E-02, 0.32340663E-02, 0.99193573E-03,-0.22228921E-02,
         0.85958447E-04, 0.11474261E-02, 0.33848343E-03,-0.48398672E-03,
        -0.39296634E-02, 0.27460326E-03,-0.39203968E-03,-0.17712731E-02,
        -0.95251069E-03, 0.89221192E-03,-0.14595190E-02, 0.29610785E-04,
        -0.21044947E-04, 0.85829321E-04,-0.80635698E-04, 0.54385938E-03,
         0.16288116E-03,-0.11520850E-02,-0.14964485E-03,-0.60752191E-03,
        -0.59040685E-04,-0.13410498E-04, 0.29730066E-03,-0.69323980E-03,
        -0.37815516E-04,-0.92883431E-03, 0.11851637E-03, 0.46923381E-03,
        -0.43257896E-03, 0.67025545E-03,-0.10147787E-03, 0.33851151E-03,
         0.20472861E-02, 0.29750988E-02, 0.18489012E-03, 0.22802650E-03,
        -0.74665307E-03, 0.65951078E-03, 0.54851809E-03, 0.17508579E-04,
        -0.59444832E-04, 0.39023321E-03, 0.33273741E-02, 0.14366728E-03,
         0.17684871E-03,-0.13677096E-03, 0.32731143E-02,-0.45803277E-04,
        -0.69051451E-03, 0.95488220E-04, 0.10603110E-02, 0.16573409E-03,
        -0.10581663E-02, 0.97140794E-04,-0.46595634E-03,-0.61534718E-02,
        -0.35716541E-03,-0.58915187E-02, 0.27965425E-03,-0.17385022E-02,
        -0.22878705E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11    *x31        
        +coeff[ 15]            *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21*x31    *x51
        +coeff[ 19]            *x41*x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]        *x31    *x52
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 26]    *x23*x31        
        +coeff[ 27]        *x32*x43    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22*x32*x41    
        +coeff[ 30]    *x21    *x45    
        +coeff[ 31]    *x23    *x41*x51
        +coeff[ 32]    *x21*x32*x43    
        +coeff[ 33]    *x23*x32*x41    
        +coeff[ 34]    *x23*x33        
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 35]    *x22    *x45    
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21*x31    *x52
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]    *x24    *x43    
        +coeff[ 47]    *x21            
        +coeff[ 48]                *x51
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]*x11    *x31    *x51
        +coeff[ 51]        *x32*x41*x51
        +coeff[ 52]        *x33    *x51
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 53]    *x24    *x41    
        +coeff[ 54]        *x31    *x53
        +coeff[ 55]    *x21*x32*x41*x51
        +coeff[ 56]*x12        *x41    
        +coeff[ 57]*x11        *x41*x51
        +coeff[ 58]            *x43*x51
        +coeff[ 59]        *x31*x44    
        +coeff[ 60]*x11*x21    *x41*x51
        +coeff[ 61]        *x33*x42    
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 62]*x11*x21*x31    *x51
        +coeff[ 63]*x11*x21    *x43    
        +coeff[ 64]        *x34*x41    
        +coeff[ 65]*x11*x21*x31*x42    
        +coeff[ 66]            *x43*x52
        +coeff[ 67]*x11*x21*x32*x41    
        +coeff[ 68]    *x23    *x43    
        +coeff[ 69]    *x23*x31*x42    
        +coeff[ 70]    *x22*x31    *x52
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 71]    *x21    *x41*x53
        +coeff[ 72]    *x24    *x41*x51
        +coeff[ 73]    *x23*x31*x44    
        +coeff[ 74]    *x21*x31*x44*x52
        +coeff[ 75]    *x22            
        +coeff[ 76]            *x41*x53
        +coeff[ 77]    *x21*x31*x42*x51
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]        *x31*x42*x52
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 80]    *x21*x33    *x51
        +coeff[ 81]*x11*x22    *x41*x51
        +coeff[ 82]    *x21*x33*x42    
        +coeff[ 83]*x12*x22*x31        
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]        *x32*x43*x51
        +coeff[ 86]    *x21*x34*x41    
        +coeff[ 87]*x11*x22*x31*x42    
        +coeff[ 88]    *x22*x31*x42*x51
    ;
    v_y_e_q3ex_1_150                              =v_y_e_q3ex_1_150                              
        +coeff[ 89]    *x21*x31    *x53
        +coeff[ 90]    *x22*x32*x41*x51
        +coeff[ 91]    *x22*x31*x44    
        +coeff[ 92]    *x24*x31    *x51
        +coeff[ 93]    *x22*x32*x43    
        +coeff[ 94]    *x23    *x41*x52
        +coeff[ 95]    *x24*x31*x42    
        +coeff[ 96]    *x22*x33*x42    
        ;

    return v_y_e_q3ex_1_150                              ;
}
float p_e_q3ex_1_150                              (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.7899922E-04;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.68460904E-04, 0.17828187E-01,-0.29953923E-01, 0.15445339E-01,
        -0.81603648E-02,-0.89972904E-02, 0.69632875E-02,-0.51157415E-03,
         0.90190537E-04, 0.10087359E-03, 0.10502552E-03, 0.49119275E-02,
         0.22760818E-02, 0.10256041E-03, 0.43737683E-02,-0.52498025E-03,
         0.30111768E-02, 0.12906403E-02, 0.75490901E-03,-0.52917159E-04,
         0.63174637E-03, 0.16181364E-02, 0.11534332E-02,-0.92477567E-03,
        -0.15370305E-03,-0.29558802E-03,-0.93471457E-03,-0.58539142E-03,
        -0.51012111E-03,-0.58646669E-03,-0.69552350E-07,-0.21037667E-03,
        -0.30820817E-03,-0.75399948E-04, 0.93720832E-04,-0.19546533E-03,
        -0.31838953E-03,-0.23151941E-03, 0.12288226E-02, 0.91606227E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_150                              =v_p_e_q3ex_1_150                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x23*x33        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x23*x31    *x51
        +coeff[ 14]    *x22*x31        
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]        *x31*x42    
    ;
    v_p_e_q3ex_1_150                              =v_p_e_q3ex_1_150                              
        +coeff[ 17]            *x43    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x21*x31    *x51
        +coeff[ 23]    *x23    *x41    
        +coeff[ 24]*x11    *x31        
        +coeff[ 25]*x11*x21    *x41    
    ;
    v_p_e_q3ex_1_150                              =v_p_e_q3ex_1_150                              
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]    *x21*x32*x41*x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_150                              =v_p_e_q3ex_1_150                              
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]            *x43*x51
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_150                              ;
}
float l_e_q3ex_1_150                              (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2085499E-01;
    float xmin[10]={
        -0.39990E-02,-0.57360E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.13336155E-01,-0.32627961E+00,-0.25337283E-01, 0.11875873E-01,
        -0.35076778E-01,-0.10768271E-01,-0.32820171E-02,-0.55440352E-02,
        -0.52331551E-02,-0.21027434E-02, 0.22963481E-02, 0.10358084E-01,
         0.85181538E-02, 0.10435771E-02,-0.90727193E-03,-0.12751982E-02,
         0.41045835E-02,-0.22848069E-02, 0.18646276E-02, 0.11095154E-02,
         0.54168835E-03, 0.55420509E-03,-0.35313162E-03, 0.10063365E-02,
         0.49183909E-02, 0.41315183E-02,-0.17865098E-02,-0.18328043E-02,
        -0.91661914E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_150                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x32        
        +coeff[  7]        *x31*x41    
    ;
    v_l_e_q3ex_1_150                              =v_l_e_q3ex_1_150                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x52
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_q3ex_1_150                              =v_l_e_q3ex_1_150                              
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x32    *x53
        +coeff[ 20]            *x42*x51
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11*x21        *x51
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q3ex_1_150                              =v_l_e_q3ex_1_150                              
        +coeff[ 26]    *x21*x31*x41*x52
        +coeff[ 27]        *x32*x42*x52
        +coeff[ 28]    *x23        *x53
        ;

    return v_l_e_q3ex_1_150                              ;
}
float x_e_q3ex_1_125                              (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.7877177E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.10059594E-01, 0.20296994E+00, 0.92807353E-01,-0.97467946E-02,
         0.24839701E-01, 0.89840842E-02, 0.25125817E-04,-0.68160356E-02,
        -0.54035871E-02,-0.42664963E-02,-0.91956714E-02,-0.65614684E-02,
        -0.90439580E-05,-0.43167852E-03,-0.99228951E-03,-0.34260935E-04,
         0.16420735E-02,-0.41174483E-02, 0.34213299E-02,-0.23780033E-03,
         0.75206580E-03,-0.41100886E-03, 0.72798505E-03, 0.11349147E-02,
        -0.18559731E-03,-0.31219880E-03, 0.48739894E-03,-0.80446487E-04,
        -0.78395271E-03, 0.81802037E-03, 0.18501556E-02,-0.22594924E-02,
        -0.14595051E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x21            
        +coeff[  3]                *x52
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]*x13                
        +coeff[  7]*x11                
    ;
    v_x_e_q3ex_1_125                              =v_x_e_q3ex_1_125                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x21*x32    *x52
        +coeff[ 14]        *x32        
        +coeff[ 15]*x12*x21            
        +coeff[ 16]    *x22        *x51
    ;
    v_x_e_q3ex_1_125                              =v_x_e_q3ex_1_125                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]*x11            *x51
        +coeff[ 20]                *x53
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x21            
        +coeff[ 25]*x11*x22            
    ;
    v_x_e_q3ex_1_125                              =v_x_e_q3ex_1_125                              
        +coeff[ 26]        *x31*x41*x51
        +coeff[ 27]            *x42*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]    *x22    *x42*x51
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]    *x23    *x42    
        ;

    return v_x_e_q3ex_1_125                              ;
}
float t_e_q3ex_1_125                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6396933E-02;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.53524585E-02,-0.31925563E-01, 0.73015139E-01, 0.30812786E-02,
        -0.54566180E-02,-0.15953123E-02,-0.21611708E-02,-0.19841304E-02,
        -0.10215341E-02, 0.92986983E-03,-0.18000545E-03, 0.29287225E-03,
        -0.46060418E-03, 0.40872439E-03, 0.15131439E-02, 0.59810915E-03,
        -0.95579227E-04, 0.23083892E-03, 0.28068858E-03, 0.22996579E-03,
        -0.45960755E-03,-0.20415596E-03, 0.78970959E-04, 0.60349877E-03,
        -0.56290295E-03,-0.34079471E-03,-0.76183627E-04, 0.54381386E-03,
         0.73435731E-04,-0.60961465E-04, 0.90005298E-04,-0.15760638E-03,
         0.37711419E-03,-0.10215781E-03,-0.10922541E-03, 0.13351235E-03,
         0.31252226E-03,-0.32377333E-03,-0.28911035E-03,-0.45001751E-03,
         0.54347445E-03,-0.13999619E-04,-0.23020884E-04, 0.52998526E-04,
        -0.39582617E-04, 0.32938170E-03, 0.28253632E-03, 0.11313937E-03,
        -0.13038403E-03,-0.13260879E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_1_125                              =v_t_e_q3ex_1_125                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x53
        +coeff[ 10]*x11                
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_1_125                              =v_t_e_q3ex_1_125                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]        *x32        
        +coeff[ 22]    *x21*x31*x41    
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_125                              =v_t_e_q3ex_1_125                              
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]    *x22    *x42*x51
        +coeff[ 28]*x11        *x42    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]        *x32    *x51
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]        *x31*x41*x52
    ;
    v_t_e_q3ex_1_125                              =v_t_e_q3ex_1_125                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]    *x23*x32        
        +coeff[ 37]    *x21*x32    *x52
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]    *x22*x31*x41*x52
        +coeff[ 40]    *x23        *x53
        +coeff[ 41]*x12                
        +coeff[ 42]*x11*x22            
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_q3ex_1_125                              =v_t_e_q3ex_1_125                              
        +coeff[ 44]*x11*x23    *x41    
        +coeff[ 45]    *x23*x31*x41    
        +coeff[ 46]    *x23    *x42    
        +coeff[ 47]        *x33*x41*x51
        +coeff[ 48]        *x32*x42*x51
        +coeff[ 49]    *x21*x31*x41*x52
        ;

    return v_t_e_q3ex_1_125                              ;
}
float y_e_q3ex_1_125                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1545138E-03;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.80610182E-04, 0.87053880E-01,-0.11719080E+00,-0.73158652E-01,
        -0.30388821E-01, 0.53397015E-01, 0.26796186E-01,-0.39108276E-01,
        -0.14643432E-01,-0.91340058E-02, 0.23488440E-02,-0.10233189E-01,
        -0.29867381E-03,-0.13098184E-02,-0.40154177E-03, 0.10722301E-02,
        -0.64698183E-02,-0.15023778E-02, 0.20161651E-02,-0.24236615E-02,
        -0.19997093E-02, 0.63268854E-02,-0.33620866E-02,-0.30789315E-02,
        -0.54077432E-02,-0.85792388E-03, 0.51874836E-03, 0.28564997E-02,
         0.13387394E-02,-0.88458005E-02,-0.72746342E-02, 0.14192829E-02,
         0.22050294E-02, 0.52990895E-02,-0.19420816E-02,-0.28151675E-04,
         0.31886701E-03, 0.56049255E-04,-0.40442770E-04, 0.21442286E-03,
         0.11476020E-02, 0.57192758E-03,-0.52104128E-03,-0.54185563E-02,
        -0.37960004E-03,-0.17143319E-02,-0.22518996E-02, 0.20345498E-02,
         0.94998896E-03, 0.10098466E-03,-0.80606020E-04, 0.45676262E-03,
        -0.54788153E-03, 0.44794296E-03, 0.12119728E-03,-0.14432300E-03,
        -0.71526715E-03, 0.33325327E-04,-0.44028930E-04,-0.87092785E-05,
         0.26686009E-03, 0.16747061E-03,-0.51347539E-03, 0.46314171E-03,
         0.68316748E-03,-0.11302412E-02, 0.36695710E-03, 0.22658794E-02,
        -0.17119711E-03, 0.23843210E-03, 0.29584873E-02, 0.19076627E-03,
        -0.62699924E-03,-0.66175228E-02,-0.57545407E-02,-0.33095449E-02,
         0.35723005E-02,-0.62454556E-03, 0.15334602E-02, 0.31478387E-04,
         0.10407778E-04, 0.54524149E-04,-0.21658185E-04, 0.74321979E-04,
         0.47157158E-04,-0.56941812E-04,-0.75572461E-04, 0.86069500E-04,
         0.13610307E-03, 0.19438113E-02,-0.45363128E-03, 0.10045210E-02,
        -0.11039300E-03,-0.68615231E-03, 0.60476764E-03, 0.90037327E-04,
        -0.21911814E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]            *x45    
        +coeff[ 13]        *x32*x43    
        +coeff[ 14]        *x34*x41    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x21*x31    *x51
        +coeff[ 20]            *x41*x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]            *x43    
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x23*x31        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22*x32*x41    
        +coeff[ 31]    *x21    *x45    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x21*x32*x43    
        +coeff[ 34]    *x22    *x45    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 35]    *x21*x32*x41*x52
        +coeff[ 36]    *x24*x33        
        +coeff[ 37]    *x21            
        +coeff[ 38]                *x51
        +coeff[ 39]    *x21*x32*x41    
        +coeff[ 40]        *x31*x42*x51
        +coeff[ 41]    *x21*x33        
        +coeff[ 42]*x11*x22    *x41    
        +coeff[ 43]    *x22    *x43    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 44]    *x21*x31    *x52
        +coeff[ 45]    *x24    *x41    
        +coeff[ 46]    *x24*x31        
        +coeff[ 47]    *x21*x31*x44    
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]*x11    *x31    *x51
        +coeff[ 51]        *x32*x41*x51
        +coeff[ 52]        *x31*x44    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]*x11*x21*x31    *x51
        +coeff[ 55]        *x31    *x53
        +coeff[ 56]    *x21*x32*x41*x51
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]            *x43*x51
        +coeff[ 61]        *x33    *x51
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 62]        *x33*x42    
        +coeff[ 63]*x11*x21    *x43    
        +coeff[ 64]*x11*x21*x31*x42    
        +coeff[ 65]    *x22*x33        
        +coeff[ 66]*x11*x21*x32*x41    
        +coeff[ 67]    *x23    *x43    
        +coeff[ 68]*x11*x22    *x41*x51
        +coeff[ 69]    *x22*x31    *x52
        +coeff[ 70]    *x23*x32*x41    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 71]    *x21    *x41*x53
        +coeff[ 72]    *x24    *x41*x51
        +coeff[ 73]    *x22*x31*x44    
        +coeff[ 74]    *x22*x32*x43    
        +coeff[ 75]    *x22*x33*x42    
        +coeff[ 76]    *x23*x31*x44    
        +coeff[ 77]    *x21*x34*x43    
        +coeff[ 78]    *x23*x33*x42    
        +coeff[ 79]        *x31*x41    
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 80]    *x21        *x51
        +coeff[ 81]        *x32*x42    
        +coeff[ 82]*x12    *x31        
        +coeff[ 83]*x11    *x31*x42    
        +coeff[ 84]*x11*x22*x31        
        +coeff[ 85]*x11*x21    *x41*x51
        +coeff[ 86]            *x43*x52
        +coeff[ 87]        *x31*x42*x52
        +coeff[ 88]    *x21*x33    *x51
    ;
    v_y_e_q3ex_1_125                              =v_y_e_q3ex_1_125                              
        +coeff[ 89]    *x21*x33*x42    
        +coeff[ 90]    *x22    *x43*x51
        +coeff[ 91]    *x21*x34*x41    
        +coeff[ 92]*x11*x21    *x41*x52
        +coeff[ 93]    *x22*x31*x42*x51
        +coeff[ 94]    *x23*x33        
        +coeff[ 95]    *x21*x31    *x53
        +coeff[ 96]    *x21    *x43*x52
        ;

    return v_y_e_q3ex_1_125                              ;
}
float p_e_q3ex_1_125                              (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.7096872E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.49318383E-04, 0.18152967E-01,-0.29150512E-01, 0.15383735E-01,
        -0.81495652E-02,-0.89529762E-02, 0.70543457E-02,-0.46031649E-03,
         0.13794684E-03, 0.84221567E-04, 0.47807503E-02, 0.22740006E-02,
         0.83190098E-04, 0.44352068E-02,-0.51228108E-03, 0.29296502E-02,
         0.12785071E-02, 0.11785921E-02, 0.79581299E-03,-0.15987260E-04,
         0.62841730E-03, 0.15476455E-02,-0.14634919E-03,-0.29052203E-03,
        -0.90699794E-03,-0.93857781E-03,-0.58209704E-03,-0.50885847E-03,
        -0.62331167E-03,-0.77430350E-05,-0.20530052E-03,-0.29953927E-03,
        -0.73822215E-04, 0.92060924E-04,-0.17598258E-03,-0.30965576E-03,
        -0.21774587E-03, 0.12731017E-02, 0.88750193E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x23*x31        
    ;
    v_p_e_q3ex_1_125                              =v_p_e_q3ex_1_125                              
        +coeff[  8]    *x21*x33        
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x21*x31        
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x23*x31    *x51
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_q3ex_1_125                              =v_p_e_q3ex_1_125                              
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11    *x31        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_125                              =v_p_e_q3ex_1_125                              
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]            *x41*x53
        +coeff[ 28]    *x21*x32*x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11*x21*x31        
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_p_e_q3ex_1_125                              =v_p_e_q3ex_1_125                              
        +coeff[ 35]        *x32*x41*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_125                              ;
}
float l_e_q3ex_1_125                              (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1849652E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.11586473E-01,-0.33142194E+00,-0.25392942E-01, 0.11755898E-01,
        -0.36366615E-01,-0.10813381E-01, 0.15834731E-03,-0.34295314E-02,
        -0.53780810E-02,-0.53288522E-02,-0.24269982E-02, 0.22615644E-02,
         0.64897905E-02, 0.76954346E-02, 0.13363872E-02,-0.94842381E-03,
        -0.18561304E-02, 0.38699077E-02,-0.20815630E-02, 0.19457557E-02,
         0.10859536E-02, 0.40124237E-03, 0.50329941E-03, 0.59411227E-03,
         0.53522893E-03,-0.15564172E-02, 0.83308211E-02, 0.10013781E-02,
         0.67125997E-02, 0.10746166E-02, 0.76487199E-02,-0.86949801E-03,
         0.10538679E-02, 0.14302523E-02,-0.14806995E-02, 0.90085657E-03,
         0.68245735E-02,-0.23761282E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_125                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]            *x42*x52
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_1_125                              =v_l_e_q3ex_1_125                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_1_125                              =v_l_e_q3ex_1_125                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]        *x32    *x53
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_q3ex_1_125                              =v_l_e_q3ex_1_125                              
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]        *x34*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]        *x33*x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]*x11*x21*x32    *x51
        +coeff[ 32]*x12*x21    *x41*x51
        +coeff[ 33]    *x24    *x42    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_l_e_q3ex_1_125                              =v_l_e_q3ex_1_125                              
        +coeff[ 35]*x12*x23    *x41    
        +coeff[ 36]    *x21*x32*x44    
        +coeff[ 37]    *x24*x31*x41*x51
        ;

    return v_l_e_q3ex_1_125                              ;
}
float x_e_q3ex_1_100                              (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.8197924E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.10676360E-01,-0.66928095E-02, 0.20260228E+00, 0.94817221E-01,
        -0.97261053E-02, 0.24811914E-01, 0.90591721E-02,-0.53761122E-02,
        -0.42372383E-02,-0.96171396E-02,-0.42324215E-02,-0.66166427E-02,
        -0.95425284E-03, 0.16676894E-02, 0.32871517E-02,-0.21741960E-03,
         0.76826080E-03,-0.40780040E-03, 0.57469762E-03, 0.11865612E-02,
        -0.18286273E-03,-0.28709829E-03, 0.49004244E-03,-0.47412021E-04,
        -0.62196283E-03, 0.83478173E-03, 0.17693008E-02,-0.16219619E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_x_e_q3ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]                *x52
        +coeff[  5]    *x21        *x51
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_x_e_q3ex_1_100                              =v_x_e_q3ex_1_100                              
        +coeff[  8]            *x42    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]        *x32        
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x11            *x51
        +coeff[ 16]                *x53
    ;
    v_x_e_q3ex_1_100                              =v_x_e_q3ex_1_100                              
        +coeff[ 17]        *x32    *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]*x11*x21            
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22*x32        
    ;
    v_x_e_q3ex_1_100                              =v_x_e_q3ex_1_100                              
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x23*x31*x41    
        ;

    return v_x_e_q3ex_1_100                              ;
}
float t_e_q3ex_1_100                              (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.6582090E-02;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54382286E-02,-0.32388236E-01, 0.72500341E-01, 0.30909441E-02,
        -0.54315664E-02,-0.17500874E-02,-0.20890427E-02,-0.19231811E-02,
        -0.10298778E-02, 0.96399552E-03,-0.16502538E-03, 0.31114832E-03,
        -0.42024787E-03, 0.39031444E-03, 0.14222346E-02, 0.59642724E-03,
        -0.10758742E-03, 0.19890227E-03, 0.33011302E-03,-0.58510498E-03,
         0.16541235E-03,-0.17671048E-03, 0.20479613E-03, 0.61642635E-03,
        -0.61199925E-03,-0.30950300E-03,-0.54177963E-05,-0.21087092E-03,
        -0.10803572E-03, 0.44974167E-03,-0.40116243E-03,-0.18076041E-03,
         0.76288175E-04,-0.62626583E-04, 0.17956203E-03, 0.15214275E-03,
        -0.16925231E-03,-0.29172411E-03, 0.64493663E-03, 0.21340586E-04,
        -0.28019937E-04, 0.19111880E-03, 0.83780011E-04, 0.49243459E-04,
         0.68312482E-04, 0.23969998E-03, 0.13304595E-03, 0.29424787E-03,
         0.15795724E-03, 0.12819516E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q3ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]                *x52
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q3ex_1_100                              =v_t_e_q3ex_1_100                              
        +coeff[  8]            *x42    
        +coeff[  9]                *x53
        +coeff[ 10]*x11                
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]    *x21*x32    *x51
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_q3ex_1_100                              =v_t_e_q3ex_1_100                              
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x31*x41    
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_q3ex_1_100                              =v_t_e_q3ex_1_100                              
        +coeff[ 26]        *x32    *x52
        +coeff[ 27]        *x31*x41*x52
        +coeff[ 28]    *x21        *x53
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]    *x21*x32    *x52
        +coeff[ 31]        *x32        
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]        *x32    *x51
    ;
    v_t_e_q3ex_1_100                              =v_t_e_q3ex_1_100                              
        +coeff[ 35]            *x42*x52
        +coeff[ 36]        *x32    *x53
        +coeff[ 37]*x11*x21*x32*x42    
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]*x11    *x32        
        +coeff[ 40]*x11            *x52
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]        *x32*x42    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_q3ex_1_100                              =v_t_e_q3ex_1_100                              
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]    *x23*x32        
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x23    *x42    
        +coeff[ 48]        *x33*x41*x51
        +coeff[ 49]    *x23        *x52
        ;

    return v_t_e_q3ex_1_100                              ;
}
float y_e_q3ex_1_100                              (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9572281E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.90725732E-03, 0.83091214E-01,-0.12119154E+00,-0.73966645E-01,
        -0.30271245E-01, 0.53327195E-01, 0.26717572E-01,-0.39860312E-01,
        -0.14562792E-01,-0.90957545E-02, 0.22935953E-02,-0.97879488E-02,
         0.65074554E-02,-0.29223465E-03,-0.15401271E-02,-0.29486066E-03,
         0.10408391E-02,-0.64696199E-02,-0.14325186E-02, 0.19366912E-02,
        -0.23377645E-02,-0.19817736E-02,-0.29160797E-02,-0.29308260E-02,
        -0.54397751E-02,-0.84203691E-03, 0.50609501E-03, 0.76806260E-03,
         0.36902807E-02, 0.12895387E-02, 0.10656235E-02, 0.53044257E-03,
        -0.47528017E-02,-0.10376664E-01,-0.72364244E-02, 0.22126320E-02,
         0.10481406E-02,-0.51036879E-03, 0.39145412E-03,-0.49100752E-03,
        -0.21288560E-02,-0.10020656E-02, 0.91207377E-03,-0.11901925E-02,
         0.42426618E-04,-0.32544172E-04,-0.79522208E-04, 0.52695750E-03,
        -0.95640292E-03,-0.13542610E-02,-0.13240534E-02,-0.15447827E-03,
        -0.57656964E-03, 0.19671850E-04,-0.44465269E-04,-0.13842248E-04,
         0.36035100E-03, 0.15476772E-03, 0.12769486E-03, 0.52375987E-03,
         0.66892343E-03, 0.37611404E-03, 0.18327161E-02, 0.18854675E-03,
         0.26590726E-02, 0.20673143E-03, 0.84654288E-03,-0.97588665E-03,
        -0.44353376E-02, 0.61388063E-03,-0.30866929E-02, 0.71539235E-03,
         0.25641832E-02, 0.17378520E-03, 0.30984349E-04,-0.21747519E-04,
         0.33559321E-04,-0.51010629E-04, 0.77371177E-03, 0.37869457E-04,
         0.14488913E-03,-0.12923998E-03, 0.15836451E-03, 0.19679777E-02,
        -0.16831115E-03, 0.23457152E-03, 0.44925787E-03, 0.49196882E-03,
        -0.77270408E-03, 0.57714489E-04, 0.21169338E-03,-0.11593872E-02,
        -0.19238268E-02, 0.96802527E-04,-0.46487208E-03, 0.73482789E-03,
        -0.29026103E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q3ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x23    *x41    
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]        *x34*x41    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]    *x21*x31    *x51
        +coeff[ 21]            *x41*x52
        +coeff[ 22]    *x22    *x41*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]            *x43    
        +coeff[ 25]        *x31    *x52
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x33        
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22*x32*x41    
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 35]    *x23    *x41*x51
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x21*x31    *x52
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x22*x33        
        +coeff[ 42]    *x22    *x41*x52
        +coeff[ 43]    *x24    *x43    
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]*x11    *x31    *x51
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x44    
        +coeff[ 49]        *x33*x42    
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]        *x31    *x53
        +coeff[ 52]    *x21*x32*x41*x51
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 53]    *x22            
        +coeff[ 54]*x12        *x41    
        +coeff[ 55]*x11        *x41*x51
        +coeff[ 56]            *x43*x51
        +coeff[ 57]        *x33    *x51
        +coeff[ 58]*x11*x21*x31    *x51
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]*x11*x21*x32*x41    
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 62]    *x23    *x43    
        +coeff[ 63]    *x22*x31    *x52
        +coeff[ 64]    *x23*x32*x41    
        +coeff[ 65]    *x21    *x41*x53
        +coeff[ 66]    *x23*x33        
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]    *x21*x31*x42*x52
        +coeff[ 70]    *x22*x32*x43    
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 71]    *x21*x33*x42*x51
        +coeff[ 72]    *x23*x31*x44    
        +coeff[ 73]    *x24    *x45    
        +coeff[ 74]    *x22    *x42    
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]*x11    *x31*x42    
        +coeff[ 77]            *x41*x53
        +coeff[ 78]    *x21    *x45    
        +coeff[ 79]*x11    *x31    *x52
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 80]        *x31*x42*x52
        +coeff[ 81]    *x23*x31    *x51
        +coeff[ 82]    *x21*x33    *x51
        +coeff[ 83]    *x21*x32*x43    
        +coeff[ 84]*x11*x22    *x41*x51
        +coeff[ 85]        *x31*x44*x51
        +coeff[ 86]    *x23*x31*x42    
        +coeff[ 87]    *x21*x33*x42    
        +coeff[ 88]    *x22    *x43*x51
    ;
    v_y_e_q3ex_1_100                              =v_y_e_q3ex_1_100                              
        +coeff[ 89]        *x32*x43*x51
        +coeff[ 90]*x11*x22*x31*x42    
        +coeff[ 91]    *x22*x31*x42*x51
        +coeff[ 92]    *x22    *x45    
        +coeff[ 93]    *x21*x31    *x53
        +coeff[ 94]    *x22*x32*x41*x51
        +coeff[ 95]        *x33*x44    
        +coeff[ 96]    *x24*x31    *x51
        ;

    return v_y_e_q3ex_1_100                              ;
}
float p_e_q3ex_1_100                              (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.2827975E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.26727832E-03,-0.95131372E-05, 0.18632650E-01,-0.27930282E-01,
         0.15436901E-01,-0.80919154E-02,-0.88908942E-02, 0.69862828E-02,
        -0.53911784E-03, 0.41767922E-04, 0.84211082E-04, 0.47280886E-02,
         0.44217524E-02, 0.12667009E-02, 0.22942116E-02,-0.49030595E-03,
         0.28420617E-02, 0.12422134E-02, 0.82694733E-03, 0.39000447E-05,
         0.63578150E-03, 0.14787751E-02,-0.10170620E-02,-0.13654002E-03,
        -0.29212129E-03,-0.13186277E-02,-0.52426982E-03,-0.58589352E-03,
        -0.49230276E-03,-0.62900491E-03,-0.20912164E-04,-0.19798840E-03,
        -0.49620401E-03,-0.77826029E-04, 0.84865169E-04,-0.20005459E-03,
        -0.34544477E-03,-0.20173444E-03, 0.11621430E-02, 0.93070726E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_p_e_q3ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_q3ex_1_100                              =v_p_e_q3ex_1_100                              
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11        *x41    
        +coeff[ 16]        *x31*x42    
    ;
    v_p_e_q3ex_1_100                              =v_p_e_q3ex_1_100                              
        +coeff[ 17]            *x43    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]    *x23    *x41    
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x31*x42    
    ;
    v_p_e_q3ex_1_100                              =v_p_e_q3ex_1_100                              
        +coeff[ 26]    *x21    *x43    
        +coeff[ 27]        *x31    *x53
        +coeff[ 28]            *x41*x53
        +coeff[ 29]    *x21*x32*x41*x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x21*x31        
        +coeff[ 32]    *x21*x32*x41    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_p_e_q3ex_1_100                              =v_p_e_q3ex_1_100                              
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]            *x43*x51
        +coeff[ 38]    *x22*x31*x42    
        +coeff[ 39]    *x22    *x43    
        ;

    return v_p_e_q3ex_1_100                              ;
}
float l_e_q3ex_1_100                              (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1880069E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.10894749E-01,-0.33602914E+00,-0.25430825E-01, 0.11491147E-01,
        -0.37219394E-01,-0.10658239E-01, 0.24356935E-02,-0.34399917E-02,
        -0.55235401E-02,-0.54759029E-02,-0.23037803E-02, 0.76192562E-02,
         0.85061584E-02,-0.29679373E-03,-0.50368364E-03,-0.18947440E-02,
        -0.17053584E-02, 0.45021889E-02, 0.19767724E-02, 0.15500424E-02,
         0.57200523E-03, 0.17779060E-02,-0.43668359E-03, 0.19981987E-02,
         0.13388491E-02, 0.65542604E-02, 0.45034578E-02, 0.38121841E-02,
        -0.57132868E-03, 0.79406617E-03,-0.16753450E-02, 0.91213721E-03,
         0.79239690E-03,-0.11235398E-02, 0.11579461E-02,-0.13078914E-02,
         0.51471684E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_q3ex_1_100                              =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]        *x32        
    ;
    v_l_e_q3ex_1_100                              =v_l_e_q3ex_1_100                              
        +coeff[  8]        *x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]                *x52
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]    *x21*x34        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_q3ex_1_100                              =v_l_e_q3ex_1_100                              
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]        *x32*x42*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_q3ex_1_100                              =v_l_e_q3ex_1_100                              
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x13            *x51
        +coeff[ 29]            *x42*x53
        +coeff[ 30]*x11*x24            
        +coeff[ 31]*x11    *x31*x43    
        +coeff[ 32]*x12    *x32    *x51
        +coeff[ 33]    *x23*x31*x42    
        +coeff[ 34]    *x21    *x42*x53
    ;
    v_l_e_q3ex_1_100                              =v_l_e_q3ex_1_100                              
        +coeff[ 35]*x12    *x33*x41    
        +coeff[ 36]    *x23*x32*x42    
        ;

    return v_l_e_q3ex_1_100                              ;
}
