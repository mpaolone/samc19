float x_e_qd_1200                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314736E+01;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.17999674E-02,-0.67708497E-02, 0.22723712E+00, 0.11018568E-01,
         0.56656037E-03,-0.16557543E-02,-0.50274218E-02,-0.38283570E-02,
         0.23153500E-05,-0.51622587E-05,-0.10932361E-02,-0.19182790E-02,
        -0.63316600E-03,-0.40122690E-02,-0.32956968E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_1200                                  =v_x_e_qd_1200                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_1200                                  ;
}
float t_e_qd_1200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5786150E+00;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12629948E-02,-0.33457589E-02, 0.72385348E-01, 0.33249483E-02,
        -0.22299308E-02,-0.16993978E-02,-0.17826662E-02,-0.69449778E-03,
         0.20591148E-03, 0.23774143E-03,-0.57833479E-03,-0.69278351E-03,
        -0.21849974E-03,-0.26440498E-03,-0.22324412E-02,-0.17184684E-02,
        -0.14787728E-02,-0.64961685E-04, 0.26269804E-03, 0.18384641E-03,
        -0.75989694E-03,-0.17067315E-02, 0.11612764E-04,-0.20146715E-05,
        -0.26000221E-05,-0.37414655E-04,-0.25410183E-04,-0.20509999E-04,
         0.17928722E-03, 0.16914529E-03, 0.66471007E-05, 0.35807003E-04,
        -0.45951260E-05,-0.32583716E-05,-0.19640034E-04, 0.76401084E-04,
         0.70567403E-04, 0.88498353E-04, 0.21468906E-04, 0.11226925E-04,
        -0.10392248E-04,-0.14775022E-03,-0.12884205E-03, 0.19835779E-04,
         0.62323438E-05, 0.22933607E-05,-0.72599942E-04, 0.19598498E-04,
         0.28203036E-04, 0.22380842E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_1200                                  =v_t_e_qd_1200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23*x31*x41    
        +coeff[ 15]    *x23    *x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_qd_1200                                  =v_t_e_qd_1200                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_1200                                  =v_t_e_qd_1200                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]        *x32    *x52
        +coeff[ 31]    *x21        *x53
        +coeff[ 32]*x12                
        +coeff[ 33]        *x32        
        +coeff[ 34]*x11    *x32        
    ;
    v_t_e_qd_1200                                  =v_t_e_qd_1200                                  
        +coeff[ 35]    *x22*x32        
        +coeff[ 36]    *x23        *x51
        +coeff[ 37]    *x21*x32    *x51
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]*x12*x23            
        +coeff[ 40]*x11*x22*x32        
        +coeff[ 41]*x11*x22*x31*x41    
        +coeff[ 42]*x11*x22    *x42    
        +coeff[ 43]    *x22*x32    *x51
    ;
    v_t_e_qd_1200                                  =v_t_e_qd_1200                                  
        +coeff[ 44]    *x23*x32*x41    
        +coeff[ 45]    *x22*x33*x41    
        +coeff[ 46]    *x22*x32*x42    
        +coeff[ 47]    *x23    *x43    
        +coeff[ 48]*x12*x22*x31    *x51
        +coeff[ 49]*x13        *x42*x51
        ;

    return v_t_e_qd_1200                                  ;
}
float y_e_qd_1200                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2094218E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.12244791E-03, 0.18028548E+00, 0.86836368E-01, 0.40520248E-02,
         0.10884770E-02,-0.13084573E-02,-0.83967263E-03,-0.12164587E-02,
        -0.17889895E-04,-0.17735329E-03,-0.77138335E-03,-0.17080343E-03,
        -0.25429335E-03,-0.70248352E-03,-0.14001846E-03,-0.16699665E-04,
        -0.78341609E-03,-0.65096270E-03, 0.79319334E-05,-0.12507002E-03,
         0.51547802E-04,-0.14738500E-02,-0.87616259E-04,-0.10408360E-02,
        -0.26524084E-03,-0.40022529E-04,-0.35634552E-03, 0.18418484E-03,
        -0.19280383E-03,-0.16384821E-03, 0.14101558E-03, 0.93549985E-04,
         0.33719403E-04,-0.90986240E-03,-0.90932605E-04,-0.77598197E-04,
         0.67836896E-04, 0.25461124E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_1200                                  =v_y_e_qd_1200                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_1200                                  =v_y_e_qd_1200                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x44    
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]    *x22    *x45    
    ;
    v_y_e_qd_1200                                  =v_y_e_qd_1200                                  
        +coeff[ 26]    *x22*x31*x44    
        +coeff[ 27]    *x24    *x43    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]    *x24    *x45    
        +coeff[ 30]        *x31*x42*x51
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]*x11*x23    *x41    
    ;
    v_y_e_qd_1200                                  =v_y_e_qd_1200                                  
        +coeff[ 35]*x11*x23*x31        
        +coeff[ 36]            *x45*x51
        +coeff[ 37]    *x24    *x41*x51
        ;

    return v_y_e_qd_1200                                  ;
}
float p_e_qd_1200                                  (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.3046495E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
         0.80309655E-05,-0.21400802E-01,-0.22768853E-01, 0.19944413E-02,
         0.34260161E-02, 0.66666718E-03, 0.71518845E-03,-0.73571963E-03,
        -0.40878245E-03,-0.65896095E-03,-0.39379433E-03,-0.17112987E-03,
         0.74867494E-04, 0.35086373E-05,-0.17398299E-03,-0.38925686E-03,
        -0.55104727E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_1200                                  =v_p_e_qd_1200                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]    *x22*x32*x41    
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_1200                                  ;
}
float l_e_qd_1200                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3608274E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36285380E-02,-0.65897540E-02,-0.16765413E-02,-0.52207746E-02,
        -0.58151516E-02, 0.37273765E-03, 0.15434576E-04, 0.86431704E-04,
         0.86549313E-04,-0.20738358E-03,-0.51206333E-03,-0.37452506E-03,
         0.87000713E-04,-0.26957967E-03, 0.10463897E-02,-0.38870482E-03,
         0.30323801E-04, 0.14663863E-02,-0.13487236E-03,-0.10116915E-02,
         0.20557315E-03,-0.23749570E-03,-0.31537979E-03, 0.65063994E-03,
         0.19154882E-03,-0.21415259E-03,-0.34632254E-02, 0.92338974E-03,
         0.43496693E-03,-0.20577365E-02, 0.99364924E-03, 0.43887491E-03,
        -0.74133766E-03, 0.44485877E-03,-0.11833714E-02,-0.96721534E-03,
        -0.11576752E-02,-0.64105977E-03, 0.17771776E-02,-0.12549536E-02,
         0.79425168E-03, 0.10076315E-02,-0.10373696E-02, 0.21477223E-02,
        -0.18099434E-02,-0.16211167E-02, 0.25657017E-02, 0.65076869E-03,
        -0.70698617E-04,-0.31441532E-03, 0.23560385E-03,-0.46855240E-03,
        -0.30813838E-03, 0.94558825E-04, 0.50897885E-03, 0.68877102E-03,
        -0.26238101E-03,-0.21934960E-03, 0.15959602E-03,-0.89801244E-04,
        -0.28145339E-03, 0.29358242E-03, 0.11900249E-03, 0.21822953E-03,
        -0.11393347E-03,-0.75559545E-03,-0.26543415E-03,-0.60900924E-03,
        -0.43868535E-04, 0.45324236E-03,-0.57938567E-03,-0.53384592E-03,
         0.20002133E-03,-0.79468409E-04,-0.86090200E-04, 0.31070225E-03,
         0.23612718E-03,-0.21483410E-03,-0.31605150E-03, 0.50891907E-03,
        -0.24853979E-03,-0.14877728E-02, 0.10218674E-02, 0.53087727E-03,
         0.56210626E-03,-0.37138548E-03,-0.54470060E-03,-0.25355027E-03,
         0.20416913E-03, 0.58919779E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]        *x31        
        +coeff[  7]            *x41*x51
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[  8]*x11            *x51
        +coeff[  9]        *x31*x42    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]                *x53
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]        *x32*x42    
        +coeff[ 15]            *x44    
        +coeff[ 16]        *x32*x41*x51
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 17]        *x31*x42*x51
        +coeff[ 18]*x11*x21*x32        
        +coeff[ 19]*x11    *x32*x41    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x42*x51
        +coeff[ 22]*x11*x21        *x52
        +coeff[ 23]*x12    *x31*x41    
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]            *x43*x52
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 26]        *x31*x44*x51
        +coeff[ 27]*x11*x23*x32        
        +coeff[ 28]*x11*x24    *x41    
        +coeff[ 29]*x11*x21*x31*x42*x51
        +coeff[ 30]*x11    *x32*x41*x52
        +coeff[ 31]*x11*x21*x31    *x53
        +coeff[ 32]*x11*x21    *x41*x53
        +coeff[ 33]*x12*x24            
        +coeff[ 34]*x12    *x32*x41*x51
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 35]    *x21*x34*x42    
        +coeff[ 36]    *x23*x31*x42*x51
        +coeff[ 37]        *x33*x42*x52
        +coeff[ 38]    *x22*x31    *x54
        +coeff[ 39]        *x33    *x54
        +coeff[ 40]    *x22    *x41*x54
        +coeff[ 41]*x12*x23*x31    *x51
        +coeff[ 42]*x12*x22*x32    *x51
        +coeff[ 43]    *x23*x34    *x51
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 44]    *x21*x34*x42*x51
        +coeff[ 45]        *x34*x43*x51
        +coeff[ 46]        *x33*x43*x52
        +coeff[ 47]        *x34    *x54
        +coeff[ 48]    *x21        *x51
        +coeff[ 49]    *x22*x31        
        +coeff[ 50]        *x33        
        +coeff[ 51]    *x21*x31*x41    
        +coeff[ 52]    *x21    *x42    
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 53]        *x32    *x51
        +coeff[ 54]        *x31*x41*x51
        +coeff[ 55]        *x31    *x52
        +coeff[ 56]*x11*x22            
        +coeff[ 57]*x11    *x32        
        +coeff[ 58]*x11    *x31*x41    
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]    *x24            
        +coeff[ 61]    *x23*x31        
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 62]    *x23    *x41    
        +coeff[ 63]    *x22*x31*x41    
        +coeff[ 64]    *x21*x32*x41    
        +coeff[ 65]    *x21*x31*x42    
        +coeff[ 66]    *x21    *x43    
        +coeff[ 67]    *x21*x32    *x51
        +coeff[ 68]    *x21*x31*x41*x51
        +coeff[ 69]    *x21    *x42*x51
        +coeff[ 70]            *x43*x51
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 71]        *x31*x41*x52
        +coeff[ 72]            *x41*x53
        +coeff[ 73]                *x54
        +coeff[ 74]*x11    *x33        
        +coeff[ 75]*x11*x22        *x51
        +coeff[ 76]*x11    *x31*x41*x51
        +coeff[ 77]*x12    *x31    *x51
        +coeff[ 78]    *x23    *x42    
        +coeff[ 79]    *x21    *x44    
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 80]    *x21*x33    *x51
        +coeff[ 81]        *x33*x41*x51
        +coeff[ 82]        *x31*x43*x51
        +coeff[ 83]            *x44*x51
        +coeff[ 84]    *x22        *x53
        +coeff[ 85]            *x42*x53
        +coeff[ 86]        *x31    *x54
        +coeff[ 87]            *x41*x54
        +coeff[ 88]*x11*x24            
    ;
    v_l_e_qd_1200                                  =v_l_e_qd_1200                                  
        +coeff[ 89]*x11    *x34        
        ;

    return v_l_e_qd_1200                                  ;
}
float x_e_qd_1100                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315059E+01;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.14484825E-02,-0.67723719E-02, 0.22728349E+00, 0.11016364E-01,
         0.58336637E-03,-0.16240784E-02,-0.49978583E-02,-0.38526168E-02,
         0.78625144E-05,-0.77618497E-05,-0.11927952E-02,-0.18602239E-02,
        -0.62470901E-03,-0.40518572E-02,-0.32630120E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_1100                                  =v_x_e_qd_1100                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_1100                                  ;
}
float t_e_qd_1100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5784875E+00;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.11248933E-02,-0.33460555E-02, 0.72413512E-01, 0.33365092E-02,
        -0.22406427E-02,-0.16826520E-02,-0.17847907E-02,-0.73393265E-03,
         0.20554478E-03, 0.24731134E-03,-0.56401343E-03,-0.66855311E-03,
        -0.21178389E-03,-0.26183852E-03,-0.78538869E-04,-0.22896803E-02,
        -0.17276144E-02,-0.14956081E-02,-0.77346164E-06,-0.14442653E-05,
         0.28151329E-03, 0.19555361E-03,-0.74050255E-03,-0.17220674E-02,
         0.28986573E-04,-0.15736092E-05,-0.70025817E-04,-0.35964214E-04,
         0.22661242E-03, 0.17294631E-03, 0.77437668E-04, 0.88889741E-04,
         0.19631176E-04,-0.11735782E-04, 0.17358005E-04, 0.42992885E-08,
        -0.41977797E-04,-0.23943025E-04, 0.17003933E-04, 0.73571944E-04,
        -0.97820448E-05, 0.16633843E-04,-0.34718381E-04, 0.23722705E-04,
        -0.22075428E-04,-0.24233237E-04,-0.33366607E-04, 0.23775530E-04,
         0.46760910E-04,-0.25665338E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_qd_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_1100                                  =v_t_e_qd_1100                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_1100                                  =v_t_e_qd_1100                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]        *x31*x41    
        +coeff[ 19]            *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_1100                                  =v_t_e_qd_1100                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]*x11    *x32    *x52
        +coeff[ 34]    *x22*x32    *x52
    ;
    v_t_e_qd_1100                                  =v_t_e_qd_1100                                  
        +coeff[ 35]*x13                
        +coeff[ 36]*x11    *x32        
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]*x11*x23            
        +coeff[ 39]    *x22*x32        
        +coeff[ 40]    *x22*x31    *x51
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]*x11*x23*x31        
        +coeff[ 43]*x11*x21*x33        
    ;
    v_t_e_qd_1100                                  =v_t_e_qd_1100                                  
        +coeff[ 44]    *x22*x33        
        +coeff[ 45]*x12*x21    *x42    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]    *x22*x31*x42    
        +coeff[ 48]*x11*x21*x32    *x51
        +coeff[ 49]*x11        *x42*x52
        ;

    return v_t_e_qd_1100                                  ;
}
float y_e_qd_1100                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.7689209E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.18359769E-03, 0.18021932E+00, 0.86835399E-01, 0.40571163E-02,
         0.11089980E-02,-0.13774784E-02,-0.85448317E-03,-0.13288646E-02,
        -0.38123802E-04,-0.23203283E-03,-0.78865368E-03,-0.17714278E-03,
        -0.27162032E-03,-0.71483449E-03,-0.14031149E-03,-0.28433544E-04,
        -0.75140939E-03,-0.62565610E-03,-0.36511618E-04,-0.14254759E-02,
        -0.97866147E-03,-0.27725787E-03,-0.43041705E-05,-0.22419241E-03,
        -0.20170659E-04,-0.34077291E-03,-0.27887934E-04,-0.20850214E-03,
         0.13228272E-03, 0.11633796E-03, 0.47366222E-04,-0.63348323E-03,
        -0.58255602E-04, 0.54136082E-04, 0.45327364E-04,-0.18022962E-04,
         0.21471202E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_1100                                  =v_y_e_qd_1100                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_1100                                  =v_y_e_qd_1100                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31*x42    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]    *x22*x31*x42*x51
        +coeff[ 23]    *x22    *x45    
        +coeff[ 24]        *x34*x41*x51
        +coeff[ 25]    *x22*x31*x44    
    ;
    v_y_e_qd_1100                                  =v_y_e_qd_1100                                  
        +coeff[ 26]    *x24    *x43    
        +coeff[ 27]    *x22*x32*x43    
        +coeff[ 28]        *x31*x42*x51
        +coeff[ 29]        *x32*x41*x51
        +coeff[ 30]    *x22*x31    *x51
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]            *x45*x51
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_y_e_qd_1100                                  =v_y_e_qd_1100                                  
        +coeff[ 35]    *x21    *x44*x51
        +coeff[ 36]        *x32*x45    
        ;

    return v_y_e_qd_1100                                  ;
}
float p_e_qd_1100                                  (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.5732704E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.69608439E-04,-0.21413829E-01,-0.22791808E-01, 0.19964862E-02,
         0.34276275E-02, 0.66658802E-03, 0.71540585E-03,-0.71672181E-03,
        -0.41855080E-03,-0.65071625E-03,-0.38771433E-03,-0.17255050E-03,
         0.31207014E-06,-0.18391534E-03,-0.36266621E-03,-0.55808254E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_1100                                  =v_p_e_qd_1100                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_1100                                  ;
}
float l_e_qd_1100                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3596472E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35347915E-02,-0.72931274E-04,-0.67935460E-02,-0.10179343E-02,
        -0.53489250E-02,-0.57010362E-02, 0.55949786E-03, 0.11270363E-02,
        -0.27364073E-03, 0.56981607E-04, 0.18688716E-03, 0.41756447E-03,
         0.13894781E-02,-0.64944892E-04, 0.10906927E-03,-0.28807928E-04,
         0.82645696E-04, 0.59763616E-03,-0.10813673E-02,-0.34360818E-03,
        -0.73585729E-03,-0.10687580E-02, 0.33729066E-03,-0.39694717E-03,
         0.10054777E-02,-0.83990395E-03, 0.42211711E-02,-0.16137980E-02,
         0.30217546E-02, 0.17576581E-02,-0.76067331E-03, 0.11935622E-02,
         0.88271353E-03, 0.90359349E-03,-0.49729203E-03, 0.41407102E-03,
         0.72184880E-03, 0.99907548E-03, 0.12805989E-02,-0.48278985E-03,
        -0.31355808E-02,-0.49949414E-02,-0.25818746E-02,-0.15851193E-02,
         0.22932768E-02, 0.14459434E-02, 0.15405625E-02,-0.12206784E-02,
         0.63171599E-03,-0.44605867E-02,-0.12703858E-02,-0.64775813E-04,
        -0.19644457E-03,-0.98391283E-04, 0.31305020E-03,-0.16344943E-03,
        -0.14601581E-03, 0.17626875E-03,-0.27105655E-03, 0.57939265E-04,
        -0.12726993E-03, 0.24029159E-03, 0.10646134E-03, 0.12013620E-03,
        -0.18877393E-03,-0.37122922E-03, 0.49117580E-03,-0.70460251E-03,
        -0.17019035E-03, 0.53079851E-03,-0.22574190E-03,-0.76542515E-03,
        -0.50391944E-03, 0.55620784E-03,-0.35886944E-03,-0.17336462E-03,
         0.28988224E-03,-0.17828001E-03,-0.47604906E-03,-0.30129316E-03,
         0.27760296E-03, 0.23515732E-03,-0.32996453E-03, 0.33645402E-03,
         0.45880873E-03, 0.37281867E-03,-0.89944183E-03,-0.53152064E-03,
        -0.24753704E-03,-0.48859778E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]        *x31*x41*x51
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[  8]*x12                
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21*x31    *x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]                *x53
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x24            
        +coeff[ 16]    *x23*x31        
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 17]    *x21*x31*x42    
        +coeff[ 18]*x11*x21*x32        
        +coeff[ 19]    *x24*x31        
        +coeff[ 20]        *x31*x41*x53
        +coeff[ 21]        *x31    *x54
        +coeff[ 22]*x11*x24            
        +coeff[ 23]*x12    *x31    *x52
        +coeff[ 24]*x13    *x32        
        +coeff[ 25]    *x23*x33        
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 26]    *x22*x33*x41    
        +coeff[ 27]    *x22    *x42*x52
        +coeff[ 28]        *x32*x42*x52
        +coeff[ 29]    *x21    *x43*x52
        +coeff[ 30]*x11*x24*x31        
        +coeff[ 31]*x11    *x34    *x51
        +coeff[ 32]*x11*x21*x31*x42*x51
        +coeff[ 33]*x11*x22*x31    *x52
        +coeff[ 34]*x11*x22        *x53
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 35]*x12*x24            
        +coeff[ 36]*x12*x22*x31    *x51
        +coeff[ 37]*x12*x21*x31    *x52
        +coeff[ 38]*x13*x21*x32        
        +coeff[ 39]*x11*x23*x33        
        +coeff[ 40]*x11    *x34*x42    
        +coeff[ 41]*x11    *x33*x43    
        +coeff[ 42]*x11    *x32*x44    
        +coeff[ 43]*x11*x22*x32    *x52
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 44]*x11*x22    *x42*x52
        +coeff[ 45]*x11    *x31*x43*x52
        +coeff[ 46]*x12*x21*x33*x41    
        +coeff[ 47]*x12*x22*x31*x41*x51
        +coeff[ 48]*x12*x23        *x52
        +coeff[ 49]    *x24*x33*x41    
        +coeff[ 50]*x13*x22    *x42    
        +coeff[ 51]            *x41    
        +coeff[ 52]*x11                
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 53]            *x41*x51
        +coeff[ 54]                *x52
        +coeff[ 55]*x11        *x41    
        +coeff[ 56]    *x23            
        +coeff[ 57]        *x32*x41    
        +coeff[ 58]    *x22        *x51
        +coeff[ 59]    *x21    *x41*x51
        +coeff[ 60]*x11    *x31*x41    
        +coeff[ 61]*x11        *x42    
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 62]*x11        *x41*x51
        +coeff[ 63]*x12*x21            
        +coeff[ 64]    *x22*x32        
        +coeff[ 65]        *x33*x41    
        +coeff[ 66]    *x22    *x42    
        +coeff[ 67]        *x32*x42    
        +coeff[ 68]    *x21    *x43    
        +coeff[ 69]        *x31*x43    
        +coeff[ 70]        *x31*x42*x51
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 71]        *x32    *x52
        +coeff[ 72]    *x21    *x41*x52
        +coeff[ 73]        *x31*x41*x52
        +coeff[ 74]            *x42*x52
        +coeff[ 75]*x11*x23            
        +coeff[ 76]*x11    *x32*x41    
        +coeff[ 77]*x11*x21    *x42    
        +coeff[ 78]*x11    *x32    *x51
        +coeff[ 79]*x12*x21*x31        
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 80]*x12    *x32        
        +coeff[ 81]*x12    *x31*x41    
        +coeff[ 82]    *x22*x32*x41    
        +coeff[ 83]    *x21*x32*x42    
        +coeff[ 84]    *x22*x32    *x51
        +coeff[ 85]    *x21*x32*x41*x51
        +coeff[ 86]        *x32*x42*x51
        +coeff[ 87]        *x31*x43*x51
        +coeff[ 88]        *x33    *x52
    ;
    v_l_e_qd_1100                                  =v_l_e_qd_1100                                  
        +coeff[ 89]    *x21*x31    *x53
        ;

    return v_l_e_qd_1100                                  ;
}
float x_e_qd_1000                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315490E+01;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.97819255E-03,-0.67702355E-02, 0.22724800E+00, 0.10996602E-01,
         0.58505987E-03,-0.16324591E-02,-0.49773608E-02,-0.38235486E-02,
        -0.55917291E-04,-0.12175833E-03,-0.11633987E-02,-0.18279048E-02,
        -0.65501698E-03,-0.40643504E-02,-0.33024913E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_qd_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_1000                                  =v_x_e_qd_1000                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x54
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_1000                                  ;
}
float t_e_qd_1000                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5783573E+00;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.97909570E-03,-0.33439954E-02, 0.72445430E-01, 0.33359446E-02,
        -0.22376070E-02,-0.17314079E-02,-0.17906711E-02,-0.72002527E-03,
         0.20902501E-03, 0.24450131E-03,-0.56526333E-03,-0.69752405E-03,
        -0.21248304E-03,-0.27525728E-03,-0.72796931E-04,-0.22636298E-02,
        -0.17313454E-02,-0.14519665E-02, 0.22269851E-03, 0.17599217E-03,
         0.29818845E-03, 0.14774504E-03,-0.67195407E-03,-0.16437869E-02,
         0.20601525E-04, 0.25489801E-05,-0.44741984E-04,-0.33549382E-04,
        -0.15906784E-04, 0.74821815E-04,-0.88553425E-05,-0.26505581E-04,
        -0.68990876E-05,-0.10281761E-04, 0.14780324E-04, 0.12758748E-04,
         0.40094092E-04, 0.77862642E-04, 0.15991443E-04, 0.21448706E-04,
        -0.10653701E-03,-0.71691022E-04,-0.21851836E-04, 0.28549308E-04,
         0.17777193E-04,-0.12823330E-04,-0.18030814E-04,-0.16812990E-04,
         0.69202120E-04, 0.82526320E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_1000                                  =v_t_e_qd_1000                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_1000                                  =v_t_e_qd_1000                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_1000                                  =v_t_e_qd_1000                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]*x11            *x52
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x11    *x32    *x52
        +coeff[ 31]*x11    *x32        
        +coeff[ 32]*x11    *x33        
        +coeff[ 33]    *x21    *x43    
        +coeff[ 34]*x12*x21        *x51
    ;
    v_t_e_qd_1000                                  =v_t_e_qd_1000                                  
        +coeff[ 35]*x11*x22        *x51
        +coeff[ 36]    *x23        *x51
        +coeff[ 37]    *x21*x32    *x51
        +coeff[ 38]    *x22        *x52
        +coeff[ 39]    *x21        *x53
        +coeff[ 40]*x11*x22*x31*x41    
        +coeff[ 41]*x11*x22    *x42    
        +coeff[ 42]*x11*x22    *x41*x51
        +coeff[ 43]    *x21*x32    *x52
    ;
    v_t_e_qd_1000                                  =v_t_e_qd_1000                                  
        +coeff[ 44]    *x22    *x41*x52
        +coeff[ 45]*x11*x21        *x53
        +coeff[ 46]*x12*x23*x31        
        +coeff[ 47]    *x22*x33    *x51
        +coeff[ 48]    *x23    *x42*x51
        +coeff[ 49]    *x21*x32*x42*x51
        ;

    return v_t_e_qd_1000                                  ;
}
float y_e_qd_1000                                  (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.9891197E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.61266910E-04, 0.18004356E+00, 0.86786620E-01, 0.40241499E-02,
         0.10880008E-02,-0.12713940E-02,-0.82833256E-03,-0.12090055E-02,
        -0.11497357E-03,-0.18962618E-03,-0.77550259E-03,-0.16700965E-03,
        -0.25695655E-03,-0.62831736E-03,-0.13856050E-03,-0.13541590E-04,
        -0.80155558E-03,-0.64329407E-03,-0.50921306E-04,-0.38079088E-03,
         0.15090688E-04, 0.14865611E-03, 0.59786911E-04,-0.15274890E-02,
        -0.84503576E-04,-0.10708455E-02,-0.28805676E-03, 0.34561387E-03,
        -0.83713305E-04, 0.31260960E-03, 0.69116577E-04, 0.99821642E-04,
        -0.16671795E-03, 0.31447420E-04,-0.11769576E-02, 0.21980657E-04,
        -0.10842581E-03, 0.15160889E-04,-0.87972170E-04, 0.41657448E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_qd_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_1000                                  =v_y_e_qd_1000                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_1000                                  =v_y_e_qd_1000                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x24*x31*x42    
        +coeff[ 19]    *x24    *x45    
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]        *x31*x42*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_qd_1000                                  =v_y_e_qd_1000                                  
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]    *x22    *x45    
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]            *x43*x51
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]        *x31*x44    
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_qd_1000                                  =v_y_e_qd_1000                                  
        +coeff[ 35]            *x41*x53
        +coeff[ 36]*x11*x23    *x41    
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]*x11*x23*x31        
        +coeff[ 39]    *x24    *x41*x51
        ;

    return v_y_e_qd_1000                                  ;
}
float p_e_qd_1000                                  (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.7215393E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.67000525E-04,-0.21428240E-01,-0.22801530E-01, 0.19967654E-02,
         0.34266815E-02, 0.66683383E-03, 0.71580353E-03,-0.71574701E-03,
        -0.41633818E-03,-0.65630366E-03,-0.38820921E-03,-0.17296609E-03,
        -0.62648601E-05,-0.17932367E-03,-0.36131285E-03,-0.54454140E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_1000                                  =v_p_e_qd_1000                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_1000                                  ;
}
float l_e_qd_1000                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3578019E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35800959E-02,-0.68517225E-02,-0.88128820E-03,-0.50504114E-02,
        -0.59290002E-02, 0.11603851E-03,-0.62736415E-03, 0.42733905E-03,
        -0.27793940E-03, 0.12304104E-03, 0.17013323E-02, 0.14692906E-03,
         0.45554008E-03,-0.50078257E-03, 0.38639380E-03,-0.57029416E-03,
         0.33404451E-03,-0.38462883E-03, 0.28825350E-03, 0.13934426E-02,
        -0.10752280E-02,-0.11460179E-02,-0.62458601E-03, 0.22626770E-03,
         0.21379953E-03,-0.79053157E-03, 0.21249268E-02, 0.71586127E-03,
        -0.14265185E-02, 0.17437055E-03,-0.13216417E-02,-0.56103535E-03,
         0.19386187E-04, 0.59979258E-03, 0.33197875E-03,-0.17002327E-03,
        -0.35895678E-03, 0.58549165E-04, 0.16471200E-04, 0.18343766E-03,
         0.24252356E-03, 0.92234572E-04,-0.30971249E-03,-0.35618959E-03,
        -0.30171787E-03, 0.25937558E-03, 0.22727957E-03, 0.15680645E-03,
         0.25084641E-03, 0.24276556E-03, 0.48084017E-04, 0.51477691E-03,
        -0.11457790E-02,-0.41144576E-04,-0.20332233E-03,-0.19196088E-03,
        -0.22632495E-03, 0.22934952E-03,-0.17624329E-03,-0.62113785E-03,
         0.34948075E-03, 0.11786907E-03,-0.31184373E-03, 0.62426663E-03,
         0.54803793E-03,-0.12609984E-02,-0.44051866E-03,-0.42310575E-03,
         0.86953405E-04,-0.51909679E-03, 0.69521257E-03, 0.92279637E-03,
         0.17544687E-03, 0.41739483E-03, 0.44064812E-03,-0.69265906E-03,
        -0.66361274E-03, 0.25634360E-03,-0.61069516E-03,-0.48092776E-03,
        -0.66887750E-03,-0.37324973E-03, 0.58186217E-03,-0.13956946E-03,
         0.38324212E-03,-0.28734325E-03, 0.59268310E-04,-0.44952083E-03,
         0.68554669E-04,-0.46934091E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_qd_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]*x12            *x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[  8]        *x33    *x51
        +coeff[  9]    *x21*x31    *x51
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]*x11*x21    *x41    
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]    *x21*x33        
        +coeff[ 15]        *x34        
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 17]    *x21*x31    *x52
        +coeff[ 18]*x12*x21        *x51
        +coeff[ 19]    *x22*x32    *x51
        +coeff[ 20]    *x22*x31*x41*x51
        +coeff[ 21]        *x33*x41*x51
        +coeff[ 22]*x11*x21    *x43    
        +coeff[ 23]*x11*x21        *x53
        +coeff[ 24]*x11        *x41*x53
        +coeff[ 25]*x11*x21*x32*x41*x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 26]*x11    *x31*x41*x53
        +coeff[ 27]    *x23    *x44    
        +coeff[ 28]    *x22*x34    *x51
        +coeff[ 29]*x13    *x31*x41*x51
        +coeff[ 30]            *x44*x53
        +coeff[ 31]*x12    *x34    *x51
        +coeff[ 32]*x12                
        +coeff[ 33]    *x21*x31*x41    
        +coeff[ 34]        *x31*x42    
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 35]    *x21    *x41*x51
        +coeff[ 36]*x11    *x32        
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]*x12    *x31        
        +coeff[ 39]*x13                
        +coeff[ 40]    *x24            
        +coeff[ 41]    *x23    *x41    
        +coeff[ 42]    *x23        *x51
        +coeff[ 43]        *x31*x42*x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 44]            *x43*x51
        +coeff[ 45]        *x32    *x52
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]            *x41*x53
        +coeff[ 48]*x11*x23            
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]*x11    *x32*x41    
        +coeff[ 51]*x11    *x31*x42    
        +coeff[ 52]*x11    *x31*x41*x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 53]*x11    *x31    *x52
        +coeff[ 54]*x11        *x41*x52
        +coeff[ 55]*x12    *x31    *x51
        +coeff[ 56]*x12            *x52
        +coeff[ 57]*x13*x21            
        +coeff[ 58]*x13    *x31        
        +coeff[ 59]    *x24*x31        
        +coeff[ 60]    *x22*x33        
        +coeff[ 61]*x13        *x41    
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 62]    *x24    *x41    
        +coeff[ 63]    *x23*x31*x41    
        +coeff[ 64]        *x34*x41    
        +coeff[ 65]    *x21*x31*x43    
        +coeff[ 66]        *x32*x43    
        +coeff[ 67]    *x21    *x44    
        +coeff[ 68]*x13            *x51
        +coeff[ 69]    *x24        *x51
        +coeff[ 70]    *x21    *x43*x51
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 71]            *x44*x51
        +coeff[ 72]    *x23        *x52
        +coeff[ 73]    *x22*x31    *x52
        +coeff[ 74]    *x22    *x41*x52
        +coeff[ 75]        *x32*x41*x52
        +coeff[ 76]        *x31*x42*x52
        +coeff[ 77]    *x22        *x53
        +coeff[ 78]    *x21*x31    *x53
        +coeff[ 79]    *x21    *x41*x53
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 80]        *x31*x41*x53
        +coeff[ 81]*x11*x24            
        +coeff[ 82]*x11*x22*x32        
        +coeff[ 83]*x11*x21*x33        
        +coeff[ 84]*x11    *x33    *x51
        +coeff[ 85]*x11    *x31    *x53
        +coeff[ 86]*x12*x21*x32        
        +coeff[ 87]*x12*x21*x31*x41    
        +coeff[ 88]*x12    *x32*x41    
    ;
    v_l_e_qd_1000                                  =v_l_e_qd_1000                                  
        +coeff[ 89]*x12    *x31*x42    
        ;

    return v_l_e_qd_1000                                  ;
}
float x_e_qd_900                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315803E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.67574211E-03,-0.67670168E-02, 0.22743545E+00, 0.11000850E-01,
         0.57632627E-03,-0.16540018E-02,-0.50563822E-02,-0.38749913E-02,
         0.20863023E-04,-0.31764925E-04,-0.10989057E-02,-0.19000751E-02,
        -0.62212744E-03,-0.38742521E-02,-0.31569235E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_900                                  =v_x_e_qd_900                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_900                                  ;
}
float t_e_qd_900                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5782575E+00;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.88228215E-03,-0.33444553E-02, 0.72492220E-01, 0.33564605E-02,
        -0.22308370E-02,-0.17242504E-02,-0.18037317E-02,-0.68600557E-03,
         0.21046129E-03, 0.24523347E-03,-0.57602621E-03,-0.68892993E-03,
        -0.21357113E-03,-0.25963047E-03,-0.21923839E-02,-0.16825837E-02,
        -0.15130469E-02,-0.72303796E-04, 0.27804062E-03, 0.15526329E-03,
        -0.72279759E-03,-0.17175848E-02, 0.90130532E-04,-0.19897193E-06,
        -0.20168279E-05,-0.49910006E-04,-0.38476730E-04, 0.19998611E-03,
         0.19780592E-03, 0.49522839E-06,-0.68763001E-06,-0.34760818E-04,
         0.55005239E-04, 0.12906184E-04, 0.12766036E-05, 0.62475992E-05,
        -0.27145090E-04,-0.12466588E-04,-0.23521510E-04, 0.11854574E-04,
        -0.10068329E-04, 0.69526097E-04, 0.66369095E-04, 0.33910915E-05,
         0.22220042E-04,-0.95580108E-04,-0.67916139E-04,-0.80838217E-04,
        -0.49771741E-04, 0.77467783E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_900                                  =v_t_e_qd_900                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23*x31*x41    
        +coeff[ 15]    *x23    *x42    
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_qd_900                                  =v_t_e_qd_900                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_900                                  =v_t_e_qd_900                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]        *x32    *x52
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]*x12*x21*x31    *x51
        +coeff[ 32]*x12*x22*x32        
        +coeff[ 33]*x12*x22*x31*x41    
        +coeff[ 34]*x12                
    ;
    v_t_e_qd_900                                  =v_t_e_qd_900                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]*x11    *x32        
        +coeff[ 37]*x11            *x52
        +coeff[ 38]*x12*x22            
        +coeff[ 39]*x11*x22*x31        
        +coeff[ 40]*x12    *x32        
        +coeff[ 41]    *x22*x32        
        +coeff[ 42]    *x21*x32    *x51
        +coeff[ 43]*x12        *x41*x51
    ;
    v_t_e_qd_900                                  =v_t_e_qd_900                                  
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]*x12*x22    *x41*x51
        +coeff[ 49]    *x23    *x42*x51
        ;

    return v_t_e_qd_900                                  ;
}
float y_e_qd_900                                  (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.1674844E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.10618324E-03, 0.18002619E+00, 0.86727761E-01, 0.40393472E-02,
         0.10917744E-02,-0.12897126E-02,-0.81811077E-03,-0.11853598E-02,
        -0.98132608E-04,-0.22236997E-03,-0.72867167E-03,-0.16027402E-03,
        -0.26082661E-03,-0.64067234E-03,-0.14154251E-03,-0.32711272E-04,
        -0.77459699E-03,-0.66339161E-03,-0.42344531E-04, 0.18157093E-03,
        -0.18606221E-03,-0.15182402E-02,-0.13336221E-03,-0.11073789E-02,
        -0.29939361E-04,-0.28153943E-03, 0.33711975E-04,-0.12998958E-03,
         0.13237049E-03,-0.76110104E-04,-0.75717803E-05, 0.10989241E-04,
         0.76905606E-04, 0.10684584E-03, 0.54919601E-04, 0.26278060E-04,
        -0.97093987E-03, 0.35581463E-04, 0.25110239E-04,-0.46232195E-04,
         0.11170933E-04, 0.21131767E-04, 0.44338329E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_900                                  =v_y_e_qd_900                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_900                                  =v_y_e_qd_900                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x31*x44    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]        *x34*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_qd_900                                  =v_y_e_qd_900                                  
        +coeff[ 26]    *x22    *x45    
        +coeff[ 27]    *x22*x31*x44    
        +coeff[ 28]    *x24    *x43    
        +coeff[ 29]    *x24    *x45    
        +coeff[ 30]    *x21*x31*x41    
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]            *x43*x51
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_qd_900                                  =v_y_e_qd_900                                  
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]*x11    *x31*x42*x51
        +coeff[ 39]*x11*x23*x31        
        +coeff[ 40]    *x22    *x43*x51
        +coeff[ 41]*x11*x23    *x42    
        +coeff[ 42]    *x24    *x41*x51
        ;

    return v_y_e_qd_900                                  ;
}
float p_e_qd_900                                  (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.4279451E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.32955693E-04,-0.21442089E-01,-0.22837525E-01, 0.19960927E-02,
         0.34283577E-02, 0.66764856E-03, 0.71803841E-03,-0.71659283E-03,
        -0.41822516E-03,-0.65240776E-03,-0.38494775E-03,-0.17172741E-03,
        -0.33790195E-05,-0.18109084E-03,-0.36324281E-03,-0.55742086E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_900                                  =v_p_e_qd_900                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_900                                  ;
}
float l_e_qd_900                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3566400E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35646700E-02,-0.69023832E-02,-0.14180160E-02,-0.47885492E-02,
        -0.54971366E-02,-0.15994621E-03, 0.52610657E-03,-0.52624667E-03,
         0.89405294E-04, 0.25528259E-03, 0.61626342E-03,-0.29900609E-03,
        -0.68216631E-03,-0.89634064E-04,-0.10466125E-02, 0.22159617E-03,
         0.66004333E-03,-0.53792348E-03,-0.32368622E-03,-0.44995363E-03,
        -0.36716199E-03,-0.43752734E-03,-0.57044276E-03,-0.64153119E-03,
        -0.63709426E-03, 0.18253532E-02,-0.34802323E-03, 0.10136644E-02,
        -0.11045159E-03,-0.10350826E-02, 0.40933950E-03,-0.87870780E-03,
        -0.24668890E-03,-0.22279490E-02,-0.16108786E-02, 0.10720260E-02,
        -0.16012636E-02,-0.14091978E-02, 0.21313790E-02, 0.27122011E-03,
        -0.93017664E-03, 0.72115083E-03,-0.25475670E-02,-0.22354850E-02,
         0.16802277E-02,-0.12977010E-02, 0.54889102E-03, 0.19152792E-02,
         0.10814494E-03,-0.78109279E-03,-0.13817845E-03, 0.70607901E-03,
         0.92542636E-04,-0.49063773E-03, 0.94239695E-04,-0.10450240E-03,
        -0.12109563E-03, 0.52028918E-03, 0.69790595E-03,-0.38841958E-03,
         0.39368193E-03, 0.10309870E-02, 0.26321883E-03, 0.92042278E-03,
        -0.42797875E-03,-0.24609620E-03, 0.11721252E-03,-0.99275574E-04,
        -0.42092110E-03, 0.10245488E-02,-0.59532071E-03, 0.51174662E-03,
         0.23547580E-03, 0.71919349E-03, 0.20993619E-03, 0.19432914E-03,
        -0.18192469E-02,-0.43569109E-03, 0.42403478E-03,-0.27935155E-03,
        -0.24474174E-03,-0.91995893E-03,-0.45234943E-03,-0.32633395E-03,
         0.18201378E-03, 0.22916458E-03,-0.19385316E-03,-0.33488340E-03,
        -0.98726619E-03, 0.32628924E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[  8]                *x53
        +coeff[  9]        *x32    *x51
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]*x11*x21    *x41    
        +coeff[ 12]*x11        *x41*x51
        +coeff[ 13]*x12    *x31        
        +coeff[ 14]    *x21*x31*x42    
        +coeff[ 15]    *x21*x31    *x52
        +coeff[ 16]*x11    *x31*x41*x51
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 17]*x12        *x42    
        +coeff[ 18]*x12    *x31    *x51
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]    *x21    *x41*x53
        +coeff[ 21]    *x21        *x54
        +coeff[ 22]*x11*x21*x33        
        +coeff[ 23]*x11*x21*x31*x42    
        +coeff[ 24]*x11    *x33    *x51
        +coeff[ 25]*x11    *x31*x42*x51
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 26]*x11    *x31    *x53
        +coeff[ 27]*x11        *x41*x53
        +coeff[ 28]*x12*x21*x32        
        +coeff[ 29]*x12*x21    *x41*x51
        +coeff[ 30]*x12        *x42*x51
        +coeff[ 31]    *x23*x33        
        +coeff[ 32]    *x23*x32*x41    
        +coeff[ 33]*x11*x22*x32*x41    
        +coeff[ 34]*x11    *x32*x43    
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 35]*x11*x22*x32    *x51
        +coeff[ 36]*x11    *x31*x43*x51
        +coeff[ 37]*x11    *x32    *x53
        +coeff[ 38]*x11    *x31    *x54
        +coeff[ 39]*x12*x24            
        +coeff[ 40]*x13*x22*x31        
        +coeff[ 41]    *x23        *x54
        +coeff[ 42]*x11*x22*x33    *x51
        +coeff[ 43]*x11    *x31*x44*x51
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 44]*x11*x22*x31    *x53
        +coeff[ 45]*x11    *x32*x41*x53
        +coeff[ 46]*x12*x21*x33*x41    
        +coeff[ 47]*x12*x21*x32    *x52
        +coeff[ 48]    *x21            
        +coeff[ 49]    *x21*x31        
        +coeff[ 50]    *x21    *x41    
        +coeff[ 51]*x11    *x31        
        +coeff[ 52]*x11        *x41    
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 53]*x11            *x51
        +coeff[ 54]*x12                
        +coeff[ 55]        *x33        
        +coeff[ 56]    *x21*x31    *x51
        +coeff[ 57]    *x21    *x41*x51
        +coeff[ 58]        *x31*x41*x51
        +coeff[ 59]    *x21        *x52
        +coeff[ 60]*x11    *x31    *x51
        +coeff[ 61]    *x23*x31        
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 62]    *x22*x32        
        +coeff[ 63]    *x21*x33        
        +coeff[ 64]        *x32*x42    
        +coeff[ 65]        *x31*x43    
        +coeff[ 66]    *x22        *x52
        +coeff[ 67]            *x41*x53
        +coeff[ 68]*x11    *x33        
        +coeff[ 69]*x11    *x32*x41    
        +coeff[ 70]*x11    *x31*x42    
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 71]*x11        *x43    
        +coeff[ 72]*x11*x21*x31    *x51
        +coeff[ 73]*x11    *x32    *x51
        +coeff[ 74]*x11*x21    *x41*x51
        +coeff[ 75]*x11*x21        *x52
        +coeff[ 76]*x11    *x31    *x52
        +coeff[ 77]*x11        *x41*x52
        +coeff[ 78]*x11            *x53
        +coeff[ 79]*x13*x21            
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 80]*x13        *x41    
        +coeff[ 81]    *x22*x31*x42    
        +coeff[ 82]    *x21*x32*x42    
        +coeff[ 83]    *x22    *x43    
        +coeff[ 84]    *x21    *x44    
        +coeff[ 85]*x13            *x51
        +coeff[ 86]        *x32*x42*x51
        +coeff[ 87]    *x21    *x43*x51
        +coeff[ 88]        *x31*x43*x51
    ;
    v_l_e_qd_900                                  =v_l_e_qd_900                                  
        +coeff[ 89]    *x22*x31    *x52
        ;

    return v_l_e_qd_900                                  ;
}
float x_e_qd_800                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1316567E+01;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.31037889E-04,-0.67649987E-02, 0.22747847E+00, 0.11006456E-01,
         0.57350588E-03,-0.16558476E-02,-0.50731222E-02,-0.38981289E-02,
         0.16930525E-05,-0.18449397E-03,-0.10836227E-02,-0.18494765E-02,
        -0.64218341E-03,-0.38587707E-02,-0.31349282E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_qd_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_800                                  =v_x_e_qd_800                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x54
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_800                                  ;
}
float t_e_qd_800                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5780016E+00;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64833468E-03,-0.33399190E-02, 0.72547667E-01, 0.33256593E-02,
        -0.22433014E-02,-0.17008947E-02,-0.18101289E-02,-0.71576762E-03,
         0.20990193E-03, 0.24465093E-03,-0.56370877E-03,-0.67579316E-03,
        -0.21471584E-03,-0.26899140E-03,-0.77875171E-04,-0.22155270E-02,
        -0.16805010E-02,-0.15167993E-02, 0.16915347E-03, 0.28787128E-03,
         0.21385709E-03,-0.73499518E-03,-0.17061315E-02, 0.50357699E-04,
        -0.15098488E-04, 0.75163416E-06,-0.32240190E-04,-0.43332049E-04,
        -0.38625653E-05, 0.18476899E-03,-0.22817853E-05,-0.20142284E-04,
         0.64404248E-05, 0.23488001E-04,-0.30610440E-05, 0.19130141E-05,
         0.54528341E-05,-0.29510371E-04,-0.14821619E-04, 0.61158076E-04,
         0.77919140E-04, 0.11886167E-03,-0.15490696E-04,-0.13225369E-04,
        -0.10509288E-03,-0.34558590E-04,-0.63007894E-04, 0.16941794E-04,
        -0.55718730E-04,-0.36169673E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_800                                  =v_t_e_qd_800                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_800                                  =v_t_e_qd_800                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x22*x31*x41    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x31*x41    
    ;
    v_t_e_qd_800                                  =v_t_e_qd_800                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]*x12    *x32        
        +coeff[ 29]    *x22*x31*x41    
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]*x11*x21        *x53
        +coeff[ 32]*x12*x22*x32        
        +coeff[ 33]    *x22*x32    *x52
        +coeff[ 34]*x12                
    ;
    v_t_e_qd_800                                  =v_t_e_qd_800                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]    *x21    *x41    
        +coeff[ 37]*x11    *x32        
        +coeff[ 38]*x11            *x52
        +coeff[ 39]    *x22*x32        
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]    *x21*x32    *x51
        +coeff[ 42]*x12*x22    *x41    
        +coeff[ 43]*x11*x23    *x41    
    ;
    v_t_e_qd_800                                  =v_t_e_qd_800                                  
        +coeff[ 44]*x11*x22*x31*x41    
        +coeff[ 45]*x11    *x33*x41    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]*x11*x21*x31*x41*x51
        +coeff[ 49]*x11*x21    *x42*x51
        ;

    return v_t_e_qd_800                                  ;
}
float y_e_qd_800                                  (float *x,int m){
    int ncoeff= 39;
    float avdat=  0.9066427E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
        -0.93931577E-03, 0.17995632E+00, 0.86685009E-01, 0.40674973E-02,
         0.10981516E-02,-0.13266562E-02,-0.82019367E-03,-0.12311606E-02,
        -0.31068121E-04,-0.12687191E-03,-0.78313967E-03,-0.17198552E-03,
        -0.25524083E-03,-0.71221805E-03,-0.14026846E-03,-0.20139316E-04,
        -0.76754310E-03,-0.66380342E-03, 0.30958945E-04, 0.14300685E-03,
        -0.10547062E-03,-0.14817332E-02,-0.57195979E-04,-0.10383935E-02,
        -0.28255358E-03,-0.24315312E-03,-0.49266050E-03,-0.38672977E-04,
        -0.31751249E-03, 0.66986257E-04, 0.12938110E-04, 0.82580977E-04,
         0.48397567E-04, 0.27706856E-04,-0.67796384E-03,-0.10638597E-03,
        -0.59081245E-04,-0.86855092E-04, 0.50508770E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_qd_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_800                                  =v_y_e_qd_800                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_800                                  =v_y_e_qd_800                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x31*x44    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]    *x22    *x45    
    ;
    v_y_e_qd_800                                  =v_y_e_qd_800                                  
        +coeff[ 26]    *x22*x31*x44    
        +coeff[ 27]    *x24    *x43    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]            *x45*x53
        +coeff[ 30]*x11*x21    *x42    
        +coeff[ 31]        *x32*x41*x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_qd_800                                  =v_y_e_qd_800                                  
        +coeff[ 35]*x11*x23    *x41    
        +coeff[ 36]*x11*x21*x32*x41    
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]    *x22    *x43*x51
        ;

    return v_y_e_qd_800                                  ;
}
float p_e_qd_800                                  (float *x,int m){
    int ncoeff= 16;
    float avdat= -0.5399103E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.59384249E-04,-0.21465506E-01,-0.22870380E-01, 0.19979007E-02,
         0.34301772E-02, 0.66854479E-03, 0.71729964E-03,-0.71612251E-03,
        -0.41601178E-03,-0.66068373E-03,-0.39136599E-03,-0.17523924E-03,
         0.39485817E-05,-0.17867700E-03,-0.36918759E-03,-0.56785721E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_800                                  =v_p_e_qd_800                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_800                                  ;
}
float l_e_qd_800                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3555744E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36092093E-02,-0.72284574E-02,-0.13274173E-02,-0.48422073E-02,
        -0.56992667E-02, 0.36681356E-03,-0.33924400E-03,-0.51218776E-04,
        -0.13641776E-03, 0.81427454E-03,-0.40421484E-03, 0.57393219E-03,
         0.98276860E-03,-0.43500343E-03, 0.31329415E-03, 0.38048290E-03,
        -0.12445959E-02, 0.60945563E-03,-0.33351421E-03, 0.12889116E-02,
         0.94973465E-03,-0.41797312E-03, 0.12845241E-02, 0.65286970E-03,
        -0.14194438E-02, 0.81928738E-03,-0.63972361E-03,-0.11874556E-02,
         0.46281773E-03,-0.64205815E-03,-0.32598071E-03, 0.13798819E-02,
         0.14565641E-02,-0.67065732E-03, 0.29957590E-02, 0.10949661E-02,
        -0.14817471E-02,-0.18922073E-02, 0.41684177E-03, 0.15496407E-02,
         0.21784599E-02,-0.93011983E-03, 0.35716495E-02,-0.29132581E-04,
         0.13389530E-03,-0.14467879E-03, 0.58012261E-04,-0.24788902E-03,
        -0.49373750E-04, 0.12200123E-02,-0.39449538E-03, 0.18409914E-04,
         0.35277143E-03, 0.28078165E-03,-0.63422130E-03, 0.16030010E-03,
        -0.46418389E-03,-0.19601898E-03,-0.18395149E-03, 0.33330565E-04,
        -0.29999169E-03, 0.10626874E-03,-0.19200939E-03, 0.14896007E-03,
        -0.27005732E-03,-0.33903422E-03,-0.18156911E-03, 0.17174739E-03,
         0.52526273E-03,-0.67067461E-03, 0.73809549E-03,-0.69961004E-03,
         0.73204719E-03,-0.10905365E-02,-0.45650566E-03,-0.81010896E-03,
        -0.56034606E-03, 0.42011926E-03, 0.19364996E-03,-0.70975663E-03,
         0.37353759E-03, 0.74673916E-03, 0.55751117E-03,-0.79176963E-04,
        -0.52195298E-03,-0.47055949E-03, 0.26439209E-03,-0.96429646E-03,
         0.34167402E-04, 0.63399633E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x24        *x51
        +coeff[  7]                *x51
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[  8]*x12                
        +coeff[  9]            *x42*x51
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x24            
        +coeff[ 12]    *x22*x31*x41    
        +coeff[ 13]    *x23        *x51
        +coeff[ 14]*x11*x21*x32        
        +coeff[ 15]*x11*x21*x31    *x51
        +coeff[ 16]    *x22*x31*x41*x51
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 17]    *x22    *x41*x52
        +coeff[ 18]        *x31*x41*x53
        +coeff[ 19]*x11*x21*x31*x42    
        +coeff[ 20]*x11    *x31*x41*x52
        +coeff[ 21]*x13    *x31*x41    
        +coeff[ 22]    *x22*x32*x42    
        +coeff[ 23]    *x24*x31    *x51
        +coeff[ 24]    *x22*x31*x41*x52
        +coeff[ 25]    *x23        *x53
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 26]*x11    *x33*x41*x51
        +coeff[ 27]*x11*x22    *x42*x51
        +coeff[ 28]*x11        *x44*x51
        +coeff[ 29]*x12*x21    *x41*x52
        +coeff[ 30]    *x22*x33*x41*x51
        +coeff[ 31]    *x21*x34*x41*x51
        +coeff[ 32]        *x34*x42*x51
        +coeff[ 33]    *x23    *x43*x51
        +coeff[ 34]    *x22*x31*x43*x51
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 35]*x11    *x33    *x53
        +coeff[ 36]*x12    *x34*x41    
        +coeff[ 37]*x12    *x33*x42    
        +coeff[ 38]*x12*x21*x33    *x51
        +coeff[ 39]*x12    *x31*x42*x52
        +coeff[ 40]    *x23*x33*x41*x51
        +coeff[ 41]        *x34*x43*x51
        +coeff[ 42]    *x22*x31*x41*x54
        +coeff[ 43]    *x21*x31        
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 44]    *x21    *x41    
        +coeff[ 45]        *x31    *x51
        +coeff[ 46]            *x41*x51
        +coeff[ 47]*x11    *x31        
        +coeff[ 48]    *x21*x32        
        +coeff[ 49]        *x31*x41*x51
        +coeff[ 50]        *x31    *x52
        +coeff[ 51]                *x53
        +coeff[ 52]*x12    *x31        
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 53]    *x22    *x41*x51
        +coeff[ 54]    *x21*x31*x41*x51
        +coeff[ 55]        *x32    *x52
        +coeff[ 56]        *x31*x41*x52
        +coeff[ 57]            *x42*x52
        +coeff[ 58]    *x21        *x53
        +coeff[ 59]*x11*x22*x31        
        +coeff[ 60]*x11*x22    *x41    
        +coeff[ 61]*x11        *x43    
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 62]*x11*x21        *x52
        +coeff[ 63]*x11    *x31    *x52
        +coeff[ 64]*x12*x21    *x41    
        +coeff[ 65]*x12    *x31*x41    
        +coeff[ 66]*x12        *x41*x51
        +coeff[ 67]*x13    *x31        
        +coeff[ 68]    *x24*x31        
        +coeff[ 69]    *x23*x32        
        +coeff[ 70]    *x21*x34        
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 71]    *x23*x31*x41    
        +coeff[ 72]    *x21*x33*x41    
        +coeff[ 73]        *x33*x42    
        +coeff[ 74]    *x21*x31*x43    
        +coeff[ 75]        *x32*x43    
        +coeff[ 76]        *x31*x43*x51
        +coeff[ 77]        *x33    *x52
        +coeff[ 78]    *x21*x31    *x53
        +coeff[ 79]            *x42*x53
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 80]*x11*x22*x32        
        +coeff[ 81]*x11*x21*x33        
        +coeff[ 82]*x11*x21*x32*x41    
        +coeff[ 83]*x11*x22    *x42    
        +coeff[ 84]*x11*x22        *x52
        +coeff[ 85]*x11    *x31    *x53
        +coeff[ 86]*x11            *x54
        +coeff[ 87]*x12*x22*x31        
        +coeff[ 88]*x12*x21*x32        
    ;
    v_l_e_qd_800                                  =v_l_e_qd_800                                  
        +coeff[ 89]*x12*x21*x31*x41    
        ;

    return v_l_e_qd_800                                  ;
}
float x_e_qd_700                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315434E+01;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.11054401E-02,-0.67664310E-02, 0.22766489E+00, 0.10999236E-01,
         0.57313801E-03,-0.16001496E-02,-0.49898927E-02,-0.38036031E-02,
        -0.40353792E-04,-0.13203259E-04,-0.11924517E-02,-0.18222737E-02,
        -0.63272961E-03,-0.40078964E-02,-0.32878206E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_700                                  =v_x_e_qd_700                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_700                                  ;
}
float t_e_qd_700                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5783743E+00;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.10182027E-02,-0.33509776E-02, 0.72608583E-01, 0.33269546E-02,
        -0.22382562E-02,-0.16865354E-02,-0.17779049E-02,-0.74291945E-03,
         0.19689187E-03, 0.23904642E-03,-0.54587121E-03,-0.65629923E-03,
        -0.20782853E-03,-0.26261379E-03,-0.81371152E-04,-0.22653784E-02,
        -0.17461595E-02,-0.15124573E-02, 0.19111589E-03, 0.42872853E-04,
         0.26230401E-03, 0.18303246E-03,-0.73195313E-03,-0.17170130E-02,
        -0.22893355E-04,-0.13850607E-04,-0.37809939E-04,-0.33811644E-04,
         0.19629189E-03, 0.23609951E-04, 0.91943162E-04, 0.10273430E-03,
        -0.37192542E-04, 0.34256598E-04,-0.28355626E-04, 0.15652849E-04,
         0.15266819E-05,-0.13806936E-04, 0.20912565E-04,-0.11273016E-04,
        -0.57897842E-05, 0.49910534E-04, 0.14661126E-04,-0.14930427E-04,
         0.45333116E-04,-0.90702240E-04,-0.73968928E-04,-0.35962341E-04,
        -0.23498391E-04, 0.27841579E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_qd_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_700                                  =v_t_e_qd_700                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_700                                  =v_t_e_qd_700                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]        *x31*x43    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x31*x41    
    ;
    v_t_e_qd_700                                  =v_t_e_qd_700                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]        *x32*x42    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x11    *x32    *x52
        +coeff[ 33]*x12*x22*x32        
        +coeff[ 34]*x12*x22    *x42    
    ;
    v_t_e_qd_700                                  =v_t_e_qd_700                                  
        +coeff[ 35]    *x22*x32    *x52
        +coeff[ 36]        *x32        
        +coeff[ 37]*x11*x21    *x41    
        +coeff[ 38]*x13*x21            
        +coeff[ 39]*x12*x22            
        +coeff[ 40]*x12    *x32        
        +coeff[ 41]    *x22*x32        
        +coeff[ 42]*x11    *x32    *x51
        +coeff[ 43]*x12*x23            
    ;
    v_t_e_qd_700                                  =v_t_e_qd_700                                  
        +coeff[ 44]*x11*x23    *x41    
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]*x13*x21        *x51
        +coeff[ 48]*x12*x22        *x51
        +coeff[ 49]*x11*x23        *x51
        ;

    return v_t_e_qd_700                                  ;
}
float y_e_qd_700                                  (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.4137153E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.46815581E-03, 0.17979728E+00, 0.86598992E-01, 0.40585999E-02,
         0.11080002E-02,-0.13161658E-02,-0.81261341E-03,-0.12577396E-02,
         0.65254894E-05,-0.13152204E-03,-0.76565903E-03,-0.16756376E-03,
        -0.25700664E-03,-0.74153987E-03,-0.14579437E-03,-0.23956542E-04,
        -0.77924150E-03,-0.67751849E-03, 0.13589047E-04, 0.99417841E-04,
         0.11706814E-03,-0.72206785E-04, 0.47620881E-04,-0.15149768E-02,
        -0.73403062E-04,-0.11389754E-02,-0.73004185E-05,-0.27725819E-03,
         0.98734141E-04,-0.77775090E-04,-0.20693225E-03, 0.19588209E-03,
        -0.20987347E-03,-0.11382873E-04, 0.14317449E-04, 0.23320616E-04,
        -0.84278802E-03,-0.84015999E-04,-0.71402392E-04,-0.11798328E-04,
         0.97909862E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_700                                  =v_y_e_qd_700                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_700                                  =v_y_e_qd_700                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]        *x31*x44    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_qd_700                                  =v_y_e_qd_700                                  
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]            *x45*x51
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x31*x44    
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24    *x45    
        +coeff[ 33]*x12        *x41    
        +coeff[ 34]    *x21*x31*x42    
    ;
    v_y_e_qd_700                                  =v_y_e_qd_700                                  
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]*x11*x23    *x41    
        +coeff[ 38]*x11*x23*x31        
        +coeff[ 39]*x11        *x45    
        +coeff[ 40]        *x31*x44*x51
        ;

    return v_y_e_qd_700                                  ;
}
float p_e_qd_700                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.1453444E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.22639724E-04,-0.21489443E-01,-0.22911247E-01, 0.19999542E-02,
         0.34311374E-02, 0.66966057E-03, 0.72292821E-03,-0.71983744E-03,
        -0.42060518E-03,-0.65834977E-03,-0.38898471E-03,-0.17349819E-03,
        -0.26144710E-05,-0.17718252E-03,-0.36326385E-03,-0.54503165E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_700                                  =v_p_e_qd_700                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_700                                  ;
}
float l_e_qd_700                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3563850E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.34289181E-02,-0.65183453E-02,-0.12284460E-02,-0.50028982E-02,
        -0.56062178E-02, 0.20126010E-03,-0.76697679E-03,-0.18818562E-03,
         0.78984757E-03, 0.10462767E-02,-0.33803261E-03, 0.45402534E-03,
         0.52141445E-03, 0.58091455E-03, 0.22121187E-03, 0.49205957E-03,
        -0.38035488E-03, 0.49714651E-03,-0.13531217E-02,-0.93600381E-03,
         0.10819221E-02, 0.69338974E-04,-0.13443045E-02,-0.12406007E-02,
         0.55906852E-03, 0.12398025E-03,-0.15588779E-02,-0.53835165E-03,
        -0.86361152E-03, 0.55253471E-03,-0.71799231E-03,-0.12961209E-02,
         0.30014531E-02,-0.16992898E-02,-0.31570112E-03,-0.31863773E-03,
        -0.29355548E-02,-0.10180990E-02,-0.16733942E-02,-0.31559719E-02,
        -0.46700725E-03, 0.12432133E-02,-0.26460940E-02, 0.27772428E-02,
         0.33374447E-04, 0.24656352E-03, 0.31270290E-03,-0.46578763E-03,
        -0.10512970E-04,-0.20377176E-03,-0.90208114E-03,-0.53884246E-03,
         0.39532111E-04, 0.19189846E-03, 0.17962183E-03,-0.27698430E-03,
         0.11694993E-03,-0.18644918E-03,-0.29374170E-03, 0.33258161E-03,
         0.20921996E-03, 0.33896981E-03, 0.15895667E-03,-0.94108509E-04,
         0.35811085E-03, 0.50853612E-03, 0.36287014E-03,-0.27885666E-03,
         0.65786415E-03,-0.26510376E-03, 0.32783582E-03,-0.42407974E-03,
         0.68212929E-03,-0.46099885E-03,-0.33108483E-03, 0.24908886E-03,
         0.42935455E-03,-0.47070676E-03, 0.65244612E-03, 0.77316375E-03,
         0.13185914E-02,-0.73292962E-03, 0.29417968E-03, 0.69357071E-03,
        -0.41421453E-03, 0.12469181E-02,-0.50613657E-03, 0.37849296E-03,
         0.12432847E-02,-0.67028013E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23*x31*x41    
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[  8]        *x31*x41*x51
        +coeff[  9]            *x42*x51
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]        *x31*x41*x52
        +coeff[ 12]*x11*x22    *x41    
        +coeff[ 13]*x11*x21*x31*x41    
        +coeff[ 14]*x11    *x32*x41    
        +coeff[ 15]*x12        *x42    
        +coeff[ 16]*x12*x21        *x51
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 17]*x12            *x52
        +coeff[ 18]    *x22*x31*x41*x51
        +coeff[ 19]            *x44*x51
        +coeff[ 20]*x11        *x44    
        +coeff[ 21]    *x23    *x42*x51
        +coeff[ 22]    *x21*x32*x41*x52
        +coeff[ 23]    *x21    *x43*x52
        +coeff[ 24]    *x21*x31    *x54
        +coeff[ 25]*x11*x23*x32        
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 26]*x11    *x32*x43    
        +coeff[ 27]*x12*x24            
        +coeff[ 28]*x12            *x54
        +coeff[ 29]*x13*x21*x32        
        +coeff[ 30]*x13    *x31*x42    
        +coeff[ 31]    *x21*x31*x44*x51
        +coeff[ 32]*x11*x24*x31*x41    
        +coeff[ 33]*x11*x22*x31*x43    
        +coeff[ 34]*x11    *x32*x41*x53
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 35]*x11        *x43*x53
        +coeff[ 36]*x12    *x31*x43*x51
        +coeff[ 37]*x12        *x44*x51
        +coeff[ 38]*x13*x22*x31*x41    
        +coeff[ 39]*x13*x21*x31*x41*x51
        +coeff[ 40]*x13*x21    *x42*x51
        +coeff[ 41]*x13        *x43*x51
        +coeff[ 42]    *x23*x31*x42*x52
        +coeff[ 43]    *x21*x32*x41*x54
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 44]*x11                
        +coeff[ 45]            *x41*x51
        +coeff[ 46]    *x21    *x42    
        +coeff[ 47]    *x21*x31    *x51
        +coeff[ 48]        *x31    *x52
        +coeff[ 49]            *x41*x52
        +coeff[ 50]*x11        *x42    
        +coeff[ 51]*x11*x21        *x51
        +coeff[ 52]*x12    *x31        
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 53]*x12        *x41    
        +coeff[ 54]    *x21*x31*x42    
        +coeff[ 55]            *x44    
        +coeff[ 56]    *x22*x31    *x51
        +coeff[ 57]        *x33    *x51
        +coeff[ 58]    *x22    *x41*x51
        +coeff[ 59]    *x21    *x42*x51
        +coeff[ 60]    *x22        *x52
        +coeff[ 61]    *x21    *x41*x52
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 62]        *x31    *x53
        +coeff[ 63]*x11*x22        *x51
        +coeff[ 64]*x11    *x32    *x51
        +coeff[ 65]*x11    *x31*x41*x51
        +coeff[ 66]*x11        *x42*x51
        +coeff[ 67]*x12        *x41*x51
        +coeff[ 68]    *x21*x31*x43    
        +coeff[ 69]*x13            *x51
        +coeff[ 70]    *x23*x31    *x51
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 71]    *x22*x32    *x51
        +coeff[ 72]    *x21*x33    *x51
        +coeff[ 73]    *x22    *x42*x51
        +coeff[ 74]    *x21    *x43*x51
        +coeff[ 75]    *x22*x31    *x52
        +coeff[ 76]    *x21*x32    *x52
        +coeff[ 77]        *x33    *x52
        +coeff[ 78]*x11    *x31*x43    
        +coeff[ 79]*x11*x23        *x51
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 80]*x11*x21*x31*x41*x51
        +coeff[ 81]*x11    *x32*x41*x51
        +coeff[ 82]*x12    *x33        
        +coeff[ 83]*x12    *x31*x41*x51
        +coeff[ 84]*x13    *x31*x41    
        +coeff[ 85]    *x24*x31*x41    
        +coeff[ 86]    *x22*x33*x41    
        +coeff[ 87]*x13        *x42    
        +coeff[ 88]    *x22*x32*x42    
    ;
    v_l_e_qd_700                                  =v_l_e_qd_700                                  
        +coeff[ 89]        *x34*x42    
        ;

    return v_l_e_qd_700                                  ;
}
float x_e_qd_600                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1316311E+01;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.17752912E-03,-0.67595309E-02, 0.22795433E+00, 0.10992625E-01,
         0.57124492E-03,-0.16855845E-02,-0.50776489E-02,-0.38750316E-02,
        -0.25483939E-05,-0.97993529E-06,-0.97757298E-03,-0.19509618E-02,
        -0.64416166E-03,-0.38830314E-02,-0.31685864E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_600                                  =v_x_e_qd_600                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_600                                  ;
}
float t_e_qd_600                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5781073E+00;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.74043864E-03,-0.33512563E-02, 0.72746560E-01, 0.33222095E-02,
        -0.22547604E-02,-0.17155961E-02,-0.18295799E-02,-0.64880797E-03,
         0.21472282E-03, 0.24540705E-03,-0.58703648E-03,-0.71414287E-03,
        -0.20635403E-03,-0.27034176E-03,-0.63855434E-04,-0.22059688E-02,
        -0.16667454E-02,-0.14970452E-02, 0.17359060E-03, 0.18947819E-03,
         0.20200919E-03,-0.72978018E-03,-0.16501951E-02,-0.16681528E-04,
         0.35959823E-04, 0.51077874E-04,-0.45432401E-06,-0.15859994E-05,
        -0.65164793E-04,-0.35893558E-04, 0.19568930E-03,-0.24179610E-04,
        -0.40019146E-04,-0.67097017E-05, 0.90806452E-05,-0.23293143E-04,
         0.72644951E-04,-0.25372659E-04,-0.62953986E-04, 0.63202613E-04,
         0.76193217E-04, 0.19679728E-04,-0.35110337E-04, 0.20558191E-04,
        -0.20354268E-04, 0.12962847E-04,-0.29909963E-04, 0.69913003E-04,
         0.74657204E-04, 0.99622353E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_600                                  =v_t_e_qd_600                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_600                                  =v_t_e_qd_600                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x22*x31*x41    
        +coeff[ 24]    *x22*x31*x43    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_qd_600                                  =v_t_e_qd_600                                  
        +coeff[ 26]        *x32        
        +coeff[ 27]        *x31*x41    
        +coeff[ 28]*x11    *x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]    *x22*x31*x41    
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11    *x32    *x52
        +coeff[ 33]*x12                
        +coeff[ 34]    *x21*x31        
    ;
    v_t_e_qd_600                                  =v_t_e_qd_600                                  
        +coeff[ 35]*x11*x21*x32        
        +coeff[ 36]    *x22*x32        
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x21*x31*x41    
        +coeff[ 39]    *x23        *x51
        +coeff[ 40]    *x21*x32    *x51
        +coeff[ 41]    *x21        *x53
        +coeff[ 42]*x11*x22    *x42    
        +coeff[ 43]*x11*x22*x31    *x51
    ;
    v_t_e_qd_600                                  =v_t_e_qd_600                                  
        +coeff[ 44]*x11*x21*x32    *x51
        +coeff[ 45]    *x22    *x41*x52
        +coeff[ 46]*x11*x22*x31*x42    
        +coeff[ 47]*x11*x21*x31*x43    
        +coeff[ 48]    *x23*x31*x41*x51
        +coeff[ 49]    *x21*x31*x43*x51
        ;

    return v_t_e_qd_600                                  ;
}
float y_e_qd_600                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.7741575E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.85097743E-03, 0.17959131E+00, 0.86509638E-01, 0.40712319E-02,
         0.11250069E-02,-0.13319948E-02,-0.84939186E-03,-0.12326374E-02,
        -0.82977283E-04,-0.14075293E-03,-0.77230623E-03,-0.17819504E-03,
        -0.25707035E-03,-0.66384644E-03,-0.14287449E-03,-0.78993262E-05,
        -0.72954729E-03,-0.65047154E-03,-0.30985444E-04,-0.85310912E-03,
        -0.15525699E-02,-0.47343212E-04,-0.11140127E-02,-0.25526539E-03,
        -0.10473672E-03, 0.13759514E-03, 0.93751230E-04,-0.15573368E-03,
         0.51646552E-04, 0.61758074E-04, 0.44611759E-04,-0.60650818E-04,
        -0.14432740E-04, 0.62729043E-04,-0.25996413E-04, 0.51409344E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_600                                  =v_y_e_qd_600                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_600                                  =v_y_e_qd_600                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22    *x43    
        +coeff[ 20]    *x22*x31*x42    
        +coeff[ 21]        *x33*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_qd_600                                  =v_y_e_qd_600                                  
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]        *x31*x44    
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]*x11*x21    *x43    
        +coeff[ 30]*x11*x21*x31*x42    
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]    *x21    *x42*x52
        +coeff[ 33]            *x45*x51
        +coeff[ 34]*x12*x22    *x41    
    ;
    v_y_e_qd_600                                  =v_y_e_qd_600                                  
        +coeff[ 35]    *x22    *x43*x51
        ;

    return v_y_e_qd_600                                  ;
}
float p_e_qd_600                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.7020951E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.80528633E-04,-0.21528754E-01,-0.22971909E-01, 0.20000648E-02,
         0.34325086E-02, 0.67092467E-03, 0.72342705E-03,-0.71532646E-03,
        -0.42242190E-03,-0.65674272E-03,-0.39157475E-03,-0.16920100E-03,
         0.81969364E-06,-0.17530395E-03,-0.36151861E-03,-0.54367865E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_600                                  =v_p_e_qd_600                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_600                                  ;
}
float l_e_qd_600                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3569711E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36022386E-02,-0.68121064E-02,-0.13122434E-02,-0.53394656E-02,
        -0.60266359E-02, 0.20371258E-03,-0.31222307E-03,-0.25184421E-03,
        -0.90906519E-03, 0.59047448E-04,-0.14380482E-04,-0.99565268E-05,
         0.17149745E-03, 0.28870668E-03, 0.63473894E-03,-0.14791758E-03,
        -0.61083131E-03, 0.24680753E-03,-0.61349751E-03, 0.92497579E-03,
         0.77033532E-03,-0.41990416E-03, 0.55627967E-03, 0.13313734E-02,
        -0.14713817E-03,-0.84924727E-03,-0.15735405E-03, 0.13724256E-02,
         0.21629481E-03, 0.33927909E-02,-0.10105259E-02, 0.83416654E-03,
        -0.13419160E-02,-0.11517480E-02, 0.13478438E-02,-0.86498866E-03,
        -0.14001632E-02,-0.13987415E-02,-0.62730280E-03,-0.21586216E-02,
        -0.36506834E-02,-0.23028883E-02, 0.29095744E-02,-0.19174680E-02,
         0.17534519E-02, 0.96110301E-03,-0.75563858E-03,-0.12901500E-02,
         0.38350580E-03, 0.96982549E-03, 0.11804583E-02,-0.12356181E-02,
         0.24786772E-03, 0.17141811E-03, 0.25300763E-03,-0.88566903E-03,
        -0.23964851E-05,-0.53998036E-04,-0.27431297E-03,-0.26632054E-03,
        -0.11213405E-03,-0.17865184E-03, 0.13341821E-02, 0.20110798E-03,
         0.34139826E-03, 0.36859419E-03,-0.36241164E-03,-0.17067442E-03,
        -0.30871003E-03, 0.16634163E-03, 0.13205809E-03,-0.25067135E-03,
        -0.19141563E-03, 0.24240534E-03,-0.65643498E-03,-0.58772223E-03,
        -0.23982389E-03, 0.42434968E-03, 0.43423878E-03, 0.13845417E-03,
         0.34728273E-04,-0.10262193E-02,-0.57877577E-03, 0.50437317E-03,
        -0.11821187E-03, 0.23047962E-03,-0.48151601E-03,-0.15175577E-03,
         0.64785668E-03,-0.48874388E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]        *x33        
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[  8]    *x22*x33*x41    
        +coeff[  9]                *x51
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]    *x22        *x52
        +coeff[ 15]    *x21    *x41*x52
        +coeff[ 16]*x11    *x32*x41    
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 17]*x11        *x43    
        +coeff[ 18]*x11*x21    *x41*x51
        +coeff[ 19]*x12*x21*x31        
        +coeff[ 20]        *x31*x43*x51
        +coeff[ 21]            *x44*x51
        +coeff[ 22]            *x42*x53
        +coeff[ 23]*x11    *x31*x42*x51
        +coeff[ 24]*x11    *x31    *x53
        +coeff[ 25]*x12*x21*x31*x41    
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 26]*x12    *x31*x41*x51
        +coeff[ 27]*x12        *x42*x51
        +coeff[ 28]*x12*x21        *x52
        +coeff[ 29]    *x22*x32*x42    
        +coeff[ 30]        *x34*x42    
        +coeff[ 31]    *x22*x31*x43    
        +coeff[ 32]    *x23    *x41*x52
        +coeff[ 33]    *x22        *x54
        +coeff[ 34]    *x21    *x41*x54
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 35]*x11*x21*x31*x42*x51
        +coeff[ 36]*x12*x23*x31        
        +coeff[ 37]    *x23*x32    *x52
        +coeff[ 38]*x11*x22*x34        
        +coeff[ 39]*x11*x24*x31*x41    
        +coeff[ 40]*x11*x23*x31*x42    
        +coeff[ 41]*x11*x22*x32*x42    
        +coeff[ 42]*x11*x21*x33*x42    
        +coeff[ 43]*x11    *x33*x41*x52
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 44]*x11    *x31*x43*x52
        +coeff[ 45]*x12*x24    *x41    
        +coeff[ 46]*x12*x22*x32*x41    
        +coeff[ 47]*x12*x22    *x42*x51
        +coeff[ 48]*x13        *x44    
        +coeff[ 49]    *x23*x33    *x52
        +coeff[ 50]    *x22    *x44*x52
        +coeff[ 51]    *x21*x31*x42*x54
        +coeff[ 52]        *x31        
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 53]    *x21    *x41    
        +coeff[ 54]    *x21*x32        
        +coeff[ 55]        *x31*x42    
        +coeff[ 56]            *x43    
        +coeff[ 57]*x11*x22            
        +coeff[ 58]*x11    *x31    *x51
        +coeff[ 59]*x12*x21            
        +coeff[ 60]*x12        *x41    
        +coeff[ 61]*x12            *x51
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 62]    *x22*x31*x41    
        +coeff[ 63]    *x21*x31*x41*x51
        +coeff[ 64]        *x31*x41*x52
        +coeff[ 65]    *x21        *x53
        +coeff[ 66]*x11    *x33        
        +coeff[ 67]*x11*x22    *x41    
        +coeff[ 68]*x11    *x32    *x51
        +coeff[ 69]*x11        *x41*x52
        +coeff[ 70]*x12    *x32        
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 71]*x12*x21    *x41    
        +coeff[ 72]*x12*x21        *x51
        +coeff[ 73]*x13*x21            
        +coeff[ 74]    *x22*x33        
        +coeff[ 75]    *x22*x32*x41    
        +coeff[ 76]        *x34*x41    
        +coeff[ 77]        *x33*x42    
        +coeff[ 78]        *x31*x44    
        +coeff[ 79]*x13            *x51
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 80]    *x22*x32    *x51
        +coeff[ 81]    *x22*x31*x41*x51
        +coeff[ 82]    *x22    *x42*x51
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]        *x33    *x52
        +coeff[ 85]    *x21    *x42*x52
        +coeff[ 86]        *x31*x42*x52
        +coeff[ 87]    *x21    *x41*x53
        +coeff[ 88]*x11*x23*x31        
    ;
    v_l_e_qd_600                                  =v_l_e_qd_600                                  
        +coeff[ 89]*x11*x21*x33        
        ;

    return v_l_e_qd_600                                  ;
}
float x_e_qd_500                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314194E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.23282904E-02,-0.67581572E-02, 0.22814839E+00, 0.10978252E-01,
         0.58348547E-03,-0.16355693E-02,-0.50568511E-02,-0.38508179E-02,
         0.23030936E-04,-0.14166422E-03,-0.10526176E-02,-0.18706874E-02,
        -0.64935331E-03,-0.39076456E-02,-0.32250332E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_qd_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_500                                  =v_x_e_qd_500                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x54
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_500                                  ;
}
float t_e_qd_500                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5787733E+00;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.14121002E-02,-0.33393274E-02, 0.72876342E-01, 0.33232486E-02,
        -0.22580056E-02,-0.17180366E-02,-0.17926864E-02,-0.68256143E-03,
         0.21062276E-03, 0.24419304E-03,-0.56010246E-03,-0.68149285E-03,
        -0.19886547E-03,-0.26879297E-03,-0.74063835E-04,-0.22311725E-02,
        -0.17293153E-02,-0.15313639E-02, 0.20973814E-03, 0.16384028E-03,
         0.28876733E-03, 0.21318738E-03,-0.71683130E-03,-0.17405850E-02,
         0.23655009E-04, 0.80292984E-05,-0.50175357E-04,-0.34259778E-04,
        -0.16253742E-04, 0.53645199E-04, 0.41158211E-04,-0.11716938E-04,
        -0.27187349E-04,-0.82116267E-05,-0.10990395E-04,-0.16574717E-04,
         0.19311730E-04, 0.66986511E-04, 0.96523356E-04, 0.13266942E-04,
         0.16179805E-04,-0.10222894E-03,-0.88657391E-04,-0.20932350E-04,
         0.17033441E-04, 0.49347753E-04,-0.29974175E-04, 0.41159768E-04,
        -0.26110829E-05, 0.42250731E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_qd_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_500                                  =v_t_e_qd_500                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_500                                  =v_t_e_qd_500                                  
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_500                                  =v_t_e_qd_500                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]*x11            *x52
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]    *x21    *x41    
        +coeff[ 32]*x11    *x32        
        +coeff[ 33]    *x22    *x41    
        +coeff[ 34]*x12    *x32        
    ;
    v_t_e_qd_500                                  =v_t_e_qd_500                                  
        +coeff[ 35]*x12*x21    *x41    
        +coeff[ 36]*x11*x22        *x51
        +coeff[ 37]    *x23        *x51
        +coeff[ 38]    *x21*x32    *x51
        +coeff[ 39]    *x22        *x52
        +coeff[ 40]*x11*x23*x31        
        +coeff[ 41]*x11*x22*x31*x41    
        +coeff[ 42]*x11*x22    *x42    
        +coeff[ 43]    *x22*x32    *x51
    ;
    v_t_e_qd_500                                  =v_t_e_qd_500                                  
        +coeff[ 44]*x11*x22*x33        
        +coeff[ 45]*x12*x22    *x41*x51
        +coeff[ 46]*x11*x23    *x41*x51
        +coeff[ 47]*x12    *x31*x42*x51
        +coeff[ 48]        *x31    *x51
        +coeff[ 49]*x12*x21            
        ;

    return v_t_e_qd_500                                  ;
}
float y_e_qd_500                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.4620966E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.42104622E-03, 0.17940949E+00, 0.86322546E-01, 0.40884484E-02,
         0.11312922E-02,-0.13134218E-02,-0.80945122E-03,-0.11696575E-02,
        -0.77072254E-04,-0.19733862E-03,-0.76571363E-03,-0.16469935E-03,
        -0.25589371E-03,-0.66741655E-03,-0.14061485E-03,-0.96216527E-05,
        -0.75119262E-03,-0.66329335E-03,-0.16761916E-04,-0.21196489E-03,
        -0.91297837E-03,-0.16678368E-02,-0.95864445E-04,-0.11227051E-02,
        -0.28294098E-03,-0.10089511E-03, 0.68434038E-05, 0.13540695E-03,
         0.92950213E-04, 0.52587100E-04, 0.67374225E-04, 0.56737099E-04,
        -0.81864418E-04, 0.45239867E-04, 0.52063708E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_500                                  =v_y_e_qd_500                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_500                                  =v_y_e_qd_500                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x44    
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_qd_500                                  =v_y_e_qd_500                                  
        +coeff[ 26]        *x31*x41    
        +coeff[ 27]        *x31*x42*x51
        +coeff[ 28]        *x32*x41*x51
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]*x11*x21    *x43    
        +coeff[ 31]*x11*x21*x31*x42    
        +coeff[ 32]*x11*x23    *x41    
        +coeff[ 33]            *x45*x51
        +coeff[ 34]    *x22    *x43*x51
        ;

    return v_y_e_qd_500                                  ;
}
float p_e_qd_500                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.3302534E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.27788303E-04,-0.21576831E-01,-0.23066599E-01, 0.20042015E-02,
         0.34385358E-02, 0.67608949E-03, 0.72787388E-03,-0.72021631E-03,
        -0.41990791E-03,-0.65650325E-03,-0.38955591E-03,-0.17147395E-03,
        -0.28208819E-05,-0.17935064E-03,-0.36169926E-03,-0.53249405E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_500                                  =v_p_e_qd_500                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_500                                  ;
}
float l_e_qd_500                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3567325E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36058915E-02,-0.69936998E-02,-0.16527600E-02,-0.49786177E-02,
        -0.57138694E-02, 0.36082478E-03,-0.14590053E-02,-0.51478328E-04,
        -0.77303259E-04, 0.29770276E-03,-0.89642599E-04, 0.14086768E-03,
        -0.15505803E-03, 0.53122603E-05, 0.27130378E-03, 0.14504693E-03,
        -0.89363362E-04, 0.17434850E-03,-0.51356346E-03, 0.86083973E-03,
        -0.43449175E-03,-0.37226049E-03, 0.28269200E-03,-0.16989150E-03,
         0.81114745E-03,-0.14222310E-02, 0.14535410E-02,-0.60987647E-03,
         0.12161221E-02, 0.68879529E-03, 0.89059200E-03, 0.18801071E-02,
        -0.13218207E-02, 0.87980938E-03, 0.18022027E-02, 0.32703116E-03,
        -0.10477825E-02,-0.10897082E-03,-0.52458711E-03,-0.49806299E-03,
         0.24003622E-02,-0.56259980E-03, 0.15602795E-02, 0.88836340E-03,
        -0.28107024E-03, 0.71151281E-03,-0.60857373E-03,-0.52813427E-02,
        -0.32776922E-02, 0.22965574E-02, 0.32906369E-02,-0.17310440E-02,
         0.71978674E-03, 0.13369079E-02, 0.83268178E-03,-0.70747774E-03,
        -0.14707772E-03,-0.48030819E-04,-0.22182132E-03, 0.25584197E-03,
         0.26620217E-04,-0.19045232E-03,-0.26796604E-03,-0.12247537E-03,
         0.19670057E-03,-0.45645497E-05,-0.50261791E-03,-0.77776327E-04,
        -0.12768181E-03,-0.68615860E-04, 0.27333581E-03, 0.51062257E-03,
         0.47850510E-03, 0.29010978E-03, 0.39920665E-03, 0.91778675E-04,
         0.32303308E-03, 0.91969225E-04,-0.38860136E-03,-0.71145152E-03,
         0.37573598E-03, 0.16041164E-04,-0.19061858E-03,-0.23860837E-03,
         0.65160706E-03, 0.33621764E-03, 0.22230053E-03,-0.57048124E-03,
         0.46330778E-03,-0.34182207E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_qd_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[  8]        *x31        
        +coeff[  9]                *x51
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 17]*x12    *x31        
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43*x51
        +coeff[ 22]*x11*x22        *x51
        +coeff[ 23]*x12            *x52
        +coeff[ 24]    *x24        *x51
        +coeff[ 25]    *x22*x32    *x51
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 26]*x11*x21*x32    *x51
        +coeff[ 27]*x11*x22    *x41*x51
        +coeff[ 28]*x11        *x41*x53
        +coeff[ 29]*x12        *x43    
        +coeff[ 30]    *x22*x33*x41    
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]    *x21*x31*x41*x53
        +coeff[ 33]        *x32*x41*x53
        +coeff[ 34]*x11*x24*x31        
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]*x11*x21    *x42*x52
        +coeff[ 37]*x12*x24            
        +coeff[ 38]*x13*x22*x31        
        +coeff[ 39]*x13*x21*x31*x41    
        +coeff[ 40]    *x22*x34    *x51
        +coeff[ 41]    *x21*x32*x43*x51
        +coeff[ 42]    *x21*x33*x41*x52
        +coeff[ 43]        *x34*x41*x52
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 44]    *x23    *x41*x53
        +coeff[ 45]*x11*x23*x33        
        +coeff[ 46]*x11    *x34*x41*x51
        +coeff[ 47]*x11*x21*x32*x42*x51
        +coeff[ 48]*x11*x21*x31*x43*x51
        +coeff[ 49]*x12*x23    *x41*x51
        +coeff[ 50]*x12*x21*x31*x42*x51
        +coeff[ 51]*x12    *x32*x42*x51
        +coeff[ 52]*x12        *x44*x51
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 53]*x12*x21    *x42*x52
        +coeff[ 54]*x12*x22        *x53
        +coeff[ 55]*x13*x21*x31    *x52
        +coeff[ 56]    *x21            
        +coeff[ 57]*x11            *x51
        +coeff[ 58]*x12                
        +coeff[ 59]    *x23            
        +coeff[ 60]    *x21*x32        
        +coeff[ 61]    *x21    *x42    
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 62]    *x21*x31    *x51
        +coeff[ 63]            *x41*x52
        +coeff[ 64]*x11    *x32        
        +coeff[ 65]*x11        *x42    
        +coeff[ 66]*x11        *x41*x51
        +coeff[ 67]*x11            *x52
        +coeff[ 68]*x12            *x51
        +coeff[ 69]*x13                
        +coeff[ 70]        *x34        
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 71]    *x21*x32*x41    
        +coeff[ 72]    *x21*x31*x42    
        +coeff[ 73]    *x21    *x43    
        +coeff[ 74]    *x23        *x51
        +coeff[ 75]        *x33    *x51
        +coeff[ 76]    *x22    *x41*x51
        +coeff[ 77]        *x31    *x53
        +coeff[ 78]            *x41*x53
        +coeff[ 79]*x11*x22*x31        
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 80]*x11*x22    *x41    
        +coeff[ 81]*x11    *x31*x42    
        +coeff[ 82]*x11        *x43    
        +coeff[ 83]*x11*x21*x31    *x51
        +coeff[ 84]*x12*x22            
        +coeff[ 85]*x12    *x31*x41    
        +coeff[ 86]*x12        *x42    
        +coeff[ 87]    *x23*x32        
        +coeff[ 88]    *x21*x34        
    ;
    v_l_e_qd_500                                  =v_l_e_qd_500                                  
        +coeff[ 89]    *x22*x31*x42    
        ;

    return v_l_e_qd_500                                  ;
}
float x_e_qd_450                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315689E+01;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.85948675E-03,-0.67555201E-02, 0.22841369E+00, 0.10976010E-01,
         0.58398815E-03,-0.16401802E-02,-0.51062889E-02,-0.39134882E-02,
        -0.33941858E-04,-0.89029669E-06,-0.11110556E-02,-0.18824267E-02,
        -0.64387749E-03,-0.37510644E-02,-0.30438758E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_450                                  =v_x_e_qd_450                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_450                                  ;
}
float t_e_qd_450                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5783044E+00;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.95382362E-03,-0.33442646E-02, 0.72971046E-01, 0.33597359E-02,
        -0.22583450E-02,-0.17639106E-02,-0.18118955E-02,-0.71471080E-03,
         0.20750378E-03, 0.24569919E-03,-0.56419492E-03,-0.67035237E-03,
        -0.20808498E-03,-0.26848062E-03,-0.98866636E-04,-0.21744967E-02,
        -0.16496466E-02,-0.10901392E-05,-0.54102693E-05, 0.27258808E-03,
         0.20310277E-03,-0.17046231E-02,-0.14561268E-02, 0.13093925E-03,
         0.18877404E-05,-0.23951161E-04,-0.29222463E-04,-0.15266791E-04,
         0.22976352E-03, 0.15818178E-03,-0.44707798E-04,-0.70348073E-03,
        -0.59397807E-04, 0.10583719E-04,-0.43294393E-04,-0.56271811E-05,
         0.66103189E-05,-0.94025445E-05,-0.70064657E-05, 0.23583199E-04,
         0.71947718E-04, 0.15048989E-04, 0.16455413E-04, 0.18471317E-04,
         0.33805925E-04, 0.12907184E-04,-0.99428958E-04,-0.29627270E-04,
        -0.65025619E-04, 0.12102325E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_450                                  =v_t_e_qd_450                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_450                                  =v_t_e_qd_450                                  
        +coeff[ 17]        *x31*x41    
        +coeff[ 18]            *x42    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]        *x32        
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_450                                  =v_t_e_qd_450                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]*x11*x23        *x52
        +coeff[ 33]    *x22*x32    *x52
        +coeff[ 34]*x11*x21    *x41*x53
    ;
    v_t_e_qd_450                                  =v_t_e_qd_450                                  
        +coeff[ 35]*x12                
        +coeff[ 36]*x13                
        +coeff[ 37]*x12*x21            
        +coeff[ 38]            *x41*x52
        +coeff[ 39]*x11*x23            
        +coeff[ 40]    *x22*x32        
        +coeff[ 41]*x11*x22        *x51
        +coeff[ 42]            *x42*x52
        +coeff[ 43]    *x21        *x53
    ;
    v_t_e_qd_450                                  =v_t_e_qd_450                                  
        +coeff[ 44]*x13*x22            
        +coeff[ 45]*x13*x21*x31        
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]*x11    *x33*x41    
        +coeff[ 48]*x11*x22    *x42    
        +coeff[ 49]    *x21*x33    *x51
        ;

    return v_t_e_qd_450                                  ;
}
float y_e_qd_450                                  (float *x,int m){
    int ncoeff= 46;
    float avdat= -0.6258223E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 47]={
         0.60433010E-03, 0.17928536E+00, 0.86212605E-01, 0.40512853E-02,
         0.11205447E-02,-0.15194954E-02,-0.82529761E-03,-0.11566250E-02,
         0.23530212E-04,-0.25150136E-03,-0.74986910E-03,-0.16711037E-03,
        -0.26101863E-03,-0.78291929E-03,-0.13962298E-03,-0.26147534E-04,
        -0.56348881E-03,-0.66269771E-03, 0.33011457E-06, 0.16780167E-03,
         0.11497292E-03,-0.20832513E-03, 0.48286729E-04,-0.16097535E-02,
        -0.15890108E-03,-0.10929507E-02,-0.31334370E-04,-0.27164424E-03,
        -0.26471615E-04,-0.75166544E-03,-0.87345077E-04,-0.89247571E-03,
         0.70789718E-03, 0.70737647E-05, 0.98779616E-04, 0.25698120E-04,
         0.24060477E-04,-0.17987328E-04, 0.13057595E-04, 0.21201580E-04,
         0.24039446E-04,-0.72152070E-04,-0.26622618E-04,-0.68864501E-04,
         0.14314245E-04,-0.29028517E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_qd_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_450                                  =v_y_e_qd_450                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_450                                  =v_y_e_qd_450                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]        *x31*x44    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x33*x42    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_qd_450                                  =v_y_e_qd_450                                  
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]            *x45*x51
        +coeff[ 29]    *x22    *x45    
        +coeff[ 30]    *x22*x31*x44    
        +coeff[ 31]    *x24    *x43    
        +coeff[ 32]    *x24    *x45    
        +coeff[ 33]*x11        *x42    
        +coeff[ 34]            *x43*x51
    ;
    v_y_e_qd_450                                  =v_y_e_qd_450                                  
        +coeff[ 35]    *x22    *x41*x51
        +coeff[ 36]        *x33    *x51
        +coeff[ 37]*x11    *x31*x41*x51
        +coeff[ 38]*x11        *x41*x52
        +coeff[ 39]            *x41*x53
        +coeff[ 40]*x11*x21*x31*x42    
        +coeff[ 41]*x11*x23    *x41    
        +coeff[ 42]*x12    *x31*x42    
        +coeff[ 43]*x11*x23*x31        
    ;
    v_y_e_qd_450                                  =v_y_e_qd_450                                  
        +coeff[ 44]*x12        *x44    
        +coeff[ 45]*x11*x21    *x43*x51
        ;

    return v_y_e_qd_450                                  ;
}
float p_e_qd_450                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.1071508E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.10509758E-03,-0.21609576E-01,-0.23128856E-01, 0.20035282E-02,
         0.34398774E-02, 0.67666586E-03, 0.73034037E-03,-0.71880699E-03,
        -0.41810298E-03,-0.65449765E-03,-0.38720304E-03,-0.17279819E-03,
        -0.54104794E-05,-0.18040881E-03,-0.35975216E-03,-0.55556869E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_450                                  =v_p_e_qd_450                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_450                                  ;
}
float l_e_qd_450                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3613139E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.34490656E-02,-0.65163639E-02,-0.75090001E-03,-0.51080454E-02,
        -0.57517565E-02, 0.20791269E-04,-0.25384309E-03, 0.18841325E-03,
        -0.23724507E-03, 0.24605962E-03,-0.72438074E-05,-0.30836327E-04,
         0.23420669E-03, 0.56109129E-03,-0.13111837E-03, 0.77543966E-03,
         0.54941545E-04,-0.39564446E-03,-0.32209398E-03,-0.25325583E-03,
        -0.29263456E-03, 0.66971808E-03,-0.98256132E-04,-0.11522535E-02,
         0.13758353E-02, 0.11595346E-02,-0.81852596E-03, 0.39321839E-03,
        -0.94413391E-03,-0.23894780E-02, 0.57870966E-04, 0.15063966E-02,
         0.72762615E-03, 0.64926571E-03,-0.18484665E-02,-0.16017527E-02,
         0.72691712E-03, 0.69207506E-03, 0.99078624E-03, 0.73551881E-03,
        -0.11933459E-02, 0.16961336E-02, 0.31869840E-02, 0.18062143E-02,
        -0.36014731E-02,-0.18273069E-02,-0.16895478E-02,-0.13252262E-02,
         0.29612125E-02,-0.14507556E-02,-0.67266933E-05,-0.34537417E-03,
         0.33688179E-04,-0.80606413E-04,-0.43876564E-04, 0.61327628E-04,
         0.28775507E-03,-0.14860576E-03,-0.14690857E-03,-0.12047854E-03,
        -0.83769730E-04, 0.17388027E-03, 0.15673069E-03, 0.20277721E-03,
         0.18951802E-03, 0.34712235E-03, 0.13344933E-03,-0.37026434E-03,
         0.36048662E-03,-0.42528461E-03, 0.56034198E-03, 0.35681698E-03,
         0.18747650E-03,-0.13874444E-03, 0.51840133E-03,-0.27515518E-03,
        -0.14834477E-03, 0.22924296E-03, 0.20973192E-03,-0.21887540E-03,
         0.39784395E-03, 0.44847955E-03,-0.11407701E-03,-0.22894281E-03,
         0.50581363E-03, 0.58263325E-03, 0.31203288E-03, 0.70835049E-04,
        -0.17849510E-03,-0.18692490E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x42*x51
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[  8]*x11            *x52
        +coeff[  9]                *x51
        +coeff[ 10]*x11                
        +coeff[ 11]*x12                
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x31*x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 17]*x12            *x51
        +coeff[ 18]    *x22*x32        
        +coeff[ 19]        *x32*x42    
        +coeff[ 20]    *x21    *x41*x52
        +coeff[ 21]*x11*x21    *x42    
        +coeff[ 22]*x11    *x31*x42    
        +coeff[ 23]    *x22*x32    *x51
        +coeff[ 24]    *x21*x32*x41*x51
        +coeff[ 25]    *x21*x31*x42*x51
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 26]        *x31*x43*x51
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]*x11*x22    *x41*x51
        +coeff[ 29]*x11*x21*x31*x41*x51
        +coeff[ 30]*x11    *x32*x41*x51
        +coeff[ 31]*x11    *x31*x41*x52
        +coeff[ 32]*x12    *x32    *x51
        +coeff[ 33]    *x22*x33    *x51
        +coeff[ 34]    *x23    *x42*x51
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 35]        *x33*x42*x51
        +coeff[ 36]*x12    *x31*x43    
        +coeff[ 37]*x13*x23            
        +coeff[ 38]*x13    *x31*x42    
        +coeff[ 39]*x13*x21    *x41*x51
        +coeff[ 40]    *x24*x31*x41*x51
        +coeff[ 41]    *x21*x32*x42*x52
        +coeff[ 42]*x11*x21*x33*x41*x51
        +coeff[ 43]*x11*x22*x31*x41*x52
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 44]*x11    *x33*x41*x52
        +coeff[ 45]*x11    *x32*x42*x52
        +coeff[ 46]*x11    *x32*x41*x53
        +coeff[ 47]*x13*x22*x31    *x51
        +coeff[ 48]    *x23    *x44*x51
        +coeff[ 49]        *x34*x41*x53
        +coeff[ 50]    *x21            
        +coeff[ 51]        *x31    *x51
        +coeff[ 52]            *x41*x51
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 53]*x11    *x31        
        +coeff[ 54]    *x22*x31        
        +coeff[ 55]    *x21*x32        
        +coeff[ 56]    *x22    *x41    
        +coeff[ 57]    *x21    *x42    
        +coeff[ 58]    *x21        *x52
        +coeff[ 59]            *x41*x52
        +coeff[ 60]                *x53
        +coeff[ 61]*x11*x22            
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 62]*x11*x21    *x41    
        +coeff[ 63]*x11        *x42    
        +coeff[ 64]*x11    *x31    *x51
        +coeff[ 65]*x11        *x41*x51
        +coeff[ 66]*x12    *x31        
        +coeff[ 67]    *x23*x31        
        +coeff[ 68]    *x21*x33        
        +coeff[ 69]        *x34        
        +coeff[ 70]    *x22*x31*x41    
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 71]    *x22    *x42    
        +coeff[ 72]    *x23        *x51
        +coeff[ 73]    *x21*x32    *x51
        +coeff[ 74]        *x31*x42*x51
        +coeff[ 75]            *x43*x51
        +coeff[ 76]    *x22        *x52
        +coeff[ 77]    *x21*x31    *x52
        +coeff[ 78]        *x32    *x52
        +coeff[ 79]        *x31*x41*x52
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 80]        *x31    *x53
        +coeff[ 81]            *x41*x53
        +coeff[ 82]                *x54
        +coeff[ 83]*x11*x23            
        +coeff[ 84]*x11*x21*x32        
        +coeff[ 85]*x11*x21*x31*x41    
        +coeff[ 86]*x11*x21*x31    *x51
        +coeff[ 87]*x11            *x53
        +coeff[ 88]*x12*x22            
    ;
    v_l_e_qd_450                                  =v_l_e_qd_450                                  
        +coeff[ 89]*x12*x21*x31        
        ;

    return v_l_e_qd_450                                  ;
}
float x_e_qd_449                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314463E+01;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.20431532E-02,-0.66449945E-02, 0.23080836E+00, 0.10822142E-01,
         0.56160917E-03,-0.16291474E-02,-0.54499838E-02,-0.38183564E-02,
         0.97062511E-05, 0.17472174E-04,-0.14279706E-02,-0.22497918E-02,
        -0.62962819E-03,-0.44023902E-02,-0.32040849E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_449                                  =v_x_e_qd_449                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_449                                  ;
}
float t_e_qd_449                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5787222E+00;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13603115E-02,-0.32926579E-02, 0.73776640E-01, 0.32372691E-02,
        -0.23288825E-02,-0.19080396E-02,-0.17757656E-02,-0.88513165E-03,
         0.21246260E-03, 0.23951304E-03,-0.21745745E-03,-0.17525335E-04,
        -0.55356714E-03,-0.83553407E-03,-0.80159429E-04,-0.24672606E-03,
        -0.24960884E-02,-0.17172156E-02, 0.30051515E-03, 0.18491846E-03,
        -0.20043678E-02,-0.15852345E-02, 0.87823719E-05,-0.27606143E-05,
        -0.64222454E-05,-0.50037110E-04,-0.28448521E-04, 0.23205252E-03,
         0.17535170E-03, 0.84548319E-05, 0.83870276E-04, 0.11765047E-03,
        -0.54609350E-05,-0.90602040E-03,-0.25635356E-04,-0.25296442E-04,
        -0.51379925E-05,-0.73931674E-05,-0.32570875E-04,-0.11497244E-04,
         0.10541837E-03, 0.81658186E-06, 0.17521998E-04, 0.17292625E-04,
        -0.10963218E-03,-0.82629755E-04, 0.28922152E-04,-0.15159748E-04,
         0.17495162E-04, 0.56812387E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_449                                  =v_t_e_qd_449                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_449                                  =v_t_e_qd_449                                  
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_449                                  =v_t_e_qd_449                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]        *x32*x42    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]    *x21*x33*x41    
        +coeff[ 34]    *x22*x33*x41    
    ;
    v_t_e_qd_449                                  =v_t_e_qd_449                                  
        +coeff[ 35]    *x22*x32    *x52
        +coeff[ 36]        *x32        
        +coeff[ 37]    *x22*x31        
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11            *x52
        +coeff[ 40]    *x22*x32        
        +coeff[ 41]*x12    *x31*x41    
        +coeff[ 42]    *x22        *x52
        +coeff[ 43]    *x21        *x53
    ;
    v_t_e_qd_449                                  =v_t_e_qd_449                                  
        +coeff[ 44]*x11*x22*x31*x41    
        +coeff[ 45]*x11*x22    *x42    
        +coeff[ 46]*x12*x22        *x51
        +coeff[ 47]    *x23*x31    *x51
        +coeff[ 48]*x13*x22    *x41    
        +coeff[ 49]*x12*x22*x31*x41    
        ;

    return v_t_e_qd_449                                  ;
}
float y_e_qd_449                                  (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.8840030E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.89456508E-03, 0.17965966E+00, 0.95805787E-01, 0.40278635E-02,
         0.12062876E-02,-0.13205135E-02,-0.90817572E-03,-0.13277325E-02,
        -0.94198529E-03,-0.62687795E-04,-0.22256566E-03,-0.26114995E-03,
        -0.65875001E-03,-0.15611692E-03,-0.32121265E-04,-0.72511390E-03,
        -0.44092900E-04,-0.19293949E-03, 0.10429472E-04,-0.42889536E-04,
         0.18173826E-03, 0.13929886E-03,-0.13192755E-03, 0.60612212E-04,
        -0.18017784E-03,-0.17126012E-02,-0.74617245E-03,-0.13310170E-02,
        -0.38778596E-03, 0.72700488E-04,-0.11714786E-03,-0.29498915E-03,
         0.79605103E-04,-0.64981318E-05, 0.25693507E-04, 0.33361444E-04,
        -0.83151652E-03,-0.10833000E-03,-0.63572210E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_449                                  =v_y_e_qd_449                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]    *x24    *x43    
    ;
    v_y_e_qd_449                                  =v_y_e_qd_449                                  
        +coeff[ 17]    *x22*x32*x43    
        +coeff[ 18]    *x24*x32*x41    
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]        *x32*x41*x51
        +coeff[ 22]        *x31*x44    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]        *x32*x43    
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_qd_449                                  =v_y_e_qd_449                                  
        +coeff[ 26]    *x24    *x41    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]            *x45*x51
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]    *x22*x31*x44    
        +coeff[ 32]    *x24    *x45    
        +coeff[ 33]            *x44    
        +coeff[ 34]    *x22    *x41*x51
    ;
    v_y_e_qd_449                                  =v_y_e_qd_449                                  
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]        *x33*x42    
        +coeff[ 38]*x11*x23*x31        
        ;

    return v_y_e_qd_449                                  ;
}
float p_e_qd_449                                  (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.7134787E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.73199080E-04,-0.23683483E-01,-0.22601783E-01, 0.21952447E-02,
         0.34027717E-02, 0.75180724E-03, 0.72344817E-03,-0.72873401E-03,
         0.17121460E-04,-0.48414787E-03,-0.70980395E-03,-0.38376235E-03,
        -0.16810959E-03, 0.67338965E-05,-0.20109474E-03,-0.43782880E-03,
        -0.52856049E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_449                                  =v_p_e_qd_449                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_449                                  ;
}
float l_e_qd_449                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3565700E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35955119E-02,-0.66626361E-02,-0.16714812E-02,-0.54174922E-02,
        -0.59749833E-02, 0.62833668E-03, 0.66958870E-04, 0.14196672E-03,
         0.63027366E-03, 0.25423319E-03,-0.31216454E-03,-0.26055952E-03,
         0.21204738E-03, 0.88567100E-03, 0.42842972E-03,-0.22419416E-02,
         0.82144752E-03, 0.57458214E-03, 0.87940833E-03,-0.73717959E-03,
         0.59840141E-03, 0.53952099E-03,-0.16436310E-02,-0.36248672E-03,
        -0.58275746E-03,-0.18887591E-02, 0.13044100E-02,-0.35293287E-03,
         0.34825000E-03, 0.28516827E-03,-0.79577636E-04, 0.96463220E-03,
         0.22282200E-02, 0.10892716E-02, 0.16168620E-02, 0.96787984E-03,
        -0.10883784E-02,-0.14832334E-02, 0.11671287E-03, 0.22811057E-04,
        -0.13028982E-03,-0.59773703E-03, 0.17594101E-03,-0.14285928E-04,
        -0.15687848E-03,-0.14960625E-03, 0.72732873E-04,-0.29057355E-03,
        -0.69560891E-04,-0.24863458E-03, 0.55689336E-04, 0.60410029E-03,
         0.51911792E-03, 0.34110376E-03, 0.37970807E-03,-0.35879255E-03,
        -0.11337562E-03,-0.24718652E-03, 0.71807025E-03,-0.14277619E-03,
        -0.47709976E-03, 0.91023115E-03, 0.45775910E-03, 0.65936742E-03,
        -0.13952005E-03,-0.31724031E-03,-0.16388395E-03,-0.13272060E-03,
        -0.20929681E-03, 0.35782187E-03, 0.47327284E-03,-0.43784865E-03,
        -0.35386274E-03,-0.28551451E-03,-0.12730477E-03, 0.13567966E-02,
         0.67098887E-03,-0.25256778E-03,-0.25962776E-03,-0.20740034E-03,
        -0.27922532E-03,-0.39315701E-03,-0.64237486E-03,-0.73143578E-03,
        -0.84072090E-03,-0.69379853E-03, 0.23043291E-03,-0.33602203E-03,
         0.20962283E-03, 0.59454236E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]            *x41    
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[  8]            *x42*x51
        +coeff[  9]*x12    *x31        
        +coeff[ 10]    *x21*x33        
        +coeff[ 11]    *x22*x31    *x51
        +coeff[ 12]*x11    *x32*x41    
        +coeff[ 13]*x11    *x31    *x52
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]    *x22*x32    *x51
        +coeff[ 16]        *x34    *x51
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 17]        *x33*x41*x51
        +coeff[ 18]*x11*x21*x32*x41    
        +coeff[ 19]*x11    *x33    *x51
        +coeff[ 20]*x12*x21    *x41*x51
        +coeff[ 21]*x13    *x31    *x51
        +coeff[ 22]*x11*x24    *x41    
        +coeff[ 23]*x11*x24        *x51
        +coeff[ 24]*x11        *x44*x51
        +coeff[ 25]*x11*x22*x31    *x52
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 26]*x12*x23        *x51
        +coeff[ 27]*x12*x21*x32    *x51
        +coeff[ 28]*x13    *x33        
        +coeff[ 29]*x13*x22    *x41    
        +coeff[ 30]    *x24*x32*x41    
        +coeff[ 31]*x13*x21    *x42    
        +coeff[ 32]    *x24*x32    *x51
        +coeff[ 33]*x11*x23*x31*x42    
        +coeff[ 34]*x12*x22*x32*x41    
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 35]*x12    *x31    *x54
        +coeff[ 36]*x13*x21*x31*x42    
        +coeff[ 37]    *x23*x33    *x52
        +coeff[ 38]    *x21        *x51
        +coeff[ 39]*x11        *x41    
        +coeff[ 40]    *x23            
        +coeff[ 41]    *x22*x31        
        +coeff[ 42]    *x21*x32        
        +coeff[ 43]        *x31    *x52
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 44]*x11*x21*x31        
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x13                
        +coeff[ 47]    *x24            
        +coeff[ 48]    *x23*x31        
        +coeff[ 49]        *x31*x43    
        +coeff[ 50]    *x23        *x51
        +coeff[ 51]        *x32*x41*x51
        +coeff[ 52]        *x31*x42*x51
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 53]    *x21*x31    *x52
        +coeff[ 54]            *x42*x52
        +coeff[ 55]    *x21        *x53
        +coeff[ 56]                *x54
        +coeff[ 57]*x11*x23            
        +coeff[ 58]*x11*x22*x31        
        +coeff[ 59]*x11*x21*x32        
        +coeff[ 60]*x11    *x33        
        +coeff[ 61]*x11*x22    *x41    
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 62]*x11*x21*x31*x41    
        +coeff[ 63]*x11*x22        *x51
        +coeff[ 64]*x11*x21    *x41*x51
        +coeff[ 65]*x11    *x31*x41*x51
        +coeff[ 66]*x11        *x41*x52
        +coeff[ 67]*x11            *x53
        +coeff[ 68]*x12*x22            
        +coeff[ 69]*x12    *x32        
        +coeff[ 70]*x12    *x31*x41    
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 71]*x12*x21        *x51
        +coeff[ 72]*x13*x21            
        +coeff[ 73]*x13    *x31        
        +coeff[ 74]    *x23*x31*x41    
        +coeff[ 75]    *x22*x31*x42    
        +coeff[ 76]    *x21*x32*x42    
        +coeff[ 77]    *x21    *x44    
        +coeff[ 78]        *x31*x44    
        +coeff[ 79]    *x23    *x41*x51
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 80]    *x22    *x42*x51
        +coeff[ 81]            *x44*x51
        +coeff[ 82]    *x22*x31    *x52
        +coeff[ 83]    *x21*x32    *x52
        +coeff[ 84]        *x32*x41*x52
        +coeff[ 85]        *x31*x42*x52
        +coeff[ 86]            *x43*x52
        +coeff[ 87]        *x32    *x53
        +coeff[ 88]    *x21        *x54
    ;
    v_l_e_qd_449                                  =v_l_e_qd_449                                  
        +coeff[ 89]*x11*x23*x31        
        ;

    return v_l_e_qd_449                                  ;
}
float x_e_qd_400                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1317045E+01;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.54091203E-03,-0.66251219E-02, 0.23131785E+00, 0.10781513E-01,
         0.55870390E-03,-0.16439975E-02,-0.55017807E-02,-0.38340446E-02,
         0.21317779E-04, 0.22021889E-05,-0.13467544E-02,-0.22978946E-02,
        -0.62203757E-03,-0.42494084E-02,-0.31059440E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_400                                  =v_x_e_qd_400                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_400                                  ;
}
float t_e_qd_400                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5778739E+00;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.50968723E-03,-0.32797451E-02, 0.74001469E-01, 0.32027415E-02,
        -0.23242973E-02,-0.19295342E-02,-0.17822997E-02,-0.81787340E-03,
         0.20323150E-03, 0.23886583E-03,-0.20713515E-03, 0.28474757E-04,
        -0.57447201E-03,-0.87112974E-03,-0.93502240E-04,-0.27063483E-03,
        -0.23971777E-02,-0.16645929E-02, 0.31466741E-03, 0.19855186E-03,
        -0.19905332E-02,-0.15814635E-02,-0.38200295E-04,-0.61223573E-05,
        -0.43639025E-05,-0.58623260E-04,-0.49037764E-04,-0.19235929E-04,
         0.26699627E-03, 0.17924397E-03,-0.35580797E-05, 0.36015688E-04,
        -0.91115379E-03,-0.92164992E-04,-0.48510851E-05, 0.42845586E-05,
        -0.31578715E-04,-0.28943641E-04, 0.54348729E-04,-0.10315955E-04,
        -0.13140283E-04, 0.20799183E-04, 0.88252535E-04, 0.14571604E-03,
         0.20227717E-04,-0.16300995E-04, 0.16595701E-04, 0.41310894E-04,
         0.18078264E-04,-0.48194641E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_400                                  =v_t_e_qd_400                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_400                                  =v_t_e_qd_400                                  
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_400                                  =v_t_e_qd_400                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]        *x32    *x52
        +coeff[ 31]    *x21        *x53
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x22*x33*x41    
        +coeff[ 34]*x12                
    ;
    v_t_e_qd_400                                  =v_t_e_qd_400                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]*x11*x21*x31        
        +coeff[ 37]*x11    *x32        
        +coeff[ 38]    *x22*x32        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x21    *x43    
        +coeff[ 41]        *x31*x43    
        +coeff[ 42]    *x23        *x51
        +coeff[ 43]    *x21*x32    *x51
    ;
    v_t_e_qd_400                                  =v_t_e_qd_400                                  
        +coeff[ 44]*x11*x21        *x52
        +coeff[ 45]    *x21*x31    *x52
        +coeff[ 46]            *x42*x52
        +coeff[ 47]*x11*x21*x33        
        +coeff[ 48]*x12*x22    *x41    
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_qd_400                                  ;
}
float y_e_qd_400                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.9363447E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.98827679E-03, 0.17959401E+00, 0.95666595E-01, 0.40619588E-02,
         0.12354103E-02,-0.12360029E-02,-0.92133740E-03,-0.14163994E-02,
        -0.96566207E-03,-0.80902530E-04,-0.24042439E-03,-0.25316427E-03,
        -0.65378024E-03,-0.15513093E-03,-0.26292000E-04,-0.84794918E-03,
        -0.73223596E-03,-0.41517560E-04,-0.19592762E-04,-0.17028740E-02,
        -0.13295643E-02,-0.35935818E-03, 0.55500900E-03,-0.99555255E-04,
         0.57700445E-03,-0.61685254E-03, 0.91967686E-05, 0.25884121E-04,
         0.47056990E-04,-0.13973961E-02, 0.26419411E-04,-0.18436825E-04,
        -0.67863752E-04, 0.13963600E-03, 0.33726683E-03, 0.21710453E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_400                                  =v_y_e_qd_400                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_400                                  =v_y_e_qd_400                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]        *x32*x43    
        +coeff[ 19]    *x22*x31*x42    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x33        
        +coeff[ 22]    *x22    *x45    
        +coeff[ 23]    *x22*x31*x44    
        +coeff[ 24]    *x24    *x43    
        +coeff[ 25]    *x24    *x45    
    ;
    v_y_e_qd_400                                  =v_y_e_qd_400                                  
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]        *x32*x41*x51
        +coeff[ 28]    *x22*x31    *x51
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]    *x22    *x44    
        +coeff[ 31]        *x32*x44    
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]        *x31*x44*x51
        +coeff[ 34]        *x32*x43*x51
    ;
    v_y_e_qd_400                                  =v_y_e_qd_400                                  
        +coeff[ 35]        *x33*x42*x51
        ;

    return v_y_e_qd_400                                  ;
}
float p_e_qd_400                                  (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.7812035E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.86164422E-04,-0.23691861E-01,-0.22626113E-01, 0.21952470E-02,
         0.34037561E-02, 0.75546303E-03, 0.72721159E-03,-0.73206733E-03,
         0.23819202E-04,-0.49165101E-03,-0.71534002E-03,-0.38058814E-03,
        -0.17226217E-03, 0.18530627E-04,-0.20587843E-03,-0.45194689E-03,
        -0.56522338E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_400                                  =v_p_e_qd_400                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_400                                  ;
}
float l_e_qd_400                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3587137E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36694077E-02, 0.41006555E-04,-0.71351472E-02,-0.15818615E-02,
        -0.51277066E-02,-0.58823940E-02, 0.72178501E-03,-0.71336754E-03,
         0.44803828E-03,-0.13792943E-03, 0.40278173E-03,-0.11827076E-03,
         0.11782897E-03,-0.18423509E-03,-0.66697146E-04,-0.16693311E-03,
         0.11136822E-03,-0.14293402E-02,-0.35966333E-03,-0.51345397E-03,
         0.52584655E-03,-0.10989656E-02,-0.12351622E-03,-0.58266922E-03,
        -0.53520023E-03,-0.70298236E-03, 0.54172444E-03, 0.72642037E-03,
         0.10254796E-02, 0.64592117E-04,-0.12664807E-02, 0.61726093E-03,
         0.17346648E-02, 0.65743795E-03,-0.10813584E-02, 0.67297326E-04,
        -0.81132492E-03,-0.20224245E-02, 0.59528224E-03, 0.12570476E-02,
        -0.94663718E-03, 0.13402354E-02, 0.27242748E-03,-0.14508347E-02,
         0.17532366E-02,-0.17779112E-02,-0.20759101E-02,-0.32720414E-02,
         0.68596035E-03, 0.59723028E-03, 0.16898576E-02, 0.71969326E-03,
         0.11704647E-03, 0.69389923E-03, 0.10294552E-02,-0.18118178E-02,
         0.36558732E-02, 0.27248347E-02, 0.65398816E-03, 0.15707477E-03,
        -0.16238957E-03,-0.15645628E-03, 0.71397648E-04,-0.12487444E-03,
        -0.69348498E-04, 0.15144570E-03, 0.84526133E-03, 0.19874236E-03,
         0.32833393E-03,-0.14123166E-03, 0.17644066E-03,-0.12042220E-03,
         0.39224987E-03,-0.27009440E-03,-0.15309524E-03, 0.14146914E-03,
         0.21511014E-03, 0.42708314E-03, 0.24401207E-03,-0.26082876E-03,
        -0.11028104E-03,-0.28212709E-03,-0.33624665E-03, 0.17995597E-03,
         0.38352772E-03,-0.10999741E-02, 0.25728508E-03, 0.18269994E-03,
         0.64600760E-03, 0.15097075E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_qd_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[  8]        *x31*x41*x51
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]*x11    *x32        
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x11            *x52
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 17]        *x31*x43    
        +coeff[ 18]            *x41*x53
        +coeff[ 19]*x11*x23            
        +coeff[ 20]*x11    *x32    *x51
        +coeff[ 21]    *x22    *x43    
        +coeff[ 22]*x13            *x51
        +coeff[ 23]    *x21*x32    *x52
        +coeff[ 24]    *x22    *x41*x52
        +coeff[ 25]*x11*x21*x32    *x51
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 26]*x11*x21    *x42*x51
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x12    *x31*x41*x51
        +coeff[ 29]        *x34*x42    
        +coeff[ 30]        *x32*x44    
        +coeff[ 31]    *x24        *x52
        +coeff[ 32]*x11*x24        *x51
        +coeff[ 33]*x11*x23*x31    *x51
        +coeff[ 34]*x12    *x32*x41*x51
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 35]*x13*x23            
        +coeff[ 36]*x13*x22*x31        
        +coeff[ 37]    *x23*x33*x41    
        +coeff[ 38]    *x21*x33*x43    
        +coeff[ 39]        *x34*x43    
        +coeff[ 40]    *x21*x32*x43*x51
        +coeff[ 41]    *x22    *x44*x51
        +coeff[ 42]        *x32*x44*x51
        +coeff[ 43]*x13*x21        *x52
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 44]    *x24    *x41*x52
        +coeff[ 45]    *x23*x31*x41*x52
        +coeff[ 46]        *x33*x41*x53
        +coeff[ 47]        *x32*x42*x53
        +coeff[ 48]*x11*x24*x31*x41    
        +coeff[ 49]*x12*x23*x31*x41    
        +coeff[ 50]*x12    *x33*x42    
        +coeff[ 51]*x12*x22    *x43    
        +coeff[ 52]*x12    *x34    *x51
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 53]*x12*x23    *x41*x51
        +coeff[ 54]*x12*x22        *x53
        +coeff[ 55]*x13*x23    *x41    
        +coeff[ 56]    *x22*x34*x41*x51
        +coeff[ 57]    *x22*x33*x42*x51
        +coeff[ 58]*x13*x21    *x41*x52
        +coeff[ 59]    *x21            
        +coeff[ 60]*x11                
        +coeff[ 61]                *x52
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 62]*x11        *x41    
        +coeff[ 63]    *x23            
        +coeff[ 64]    *x21*x32        
        +coeff[ 65]        *x33        
        +coeff[ 66]    *x21*x31*x41    
        +coeff[ 67]    *x21    *x42    
        +coeff[ 68]        *x32    *x51
        +coeff[ 69]    *x21        *x52
        +coeff[ 70]                *x53
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 71]*x11*x21*x31        
        +coeff[ 72]*x11*x21    *x41    
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x12            *x51
        +coeff[ 75]*x13                
        +coeff[ 76]    *x21*x33        
        +coeff[ 77]    *x22*x31*x41    
        +coeff[ 78]    *x22    *x42    
        +coeff[ 79]    *x21*x31*x42    
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 80]        *x33    *x51
        +coeff[ 81]        *x32*x41*x51
        +coeff[ 82]    *x21    *x42*x51
        +coeff[ 83]    *x21        *x53
        +coeff[ 84]*x11*x22*x31        
        +coeff[ 85]*x11*x22        *x51
        +coeff[ 86]*x11*x21    *x41*x51
        +coeff[ 87]*x11    *x31*x41*x51
        +coeff[ 88]*x11*x21        *x52
    ;
    v_l_e_qd_400                                  =v_l_e_qd_400                                  
        +coeff[ 89]*x11    *x31    *x52
        ;

    return v_l_e_qd_400                                  ;
}
float x_e_qd_350                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1317051E+01;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.52315142E-03,-0.66032601E-02, 0.23208660E+00, 0.10743684E-01,
         0.57536288E-03,-0.16282203E-02,-0.54730261E-02,-0.38114749E-02,
         0.18748960E-04,-0.14001813E-04,-0.13307009E-02,-0.22815480E-02,
        -0.61576196E-03,-0.42154575E-02,-0.30892363E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_350                                  =v_x_e_qd_350                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_350                                  ;
}
float t_e_qd_350                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5778914E+00;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.53639198E-03,-0.32714463E-02, 0.74307300E-01, 0.32007454E-02,
        -0.23436309E-02,-0.18695135E-02,-0.17662070E-02,-0.86527533E-03,
         0.19977140E-03, 0.24320724E-03,-0.20836761E-03,-0.19646241E-04,
        -0.54916402E-03,-0.84116351E-03,-0.82726656E-04,-0.24702543E-03,
        -0.24416775E-02,-0.16634779E-02, 0.23687839E-03, 0.13910989E-03,
         0.34025373E-03, 0.21092685E-03,-0.20997997E-02,-0.16457768E-02,
        -0.89076339E-05, 0.14863991E-05,-0.67479690E-04,-0.45294426E-04,
        -0.18425199E-04, 0.61903716E-04, 0.14207266E-05,-0.97059732E-03,
        -0.56027875E-05,-0.37358303E-04,-0.33165804E-05, 0.13217077E-05,
        -0.93262233E-05, 0.20143594E-04, 0.38015773E-04,-0.46864989E-05,
         0.87523302E-04, 0.14560930E-03, 0.29663483E-04,-0.70782371E-04,
        -0.49336755E-04, 0.15526402E-04,-0.35626090E-04,-0.46217745E-04,
        -0.50332965E-04,-0.53906886E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_qd_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_350                                  =v_t_e_qd_350                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_350                                  =v_t_e_qd_350                                  
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_350                                  =v_t_e_qd_350                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]*x11            *x52
        +coeff[ 29]    *x22*x32        
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]*x12                
        +coeff[ 33]*x11    *x32        
        +coeff[ 34]*x12        *x41    
    ;
    v_t_e_qd_350                                  =v_t_e_qd_350                                  
        +coeff[ 35]    *x22    *x41    
        +coeff[ 36]*x11*x21        *x51
        +coeff[ 37]*x13*x21            
        +coeff[ 38]*x11*x22*x31        
        +coeff[ 39]    *x21    *x43    
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]    *x21*x32    *x51
        +coeff[ 42]*x12*x22    *x41    
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_qd_350                                  =v_t_e_qd_350                                  
        +coeff[ 44]*x11*x22    *x42    
        +coeff[ 45]    *x23    *x41*x51
        +coeff[ 46]*x12*x21*x33        
        +coeff[ 47]*x11*x22*x33        
        +coeff[ 48]*x12*x21*x32*x41    
        +coeff[ 49]    *x22*x33*x41    
        ;

    return v_t_e_qd_350                                  ;
}
float y_e_qd_350                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1684413E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.16410757E-02, 0.17917080E+00, 0.95441133E-01, 0.40467838E-02,
         0.12211166E-02,-0.13598042E-02,-0.93687919E-03,-0.14186837E-02,
        -0.95097243E-03,-0.52148680E-04,-0.24002523E-03,-0.25918550E-03,
        -0.68499899E-03,-0.15423198E-03,-0.12822312E-05,-0.73679845E-03,
        -0.71857125E-03, 0.56947320E-05, 0.15695136E-03, 0.10896679E-03,
        -0.18844358E-03,-0.16876670E-02,-0.12622421E-02,-0.35148099E-03,
        -0.11583609E-03, 0.61481565E-04, 0.42459964E-04,-0.58028389E-04,
         0.59000293E-04, 0.28020368E-04,-0.81439663E-04, 0.25427402E-04,
         0.31224507E-04,-0.86541130E-03,-0.82059742E-04,-0.24647579E-04,
         0.16937703E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_350                                  =v_y_e_qd_350                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_350                                  =v_y_e_qd_350                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]        *x31*x42*x51
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]        *x32*x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]            *x45*x51
    ;
    v_y_e_qd_350                                  =v_y_e_qd_350                                  
        +coeff[ 26]    *x22    *x45    
        +coeff[ 27]    *x22*x31*x44    
        +coeff[ 28]    *x24*x31    *x51
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24    *x45    
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]*x11*x23    *x41    
    ;
    v_y_e_qd_350                                  =v_y_e_qd_350                                  
        +coeff[ 35]*x12    *x31*x42    
        +coeff[ 36]        *x32*x45    
        ;

    return v_y_e_qd_350                                  ;
}
float p_e_qd_350                                  (float *x,int m){
    int ncoeff= 17;
    float avdat= -0.2661125E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
         0.25605419E-03,-0.23699930E-01,-0.22619037E-01, 0.21919787E-02,
         0.33970515E-02, 0.75776112E-03, 0.72768400E-03,-0.73369074E-03,
         0.16420809E-04,-0.48669841E-03,-0.70908549E-03,-0.37865370E-03,
        -0.16890590E-03, 0.10339614E-04,-0.20186808E-03,-0.43965020E-03,
        -0.54806180E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_350                                  =v_p_e_qd_350                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_350                                  ;
}
float l_e_qd_350                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3629409E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35824352E-02,-0.61033792E-04,-0.68682092E-02,-0.15670159E-02,
        -0.59788250E-02,-0.59760357E-02, 0.42288157E-03, 0.47517122E-04,
        -0.12873109E-03,-0.10315154E-03, 0.98823151E-03, 0.21751225E-03,
         0.13507374E-02, 0.91267540E-03, 0.78995159E-03,-0.59616385E-04,
         0.63696316E-04, 0.76444465E-03, 0.81396039E-03,-0.79515058E-03,
        -0.81359240E-03, 0.11607229E-02, 0.23619814E-02, 0.24408887E-02,
         0.17743239E-02, 0.96378557E-03,-0.22450732E-02, 0.14567794E-02,
         0.69841172E-03, 0.16830991E-02,-0.16313902E-02, 0.20694421E-02,
        -0.10877994E-02,-0.18082500E-02, 0.11284689E-02,-0.43242293E-04,
        -0.98311953E-04,-0.27950926E-04, 0.12799984E-03,-0.82904613E-03,
         0.30165471E-03,-0.43893035E-03, 0.14318299E-03,-0.27714725E-03,
        -0.43155623E-03,-0.23973966E-03,-0.49839946E-04,-0.10691576E-03,
        -0.48814845E-03, 0.51435485E-03,-0.18456082E-03,-0.42354121E-03,
         0.92334005E-04,-0.27111254E-03, 0.21415901E-03, 0.27584229E-03,
         0.45153397E-03,-0.32757726E-03, 0.40926944E-03,-0.15415321E-02,
         0.35301424E-03,-0.21087780E-03,-0.60783647E-03, 0.11025462E-03,
        -0.72289590E-03, 0.87211787E-03,-0.24490955E-03, 0.33728304E-03,
         0.97235708E-04, 0.65678229E-04,-0.47861211E-03, 0.90245024E-03,
        -0.27935204E-03, 0.70152187E-03, 0.42305145E-03, 0.32528798E-03,
         0.11297010E-03, 0.25890616E-03,-0.30948248E-03, 0.47682412E-03,
         0.59753604E-03, 0.71142451E-03, 0.72680204E-03,-0.37652644E-03,
         0.75069116E-03, 0.43835049E-03,-0.22949312E-03,-0.15910565E-03,
         0.69143728E-03,-0.65031572E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[  8]    *x21*x31        
        +coeff[  9]    *x21*x32        
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]*x13                
        +coeff[ 12]    *x22*x31*x41    
        +coeff[ 13]    *x22    *x42    
        +coeff[ 14]*x12    *x31*x41    
        +coeff[ 15]        *x33*x42    
        +coeff[ 16]        *x34    *x51
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 17]        *x32*x42*x51
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]*x11*x21*x33        
        +coeff[ 20]*x12    *x31    *x52
        +coeff[ 21]*x12*x22*x32        
        +coeff[ 22]    *x24*x31    *x52
        +coeff[ 23]        *x33*x42*x52
        +coeff[ 24]        *x32*x43*x52
        +coeff[ 25]*x11*x24*x31*x41    
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 26]*x11*x24    *x41*x51
        +coeff[ 27]*x11*x22    *x43*x51
        +coeff[ 28]*x11*x21*x33    *x52
        +coeff[ 29]*x12*x22*x31*x42    
        +coeff[ 30]*x13    *x31*x41*x52
        +coeff[ 31]    *x23    *x43*x52
        +coeff[ 32]    *x23    *x41*x54
        +coeff[ 33]    *x21*x32*x41*x54
        +coeff[ 34]            *x44*x54
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 35]*x11                
        +coeff[ 36]                *x52
        +coeff[ 37]    *x22*x31        
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11    *x31    *x51
        +coeff[ 40]*x11        *x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]    *x23*x31        
        +coeff[ 43]    *x21*x31    *x52
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 44]            *x42*x52
        +coeff[ 45]        *x31    *x53
        +coeff[ 46]            *x41*x53
        +coeff[ 47]*x11*x22        *x51
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]*x11*x21    *x41*x51
        +coeff[ 50]*x11*x21        *x52
        +coeff[ 51]*x12*x22            
        +coeff[ 52]*x12        *x42    
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 53]*x12        *x41*x51
        +coeff[ 54]*x12            *x52
        +coeff[ 55]    *x24    *x41    
        +coeff[ 56]    *x22*x32*x41    
        +coeff[ 57]    *x22    *x43    
        +coeff[ 58]    *x22*x31*x41*x51
        +coeff[ 59]    *x22*x31    *x52
        +coeff[ 60]        *x33    *x52
        +coeff[ 61]    *x21    *x42*x52
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 62]    *x22        *x53
        +coeff[ 63]    *x21*x31    *x53
        +coeff[ 64]        *x31*x41*x53
        +coeff[ 65]*x11*x21*x32*x41    
        +coeff[ 66]*x11*x21    *x43    
        +coeff[ 67]*x11*x23        *x51
        +coeff[ 68]*x11*x21*x31*x41*x51
        +coeff[ 69]*x11    *x32*x41*x51
        +coeff[ 70]*x11*x21    *x42*x51
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 71]*x11    *x31*x42*x51
        +coeff[ 72]*x11*x21    *x41*x52
        +coeff[ 73]*x11    *x31*x41*x52
        +coeff[ 74]*x11    *x31    *x53
        +coeff[ 75]*x11            *x54
        +coeff[ 76]*x12            *x53
        +coeff[ 77]*x13*x21*x31        
        +coeff[ 78]        *x34*x42    
        +coeff[ 79]*x13    *x31    *x51
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 80]    *x24    *x41*x51
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]        *x32*x43*x51
        +coeff[ 83]    *x22*x32    *x52
        +coeff[ 84]        *x31*x42*x53
        +coeff[ 85]    *x21    *x41*x54
        +coeff[ 86]*x11*x23*x32        
        +coeff[ 87]*x11    *x31*x44    
        +coeff[ 88]*x11*x21*x33    *x51
    ;
    v_l_e_qd_350                                  =v_l_e_qd_350                                  
        +coeff[ 89]*x11*x23    *x41*x51
        ;

    return v_l_e_qd_350                                  ;
}
float x_e_qd_300                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314984E+01;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.15929451E-02,-0.65677469E-02, 0.23308681E+00, 0.10693060E-01,
         0.56038250E-03,-0.16055183E-02,-0.54687457E-02,-0.37664252E-02,
        -0.63575644E-05, 0.40437295E-04,-0.13485218E-02,-0.22628168E-02,
        -0.63160056E-03,-0.41649486E-02,-0.31665990E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_300                                  =v_x_e_qd_300                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_300                                  ;
}
float t_e_qd_300                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5785667E+00;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.12234956E-02,-0.32649480E-02, 0.74719228E-01, 0.32049848E-02,
        -0.23595598E-02,-0.18977458E-02,-0.17495657E-02,-0.86007343E-03,
         0.20667711E-03, 0.23912120E-03,-0.19744967E-03,-0.12148767E-04,
        -0.54023258E-03,-0.83948229E-03,-0.25176202E-03,-0.24399424E-02,
        -0.17196120E-02,-0.71089918E-04, 0.21401054E-03, 0.12552674E-03,
         0.31662587E-03, 0.19884750E-03,-0.92749024E-03,-0.15877550E-02,
         0.16691479E-06, 0.13068029E-05,-0.61012510E-04,-0.38606129E-04,
         0.78800127E-04,-0.85458978E-05,-0.74193895E-05,-0.20334856E-02,
        -0.69037042E-05,-0.31209351E-04, 0.11980957E-04,-0.94860270E-05,
         0.12250047E-03,-0.74235184E-04,-0.53680491E-04,-0.21205820E-04,
         0.17395236E-04, 0.29121056E-04, 0.88847555E-04,-0.30428538E-06,
         0.39508127E-05,-0.67840069E-05, 0.36235801E-05,-0.69530079E-05,
        -0.41894568E-05,-0.11345505E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_300                                  =v_t_e_qd_300                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]    *x23*x31*x41    
        +coeff[ 16]    *x23    *x42    
    ;
    v_t_e_qd_300                                  =v_t_e_qd_300                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_300                                  =v_t_e_qd_300                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]    *x21*x32*x42    
        +coeff[ 32]*x12                
        +coeff[ 33]*x11    *x32        
        +coeff[ 34]    *x22    *x41    
    ;
    v_t_e_qd_300                                  =v_t_e_qd_300                                  
        +coeff[ 35]*x11            *x52
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]*x11*x22*x31*x41    
        +coeff[ 38]*x11*x22    *x42    
        +coeff[ 39]    *x22        *x53
        +coeff[ 40]    *x23    *x42*x51
        +coeff[ 41]*x13*x21        *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]        *x31*x41    
    ;
    v_t_e_qd_300                                  =v_t_e_qd_300                                  
        +coeff[ 44]                *x52
        +coeff[ 45]*x12*x21            
        +coeff[ 46]*x12            *x51
        +coeff[ 47]*x11*x21        *x51
        +coeff[ 48]            *x41*x52
        +coeff[ 49]*x12    *x31*x41    
        ;

    return v_t_e_qd_300                                  ;
}
float y_e_qd_300                                  (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.8020608E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.79682958E-03, 0.17905858E+00, 0.95173068E-01, 0.40519680E-02,
         0.12398246E-02,-0.15206257E-02,-0.91744092E-03,-0.14113686E-02,
         0.76996555E-04, 0.15381078E-04,-0.99423912E-03,-0.23110572E-03,
        -0.25276493E-03,-0.81456138E-03,-0.14915035E-03,-0.30408824E-04,
        -0.72740624E-03,-0.60679839E-03,-0.43419522E-03,-0.63873384E-04,
        -0.30720963E-04,-0.14925294E-02,-0.61706523E-03,-0.10764877E-02,
        -0.36823563E-03,-0.91903022E-03,-0.59431465E-03, 0.64820895E-03,
         0.11067232E-04, 0.14228695E-03, 0.95606913E-04, 0.47797766E-04,
         0.35137255E-04,-0.66121553E-04, 0.51553230E-04, 0.61866049E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_300                                  =v_y_e_qd_300                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_300                                  =v_y_e_qd_300                                  
        +coeff[ 17]    *x24    *x43    
        +coeff[ 18]    *x22*x32*x43    
        +coeff[ 19]    *x24*x32*x41    
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x24    *x41    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]    *x22    *x45    
    ;
    v_y_e_qd_300                                  =v_y_e_qd_300                                  
        +coeff[ 26]    *x22*x31*x44    
        +coeff[ 27]    *x24    *x45    
        +coeff[ 28]    *x21*x31*x41    
        +coeff[ 29]        *x31*x42*x51
        +coeff[ 30]        *x32*x41*x51
        +coeff[ 31]    *x22*x31    *x51
        +coeff[ 32]*x11*x21*x31*x42    
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]            *x45*x51
    ;
    v_y_e_qd_300                                  =v_y_e_qd_300                                  
        +coeff[ 35]    *x22    *x43*x51
        ;

    return v_y_e_qd_300                                  ;
}
float p_e_qd_300                                  (float *x,int m){
    int ncoeff= 17;
    float avdat= -0.7311079E-04;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
         0.70836883E-04,-0.23718467E-01,-0.22659432E-01, 0.21900956E-02,
         0.33994671E-02, 0.76167396E-03, 0.73397352E-03,-0.74146176E-03,
         0.28800785E-04,-0.50064974E-03,-0.70776889E-03,-0.38143882E-03,
        -0.16694028E-03, 0.24008164E-04,-0.20215317E-03,-0.44887574E-03,
        -0.54532651E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_300                                  =v_p_e_qd_300                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_300                                  ;
}
float l_e_qd_300                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3613803E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35707585E-02,-0.67739561E-02,-0.17254332E-02,-0.48000352E-02,
        -0.56685884E-02, 0.77367999E-03,-0.86662988E-03,-0.11879444E-03,
        -0.14431857E-02,-0.53473606E-04, 0.32767837E-03,-0.22441606E-03,
        -0.41926018E-04, 0.10898202E-02, 0.11628998E-02, 0.76203136E-03,
        -0.70396141E-03,-0.12425005E-02, 0.39872766E-03,-0.92301017E-03,
        -0.45580007E-03,-0.25839591E-03, 0.17008076E-02,-0.11406995E-02,
         0.79235819E-03, 0.25924055E-02, 0.37695639E-03, 0.69637643E-03,
        -0.12377509E-02,-0.75218943E-03, 0.53076254E-03, 0.16312759E-02,
        -0.15500360E-02,-0.86048548E-03, 0.46837403E-03,-0.63913071E-03,
         0.47435926E-03,-0.26481831E-03, 0.13768604E-02, 0.16797945E-02,
        -0.16296143E-02,-0.26588074E-02, 0.12158111E-02,-0.13447551E-03,
         0.10021513E-03,-0.66633538E-05,-0.34238031E-03,-0.15508219E-03,
         0.23701544E-03,-0.53766568E-03, 0.86770282E-03, 0.10451972E-03,
         0.77165678E-04, 0.25590265E-03, 0.19100899E-03,-0.51872735E-03,
         0.22991146E-03,-0.22180978E-03,-0.58170158E-03,-0.21245040E-03,
        -0.20786455E-03,-0.17659509E-03,-0.25538859E-03, 0.63135209E-04,
         0.67253964E-03,-0.19669427E-03, 0.40593225E-03,-0.73824784E-04,
         0.18439656E-03,-0.16861298E-03,-0.16043927E-03, 0.20984669E-03,
         0.28307442E-03,-0.48253301E-03,-0.28823066E-03,-0.38259744E-03,
        -0.22994036E-03,-0.49278868E-03, 0.50463818E-03, 0.11191943E-02,
         0.46846687E-03, 0.17536187E-03, 0.15968787E-03,-0.34869241E-03,
         0.35630233E-03, 0.15983869E-03,-0.84432214E-03, 0.60330029E-03,
        -0.16862857E-02, 0.19025577E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[  8]    *x21*x33*x42    
        +coeff[  9]    *x21            
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]            *x42*x51
        +coeff[ 14]*x11    *x31    *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x11            *x52
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]        *x31    *x53
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]*x13*x21            
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x21*x31*x42*x51
        +coeff[ 23]            *x44*x51
        +coeff[ 24]        *x32    *x53
        +coeff[ 25]*x11*x21*x31*x41*x51
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 26]*x11*x21    *x42*x51
        +coeff[ 27]*x11            *x54
        +coeff[ 28]*x12*x21*x31*x41    
        +coeff[ 29]*x12*x21    *x42    
        +coeff[ 30]*x12*x21    *x41*x51
        +coeff[ 31]    *x24*x31*x41    
        +coeff[ 32]*x13    *x31    *x51
        +coeff[ 33]*x13        *x41*x51
        +coeff[ 34]*x13            *x52
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 35]    *x21    *x43*x52
        +coeff[ 36]    *x22    *x41*x53
        +coeff[ 37]*x12*x24            
        +coeff[ 38]*x12*x22*x31*x41    
        +coeff[ 39]    *x23*x33*x41    
        +coeff[ 40]    *x23*x31*x42*x51
        +coeff[ 41]*x11*x21*x31*x43*x51
        +coeff[ 42]*x11*x21    *x43*x52
        +coeff[ 43]        *x31    *x51
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 44]*x11    *x31        
        +coeff[ 45]    *x22    *x41    
        +coeff[ 46]    *x21*x31*x41    
        +coeff[ 47]        *x31*x42    
        +coeff[ 48]*x11*x21*x31        
        +coeff[ 49]*x11*x21    *x41    
        +coeff[ 50]*x11    *x31*x41    
        +coeff[ 51]*x11        *x42    
        +coeff[ 52]*x12        *x41    
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 53]        *x34        
        +coeff[ 54]    *x23    *x41    
        +coeff[ 55]    *x21*x32*x41    
        +coeff[ 56]            *x43*x51
        +coeff[ 57]    *x22        *x52
        +coeff[ 58]        *x31*x41*x52
        +coeff[ 59]            *x42*x52
        +coeff[ 60]            *x41*x53
        +coeff[ 61]*x11*x22*x31        
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 62]*x11*x21*x32        
        +coeff[ 63]*x11    *x33        
        +coeff[ 64]*x11    *x32*x41    
        +coeff[ 65]*x11*x21    *x42    
        +coeff[ 66]*x11    *x31*x42    
        +coeff[ 67]*x11        *x43    
        +coeff[ 68]*x11*x21        *x52
        +coeff[ 69]*x11        *x41*x52
        +coeff[ 70]*x12*x21*x31        
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 71]*x12*x21        *x51
        +coeff[ 72]    *x21*x34        
        +coeff[ 73]    *x22    *x43    
        +coeff[ 74]        *x34    *x51
        +coeff[ 75]    *x22*x31    *x52
        +coeff[ 76]    *x21*x32    *x52
        +coeff[ 77]    *x21    *x42*x52
        +coeff[ 78]    *x22        *x53
        +coeff[ 79]        *x31*x41*x53
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 80]            *x42*x53
        +coeff[ 81]    *x21        *x54
        +coeff[ 82]        *x31    *x54
        +coeff[ 83]*x11*x21*x33        
        +coeff[ 84]*x11    *x34        
        +coeff[ 85]*x11    *x33*x41    
        +coeff[ 86]*x11    *x32*x42    
        +coeff[ 87]*x11*x21    *x43    
        +coeff[ 88]*x11    *x31*x43    
    ;
    v_l_e_qd_300                                  =v_l_e_qd_300                                  
        +coeff[ 89]*x11*x23        *x51
        ;

    return v_l_e_qd_300                                  ;
}
float x_e_qd_250                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314006E+01;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.25768864E-02,-0.65252385E-02, 0.23450966E+00, 0.10630539E-01,
         0.56422583E-03,-0.16093616E-02,-0.54077585E-02,-0.37793473E-02,
         0.59411810E-04, 0.71681666E-06,-0.13701086E-02,-0.22619388E-02,
        -0.62668207E-03,-0.42415829E-02,-0.30780819E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_250                                  =v_x_e_qd_250                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_250                                  ;
}
float t_e_qd_250                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5788985E+00;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15619241E-02,-0.32495211E-02, 0.75293668E-01, 0.31493509E-02,
        -0.24166722E-02,-0.19119107E-02,-0.17517277E-02,-0.86685951E-03,
         0.21122667E-03, 0.24016429E-03,-0.20755446E-03,-0.30658055E-04,
        -0.83412207E-03,-0.24204228E-03,-0.16719522E-02,-0.53334580E-03,
        -0.24334304E-02,-0.59913553E-04, 0.24925175E-03, 0.16649494E-03,
         0.32653240E-03, 0.23281905E-03,-0.19791825E-02,-0.15330771E-02,
         0.15052461E-04,-0.60976145E-05,-0.41854972E-04,-0.25389365E-04,
         0.10759981E-03, 0.13666762E-03, 0.50591593E-05,-0.89193945E-03,
         0.31057833E-04,-0.76294318E-05,-0.67111068E-05,-0.83643761E-06,
         0.83819741E-05, 0.17119370E-04,-0.29182058E-04, 0.73803658E-05,
         0.54062271E-04,-0.37271951E-04, 0.17438661E-04,-0.18284134E-03,
        -0.14243429E-03,-0.12605029E-04,-0.47044482E-05,-0.37665407E-05,
         0.47318895E-05,-0.58529413E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_250                                  =v_t_e_qd_250                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_250                                  =v_t_e_qd_250                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x21*x31*x41*x51
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x21*x31*x43    
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]        *x32        
    ;
    v_t_e_qd_250                                  =v_t_e_qd_250                                  
        +coeff[ 26]*x11    *x31*x41    
        +coeff[ 27]*x11        *x42    
        +coeff[ 28]    *x22*x32        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x13    *x32        
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]    *x23        *x53
        +coeff[ 33]*x12                
        +coeff[ 34]*x11        *x41    
    ;
    v_t_e_qd_250                                  =v_t_e_qd_250                                  
        +coeff[ 35]*x13                
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11*x21*x31        
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]    *x21*x31    *x51
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x11*x22*x32        
        +coeff[ 42]*x11*x23    *x41    
        +coeff[ 43]*x11*x22*x31*x41    
    ;
    v_t_e_qd_250                                  =v_t_e_qd_250                                  
        +coeff[ 44]*x11*x22    *x42    
        +coeff[ 45]*x11*x23        *x51
        +coeff[ 46]        *x31*x41    
        +coeff[ 47]    *x22    *x41    
        +coeff[ 48]*x12            *x51
        +coeff[ 49]*x11            *x52
        ;

    return v_t_e_qd_250                                  ;
}
float y_e_qd_250                                  (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.7256052E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.71053806E-03, 0.17856301E+00, 0.94809748E-01, 0.40437686E-02,
         0.12345993E-02,-0.14255206E-02,-0.93334576E-03,-0.14201649E-02,
        -0.97929698E-03, 0.72973353E-05,-0.76269725E-03,-0.23608992E-03,
        -0.25918245E-03,-0.15527138E-03,-0.37622969E-04,-0.69685542E-03,
        -0.71083312E-03, 0.10194270E-04,-0.17315926E-04, 0.16103979E-03,
         0.11187982E-03,-0.15207990E-02,-0.11718443E-02,-0.36822184E-03,
        -0.28022059E-03,-0.55178895E-03, 0.58490532E-04,-0.11094817E-03,
        -0.37307682E-03,-0.10648115E-04, 0.51632342E-04, 0.14995643E-04,
         0.32735796E-04, 0.30147019E-04,-0.53978898E-03, 0.32491815E-04,
        -0.49203507E-04,-0.22640566E-04,-0.31027135E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_250                                  =v_y_e_qd_250                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_250                                  =v_y_e_qd_250                                  
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]    *x22    *x45    
        +coeff[ 25]    *x22*x31*x44    
    ;
    v_y_e_qd_250                                  =v_y_e_qd_250                                  
        +coeff[ 26]    *x24*x31    *x51
        +coeff[ 27]    *x24    *x43    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]        *x31*x41*x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]*x11*x21    *x42    
        +coeff[ 32]    *x22    *x41*x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_qd_250                                  =v_y_e_qd_250                                  
        +coeff[ 35]*x11*x21*x31*x42    
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]*x12*x22    *x41    
        +coeff[ 38]*x11*x21    *x41*x52
        ;

    return v_y_e_qd_250                                  ;
}
float p_e_qd_250                                  (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.1517357E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.14920627E-03,-0.23750039E-01,-0.22678318E-01, 0.21866462E-02,
         0.33957746E-02, 0.77073375E-03, 0.74197940E-03,-0.76617231E-03,
        -0.47344848E-03,-0.70033252E-03,-0.37607565E-03,-0.16776420E-03,
         0.75742617E-04, 0.17786910E-04,-0.19885591E-03,-0.46452071E-03,
        -0.56206671E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_250                                  =v_p_e_qd_250                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]    *x22*x32*x41    
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_250                                  ;
}
float l_e_qd_250                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3601427E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.34845918E-02,-0.69493745E-02,-0.17864993E-02,-0.52777291E-02,
        -0.55642175E-02, 0.91093250E-04,-0.56392595E-03, 0.36356234E-03,
         0.66798157E-03,-0.47747933E-03,-0.12299110E-05,-0.31594216E-03,
        -0.18594183E-04, 0.48242815E-03, 0.56691194E-03, 0.56767813E-03,
        -0.60253532E-03, 0.60815399E-03, 0.31625785E-03, 0.20214540E-03,
        -0.41724436E-03,-0.31205412E-03,-0.58935536E-03, 0.12482075E-02,
         0.29079437E-02,-0.56926173E-03,-0.30231316E-03, 0.84473280E-03,
        -0.10884576E-02, 0.27780666E-03,-0.16368994E-02,-0.14794652E-02,
         0.44602348E-03, 0.64773415E-03,-0.92910806E-03,-0.12214201E-02,
         0.18568923E-03, 0.17988884E-02, 0.74714335E-03,-0.40681022E-02,
         0.21321997E-02,-0.30205976E-04, 0.12894860E-03, 0.26195394E-03,
        -0.65357533E-04, 0.12910405E-03,-0.50382674E-04, 0.29177932E-03,
         0.10657287E-03,-0.12360027E-03,-0.35380822E-03,-0.23834647E-03,
        -0.32662565E-03,-0.16934227E-03, 0.22725313E-03, 0.97020098E-03,
         0.33730399E-03,-0.29581558E-03, 0.10353349E-02, 0.54423354E-03,
         0.45455626E-03, 0.51234767E-03, 0.10561313E-02,-0.23587246E-03,
        -0.17871433E-03, 0.27681008E-03, 0.18538361E-03, 0.42479331E-03,
         0.35108806E-03, 0.43821012E-03, 0.29883982E-03,-0.44694662E-03,
         0.50478766E-03, 0.10087579E-02, 0.62815915E-03,-0.19053167E-03,
         0.69346494E-03,-0.52851200E-03,-0.37895094E-03,-0.48149974E-03,
         0.36906044E-03,-0.34461234E-03,-0.41927188E-03,-0.94988209E-03,
         0.10684945E-02, 0.45515926E-03, 0.51248953E-03,-0.42684138E-03,
         0.20049578E-02,-0.13092156E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]        *x32    *x51
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[  8]            *x42*x51
        +coeff[  9]*x11*x21    *x41*x52
        +coeff[ 10]        *x31    *x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]        *x31*x41*x51
        +coeff[ 14]        *x31*x42*x51
        +coeff[ 15]*x11    *x32*x41    
        +coeff[ 16]*x11        *x43    
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 17]*x11*x21        *x52
        +coeff[ 18]*x11            *x53
        +coeff[ 19]*x12*x22            
        +coeff[ 20]*x12            *x52
        +coeff[ 21]    *x21*x33    *x51
        +coeff[ 22]    *x23        *x52
        +coeff[ 23]*x11*x22    *x41*x51
        +coeff[ 24]*x11*x21    *x42*x51
        +coeff[ 25]*x11    *x32    *x52
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 26]*x12            *x53
        +coeff[ 27]    *x22*x33*x41    
        +coeff[ 28]    *x21*x31*x42*x52
        +coeff[ 29]            *x44*x52
        +coeff[ 30]        *x32    *x54
        +coeff[ 31]*x11    *x31*x44    
        +coeff[ 32]*x12*x22*x32        
        +coeff[ 33]*x12*x21    *x42*x51
        +coeff[ 34]    *x22*x33    *x52
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 35]*x11*x23*x32    *x51
        +coeff[ 36]*x11*x23        *x53
        +coeff[ 37]*x12    *x31    *x54
        +coeff[ 38]    *x23*x31*x44    
        +coeff[ 39]*x13*x21    *x42*x51
        +coeff[ 40]*x13*x21        *x53
        +coeff[ 41]            *x41    
        +coeff[ 42]*x11                
        +coeff[ 43]                *x52
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 44]*x11            *x51
        +coeff[ 45]    *x23            
        +coeff[ 46]    *x22    *x41    
        +coeff[ 47]            *x43    
        +coeff[ 48]    *x21    *x41*x51
        +coeff[ 49]*x11*x21    *x41    
        +coeff[ 50]*x11*x21        *x51
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]*x12*x21            
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 53]*x13                
        +coeff[ 54]        *x34        
        +coeff[ 55]    *x21*x31*x42    
        +coeff[ 56]        *x32*x41*x51
        +coeff[ 57]    *x22        *x52
        +coeff[ 58]        *x32    *x52
        +coeff[ 59]*x11*x22*x31        
        +coeff[ 60]*x11    *x33        
        +coeff[ 61]*x11*x22    *x41    
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 62]*x11    *x31*x42    
        +coeff[ 63]*x11*x22        *x51
        +coeff[ 64]*x11        *x41*x52
        +coeff[ 65]*x12    *x32        
        +coeff[ 66]*x13*x21            
        +coeff[ 67]    *x21*x33*x41    
        +coeff[ 68]    *x22*x31*x42    
        +coeff[ 69]    *x21*x32*x42    
        +coeff[ 70]    *x23    *x41*x51
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 71]    *x21    *x43*x51
        +coeff[ 72]    *x21*x32    *x52
        +coeff[ 73]    *x21*x31*x41*x52
        +coeff[ 74]    *x21    *x42*x52
        +coeff[ 75]*x11    *x33*x41    
        +coeff[ 76]*x11*x22*x31    *x51
        +coeff[ 77]*x11    *x33    *x51
        +coeff[ 78]*x11    *x32*x41*x51
        +coeff[ 79]*x11*x21        *x53
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 80]*x12*x23            
        +coeff[ 81]*x12        *x43    
        +coeff[ 82]*x12*x21    *x41*x51
        +coeff[ 83]*x12    *x31    *x52
        +coeff[ 84]    *x23*x32*x41    
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x31    *x52
        +coeff[ 87]    *x21*x33    *x52
        +coeff[ 88]    *x21    *x43*x52
    ;
    v_l_e_qd_250                                  =v_l_e_qd_250                                  
        +coeff[ 89]    *x21    *x41*x54
        ;

    return v_l_e_qd_250                                  ;
}
float x_e_qd_200                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1316201E+01;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.39456657E-03,-0.64646578E-02, 0.23660658E+00, 0.10534510E-01,
         0.55737962E-03,-0.16198979E-02,-0.54022316E-02,-0.37897264E-02,
         0.11043733E-04,-0.41691041E-04,-0.13101684E-02,-0.22504167E-02,
        -0.60467538E-03,-0.42530233E-02,-0.30798537E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_200                                  =v_x_e_qd_200                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_200                                  ;
}
float t_e_qd_200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5782191E+00;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.88394404E-03,-0.32215684E-02, 0.76171443E-01, 0.31237779E-02,
        -0.24700044E-02,-0.18970123E-02,-0.17786856E-02,-0.84125099E-03,
         0.21908658E-03, 0.23678558E-03,-0.22082949E-03, 0.93355908E-07,
        -0.54931623E-03,-0.83751755E-03,-0.75399883E-04,-0.25209723E-03,
        -0.24148785E-02,-0.16433750E-02, 0.31061316E-03, 0.19381753E-03,
        -0.89063711E-03,-0.15811964E-02,-0.19852760E-04, 0.26603770E-05,
         0.41042376E-05,-0.69446600E-04,-0.35376983E-04,-0.15202584E-04,
         0.21480494E-03, 0.14687101E-03, 0.13970920E-03, 0.87523204E-05,
         0.49209084E-05,-0.19945987E-02,-0.16544268E-05,-0.40077424E-06,
        -0.32281176E-04, 0.12370399E-04, 0.67884241E-04,-0.15350775E-04,
         0.63800842E-04,-0.37947677E-04, 0.26568203E-04,-0.27813010E-04,
        -0.65434047E-05, 0.35024509E-05,-0.51020211E-05,-0.11647009E-04,
         0.12004401E-04, 0.11142271E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_200                                  =v_t_e_qd_200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_200                                  =v_t_e_qd_200                                  
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_200                                  =v_t_e_qd_200                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x22    *x42    
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]        *x32    *x52
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31        
    ;
    v_t_e_qd_200                                  =v_t_e_qd_200                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]*x11    *x32        
        +coeff[ 37]*x11*x21    *x41    
        +coeff[ 38]    *x22*x32        
        +coeff[ 39]*x12*x21        *x51
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x11*x22    *x42    
        +coeff[ 42]    *x22        *x53
        +coeff[ 43]*x11*x22*x31    *x52
    ;
    v_t_e_qd_200                                  =v_t_e_qd_200                                  
        +coeff[ 44]*x12                
        +coeff[ 45]                *x52
        +coeff[ 46]*x11*x21        *x51
        +coeff[ 47]*x13*x21            
        +coeff[ 48]*x12*x22            
        +coeff[ 49]    *x23*x31        
        ;

    return v_t_e_qd_200                                  ;
}
float y_e_qd_200                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1176182E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.33918250E-05, 0.17798933E+00, 0.94222508E-01, 0.40538851E-02,
         0.12356488E-02,-0.13700612E-02,-0.93427917E-03,-0.14221399E-02,
        -0.36134127E-05,-0.21853872E-04,-0.95619907E-03,-0.23105359E-03,
        -0.24979154E-03,-0.73020253E-03,-0.15355900E-03,-0.78808671E-05,
        -0.73271233E-03,-0.72103657E-03, 0.38528610E-05, 0.17151126E-03,
         0.12366998E-03, 0.55991346E-04,-0.15479613E-02,-0.12828669E-02,
        -0.35787452E-03,-0.10351695E-03, 0.69207432E-04,-0.48573489E-04,
        -0.25427638E-03,-0.49891255E-05,-0.34337849E-04,-0.33231831E-05,
         0.37676498E-04, 0.33001921E-04,-0.82376064E-03, 0.18653694E-04,
         0.19080622E-04,-0.71269700E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_200                                  =v_y_e_qd_200                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_200                                  =v_y_e_qd_200                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x32*x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_qd_200                                  =v_y_e_qd_200                                  
        +coeff[ 26]            *x45*x51
        +coeff[ 27]    *x22    *x45    
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24    *x45    
        +coeff[ 31]                *x51
        +coeff[ 32]    *x22    *x41*x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_qd_200                                  =v_y_e_qd_200                                  
        +coeff[ 35]*x11    *x31*x43    
        +coeff[ 36]*x12        *x41*x51
        +coeff[ 37]*x11*x23    *x41    
        ;

    return v_y_e_qd_200                                  ;
}
float p_e_qd_200                                  (float *x,int m){
    int ncoeff= 17;
    float avdat= -0.4695695E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
         0.64629072E-04,-0.23775436E-01,-0.22737492E-01, 0.21814439E-02,
         0.33905085E-02, 0.77963911E-03, 0.75187266E-03,-0.76087937E-03,
         0.18366321E-04,-0.50980598E-03,-0.70170534E-03,-0.37744752E-03,
        -0.17140899E-03, 0.17206557E-04,-0.19864998E-03,-0.44144434E-03,
        -0.58506306E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_200                                  =v_p_e_qd_200                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_200                                  ;
}
float l_e_qd_200                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3705729E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36135286E-02,-0.71006594E-02,-0.12716178E-02,-0.50058230E-02,
        -0.56292242E-02, 0.31081971E-03,-0.28506987E-03, 0.15144849E-03,
        -0.30552990E-04,-0.34133158E-04,-0.44626828E-04,-0.11822591E-03,
         0.11343708E-03,-0.20168220E-03, 0.54900692E-03, 0.15872413E-03,
        -0.65902382E-03,-0.25580608E-03,-0.74037799E-03,-0.10436143E-02,
        -0.38951170E-03,-0.14025784E-02,-0.52861102E-04,-0.77656598E-03,
         0.17785863E-02, 0.44228151E-03, 0.68474456E-03, 0.17301070E-02,
        -0.82150829E-03, 0.11801731E-02, 0.51394955E-03, 0.29773829E-02,
         0.29073863E-02, 0.77043258E-03, 0.23350087E-02,-0.13039896E-02,
        -0.17080024E-03,-0.22528680E-03, 0.93520626E-04, 0.30010374E-03,
         0.13269235E-03, 0.37015582E-03, 0.35726969E-03, 0.16485570E-04,
         0.37014385E-03, 0.54012070E-03,-0.98257824E-05,-0.15272862E-03,
         0.16856182E-03,-0.26845033E-03, 0.43121551E-03, 0.21943811E-03,
        -0.17110561E-03,-0.25483978E-03, 0.17155180E-03,-0.58721652E-03,
         0.94144641E-04, 0.19942786E-03,-0.86669013E-03,-0.16868333E-03,
        -0.71836537E-03,-0.21378482E-03, 0.15484792E-03,-0.54131181E-03,
        -0.19701534E-03,-0.18226619E-03,-0.32515646E-03,-0.57202729E-03,
         0.13581583E-03, 0.50157320E-03, 0.17107533E-03,-0.54051797E-03,
        -0.14507168E-02, 0.62559260E-03,-0.70170470E-03,-0.46435770E-03,
         0.21294654E-03, 0.51397528E-03,-0.31585849E-03,-0.69958402E-03,
        -0.17012013E-02,-0.46845590E-03,-0.17875625E-03, 0.30033363E-03,
        -0.18626383E-03,-0.10256320E-02, 0.72128366E-03,-0.12175030E-03,
        -0.49637136E-03, 0.47129474E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[  8]            *x41    
        +coeff[  9]                *x51
        +coeff[ 10]*x12                
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]        *x33*x41    
        +coeff[ 13]        *x32    *x52
        +coeff[ 14]    *x21        *x53
        +coeff[ 15]*x11*x22    *x41    
        +coeff[ 16]*x11*x21*x31    *x52
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 17]*x11        *x42*x52
        +coeff[ 18]*x12*x22        *x51
        +coeff[ 19]*x12        *x42*x51
        +coeff[ 20]    *x21*x33    *x52
        +coeff[ 21]        *x32*x42*x52
        +coeff[ 22]*x11*x22*x31*x42    
        +coeff[ 23]*x11    *x32*x42*x51
        +coeff[ 24]*x11*x22*x31    *x52
        +coeff[ 25]*x13    *x33        
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 26]    *x24*x31    *x52
        +coeff[ 27]    *x23*x31*x41*x52
        +coeff[ 28]    *x21*x33*x41*x52
        +coeff[ 29]    *x23        *x54
        +coeff[ 30]*x11*x24*x31*x41    
        +coeff[ 31]*x11*x22*x32*x42    
        +coeff[ 32]*x11*x21*x33*x41*x51
        +coeff[ 33]*x11    *x34*x41*x51
        +coeff[ 34]*x11    *x33*x41*x52
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 35]*x12*x21*x33    *x51
        +coeff[ 36]    *x21            
        +coeff[ 37]    *x21        *x51
        +coeff[ 38]            *x41*x51
        +coeff[ 39]*x11    *x31        
        +coeff[ 40]*x11            *x51
        +coeff[ 41]    *x22    *x41    
        +coeff[ 42]    *x21    *x42    
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 44]            *x42*x51
        +coeff[ 45]    *x21        *x52
        +coeff[ 46]*x11        *x42    
        +coeff[ 47]*x12*x21            
        +coeff[ 48]*x12    *x31        
        +coeff[ 49]*x12        *x41    
        +coeff[ 50]*x12            *x51
        +coeff[ 51]            *x44    
        +coeff[ 52]    *x21    *x42*x51
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 53]            *x43*x51
        +coeff[ 54]    *x22        *x52
        +coeff[ 55]        *x31*x41*x52
        +coeff[ 56]        *x31    *x53
        +coeff[ 57]            *x41*x53
        +coeff[ 58]*x11*x22*x31        
        +coeff[ 59]*x11*x21*x32        
        +coeff[ 60]*x11    *x31*x42    
        +coeff[ 61]*x11*x22        *x51
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 62]*x11*x21        *x52
        +coeff[ 63]*x11    *x31    *x52
        +coeff[ 64]*x11        *x41*x52
        +coeff[ 65]*x12        *x42    
        +coeff[ 66]    *x22*x33        
        +coeff[ 67]    *x24    *x41    
        +coeff[ 68]    *x23*x31    *x51
        +coeff[ 69]        *x34    *x51
        +coeff[ 70]    *x22*x31*x41*x51
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 71]    *x21*x32*x41*x51
        +coeff[ 72]    *x23        *x52
        +coeff[ 73]    *x21*x32    *x52
        +coeff[ 74]    *x21    *x42*x52
        +coeff[ 75]        *x32    *x53
        +coeff[ 76]        *x31*x41*x53
        +coeff[ 77]*x11*x23*x31        
        +coeff[ 78]*x11*x21*x33        
        +coeff[ 79]*x11*x22    *x42    
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 80]*x11*x21*x31*x41*x51
        +coeff[ 81]*x11*x21    *x42*x51
        +coeff[ 82]*x11        *x43*x51
        +coeff[ 83]*x11*x22        *x52
        +coeff[ 84]*x11    *x32    *x52
        +coeff[ 85]*x11    *x31*x41*x52
        +coeff[ 86]*x12*x23            
        +coeff[ 87]*x12*x22*x31        
        +coeff[ 88]*x12*x21*x32        
    ;
    v_l_e_qd_200                                  =v_l_e_qd_200                                  
        +coeff[ 89]*x12*x22    *x41    
        ;

    return v_l_e_qd_200                                  ;
}
float x_e_qd_175                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1317895E+01;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
        -0.14083652E-02,-0.64162142E-02, 0.23811007E+00, 0.10455626E-01,
         0.56294311E-03,-0.16186907E-02,-0.54118088E-02,-0.38196142E-02,
        -0.23306407E-05,-0.77201939E-05,-0.12683875E-02,-0.22507680E-02,
        -0.61754760E-03,-0.41063693E-02,-0.29803850E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_175                                  =v_x_e_qd_175                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_175                                  ;
}
float t_e_qd_175                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5776566E+00;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.28625529E-03,-0.31996868E-02, 0.76794475E-01, 0.30641183E-02,
        -0.25097679E-02,-0.19069342E-02,-0.17852036E-02,-0.80143119E-03,
         0.21445428E-03, 0.23865917E-03,-0.19487982E-03,-0.12999915E-05,
        -0.85028028E-03,-0.25351116E-03,-0.16103117E-02,-0.95279000E-04,
        -0.55393565E-03,-0.23745601E-02, 0.27990405E-03, 0.18991373E-03,
        -0.19175457E-02,-0.15330714E-02, 0.20978263E-04, 0.51037273E-05,
         0.16872352E-06,-0.53591415E-04,-0.30017276E-04,-0.19574418E-04,
         0.16773822E-03,-0.86625706E-03, 0.38069120E-04,-0.12574505E-04,
         0.18911576E-04, 0.42139265E-04, 0.43796267E-05, 0.11528601E-05,
        -0.29214511E-04,-0.93672152E-05,-0.70949636E-05,-0.85525221E-06,
         0.70793045E-04, 0.18356490E-03, 0.61760358E-04, 0.96783580E-04,
         0.21710173E-04, 0.23358671E-04, 0.26795589E-04,-0.10664094E-03,
        -0.96380638E-04,-0.32436270E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_175                                  =v_t_e_qd_175                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]    *x23            
    ;
    v_t_e_qd_175                                  =v_t_e_qd_175                                  
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_175                                  =v_t_e_qd_175                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x21*x33*x41    
        +coeff[ 30]*x12*x22*x31*x41    
        +coeff[ 31]*x12    *x33*x41    
        +coeff[ 32]    *x22*x33*x41    
        +coeff[ 33]*x12*x23        *x51
        +coeff[ 34]    *x21*x31        
    ;
    v_t_e_qd_175                                  =v_t_e_qd_175                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]*x11    *x32        
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]*x11    *x31    *x51
        +coeff[ 39]            *x42*x51
        +coeff[ 40]    *x22*x32        
        +coeff[ 41]    *x22*x31*x41    
        +coeff[ 42]    *x23        *x51
        +coeff[ 43]    *x21*x32    *x51
    ;
    v_t_e_qd_175                                  =v_t_e_qd_175                                  
        +coeff[ 44]*x11*x21    *x41*x51
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x13*x22            
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]*x11*x22    *x42    
        +coeff[ 49]    *x22    *x42*x51
        ;

    return v_t_e_qd_175                                  ;
}
float y_e_qd_175                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.5052847E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.39793024E-03, 0.17751603E+00, 0.93811847E-01, 0.40888377E-02,
         0.12643113E-02,-0.14406132E-02,-0.93708158E-03,-0.13683387E-02,
        -0.10233226E-02, 0.11190241E-04,-0.22685673E-03,-0.26545054E-03,
        -0.74383192E-03,-0.14899034E-03,-0.21097781E-04,-0.71315677E-03,
        -0.71268063E-03,-0.36720998E-04, 0.14500512E-03, 0.10259196E-03,
         0.47641177E-04,-0.14964275E-02,-0.11048533E-02,-0.35608825E-03,
         0.50797855E-04,-0.31110746E-03,-0.74654364E-03,-0.12515641E-03,
        -0.63061365E-03,-0.52098947E-03,-0.44043802E-04, 0.58040958E-04,
        -0.12433391E-04,-0.13826806E-04,-0.39759216E-04,-0.71393435E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_175                                  =v_y_e_qd_175                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_175                                  =v_y_e_qd_175                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]        *x31*x42*x51
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]            *x45*x51
        +coeff[ 25]    *x22    *x45    
    ;
    v_y_e_qd_175                                  =v_y_e_qd_175                                  
        +coeff[ 26]    *x22*x31*x44    
        +coeff[ 27]    *x24    *x43    
        +coeff[ 28]    *x22*x32*x43    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]        *x33*x42    
        +coeff[ 31]        *x34*x41    
        +coeff[ 32]    *x21    *x45    
        +coeff[ 33]*x11        *x43*x51
        +coeff[ 34]        *x31*x42*x52
    ;
    v_y_e_qd_175                                  =v_y_e_qd_175                                  
        +coeff[ 35]*x11*x23*x31        
        ;

    return v_y_e_qd_175                                  ;
}
float p_e_qd_175                                  (float *x,int m){
    int ncoeff= 17;
    float avdat=  0.1219469E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 18]={
        -0.10781747E-03,-0.23802640E-01,-0.22771260E-01, 0.21764804E-02,
         0.33850935E-02, 0.78713894E-03, 0.76106761E-03,-0.76609309E-03,
         0.15790229E-04,-0.50831278E-03,-0.69544883E-03,-0.37535263E-03,
        -0.16889977E-03, 0.51621696E-05,-0.19953205E-03,-0.42524157E-03,
        -0.58278110E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_175                                  =v_p_e_qd_175                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]            *x43    
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x34*x41    
        +coeff[ 14]        *x33    *x52
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]    *x21    *x41*x51
        ;

    return v_p_e_qd_175                                  ;
}
float l_e_qd_175                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3638942E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.35731127E-02,-0.71134428E-02,-0.11976635E-02,-0.55474914E-02,
        -0.59492192E-02, 0.13908288E-03,-0.42809121E-03,-0.13886584E-02,
         0.57290104E-04, 0.21061634E-03,-0.23204750E-03,-0.51351497E-03,
         0.19714574E-03,-0.41726613E-03, 0.12211672E-03, 0.57796395E-03,
         0.27902039E-04,-0.12870964E-02, 0.51645713E-03, 0.76556229E-03,
        -0.19763946E-03,-0.22093023E-02,-0.11801521E-02,-0.98266151E-04,
         0.69427735E-03,-0.38904313E-03,-0.16088753E-02, 0.14358495E-02,
        -0.11754634E-02,-0.12648605E-02, 0.13059317E-02,-0.10863559E-02,
         0.11715982E-02, 0.20118409E-02, 0.39846447E-03,-0.10639128E-02,
         0.18342851E-02, 0.91460033E-03, 0.19145854E-02, 0.35153120E-02,
        -0.94523211E-03,-0.77563600E-03, 0.26040641E-02, 0.12126096E-02,
         0.79032674E-03, 0.58092683E-03, 0.34389340E-02,-0.11837124E-02,
         0.26525885E-02,-0.19335564E-02, 0.79238176E-03, 0.71730418E-03,
         0.36297593E-03, 0.13912280E-03,-0.66267268E-03,-0.79430938E-05,
         0.45596360E-03,-0.16275365E-03,-0.86364111E-04, 0.32723951E-03,
         0.94425741E-04, 0.17992681E-03,-0.25711980E-03, 0.51015860E-03,
         0.14081594E-02, 0.72992983E-03,-0.33368543E-03,-0.41323729E-03,
         0.14459340E-03, 0.16209768E-03,-0.33458963E-03, 0.48975874E-03,
         0.32777269E-03, 0.14097687E-03,-0.20716316E-03, 0.22411750E-03,
         0.11725590E-03,-0.18206629E-03,-0.60173916E-03, 0.23579311E-03,
         0.63702045E-03, 0.35628644E-03, 0.12052679E-02,-0.88281790E-03,
         0.93776430E-03,-0.39454183E-03, 0.34592001E-03, 0.49895345E-03,
         0.56217093E-03,-0.59156847E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]*x11*x23        *x52
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[  8]        *x31        
        +coeff[  9]        *x32*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23        *x51
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x41*x52
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 17]*x11*x21*x31    *x51
        +coeff[ 18]*x11        *x42*x51
        +coeff[ 19]*x13*x21            
        +coeff[ 20]*x13        *x41    
        +coeff[ 21]    *x21*x32    *x52
        +coeff[ 22]*x11*x22    *x41*x51
        +coeff[ 23]*x12*x22        *x51
        +coeff[ 24]*x12*x21    *x41*x51
        +coeff[ 25]*x12    *x31    *x52
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 26]    *x23*x32*x41    
        +coeff[ 27]    *x23*x32    *x51
        +coeff[ 28]*x13        *x41*x51
        +coeff[ 29]    *x21*x32    *x53
        +coeff[ 30]*x11*x21*x33    *x51
        +coeff[ 31]*x11*x22    *x42*x51
        +coeff[ 32]*x11*x21        *x54
        +coeff[ 33]*x12*x21*x32    *x51
        +coeff[ 34]    *x23*x34        
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 35]*x13*x21        *x52
        +coeff[ 36]    *x21*x34    *x52
        +coeff[ 37]    *x21*x31*x42*x53
        +coeff[ 38]*x11*x24*x31    *x51
        +coeff[ 39]*x11*x22*x32*x41*x51
        +coeff[ 40]*x11*x21    *x44*x51
        +coeff[ 41]*x11*x24        *x52
        +coeff[ 42]*x12*x22    *x42*x51
        +coeff[ 43]*x12    *x32*x41*x52
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 44]*x12    *x31*x41*x53
        +coeff[ 45]*x13*x22*x32        
        +coeff[ 46]*x13*x22    *x41*x51
        +coeff[ 47]    *x23*x33    *x52
        +coeff[ 48]    *x23*x32*x41*x52
        +coeff[ 49]        *x34*x42*x52
        +coeff[ 50]*x13*x21        *x53
        +coeff[ 51]            *x44*x54
        +coeff[ 52]    *x21    *x41    
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 53]*x11    *x31        
        +coeff[ 54]        *x31*x41*x51
        +coeff[ 55]                *x53
        +coeff[ 56]*x11*x21*x31        
        +coeff[ 57]*x11*x21        *x51
        +coeff[ 58]*x11    *x31    *x51
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]*x11            *x52
        +coeff[ 61]*x12    *x31        
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 62]    *x23    *x41    
        +coeff[ 63]        *x33*x41    
        +coeff[ 64]        *x32*x42    
        +coeff[ 65]        *x31*x43    
        +coeff[ 66]    *x22*x31    *x51
        +coeff[ 67]    *x21*x32    *x51
        +coeff[ 68]        *x33    *x51
        +coeff[ 69]        *x31*x42*x51
        +coeff[ 70]    *x22        *x52
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 71]    *x21*x31    *x52
        +coeff[ 72]    *x21        *x53
        +coeff[ 73]                *x54
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]*x11        *x41*x52
        +coeff[ 76]*x12*x22            
        +coeff[ 77]*x12*x21    *x41    
        +coeff[ 78]*x12*x21        *x51
        +coeff[ 79]    *x22*x31*x42    
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 80]    *x21    *x44    
        +coeff[ 81]    *x22*x32    *x51
        +coeff[ 82]        *x33*x41*x51
        +coeff[ 83]    *x22    *x42*x51
        +coeff[ 84]        *x32*x42*x51
        +coeff[ 85]        *x32*x41*x52
        +coeff[ 86]    *x22        *x53
        +coeff[ 87]        *x32    *x53
        +coeff[ 88]        *x31*x41*x53
    ;
    v_l_e_qd_175                                  =v_l_e_qd_175                                  
        +coeff[ 89]*x11*x23*x31        
        ;

    return v_l_e_qd_175                                  ;
}
float x_e_qd_150                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1316010E+01;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.49898314E-03,-0.63534747E-02, 0.24010631E+00, 0.10362733E-01,
         0.55503071E-03,-0.15912940E-02,-0.53283046E-02,-0.37410269E-02,
         0.33411663E-04,-0.20240113E-04,-0.12838548E-02,-0.22061307E-02,
        -0.58519945E-03,-0.41647651E-02,-0.30681326E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_150                                  =v_x_e_qd_150                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_150                                  ;
}
float t_e_qd_150                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5782903E+00;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.92557579E-03,-0.31796114E-02, 0.77619016E-01, 0.30261253E-02,
        -0.25587524E-02,-0.18737491E-02,-0.17612376E-02,-0.80085662E-03,
         0.21865655E-03, 0.23424072E-03,-0.20601442E-03,-0.19030166E-07,
        -0.54390746E-03,-0.83630776E-03,-0.79598540E-04,-0.24121701E-03,
        -0.23673929E-02,-0.16256383E-02, 0.32653575E-03, 0.23243610E-03,
        -0.19511194E-02,-0.15622742E-02,-0.31393884E-04, 0.66095186E-05,
         0.29606331E-05,-0.47350612E-04,-0.14993194E-04, 0.21630817E-03,
         0.13100696E-03, 0.54581932E-06,-0.27872777E-05,-0.88049076E-03,
         0.53886331E-04, 0.78702951E-05,-0.31609597E-04, 0.12657025E-04,
         0.73810821E-04,-0.24281399E-04, 0.78052755E-04, 0.10191972E-03,
        -0.92569753E-05,-0.11896159E-03,-0.29147372E-04,-0.82641054E-04,
        -0.21027940E-04, 0.26496642E-04, 0.43315384E-04,-0.42511679E-05,
         0.46024529E-05,-0.29711846E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_150                                  =v_t_e_qd_150                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_150                                  =v_t_e_qd_150                                  
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_150                                  =v_t_e_qd_150                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]        *x32*x42    
        +coeff[ 30]        *x32    *x52
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]    *x21*x32    *x53
        +coeff[ 33]        *x32        
        +coeff[ 34]*x11    *x32        
    ;
    v_t_e_qd_150                                  =v_t_e_qd_150                                  
        +coeff[ 35]    *x21    *x41*x51
        +coeff[ 36]    *x22*x32        
        +coeff[ 37]*x11*x21    *x42    
        +coeff[ 38]    *x23        *x51
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]        *x31*x41*x52
        +coeff[ 41]*x11*x22*x31*x41    
        +coeff[ 42]*x13        *x42    
        +coeff[ 43]*x11*x22    *x42    
    ;
    v_t_e_qd_150                                  =v_t_e_qd_150                                  
        +coeff[ 44]*x11*x22    *x41*x51
        +coeff[ 45]*x11*x21    *x43*x51
        +coeff[ 46]    *x22    *x42*x52
        +coeff[ 47]*x12                
        +coeff[ 48]    *x21    *x41    
        +coeff[ 49]            *x41*x51
        ;

    return v_t_e_qd_150                                  ;
}
float y_e_qd_150                                  (float *x,int m){
    int ncoeff= 33;
    float avdat=  0.4122821E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
        -0.12248897E-03, 0.17693831E+00, 0.93278468E-01, 0.40819589E-02,
         0.12457804E-02,-0.13305970E-02,-0.92644914E-03,-0.13799191E-02,
        -0.94736094E-03,-0.18557863E-04,-0.22370693E-03,-0.26025809E-03,
        -0.70003164E-03,-0.15310785E-03,-0.30467148E-04,-0.78364729E-03,
        -0.71268604E-03,-0.34943911E-04, 0.19660946E-03, 0.10704106E-03,
        -0.89002313E-03,-0.17746771E-02,-0.13085344E-02,-0.36584842E-03,
         0.53095355E-04,-0.17998478E-04,-0.35112416E-05, 0.32614407E-04,
        -0.11517863E-04, 0.60593233E-04, 0.44599696E-04,-0.51181258E-04,
        -0.75073913E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_150                                  =v_y_e_qd_150                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]            *x43    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_150                                  =v_y_e_qd_150                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]        *x31*x42*x51
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]            *x45*x51
        +coeff[ 25]    *x22*x33    *x51
    ;
    v_y_e_qd_150                                  =v_y_e_qd_150                                  
        +coeff[ 26]                *x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]    *x22*x31    *x51
        +coeff[ 30]        *x33    *x51
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]        *x33*x42*x51
        ;

    return v_y_e_qd_150                                  ;
}
float p_e_qd_150                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.1149029E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
         0.47333792E-07,-0.23837734E-01,-0.22818891E-01, 0.21731933E-02,
         0.33832502E-02, 0.79606770E-03, 0.76902576E-03,-0.77837444E-03,
        -0.50173351E-03,-0.69200789E-03,-0.37341675E-03,-0.16639120E-03,
         0.10648742E-04,-0.19554551E-03,-0.42698186E-03,-0.58220648E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_150                                  =v_p_e_qd_150                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_150                                  ;
}
float l_e_qd_150                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3635598E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36818644E-02,-0.74511771E-02,-0.18073365E-02,-0.54515833E-02,
        -0.61009089E-02, 0.41940223E-03,-0.48870471E-03, 0.23542457E-03,
         0.70770504E-03, 0.38783962E-03, 0.98201861E-04, 0.20398993E-03,
         0.20944711E-03,-0.55393286E-03,-0.21400815E-03, 0.15230249E-04,
         0.39452649E-03, 0.87050349E-03,-0.10862264E-02,-0.10497622E-02,
        -0.19629819E-02,-0.16197971E-02,-0.14345812E-03,-0.17672990E-02,
         0.88173803E-03,-0.46026681E-03, 0.78984170E-03,-0.65624813E-03,
        -0.13189482E-02, 0.46389229E-02, 0.44818418E-02, 0.11047720E-02,
        -0.14300563E-02, 0.19481977E-02,-0.31983491E-03, 0.70471619E-03,
        -0.12768222E-02, 0.13640627E-02, 0.20301414E-02, 0.27212041E-04,
        -0.22275311E-04, 0.18338943E-03,-0.49530823E-04, 0.86407752E-04,
         0.47556989E-03, 0.47116287E-03, 0.55755962E-04,-0.15519430E-03,
        -0.97913621E-03, 0.37432750E-03, 0.51302568E-03, 0.28291333E-03,
         0.72417373E-03, 0.38023185E-03,-0.14906607E-03, 0.27476292E-03,
         0.34802462E-03,-0.32123533E-03,-0.22888438E-03,-0.26198593E-03,
         0.56957843E-03, 0.22798050E-03, 0.26342665E-04,-0.24246192E-03,
         0.27016082E-03,-0.17035521E-03,-0.10445568E-03,-0.22136133E-03,
        -0.44569140E-03, 0.71375747E-03, 0.67521738E-04,-0.64871868E-03,
         0.23314485E-03,-0.47003699E-03,-0.59834687E-03, 0.56759937E-03,
         0.58396079E-03,-0.62741491E-03,-0.22874947E-03,-0.43546813E-03,
        -0.15220817E-03,-0.63756388E-03,-0.58858201E-03,-0.41416171E-03,
        -0.86044043E-03, 0.50155714E-03,-0.98045648E-03, 0.30570434E-03,
         0.47101043E-03, 0.91990567E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x42*x51
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[  8]        *x33*x41*x51
        +coeff[  9]        *x32    *x53
        +coeff[ 10]                *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]                *x52
        +coeff[ 13]    *x21    *x41*x51
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]            *x44    
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 17]    *x22    *x43    
        +coeff[ 18]        *x32*x43    
        +coeff[ 19]    *x23*x31    *x51
        +coeff[ 20]    *x21*x31*x41*x52
        +coeff[ 21]    *x21    *x42*x52
        +coeff[ 22]*x11*x22    *x42    
        +coeff[ 23]*x11*x21*x31*x41*x51
        +coeff[ 24]*x12*x21    *x41*x51
        +coeff[ 25]*x12        *x41*x52
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 26]*x11*x21*x32    *x52
        +coeff[ 27]*x12*x21    *x41*x52
        +coeff[ 28]*x13    *x32*x41    
        +coeff[ 29]    *x23*x32*x42    
        +coeff[ 30]    *x23*x31*x43    
        +coeff[ 31]    *x23*x33    *x51
        +coeff[ 32]    *x23*x32    *x52
        +coeff[ 33]    *x21    *x44*x52
        +coeff[ 34]        *x31*x44*x52
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 35]    *x22*x31    *x54
        +coeff[ 36]        *x31*x42*x54
        +coeff[ 37]*x12*x22    *x42*x51
        +coeff[ 38]*x13    *x31*x41*x52
        +coeff[ 39]            *x41    
        +coeff[ 40]*x11                
        +coeff[ 41]    *x21        *x51
        +coeff[ 42]*x11        *x41    
        +coeff[ 43]    *x23            
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 44]        *x32*x41    
        +coeff[ 45]        *x31*x41*x51
        +coeff[ 46]*x11    *x32        
        +coeff[ 47]*x11        *x42    
        +coeff[ 48]*x11            *x52
        +coeff[ 49]*x13                
        +coeff[ 50]    *x22*x32        
        +coeff[ 51]        *x34        
        +coeff[ 52]    *x22*x31*x41    
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 53]    *x22    *x42    
        +coeff[ 54]    *x23        *x51
        +coeff[ 55]    *x22    *x41*x51
        +coeff[ 56]    *x21*x31*x41*x51
        +coeff[ 57]        *x32*x41*x51
        +coeff[ 58]        *x32    *x52
        +coeff[ 59]*x11*x21*x32        
        +coeff[ 60]*x11    *x32*x41    
        +coeff[ 61]*x11*x21    *x42    
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 62]*x11    *x31*x41*x51
        +coeff[ 63]*x11        *x42*x51
        +coeff[ 64]*x11        *x41*x52
        +coeff[ 65]*x12        *x41*x51
        +coeff[ 66]*x12            *x52
        +coeff[ 67]*x13*x21            
        +coeff[ 68]    *x24    *x41    
        +coeff[ 69]    *x21*x33*x41    
        +coeff[ 70]*x13            *x51
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 71]    *x22*x32    *x51
        +coeff[ 72]        *x34    *x51
        +coeff[ 73]    *x21*x31*x42*x51
        +coeff[ 74]        *x31*x43*x51
        +coeff[ 75]    *x21*x32    *x52
        +coeff[ 76]    *x22    *x41*x52
        +coeff[ 77]        *x32*x41*x52
        +coeff[ 78]            *x43*x52
        +coeff[ 79]    *x21    *x41*x53
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 80]    *x21        *x54
        +coeff[ 81]*x11*x21*x32    *x51
        +coeff[ 82]*x11    *x33    *x51
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]*x11*x21    *x42*x51
        +coeff[ 85]*x11        *x43*x51
        +coeff[ 86]*x11    *x31*x41*x52
        +coeff[ 87]*x11*x21        *x53
        +coeff[ 88]*x11    *x31    *x53
    ;
    v_l_e_qd_150                                  =v_l_e_qd_150                                  
        +coeff[ 89]*x11            *x54
        ;

    return v_l_e_qd_150                                  ;
}
float x_e_qd_125                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1315883E+01;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.66662981E-03,-0.62686172E-02, 0.24292831E+00, 0.10222770E-01,
         0.54480840E-03,-0.53930548E-02,-0.37645823E-02, 0.17622117E-04,
         0.48024445E-04,-0.11673445E-02,-0.16102816E-02,-0.22368182E-02,
        -0.61781821E-03,-0.38627009E-02,-0.28880532E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_x_e_qd_125                                  =v_x_e_qd_125                                  
        +coeff[  8]    *x23        *x52
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_125                                  ;
}
float t_e_qd_125                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5783443E+00;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.99584402E-03,-0.31435993E-02, 0.78776032E-01, 0.29575496E-02,
        -0.26495480E-02,-0.18572869E-02,-0.17714615E-02,-0.79756539E-03,
         0.21576537E-03, 0.23189947E-03,-0.20163426E-03, 0.18470746E-04,
        -0.82046125E-03,-0.23799379E-03,-0.15923191E-02,-0.83411804E-04,
        -0.54008752E-03,-0.23368532E-02, 0.29777689E-03, 0.20188441E-03,
        -0.20228385E-02,-0.16076661E-02, 0.27892949E-04, 0.79186805E-06,
         0.17460950E-06,-0.67002577E-04,-0.45823934E-04, 0.22255017E-03,
         0.16455770E-03, 0.92606761E-05,-0.81401333E-06,-0.14098476E-04,
        -0.91860967E-03,-0.53655858E-04, 0.24186831E-04, 0.39710795E-06,
         0.95451069E-05,-0.25531926E-04,-0.16964555E-04, 0.79369427E-04,
        -0.16488937E-04, 0.63689957E-04, 0.98782344E-04, 0.25771305E-04,
         0.31022864E-04, 0.32548036E-04,-0.41029525E-04, 0.17817969E-04,
        -0.33243050E-04,-0.23807934E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_qd_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x22            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_125                                  =v_t_e_qd_125                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]    *x23            
    ;
    v_t_e_qd_125                                  =v_t_e_qd_125                                  
        +coeff[ 17]    *x23*x31*x41    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]        *x31*x41    
        +coeff[ 24]            *x42    
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_qd_125                                  =v_t_e_qd_125                                  
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]        *x32*x42    
        +coeff[ 30]        *x32    *x52
        +coeff[ 31]*x13    *x32        
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]*x11*x21        *x53
        +coeff[ 34]    *x22*x32    *x52
    ;
    v_t_e_qd_125                                  =v_t_e_qd_125                                  
        +coeff[ 35]        *x32        
        +coeff[ 36]*x13                
        +coeff[ 37]*x11    *x32        
        +coeff[ 38]*x11            *x52
        +coeff[ 39]    *x22*x32        
        +coeff[ 40]*x12        *x42    
        +coeff[ 41]    *x23        *x51
        +coeff[ 42]    *x21*x32    *x51
        +coeff[ 43]    *x21        *x53
    ;
    v_t_e_qd_125                                  =v_t_e_qd_125                                  
        +coeff[ 44]*x12*x21    *x42    
        +coeff[ 45]*x13*x21        *x51
        +coeff[ 46]*x12*x21*x31    *x51
        +coeff[ 47]    *x21*x33    *x51
        +coeff[ 48]    *x21*x32    *x52
        +coeff[ 49]*x11*x23*x31    *x51
        ;

    return v_t_e_qd_125                                  ;
}
float y_e_qd_125                                  (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.3988828E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.41325804E-03, 0.17619129E+00, 0.92478521E-01, 0.41026822E-02,
         0.12903871E-02,-0.13851181E-02,-0.94996585E-03,-0.13738004E-02,
        -0.95156103E-03, 0.24510664E-04,-0.74375054E-03,-0.22630973E-03,
        -0.25448491E-03,-0.15072548E-03,-0.14475824E-04,-0.75961713E-03,
        -0.70015166E-03,-0.86347299E-03,-0.17034098E-02,-0.12127823E-02,
        -0.33474230E-03,-0.52012874E-04,-0.86193555E-04, 0.62316227E-04,
        -0.11619618E-04, 0.13472501E-03, 0.99322111E-04, 0.58033103E-04,
         0.37222708E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_125                                  =v_y_e_qd_125                                  
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]            *x43    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_qd_125                                  =v_y_e_qd_125                                  
        +coeff[ 17]    *x22    *x43    
        +coeff[ 18]    *x22*x31*x42    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]*x11*x23*x31        
        +coeff[ 23]    *x24*x31    *x51
        +coeff[ 24]*x12        *x41    
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_qd_125                                  =v_y_e_qd_125                                  
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x22    *x43*x51
        ;

    return v_y_e_qd_125                                  ;
}
float p_e_qd_125                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.4993753E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.53749962E-04,-0.23873776E-01,-0.22890255E-01, 0.21650409E-02,
         0.33781372E-02, 0.80889097E-03, 0.78193331E-03,-0.79698866E-03,
        -0.51247643E-03,-0.68914413E-03,-0.37364601E-03,-0.16617430E-03,
         0.13879554E-04,-0.19557767E-03,-0.42693602E-03,-0.58465615E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_125                                  =v_p_e_qd_125                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_125                                  ;
}
float l_e_qd_125                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3719120E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.36961040E-02,-0.82826213E-04,-0.75165974E-02,-0.13268932E-02,
        -0.51981728E-02,-0.54643275E-02, 0.49693906E-03,-0.58600848E-03,
         0.21651591E-03, 0.43666028E-03, 0.13790939E-03, 0.63525554E-03,
        -0.43883460E-03,-0.56601356E-03, 0.18653073E-03,-0.39216955E-03,
        -0.22582084E-03,-0.34812753E-03,-0.32698564E-03, 0.15985878E-02,
        -0.21158339E-03, 0.87616692E-03, 0.94333681E-03,-0.63550228E-03,
        -0.19751486E-04,-0.35109147E-03,-0.33034151E-03,-0.17965300E-02,
        -0.10116437E-02, 0.84334670E-03, 0.10478884E-02, 0.11696960E-02,
         0.95297949E-03,-0.22311637E-02,-0.11725631E-02, 0.40831049E-02,
         0.20484952E-02,-0.23078730E-02,-0.11308899E-02, 0.53899753E-03,
         0.89181459E-03,-0.88936859E-03, 0.41284511E-04, 0.10583144E-03,
        -0.44662287E-03, 0.26171561E-03, 0.62028726E-03, 0.18433327E-03,
        -0.56946941E-03,-0.77860721E-04, 0.13084117E-03,-0.73602051E-03,
         0.42312231E-03, 0.24559122E-03, 0.36266596E-04, 0.31455907E-04,
        -0.27856950E-03,-0.21856202E-04, 0.35178216E-03, 0.12385650E-03,
         0.50238322E-03,-0.18819011E-03,-0.41876800E-03, 0.99447498E-03,
        -0.31126788E-03,-0.12142409E-03, 0.33673370E-03,-0.28899408E-03,
         0.24041449E-03,-0.28324145E-03, 0.53109362E-03,-0.20568406E-03,
        -0.28326650E-03,-0.37683095E-03,-0.70180430E-03, 0.45049281E-03,
         0.97295502E-03, 0.38508369E-03,-0.25594237E-03, 0.17876380E-03,
         0.81482576E-03, 0.12467870E-02, 0.45650208E-03, 0.65370911E-03,
        -0.34758789E-03, 0.57968003E-03, 0.35508137E-03,-0.52903307E-03,
        -0.63052488E-03, 0.47421362E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[  8]                *x53
        +coeff[  9]        *x31*x41*x53
        +coeff[ 10]*x12                
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]*x12            *x51
        +coeff[ 15]*x11*x21*x32        
        +coeff[ 16]*x11    *x32    *x51
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 17]*x12    *x32        
        +coeff[ 18]*x12    *x31    *x51
        +coeff[ 19]        *x33*x42    
        +coeff[ 20]    *x21*x33    *x51
        +coeff[ 21]    *x22*x31*x41*x51
        +coeff[ 22]*x11*x23        *x51
        +coeff[ 23]*x11*x22*x31    *x51
        +coeff[ 24]*x11    *x33    *x51
        +coeff[ 25]*x12    *x33        
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 26]*x12        *x41*x52
        +coeff[ 27]        *x34*x41*x51
        +coeff[ 28]*x11*x22        *x53
        +coeff[ 29]*x12*x22    *x42    
        +coeff[ 30]*x12    *x32*x41*x51
        +coeff[ 31]*x12*x21    *x42*x51
        +coeff[ 32]*x13*x22        *x51
        +coeff[ 33]    *x24*x31*x41*x51
        +coeff[ 34]*x13        *x41*x52
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 35]*x11    *x33*x42*x51
        +coeff[ 36]*x11    *x32*x43*x51
        +coeff[ 37]    *x23*x32*x42*x51
        +coeff[ 38]*x13    *x31    *x53
        +coeff[ 39]    *x21*x34    *x53
        +coeff[ 40]    *x24        *x54
        +coeff[ 41]    *x22*x32    *x54
        +coeff[ 42]*x11                
        +coeff[ 43]        *x31    *x51
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 44]                *x52
        +coeff[ 45]*x11    *x31        
        +coeff[ 46]    *x22*x31        
        +coeff[ 47]    *x21*x32        
        +coeff[ 48]    *x21*x31*x41    
        +coeff[ 49]        *x32    *x51
        +coeff[ 50]    *x21    *x41*x51
        +coeff[ 51]*x11*x22            
        +coeff[ 52]*x11    *x32        
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 53]*x11    *x31*x41    
        +coeff[ 54]*x11    *x31    *x51
        +coeff[ 55]*x12    *x31        
        +coeff[ 56]    *x23*x31        
        +coeff[ 57]    *x23    *x41    
        +coeff[ 58]    *x22*x31*x41    
        +coeff[ 59]    *x21*x32*x41    
        +coeff[ 60]    *x21*x31*x42    
        +coeff[ 61]            *x44    
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 62]    *x21*x31*x41*x51
        +coeff[ 63]        *x32*x41*x51
        +coeff[ 64]        *x31*x41*x52
        +coeff[ 65]            *x41*x53
        +coeff[ 66]                *x54
        +coeff[ 67]*x11    *x33        
        +coeff[ 68]*x11*x21    *x41*x51
        +coeff[ 69]*x11*x21        *x52
        +coeff[ 70]*x11        *x41*x52
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 71]*x12*x21        *x51
        +coeff[ 72]*x13    *x31        
        +coeff[ 73]    *x23*x32        
        +coeff[ 74]    *x22*x33        
        +coeff[ 75]    *x21*x32*x42    
        +coeff[ 76]    *x21*x31*x43    
        +coeff[ 77]        *x31*x43*x51
        +coeff[ 78]    *x22*x31    *x52
        +coeff[ 79]            *x42*x53
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 80]*x11*x24            
        +coeff[ 81]*x11*x22*x31*x41    
        +coeff[ 82]*x11*x21*x32*x41    
        +coeff[ 83]*x11*x22    *x42    
        +coeff[ 84]*x11*x21*x32    *x51
        +coeff[ 85]*x11    *x32*x41*x51
        +coeff[ 86]*x11*x21*x31    *x52
        +coeff[ 87]*x11    *x32    *x52
        +coeff[ 88]*x11    *x31*x41*x52
    ;
    v_l_e_qd_125                                  =v_l_e_qd_125                                  
        +coeff[ 89]*x11    *x31    *x53
        ;

    return v_l_e_qd_125                                  ;
}
float x_e_qd_100                                  (float *x,int m){
    int ncoeff= 15;
    float avdat=  0.1314839E+01;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.17610355E-02,-0.61305440E-02, 0.24709335E+00, 0.10011058E-01,
         0.54802542E-03,-0.15494900E-02,-0.52103014E-02,-0.36669481E-02,
        -0.37475034E-04,-0.25387160E-05,-0.12756312E-02,-0.21225926E-02,
        -0.58361801E-03,-0.39907126E-02,-0.29714725E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_qd_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_qd_100                                  =v_x_e_qd_100                                  
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_qd_100                                  ;
}
float t_e_qd_100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5787214E+00;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.13892880E-02,-0.30906450E-02, 0.80501907E-01,-0.27702572E-02,
         0.28692561E-02,-0.18441799E-02,-0.17420466E-02,-0.81286824E-03,
         0.22045810E-03, 0.23122854E-03,-0.20469102E-03, 0.13921923E-04,
        -0.81390765E-03,-0.24677880E-03,-0.16117581E-02,-0.51652989E-03,
        -0.23433578E-02,-0.75804172E-04,-0.15010623E-02, 0.14330859E-05,
        -0.89683417E-05,-0.61185914E-04,-0.38513994E-04, 0.16689357E-03,
         0.18864442E-03, 0.30912473E-03, 0.21415303E-03,-0.44582507E-05,
        -0.79171286E-05,-0.85018779E-03,-0.18647234E-02, 0.40355713E-04,
         0.33005417E-04,-0.99093700E-06, 0.73288743E-05, 0.23759362E-05,
        -0.65695235E-05,-0.26807447E-04,-0.10730105E-04, 0.95737734E-04,
        -0.29156006E-04, 0.18838055E-04, 0.66327186E-04, 0.93746152E-04,
        -0.16577604E-04, 0.49123984E-04, 0.14087095E-04,-0.13902551E-03,
        -0.19314308E-04,-0.42025154E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_qd_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_qd_100                                  =v_t_e_qd_100                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]*x11            *x51
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x23        *x52
        +coeff[ 12]    *x21*x32        
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]    *x23    *x42    
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x23*x31*x41    
    ;
    v_t_e_qd_100                                  =v_t_e_qd_100                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x21*x31*x43    
        +coeff[ 19]        *x31*x41    
        +coeff[ 20]            *x42    
        +coeff[ 21]*x11    *x31*x41    
        +coeff[ 22]*x11        *x42    
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x22    *x42    
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_qd_100                                  =v_t_e_qd_100                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]        *x32    *x52
        +coeff[ 28]*x13    *x32        
        +coeff[ 29]    *x21*x33*x41    
        +coeff[ 30]    *x21*x32*x42    
        +coeff[ 31]*x12*x21*x32    *x51
        +coeff[ 32]    *x23*x32    *x51
        +coeff[ 33]        *x32        
        +coeff[ 34]                *x52
    ;
    v_t_e_qd_100                                  =v_t_e_qd_100                                  
        +coeff[ 35]*x13                
        +coeff[ 36]    *x22*x31        
        +coeff[ 37]*x11    *x32        
        +coeff[ 38]*x11            *x52
        +coeff[ 39]    *x22*x32        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x32*x42    
        +coeff[ 42]    *x23        *x51
        +coeff[ 43]    *x21*x32    *x51
    ;
    v_t_e_qd_100                                  =v_t_e_qd_100                                  
        +coeff[ 44]*x11*x21        *x52
        +coeff[ 45]*x11*x22*x32*x41    
        +coeff[ 46]    *x22*x33*x41    
        +coeff[ 47]    *x22*x32*x42    
        +coeff[ 48]    *x22*x33    *x51
        +coeff[ 49]*x12                
        ;

    return v_t_e_qd_100                                  ;
}
float y_e_qd_100                                  (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.1374404E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.12642066E-02, 0.17494002E+00, 0.91354206E-01, 0.41177711E-02,
         0.12870790E-02,-0.13816425E-02,-0.93415705E-03,-0.12898053E-02,
         0.73247802E-05,-0.13471988E-03,-0.85508550E-03,-0.20370621E-03,
        -0.24175472E-03,-0.67181262E-03,-0.15516252E-03,-0.26833348E-04,
        -0.79321151E-03,-0.70157583E-03,-0.35543486E-04, 0.14859725E-03,
        -0.43473836E-04,-0.15840640E-02,-0.10644989E-03,-0.11990148E-02,
        -0.50958191E-04,-0.37283916E-03,-0.64989887E-04,-0.53458894E-03,
         0.17743716E-03,-0.33061981E-03,-0.17436998E-03,-0.44118292E-05,
         0.47825553E-04, 0.10493697E-03, 0.48405622E-04, 0.30968327E-04,
        -0.90800918E-03, 0.21190912E-04,-0.37054451E-04,-0.57907455E-04,
         0.43340453E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_qd_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_qd_100                                  =v_y_e_qd_100                                  
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]*x11*x21*x31        
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_qd_100                                  =v_y_e_qd_100                                  
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x42*x51
        +coeff[ 20]        *x31*x44    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]        *x34*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_qd_100                                  =v_y_e_qd_100                                  
        +coeff[ 26]    *x22    *x45    
        +coeff[ 27]    *x22*x31*x44    
        +coeff[ 28]    *x24    *x43    
        +coeff[ 29]    *x22*x32*x43    
        +coeff[ 30]    *x24    *x45    
        +coeff[ 31]                *x51
        +coeff[ 32]            *x43*x51
        +coeff[ 33]        *x32*x41*x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_qd_100                                  =v_y_e_qd_100                                  
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]    *x22    *x43    
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]            *x43*x52
        +coeff[ 39]*x11*x23*x31        
        +coeff[ 40]    *x22    *x43*x51
        ;

    return v_y_e_qd_100                                  ;
}
float p_e_qd_100                                  (float *x,int m){
    int ncoeff= 16;
    float avdat=  0.2156383E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 17]={
        -0.19973583E-03,-0.23944048E-01,-0.22982365E-01, 0.21548916E-02,
         0.33666845E-02, 0.83123182E-03, 0.80774352E-03,-0.81652164E-03,
        -0.52325841E-03,-0.69228938E-03,-0.37870498E-03,-0.16343288E-03,
         0.17558737E-04,-0.19636685E-03,-0.42878528E-03,-0.63146486E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_qd_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x21*x31        
        +coeff[  6]    *x21    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_qd_100                                  =v_p_e_qd_100                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]            *x43    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x34*x41    
        +coeff[ 13]        *x33    *x52
        +coeff[ 14]        *x32*x41    
        +coeff[ 15]    *x21    *x41*x51
        ;

    return v_p_e_qd_100                                  ;
}
float l_e_qd_100                                  (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.3756762E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.37961903E-02,-0.77540544E-02,-0.16537677E-02,-0.51365900E-02,
        -0.56691235E-02, 0.60289825E-03,-0.18245737E-03,-0.22028433E-04,
        -0.29231166E-03,-0.64561167E-03, 0.29549410E-03, 0.78394084E-03,
        -0.32319676E-03,-0.46438875E-03, 0.65884757E-04,-0.13790794E-02,
        -0.13153541E-02,-0.68622897E-03, 0.89283905E-03, 0.31882842E-03,
         0.86490391E-03,-0.92358858E-03, 0.14365533E-02, 0.12885691E-02,
         0.13324138E-02, 0.15441038E-02, 0.28399922E-05,-0.21665855E-02,
        -0.16674743E-02, 0.10517524E-02, 0.13250413E-02,-0.75961399E-03,
         0.13755029E-02, 0.20788552E-02,-0.21267025E-03,-0.33372492E-03,
         0.37124674E-03, 0.54297016E-04,-0.20792718E-03, 0.26839328E-03,
         0.53890212E-03, 0.10927608E-03, 0.59340004E-03, 0.22121645E-04,
         0.31539210E-03,-0.45107663E-03, 0.10509986E-02, 0.31251390E-03,
        -0.10401591E-03, 0.16601357E-03, 0.21095955E-03,-0.26989670E-03,
         0.35137575E-03,-0.45805864E-03,-0.41657235E-03, 0.34132873E-03,
        -0.36924216E-03, 0.31897161E-03, 0.21379236E-03,-0.23536841E-03,
         0.12366446E-03,-0.71511604E-03,-0.62069530E-03, 0.27206284E-03,
         0.19964934E-03,-0.22521500E-03,-0.20587388E-03,-0.30473550E-03,
        -0.22151652E-03, 0.28515409E-03,-0.10068355E-02,-0.93903793E-04,
         0.23479102E-03, 0.47349671E-03, 0.30720688E-03,-0.38806928E-03,
         0.40128778E-03,-0.49354328E-03,-0.43919749E-03,-0.50272810E-03,
        -0.38997212E-03, 0.29083135E-03,-0.97034284E-03, 0.34454040E-03,
        -0.48676456E-03, 0.24929189E-03,-0.47664688E-03, 0.24478103E-03,
         0.54490403E-03, 0.42488208E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_qd_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x51
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[  8]    *x21*x31        
        +coeff[  9]*x11*x21    *x41    
        +coeff[ 10]*x13                
        +coeff[ 11]            *x42*x52
        +coeff[ 12]                *x54
        +coeff[ 13]*x11*x21*x32        
        +coeff[ 14]*x11    *x33        
        +coeff[ 15]*x11*x24            
        +coeff[ 16]*x11*x23*x31        
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 17]*x11*x21*x31*x42    
        +coeff[ 18]*x11*x22        *x52
        +coeff[ 19]*x12        *x42*x51
        +coeff[ 20]    *x23    *x42*x51
        +coeff[ 21]    *x22*x31*x42*x51
        +coeff[ 22]    *x22*x31*x41*x52
        +coeff[ 23]*x12*x21    *x41*x52
        +coeff[ 24]    *x22*x31*x41*x53
        +coeff[ 25]*x11*x23*x33        
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 26]*x11    *x33*x42*x51
        +coeff[ 27]*x11    *x31*x42*x53
        +coeff[ 28]*x11*x22        *x54
        +coeff[ 29]*x12*x22*x32    *x51
        +coeff[ 30]*x13*x21*x32*x41    
        +coeff[ 31]    *x23*x34*x41    
        +coeff[ 32]*x13    *x31*x42*x51
        +coeff[ 33]    *x22*x31*x44*x51
        +coeff[ 34]*x11                
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 35]    *x21        *x51
        +coeff[ 36]            *x41*x51
        +coeff[ 37]*x11    *x31        
        +coeff[ 38]*x11        *x41    
        +coeff[ 39]*x11            *x51
        +coeff[ 40]    *x21*x32        
        +coeff[ 41]    *x21*x31    *x51
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]            *x42*x51
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 44]    *x21        *x52
        +coeff[ 45]            *x41*x52
        +coeff[ 46]*x11*x22            
        +coeff[ 47]*x11*x21*x31        
        +coeff[ 48]*x12*x21            
        +coeff[ 49]*x12        *x41    
        +coeff[ 50]        *x34        
        +coeff[ 51]        *x33*x41    
        +coeff[ 52]    *x22    *x42    
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 53]            *x44    
        +coeff[ 54]            *x43*x51
        +coeff[ 55]    *x22        *x52
        +coeff[ 56]    *x21    *x41*x52
        +coeff[ 57]    *x21        *x53
        +coeff[ 58]        *x31    *x53
        +coeff[ 59]*x11*x22*x31        
        +coeff[ 60]*x11*x22    *x41    
        +coeff[ 61]*x11    *x32*x41    
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 62]*x11    *x31*x42    
        +coeff[ 63]*x11    *x31    *x52
        +coeff[ 64]*x12*x21*x31        
        +coeff[ 65]*x12    *x31    *x51
        +coeff[ 66]*x12        *x41*x51
        +coeff[ 67]    *x23*x32        
        +coeff[ 68]    *x21*x34        
        +coeff[ 69]*x13        *x41    
        +coeff[ 70]    *x21*x32*x42    
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 71]        *x33*x42    
        +coeff[ 72]    *x21*x31*x43    
        +coeff[ 73]    *x21    *x44    
        +coeff[ 74]        *x31*x44    
        +coeff[ 75]*x13            *x51
        +coeff[ 76]    *x22*x32    *x51
        +coeff[ 77]    *x21*x33    *x51
        +coeff[ 78]    *x23    *x41*x51
        +coeff[ 79]        *x33*x41*x51
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 80]    *x23        *x52
        +coeff[ 81]    *x22*x31    *x52
        +coeff[ 82]    *x21*x31*x41*x52
        +coeff[ 83]        *x32*x41*x52
        +coeff[ 84]    *x21    *x42*x52
        +coeff[ 85]            *x43*x52
        +coeff[ 86]    *x22        *x53
        +coeff[ 87]    *x21    *x41*x53
        +coeff[ 88]            *x42*x53
    ;
    v_l_e_qd_100                                  =v_l_e_qd_100                                  
        +coeff[ 89]            *x41*x54
        ;

    return v_l_e_qd_100                                  ;
}
