float ldelta_1100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1512663E-02;
    float xmin[10]={
        -0.55830E+00,-0.11746E+00,-0.49461E-01,-0.46628E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.46041E+00, 0.95126E-01, 0.47177E-01, 0.45610E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.23298322E-02, 0.45646586E-01,-0.15901549E-01,-0.46701054E-02,
         0.34348644E-01,-0.13716370E-01, 0.91224659E-03, 0.13899868E-02,
         0.87329150E-04, 0.41940254E-02,-0.44068568E-02, 0.52204044E-02,
        -0.12182804E-01, 0.75869071E-02, 0.23076860E-02,-0.32373359E-02,
         0.37345989E-02,-0.43908171E-02,-0.90287656E-04, 0.94546303E-02,
        -0.21667613E-03, 0.73725728E-05, 0.26305154E-03,-0.39658193E-02,
        -0.58865286E-02,-0.34050029E-02, 0.10103603E-01,-0.70812367E-02,
        -0.19937013E-03, 0.38009736E-03,-0.79803844E-03,-0.57002803E-03,
         0.11311201E-02,-0.25231189E-02,-0.86162014E-04, 0.13779438E-02,
         0.14511892E-02,-0.16247126E-03, 0.18653745E-03, 0.71106282E-04,
        -0.63417618E-04, 0.39324313E-03,-0.10599630E-03,-0.13069526E-03,
         0.20446649E-03,-0.35429999E-03,-0.73626026E-03, 0.64112362E-03,
        -0.34209460E-03, 0.61617017E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;

//                 function

    float v_ldelta                                  =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]*x12            
        +coeff[  3]    *x21        
        +coeff[  4]*x11*x21        
        +coeff[  5]    *x22        
        +coeff[  6]        *x32    
        +coeff[  7]        *x31*x41
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[  8]            *x42
        +coeff[  9]*x11    *x31*x41
        +coeff[ 10]    *x21*x31*x41
        +coeff[ 11]*x12*x21        
        +coeff[ 12]*x11*x22        
        +coeff[ 13]    *x23        
        +coeff[ 14]*x11    *x32    
        +coeff[ 15]    *x21*x32    
        +coeff[ 16]*x11        *x42
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 17]    *x21    *x42
        +coeff[ 18]        *x31    
        +coeff[ 19]*x11*x21*x31*x41
        +coeff[ 20]*x13        *x42
        +coeff[ 21]            *x41
        +coeff[ 22]*x11*x23        
        +coeff[ 23]*x12    *x31*x41
        +coeff[ 24]    *x22*x31*x41
        +coeff[ 25]*x12        *x42
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 26]*x11*x21    *x42
        +coeff[ 27]    *x22    *x42
        +coeff[ 28]        *x31*x43
        +coeff[ 29]*x12    *x31*x42
        +coeff[ 30]*x12*x22*x32    
        +coeff[ 31]*x12*x21    *x43
        +coeff[ 32]*x11*x22    *x43
        +coeff[ 33]    *x22*x31*x43
        +coeff[ 34]*x11    *x31*x44
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 35]*x14    *x31*x43
        +coeff[ 36]    *x23*x31*x44
        +coeff[ 37]*x11    *x31    
        +coeff[ 38]    *x21*x31    
        +coeff[ 39]        *x33    
        +coeff[ 40]            *x43
        +coeff[ 41]*x11*x21*x32    
        +coeff[ 42]        *x34    
        +coeff[ 43]*x13        *x41
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 44]*x11    *x32*x41
        +coeff[ 45]        *x33*x41
        +coeff[ 46]*x14*x21        
        +coeff[ 47]*x13*x22        
        +coeff[ 48]    *x21*x33*x41
        +coeff[ 49]*x12*x21    *x42
        ;

    return v_ldelta                                  ;
}
float ltheta_1100                                  (float *x,int m){
    int ncoeff= 14;
    float avdat=  0.1222233E-02;
    float xmin[10]={
        -0.55830E+00,-0.11746E+00,-0.49461E-01,-0.46628E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.46041E+00, 0.95126E-01, 0.47177E-01, 0.45610E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
         0.54843486E-02, 0.19438070E+00,-0.24539560E+00,-0.78323662E-01,
         0.94569409E-02,-0.22150490E-01, 0.13084258E-01,-0.73667020E-01,
         0.15185422E+00, 0.78922685E-03, 0.15074546E-03,-0.10763619E-02,
         0.73117288E-02,-0.87555451E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;

//                 function

    float v_ltheta                                  =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]    *x22        
        +coeff[  4]*x14            
        +coeff[  5]*x13*x21        
        +coeff[  6]*x12*x22        
        +coeff[  7]*x12            
    ;
    v_ltheta                                  =v_ltheta                                  
        +coeff[  8]*x11*x21        
        +coeff[  9]            *x42
        +coeff[ 10]        *x32    
        +coeff[ 11]    *x21*x32    
        +coeff[ 12]*x11    *x34    
        +coeff[ 13]    *x21*x34    
        ;

    return v_ltheta                                  ;
}
float lphi_1100                                    (float *x,int m){
    int ncoeff= 70;
    float avdat=  0.7993998E-04;
    float xmin[10]={
        -0.55830E+00,-0.11746E+00,-0.49461E-01,-0.46628E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.46041E+00, 0.95126E-01, 0.47177E-01, 0.45610E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 71]={
         0.65125083E-03,-0.16292844E-01,-0.30598072E-01,-0.59252977E-02,
         0.17744733E-01,-0.28238438E-01, 0.30477531E-01,-0.69236602E-02,
         0.23562215E-01, 0.47213474E-03,-0.77414582E-03, 0.47711853E-03,
        -0.41599199E-01, 0.10090114E+00,-0.62016748E-01,-0.13378405E-02,
         0.17199163E-03,-0.33956429E-02, 0.32591091E-02, 0.25392003E-01,
        -0.89657260E-03,-0.11687690E-02,-0.13570901E-02, 0.14121728E+00,
        -0.15170717E-02, 0.15184182E+00, 0.81940793E-01,-0.79398960E-01,
         0.20522432E-01,-0.75731024E-01, 0.91820851E-01,-0.37042134E-01,
         0.56933787E-01,-0.12807468E+00, 0.73176183E-01, 0.77584287E-03,
         0.80075151E-04, 0.92774579E-04,-0.12611357E-01,-0.83130399E-05,
        -0.48250832E-01,-0.13177699E+00, 0.37918057E-01,-0.73867105E-03,
        -0.96496049E-03,-0.62384409E+00, 0.94674462E+00,-0.61739773E+00,
         0.14036305E+00,-0.67551085E-02,-0.20359363E-02, 0.12878318E-02,
         0.65876171E-04, 0.52355979E-04,-0.49817510E-03, 0.17840305E-03,
         0.97685144E-03, 0.13271420E-01,-0.70786960E-02, 0.16895292E-01,
        -0.37709482E-01, 0.21379605E-01, 0.17756518E-01,-0.19523295E-01,
        -0.31481817E-01, 0.35784073E-01,-0.28550651E-01, 0.16457642E-02,
         0.31236371E-02,-0.31548343E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;

//                 function

    float v_lphi                                    =avdat
        +coeff[  0]                
        +coeff[  1]        *x31    
        +coeff[  2]            *x41
        +coeff[  3]*x11    *x31    
        +coeff[  4]    *x21*x31    
        +coeff[  5]*x11        *x41
        +coeff[  6]    *x21    *x41
        +coeff[  7]*x12        *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[  8]*x11*x21    *x41
        +coeff[  9]*x11            
        +coeff[ 10]    *x21        
        +coeff[ 11]        *x33*x42
        +coeff[ 12]*x12    *x31    
        +coeff[ 13]*x11*x21*x31    
        +coeff[ 14]    *x22*x31    
        +coeff[ 15]        *x32*x41
        +coeff[ 16]            *x43
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 17]*x11    *x31*x42
        +coeff[ 18]    *x21*x31*x42
        +coeff[ 19]*x11*x24    *x41
        +coeff[ 20]*x11    *x31*x44
        +coeff[ 21]        *x33    
        +coeff[ 22]        *x31*x42
        +coeff[ 23]*x12*x21    *x41
        +coeff[ 24]*x11    *x32*x41
        +coeff[ 25]*x14        *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 26]*x13*x22    *x41
        +coeff[ 27]*x12*x23    *x41
        +coeff[ 28]*x13        *x43
        +coeff[ 29]*x12*x21    *x43
        +coeff[ 30]*x11*x22    *x43
        +coeff[ 31]    *x23    *x43
        +coeff[ 32]*x14*x22    *x41
        +coeff[ 33]*x13*x23    *x41
        +coeff[ 34]*x12*x24    *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 35]*x12            
        +coeff[ 36]        *x32    
        +coeff[ 37]        *x31*x41
        +coeff[ 38]    *x22    *x41
        +coeff[ 39]    *x24        
        +coeff[ 40]*x13        *x41
        +coeff[ 41]*x11*x22    *x41
        +coeff[ 42]    *x23    *x41
        +coeff[ 43]    *x21    *x43
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 44]*x14    *x31    
        +coeff[ 45]*x13*x21    *x41
        +coeff[ 46]*x12*x22    *x41
        +coeff[ 47]*x11*x23    *x41
        +coeff[ 48]    *x24    *x41
        +coeff[ 49]*x12    *x31*x42
        +coeff[ 50]*x11*x21        
        +coeff[ 51]    *x22        
        +coeff[ 52]*x11    *x31*x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 53]*x11        *x42
        +coeff[ 54]*x11    *x33    
        +coeff[ 55]*x11        *x43
        +coeff[ 56]*x12*x22*x31    
        +coeff[ 57]*x11*x21*x31*x42
        +coeff[ 58]    *x22*x31*x42
        +coeff[ 59]*x12        *x43
        +coeff[ 60]*x11*x21    *x43
        +coeff[ 61]    *x22    *x43
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 62]*x13    *x33    
        +coeff[ 63]*x12*x21*x33    
        +coeff[ 64]*x11*x22*x33    
        +coeff[ 65]    *x23*x33    
        +coeff[ 66]*x14*x21    *x41
        +coeff[ 67]    *x21*x34*x41
        +coeff[ 68]*x13    *x31*x42
        +coeff[ 69]*x12*x21*x31*x42
        ;

    return v_lphi                                    ;
}
float ly00_1100                                    (float *x,int m){
    int ncoeff= 60;
    float avdat= -0.1314003E-03;
    float xmin[10]={
        -0.55830E+00,-0.11746E+00,-0.49461E-01,-0.46628E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.46041E+00, 0.95126E-01, 0.47177E-01, 0.45610E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 61]={
         0.92358078E-03,-0.51883597E-01, 0.39338082E-01, 0.68914421E-01,
        -0.11083671E+00, 0.31082461E-01,-0.74140392E-01,-0.59575699E-02,
         0.21402302E+00,-0.51404691E+00, 0.29496491E+00, 0.34270226E-02,
         0.30566163E-02, 0.16273010E-02,-0.67360565E-01, 0.37885204E-01,
        -0.31608625E-02,-0.40680522E-03, 0.52546286E-02,-0.48553135E-03,
         0.42453691E-03,-0.14155692E-02,-0.19734844E-02, 0.11829723E+00,
        -0.29549581E+00, 0.18571086E+00, 0.32414002E-02, 0.46693208E-02,
         0.22209701E-02,-0.19286938E+00,-0.96753812E+00, 0.42127463E+00,
         0.33532497E-02, 0.16553438E-03, 0.13408500E-01,-0.27021611E-03,
         0.14230491E-01, 0.74643964E+00, 0.41048601E-02, 0.76114987E-02,
        -0.73637394E-02,-0.81249205E-02,-0.24373458E-03,-0.11318596E-03,
        -0.11413403E-01, 0.80822436E-02,-0.37602365E-01, 0.66634238E-01,
        -0.53130671E-01, 0.16025247E-01, 0.22968042E-02,-0.26307847E-01,
         0.55992022E-01, 0.60973624E-02,-0.55958680E-02,-0.48009053E-03,
        -0.26974243E-02,-0.54709888E-02,-0.12446826E-03, 0.22060903E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;

//                 function

    float v_ly00                                    =avdat
        +coeff[  0]                
        +coeff[  1]        *x31    
        +coeff[  2]            *x41
        +coeff[  3]*x11    *x31    
        +coeff[  4]    *x21*x31    
        +coeff[  5]*x11        *x41
        +coeff[  6]    *x21    *x41
        +coeff[  7]    *x23        
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[  8]*x12        *x41
        +coeff[  9]*x11*x21    *x41
        +coeff[ 10]    *x22    *x41
        +coeff[ 11]    *x21        
        +coeff[ 12]*x13            
        +coeff[ 13]            *x43
        +coeff[ 14]*x11*x23*x31    
        +coeff[ 15]    *x24*x31    
        +coeff[ 16]*x12    *x33    
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 17]*x11*x21*x33    
        +coeff[ 18]    *x22*x33    
        +coeff[ 19]*x12    *x32*x41
        +coeff[ 20]        *x34*x41
        +coeff[ 21]*x11    *x34*x41
        +coeff[ 22]*x11            
        +coeff[ 23]*x12    *x31    
        +coeff[ 24]*x11*x21*x31    
        +coeff[ 25]    *x22*x31    
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 26]        *x33    
        +coeff[ 27]        *x32*x41
        +coeff[ 28]        *x31*x42
        +coeff[ 29]*x13        *x41
        +coeff[ 30]*x11*x22    *x41
        +coeff[ 31]    *x23    *x41
        +coeff[ 32]*x11    *x31*x42
        +coeff[ 33]*x11        *x43
        +coeff[ 34]*x11*x21        
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 35]        *x32    
        +coeff[ 36]*x11*x22        
        +coeff[ 37]*x12*x21    *x41
        +coeff[ 38]*x11    *x32*x41
        +coeff[ 39]*x14        *x41
        +coeff[ 40]*x13*x21    *x41
        +coeff[ 41]    *x22        
        +coeff[ 42]        *x31*x41
        +coeff[ 43]            *x42
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 44]*x12*x21        
        +coeff[ 45]*x14            
        +coeff[ 46]*x13*x21        
        +coeff[ 47]*x12*x22        
        +coeff[ 48]*x11*x23        
        +coeff[ 49]    *x24        
        +coeff[ 50]    *x21    *x43
        +coeff[ 51]*x14    *x31    
        +coeff[ 52]*x13*x21*x31    
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 53]*x12        *x43
        +coeff[ 54]*x11*x21    *x43
        +coeff[ 55]*x11    *x31*x43
        +coeff[ 56]*x11    *x33*x42
        +coeff[ 57]*x12            
        +coeff[ 58]*x11        *x42
        +coeff[ 59]*x12*x21*x31    
        ;

    return v_ly00                                    ;
}


float ldelta_400                                  (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1727191E-02;
    float xmin[10]={
        -0.55111E+00,-0.11582E+00,-0.52756E-01,-0.43928E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.45493E+00, 0.94886E-01, 0.52619E-01, 0.43213E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.20595840E-02, 0.46708986E-01,-0.59844740E-02,-0.14954365E-01,
         0.32851018E-01,-0.13339289E-01, 0.87252510E-03, 0.13863195E-02,
         0.35859199E-03, 0.75573302E-02,-0.15846254E-01, 0.86340252E-02,
         0.25411283E-02, 0.44073355E-02,-0.50626947E-02,-0.22295963E-02,
         0.55562641E-03, 0.55171410E-03,-0.30037365E-02,-0.14095737E-03,
        -0.13180500E-01, 0.15384046E-02,-0.10513986E-03,-0.61231847E-04,
         0.42672195E-02,-0.50911545E-02, 0.24589308E-03, 0.75667765E-03,
         0.32682274E-02,-0.60423273E-02, 0.22733929E-02, 0.38353849E-01,
        -0.36816943E-01, 0.13752133E-01,-0.40033553E-03,-0.23326703E-03,
        -0.55937184E-03, 0.24444205E-02, 0.72657769E-02,-0.46226371E-03,
        -0.21843957E-02,-0.43497630E-02,-0.19656988E-02, 0.18970929E-02,
         0.15591780E-02, 0.73986885E-03,-0.61457143E-02,-0.23203590E-02,
         0.68465434E-02, 0.34092058E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;

//                 function

    float v_ldelta                                  =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]*x12            
        +coeff[  4]*x11*x21        
        +coeff[  5]    *x22        
        +coeff[  6]        *x32    
        +coeff[  7]        *x31*x41
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[  8]            *x44
        +coeff[  9]*x12*x21        
        +coeff[ 10]*x11*x22        
        +coeff[ 11]    *x23        
        +coeff[ 12]*x11    *x32    
        +coeff[ 13]*x11        *x42
        +coeff[ 14]    *x21    *x42
        +coeff[ 15]*x11*x22*x32    
        +coeff[ 16]*x11    *x33*x41
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 17]    *x21*x33*x41
        +coeff[ 18]    *x21*x32    
        +coeff[ 19]        *x32*x42
        +coeff[ 20]*x14*x21        
        +coeff[ 21]        *x34*x42
        +coeff[ 22]            *x42
        +coeff[ 23]        *x33    
        +coeff[ 24]*x11    *x31*x41
        +coeff[ 25]    *x21*x31*x41
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 26]*x14            
        +coeff[ 27]    *x22*x32    
        +coeff[ 28]*x11*x21*x31*x41
        +coeff[ 29]    *x22*x31*x41
        +coeff[ 30]    *x22    *x42
        +coeff[ 31]*x13*x22        
        +coeff[ 32]*x12*x23        
        +coeff[ 33]*x11*x24        
        +coeff[ 34]*x13        *x42
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 35]*x14    *x32    
        +coeff[ 36]*x12    *x34    
        +coeff[ 37]*x12*x22*x31*x41
        +coeff[ 38]    *x22*x33*x41
        +coeff[ 39]*x14        *x42
        +coeff[ 40]    *x24    *x42
        +coeff[ 41]    *x22    *x44
        +coeff[ 42]*x14*x23        
        +coeff[ 43]*x13*x22*x32    
    ;
    v_ldelta                                  =v_ldelta                                  
        +coeff[ 44]*x11    *x34*x42
        +coeff[ 45]*x13        *x44
        +coeff[ 46]*x14    *x33*x41
        +coeff[ 47]*x14        *x44
        +coeff[ 48]*x13*x21    *x44
        +coeff[ 49]*x14*x22*x33    
        ;

    return v_ldelta                                  ;
}
float ltheta_400                                  (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.8608627E-03;
    float xmin[10]={
        -0.55111E+00,-0.11582E+00,-0.52756E-01,-0.43928E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.45493E+00, 0.94886E-01, 0.52619E-01, 0.43213E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.44659069E-02, 0.18485357E+00,-0.23566477E+00,-0.71302101E-01,
         0.34631314E-02,-0.73117153E-02, 0.41013467E-02,-0.66438712E-01,
         0.13737424E+00, 0.66699024E-03, 0.15591464E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;

//                 function

    float v_ltheta                                  =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]    *x22        
        +coeff[  4]*x14            
        +coeff[  5]*x13*x21        
        +coeff[  6]*x12*x22        
        +coeff[  7]*x12            
    ;
    v_ltheta                                  =v_ltheta                                  
        +coeff[  8]*x11*x21        
        +coeff[  9]            *x42
        +coeff[ 10]    *x22*x32    
        ;

    return v_ltheta                                  ;
}
float lphi_400                                    (float *x,int m){
    int ncoeff= 70;
    float avdat= -0.1489032E-03;
    float xmin[10]={
        -0.55111E+00,-0.11582E+00,-0.52756E-01,-0.43928E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.45493E+00, 0.94886E-01, 0.52619E-01, 0.43213E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 71]={
         0.43956915E-03,-0.17007986E-01,-0.31796832E-01,-0.75536976E-02,
         0.20043427E-01,-0.27600139E-01, 0.31457301E-01,-0.10470341E-01,
         0.32477468E-01,-0.15951542E-02,-0.30063434E-01,-0.19553773E-01,
        -0.22517999E-02,-0.11917323E-03,-0.19118980E+00, 0.12508292E+00,
         0.54751924E-03,-0.43919184E-02,-0.11023528E-03,-0.91303089E-04,
        -0.38256038E-01, 0.94224200E-01,-0.58812361E-01,-0.12460038E-02,
         0.10003120E+00, 0.15998013E-01,-0.10921948E-02,-0.82419743E-03,
        -0.24599323E-03,-0.10115214E-01, 0.30535724E-01,-0.32666121E-01,
         0.11712422E-01, 0.45465739E-02, 0.25204758E-03,-0.29055538E-03,
        -0.56942867E-04,-0.37724253E-01,-0.79140618E-01, 0.12218452E+00,
        -0.49157900E+00, 0.75634331E+00,-0.52287138E+00, 0.13631339E+00,
        -0.64432169E-02, 0.14592006E-01,-0.91654500E-02, 0.35390032E-04,
         0.40890429E-01,-0.13525333E+00, 0.15069667E+00,-0.56393564E-01,
         0.19292177E-02,-0.19425990E-02,-0.33208624E-01, 0.12947863E+00,
         0.13713872E-01,-0.29809359E-01, 0.16677691E-01,-0.28835455E-03,
         0.14776570E-02, 0.16137109E-02,-0.37380585E-02,-0.62643428E-03,
         0.50223372E-02,-0.53477464E-02,-0.14303421E-02, 0.16149937E-02,
         0.26861500E-04,-0.11153861E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;

//                 function

    float v_lphi                                    =avdat
        +coeff[  0]                
        +coeff[  1]        *x31    
        +coeff[  2]            *x41
        +coeff[  3]*x11    *x31    
        +coeff[  4]    *x21*x31    
        +coeff[  5]*x11        *x41
        +coeff[  6]    *x21    *x41
        +coeff[  7]*x12        *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[  8]*x11*x21    *x41
        +coeff[  9]        *x31*x42
        +coeff[ 10]    *x24*x31    
        +coeff[ 11]    *x22    *x41
        +coeff[ 12]        *x32*x41
        +coeff[ 13]            *x43
        +coeff[ 14]*x12*x22*x31    
        +coeff[ 15]*x11*x23*x31    
        +coeff[ 16]        *x34*x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 17]*x11    *x31*x44
        +coeff[ 18]*x14*x22*x31    
        +coeff[ 19]*x13*x24    *x41
        +coeff[ 20]*x12    *x31    
        +coeff[ 21]*x11*x21*x31    
        +coeff[ 22]    *x22*x31    
        +coeff[ 23]        *x33    
        +coeff[ 24]*x12*x21    *x41
        +coeff[ 25]    *x23    *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 26]*x11    *x32*x41
        +coeff[ 27]    *x21*x31*x42
        +coeff[ 28]*x11        *x43
        +coeff[ 29]*x14*x21    *x41
        +coeff[ 30]*x13*x22    *x41
        +coeff[ 31]*x12*x23    *x41
        +coeff[ 32]*x11*x24    *x41
        +coeff[ 33]    *x21*x31*x44
        +coeff[ 34]*x11            
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 35]    *x21        
        +coeff[ 36]*x12            
        +coeff[ 37]*x13        *x41
        +coeff[ 38]*x11*x22    *x41
        +coeff[ 39]*x14        *x41
        +coeff[ 40]*x13*x21    *x41
        +coeff[ 41]*x12*x22    *x41
        +coeff[ 42]*x11*x23    *x41
        +coeff[ 43]    *x24    *x41
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 44]*x12    *x31*x42
        +coeff[ 45]*x11*x21*x31*x42
        +coeff[ 46]    *x22*x31*x42
        +coeff[ 47]    *x22        
        +coeff[ 48]*x13    *x31    
        +coeff[ 49]*x12*x21*x31    
        +coeff[ 50]*x11*x22*x31    
        +coeff[ 51]    *x23*x31    
        +coeff[ 52]*x11    *x33    
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 53]    *x21*x33    
        +coeff[ 54]*x14    *x31    
        +coeff[ 55]*x13*x21*x31    
        +coeff[ 56]*x12        *x43
        +coeff[ 57]*x11*x21    *x43
        +coeff[ 58]    *x22    *x43
        +coeff[ 59]        *x31*x44
        +coeff[ 60]*x11    *x34*x41
        +coeff[ 61]*x12*x21    *x43
    ;
    v_lphi                                    =v_lphi                                    
        +coeff[ 62]*x11*x22    *x43
        +coeff[ 63]    *x21*x32*x43
        +coeff[ 64]*x14    *x32*x41
        +coeff[ 65]*x13*x21*x32*x41
        +coeff[ 66]*x13    *x34*x41
        +coeff[ 67]*x11*x24    *x43
        +coeff[ 68]        *x31*x41
        +coeff[ 69]            *x42
        ;

    return v_lphi                                    ;
}
float ly00_400                                    (float *x,int m){
    int ncoeff= 60;
    float avdat=  0.8769109E-05;
    float xmin[10]={
        -0.55111E+00,-0.11582E+00,-0.52756E-01,-0.43928E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.45493E+00, 0.94886E-01, 0.52619E-01, 0.43213E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 61]={
        -0.20250729E-03,-0.55740509E-01, 0.30444723E-01, 0.65633617E-01,
        -0.10803697E+00, 0.33259086E-01,-0.79540357E-01, 0.22175787E+00,
        -0.53677428E+00, 0.31216115E+00,-0.26000466E-03, 0.17903738E+00,
         0.17360919E-02,-0.93595427E-03, 0.16907358E-02,-0.91481063E-03,
         0.65119134E-03, 0.10830831E+00,-0.27769783E+00, 0.37666427E-02,
         0.76305619E-02, 0.34447007E-02,-0.16062076E+00, 0.62000936E+00,
        -0.81148452E+00, 0.35750875E+00, 0.10406887E-01, 0.17738628E-02,
         0.46609784E-03,-0.13704597E-02, 0.14732700E-01, 0.58628153E-02,
        -0.19062751E-02, 0.46295128E-02,-0.27219167E-02, 0.24765080E-02,
        -0.74284654E-02, 0.25032365E-03,-0.13349330E+00, 0.60241210E+00,
        -0.28328463E-01,-0.46357671E-02, 0.26370781E-01,-0.33915680E-01,
        -0.51088477E-02,-0.22884851E-01, 0.55917028E-01,-0.34046683E-01,
        -0.37540984E-02, 0.49118735E-02,-0.99757075E+00, 0.72640574E+00,
        -0.19749637E+00,-0.59255108E-03,-0.27965263E-02, 0.14319492E-01,
        -0.41392829E-01, 0.28128937E-01, 0.98835165E-03, 0.14914532E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x14 = x13*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;

//                 function

    float v_ly00                                    =avdat
        +coeff[  0]                
        +coeff[  1]        *x31    
        +coeff[  2]            *x41
        +coeff[  3]*x11    *x31    
        +coeff[  4]    *x21*x31    
        +coeff[  5]*x11        *x41
        +coeff[  6]    *x21    *x41
        +coeff[  7]*x12        *x41
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[  8]*x11*x21    *x41
        +coeff[  9]    *x22    *x41
        +coeff[ 10]*x11            
        +coeff[ 11]    *x22*x31    
        +coeff[ 12]            *x43
        +coeff[ 13]*x14    *x31    
        +coeff[ 14]*x11*x23*x31    
        +coeff[ 15]    *x24*x31    
        +coeff[ 16]    *x21        
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 17]*x12    *x31    
        +coeff[ 18]*x11*x21*x31    
        +coeff[ 19]        *x33    
        +coeff[ 20]        *x32*x41
        +coeff[ 21]        *x31*x42
        +coeff[ 22]*x13        *x41
        +coeff[ 23]*x12*x21    *x41
        +coeff[ 24]*x11*x22    *x41
        +coeff[ 25]    *x23    *x41
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 26]*x11    *x31*x42
        +coeff[ 27]*x11        *x43
        +coeff[ 28]*x13    *x32*x41
        +coeff[ 29]*x11    *x32*x41
        +coeff[ 30]*x12        *x43
        +coeff[ 31]*x14*x21    *x41
        +coeff[ 32]*x12            
        +coeff[ 33]*x11*x21        
        +coeff[ 34]    *x22        
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 35]*x11    *x33    
        +coeff[ 36]    *x21*x31*x42
        +coeff[ 37]    *x21    *x43
        +coeff[ 38]*x14        *x41
        +coeff[ 39]*x13*x21    *x41
        +coeff[ 40]*x11*x21    *x43
        +coeff[ 41]*x13*x22    *x41
        +coeff[ 42]*x11    *x34*x41
        +coeff[ 43]    *x21*x34*x41
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 44]*x11    *x33*x42
        +coeff[ 45]*x12*x21*x31    
        +coeff[ 46]*x11*x22*x31    
        +coeff[ 47]    *x23*x31    
        +coeff[ 48]    *x21*x33    
        +coeff[ 49]    *x21*x32*x41
        +coeff[ 50]*x12*x22    *x41
        +coeff[ 51]*x11*x23    *x41
        +coeff[ 52]    *x24    *x41
    ;
    v_ly00                                    =v_ly00                                    
        +coeff[ 53]*x12    *x32*x41
        +coeff[ 54]        *x34*x41
        +coeff[ 55]*x12    *x31*x42
        +coeff[ 56]*x11*x21*x31*x42
        +coeff[ 57]    *x22*x31*x42
        +coeff[ 58]        *x33*x42
        +coeff[ 59]    *x22    *x43
        ;

    return v_ly00                                    ;
}
