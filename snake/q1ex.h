float x_e_q1ex_1200                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.7108546E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.72156201E-03, 0.30040939E-02, 0.12367453E+00, 0.10900414E-02,
        -0.14273374E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_1200                                ;
}
float t_e_q1ex_1200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4715351E-05;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47083972E-05,-0.19499180E-02,-0.43878556E-03, 0.22366527E-02,
        -0.69514877E-03,-0.50010183E-03,-0.10465736E-02,-0.10062471E-02,
         0.72707495E-04,-0.82060309E-04,-0.10512501E-02,-0.10940644E-02,
        -0.99757768E-03,-0.58099959E-04,-0.51215844E-04, 0.93271505E-04,
        -0.52554515E-03,-0.76922454E-03, 0.95004718E-04,-0.31002404E-04,
        -0.34799331E-03, 0.28068989E-04,-0.15513582E-04, 0.34147033E-04,
         0.53711028E-04,-0.69664246E-04,-0.45612422E-04, 0.79288176E-04,
        -0.81924190E-05, 0.10974467E-04,-0.14239536E-04,-0.20392323E-04,
         0.44035067E-04,-0.46540235E-05, 0.18254079E-05, 0.23647310E-05,
         0.32963569E-05, 0.27671761E-05, 0.44937392E-05, 0.25869722E-05,
        -0.16473650E-04,-0.78517987E-05,-0.58937003E-05, 0.14262853E-04,
         0.75989269E-05,-0.45514371E-05,-0.28576142E-05, 0.58108903E-05,
         0.62505324E-05, 0.48600650E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_1200                                =v_t_e_q1ex_1200                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21*x33*x41    
    ;
    v_t_e_q1ex_1200                                =v_t_e_q1ex_1200                                
        +coeff[ 17]    *x23    *x42    
        +coeff[ 18]    *x21*x32*x42*x51
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]    *x23*x32        
        +coeff[ 21]    *x23    *x42*x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]*x11*x22*x31*x41    
    ;
    v_t_e_q1ex_1200                                =v_t_e_q1ex_1200                                
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]    *x21*x31*x43*x51
        +coeff[ 28]*x11            *x52
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x11*x21*x32*x41    
        +coeff[ 31]*x11    *x32*x42    
        +coeff[ 32]*x13*x21    *x42    
        +coeff[ 33]*x11*x21            
        +coeff[ 34]*x11        *x41    
    ;
    v_t_e_q1ex_1200                                =v_t_e_q1ex_1200                                
        +coeff[ 35]    *x22*x31        
        +coeff[ 36]*x11*x21        *x51
        +coeff[ 37]*x13    *x31        
        +coeff[ 38]*x11*x21*x31*x41    
        +coeff[ 39]        *x33*x41    
        +coeff[ 40]*x11*x21    *x42    
        +coeff[ 41]*x12*x21        *x51
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]    *x23        *x51
    ;
    v_t_e_q1ex_1200                                =v_t_e_q1ex_1200                                
        +coeff[ 44]    *x22*x31    *x51
        +coeff[ 45]*x11    *x32    *x51
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]*x11    *x31*x41*x51
        +coeff[ 48]*x11        *x42*x51
        +coeff[ 49]*x11*x21        *x52
        ;

    return v_t_e_q1ex_1200                                ;
}
float y_e_q1ex_1200                                (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.1267878E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.17032822E-03, 0.91404818E-01, 0.68608575E-01,-0.63815399E-03,
        -0.61040663E-03, 0.10748851E-03,-0.38092920E-04,-0.12697249E-03,
         0.78619669E-04, 0.63352811E-04, 0.28294593E-04,-0.14942289E-03,
         0.61365427E-05,-0.16136348E-04, 0.23922834E-04,-0.12971382E-03,
        -0.30315219E-04, 0.10586390E-04, 0.13017363E-05,-0.17461985E-04,
        -0.15421947E-04,-0.11707226E-04, 0.12896086E-04,-0.35786037E-04,
        -0.26792528E-04,-0.87793975E-04,-0.21060110E-04,-0.41113421E-04,
        -0.11346810E-04,-0.15832722E-04, 0.21413904E-04,-0.10724241E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_1200                                =v_y_e_q1ex_1200                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x31*x44    
    ;
    v_y_e_q1ex_1200                                =v_y_e_q1ex_1200                                
        +coeff[ 17]            *x42*x51
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]        *x32*x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]        *x32*x43    
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1ex_1200                                =v_y_e_q1ex_1200                                
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11    *x33*x41    
        +coeff[ 29]    *x21*x33*x42    
        +coeff[ 30]*x11*x21    *x41*x52
        +coeff[ 31]*x11    *x34*x41    
        ;

    return v_y_e_q1ex_1200                                ;
}
float p_e_q1ex_1200                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.4283190E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.74764052E-04, 0.31183761E-01, 0.65216705E-01,-0.13619404E-02,
        -0.15231221E-02, 0.96746435E-03, 0.66329003E-03, 0.66362327E-03,
         0.46068584E-03, 0.29652860E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_1200                                =v_p_e_q1ex_1200                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_1200                                ;
}
float l_e_q1ex_1200                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1704642E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39991E-02, 0.60069E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16516260E-02,-0.27803392E-02,-0.12206914E-02,-0.16515998E-02,
        -0.15718754E-02,-0.46543655E-03,-0.90497185E-03, 0.26968692E-02,
         0.87533356E-03,-0.32131084E-04, 0.25810019E-04,-0.20621653E-03,
         0.14455181E-03,-0.57470595E-03,-0.18575121E-03,-0.94175775E-03,
         0.19254968E-03,-0.89769613E-03, 0.38104338E-03, 0.27226479E-03,
        -0.37203758E-03,-0.21297352E-02, 0.49683219E-03,-0.45218767E-03,
         0.29930894E-03,-0.24358296E-04,-0.61001995E-03,-0.45269442E-03,
         0.46453162E-03, 0.12103704E-03, 0.33263100E-03,-0.28307832E-04,
         0.51772094E-03, 0.15293303E-02, 0.56471916E-04,-0.14099176E-02,
         0.14364108E-03, 0.13864239E-02, 0.24780343E-03,-0.60312182E-03,
        -0.10664251E-02, 0.19878952E-02, 0.69110258E-03, 0.37124245E-02,
        -0.57314313E-03,-0.12641326E-02, 0.15999128E-03, 0.73844666E-03,
         0.61476574E-03,-0.78837336E-04, 0.15772831E-02, 0.54583495E-03,
        -0.76846266E-03,-0.96253509E-03, 0.13182550E-02, 0.87039143E-05,
         0.50919526E-03, 0.40253578E-03,-0.14317865E-02, 0.11581586E-02,
        -0.10095007E-02,-0.51688508E-03,-0.18112714E-03,-0.72998478E-03,
         0.18380678E-02, 0.13704873E-02,-0.11997811E-02, 0.35152357E-03,
        -0.73749694E-03,-0.99995802E-03, 0.98580366E-03,-0.58543123E-03,
         0.79032005E-03,-0.13150198E-02, 0.57256414E-03, 0.11680570E-02,
         0.81380410E-03, 0.12362575E-02,-0.32179812E-02, 0.51815307E-03,
        -0.95798145E-03, 0.97064412E-03,-0.18962591E-02, 0.12619339E-03,
         0.34112345E-04,-0.14652744E-03, 0.69385060E-04, 0.55072738E-04,
        -0.43762085E-03,-0.85812375E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x32        
        +coeff[  5]        *x32*x41*x51
        +coeff[  6]*x11*x21*x32    *x51
        +coeff[  7]    *x24    *x41*x51
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[  8]*x11*x23*x32        
        +coeff[  9]*x11*x23        *x52
        +coeff[ 10]            *x41    
        +coeff[ 11]        *x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]    *x21    *x41*x51
        +coeff[ 14]            *x41*x52
        +coeff[ 15]*x11    *x32        
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 17]    *x24            
        +coeff[ 18]        *x32*x42    
        +coeff[ 19]        *x31*x43    
        +coeff[ 20]            *x44    
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]            *x41*x53
        +coeff[ 23]*x11    *x32*x41    
        +coeff[ 24]*x11        *x43    
        +coeff[ 25]*x11    *x32    *x51
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 26]*x11    *x31*x41*x51
        +coeff[ 27]*x11*x21        *x52
        +coeff[ 28]*x12    *x31*x41    
        +coeff[ 29]*x12        *x41*x51
        +coeff[ 30]    *x21*x33*x41    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x23    *x41*x51
        +coeff[ 33]    *x22*x31    *x52
        +coeff[ 34]        *x33    *x52
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 35]    *x21*x31*x41*x52
        +coeff[ 36]    *x22        *x53
        +coeff[ 37]*x11    *x34        
        +coeff[ 38]*x11*x22    *x42    
        +coeff[ 39]*x12    *x32*x41    
        +coeff[ 40]*x12*x22        *x51
        +coeff[ 41]    *x24*x32        
        +coeff[ 42]    *x22*x33    *x51
        +coeff[ 43]    *x22*x32*x41*x51
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 44]    *x22*x31*x42*x51
        +coeff[ 45]        *x31*x44*x51
        +coeff[ 46]*x13            *x52
        +coeff[ 47]    *x22*x31*x41*x52
        +coeff[ 48]    *x21*x31*x42*x52
        +coeff[ 49]    *x21*x31*x41*x53
        +coeff[ 50]        *x31*x42*x53
        +coeff[ 51]*x11    *x33*x42    
        +coeff[ 52]*x11    *x32*x42*x51
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 53]*x11*x22*x31    *x52
        +coeff[ 54]*x11    *x31*x41*x53
        +coeff[ 55]*x12*x24            
        +coeff[ 56]*x12*x21*x32*x41    
        +coeff[ 57]*x12    *x33    *x51
        +coeff[ 58]*x12    *x32*x41*x51
        +coeff[ 59]*x12*x22        *x52
        +coeff[ 60]*x12    *x31    *x53
        +coeff[ 61]*x12            *x54
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 62]    *x24*x33        
        +coeff[ 63]    *x24*x31*x41*x51
        +coeff[ 64]    *x21*x33*x41*x52
        +coeff[ 65]        *x34*x41*x52
        +coeff[ 66]    *x22*x31*x42*x52
        +coeff[ 67]        *x31*x43*x53
        +coeff[ 68]        *x33    *x54
        +coeff[ 69]        *x32*x41*x54
        +coeff[ 70]*x12*x23*x32        
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 71]*x12*x23    *x42    
        +coeff[ 72]*x12*x21*x31*x43    
        +coeff[ 73]*x12    *x33*x41*x51
        +coeff[ 74]*x12*x21    *x43*x51
        +coeff[ 75]*x12    *x31*x43*x51
        +coeff[ 76]*x12*x22    *x41*x52
        +coeff[ 77]*x12*x22        *x53
        +coeff[ 78]    *x24*x32*x41*x51
        +coeff[ 79]*x13*x21    *x42*x51
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 80]    *x23*x32*x42*x51
        +coeff[ 81]    *x21*x33*x43*x51
        +coeff[ 82]    *x23*x31*x41*x53
        +coeff[ 83]        *x31        
        +coeff[ 84]                *x51
        +coeff[ 85]            *x41*x51
        +coeff[ 86]                *x52
        +coeff[ 87]*x11    *x31        
        +coeff[ 88]    *x22*x31        
    ;
    v_l_e_q1ex_1200                                =v_l_e_q1ex_1200                                
        +coeff[ 89]    *x21*x32        
        ;

    return v_l_e_q1ex_1200                                ;
}
float x_e_q1ex_1100                                (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.5414895E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.52745605E-03, 0.12367247E+00,-0.14750407E-05, 0.30057849E-02,
         0.10877346E-02,-0.14496737E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_1100                                ;
}
float t_e_q1ex_1100                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5807095E-05;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.66473858E-05,-0.19510161E-02,-0.43929467E-03, 0.22426904E-02,
        -0.69137540E-03,-0.49572007E-03,-0.10524654E-02,-0.99360174E-03,
         0.69954564E-04,-0.10585928E-02,-0.77372172E-03,-0.10798463E-02,
        -0.98546629E-03,-0.52223379E-04,-0.32797663E-04,-0.27947257E-04,
        -0.76882388E-04, 0.10942652E-03,-0.34683666E-03,-0.49934810E-03,
        -0.14584753E-04, 0.67920802E-04, 0.26717516E-04,-0.84767955E-04,
        -0.66572880E-04,-0.26553932E-05,-0.73895485E-05,-0.56501349E-05,
         0.10553231E-04, 0.25300765E-05, 0.39259685E-04,-0.26838770E-04,
        -0.32728443E-04,-0.31596344E-04, 0.84823523E-05,-0.23100272E-04,
        -0.12765033E-04,-0.26185899E-04, 0.28949433E-04, 0.44919281E-04,
         0.43266024E-04,-0.10499431E-04, 0.25493127E-05,-0.38065186E-05,
        -0.27026488E-05,-0.60146585E-05, 0.14833852E-05,-0.66355744E-06,
        -0.21354169E-05, 0.45888546E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_1100                                =v_t_e_q1ex_1100                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_q1ex_1100                                =v_t_e_q1ex_1100                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]*x11*x22    *x42    
        +coeff[ 25]*x11*x21            
    ;
    v_t_e_q1ex_1100                                =v_t_e_q1ex_1100                                
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]*x11*x23            
        +coeff[ 29]*x11*x22        *x51
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11    *x32*x42    
        +coeff[ 33]*x11    *x31*x43    
        +coeff[ 34]    *x21*x33    *x51
    ;
    v_t_e_q1ex_1100                                =v_t_e_q1ex_1100                                
        +coeff[ 35]*x11*x21*x31*x41*x51
        +coeff[ 36]*x11*x21    *x42*x51
        +coeff[ 37]    *x21    *x42*x52
        +coeff[ 38]*x11*x22    *x42*x51
        +coeff[ 39]    *x23    *x42*x51
        +coeff[ 40]    *x21*x31*x43*x51
        +coeff[ 41]    *x22*x31    *x53
        +coeff[ 42]                *x52
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_q1ex_1100                                =v_t_e_q1ex_1100                                
        +coeff[ 44]*x12    *x31        
        +coeff[ 45]    *x22*x31        
        +coeff[ 46]        *x33        
        +coeff[ 47]*x12        *x41    
        +coeff[ 48]    *x22    *x41    
        +coeff[ 49]        *x31*x42    
        ;

    return v_t_e_q1ex_1100                                ;
}
float y_e_q1ex_1100                                (float *x,int m){
    int ncoeff= 27;
    float avdat=  0.8676103E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 28]={
        -0.89225493E-03, 0.91424286E-01, 0.68628967E-01,-0.63829223E-03,
        -0.61353942E-03, 0.97575168E-04,-0.47960140E-04,-0.13915217E-03,
         0.56218658E-04, 0.18373388E-04, 0.19923060E-04, 0.16113776E-04,
        -0.13097990E-03,-0.20056339E-04,-0.12585786E-03,-0.11412019E-03,
        -0.42999854E-04, 0.34190529E-04,-0.70291658E-05, 0.17615099E-04,
        -0.13631757E-04, 0.14878000E-04, 0.31576634E-04,-0.12974641E-04,
        -0.26059353E-04, 0.42288277E-04,-0.27416883E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_1100                                =v_y_e_q1ex_1100                                
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]        *x32*x43    
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x22*x31*x42    
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1ex_1100                                =v_y_e_q1ex_1100                                
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x31*x42*x52
        +coeff[ 23]            *x45*x51
        +coeff[ 24]*x12*x22    *x41    
        +coeff[ 25]    *x22    *x41*x52
    ;
    v_y_e_q1ex_1100                                =v_y_e_q1ex_1100                                
        +coeff[ 26]    *x22    *x45    
        ;

    return v_y_e_q1ex_1100                                ;
}
float p_e_q1ex_1100                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.4766099E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.49691764E-03, 0.31190699E-01, 0.65223716E-01,-0.13639304E-02,
        -0.15241567E-02, 0.96916239E-03, 0.66347112E-03, 0.65641047E-03,
         0.46214240E-03, 0.28924717E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_1100                                =v_p_e_q1ex_1100                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_1100                                ;
}
float l_e_q1ex_1100                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1676580E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16770776E-02,-0.36631525E-02,-0.10614564E-02,-0.15780044E-02,
        -0.12121328E-03, 0.19737897E-02, 0.85765314E-04, 0.14429151E-03,
         0.10840651E-03,-0.16597696E-04,-0.15850719E-03, 0.16298958E-06,
        -0.11021679E-03,-0.13779661E-03, 0.22652831E-03, 0.17383638E-03,
         0.97105774E-04, 0.23646477E-03,-0.34960645E-03, 0.99910283E-03,
         0.34617310E-03, 0.28913279E-03, 0.54891283E-04,-0.39628937E-03,
        -0.43693712E-03, 0.84524264E-03, 0.10175797E-03,-0.70321368E-03,
        -0.44720541E-03,-0.25733659E-03, 0.20413287E-02, 0.85891871E-03,
         0.18626673E-03,-0.42845745E-03, 0.54002361E-03,-0.73781138E-03,
        -0.48243074E-03, 0.69061888E-03, 0.49686641E-03,-0.15776856E-02,
         0.40057508E-03,-0.36261207E-03, 0.46360638E-03,-0.86468575E-03,
         0.39607921E-03,-0.47446473E-03, 0.59649155E-04, 0.23217201E-03,
        -0.43102575E-03,-0.17374192E-02,-0.16923060E-02,-0.46629529E-03,
         0.83347975E-03,-0.23330333E-03, 0.38547759E-03, 0.12853769E-03,
        -0.14101986E-02,-0.15043073E-02, 0.65257633E-03, 0.43382627E-03,
         0.32273165E-03, 0.83264970E-03,-0.75889763E-03, 0.97020576E-03,
         0.28951961E-03,-0.29303620E-03, 0.69569891E-04,-0.14281059E-02,
        -0.11430028E-02,-0.17344014E-02, 0.39067754E-03, 0.60066738E-03,
        -0.12499165E-03, 0.93326240E-03, 0.13565150E-02,-0.34157417E-03,
        -0.53238991E-03,-0.99078135E-03,-0.21683394E-02, 0.11508230E-02,
         0.67671551E-03, 0.16036005E-02, 0.18797418E-02,-0.37287778E-03,
         0.40538097E-03,-0.12729348E-02,-0.21772266E-03,-0.59431040E-03,
        -0.49691857E-03, 0.20499392E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x12                
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]*x12*x22            
        +coeff[  7]*x11*x24            
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[  8]        *x31        
        +coeff[  9]        *x31    *x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]*x11            *x51
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]            *x42*x51
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 17]    *x24            
        +coeff[ 18]    *x23*x31        
        +coeff[ 19]    *x21*x31*x42    
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x21*x31    *x52
        +coeff[ 22]*x11*x22*x31        
        +coeff[ 23]*x11*x21*x31*x41    
        +coeff[ 24]*x11*x22        *x51
        +coeff[ 25]*x11    *x32    *x51
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 26]*x11    *x31*x41*x51
        +coeff[ 27]*x12        *x42    
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]        *x34    *x51
        +coeff[ 30]    *x22*x31*x41*x51
        +coeff[ 31]    *x21*x32*x41*x51
        +coeff[ 32]    *x22    *x42*x51
        +coeff[ 33]    *x22    *x41*x52
        +coeff[ 34]            *x43*x52
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 35]    *x21*x31    *x53
        +coeff[ 36]*x11*x21*x33        
        +coeff[ 37]*x11    *x31*x43    
        +coeff[ 38]*x11*x23        *x51
        +coeff[ 39]*x11*x21*x32    *x51
        +coeff[ 40]*x11*x21*x31    *x52
        +coeff[ 41]*x12*x22*x31        
        +coeff[ 42]*x12*x22    *x41    
        +coeff[ 43]*x12*x21    *x41*x51
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 44]*x12*x21        *x52
        +coeff[ 45]*x12        *x41*x52
        +coeff[ 46]*x13*x22            
        +coeff[ 47]*x13    *x32        
        +coeff[ 48]*x13    *x31*x41    
        +coeff[ 49]    *x24*x31*x41    
        +coeff[ 50]    *x23*x31*x42    
        +coeff[ 51]    *x23    *x43    
        +coeff[ 52]        *x33*x43    
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 53]*x13*x21        *x51
        +coeff[ 54]    *x22*x32*x41*x51
        +coeff[ 55]        *x32*x43*x51
        +coeff[ 56]        *x33*x41*x52
        +coeff[ 57]    *x22    *x42*x52
        +coeff[ 58]    *x21    *x43*x52
        +coeff[ 59]            *x44*x52
        +coeff[ 60]            *x43*x53
        +coeff[ 61]        *x31*x41*x54
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 62]*x11*x24*x31        
        +coeff[ 63]*x11*x21*x33*x41    
        +coeff[ 64]*x11    *x34*x41    
        +coeff[ 65]*x11*x23    *x42    
        +coeff[ 66]*x11*x22*x32    *x51
        +coeff[ 67]*x11*x22*x31*x41*x51
        +coeff[ 68]*x11    *x33*x41*x51
        +coeff[ 69]*x11    *x32*x42*x51
        +coeff[ 70]*x11        *x44*x51
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 71]*x11*x22*x31    *x52
        +coeff[ 72]*x11    *x32    *x53
        +coeff[ 73]*x11    *x31*x41*x53
        +coeff[ 74]*x12*x22    *x42    
        +coeff[ 75]*x12*x21    *x43    
        +coeff[ 76]*x12    *x31*x41*x52
        +coeff[ 77]    *x21*x34*x42    
        +coeff[ 78]    *x24*x31*x41*x51
        +coeff[ 79]    *x21*x33*x42*x51
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 80]    *x23    *x43*x51
        +coeff[ 81]    *x22*x31*x42*x52
        +coeff[ 82]    *x21*x32*x42*x52
        +coeff[ 83]*x13            *x53
        +coeff[ 84]    *x22*x32    *x53
        +coeff[ 85]    *x22*x31*x41*x53
        +coeff[ 86]    *x23        *x54
        +coeff[ 87]    *x22*x31    *x54
        +coeff[ 88]    *x21    *x42*x54
    ;
    v_l_e_q1ex_1100                                =v_l_e_q1ex_1100                                
        +coeff[ 89]*x11*x23*x32    *x51
        ;

    return v_l_e_q1ex_1100                                ;
}
float x_e_q1ex_1000                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.5540306E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30024E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60028E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.51607867E-03, 0.30029838E-02, 0.12364535E+00, 0.10952288E-02,
        -0.14427962E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_1000                                ;
}
float t_e_q1ex_1000                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.4407148E-05;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30024E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60028E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.51768848E-05,-0.19487448E-02,-0.43166053E-03, 0.22427186E-02,
        -0.69053803E-03,-0.10795876E-02,-0.10060350E-02,-0.69030466E-05,
        -0.36129044E-03,-0.10634892E-02, 0.75349431E-04,-0.49523782E-03,
        -0.10455569E-02,-0.76673750E-03,-0.96343627E-03,-0.56444998E-04,
        -0.53590375E-04,-0.35667261E-04,-0.83962965E-04, 0.11091797E-03,
        -0.49224991E-03, 0.96361073E-04,-0.21873120E-04, 0.61077561E-04,
        -0.58419118E-04, 0.27278295E-04, 0.20222227E-04,-0.41459261E-04,
         0.22055370E-04, 0.28957029E-04,-0.26386706E-04, 0.46496021E-04,
         0.22203089E-05, 0.56721087E-05, 0.46690716E-05,-0.40360587E-05,
         0.26031832E-05,-0.34402913E-05,-0.95985297E-05,-0.66387365E-05,
         0.40059822E-05,-0.71334130E-05,-0.45194433E-05, 0.48840498E-05,
        -0.56721115E-05, 0.64413580E-05, 0.70098868E-05,-0.42588954E-05,
         0.60576654E-05,-0.12607770E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1ex_1000                                =v_t_e_q1ex_1000                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x21*x32*x42    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1ex_1000                                =v_t_e_q1ex_1000                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42*x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]    *x21    *x42*x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q1ex_1000                                =v_t_e_q1ex_1000                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]    *x21*x32*x41*x51
        +coeff[ 29]*x12*x22    *x42    
        +coeff[ 30]    *x22*x32*x42    
        +coeff[ 31]    *x21*x31*x43*x51
        +coeff[ 32]*x11*x21            
        +coeff[ 33]    *x22            
        +coeff[ 34]*x11        *x41    
    ;
    v_t_e_q1ex_1000                                =v_t_e_q1ex_1000                                
        +coeff[ 35]    *x21    *x41*x51
        +coeff[ 36]        *x31*x41*x51
        +coeff[ 37]*x11            *x52
        +coeff[ 38]*x12*x22            
        +coeff[ 39]*x11*x23            
        +coeff[ 40]*x12    *x32        
        +coeff[ 41]*x13        *x41    
        +coeff[ 42]*x12*x21    *x41    
        +coeff[ 43]*x11*x22    *x41    
    ;
    v_t_e_q1ex_1000                                =v_t_e_q1ex_1000                                
        +coeff[ 44]*x13            *x51
        +coeff[ 45]*x12*x21        *x51
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x22*x31    *x51
        +coeff[ 48]*x12*x23            
        +coeff[ 49]*x11    *x32*x42    
        ;

    return v_t_e_q1ex_1000                                ;
}
float y_e_q1ex_1000                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.4577790E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30024E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60028E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.42667039E-03, 0.91398947E-01, 0.68620719E-01,-0.65222225E-03,
        -0.61576103E-03, 0.11455931E-03,-0.20910824E-04,-0.13391799E-03,
         0.62186686E-04, 0.43342934E-04, 0.28050346E-04, 0.26515858E-04,
        -0.15283527E-03,-0.18457793E-04,-0.13428195E-03,-0.11410608E-03,
        -0.51470066E-04,-0.27106491E-04,-0.54255775E-05, 0.52181308E-05,
         0.24608546E-04, 0.13406056E-04,-0.21860349E-04, 0.13232730E-04,
        -0.10981173E-04,-0.37810667E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_q1ex_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_1000                                =v_y_e_q1ex_1000                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]    *x22*x31*x42    
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1ex_1000                                =v_y_e_q1ex_1000                                
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]        *x32    *x51
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]    *x22    *x43    
        +coeff[ 23]            *x41*x53
        +coeff[ 24]    *x23    *x43    
        +coeff[ 25]    *x22*x32*x41*x51
        ;

    return v_y_e_q1ex_1000                                ;
}
float p_e_q1ex_1000                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2241576E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30024E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60028E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.20321070E-03, 0.31185392E-01, 0.65201990E-01,-0.13606646E-02,
        -0.15253854E-02, 0.96784084E-03, 0.66392991E-03, 0.66050614E-03,
         0.45791484E-03, 0.29211963E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_1000                                =v_p_e_q1ex_1000                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_1000                                ;
}
float l_e_q1ex_1000                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1662407E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53984E-01,-0.30024E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60028E-01, 0.53992E-01, 0.30041E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16618449E-02,-0.34766358E-02,-0.10515058E-02,-0.17993522E-02,
        -0.29102378E-03,-0.26250162E-03, 0.75022038E-03,-0.14946266E-03,
        -0.69314752E-04, 0.16359473E-03,-0.13310087E-04, 0.21568740E-04,
        -0.12695280E-03,-0.84086030E-03,-0.35090922E-03, 0.83485029E-04,
         0.16333525E-03,-0.26768897E-03, 0.36552190E-03, 0.20094204E-03,
        -0.19003893E-03, 0.66563836E-03, 0.17220517E-03,-0.28799067E-03,
        -0.37102256E-03, 0.46381092E-03, 0.32600274E-03,-0.55337953E-03,
         0.31008685E-03, 0.78764319E-03, 0.57288393E-03,-0.60485152E-03,
         0.77136094E-03,-0.11207705E-03, 0.56571205E-03, 0.27317947E-03,
         0.71155350E-03, 0.95576321E-03,-0.18381480E-03, 0.57806430E-03,
        -0.59455197E-03,-0.16912752E-03,-0.97399810E-03,-0.61265484E-03,
        -0.48604925E-03, 0.19800452E-03,-0.50525594E-03, 0.53578324E-03,
        -0.64586039E-03, 0.53895294E-03, 0.58767764E-03,-0.10363214E-02,
        -0.57848077E-03, 0.36656213E-03, 0.41162749E-03, 0.45445189E-03,
         0.44518645E-03, 0.85838634E-03, 0.35715065E-03, 0.46611769E-03,
        -0.33984211E-03,-0.11511022E-02, 0.38542401E-03,-0.26323411E-03,
        -0.22864807E-03, 0.11641240E-02, 0.40801906E-03,-0.99976233E-03,
         0.31736057E-03,-0.35904755E-03,-0.55319676E-03, 0.14325426E-03,
        -0.33124699E-02,-0.66446239E-03, 0.50509296E-03, 0.18719489E-02,
        -0.93447580E-03, 0.17238867E-02,-0.29492800E-03, 0.12134617E-02,
        -0.56218536E-03,-0.58081013E-03,-0.68998989E-03,-0.41225442E-03,
        -0.11170660E-02, 0.77626423E-03, 0.16916954E-02, 0.12084706E-02,
        -0.25059341E-02, 0.14534012E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]    *x22*x32        
        +coeff[  5]        *x33    *x51
        +coeff[  6]        *x32*x41*x51
        +coeff[  7]            *x41*x51
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x23            
        +coeff[ 10]        *x33        
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11    *x32        
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]*x11*x21        *x51
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 17]*x12    *x31        
        +coeff[ 18]*x12            *x51
        +coeff[ 19]*x13                
        +coeff[ 20]        *x34        
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]        *x32    *x52
        +coeff[ 23]        *x31    *x53
        +coeff[ 24]*x11        *x42*x51
        +coeff[ 25]*x11        *x41*x52
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 26]*x12    *x31*x41    
        +coeff[ 27]    *x24*x31        
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]*x13            *x51
        +coeff[ 31]    *x24        *x51
        +coeff[ 32]    *x22*x32    *x51
        +coeff[ 33]        *x34    *x51
        +coeff[ 34]    *x23        *x52
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]        *x33    *x52
        +coeff[ 37]        *x32*x41*x52
        +coeff[ 38]            *x43*x52
        +coeff[ 39]    *x21        *x54
        +coeff[ 40]        *x31    *x54
        +coeff[ 41]*x11*x24            
        +coeff[ 42]*x11*x22*x32        
        +coeff[ 43]*x11*x21*x33        
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 44]*x11*x21*x31*x41*x51
        +coeff[ 45]*x12*x21    *x42    
        +coeff[ 46]*x12    *x32    *x51
        +coeff[ 47]*x12    *x31    *x52
        +coeff[ 48]    *x21*x34*x41    
        +coeff[ 49]    *x23*x31*x42    
        +coeff[ 50]    *x22*x31*x43    
        +coeff[ 51]    *x21*x31*x44    
        +coeff[ 52]*x13    *x31    *x51
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 53]    *x21*x34    *x51
        +coeff[ 54]    *x23*x31*x41*x51
        +coeff[ 55]        *x33*x42*x51
        +coeff[ 56]    *x23    *x41*x52
        +coeff[ 57]*x11*x23*x32        
        +coeff[ 58]*x11*x22    *x43    
        +coeff[ 59]*x11    *x31*x44    
        +coeff[ 60]*x11*x23    *x41*x51
        +coeff[ 61]*x11*x21*x32    *x52
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 62]*x11*x21    *x42*x52
        +coeff[ 63]*x11    *x31*x42*x52
        +coeff[ 64]*x11    *x32    *x53
        +coeff[ 65]*x11    *x31*x41*x53
        +coeff[ 66]*x11*x21        *x54
        +coeff[ 67]*x11        *x41*x54
        +coeff[ 68]*x12*x24            
        +coeff[ 69]*x12*x21    *x43    
        +coeff[ 70]*x12*x22*x31    *x51
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 71]    *x22*x34*x41    
        +coeff[ 72]    *x23*x32*x42    
        +coeff[ 73]    *x23*x31*x43    
        +coeff[ 74]    *x23    *x44    
        +coeff[ 75]    *x21*x32*x44    
        +coeff[ 76]*x13    *x31*x41*x51
        +coeff[ 77]    *x22*x32*x42*x51
        +coeff[ 78]*x13    *x31    *x52
        +coeff[ 79]    *x22*x32*x41*x52
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 80]    *x21    *x44*x52
        +coeff[ 81]*x13            *x53
        +coeff[ 82]    *x22*x32    *x53
        +coeff[ 83]            *x44*x53
        +coeff[ 84]        *x32*x41*x54
        +coeff[ 85]*x11*x23*x33        
        +coeff[ 86]*x11*x22*x34        
        +coeff[ 87]*x11*x24*x31    *x51
        +coeff[ 88]*x11*x22*x31*x42*x51
    ;
    v_l_e_q1ex_1000                                =v_l_e_q1ex_1000                                
        +coeff[ 89]*x11    *x33*x42*x51
        ;

    return v_l_e_q1ex_1000                                ;
}
float x_e_q1ex_900                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.2734975E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53978E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60058E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.26293652E-03, 0.30040646E-02, 0.12368266E+00, 0.10921893E-02,
        -0.14686002E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_900                                ;
}
float t_e_q1ex_900                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1181584E-04;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53978E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60058E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.10995534E-04,-0.19477465E-02,-0.42206253E-03, 0.22350389E-02,
        -0.69772563E-03,-0.10647035E-02,-0.98827167E-03, 0.10999152E-04,
        -0.33213687E-03,-0.10618746E-02, 0.70623792E-04,-0.50886621E-03,
        -0.10430117E-02,-0.79291413E-03,-0.98891451E-03,-0.57498386E-04,
        -0.51014678E-04,-0.34610723E-04,-0.92657356E-04,-0.49104070E-03,
        -0.28493949E-04, 0.12953389E-03, 0.70823378E-04,-0.40881391E-05,
         0.40386698E-04,-0.68723930E-04,-0.55242916E-04,-0.48144443E-06,
        -0.62527884E-05, 0.63910279E-05,-0.62198924E-05, 0.18355988E-04,
        -0.96123949E-05, 0.14047253E-04,-0.19090903E-04,-0.20565793E-04,
         0.14157944E-04, 0.20992184E-04,-0.20387932E-04, 0.44027714E-04,
        -0.12979311E-06,-0.23304729E-05,-0.19542545E-05,-0.29711634E-05,
        -0.52954506E-05, 0.22894692E-05, 0.52447576E-05,-0.35548330E-05,
        -0.62266190E-05, 0.53024087E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x32        
    ;
    v_t_e_q1ex_900                                =v_t_e_q1ex_900                                
        +coeff[  8]    *x23*x32        
        +coeff[  9]    *x21*x32*x42    
        +coeff[ 10]*x11            *x51
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1ex_900                                =v_t_e_q1ex_900                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]*x11*x22*x31*x41    
    ;
    v_t_e_q1ex_900                                =v_t_e_q1ex_900                                
        +coeff[ 26]*x11*x22    *x42    
        +coeff[ 27]    *x21    *x41    
        +coeff[ 28]    *x21*x31    *x51
        +coeff[ 29]*x11*x21*x32        
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x23        *x51
        +coeff[ 32]*x11*x21    *x41*x51
        +coeff[ 33]*x12*x22*x31        
        +coeff[ 34]*x11*x21*x32*x41    
    ;
    v_t_e_q1ex_900                                =v_t_e_q1ex_900                                
        +coeff[ 35]*x12*x21    *x42    
        +coeff[ 36]*x11*x21*x32    *x51
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x21*x33*x41*x51
        +coeff[ 39]    *x21*x32*x42*x51
        +coeff[ 40]*x11    *x31        
        +coeff[ 41]    *x21*x31        
        +coeff[ 42]                *x52
        +coeff[ 43]*x12    *x31        
    ;
    v_t_e_q1ex_900                                =v_t_e_q1ex_900                                
        +coeff[ 44]*x11*x21*x31        
        +coeff[ 45]        *x32*x41    
        +coeff[ 46]*x11*x22*x31        
        +coeff[ 47]*x12        *x42    
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]*x11    *x32    *x51
        ;

    return v_t_e_q1ex_900                                ;
}
float y_e_q1ex_900                                (float *x,int m){
    int ncoeff= 34;
    float avdat=  0.1067497E-02;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53978E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60058E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
        -0.10046405E-02, 0.91423139E-01, 0.68620272E-01,-0.64601039E-03,
        -0.61437767E-03, 0.10342133E-03,-0.50627375E-04,-0.14338082E-03,
         0.60706330E-04, 0.66437671E-04,-0.13337251E-03, 0.33398814E-04,
        -0.17277422E-04, 0.24344930E-04, 0.23995486E-04,-0.17820326E-03,
         0.29999348E-05,-0.11347985E-03,-0.73303352E-04,-0.28128752E-04,
         0.65036697E-05, 0.21442444E-04, 0.90028179E-05, 0.10473213E-04,
        -0.39597569E-04,-0.15799716E-04,-0.32458072E-04, 0.10249307E-04,
         0.14625440E-04, 0.10727265E-04, 0.16585600E-04, 0.83966370E-05,
         0.42309210E-04,-0.26892003E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_900                                =v_y_e_q1ex_900                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x22    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x31*x44    
    ;
    v_y_e_q1ex_900                                =v_y_e_q1ex_900                                
        +coeff[ 17]    *x24*x31*x42    
        +coeff[ 18]    *x22*x33*x42    
        +coeff[ 19]*x13*x21    *x43    
        +coeff[ 20]        *x31*x41*x51
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]*x11        *x42*x51
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_y_e_q1ex_900                                =v_y_e_q1ex_900                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]*x12        *x43    
        +coeff[ 28]*x11    *x31*x42*x51
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]    *x21*x32*x43    
        +coeff[ 31]*x12        *x44    
        +coeff[ 32]    *x22*x31*x42*x51
        +coeff[ 33]*x11*x22*x32*x41    
        ;

    return v_y_e_q1ex_900                                ;
}
float p_e_q1ex_900                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.6247933E-03;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53978E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60058E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.58238412E-03, 0.31179911E-01, 0.65222643E-01,-0.13587193E-02,
        -0.15227859E-02, 0.96783729E-03, 0.66611724E-03, 0.65959489E-03,
         0.46067810E-03, 0.28656219E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_900                                =v_p_e_q1ex_900                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_900                                ;
}
float l_e_q1ex_900                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1654832E-02;
    float xmin[10]={
        -0.39991E-02,-0.60068E-01,-0.53978E-01,-0.30027E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60058E-01, 0.53997E-01, 0.30061E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16936343E-02,-0.35889233E-02,-0.55338437E-03,-0.15584901E-02,
        -0.24188652E-03,-0.47529775E-04,-0.26724886E-02, 0.10325627E-02,
        -0.11998172E-02,-0.15727861E-02, 0.20038334E-02,-0.10867593E-03,
        -0.17401517E-04, 0.22796581E-03, 0.14559092E-03, 0.91933063E-04,
        -0.24972649E-03,-0.27676095E-03,-0.65135135E-03, 0.92494120E-04,
         0.76357793E-03,-0.28730358E-03,-0.10936874E-03,-0.27723989E-03,
         0.15133357E-03, 0.38729201E-03, 0.12302755E-03,-0.44508514E-03,
        -0.39914079E-03, 0.45101002E-04,-0.41446835E-03, 0.12198375E-02,
         0.10717228E-02,-0.29821484E-03,-0.37123071E-04, 0.19924261E-02,
        -0.22919224E-03, 0.39124358E-03,-0.48450343E-03,-0.78620622E-03,
         0.32060116E-03,-0.99038368E-03, 0.33795787E-02, 0.52098988E-03,
        -0.65087806E-04,-0.13766079E-05,-0.45545565E-03, 0.25246490E-03,
        -0.91043988E-03, 0.18954734E-04, 0.14554493E-02, 0.69466670E-03,
         0.12109432E-02, 0.50926267E-03,-0.80663711E-04,-0.50314184E-03,
        -0.12764379E-02, 0.23937317E-03, 0.27031708E-03, 0.51076512E-03,
        -0.16256787E-02,-0.11577286E-02, 0.12509611E-02, 0.27547212E-03,
        -0.12709822E-02,-0.85174921E-03, 0.56075898E-03,-0.68771973E-03,
        -0.16806872E-02, 0.71899750E-03,-0.77637588E-03, 0.75689517E-03,
        -0.10002848E-02,-0.64973987E-03,-0.11548260E-02,-0.30918126E-02,
         0.91058604E-03, 0.48795779E-03, 0.57873852E-03,-0.57751290E-03,
         0.14102100E-02, 0.11868865E-03, 0.18874486E-02,-0.76690089E-03,
         0.97055902E-03, 0.83550031E-03, 0.98307311E-04,-0.20300101E-03,
        -0.91147667E-04,-0.52360181E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x34        
        +coeff[  6]*x11*x22*x32*x41    
        +coeff[  7]*x11*x22*x32    *x51
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[  8]*x13*x22*x31        
        +coeff[  9]    *x22*x33*x41*x51
        +coeff[ 10]*x11*x22*x32*x42    
        +coeff[ 11]        *x31        
        +coeff[ 12]            *x41    
        +coeff[ 13]*x11    *x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]    *x22*x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]                *x53
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]*x11        *x42    
        +coeff[ 23]*x11        *x41*x51
        +coeff[ 24]*x12            *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 26]    *x21*x32*x41    
        +coeff[ 27]        *x32*x42    
        +coeff[ 28]        *x31*x43    
        +coeff[ 29]*x11    *x32*x41    
        +coeff[ 30]*x11    *x31*x42    
        +coeff[ 31]*x11*x21    *x41*x51
        +coeff[ 32]*x11        *x41*x52
        +coeff[ 33]*x12        *x42    
        +coeff[ 34]*x12            *x52
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 35]    *x22*x32*x41    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]        *x32*x41*x52
        +coeff[ 39]*x11*x21*x33        
        +coeff[ 40]*x11    *x33*x41    
        +coeff[ 41]*x11    *x33    *x51
        +coeff[ 42]*x11    *x31*x42*x51
        +coeff[ 43]*x11*x21    *x41*x52
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 44]*x12*x21*x32        
        +coeff[ 45]*x12*x21*x31    *x51
        +coeff[ 46]*x13*x21*x31        
        +coeff[ 47]    *x23*x33        
        +coeff[ 48]    *x23*x32*x41    
        +coeff[ 49]    *x23*x31*x42    
        +coeff[ 50]    *x21*x32*x43    
        +coeff[ 51]*x13    *x31    *x51
        +coeff[ 52]        *x34*x41*x51
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 53]        *x33*x42*x51
        +coeff[ 54]    *x23    *x41*x52
        +coeff[ 55]    *x21*x31*x42*x52
        +coeff[ 56]        *x32*x41*x53
        +coeff[ 57]            *x43*x53
        +coeff[ 58]*x11*x23*x32        
        +coeff[ 59]*x11*x21*x31*x42*x51
        +coeff[ 60]*x11*x21    *x43*x51
        +coeff[ 61]*x11    *x31*x43*x51
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 62]*x11    *x32*x41*x52
        +coeff[ 63]*x11    *x31    *x54
        +coeff[ 64]*x11        *x41*x54
        +coeff[ 65]*x12*x21*x31*x41*x51
        +coeff[ 66]*x12*x22        *x52
        +coeff[ 67]*x12*x21    *x41*x52
        +coeff[ 68]    *x24*x32*x41    
        +coeff[ 69]    *x23    *x44    
        +coeff[ 70]    *x21*x32*x44    
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 71]*x13    *x31*x41*x51
        +coeff[ 72]*x13        *x41*x52
        +coeff[ 73]    *x23*x31    *x53
        +coeff[ 74]*x11*x22*x31*x42*x51
        +coeff[ 75]*x11    *x31*x44*x51
        +coeff[ 76]*x11*x21*x33    *x52
        +coeff[ 77]*x12*x22*x33        
        +coeff[ 78]*x12*x23*x31*x41    
        +coeff[ 79]*x12*x21    *x44    
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 80]*x12*x23*x31    *x51
        +coeff[ 81]*x12*x23        *x52
        +coeff[ 82]*x12*x21*x32    *x52
        +coeff[ 83]*x13    *x31    *x53
        +coeff[ 84]    *x23*x32    *x53
        +coeff[ 85]    *x21*x31*x43*x53
        +coeff[ 86]    *x21            
        +coeff[ 87]        *x32        
        +coeff[ 88]    *x21        *x51
    ;
    v_l_e_q1ex_900                                =v_l_e_q1ex_900                                
        +coeff[ 89]*x11            *x51
        ;

    return v_l_e_q1ex_900                                ;
}
float x_e_q1ex_800                                (float *x,int m){
    int ncoeff=  5;
    float avdat=  0.5394897E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
        -0.53676782E-03, 0.30043775E-02, 0.12366500E+00, 0.10918041E-02,
        -0.14459893E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_800                                ;
}
float t_e_q1ex_800                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.8296111E-05;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.77182904E-05,-0.19459786E-02,-0.42116232E-03, 0.22308799E-02,
        -0.68304461E-03,-0.48506018E-03,-0.10373746E-02,-0.99246646E-03,
         0.71687005E-04,-0.86519394E-04,-0.10717617E-02,-0.78082667E-03,
        -0.10956737E-02,-0.10008087E-02,-0.62138148E-04,-0.50195096E-04,
        -0.40178304E-04, 0.10468093E-03,-0.36935587E-03,-0.51819364E-03,
        -0.25292991E-04, 0.41489053E-04, 0.79844940E-04,-0.67516514E-04,
         0.18186603E-04,-0.95444275E-05,-0.20958652E-04,-0.40219526E-04,
        -0.22676466E-04,-0.12864146E-04,-0.19029620E-04,-0.30916424E-04,
         0.34380118E-04, 0.62004590E-04, 0.54707521E-04,-0.14858877E-04,
        -0.23700566E-04, 0.91753712E-07,-0.17702121E-05, 0.86701302E-05,
        -0.45450938E-05, 0.32587145E-05, 0.28928166E-05, 0.42175261E-05,
        -0.31098371E-05,-0.27568356E-05, 0.66767102E-05, 0.31999923E-06,
         0.12385740E-04, 0.48771785E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_800                                =v_t_e_q1ex_800                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_800                                =v_t_e_q1ex_800                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]*x11*x21    *x41*x51
    ;
    v_t_e_q1ex_800                                =v_t_e_q1ex_800                                
        +coeff[ 26]*x11*x23    *x41    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22*x32    *x51
        +coeff[ 30]*x12*x21    *x41*x51
        +coeff[ 31]*x13*x21*x31*x41    
        +coeff[ 32]*x12*x22*x31*x41    
        +coeff[ 33]    *x21*x32*x42*x51
        +coeff[ 34]    *x21*x31*x43*x51
    ;
    v_t_e_q1ex_800                                =v_t_e_q1ex_800                                
        +coeff[ 35]*x13*x21        *x52
        +coeff[ 36]    *x22*x31*x41*x52
        +coeff[ 37]            *x41    
        +coeff[ 38]                *x52
        +coeff[ 39]*x11*x21    *x41    
        +coeff[ 40]    *x22    *x41    
        +coeff[ 41]        *x31*x42    
        +coeff[ 42]    *x21*x31    *x51
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_t_e_q1ex_800                                =v_t_e_q1ex_800                                
        +coeff[ 44]        *x31*x41*x51
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x11*x21*x32        
        +coeff[ 47]*x11    *x33        
        +coeff[ 48]*x11*x21*x31*x41    
        +coeff[ 49]*x11    *x31*x42    
        ;

    return v_t_e_q1ex_800                                ;
}
float y_e_q1ex_800                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.1083471E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.77816985E-05, 0.91384992E-01, 0.68624355E-01,-0.64356613E-03,
        -0.61552104E-03, 0.11309858E-03,-0.33350316E-04,-0.12914152E-03,
         0.55447043E-04, 0.28228198E-04,-0.14746754E-03, 0.44799901E-04,
        -0.21766093E-04, 0.25534349E-04,-0.12637400E-03,-0.12594010E-03,
        -0.28127548E-04, 0.36172135E-05, 0.15023521E-04, 0.13434008E-04,
         0.15366628E-04, 0.73776882E-05, 0.12456907E-04,-0.42813354E-04,
        -0.32144573E-04,-0.15287444E-04,-0.51797386E-04,-0.16742884E-04,
         0.20103662E-04,-0.21096790E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_800                                =v_y_e_q1ex_800                                
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x22*x31*x42    
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]*x11*x23    *x41    
    ;
    v_y_e_q1ex_800                                =v_y_e_q1ex_800                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21*x31*x41    
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]*x11        *x41*x52
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x21*x32*x41    
        +coeff[ 25]*x11    *x32*x41*x51
    ;
    v_y_e_q1ex_800                                =v_y_e_q1ex_800                                
        +coeff[ 26]*x11*x21*x31*x43    
        +coeff[ 27]        *x32*x43*x51
        +coeff[ 28]*x13*x21    *x41    
        +coeff[ 29]    *x22    *x45    
        ;

    return v_y_e_q1ex_800                                ;
}
float p_e_q1ex_800                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.1495020E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.16159404E-03, 0.31177189E-01, 0.65185249E-01,-0.13606143E-02,
        -0.15234357E-02, 0.96986286E-03, 0.66602160E-03, 0.66765770E-03,
         0.45709647E-03, 0.28893663E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_800                                =v_p_e_q1ex_800                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_800                                ;
}
float l_e_q1ex_800                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1652832E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16078962E-02,-0.35464163E-02,-0.75943006E-03,-0.15942238E-02,
        -0.42724077E-03,-0.10122345E-02,-0.17659341E-02,-0.18353399E-03,
        -0.28134780E-04, 0.13332714E-03, 0.54114160E-04,-0.39772051E-04,
         0.74543845E-03, 0.70340146E-04, 0.49705309E-03,-0.21197088E-03,
        -0.54000388E-03,-0.78429881E-03,-0.18064199E-03, 0.73298003E-03,
        -0.49822388E-03,-0.69265130E-04, 0.79473294E-03, 0.38416378E-03,
        -0.65255660E-03,-0.35574546E-03,-0.47056130E-03, 0.46181446E-03,
        -0.41162468E-04,-0.31685154E-03, 0.48359224E-03,-0.53293485E-03,
        -0.94108027E-03,-0.11512014E-02, 0.11913670E-02, 0.35483160E-03,
         0.11836694E-02, 0.76067314E-03,-0.95000298E-03,-0.72085293E-03,
         0.11153065E-02,-0.10806151E-02,-0.63105568E-03,-0.64122328E-03,
         0.70176774E-03, 0.11546115E-02, 0.91681693E-03,-0.73825830E-03,
         0.93671150E-03, 0.77339972E-03,-0.19277376E-02,-0.56171574E-03,
        -0.21606029E-02,-0.32701052E-03, 0.10780179E-02, 0.10017609E-02,
         0.20068022E-02,-0.59547758E-03, 0.94725925E-03,-0.86158962E-03,
         0.17782976E-03, 0.54199394E-03, 0.26536538E-05,-0.21066640E-02,
         0.22157743E-02,-0.20896355E-02,-0.10102186E-03,-0.27900239E-03,
        -0.98009470E-04,-0.67105102E-04,-0.45339100E-04, 0.10734899E-03,
         0.72148847E-04, 0.76648023E-04,-0.12932235E-03,-0.60814586E-04,
         0.11407457E-03, 0.79118821E-04,-0.11041829E-03, 0.66189968E-04,
         0.13289442E-03, 0.15300083E-03, 0.25334099E-03,-0.37730156E-03,
         0.49783604E-03, 0.16960494E-03,-0.18859480E-03,-0.16995364E-03,
        -0.27310263E-03, 0.26180310E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11*x21*x32*x41    
        +coeff[  5]    *x23*x32    *x51
        +coeff[  6]    *x22*x32    *x54
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[  8]                *x52
        +coeff[  9]*x11            *x51
        +coeff[ 10]            *x43    
        +coeff[ 11]*x11        *x41*x51
        +coeff[ 12]    *x22*x31*x41    
        +coeff[ 13]    *x22*x31    *x51
        +coeff[ 14]        *x32    *x52
        +coeff[ 15]    *x21    *x41*x52
        +coeff[ 16]*x11*x21*x32        
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 17]*x11*x22        *x51
        +coeff[ 18]*x11*x21*x31    *x51
        +coeff[ 19]*x11    *x32    *x51
        +coeff[ 20]*x11    *x31*x41*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]    *x21*x32*x41*x51
        +coeff[ 23]        *x32*x42*x51
        +coeff[ 24]    *x21    *x43*x51
        +coeff[ 25]    *x22        *x53
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 26]*x11*x22    *x41*x51
        +coeff[ 27]*x11*x22        *x52
        +coeff[ 28]*x12*x21*x31*x41    
        +coeff[ 29]*x12    *x32*x41    
        +coeff[ 30]*x12*x22        *x51
        +coeff[ 31]*x12*x21    *x41*x51
        +coeff[ 32]    *x22*x33*x41    
        +coeff[ 33]    *x22*x32*x41*x51
        +coeff[ 34]    *x21*x32    *x53
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 35]    *x22    *x41*x53
        +coeff[ 36]*x11*x21*x34        
        +coeff[ 37]*x11*x21*x31    *x53
        +coeff[ 38]*x11    *x32    *x53
        +coeff[ 39]*x11    *x31*x41*x53
        +coeff[ 40]*x12*x23        *x51
        +coeff[ 41]*x12*x21*x32    *x51
        +coeff[ 42]*x12*x22    *x41*x51
        +coeff[ 43]    *x23*x31*x43    
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 44]*x13*x22        *x51
        +coeff[ 45]*x13    *x31*x41*x51
        +coeff[ 46]    *x21    *x43*x53
        +coeff[ 47]    *x21*x31*x41*x54
        +coeff[ 48]*x11*x22*x34        
        +coeff[ 49]*x11*x21*x31*x44    
        +coeff[ 50]*x11*x22*x32    *x52
        +coeff[ 51]*x11*x22    *x42*x52
        +coeff[ 52]*x11    *x32*x42*x52
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 53]*x11*x23        *x53
        +coeff[ 54]*x11*x22        *x54
        +coeff[ 55]*x11    *x32    *x54
        +coeff[ 56]*x12*x23*x31*x41    
        +coeff[ 57]*x12    *x34    *x51
        +coeff[ 58]*x12    *x32*x42*x51
        +coeff[ 59]*x13*x22*x31*x41    
        +coeff[ 60]*x13    *x32*x42    
        +coeff[ 61]*x13        *x44    
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 62]    *x23*x34    *x51
        +coeff[ 63]*x13*x22        *x52
        +coeff[ 64]    *x23*x31    *x54
        +coeff[ 65]    *x21*x33    *x54
        +coeff[ 66]*x11                
        +coeff[ 67]        *x32        
        +coeff[ 68]    *x21        *x51
        +coeff[ 69]*x11    *x31        
        +coeff[ 70]        *x33        
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 71]        *x31*x42    
        +coeff[ 72]    *x21*x31    *x51
        +coeff[ 73]        *x31*x41*x51
        +coeff[ 74]    *x21        *x52
        +coeff[ 75]        *x31    *x52
        +coeff[ 76]            *x41*x52
        +coeff[ 77]*x11    *x31*x41    
        +coeff[ 78]*x11        *x42    
        +coeff[ 79]*x11    *x31    *x51
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 80]*x12*x21            
        +coeff[ 81]*x13                
        +coeff[ 82]    *x24            
        +coeff[ 83]    *x23*x31        
        +coeff[ 84]    *x21*x33        
        +coeff[ 85]        *x34        
        +coeff[ 86]    *x22    *x42    
        +coeff[ 87]        *x31*x43    
        +coeff[ 88]    *x23        *x51
    ;
    v_l_e_q1ex_800                                =v_l_e_q1ex_800                                
        +coeff[ 89]    *x22    *x41*x51
        ;

    return v_l_e_q1ex_800                                ;
}
float x_e_q1ex_700                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.8045674E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53994E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.82508370E-03, 0.30060436E-02, 0.12367858E+00, 0.10889455E-02,
        -0.14644314E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_700                                ;
}
float t_e_q1ex_700                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1594669E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53994E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.16716709E-04,-0.19469875E-02,-0.41269331E-03, 0.71820272E-04,
         0.22313620E-02,-0.67928899E-03,-0.10507196E-02,-0.99595299E-03,
        -0.52584410E-05,-0.37798067E-03,-0.10993012E-02,-0.47285168E-03,
        -0.10542097E-02,-0.77485200E-03,-0.99334621E-03,-0.62375970E-04,
        -0.52378975E-04,-0.37753325E-04,-0.85144115E-04, 0.73249197E-04,
        -0.51338878E-03,-0.27371401E-04, 0.66357403E-04,-0.96063613E-05,
        -0.51247836E-04,-0.44431592E-04, 0.10301658E-03, 0.16382197E-04,
        -0.17814349E-04, 0.29596384E-04, 0.36294103E-04,-0.18958282E-04,
        -0.11337535E-04,-0.12272987E-04, 0.67708992E-04, 0.66837812E-04,
         0.15330368E-05,-0.46552032E-05, 0.15695031E-05,-0.16326918E-05,
         0.16352320E-05,-0.25057095E-05, 0.20011294E-05, 0.63087887E-05,
        -0.55285959E-05, 0.93266026E-05,-0.50111234E-05,-0.87801900E-05,
        -0.69119174E-05, 0.67077549E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_700                                =v_t_e_q1ex_700                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32*x42    
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q1ex_700                                =v_t_e_q1ex_700                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1ex_700                                =v_t_e_q1ex_700                                
        +coeff[ 26]    *x21*x32*x42*x51
        +coeff[ 27]*x11*x21    *x41    
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]    *x23        *x51
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]*x13*x21    *x41    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]*x11*x23*x32        
        +coeff[ 34]    *x21*x33*x41*x51
    ;
    v_t_e_q1ex_700                                =v_t_e_q1ex_700                                
        +coeff[ 35]    *x21*x31*x43*x51
        +coeff[ 36]*x11    *x31        
        +coeff[ 37]    *x21*x31        
        +coeff[ 38]*x11        *x41    
        +coeff[ 39]        *x31*x41    
        +coeff[ 40]            *x41*x51
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]        *x32    *x51
        +coeff[ 43]*x13*x21            
    ;
    v_t_e_q1ex_700                                =v_t_e_q1ex_700                                
        +coeff[ 44]*x11*x23            
        +coeff[ 45]*x12*x21*x31        
        +coeff[ 46]*x11*x21*x31*x41    
        +coeff[ 47]    *x21*x32*x41    
        +coeff[ 48]    *x21*x31*x42    
        +coeff[ 49]        *x31*x43    
        ;

    return v_t_e_q1ex_700                                ;
}
float y_e_q1ex_700                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1159514E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53994E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.14424641E-03, 0.91348603E-01, 0.68616621E-01,-0.63967344E-03,
        -0.61580050E-03, 0.10345138E-03,-0.10624088E-04,-0.12196746E-03,
         0.78471137E-04, 0.54123000E-04, 0.32443106E-04, 0.19111427E-04,
        -0.17860631E-03, 0.33757414E-06,-0.19745661E-04,-0.15249873E-03,
        -0.21290080E-04,-0.28110480E-04,-0.33266497E-04,-0.67524285E-04,
        -0.50298445E-05,-0.97792213E-06, 0.17546585E-04,-0.11831603E-04,
         0.13939336E-04,-0.10001184E-03,-0.34265689E-04,-0.31520784E-04,
        -0.99606586E-05, 0.21966944E-04, 0.21307211E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_700                                =v_y_e_q1ex_700                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]    *x22    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x31*x44    
    ;
    v_y_e_q1ex_700                                =v_y_e_q1ex_700                                
        +coeff[ 17]    *x22*x33*x42    
        +coeff[ 18]*x12        *x45    
        +coeff[ 19]    *x24    *x45    
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21    *x43    
        +coeff[ 22]        *x31*x43    
        +coeff[ 23]    *x21*x32*x41    
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_y_e_q1ex_700                                =v_y_e_q1ex_700                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]        *x31*x45    
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]        *x31*x42*x52
        +coeff[ 30]*x12*x22    *x41    
        ;

    return v_y_e_q1ex_700                                ;
}
float p_e_q1ex_700                                (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.1202281E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53994E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.13972142E-03, 0.31168956E-01, 0.65164074E-01,-0.13591915E-02,
        -0.15230421E-02, 0.96662960E-03, 0.67061355E-03, 0.65949874E-03,
         0.45476126E-03, 0.28758138E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_700                                =v_p_e_q1ex_700                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_700                                ;
}
float l_e_q1ex_700                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1650701E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60067E-01, 0.53994E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.15685718E-02,-0.31833837E-02,-0.69113902E-03,-0.18296069E-02,
         0.73726266E-03,-0.58250607E-03,-0.27598974E-02,-0.58795954E-03,
        -0.68330660E-05, 0.21077260E-03,-0.46155692E-04, 0.51488452E-04,
        -0.34731807E-03, 0.49979048E-03,-0.40919488E-03, 0.12030897E-03,
        -0.75398020E-04, 0.16387628E-03, 0.25031494E-03,-0.61114295E-03,
        -0.47046607E-03,-0.11709120E-03, 0.18912263E-03, 0.28223805E-04,
        -0.19774539E-03, 0.33081439E-03,-0.67962566E-04, 0.28652220E-02,
         0.61152103E-04, 0.61982981E-03, 0.18971798E-03,-0.61000395E-03,
        -0.29573333E-03,-0.13911615E-02,-0.31763822E-03,-0.36348248E-03,
        -0.87020674E-03, 0.94001688E-03, 0.83823560E-03, 0.68815640E-03,
        -0.72104519E-03, 0.79878792E-03, 0.45439461E-03,-0.39806863E-03,
        -0.41087158E-03, 0.55636431E-03,-0.62781788E-03, 0.62096922E-03,
         0.22303386E-03, 0.12379198E-02,-0.28279156E-03,-0.21117532E-04,
         0.53508009E-03,-0.65154122E-03,-0.11452059E-02, 0.55861170E-03,
        -0.42821519E-03, 0.68909349E-03, 0.94805419E-03, 0.52206503E-03,
        -0.91366202E-03, 0.34954408E-03, 0.50085149E-03,-0.12802631E-02,
        -0.11752455E-02, 0.10003751E-02, 0.18009649E-02, 0.32948048E-03,
         0.78240503E-03, 0.71233208E-03,-0.70942770E-03, 0.52881910E-03,
        -0.60935138E-03,-0.11585884E-02,-0.71730331E-03, 0.91209082E-03,
        -0.43844251E-03,-0.56166132E-03, 0.94994128E-03, 0.82036149E-03,
        -0.10506803E-02,-0.71366824E-03, 0.18779133E-02,-0.15010138E-02,
        -0.10878774E-02,-0.86530932E-03, 0.12619196E-02, 0.34351886E-03,
         0.17698326E-02,-0.85016742E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x12        *x42    
        +coeff[  5]    *x23*x31*x41    
        +coeff[  6]*x11*x24*x31        
        +coeff[  7]*x12*x24            
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[  8]        *x32        
        +coeff[  9]            *x41*x51
        +coeff[ 10]*x11*x21            
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x21    *x41*x51
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]*x11*x22            
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 17]*x11*x21*x31        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]*x11        *x41*x51
        +coeff[ 21]*x12    *x31        
        +coeff[ 22]*x12        *x41    
        +coeff[ 23]*x13                
        +coeff[ 24]    *x23        *x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 26]            *x42*x52
        +coeff[ 27]*x11*x22*x31        
        +coeff[ 28]*x11    *x33        
        +coeff[ 29]*x11*x22    *x41    
        +coeff[ 30]*x11*x21*x31*x41    
        +coeff[ 31]*x11    *x32*x41    
        +coeff[ 32]*x11*x21    *x42    
        +coeff[ 33]*x11*x22        *x51
        +coeff[ 34]*x12*x21        *x51
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 35]*x12        *x41*x51
        +coeff[ 36]*x13    *x31        
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x21*x31*x43    
        +coeff[ 39]        *x31*x44    
        +coeff[ 40]    *x23    *x41*x51
        +coeff[ 41]        *x33*x41*x51
        +coeff[ 42]    *x22    *x42*x51
        +coeff[ 43]        *x31*x43*x51
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 44]            *x44*x51
        +coeff[ 45]    *x22*x31    *x52
        +coeff[ 46]        *x33    *x52
        +coeff[ 47]*x11        *x44    
        +coeff[ 48]*x11    *x33    *x51
        +coeff[ 49]*x11*x22    *x41*x51
        +coeff[ 50]*x11*x21        *x53
        +coeff[ 51]*x12*x23            
        +coeff[ 52]*x12    *x33        
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 53]*x12*x21*x31*x41    
        +coeff[ 54]*x12    *x32    *x51
        +coeff[ 55]*x13        *x42    
        +coeff[ 56]        *x34*x42    
        +coeff[ 57]*x13        *x41*x51
        +coeff[ 58]    *x21*x32*x41*x52
        +coeff[ 59]    *x21*x31*x42*x52
        +coeff[ 60]        *x31*x43*x52
        +coeff[ 61]    *x21*x32    *x53
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 62]        *x31*x41*x54
        +coeff[ 63]*x11*x22*x33        
        +coeff[ 64]*x11*x22*x32*x41    
        +coeff[ 65]*x11    *x34*x41    
        +coeff[ 66]*x11*x24        *x51
        +coeff[ 67]*x11    *x34    *x51
        +coeff[ 68]*x11*x21*x32*x41*x51
        +coeff[ 69]*x11*x22    *x42*x51
        +coeff[ 70]*x11*x22*x31    *x52
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 71]*x11*x21*x32    *x52
        +coeff[ 72]*x11    *x32*x41*x52
        +coeff[ 73]*x11*x21    *x41*x53
        +coeff[ 74]*x12        *x42*x52
        +coeff[ 75]*x13    *x33        
        +coeff[ 76]    *x24*x33        
        +coeff[ 77]*x13*x22        *x51
        +coeff[ 78]*x13*x21    *x41*x51
        +coeff[ 79]    *x23        *x54
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 80]*x11*x24    *x42    
        +coeff[ 81]*x11*x22*x31*x43    
        +coeff[ 82]*x11*x23*x32    *x51
        +coeff[ 83]*x11*x22*x32*x41*x51
        +coeff[ 84]*x11*x21*x32    *x53
        +coeff[ 85]*x12*x22*x32*x41    
        +coeff[ 86]*x12    *x34    *x51
        +coeff[ 87]*x12*x22    *x42*x51
        +coeff[ 88]*x12    *x32*x42*x51
    ;
    v_l_e_q1ex_700                                =v_l_e_q1ex_700                                
        +coeff[ 89]*x12*x23        *x52
        ;

    return v_l_e_q1ex_700                                ;
}
float x_e_q1ex_600                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.5544929E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.29997E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.39632701E-04, 0.30044261E-02, 0.12369421E+00, 0.10902521E-02,
        -0.14572998E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_600                                ;
}
float t_e_q1ex_600                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2377034E-05;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.29997E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.25085383E-05,-0.19475907E-02,-0.38753991E-03, 0.74643176E-04,
         0.22303332E-02,-0.69357810E-03,-0.49731170E-03,-0.10273264E-02,
        -0.10070443E-02,-0.86577071E-04,-0.10834276E-02,-0.76276192E-03,
        -0.10785463E-02,-0.99075784E-03,-0.50535404E-04,-0.46634872E-04,
        -0.39462873E-04, 0.10061383E-03,-0.35671913E-03,-0.54212956E-03,
        -0.20985184E-04, 0.84736035E-04, 0.22309832E-04,-0.24110359E-04,
         0.40971721E-04,-0.10527736E-04,-0.46819634E-04,-0.37339669E-04,
         0.19233068E-04, 0.56959067E-04,-0.15941375E-05, 0.22623508E-05,
        -0.37971790E-05,-0.28144821E-05,-0.36847237E-05,-0.70008691E-05,
         0.67020696E-05,-0.25311590E-05, 0.29448179E-05, 0.31123309E-05,
         0.13171927E-04,-0.49829468E-05,-0.27406768E-05, 0.68371414E-05,
         0.39789857E-05, 0.57276270E-05,-0.29607402E-05,-0.22602853E-04,
        -0.17560153E-04,-0.76215106E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x32        
        +coeff[  7]    *x21*x31*x41    
    ;
    v_t_e_q1ex_600                                =v_t_e_q1ex_600                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_600                                =v_t_e_q1ex_600                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21    *x42*x51
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]    *x23*x33        
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]*x11*x21*x33        
    ;
    v_t_e_q1ex_600                                =v_t_e_q1ex_600                                
        +coeff[ 26]*x11*x22*x31*x41    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]*x11*x22*x31    *x51
        +coeff[ 29]    *x21*x31*x43*x51
        +coeff[ 30]    *x21    *x41    
        +coeff[ 31]    *x22        *x51
        +coeff[ 32]*x11    *x31    *x51
        +coeff[ 33]*x11        *x41*x51
        +coeff[ 34]*x11            *x52
    ;
    v_t_e_q1ex_600                                =v_t_e_q1ex_600                                
        +coeff[ 35]*x11*x22*x31        
        +coeff[ 36]    *x23*x31        
        +coeff[ 37]    *x22*x32        
        +coeff[ 38]*x11    *x33        
        +coeff[ 39]*x12        *x42    
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x11*x21*x31    *x51
        +coeff[ 42]    *x22*x31    *x51
        +coeff[ 43]    *x22    *x41*x51
    ;
    v_t_e_q1ex_600                                =v_t_e_q1ex_600                                
        +coeff[ 44]        *x31*x41*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]            *x41*x53
        +coeff[ 47]*x11*x22*x32        
        +coeff[ 48]*x13    *x31*x41    
        +coeff[ 49]*x11*x21*x32*x41    
        ;

    return v_t_e_q1ex_600                                ;
}
float y_e_q1ex_600                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.3381582E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.29997E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.86335895E-04, 0.91327973E-01, 0.68633281E-01,-0.64058858E-03,
        -0.60619385E-03, 0.10298050E-03,-0.61000621E-04,-0.12863935E-03,
         0.70412134E-04,-0.14890076E-03, 0.34170605E-05, 0.50908115E-04,
         0.72829807E-05, 0.25957430E-04, 0.22343556E-04,-0.13731667E-03,
        -0.25033274E-04, 0.36874490E-05,-0.10092690E-04,-0.23715233E-04,
        -0.76973256E-05,-0.57975217E-05,-0.79565230E-04, 0.20469412E-04,
        -0.10515922E-04,-0.31808118E-04,-0.33691493E-04,-0.19350882E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_600                                =v_y_e_q1ex_600                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22    *x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]        *x31*x44*x51
    ;
    v_y_e_q1ex_600                                =v_y_e_q1ex_600                                
        +coeff[ 17]        *x31*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]*x12*x21    *x41    
        +coeff[ 21]*x11        *x44    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]*x11*x21    *x43    
        +coeff[ 24]*x12        *x43    
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1ex_600                                =v_y_e_q1ex_600                                
        +coeff[ 26]    *x22    *x45    
        +coeff[ 27]*x11*x21*x31    *x52
        ;

    return v_y_e_q1ex_600                                ;
}
float p_e_q1ex_600                                (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.8169272E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.29997E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.11932893E-03, 0.31166054E-01, 0.65138906E-01,-0.13600738E-02,
        -0.15217011E-02, 0.96365903E-03, 0.67006459E-03, 0.66411402E-03,
         0.45136485E-03, 0.29139014E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_600                                =v_p_e_q1ex_600                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_600                                ;
}
float l_e_q1ex_600                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1670006E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.29997E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17146039E-02,-0.29600415E-04,-0.35054053E-02,-0.11632316E-02,
        -0.20025687E-02,-0.39537175E-03, 0.13671336E-02,-0.37950554E-03,
        -0.31662672E-02, 0.81175531E-03,-0.26331842E-02,-0.18405783E-02,
         0.29931220E-02, 0.98446377E-04,-0.60866954E-04,-0.31431063E-03,
         0.65323163E-03, 0.41742518E-03,-0.34025562E-03, 0.11198119E-03,
         0.23590085E-03,-0.12714303E-02, 0.11823296E-02, 0.68968191E-03,
        -0.62324718E-03,-0.68897480E-03, 0.18656848E-02, 0.23106503E-03,
         0.99506183E-03, 0.17728662E-03,-0.83291507E-05,-0.58607879E-03,
        -0.58739999E-03, 0.44535525E-03, 0.12869012E-02, 0.48489432E-03,
        -0.11428219E-03, 0.11116116E-03,-0.54742658E-03, 0.42946389E-03,
         0.58967352E-03, 0.24723212E-03,-0.17316820E-02,-0.67172426E-03,
        -0.13585752E-02, 0.61261444E-03, 0.56313042E-03, 0.95155690E-03,
         0.92350767E-03, 0.19997130E-02,-0.15047075E-02, 0.62776945E-03,
        -0.98185893E-03, 0.14779868E-02, 0.24814543E-03,-0.11992792E-02,
        -0.39165004E-03, 0.10783209E-02,-0.17340353E-02,-0.11867259E-02,
        -0.54783281E-03,-0.75389806E-03,-0.39921160E-03,-0.10377903E-02,
        -0.73605165E-03,-0.12608747E-02,-0.24445975E-03, 0.49860025E-03,
         0.11429293E-02, 0.61596860E-03,-0.19089159E-02,-0.12116827E-02,
         0.23250042E-02, 0.13075168E-02,-0.86405860E-04, 0.93873954E-04,
         0.14345814E-03, 0.49976094E-04,-0.11080132E-03, 0.12103942E-03,
         0.56710538E-04, 0.19999806E-03, 0.17969446E-03,-0.22087929E-03,
        -0.79622514E-04,-0.14150003E-03, 0.60893431E-05, 0.79462305E-04,
         0.88215886E-04,-0.16812549E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x34        
        +coeff[  6]    *x22*x31*x41    
        +coeff[  7]*x11    *x33        
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[  8]    *x22*x33*x41    
        +coeff[  9]*x12*x22    *x42    
        +coeff[ 10]        *x34*x41*x53
        +coeff[ 11]        *x33*x42*x53
        +coeff[ 12]        *x33*x41*x54
        +coeff[ 13]    *x21        *x51
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11    *x31    *x51
        +coeff[ 16]        *x33*x41    
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 17]        *x32*x41*x51
        +coeff[ 18]*x11    *x32*x41    
        +coeff[ 19]*x11    *x31*x42    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11*x21    *x41*x51
        +coeff[ 22]*x11    *x31*x41*x51
        +coeff[ 23]*x12*x21*x31        
        +coeff[ 24]*x12        *x41*x51
        +coeff[ 25]    *x22*x33        
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x31*x43*x51
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]    *x21*x32    *x52
        +coeff[ 31]        *x31*x41*x53
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]*x11*x21*x33        
        +coeff[ 34]*x11    *x31*x42*x51
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 35]*x11*x21        *x53
        +coeff[ 36]*x12*x21*x31*x41    
        +coeff[ 37]*x12*x21    *x42    
        +coeff[ 38]*x12    *x31*x41*x51
        +coeff[ 39]    *x24*x31    *x51
        +coeff[ 40]        *x31*x44*x51
        +coeff[ 41]    *x22*x31*x41*x52
        +coeff[ 42]        *x33*x41*x52
        +coeff[ 43]    *x21*x31*x42*x52
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 44]        *x32*x42*x52
        +coeff[ 45]*x11*x22*x31*x42    
        +coeff[ 46]*x11*x23*x31    *x51
        +coeff[ 47]*x11*x23    *x41*x51
        +coeff[ 48]*x11*x21    *x43*x51
        +coeff[ 49]*x11*x21*x31*x41*x52
        +coeff[ 50]*x11    *x31*x41*x53
        +coeff[ 51]*x11        *x41*x54
        +coeff[ 52]*x12*x23*x31        
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 53]*x12    *x32*x41*x51
        +coeff[ 54]*x13*x21*x32        
        +coeff[ 55]*x13*x21*x31*x41    
        +coeff[ 56]*x13*x21    *x42    
        +coeff[ 57]    *x23*x31*x43    
        +coeff[ 58]    *x21*x32*x44    
        +coeff[ 59]    *x23*x32    *x52
        +coeff[ 60]*x13        *x41*x52
        +coeff[ 61]    *x22    *x43*x52
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 62]        *x31*x44*x52
        +coeff[ 63]*x11*x23*x31*x41*x51
        +coeff[ 64]*x11*x23    *x42*x51
        +coeff[ 65]*x11    *x31*x42*x53
        +coeff[ 66]*x12*x23*x32        
        +coeff[ 67]*x12*x24    *x41    
        +coeff[ 68]*x12*x23    *x42    
        +coeff[ 69]*x12*x22*x31    *x52
        +coeff[ 70]    *x21*x33*x43*x51
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 71]    *x22*x31*x43*x52
        +coeff[ 72]        *x32*x44*x52
        +coeff[ 73]    *x21*x31*x43*x53
        +coeff[ 74]    *x21            
        +coeff[ 75]    *x21*x31        
        +coeff[ 76]        *x32        
        +coeff[ 77]    *x21    *x41    
        +coeff[ 78]        *x31    *x51
        +coeff[ 79]            *x41*x51
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 80]*x11*x21            
        +coeff[ 81]    *x22*x31        
        +coeff[ 82]    *x21*x32        
        +coeff[ 83]    *x21*x31*x41    
        +coeff[ 84]    *x21*x31    *x51
        +coeff[ 85]        *x32    *x51
        +coeff[ 86]            *x42*x51
        +coeff[ 87]                *x53
        +coeff[ 88]*x11        *x41*x51
    ;
    v_l_e_q1ex_600                                =v_l_e_q1ex_600                                
        +coeff[ 89]*x12*x21            
        ;

    return v_l_e_q1ex_600                                ;
}
float x_e_q1ex_500                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.1442105E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.14466599E-02, 0.30051649E-02, 0.12371822E+00, 0.10937627E-02,
        -0.15014649E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_500                                ;
}
float t_e_q1ex_500                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2405064E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.24131694E-04,-0.19484406E-02,-0.37502972E-03, 0.22411724E-02,
        -0.68725873E-03,-0.48967561E-03,-0.10514361E-02,-0.99263771E-03,
         0.67590358E-04,-0.86381457E-04,-0.10592921E-02,-0.77343499E-03,
        -0.11058588E-02,-0.99667744E-03,-0.58444686E-04,-0.55115448E-04,
        -0.26930000E-04, 0.97543591E-04,-0.34445850E-03,-0.50749100E-03,
         0.73588839E-04,-0.20530795E-04, 0.75848147E-04, 0.22649843E-04,
        -0.57068850E-04,-0.76233305E-05, 0.82959860E-06, 0.19411404E-04,
        -0.37195183E-04,-0.21677746E-04, 0.99731014E-05,-0.15792188E-04,
         0.40437219E-04, 0.31416843E-04,-0.16603173E-04, 0.43355998E-04,
         0.24469806E-04,-0.35358604E-04, 0.76943514E-04,-0.66769138E-06,
        -0.27766137E-05,-0.14958030E-05,-0.17530469E-05, 0.14836545E-05,
         0.27591102E-05,-0.39222791E-05, 0.33304286E-05, 0.29191649E-05,
         0.17995488E-05,-0.23725540E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_500                                =v_t_e_q1ex_500                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_500                                =v_t_e_q1ex_500                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x32*x42*x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x32*x41    
    ;
    v_t_e_q1ex_500                                =v_t_e_q1ex_500                                
        +coeff[ 26]*x11*x22        *x51
        +coeff[ 27]*x11    *x32    *x51
        +coeff[ 28]*x11*x22    *x42    
        +coeff[ 29]*x11    *x32*x42    
        +coeff[ 30]    *x21*x33    *x51
        +coeff[ 31]*x11        *x42*x52
        +coeff[ 32]    *x21*x32*x43    
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]*x11*x23    *x41*x51
    ;
    v_t_e_q1ex_500                                =v_t_e_q1ex_500                                
        +coeff[ 35]*x11*x22    *x42*x51
        +coeff[ 36]    *x22*x31*x42*x51
        +coeff[ 37]*x11    *x32*x42*x51
        +coeff[ 38]    *x21*x31*x43*x51
        +coeff[ 39]            *x41    
        +coeff[ 40]    *x21    *x41    
        +coeff[ 41]            *x42    
        +coeff[ 42]        *x31    *x51
        +coeff[ 43]            *x41*x51
    ;
    v_t_e_q1ex_500                                =v_t_e_q1ex_500                                
        +coeff[ 44]*x11*x21*x31        
        +coeff[ 45]    *x22    *x41    
        +coeff[ 46]        *x32*x41    
        +coeff[ 47]*x11*x21        *x51
        +coeff[ 48]    *x22        *x51
        +coeff[ 49]*x11    *x31    *x51
        ;

    return v_t_e_q1ex_500                                ;
}
float y_e_q1ex_500                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.6037595E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.58499520E-03, 0.91361977E-01, 0.68602651E-01,-0.64614613E-03,
        -0.61375590E-03, 0.11502751E-03,-0.17632405E-04,-0.14949044E-03,
         0.67308465E-04, 0.55736131E-04, 0.28854141E-04,-0.16492543E-03,
         0.37874888E-04,-0.17876742E-04, 0.24122915E-04,-0.16234299E-03,
        -0.34611141E-04,-0.22546748E-04,-0.22311004E-04,-0.19064964E-04,
        -0.82868355E-05, 0.20161184E-04, 0.19017674E-04,-0.54039243E-04,
        -0.10994365E-03,-0.39444225E-04,-0.91791944E-05, 0.98176042E-05,
        -0.18562494E-04, 0.22752074E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_500                                =v_y_e_q1ex_500                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x32*x42    
    ;
    v_y_e_q1ex_500                                =v_y_e_q1ex_500                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]    *x22*x33*x42    
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q1ex_500                                =v_y_e_q1ex_500                                
        +coeff[ 26]        *x33*x41*x51
        +coeff[ 27]*x12        *x42*x51
        +coeff[ 28]    *x21*x31*x43*x51
        +coeff[ 29]*x11*x21    *x41*x52
        ;

    return v_y_e_q1ex_500                                ;
}
float p_e_q1ex_500                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4606559E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.44559801E-03, 0.31144610E-01, 0.65098710E-01,-0.13595124E-02,
        -0.15222168E-02, 0.97642856E-03, 0.67548500E-03, 0.66641218E-03,
        -0.37314632E-04, 0.29278846E-03, 0.45353643E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_500                                =v_p_e_q1ex_500                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_500                                ;
}
float l_e_q1ex_500                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1649779E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60069E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.15825261E-02,-0.33514767E-02,-0.63028379E-03,-0.16257912E-02,
         0.53378375E-03, 0.15198834E-02, 0.17560148E-02,-0.11138172E-02,
         0.13377211E-02,-0.65373759E-04,-0.23071001E-03,-0.10509482E-03,
         0.35268167E-03, 0.22484150E-03, 0.25134030E-03,-0.53766061E-03,
         0.10052337E-03,-0.29715040E-03,-0.23739220E-03, 0.27586479E-03,
         0.46939641E-03, 0.47703170E-05, 0.52141200E-03, 0.19652507E-03,
        -0.12220853E-02,-0.17002778E-03, 0.15998400E-03,-0.67975931E-03,
        -0.65207609E-03,-0.29759936E-03,-0.74318034E-03,-0.54033287E-03,
        -0.48490308E-03,-0.12767778E-02,-0.14106595E-02, 0.13453568E-02,
         0.95330659E-04,-0.21270709E-02, 0.41965605E-03,-0.25395493E-04,
        -0.32257236E-03,-0.25742702E-03,-0.43732527E-03, 0.14630589E-02,
        -0.57897274E-03, 0.33560311E-03, 0.58844947E-03,-0.10175350E-02,
         0.77686354E-03,-0.33498005E-03, 0.71122102E-03,-0.12354929E-02,
        -0.12517029E-02, 0.32892611E-03,-0.22564773E-02, 0.14444470E-02,
         0.23244203E-02,-0.84894309E-04,-0.89474255E-03, 0.10645213E-02,
         0.12575558E-02,-0.17915739E-02, 0.57918910E-03,-0.34876252E-02,
         0.10851546E-02,-0.35910573E-03,-0.31166221E-02, 0.14963038E-02,
         0.77272579E-03, 0.15806671E-02, 0.14818455E-02, 0.37584623E-03,
         0.86437725E-03,-0.11984950E-02, 0.15229894E-02,-0.14135028E-02,
         0.53085038E-03, 0.46846413E-03,-0.27691049E-03, 0.17696553E-02,
        -0.43503800E-02, 0.85186155E-03, 0.75121102E-03,-0.15820820E-02,
         0.40199813E-02, 0.24341438E-02, 0.56051643E-03, 0.95052448E-04,
        -0.13879116E-03, 0.11185955E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x33    *x51
        +coeff[  5]    *x22*x32*x41    
        +coeff[  6]*x11*x21*x32    *x51
        +coeff[  7]*x11*x21*x33    *x51
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[  8]    *x22*x32*x43    
        +coeff[  9]    *x21            
        +coeff[ 10]            *x41    
        +coeff[ 11]        *x32        
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22        *x51
        +coeff[ 16]                *x53
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 17]*x11*x21        *x51
        +coeff[ 18]*x12        *x41    
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]            *x43*x51
        +coeff[ 22]*x11*x22        *x51
        +coeff[ 23]*x12        *x41*x51
        +coeff[ 24]    *x21*x31*x42*x51
        +coeff[ 25]    *x21    *x41*x53
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 26]*x11*x21*x33        
        +coeff[ 27]*x11*x22    *x41*x51
        +coeff[ 28]*x11        *x43*x51
        +coeff[ 29]*x11        *x41*x53
        +coeff[ 30]*x12*x21*x31*x41    
        +coeff[ 31]        *x34*x42    
        +coeff[ 32]        *x33*x43    
        +coeff[ 33]    *x22*x33    *x51
        +coeff[ 34]    *x22*x32*x41*x51
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 35]    *x22*x31*x42*x51
        +coeff[ 36]    *x21*x32*x42*x51
        +coeff[ 37]    *x21*x31*x43*x51
        +coeff[ 38]    *x23        *x53
        +coeff[ 39]    *x22*x31    *x53
        +coeff[ 40]        *x31*x42*x53
        +coeff[ 41]            *x43*x53
        +coeff[ 42]    *x22        *x54
        +coeff[ 43]*x11*x24*x31        
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 44]*x11*x22*x33        
        +coeff[ 45]*x11*x24    *x41    
        +coeff[ 46]*x11*x23*x31    *x51
        +coeff[ 47]*x11*x22*x32    *x51
        +coeff[ 48]*x11*x21*x31*x41*x52
        +coeff[ 49]*x11    *x32*x41*x52
        +coeff[ 50]*x12    *x32*x41*x51
        +coeff[ 51]*x12        *x41*x53
        +coeff[ 52]*x13*x22*x31        
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 53]*x13*x21*x32        
        +coeff[ 54]    *x24*x32*x41    
        +coeff[ 55]    *x22*x34    *x51
        +coeff[ 56]    *x23*x31*x42*x51
        +coeff[ 57]        *x34*x42*x51
        +coeff[ 58]    *x22*x32    *x53
        +coeff[ 59]    *x23    *x41*x53
        +coeff[ 60]    *x21*x31*x42*x53
        +coeff[ 61]*x11*x24*x31*x41    
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 62]*x11*x22*x31*x43    
        +coeff[ 63]*x11*x23*x31*x41*x51
        +coeff[ 64]*x11*x21*x33*x41*x51
        +coeff[ 65]*x11    *x34*x41*x51
        +coeff[ 66]*x11*x21*x32*x42*x51
        +coeff[ 67]*x11*x21*x31*x43*x51
        +coeff[ 68]*x11*x21    *x44*x51
        +coeff[ 69]*x11*x22*x31*x41*x52
        +coeff[ 70]*x11        *x43*x53
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 71]*x11        *x42*x54
        +coeff[ 72]*x12*x24    *x41    
        +coeff[ 73]*x12*x22*x32*x41    
        +coeff[ 74]*x12*x22    *x42*x51
        +coeff[ 75]*x12    *x32*x42*x51
        +coeff[ 76]*x12    *x31*x43*x51
        +coeff[ 77]*x12*x23        *x52
        +coeff[ 78]*x13*x24            
        +coeff[ 79]    *x24*x33    *x51
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 80]    *x23*x32*x42*x51
        +coeff[ 81]    *x24*x32    *x52
        +coeff[ 82]*x13        *x41*x53
        +coeff[ 83]    *x23*x31*x41*x53
        +coeff[ 84]    *x22*x32*x41*x53
        +coeff[ 85]    *x21*x31*x43*x53
        +coeff[ 86]    *x21*x31*x42*x54
        +coeff[ 87]                *x51
        +coeff[ 88]        *x31    *x51
    ;
    v_l_e_q1ex_500                                =v_l_e_q1ex_500                                
        +coeff[ 89]*x11    *x31        
        ;

    return v_l_e_q1ex_500                                ;
}
float x_e_q1ex_450                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.6755513E-03;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53995E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.67526108E-03, 0.30049253E-02, 0.12371258E+00, 0.10881617E-02,
        -0.14511145E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_450                                ;
}
float t_e_q1ex_450                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1553417E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53995E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.15779602E-04,-0.19469771E-02,-0.36404617E-03, 0.22524607E-02,
        -0.68298570E-03,-0.48046434E-03,-0.10707247E-02,-0.10023359E-02,
         0.74307238E-04,-0.10516387E-02,-0.76789246E-03,-0.10839085E-02,
        -0.97738497E-03,-0.68232526E-04,-0.49226957E-04,-0.79977275E-04,
         0.10432314E-03,-0.37241506E-03,-0.48825124E-03,-0.36006975E-04,
         0.57931586E-04, 0.37365160E-04,-0.24976041E-04,-0.20826041E-04,
        -0.54041877E-04, 0.94366704E-04, 0.48640641E-05, 0.14706131E-05,
        -0.16291897E-04, 0.52354389E-05, 0.12377728E-04,-0.33132408E-04,
         0.12110787E-04,-0.12009662E-04,-0.18990540E-04, 0.27749604E-04,
        -0.14225494E-04, 0.54810454E-04, 0.22545762E-04, 0.47151016E-05,
         0.29766639E-06, 0.25340098E-05,-0.14541168E-05, 0.79890748E-06,
         0.51797006E-05, 0.36108784E-05,-0.60229440E-05, 0.43137052E-05,
         0.19091412E-04,-0.73031611E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_450                                =v_t_e_q1ex_450                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23*x31*x41    
        +coeff[ 10]    *x23    *x42    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1ex_450                                =v_t_e_q1ex_450                                
        +coeff[ 17]    *x23*x32        
        +coeff[ 18]    *x21*x33*x41    
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x23*x32    *x51
        +coeff[ 22]*x11    *x32        
        +coeff[ 23]*x11*x21        *x52
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]    *x21*x32*x42*x51
    ;
    v_t_e_q1ex_450                                =v_t_e_q1ex_450                                
        +coeff[ 26]*x11*x21            
        +coeff[ 27]            *x42    
        +coeff[ 28]    *x22    *x42    
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]*x13*x22            
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11*x21*x31*x42    
        +coeff[ 33]*x12*x21        *x52
        +coeff[ 34]    *x22    *x41*x52
    ;
    v_t_e_q1ex_450                                =v_t_e_q1ex_450                                
        +coeff[ 35]*x11*x22*x31*x42    
        +coeff[ 36]*x11*x22    *x43    
        +coeff[ 37]    *x21*x33*x41*x51
        +coeff[ 38]*x11*x21*x31*x42*x51
        +coeff[ 39]    *x22            
        +coeff[ 40]*x11        *x41    
        +coeff[ 41]    *x21    *x41    
        +coeff[ 42]    *x22*x31        
        +coeff[ 43]*x12        *x41    
    ;
    v_t_e_q1ex_450                                =v_t_e_q1ex_450                                
        +coeff[ 44]    *x22    *x41    
        +coeff[ 45]    *x21*x31    *x51
        +coeff[ 46]*x12*x22            
        +coeff[ 47]*x12*x21*x31        
        +coeff[ 48]*x11*x22    *x41    
        +coeff[ 49]*x12*x21        *x51
        ;

    return v_t_e_q1ex_450                                ;
}
float y_e_q1ex_450                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.7223261E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53995E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.71080351E-04, 0.91374174E-01, 0.68600662E-01,-0.64293836E-03,
        -0.60900941E-03, 0.11366003E-03,-0.34280562E-04,-0.14121532E-03,
         0.59446298E-04, 0.28673021E-04, 0.28087703E-04, 0.25346062E-04,
        -0.14845155E-03,-0.16696773E-04,-0.32108244E-04,-0.12582644E-03,
         0.52256790E-04,-0.32454624E-04,-0.38437925E-05, 0.24554591E-04,
        -0.44096842E-05,-0.95572968E-05, 0.27105916E-05,-0.88680945E-05,
        -0.11340978E-04,-0.12303841E-04,-0.12815141E-03, 0.15495398E-04,
        -0.38684881E-04,-0.19341480E-04, 0.13719311E-04, 0.21655820E-04,
         0.67414453E-05,-0.30971816E-04,-0.16293874E-04,-0.16864915E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_450                                =v_y_e_q1ex_450                                
        +coeff[  8]            *x43    
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]        *x34*x41    
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x31*x44    
    ;
    v_y_e_q1ex_450                                =v_y_e_q1ex_450                                
        +coeff[ 17]    *x22*x33*x42    
        +coeff[ 18]    *x22            
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11    *x31*x41    
        +coeff[ 21]    *x21*x31*x42    
        +coeff[ 22]*x11*x22    *x41    
        +coeff[ 23]*x12    *x31*x41    
        +coeff[ 24]*x11*x21    *x41*x51
        +coeff[ 25]*x11    *x31*x41*x51
    ;
    v_y_e_q1ex_450                                =v_y_e_q1ex_450                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x12    *x31*x42    
        +coeff[ 30]*x11    *x31*x42*x51
        +coeff[ 31]*x11*x21*x33        
        +coeff[ 32]*x13        *x42    
        +coeff[ 33]*x11*x21*x31*x43    
        +coeff[ 34]*x11    *x32*x43    
    ;
    v_y_e_q1ex_450                                =v_y_e_q1ex_450                                
        +coeff[ 35]*x11*x23    *x42    
        ;

    return v_y_e_q1ex_450                                ;
}
float p_e_q1ex_450                                (float *x,int m){
    int ncoeff= 10;
    float avdat= -0.5331038E-04;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53995E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.49171518E-04, 0.31140840E-01, 0.65152131E-01,-0.13570295E-02,
        -0.15209735E-02, 0.96496288E-03, 0.66811591E-03, 0.65571110E-03,
         0.45026458E-03, 0.28752830E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_450                                =v_p_e_q1ex_450                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        ;

    return v_p_e_q1ex_450                                ;
}
float l_e_q1ex_450                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1701828E-02;
    float xmin[10]={
        -0.39997E-02,-0.60061E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53995E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16120840E-02,-0.32844197E-02, 0.41887641E-03,-0.11406321E-02,
        -0.18198416E-02,-0.34390955E-03, 0.44342632E-04, 0.51845069E-03,
         0.66905731E-03, 0.25935346E-03, 0.10872883E-03,-0.89430243E-04,
         0.79775689E-03,-0.14033713E-03,-0.99754652E-04,-0.35961729E-03,
         0.32490188E-04, 0.12024138E-03,-0.83293868E-04, 0.36659549E-03,
         0.42625592E-03,-0.51073512E-04,-0.51038514E-03,-0.37956468E-03,
        -0.41683309E-03, 0.48697411E-03, 0.36299202E-03,-0.98008961E-04,
         0.14458057E-03, 0.14172695E-03,-0.16162812E-03, 0.25731462E-03,
         0.23267767E-03, 0.29619350E-03, 0.10524344E-02,-0.16046466E-02,
        -0.59691432E-03,-0.30810095E-03,-0.88072493E-03, 0.24486110E-02,
        -0.54647721E-03,-0.62516468E-04, 0.10923381E-02,-0.85730841E-04,
         0.56802091E-03, 0.11630824E-02, 0.20734782E-02,-0.14722832E-02,
         0.28746543E-03, 0.28949318E-03,-0.34967621E-03,-0.12800576E-02,
        -0.40507913E-03,-0.32321902E-03,-0.27093390E-03, 0.13412242E-02,
         0.11523524E-02,-0.27003637E-03, 0.11159286E-02,-0.12878578E-02,
         0.62087830E-03,-0.76701021E-03, 0.39752488E-03, 0.75247657E-03,
        -0.55642094E-03,-0.12678554E-02, 0.10121670E-02,-0.32322921E-02,
        -0.86722639E-03,-0.61003893E-03, 0.11026522E-02,-0.93004212E-03,
         0.74692228E-03,-0.69509033E-03,-0.33483715E-03, 0.11210121E-02,
         0.25639548E-02, 0.27695400E-03,-0.70591527E-03,-0.11179029E-02,
        -0.11433032E-02,-0.53429866E-03, 0.64153888E-03, 0.30552535E-02,
        -0.11927264E-04,-0.88854454E-03,-0.10463967E-02,-0.14652431E-02,
        -0.12739884E-02, 0.18984954E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11            *x52
        +coeff[  6]*x11    *x31*x42*x51
        +coeff[  7]*x12*x21*x31*x41    
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[  8]*x11*x21    *x44*x51
        +coeff[  9]                *x51
        +coeff[ 10]*x11                
        +coeff[ 11]*x12                
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]            *x42*x51
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]            *x41*x52
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]*x12    *x31        
        +coeff[ 22]*x12            *x51
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]        *x34        
        +coeff[ 25]        *x33*x41    
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 26]    *x21    *x41*x52
        +coeff[ 27]                *x54
        +coeff[ 28]*x11*x22*x31        
        +coeff[ 29]*x11*x21*x32        
        +coeff[ 30]*x11*x21    *x42    
        +coeff[ 31]*x11    *x31*x42    
        +coeff[ 32]*x11        *x43    
        +coeff[ 33]*x11*x21*x31    *x51
        +coeff[ 34]*x11*x21    *x41*x51
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 35]*x11    *x31*x41*x51
        +coeff[ 36]    *x24    *x41    
        +coeff[ 37]    *x23    *x42    
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]    *x22*x31*x41*x51
        +coeff[ 40]        *x32*x41*x52
        +coeff[ 41]*x11*x22    *x42    
        +coeff[ 42]*x11    *x31*x41*x52
        +coeff[ 43]*x12    *x32*x41    
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 44]*x12    *x31*x42    
        +coeff[ 45]*x12    *x32    *x51
        +coeff[ 46]    *x23*x32*x41    
        +coeff[ 47]    *x21*x34*x41    
        +coeff[ 48]*x13    *x31    *x51
        +coeff[ 49]    *x22*x33    *x51
        +coeff[ 50]    *x21*x32*x42*x51
        +coeff[ 51]    *x23    *x41*x52
        +coeff[ 52]        *x33*x41*x52
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 53]    *x21*x32    *x53
        +coeff[ 54]*x11*x22*x32    *x51
        +coeff[ 55]*x11*x21*x33    *x51
        +coeff[ 56]*x11    *x33*x41*x51
        +coeff[ 57]*x11    *x33    *x52
        +coeff[ 58]*x11*x21*x31*x41*x52
        +coeff[ 59]*x11*x21    *x41*x53
        +coeff[ 60]*x12    *x31*x43    
        +coeff[ 61]*x12*x22    *x41*x51
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 62]*x12        *x43*x51
        +coeff[ 63]*x13*x21    *x42    
        +coeff[ 64]*x13        *x43    
        +coeff[ 65]*x13*x21*x31    *x51
        +coeff[ 66]*x13    *x31*x41*x51
        +coeff[ 67]    *x24*x31*x41*x51
        +coeff[ 68]    *x22*x31*x43*x51
        +coeff[ 69]        *x33*x43*x51
        +coeff[ 70]    *x21*x32*x42*x52
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 71]        *x33*x42*x52
        +coeff[ 72]    *x21*x31*x43*x52
        +coeff[ 73]*x11*x23*x32*x41    
        +coeff[ 74]*x11*x22*x33*x41    
        +coeff[ 75]*x11    *x32*x44    
        +coeff[ 76]*x11*x22*x31*x42*x51
        +coeff[ 77]*x11*x21*x31*x43*x51
        +coeff[ 78]*x11*x21*x33    *x52
        +coeff[ 79]*x11    *x33*x41*x52
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 80]*x11*x22    *x42*x52
        +coeff[ 81]*x11    *x32*x41*x53
        +coeff[ 82]*x12    *x33    *x52
        +coeff[ 83]*x12    *x32*x41*x52
        +coeff[ 84]*x12        *x43*x52
        +coeff[ 85]*x12        *x41*x54
        +coeff[ 86]*x13    *x32*x42    
        +coeff[ 87]*x13*x22*x31    *x51
        +coeff[ 88]*x13*x21*x31*x41*x51
    ;
    v_l_e_q1ex_450                                =v_l_e_q1ex_450                                
        +coeff[ 89]    *x23*x32*x42*x51
        ;

    return v_l_e_q1ex_450                                ;
}
float x_e_q1ex_449                                (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.7716591E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59980E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.77656907E-03, 0.30126970E-02, 0.42677428E-06, 0.12393691E+00,
         0.10776754E-02,-0.13927321E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_449                                ;
}
float t_e_q1ex_449                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1016305E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59980E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.10711931E-04,-0.19331737E-02, 0.87686218E-04, 0.71566908E-04,
         0.22200570E-02,-0.68914809E-03,-0.62399922E-03,-0.11752121E-02,
        -0.99665672E-03,-0.81380749E-04,-0.11577124E-02,-0.76522928E-03,
        -0.12897587E-02,-0.10699949E-02,-0.53105989E-04,-0.59139296E-04,
        -0.35551006E-04, 0.11826592E-03,-0.43364469E-03,-0.66586345E-03,
        -0.28022414E-04, 0.51150138E-04, 0.69792201E-04, 0.21431350E-04,
        -0.63611260E-04,-0.46462566E-04,-0.34887448E-05,-0.17218854E-04,
        -0.19321536E-04,-0.22254142E-04,-0.67152232E-05,-0.26456957E-04,
         0.24589915E-04, 0.31942852E-05,-0.76538936E-05, 0.10991460E-04,
         0.14687693E-04, 0.60416831E-04,-0.14910193E-04, 0.61715509E-04,
         0.60112372E-04, 0.26805421E-04,-0.23486160E-04, 0.19518720E-04,
        -0.17349973E-05,-0.28123211E-05,-0.32800631E-05, 0.84405037E-05,
         0.11369068E-04,-0.29459411E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x32        
        +coeff[  7]    *x21*x31*x41    
    ;
    v_t_e_q1ex_449                                =v_t_e_q1ex_449                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_449                                =v_t_e_q1ex_449                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1ex_449                                =v_t_e_q1ex_449                                
        +coeff[ 26]    *x22            
        +coeff[ 27]*x11*x21*x31*x41    
        +coeff[ 28]*x13*x21*x31        
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]    *x22    *x41*x52
        +coeff[ 34]    *x22        *x53
    ;
    v_t_e_q1ex_449                                =v_t_e_q1ex_449                                
        +coeff[ 35]*x13*x22    *x41    
        +coeff[ 36]*x13*x21    *x42    
        +coeff[ 37]*x11*x21*x31*x43    
        +coeff[ 38]*x11*x21*x33    *x51
        +coeff[ 39]    *x21*x32*x42*x51
        +coeff[ 40]    *x21*x31*x43*x51
        +coeff[ 41]*x12*x21    *x41*x52
        +coeff[ 42]    *x21*x32*x41*x52
        +coeff[ 43]    *x22    *x41*x53
    ;
    v_t_e_q1ex_449                                =v_t_e_q1ex_449                                
        +coeff[ 44]    *x21*x31        
        +coeff[ 45]            *x41*x51
        +coeff[ 46]*x12*x21            
        +coeff[ 47]*x11*x21*x31        
        +coeff[ 48]    *x22    *x41    
        +coeff[ 49]*x12            *x51
        ;

    return v_t_e_q1ex_449                                ;
}
float y_e_q1ex_449                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.2909555E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59980E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.27152605E-03, 0.91307677E-01, 0.76091871E-01,-0.63834077E-03,
        -0.67538192E-03, 0.11465152E-03,-0.51047999E-04,-0.12783617E-03,
         0.55166722E-04, 0.54172757E-04,-0.17251888E-03,-0.42808583E-05,
        -0.63840857E-05, 0.19710908E-04, 0.26210520E-04,-0.15322330E-03,
        -0.22043900E-04,-0.76938151E-04,-0.66748391E-04, 0.16110960E-04,
         0.13151547E-04, 0.11566261E-04,-0.36009314E-04,-0.16520276E-04,
        -0.27317059E-04,-0.31833493E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_449                                =v_y_e_q1ex_449                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x22    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]*x13*x21    *x41    
    ;
    v_y_e_q1ex_449                                =v_y_e_q1ex_449                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]    *x22*x33*x42    
        +coeff[ 19]            *x42*x52
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]*x12    *x31*x42    
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]            *x44*x52
        ;

    return v_y_e_q1ex_449                                ;
}
float p_e_q1ex_449                                (float *x,int m){
    int ncoeff= 12;
    float avdat=  0.7281417E-04;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59980E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
        -0.61509330E-04, 0.14498197E-06, 0.34317452E-01, 0.64840525E-01,
        -0.14973708E-02,-0.15112982E-02, 0.97639288E-03, 0.74351887E-03,
         0.71944040E-03,-0.75096548E-04, 0.28971289E-03, 0.55989326E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_q1ex_449                                =v_p_e_q1ex_449                                
        +coeff[  8]        *x31*x42    
        +coeff[  9]        *x34*x41    
        +coeff[ 10]            *x45    
        +coeff[ 11]        *x32*x41    
        ;

    return v_p_e_q1ex_449                                ;
}
float l_e_q1ex_449                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1625387E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59980E-01,-0.30046E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60071E-01, 0.59994E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16290837E-02, 0.58213147E-04,-0.34846973E-02,-0.87816291E-03,
        -0.19942557E-02,-0.12569166E-03,-0.28501861E-03, 0.17083173E-02,
         0.12555022E-02, 0.38952703E-03,-0.17164704E-03,-0.12577524E-02,
         0.12406915E-03,-0.41350297E-03, 0.57544111E-03,-0.88888506E-03,
         0.62424049E-03, 0.25753674E-03,-0.35378919E-03, 0.24553298E-03,
        -0.34497335E-03, 0.10245411E-02,-0.28028470E-03, 0.37649466E-03,
        -0.44049154E-03,-0.45792977E-03,-0.41822606E-03, 0.10104656E-02,
        -0.17140752E-04, 0.17221961E-02, 0.76281116E-03, 0.34408495E-03,
         0.99256076E-03,-0.71010354E-03,-0.54457516E-03,-0.47781816E-03,
        -0.48398180E-03, 0.48750063E-03,-0.34188572E-03, 0.62182709E-03,
         0.34815364E-03,-0.88897406E-03,-0.31781923E-02, 0.79934817E-03,
         0.45707650E-02,-0.83479047E-03,-0.43387967E-03,-0.55463001E-03,
         0.43054015E-03, 0.58665482E-03, 0.24229882E-03,-0.13603749E-02,
         0.14168093E-02,-0.61257777E-03, 0.90479135E-03, 0.31429558E-03,
         0.75165619E-03, 0.30874542E-03,-0.14396627E-02, 0.13841913E-02,
        -0.91542403E-03,-0.91245223E-03,-0.14878570E-02,-0.15574031E-02,
        -0.47775940E-03, 0.20763096E-02, 0.23070243E-03, 0.11247521E-02,
        -0.10715498E-02, 0.19285261E-02, 0.38802987E-02,-0.19439889E-03,
         0.19516108E-02,-0.17906283E-02,-0.23369570E-02,-0.85066527E-03,
        -0.54186530E-03,-0.30984487E-02,-0.87891449E-03, 0.18958742E-02,
         0.13012913E-02,-0.75001852E-03,-0.56199171E-03,-0.13948409E-02,
         0.20147983E-02,-0.14612450E-02,-0.34446406E-03, 0.57778070E-04,
         0.23330805E-03, 0.88628323E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x33        
        +coeff[  6]        *x34        
        +coeff[  7]*x12*x22    *x41*x52
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[  8]*x12    *x31    *x54
        +coeff[  9]                *x52
        +coeff[ 10]    *x23            
        +coeff[ 11]    *x22*x31        
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x12    *x31        
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 17]        *x32*x42    
        +coeff[ 18]    *x21    *x43    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]    *x21*x31*x41*x51
        +coeff[ 22]    *x21    *x41*x52
        +coeff[ 23]            *x42*x52
        +coeff[ 24]    *x21        *x53
        +coeff[ 25]                *x54
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 26]*x11    *x33        
        +coeff[ 27]*x11*x22        *x51
        +coeff[ 28]*x11        *x42*x51
        +coeff[ 29]*x11    *x31    *x52
        +coeff[ 30]*x12    *x31*x41    
        +coeff[ 31]*x12*x21        *x51
        +coeff[ 32]    *x24*x31        
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x23    *x41*x51
        +coeff[ 37]            *x43*x52
        +coeff[ 38]            *x41*x54
        +coeff[ 39]*x11*x23*x31        
        +coeff[ 40]*x11*x22*x32        
        +coeff[ 41]*x11*x21*x33        
        +coeff[ 42]*x11*x21*x31*x42    
        +coeff[ 43]*x11*x22*x31    *x51
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 44]*x11    *x31*x41*x52
        +coeff[ 45]*x12    *x33        
        +coeff[ 46]*x12    *x32*x41    
        +coeff[ 47]*x12    *x31*x42    
        +coeff[ 48]    *x21*x34*x41    
        +coeff[ 49]*x13    *x31    *x51
        +coeff[ 50]    *x23*x32    *x51
        +coeff[ 51]    *x23*x31*x41*x51
        +coeff[ 52]    *x22*x31*x42*x51
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 53]        *x33*x42*x51
        +coeff[ 54]    *x23    *x41*x52
        +coeff[ 55]    *x22    *x41*x53
        +coeff[ 56]*x11*x23*x31*x41    
        +coeff[ 57]*x11*x21    *x44    
        +coeff[ 58]*x11*x24        *x51
        +coeff[ 59]*x11*x22    *x42*x51
        +coeff[ 60]*x11        *x44*x51
        +coeff[ 61]*x11*x22*x31    *x52
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 62]*x11    *x31    *x54
        +coeff[ 63]*x12*x22*x31*x41    
        +coeff[ 64]*x12*x21*x31    *x52
        +coeff[ 65]    *x22*x33*x42    
        +coeff[ 66]    *x24        *x53
        +coeff[ 67]    *x23    *x41*x53
        +coeff[ 68]*x11*x22*x33*x41    
        +coeff[ 69]*x11*x21*x33*x42    
        +coeff[ 70]*x11*x22*x31*x43    
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 71]*x11*x22    *x44    
        +coeff[ 72]*x11*x21*x31*x44    
        +coeff[ 73]*x11*x22*x33    *x51
        +coeff[ 74]*x11*x22*x31*x41*x52
        +coeff[ 75]*x11*x22*x31    *x53
        +coeff[ 76]*x11    *x33    *x53
        +coeff[ 77]*x11    *x31*x41*x54
        +coeff[ 78]*x13*x21*x33        
        +coeff[ 79]*x13    *x33*x41    
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 80]*x13*x22    *x42    
        +coeff[ 81]*x13    *x31*x43    
        +coeff[ 82]*x13*x22        *x52
        +coeff[ 83]*x13    *x31*x41*x52
        +coeff[ 84]    *x24*x31*x41*x52
        +coeff[ 85]    *x22*x33*x41*x52
        +coeff[ 86]*x13        *x41*x53
        +coeff[ 87]    *x21            
        +coeff[ 88]        *x31        
    ;
    v_l_e_q1ex_449                                =v_l_e_q1ex_449                                
        +coeff[ 89]            *x41    
        ;

    return v_l_e_q1ex_449                                ;
}
float x_e_q1ex_400                                (float *x,int m){
    int ncoeff=  5;
    float avdat=  0.2475198E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60042E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
        -0.27772097E-03, 0.30136893E-02, 0.12394977E+00, 0.10813606E-02,
        -0.14329494E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_400                                ;
}
float t_e_q1ex_400                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1215666E-04;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60042E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.12006602E-04,-0.19285309E-02, 0.16133819E-03, 0.21909643E-02,
        -0.68676076E-03,-0.62680896E-03,-0.11623037E-02,-0.98569971E-03,
         0.70565628E-04,-0.80094440E-04,-0.11635879E-02,-0.13067491E-02,
        -0.10623907E-02,-0.61244107E-04,-0.57452697E-04, 0.11936271E-03,
         0.11120115E-03,-0.68970380E-03,-0.76574954E-03,-0.31272157E-04,
        -0.35556321E-04, 0.88975765E-04,-0.43624267E-03, 0.25525829E-04,
        -0.63414816E-04,-0.52446525E-04,-0.16065867E-04, 0.20294310E-04,
        -0.48596835E-05,-0.86538521E-05, 0.29350326E-06, 0.52183705E-05,
        -0.84328403E-05, 0.17422111E-04, 0.17193290E-04, 0.94497054E-05,
         0.82062379E-05, 0.12471774E-04,-0.87708568E-05,-0.78405265E-05,
         0.28850272E-04, 0.10437595E-04,-0.52604577E-04, 0.27031836E-04,
         0.54439963E-04,-0.31635071E-04,-0.16899659E-04, 0.21474369E-04,
        -0.37112663E-04, 0.25362600E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_400                                =v_t_e_q1ex_400                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x42*x51
    ;
    v_t_e_q1ex_400                                =v_t_e_q1ex_400                                
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x23    *x42    
        +coeff[ 19]*x11    *x32        
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x23*x32        
        +coeff[ 23]    *x23        *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1ex_400                                =v_t_e_q1ex_400                                
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11*x21*x31*x41    
        +coeff[ 28]*x12        *x41*x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]        *x31*x42*x51
        +coeff[ 31]*x11*x21        *x52
        +coeff[ 32]    *x21*x31    *x52
        +coeff[ 33]    *x21        *x53
        +coeff[ 34]*x11*x21*x33        
    ;
    v_t_e_q1ex_400                                =v_t_e_q1ex_400                                
        +coeff[ 35]*x12*x22    *x41    
        +coeff[ 36]*x11*x23        *x51
        +coeff[ 37]*x11*x22*x31    *x51
        +coeff[ 38]*x12        *x42*x51
        +coeff[ 39]    *x21    *x43*x51
        +coeff[ 40]*x12*x22    *x41*x51
        +coeff[ 41]*x12*x21*x31*x41*x51
        +coeff[ 42]*x12*x21    *x42*x51
        +coeff[ 43]    *x22*x31*x42*x51
    ;
    v_t_e_q1ex_400                                =v_t_e_q1ex_400                                
        +coeff[ 44]    *x21*x31*x43*x51
        +coeff[ 45]*x11*x21*x31*x41*x52
        +coeff[ 46]    *x22*x31*x41*x52
        +coeff[ 47]*x12*x21        *x53
        +coeff[ 48]    *x21*x32    *x53
        +coeff[ 49]    *x22    *x41*x53
        ;

    return v_t_e_q1ex_400                                ;
}
float y_e_q1ex_400                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.6873264E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60042E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.71276072E-03, 0.91255933E-01, 0.76081179E-01,-0.63420145E-03,
        -0.66627125E-03, 0.12083565E-03,-0.60785635E-04,-0.14540549E-03,
         0.46292116E-04, 0.64471802E-04, 0.29773368E-04, 0.27188402E-04,
        -0.14825840E-03, 0.10109442E-04,-0.23579591E-04,-0.28596758E-04,
        -0.15972387E-03,-0.16058184E-04, 0.40616113E-04,-0.47315869E-04,
        -0.34027953E-05,-0.53703770E-05,-0.92741247E-05,-0.99424287E-05,
        -0.38065818E-05,-0.10517178E-04, 0.93975905E-05,-0.14083782E-03,
         0.16514472E-04, 0.17183465E-04,-0.10759640E-04,-0.18262179E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_400                                =v_y_e_q1ex_400                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]    *x22    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]        *x31*x42*x51
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_y_e_q1ex_400                                =v_y_e_q1ex_400                                
        +coeff[ 17]*x11*x21*x32*x41    
        +coeff[ 18]    *x22*x31*x44    
        +coeff[ 19]    *x24*x33        
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]*x11        *x42    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]        *x34        
        +coeff[ 25]        *x31*x44    
    ;
    v_y_e_q1ex_400                                =v_y_e_q1ex_400                                
        +coeff[ 26]        *x31*x41*x52
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x12        *x43    
        +coeff[ 29]*x12    *x31*x42    
        +coeff[ 30]    *x21    *x42*x52
        +coeff[ 31]    *x21*x31*x43*x51
        ;

    return v_y_e_q1ex_400                                ;
}
float p_e_q1ex_400                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.5120818E-03;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60042E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.52842282E-03, 0.34270763E-01, 0.64774834E-01,-0.14978865E-02,
        -0.15070870E-02, 0.97293314E-03, 0.74364111E-03, 0.72184124E-03,
        -0.64211556E-04, 0.28301784E-03, 0.55331783E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_400                                =v_p_e_q1ex_400                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_400                                ;
}
float l_e_q1ex_400                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1635206E-02;
    float xmin[10]={
        -0.39997E-02,-0.60071E-01,-0.59991E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60042E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16948477E-02,-0.35830529E-02,-0.86724543E-03,-0.14266979E-02,
        -0.27934564E-03,-0.33563696E-03, 0.11003031E-03,-0.46967863E-04,
         0.11410023E-03,-0.29007315E-02,-0.38748138E-03,-0.10183199E-02,
        -0.39868703E-03, 0.86084729E-04, 0.11167539E-03,-0.17930592E-03,
        -0.35719579E-03, 0.15735049E-02,-0.23327504E-03, 0.26568052E-03,
        -0.58516022E-03, 0.30521621E-03, 0.74528571E-03, 0.66498294E-03,
        -0.22687347E-03,-0.31587374E-03,-0.36859154E-03, 0.11842744E-02,
        -0.96615855E-04, 0.20624968E-03, 0.75175345E-03, 0.56456932E-03,
         0.70250640E-03,-0.16215099E-02,-0.14102317E-02,-0.73759782E-03,
        -0.67729520E-03,-0.19580111E-03,-0.22707108E-03, 0.44566189E-03,
        -0.37477268E-04, 0.11571309E-03, 0.19718001E-03, 0.65475563E-03,
         0.84066531E-03,-0.10290936E-02,-0.18609550E-02,-0.11414526E-02,
         0.14409380E-02, 0.11056844E-02, 0.12130868E-02, 0.82397147E-03,
         0.42398539E-03, 0.52343711E-03,-0.59315457E-03, 0.74858213E-03,
        -0.14015799E-02,-0.80884108E-03, 0.46117837E-03,-0.73152845E-03,
         0.37188872E-03, 0.17110361E-02,-0.46583608E-03, 0.12988204E-02,
         0.85444568E-03, 0.15820060E-02,-0.31127734E-03, 0.14299641E-02,
         0.65006030E-03, 0.92348526E-03,-0.63135923E-03, 0.11818996E-02,
         0.54501439E-03, 0.10288489E-02,-0.22261173E-02,-0.46415473E-03,
        -0.77888061E-03, 0.18624238E-02, 0.10387001E-02,-0.64032199E-03,
         0.16837828E-02, 0.24283666E-02, 0.13905753E-02, 0.75407862E-03,
        -0.10192002E-02, 0.13769964E-02, 0.10720990E-02, 0.31911688E-04,
        -0.45197816E-04,-0.11397369E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]*x11    *x31        
        +coeff[  5]        *x31*x41*x51
        +coeff[  6]*x11    *x31*x41    
        +coeff[  7]    *x22*x32        
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[  8]        *x34        
        +coeff[  9]        *x31*x42*x52
        +coeff[ 10]*x11*x23        *x51
        +coeff[ 11]*x13    *x31*x41*x52
        +coeff[ 12]        *x32        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]    *x23    *x41    
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]            *x42*x52
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 17]*x11    *x32    *x51
        +coeff[ 18]*x11        *x42*x51
        +coeff[ 19]*x12*x21    *x41    
        +coeff[ 20]*x12        *x42    
        +coeff[ 21]*x12        *x41*x51
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]*x13            *x51
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 26]    *x21*x31*x42*x51
        +coeff[ 27]        *x31*x43*x51
        +coeff[ 28]    *x22        *x53
        +coeff[ 29]    *x21    *x41*x53
        +coeff[ 30]*x11    *x33*x41    
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x11*x21*x31    *x52
        +coeff[ 34]*x11    *x32    *x52
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 35]*x12    *x32*x41    
        +coeff[ 36]*x12*x22        *x51
        +coeff[ 37]*x13*x22            
        +coeff[ 38]*x13*x21*x31        
        +coeff[ 39]    *x23*x33        
        +coeff[ 40]    *x22*x33    *x51
        +coeff[ 41]    *x24        *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]*x11*x24        *x51
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 44]*x11*x23*x31    *x51
        +coeff[ 45]*x11*x21*x33    *x51
        +coeff[ 46]*x11    *x34    *x51
        +coeff[ 47]*x11*x22    *x42*x51
        +coeff[ 48]*x11*x21*x31*x42*x51
        +coeff[ 49]*x11    *x32*x42*x51
        +coeff[ 50]*x11*x21*x32    *x52
        +coeff[ 51]*x11*x21*x31*x41*x52
        +coeff[ 52]*x11*x21    *x41*x53
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 53]*x12    *x32*x42    
        +coeff[ 54]*x12    *x31*x43    
        +coeff[ 55]*x12*x22*x31    *x51
        +coeff[ 56]*x12*x21*x31*x41*x51
        +coeff[ 57]*x12*x21    *x42*x51
        +coeff[ 58]*x12*x22        *x52
        +coeff[ 59]*x12        *x41*x53
        +coeff[ 60]*x13    *x33        
        +coeff[ 61]    *x22    *x44*x51
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 62]*x13*x21        *x52
        +coeff[ 63]        *x33*x42*x52
        +coeff[ 64]        *x31*x44*x52
        +coeff[ 65]    *x21*x31*x42*x53
        +coeff[ 66]            *x44*x53
        +coeff[ 67]        *x31*x42*x54
        +coeff[ 68]*x11    *x34*x42    
        +coeff[ 69]*x11    *x32*x43*x51
        +coeff[ 70]*x11*x22    *x41*x53
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 71]*x11    *x32    *x54
        +coeff[ 72]*x12*x24    *x41    
        +coeff[ 73]*x12*x22*x31*x41*x51
        +coeff[ 74]*x12*x22    *x42*x51
        +coeff[ 75]*x12*x22*x31    *x52
        +coeff[ 76]*x12*x21*x32    *x52
        +coeff[ 77]*x12*x22        *x53
        +coeff[ 78]*x13*x21*x33        
        +coeff[ 79]        *x34*x44    
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 80]*x13*x21*x31    *x52
        +coeff[ 81]    *x22*x32*x42*x52
        +coeff[ 82]    *x22*x31*x43*x52
        +coeff[ 83]    *x21*x31*x44*x52
        +coeff[ 84]    *x22*x33    *x53
        +coeff[ 85]    *x23*x31*x41*x53
        +coeff[ 86]    *x21*x32*x42*x53
        +coeff[ 87]    *x21            
        +coeff[ 88]            *x41    
    ;
    v_l_e_q1ex_400                                =v_l_e_q1ex_400                                
        +coeff[ 89]    *x21*x31        
        ;

    return v_l_e_q1ex_400                                ;
}
float x_e_q1ex_350                                (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.2040372E-03;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59989E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60057E-01, 0.59997E-01, 0.30012E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.21120514E-03, 0.12399442E+00, 0.35617168E-05, 0.30136455E-02,
         0.10816378E-02,-0.14500547E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_350                                ;
}
float t_e_q1ex_350                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.2038526E-04;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59989E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60057E-01, 0.59997E-01, 0.30012E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.19914347E-04,-0.19285695E-02, 0.25726014E-03, 0.22053686E-02,
        -0.68525603E-03,-0.62357512E-03,-0.11681615E-02,-0.98302565E-03,
         0.70826565E-04,-0.85644373E-04,-0.11481058E-02,-0.12922654E-02,
        -0.10744516E-02,-0.52022533E-04,-0.50367766E-04,-0.32067132E-04,
         0.15416867E-03,-0.65221020E-03,-0.76157425E-03, 0.39252198E-04,
        -0.18418894E-04,-0.42908642E-03, 0.12029896E-04, 0.63068350E-04,
         0.65166350E-04,-0.65126871E-04,-0.11646652E-05,-0.11533278E-04,
         0.14006187E-04, 0.10162139E-04, 0.22187716E-04,-0.57491652E-05,
         0.82940614E-05,-0.29733537E-04,-0.41258081E-04,-0.25317366E-04,
        -0.20680221E-04,-0.63901566E-05, 0.13609826E-04, 0.47785274E-04,
        -0.27253342E-04, 0.14887601E-04,-0.15105208E-04, 0.20658794E-04,
        -0.15437148E-04, 0.19970326E-04,-0.89053083E-06,-0.18441815E-05,
        -0.19439819E-05,-0.21850185E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_350                                =v_t_e_q1ex_350                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x21*x32*x42    
        +coeff[ 12]    *x21*x31*x43    
        +coeff[ 13]*x11*x22            
        +coeff[ 14]*x11    *x31*x41    
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]    *x21*x31*x41*x51
    ;
    v_t_e_q1ex_350                                =v_t_e_q1ex_350                                
        +coeff[ 17]    *x21*x33*x41    
        +coeff[ 18]    *x23    *x42    
        +coeff[ 19]    *x21*x32*x42*x51
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]    *x23*x32        
        +coeff[ 22]    *x23    *x42*x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]*x11*x22*x31*x41    
    ;
    v_t_e_q1ex_350                                =v_t_e_q1ex_350                                
        +coeff[ 26]    *x22    *x41    
        +coeff[ 27]*x12*x21*x31        
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]*x13*x21*x31        
        +coeff[ 33]*x11*x22*x32        
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1ex_350                                =v_t_e_q1ex_350                                
        +coeff[ 35]*x11    *x32*x42    
        +coeff[ 36]*x11    *x31*x43    
        +coeff[ 37]*x13            *x52
        +coeff[ 38]    *x22    *x41*x52
        +coeff[ 39]*x11*x23*x31*x41    
        +coeff[ 40]*x11*x21*x33*x41    
        +coeff[ 41]*x11*x23    *x42    
        +coeff[ 42]*x12*x21    *x43    
        +coeff[ 43]*x11*x22*x32    *x51
    ;
    v_t_e_q1ex_350                                =v_t_e_q1ex_350                                
        +coeff[ 44]*x11*x23    *x41*x51
        +coeff[ 45]*x11*x22*x31    *x52
        +coeff[ 46]        *x31        
        +coeff[ 47]*x11    *x31        
        +coeff[ 48]    *x22        *x51
        +coeff[ 49]*x11    *x31    *x51
        ;

    return v_t_e_q1ex_350                                ;
}
float y_e_q1ex_350                                (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.8082787E-03;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59989E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60057E-01, 0.59997E-01, 0.30012E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.81946934E-03, 0.91162406E-01, 0.76048605E-01,-0.62472990E-03,
        -0.67120150E-03, 0.11188991E-03,-0.38734695E-04,-0.12116013E-03,
         0.50882059E-04,-0.19629251E-04, 0.58851634E-04,-0.16544892E-03,
        -0.35355549E-05, 0.24809795E-04, 0.27741558E-04,-0.14031568E-03,
        -0.57079335E-04, 0.35539390E-04,-0.12876319E-04,-0.13524830E-04,
        -0.12453874E-03,-0.13018886E-04,-0.27848348E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_350                                =v_y_e_q1ex_350                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1ex_350                                =v_y_e_q1ex_350                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]    *x22*x31*x42    
        +coeff[ 21]    *x21    *x42*x52
        +coeff[ 22]*x11*x23*x31        
        ;

    return v_y_e_q1ex_350                                ;
}
float p_e_q1ex_350                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.5370241E-03;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59989E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60057E-01, 0.59997E-01, 0.30012E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.54646784E-03, 0.34207452E-01, 0.64656153E-01,-0.14924677E-02,
        -0.15014122E-02, 0.96837693E-03, 0.74222655E-03, 0.71455061E-03,
        -0.77256358E-04, 0.28400001E-03, 0.56270487E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_350                                =v_p_e_q1ex_350                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_350                                ;
}
float l_e_q1ex_350                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1683099E-02;
    float xmin[10]={
        -0.39997E-02,-0.60051E-01,-0.59989E-01,-0.30022E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60057E-01, 0.59997E-01, 0.30012E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16452078E-02,-0.11338948E-03,-0.32615541E-02,-0.10082396E-02,
        -0.19939120E-02,-0.30766072E-03,-0.11810154E-03, 0.57753175E-04,
        -0.15986768E-03,-0.12818145E-03,-0.17196972E-03, 0.12624013E-03,
        -0.40701379E-04, 0.12313160E-03, 0.11140318E-02, 0.63676744E-04,
         0.35940172E-03, 0.34378047E-03,-0.17305062E-03,-0.18945463E-03,
         0.61690464E-03,-0.75508363E-03, 0.40148769E-03,-0.49373077E-03,
        -0.17912815E-03, 0.39345204E-03,-0.52569015E-03,-0.10260449E-02,
         0.62827248E-03,-0.17807988E-02, 0.64946257E-03,-0.29451906E-03,
         0.65658492E-03, 0.75819495E-03, 0.44698510E-02, 0.67928567E-03,
         0.26001781E-03,-0.42758792E-03, 0.86232956E-03, 0.10111443E-02,
        -0.15148858E-02, 0.93526422E-03, 0.10111833E-02,-0.44836442E-03,
         0.52370044E-03, 0.61863213E-03,-0.11606617E-02, 0.34060478E-03,
         0.56453783E-03, 0.93750999E-03, 0.76931802E-03,-0.81769918E-03,
        -0.51038736E-03,-0.75649895E-03, 0.56910719E-03, 0.61715022E-03,
        -0.12761301E-02,-0.21971546E-02,-0.10907571E-02,-0.81715197E-03,
        -0.14432083E-02,-0.34091820E-02, 0.50757802E-03, 0.44736656E-03,
         0.65499940E-03,-0.95432473E-03, 0.32433335E-03,-0.70751808E-03,
        -0.12923096E-02, 0.11290463E-03, 0.18154909E-02, 0.44269944E-03,
         0.67234779E-03,-0.17238879E-02, 0.12488563E-02, 0.94217103E-03,
         0.21115258E-03,-0.10332146E-03, 0.91280781E-04,-0.18680347E-03,
        -0.12162328E-03, 0.81675251E-04,-0.26369246E-03,-0.25052321E-03,
         0.15090720E-03,-0.94656505E-04,-0.14894782E-03, 0.10374914E-03,
        -0.42005003E-03,-0.15060800E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x32        
        +coeff[  6]        *x31        
        +coeff[  7]            *x41    
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[  8]    *x21*x31        
        +coeff[  9]        *x32        
        +coeff[ 10]    *x23            
        +coeff[ 11]        *x33        
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]        *x32    *x51
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]*x11        *x42    
        +coeff[ 16]*x11        *x41*x51
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 17]            *x44    
        +coeff[ 18]    *x21    *x41*x52
        +coeff[ 19]*x11*x22*x31        
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11*x21*x31    *x51
        +coeff[ 22]*x12    *x31*x41    
        +coeff[ 23]*x12        *x41*x51
        +coeff[ 24]    *x23*x32        
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 26]        *x33*x41*x51
        +coeff[ 27]        *x31*x43*x51
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x33        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11    *x33*x41    
        +coeff[ 32]*x11*x21*x32    *x51
        +coeff[ 33]*x11*x22    *x41*x51
        +coeff[ 34]*x11*x21*x31    *x52
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 35]*x11        *x42*x52
        +coeff[ 36]*x12    *x31    *x52
        +coeff[ 37]        *x34*x42    
        +coeff[ 38]    *x21    *x43*x52
        +coeff[ 39]*x11*x24*x31        
        +coeff[ 40]*x11*x22*x33        
        +coeff[ 41]*x11*x21*x33    *x51
        +coeff[ 42]*x11    *x33    *x52
        +coeff[ 43]*x11*x22        *x53
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 44]*x12*x22*x32        
        +coeff[ 45]*x12    *x32*x41*x51
        +coeff[ 46]*x12*x22        *x52
        +coeff[ 47]*x12    *x32    *x52
        +coeff[ 48]*x12            *x54
        +coeff[ 49]*x13*x22*x31        
        +coeff[ 50]    *x21*x34*x42    
        +coeff[ 51]*x13        *x43    
        +coeff[ 52]    *x22    *x44*x51
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 53]*x13    *x31    *x52
        +coeff[ 54]    *x24*x31    *x52
        +coeff[ 55]*x13        *x41*x52
        +coeff[ 56]    *x22*x32*x41*x52
        +coeff[ 57]*x11*x24    *x41*x51
        +coeff[ 58]*x11*x21*x32*x42*x51
        +coeff[ 59]*x11    *x32*x43*x51
        +coeff[ 60]*x11        *x44*x52
        +coeff[ 61]*x11*x21*x31    *x54
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 62]*x12*x23    *x42    
        +coeff[ 63]*x12*x22*x31*x42    
        +coeff[ 64]*x12    *x33*x42    
        +coeff[ 65]*x12*x21    *x44    
        +coeff[ 66]*x12*x24        *x51
        +coeff[ 67]*x12    *x31*x41*x53
        +coeff[ 68]*x12    *x31    *x54
        +coeff[ 69]*x13*x23*x31        
        +coeff[ 70]*x13*x21*x33        
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 71]*x13        *x44    
        +coeff[ 72]    *x21*x33*x44    
        +coeff[ 73]*x13*x21*x31    *x52
        +coeff[ 74]    *x21*x32*x42*x53
        +coeff[ 75]    *x22    *x42*x54
        +coeff[ 76]    *x21            
        +coeff[ 77]    *x21        *x51
        +coeff[ 78]            *x41*x51
        +coeff[ 79]                *x52
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 80]*x11        *x41    
        +coeff[ 81]*x11            *x51
        +coeff[ 82]    *x22*x31        
        +coeff[ 83]    *x21*x32        
        +coeff[ 84]        *x32*x41    
        +coeff[ 85]    *x21    *x41*x51
        +coeff[ 86]    *x21        *x52
        +coeff[ 87]        *x31    *x52
        +coeff[ 88]*x11*x21*x31        
    ;
    v_l_e_q1ex_350                                =v_l_e_q1ex_350                                
        +coeff[ 89]*x11    *x31    *x51
        ;

    return v_l_e_q1ex_350                                ;
}
float x_e_q1ex_300                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.1133786E-02;
    float xmin[10]={
        -0.39996E-02,-0.60057E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.11377154E-02, 0.30175098E-02, 0.12406513E+00, 0.10786003E-02,
        -0.13586722E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_300                                ;
}
float t_e_q1ex_300                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1686361E-05;
    float xmin[10]={
        -0.39996E-02,-0.60057E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.23034052E-05,-0.19213441E-02, 0.38973938E-03, 0.22089987E-02,
        -0.68326539E-03,-0.62407367E-03,-0.11803383E-02,-0.98433951E-03,
         0.70106442E-04,-0.83362764E-04,-0.11360099E-02,-0.77256118E-03,
        -0.12718635E-02,-0.10489952E-02,-0.53709133E-04,-0.58373931E-04,
        -0.41811054E-04, 0.11707487E-03, 0.93644390E-04,-0.42389453E-03,
        -0.64745964E-03,-0.25267469E-04, 0.65430591E-04,-0.58151483E-04,
         0.65764070E-05,-0.10700592E-04,-0.99466615E-05,-0.13640544E-05,
        -0.69055423E-05,-0.75124271E-05,-0.81063326E-05,-0.38182598E-05,
        -0.27602855E-04, 0.18138418E-04,-0.23923185E-04, 0.35104669E-04,
        -0.23439128E-04,-0.66376233E-05, 0.51602627E-04, 0.34992929E-04,
         0.60396658E-04, 0.16837816E-04, 0.36715675E-04,-0.16684528E-06,
         0.83160688E-06,-0.25823201E-05,-0.26183843E-05,-0.37222301E-05,
         0.23643652E-05, 0.37459245E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_300                                =v_t_e_q1ex_300                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_300                                =v_t_e_q1ex_300                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]    *x22            
        +coeff[ 25]    *x22    *x42    
    ;
    v_t_e_q1ex_300                                =v_t_e_q1ex_300                                
        +coeff[ 26]    *x23        *x51
        +coeff[ 27]*x11    *x31*x41*x51
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]    *x21        *x53
        +coeff[ 30]*x12*x23            
        +coeff[ 31]*x12*x22*x31        
        +coeff[ 32]*x11*x22*x32        
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1ex_300                                =v_t_e_q1ex_300                                
        +coeff[ 35]*x11*x23*x31*x41    
        +coeff[ 36]*x11*x21*x33*x41    
        +coeff[ 37]    *x23    *x43    
        +coeff[ 38]*x11*x22*x31*x41*x51
        +coeff[ 39]*x11*x22    *x42*x51
        +coeff[ 40]    *x21*x31*x43*x51
        +coeff[ 41]*x13*x21        *x52
        +coeff[ 42]    *x23        *x53
        +coeff[ 43]            *x41    
    ;
    v_t_e_q1ex_300                                =v_t_e_q1ex_300                                
        +coeff[ 44]                *x51
        +coeff[ 45]*x11*x21            
        +coeff[ 46]*x13                
        +coeff[ 47]    *x22    *x41    
        +coeff[ 48]*x11    *x31    *x51
        +coeff[ 49]    *x21    *x41*x51
        ;

    return v_t_e_q1ex_300                                ;
}
float y_e_q1ex_300                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.6323357E-03;
    float xmin[10]={
        -0.39996E-02,-0.60057E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.58540824E-03, 0.91199599E-01, 0.75996429E-01,-0.63595403E-03,
        -0.66726335E-03, 0.14112325E-03,-0.19912772E-04,-0.11183312E-03,
         0.54565924E-04,-0.12884229E-04, 0.62236082E-04, 0.29550001E-04,
        -0.17607457E-03,-0.65748823E-05, 0.26622465E-04,-0.17115724E-03,
        -0.27173181E-04, 0.83512590E-04, 0.39466096E-06, 0.15131149E-04,
        -0.23548337E-04,-0.18509503E-05,-0.19951306E-03,-0.19463068E-04,
        -0.59624297E-04,-0.25262918E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_300                                =v_y_e_q1ex_300                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x24*x31        
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]*x11*x23    *x41    
    ;
    v_y_e_q1ex_300                                =v_y_e_q1ex_300                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]    *x24*x33        
        +coeff[ 19]    *x22    *x41*x51
        +coeff[ 20]        *x31*x44    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23*x31        
        ;

    return v_y_e_q1ex_300                                ;
}
float p_e_q1ex_300                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.4634150E-03;
    float xmin[10]={
        -0.39996E-02,-0.60057E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.43325897E-03, 0.34116559E-01, 0.64623661E-01,-0.14875572E-02,
        -0.14998888E-02, 0.97527768E-03, 0.74057619E-03, 0.71348128E-03,
        -0.78317309E-04, 0.28176638E-03, 0.56040130E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_300                                =v_p_e_q1ex_300                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_300                                ;
}
float l_e_q1ex_300                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1662983E-02;
    float xmin[10]={
        -0.39996E-02,-0.60057E-01,-0.59976E-01,-0.30033E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59998E-01, 0.30054E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16331379E-02,-0.88046618E-04,-0.29219822E-02,-0.82553091E-03,
        -0.17198968E-02, 0.24616855E-03, 0.76142239E-03,-0.48518091E-03,
         0.11677611E-03, 0.19311231E-03,-0.23609397E-04,-0.25013604E-03,
         0.67415839E-03, 0.19861411E-03, 0.69621025E-03,-0.52742544E-03,
         0.37254524E-03,-0.39222828E-03, 0.95875346E-03, 0.18833467E-03,
         0.44666897E-03,-0.21587819E-03,-0.43647250E-03,-0.98205672E-03,
         0.46374934E-03,-0.26554629E-03, 0.33211248E-03, 0.17832946E-03,
        -0.37458321E-03,-0.19467749E-03,-0.64573443E-03, 0.25403595E-02,
         0.65437058E-03,-0.12750223E-02, 0.39378900E-03, 0.82364259E-03,
        -0.79305982E-03, 0.10567988E-04,-0.11211798E-02,-0.65650168E-03,
        -0.18166646E-02,-0.11517750E-02,-0.51830977E-03, 0.13254030E-02,
        -0.66923891E-03,-0.86085900E-03, 0.12008826E-02, 0.36256024E-03,
        -0.65680401E-03,-0.48858061E-03,-0.19925034E-02, 0.94247400E-03,
        -0.67558105E-03,-0.36741476E-03, 0.36183375E-03,-0.64986740E-03,
        -0.35101958E-02,-0.70766202E-03, 0.17827075E-03, 0.10800699E-02,
        -0.17199717E-02, 0.11036983E-02,-0.83049596E-03, 0.34898036E-03,
         0.27056574E-02, 0.61147330E-04,-0.13337005E-03,-0.87789020E-04,
        -0.62866570E-05,-0.62562785E-05, 0.29401755E-03, 0.17402804E-03,
         0.12494806E-03, 0.16555272E-03,-0.25413235E-03, 0.17006006E-03,
        -0.20335442E-03, 0.27300068E-03, 0.10054425E-03,-0.42929477E-03,
        -0.25697777E-03,-0.14006456E-03, 0.10720208E-03, 0.14805324E-03,
        -0.16222139E-03,-0.21641156E-03,-0.20328729E-03,-0.15676547E-03,
         0.53867570E-03,-0.16275974E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x31*x41    
        +coeff[  6]    *x21*x34*x41    
        +coeff[  7]        *x32        
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[  8]    *x21        *x51
        +coeff[  9]*x11*x21            
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]                *x53
        +coeff[ 14]*x11    *x31    *x51
        +coeff[ 15]    *x24            
        +coeff[ 16]        *x34        
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 17]    *x22    *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]            *x43*x51
        +coeff[ 20]        *x31    *x53
        +coeff[ 21]*x11    *x31*x42    
        +coeff[ 22]*x11*x21*x31    *x51
        +coeff[ 23]*x11    *x31*x41*x51
        +coeff[ 24]*x11    *x31    *x52
        +coeff[ 25]*x12*x21*x31        
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 26]*x12*x21        *x51
        +coeff[ 27]*x13    *x31        
        +coeff[ 28]    *x22    *x43    
        +coeff[ 29]        *x31*x44    
        +coeff[ 30]            *x44*x51
        +coeff[ 31]*x11*x21*x31*x41*x51
        +coeff[ 32]*x11            *x54
        +coeff[ 33]*x12*x21*x31*x41    
        +coeff[ 34]*x12        *x43    
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 35]*x12    *x31    *x52
        +coeff[ 36]*x13    *x31    *x51
        +coeff[ 37]    *x24*x31    *x51
        +coeff[ 38]    *x23*x31*x41*x51
        +coeff[ 39]    *x23*x31    *x52
        +coeff[ 40]    *x21*x33    *x52
        +coeff[ 41]*x11*x22*x31*x42    
        +coeff[ 42]*x11*x22    *x43    
        +coeff[ 43]*x11*x21*x33    *x51
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 44]*x11    *x33    *x52
        +coeff[ 45]*x11*x21    *x42*x52
        +coeff[ 46]*x11    *x31*x41*x53
        +coeff[ 47]*x11*x21        *x54
        +coeff[ 48]*x12*x22*x31    *x51
        +coeff[ 49]*x13*x21*x32        
        +coeff[ 50]    *x23*x32*x42    
        +coeff[ 51]    *x21*x34*x42    
        +coeff[ 52]        *x34*x42*x51
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 53]    *x24*x31    *x52
        +coeff[ 54]    *x21*x33    *x53
        +coeff[ 55]*x11    *x34*x42    
        +coeff[ 56]*x11*x21*x31*x43*x51
        +coeff[ 57]*x11*x21*x33    *x52
        +coeff[ 58]*x11    *x33*x41*x52
        +coeff[ 59]*x11*x21    *x43*x52
        +coeff[ 60]*x11    *x31*x43*x52
        +coeff[ 61]*x12*x21*x31*x43    
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 62]*x12*x21    *x42*x52
        +coeff[ 63]*x13*x23*x31        
        +coeff[ 64]    *x23*x33    *x52
        +coeff[ 65]        *x31        
        +coeff[ 66]                *x51
        +coeff[ 67]*x11                
        +coeff[ 68]    *x21    *x41    
        +coeff[ 69]        *x31    *x51
        +coeff[ 70]    *x23            
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 71]    *x21*x32        
        +coeff[ 72]        *x33        
        +coeff[ 73]        *x32    *x51
        +coeff[ 74]        *x31    *x52
        +coeff[ 75]*x11    *x32        
        +coeff[ 76]*x11*x21    *x41    
        +coeff[ 77]*x11    *x31*x41    
        +coeff[ 78]*x11        *x41*x51
        +coeff[ 79]*x11            *x52
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 80]*x12    *x31        
        +coeff[ 81]*x12        *x41    
        +coeff[ 82]*x13                
        +coeff[ 83]    *x21*x31*x42    
        +coeff[ 84]        *x31*x43    
        +coeff[ 85]    *x23        *x51
        +coeff[ 86]        *x33    *x51
        +coeff[ 87]        *x32*x41*x51
        +coeff[ 88]    *x21*x31    *x52
    ;
    v_l_e_q1ex_300                                =v_l_e_q1ex_300                                
        +coeff[ 89]    *x21    *x41*x52
        ;

    return v_l_e_q1ex_300                                ;
}
float x_e_q1ex_250                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.9977141E-03;
    float xmin[10]={
        -0.39971E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.10143531E-02, 0.30201015E-02, 0.12416291E+00, 0.10726338E-02,
        -0.14289813E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_250                                ;
}
float t_e_q1ex_250                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3000210E-05;
    float xmin[10]={
        -0.39971E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.35709309E-05,-0.19139156E-02, 0.56724309E-03, 0.70892856E-04,
         0.22056832E-02,-0.67371782E-03,-0.60080487E-03,-0.11512433E-02,
        -0.97147771E-03,-0.86847132E-04,-0.11713082E-02,-0.77929796E-03,
        -0.12974127E-02,-0.10494833E-02,-0.66327426E-04,-0.54929093E-04,
        -0.37048478E-04, 0.13656417E-03, 0.93975010E-04,-0.45522352E-03,
        -0.65648858E-03,-0.30543917E-04, 0.42440704E-04,-0.79016849E-04,
        -0.50846113E-04, 0.45169003E-04, 0.14983437E-05, 0.37948923E-05,
        -0.97289631E-05,-0.12931938E-04,-0.60163275E-05, 0.18510964E-04,
        -0.25710351E-04, 0.15914020E-04,-0.22968283E-04,-0.10190970E-04,
         0.23320224E-04, 0.24773457E-04,-0.19779169E-04, 0.38320075E-04,
         0.25323778E-04,-0.20048403E-04, 0.41315869E-06,-0.20232799E-05,
        -0.36346391E-05, 0.20787993E-05, 0.12128947E-04, 0.23334646E-05,
        -0.24530343E-05,-0.34478212E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]*x11            *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x32        
        +coeff[  7]    *x21*x31*x41    
    ;
    v_t_e_q1ex_250                                =v_t_e_q1ex_250                                
        +coeff[  8]    *x21    *x42    
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_250                                =v_t_e_q1ex_250                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]*x11*x22*x31*x41    
        +coeff[ 24]*x11*x22    *x42    
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q1ex_250                                =v_t_e_q1ex_250                                
        +coeff[ 26]*x11        *x41    
        +coeff[ 27]*x11*x21*x31        
        +coeff[ 28]*x11*x22    *x41    
        +coeff[ 29]*x11*x21    *x41*x51
        +coeff[ 30]    *x21*x31    *x52
        +coeff[ 31]*x12*x21*x32        
        +coeff[ 32]*x11*x21    *x43    
        +coeff[ 33]*x11*x22        *x52
        +coeff[ 34]    *x21*x32    *x52
    ;
    v_t_e_q1ex_250                                =v_t_e_q1ex_250                                
        +coeff[ 35]    *x21*x31*x41*x52
        +coeff[ 36]    *x21    *x42*x52
        +coeff[ 37]*x11*x23    *x42    
        +coeff[ 38]*x11*x21*x32*x42    
        +coeff[ 39]*x11*x23    *x41*x51
        +coeff[ 40]*x11*x22    *x42*x51
        +coeff[ 41]*x11    *x32*x41*x52
        +coeff[ 42]                *x51
        +coeff[ 43]*x11*x21            
    ;
    v_t_e_q1ex_250                                =v_t_e_q1ex_250                                
        +coeff[ 44]*x12*x21            
        +coeff[ 45]*x12        *x41    
        +coeff[ 46]*x11*x21    *x41    
        +coeff[ 47]    *x22        *x51
        +coeff[ 48]    *x21    *x41*x51
        +coeff[ 49]*x11            *x52
        ;

    return v_t_e_q1ex_250                                ;
}
float y_e_q1ex_250                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2563750E-03;
    float xmin[10]={
        -0.39971E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.28121230E-03, 0.91101691E-01, 0.75951427E-01,-0.62400254E-03,
        -0.66337408E-03, 0.11540397E-03,-0.30375664E-04,-0.13309304E-03,
         0.51678235E-04, 0.61054387E-04, 0.29797578E-04,-0.16316079E-03,
         0.49006285E-05,-0.19122082E-04, 0.25880841E-04,-0.17675255E-04,
        -0.15890240E-03,-0.63293977E-04, 0.40275216E-04,-0.69533904E-04,
         0.10878439E-04,-0.17212511E-04, 0.35996036E-04, 0.17893377E-05,
        -0.14318508E-03,-0.19716714E-04, 0.17242350E-04,-0.37312406E-04,
        -0.17151382E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_250                                =v_y_e_q1ex_250                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]            *x41*x52
        +coeff[ 15]        *x31*x43*x51
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_y_e_q1ex_250                                =v_y_e_q1ex_250                                
        +coeff[ 17]    *x22*x33        
        +coeff[ 18]    *x22*x31*x44    
        +coeff[ 19]*x11*x21*x32*x41*x52
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]            *x43*x51
        +coeff[ 22]*x11*x21    *x42    
        +coeff[ 23]        *x31*x44    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]*x11    *x32*x42    
    ;
    v_y_e_q1ex_250                                =v_y_e_q1ex_250                                
        +coeff[ 26]    *x21*x31*x41*x52
        +coeff[ 27]*x11*x21    *x44    
        +coeff[ 28]*x11    *x31*x41*x52
        ;

    return v_y_e_q1ex_250                                ;
}
float p_e_q1ex_250                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1169342E-03;
    float xmin[10]={
        -0.39971E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.13604958E-03, 0.33997454E-01, 0.64469747E-01,-0.14817077E-02,
        -0.14956984E-02, 0.97393541E-03, 0.74320246E-03, 0.71027281E-03,
        -0.76138931E-04, 0.27990231E-03, 0.55611233E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_250                                =v_p_e_q1ex_250                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_250                                ;
}
float l_e_q1ex_250                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1650277E-02;
    float xmin[10]={
        -0.39971E-02,-0.60055E-01,-0.59997E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30039E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.15957664E-02,-0.32756685E-02,-0.79080195E-03,-0.14842938E-02,
         0.21110542E-03,-0.77383209E-03, 0.38007833E-03,-0.15936096E-03,
         0.16341980E-03,-0.45282833E-03, 0.15001545E-02, 0.37596358E-02,
         0.47842925E-03,-0.11954817E-03, 0.20941885E-03, 0.56062778E-04,
        -0.11387688E-04,-0.17567635E-03, 0.22881072E-03, 0.14549363E-03,
        -0.21088152E-03, 0.23537879E-03,-0.74287137E-03,-0.85830590E-03,
         0.40288351E-03,-0.19959226E-03,-0.29288392E-03, 0.13804632E-02,
        -0.71077004E-04, 0.38815226E-03, 0.38708291E-04,-0.25619118E-03,
        -0.31254630E-03, 0.62343804E-03, 0.38711974E-03,-0.15101646E-03,
         0.46215029E-03, 0.70266513E-04, 0.11647191E-02, 0.27800095E-02,
         0.72677259E-03, 0.24729557E-03,-0.73804165E-03,-0.72164478E-03,
         0.23295410E-03, 0.62792900E-03,-0.51128137E-03,-0.15785592E-03,
        -0.94687603E-04,-0.17300558E-02,-0.88837370E-03, 0.67043147E-03,
        -0.11281357E-02,-0.12732680E-02,-0.99337089E-03, 0.63263759E-03,
         0.11715847E-02, 0.79061580E-03, 0.10306436E-02, 0.67691074E-03,
        -0.47182853E-03, 0.30082886E-03, 0.86977059E-03, 0.56629925E-03,
         0.72300300E-03, 0.33315949E-03, 0.13656508E-02,-0.99480723E-03,
        -0.11145627E-02,-0.93403895E-03,-0.13687389E-02, 0.76331833E-03,
        -0.80849597E-03,-0.12720965E-02,-0.41867673E-03,-0.47545641E-03,
         0.15616139E-02,-0.43870127E-02, 0.42336603E-03,-0.27874105E-02,
         0.17059195E-02,-0.13776830E-02, 0.22557781E-02, 0.16335150E-02,
         0.65519940E-03,-0.36410019E-02,-0.61698265E-04, 0.63447522E-04,
        -0.29044188E-03,-0.10511187E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x41*x51
        +coeff[  6]            *x42*x51
        +coeff[  7]*x11*x21    *x41    
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[  8]        *x34        
        +coeff[  9]*x11        *x43    
        +coeff[ 10]    *x22*x32    *x52
        +coeff[ 11]    *x21*x32*x41*x52
        +coeff[ 12]*x11    *x32    *x53
        +coeff[ 13]                *x51
        +coeff[ 14]    *x21        *x51
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]*x11        *x41    
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 17]        *x33        
        +coeff[ 18]            *x43    
        +coeff[ 19]        *x32    *x51
        +coeff[ 20]*x11    *x32        
        +coeff[ 21]*x12    *x31        
        +coeff[ 22]    *x21*x32*x41    
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x31*x41*x52
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 26]        *x31    *x53
        +coeff[ 27]*x11    *x32*x41    
        +coeff[ 28]*x11*x22        *x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x12*x22            
        +coeff[ 31]*x13*x21            
        +coeff[ 32]        *x32*x43    
        +coeff[ 33]    *x22*x31*x41*x51
        +coeff[ 34]    *x21*x31*x41*x52
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 35]    *x21*x31    *x53
        +coeff[ 36]        *x31*x41*x53
        +coeff[ 37]*x11    *x33    *x51
        +coeff[ 38]*x11*x22    *x41*x51
        +coeff[ 39]*x11*x21    *x42*x51
        +coeff[ 40]*x11    *x31*x42*x51
        +coeff[ 41]*x11*x21*x31    *x52
        +coeff[ 42]*x11*x21        *x53
        +coeff[ 43]*x11        *x41*x53
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 44]*x12*x21    *x41*x51
        +coeff[ 45]*x12    *x31*x41*x51
        +coeff[ 46]*x13*x21        *x51
        +coeff[ 47]    *x21*x33    *x52
        +coeff[ 48]    *x22    *x42*x52
        +coeff[ 49]    *x21    *x42*x53
        +coeff[ 50]        *x32    *x54
        +coeff[ 51]*x11*x23*x32        
        +coeff[ 52]*x11    *x34*x41    
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 53]*x11*x23    *x42    
        +coeff[ 54]*x11*x22*x32    *x51
        +coeff[ 55]*x11    *x31*x42*x52
        +coeff[ 56]*x12*x22*x32        
        +coeff[ 57]*x12*x21*x32*x41    
        +coeff[ 58]*x12*x21*x31*x42    
        +coeff[ 59]*x12*x21    *x43    
        +coeff[ 60]*x12*x22        *x52
        +coeff[ 61]    *x23*x34        
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 62]*x13*x21    *x42    
        +coeff[ 63]*x13    *x32    *x51
        +coeff[ 64]    *x23*x32*x41*x51
        +coeff[ 65]*x11*x24*x31    *x51
        +coeff[ 66]*x11    *x34*x41*x51
        +coeff[ 67]*x11*x21*x32*x41*x52
        +coeff[ 68]*x11*x22*x31    *x53
        +coeff[ 69]*x11    *x33    *x53
        +coeff[ 70]*x11    *x32*x41*x53
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 71]*x11        *x43*x53
        +coeff[ 72]*x12*x22*x31*x42    
        +coeff[ 73]*x12*x21    *x43*x51
        +coeff[ 74]*x12*x23        *x52
        +coeff[ 75]    *x24*x34        
        +coeff[ 76]*x13*x22*x31    *x51
        +coeff[ 77]*x13*x21    *x42*x51
        +coeff[ 78]    *x21*x32*x44*x51
        +coeff[ 79]    *x24*x32    *x52
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 80]    *x24    *x42*x52
        +coeff[ 81]    *x21*x33*x42*x52
        +coeff[ 82]*x13*x21        *x53
        +coeff[ 83]    *x21    *x44*x53
        +coeff[ 84]        *x34    *x54
        +coeff[ 85]    *x21*x32*x41*x54
        +coeff[ 86]            *x41    
        +coeff[ 87]*x11                
        +coeff[ 88]        *x32        
    ;
    v_l_e_q1ex_250                                =v_l_e_q1ex_250                                
        +coeff[ 89]    *x21    *x41    
        ;

    return v_l_e_q1ex_250                                ;
}
float x_e_q1ex_200                                (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.5117041E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59997E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.54673670E-03, 0.12427369E+00,-0.93978389E-06, 0.30269334E-02,
         0.10658075E-02,-0.14257037E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_200                                ;
}
float t_e_q1ex_200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.8160782E-05;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59997E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.68302743E-05,-0.19074182E-02, 0.84690558E-03, 0.21972759E-02,
        -0.68376964E-03,-0.61178027E-03,-0.11557915E-02,-0.97030442E-03,
         0.71799150E-04,-0.80842314E-04,-0.11476874E-02,-0.75795100E-03,
        -0.12720600E-02,-0.10441552E-02,-0.60752613E-04,-0.60808532E-04,
        -0.36847214E-04, 0.13669656E-03, 0.68092333E-04,-0.42576957E-03,
        -0.63483254E-03,-0.33708879E-04, 0.64116772E-04, 0.43198465E-05,
         0.10404001E-04,-0.74919226E-05,-0.67148176E-06,-0.12216727E-04,
         0.86320533E-05,-0.54387706E-05, 0.86005348E-05,-0.41149229E-04,
        -0.29842588E-04,-0.17094333E-04, 0.14209482E-04, 0.77516524E-05,
        -0.15137090E-04, 0.40927844E-04, 0.31208001E-04,-0.20164567E-04,
         0.27444654E-04, 0.16548332E-05, 0.11621694E-06, 0.78736493E-05,
         0.25858226E-05,-0.35081296E-05,-0.25842282E-05, 0.26810101E-05,
        -0.45389479E-05,-0.65801323E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_200                                =v_t_e_q1ex_200                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_200                                =v_t_e_q1ex_200                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x23*x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x22    *x41    
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x21*x33        
    ;
    v_t_e_q1ex_200                                =v_t_e_q1ex_200                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]*x12    *x31    *x51
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x12*x21    *x41*x51
        +coeff[ 34]    *x21*x32*x41*x51
    ;
    v_t_e_q1ex_200                                =v_t_e_q1ex_200                                
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]*x12*x23        *x51
        +coeff[ 37]*x11*x21*x32*x41*x51
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]*x11*x21    *x43*x51
        +coeff[ 40]    *x22*x31*x41*x52
        +coeff[ 41]        *x31        
        +coeff[ 42]*x11    *x31        
        +coeff[ 43]    *x21    *x41    
    ;
    v_t_e_q1ex_200                                =v_t_e_q1ex_200                                
        +coeff[ 44]                *x52
        +coeff[ 45]*x12*x21            
        +coeff[ 46]        *x31    *x52
        +coeff[ 47]*x12*x22            
        +coeff[ 48]*x11*x22*x31        
        +coeff[ 49]*x12*x21    *x41    
        ;

    return v_t_e_q1ex_200                                ;
}
float y_e_q1ex_200                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.9653297E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59997E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.86735985E-04, 0.91071688E-01, 0.75861983E-01,-0.63224480E-03,
        -0.66472276E-03, 0.96487325E-04,-0.44403499E-04,-0.12089026E-03,
         0.61118270E-04, 0.36831475E-05, 0.60298258E-04,-0.16399499E-03,
        -0.86864648E-05, 0.25528510E-04, 0.32309992E-04,-0.14136291E-03,
        -0.53857959E-04,-0.13031850E-03,-0.23984818E-04, 0.41671715E-05,
        -0.10114200E-04, 0.31036798E-04, 0.11064471E-04,-0.36959602E-04,
        -0.11723319E-04, 0.13270685E-04, 0.17457260E-04,-0.12857236E-04,
        -0.28828226E-04,-0.24811219E-04, 0.16043790E-04,-0.21243752E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_200                                =v_y_e_q1ex_200                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22    *x41    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1ex_200                                =v_y_e_q1ex_200                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]*x11*x21    *x43*x52
        +coeff[ 19]            *x42    
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]        *x31*x44    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]*x11        *x44    
        +coeff[ 25]*x12        *x41*x51
    ;
    v_y_e_q1ex_200                                =v_y_e_q1ex_200                                
        +coeff[ 26]*x11*x22*x31*x41    
        +coeff[ 27]*x11    *x31*x42*x51
        +coeff[ 28]        *x31*x42*x52
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]*x11        *x42*x52
        +coeff[ 31]*x11*x22*x32*x41    
        ;

    return v_y_e_q1ex_200                                ;
}
float p_e_q1ex_200                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.8389646E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59997E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.81495047E-04, 0.33810586E-01, 0.64322241E-01,-0.14731672E-02,
        -0.14871530E-02, 0.96884568E-03, 0.74261089E-03, 0.70472527E-03,
        -0.62321880E-04, 0.27725182E-03, 0.54599205E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_200                                =v_p_e_q1ex_200                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_200                                ;
}
float l_e_q1ex_200                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1678746E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59997E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39996E-02, 0.60069E-01, 0.59976E-01, 0.30047E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.15722823E-02, 0.86508298E-04,-0.32622607E-02,-0.94796834E-03,
        -0.15676446E-02,-0.20757815E-03,-0.39076596E-03, 0.95661334E-03,
        -0.13661784E-02, 0.15802174E-03,-0.17980883E-03,-0.77765166E-04,
        -0.31206579E-04,-0.18849126E-03,-0.31894122E-03, 0.70415379E-03,
        -0.36060554E-03, 0.28938879E-03,-0.40129278E-03, 0.45110330E-04,
         0.45403503E-03, 0.23289984E-03, 0.73882041E-03, 0.36555281E-03,
        -0.19813182E-03, 0.31257040E-03,-0.68782821E-04,-0.24285090E-03,
         0.90373500E-03,-0.44198672E-03, 0.41667218E-03,-0.13312583E-02,
         0.67675178E-03, 0.48459866E-03,-0.16760240E-03, 0.24155965E-02,
         0.26585459E-03,-0.95823652E-03,-0.20749180E-03,-0.39194839E-03,
         0.80098701E-03,-0.62262517E-03, 0.30106725E-02, 0.18795519E-02,
        -0.95671404E-03,-0.82517305E-03,-0.87673712E-03,-0.97306020E-03,
         0.11020520E-02, 0.54302649E-03, 0.32179654E-03, 0.53381181E-03,
        -0.12103928E-02,-0.13226207E-02, 0.19252482E-02,-0.18556033E-02,
        -0.92859758E-03, 0.49473945E-03, 0.10036052E-02, 0.46695361E-03,
        -0.98348700E-03, 0.25372144E-02, 0.14547136E-02, 0.61704643E-03,
         0.13929298E-02,-0.17346552E-02, 0.54992782E-03,-0.11672014E-02,
         0.15793776E-02,-0.19749701E-02,-0.19417966E-02,-0.16187706E-02,
         0.12748318E-02,-0.14188994E-02, 0.98137779E-03, 0.18763407E-02,
         0.19311146E-02,-0.31039712E-02,-0.11893925E-02, 0.15536796E-02,
         0.66278805E-03,-0.10026316E-02, 0.10950756E-02, 0.74728311E-03,
        -0.23301255E-02,-0.89711662E-04, 0.42090025E-04,-0.15826788E-03,
        -0.62714324E-04, 0.73049479E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x32        
        +coeff[  6]        *x32*x42    
        +coeff[  7]    *x22*x31    *x52
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[  8]*x11*x22*x31*x42    
        +coeff[  9]            *x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]            *x43    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x12        *x41    
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]        *x33    *x51
        +coeff[ 19]        *x31*x41*x52
        +coeff[ 20]    *x21        *x53
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]*x11*x21*x31*x41    
        +coeff[ 23]*x11*x21    *x42    
        +coeff[ 24]*x12*x22            
        +coeff[ 25]*x13    *x31        
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]    *x24        *x51
        +coeff[ 28]    *x21*x32    *x52
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11    *x33*x41    
        +coeff[ 32]*x11*x21*x31*x41*x51
        +coeff[ 33]*x11    *x32*x41*x51
        +coeff[ 34]*x12    *x32*x41    
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 35]*x12*x22        *x51
        +coeff[ 36]*x12*x21*x31    *x51
        +coeff[ 37]*x12        *x42*x51
        +coeff[ 38]*x13    *x31*x41    
        +coeff[ 39]    *x23*x32*x41    
        +coeff[ 40]    *x21*x33*x42    
        +coeff[ 41]    *x23*x32    *x51
        +coeff[ 42]    *x21*x32*x42*x51
        +coeff[ 43]    *x21*x31*x43*x51
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 44]    *x21*x31*x41*x53
        +coeff[ 45]*x11    *x32*x42*x51
        +coeff[ 46]*x11*x22    *x41*x52
        +coeff[ 47]*x11*x21*x31*x41*x52
        +coeff[ 48]*x12*x22*x31    *x51
        +coeff[ 49]*x13*x22    *x41    
        +coeff[ 50]    *x23*x33    *x51
        +coeff[ 51]    *x24    *x42*x51
        +coeff[ 52]    *x23*x32    *x52
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 53]    *x22*x33    *x52
        +coeff[ 54]    *x23*x31*x41*x52
        +coeff[ 55]    *x22*x32*x41*x52
        +coeff[ 56]    *x21*x33*x41*x52
        +coeff[ 57]    *x23*x31    *x53
        +coeff[ 58]    *x22    *x42*x53
        +coeff[ 59]    *x23        *x54
        +coeff[ 60]*x11*x22*x33*x41    
        +coeff[ 61]*x11*x22*x32*x42    
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 62]*x11*x21*x33*x42    
        +coeff[ 63]*x11*x21*x32*x43    
        +coeff[ 64]*x11*x21    *x44*x51
        +coeff[ 65]*x11    *x32*x42*x52
        +coeff[ 66]*x11*x21    *x43*x52
        +coeff[ 67]*x11*x21    *x42*x53
        +coeff[ 68]*x12*x22*x32*x41    
        +coeff[ 69]*x12*x24        *x51
        +coeff[ 70]*x12*x21*x33    *x51
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 71]*x12*x22    *x42*x51
        +coeff[ 72]*x12    *x32*x41*x52
        +coeff[ 73]*x12*x22        *x53
        +coeff[ 74]*x12        *x42*x53
        +coeff[ 75]*x13    *x33*x41    
        +coeff[ 76]    *x23*x34*x41    
        +coeff[ 77]    *x24*x31*x42*x51
        +coeff[ 78]    *x24    *x43*x51
        +coeff[ 79]    *x22*x31*x44*x51
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 80]*x13    *x32    *x52
        +coeff[ 81]*x13    *x31*x41*x52
        +coeff[ 82]        *x33*x43*x52
        +coeff[ 83]    *x24    *x41*x53
        +coeff[ 84]    *x21*x32*x42*x53
        +coeff[ 85]    *x21            
        +coeff[ 86]        *x31        
        +coeff[ 87]    *x21    *x41    
        +coeff[ 88]*x11*x21            
    ;
    v_l_e_q1ex_200                                =v_l_e_q1ex_200                                
        +coeff[ 89]*x11            *x51
        ;

    return v_l_e_q1ex_200                                ;
}
float x_e_q1ex_175                                (float *x,int m){
    int ncoeff=  6;
    float avdat=  0.9600511E-03;
    float xmin[10]={
        -0.40000E-02,-0.60045E-01,-0.59988E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59988E-01, 0.30005E-01, 0.39988E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
        -0.95114473E-03, 0.12436571E+00,-0.61779692E-05, 0.30335770E-02,
         0.10630330E-02,-0.14420907E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_175                                ;
}
float t_e_q1ex_175                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7553124E-05;
    float xmin[10]={
        -0.40000E-02,-0.60045E-01,-0.59988E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59988E-01, 0.30005E-01, 0.39988E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.74185641E-05,-0.19017265E-02, 0.21793763E-02, 0.22666165E-05,
        -0.11483830E-02,-0.98757714E-03,-0.87117201E-04, 0.36356689E-05,
        -0.76048223E-05,-0.48478623E-05,-0.41213536E-03, 0.17024719E-04,
        -0.73085324E-03,-0.12417415E-02,-0.52451965E-05, 0.55415367E-05,
         0.10643200E-05, 0.73659753E-05, 0.10470921E-02,-0.69076091E-03,
        -0.61410415E-03,-0.11341395E-02,-0.10264944E-02, 0.63092768E-04,
        -0.65028980E-04,-0.60327762E-04,-0.34964767E-04, 0.13164290E-03,
        -0.64205652E-03,-0.20847545E-04, 0.54662803E-04, 0.77991754E-04,
         0.23667484E-04,-0.52193194E-04,-0.43696538E-04,-0.29258676E-04,
         0.14153261E-05,-0.85582707E-06, 0.79190959E-05, 0.91661777E-05,
         0.86410118E-05,-0.74553400E-05, 0.92750533E-05, 0.19736093E-04,
        -0.14370895E-04, 0.12577340E-04, 0.21716602E-04,-0.15329777E-04,
         0.14263293E-04, 0.13354450E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x12*x21            
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x21        *x52
        +coeff[  7]*x13            *x51
    ;
    v_t_e_q1ex_175                                =v_t_e_q1ex_175                                
        +coeff[  8]*x12*x23            
        +coeff[  9]*x12*x21*x32        
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x12*x21    *x42    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x32*x42    
        +coeff[ 14]*x12*x21        *x52
        +coeff[ 15]    *x23        *x52
        +coeff[ 16]    *x21*x32    *x52
    ;
    v_t_e_q1ex_175                                =v_t_e_q1ex_175                                
        +coeff[ 17]    *x21    *x42*x52
        +coeff[ 18]    *x21            
        +coeff[ 19]    *x23            
        +coeff[ 20]    *x21*x32        
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]*x11            *x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_t_e_q1ex_175                                =v_t_e_q1ex_175                                
        +coeff[ 26]*x11        *x42    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21*x33*x41    
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x23        *x51
        +coeff[ 33]*x11*x22*x31*x41    
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q1ex_175                                =v_t_e_q1ex_175                                
        +coeff[ 35]*x11*x22        *x52
        +coeff[ 36]    *x21*x31        
        +coeff[ 37]*x11        *x41*x51
        +coeff[ 38]*x12*x21    *x41    
        +coeff[ 39]    *x22    *x42    
        +coeff[ 40]*x12*x21        *x51
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]*x11            *x53
        +coeff[ 43]*x13*x22            
    ;
    v_t_e_q1ex_175                                =v_t_e_q1ex_175                                
        +coeff[ 44]*x13    *x32        
        +coeff[ 45]    *x22*x32    *x51
        +coeff[ 46]*x11*x22    *x41*x51
        +coeff[ 47]*x11*x21*x31*x41*x51
        +coeff[ 48]    *x22*x31*x41*x51
        +coeff[ 49]*x12*x23*x31        
        ;

    return v_t_e_q1ex_175                                ;
}
float y_e_q1ex_175                                (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.5188717E-04;
    float xmin[10]={
        -0.40000E-02,-0.60045E-01,-0.59988E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59988E-01, 0.30005E-01, 0.39988E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.87764573E-04, 0.90934955E-01, 0.75805493E-01,-0.62113814E-03,
        -0.65608858E-03, 0.97763885E-04,-0.31542866E-04,-0.13503658E-03,
         0.58932990E-04, 0.49041617E-04,-0.17116645E-03, 0.15771506E-05,
         0.16974926E-04, 0.27644668E-04,-0.14957140E-03, 0.11096897E-05,
         0.84874564E-05,-0.11282670E-04,-0.76083520E-05, 0.19135965E-04,
         0.13744589E-04,-0.12869292E-04, 0.11402888E-04,-0.10659952E-03,
        -0.57623602E-04,-0.33803946E-04, 0.25106225E-04, 0.16932747E-04,
        -0.19017845E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_175                                =v_y_e_q1ex_175                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]*x11*x21*x31        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x22*x32*x41    
        +coeff[ 15]    *x22*x31*x44    
        +coeff[ 16]    *x24*x33        
    ;
    v_y_e_q1ex_175                                =v_y_e_q1ex_175                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x21    *x43    
        +coeff[ 19]        *x31*x44    
        +coeff[ 20]    *x22*x31    *x51
        +coeff[ 21]        *x33    *x51
        +coeff[ 22]*x11    *x31*x41*x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23*x31        
    ;
    v_y_e_q1ex_175                                =v_y_e_q1ex_175                                
        +coeff[ 26]        *x32*x41*x52
        +coeff[ 27]*x13        *x42    
        +coeff[ 28]*x11        *x42*x52
        ;

    return v_y_e_q1ex_175                                ;
}
float p_e_q1ex_175                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.4893166E-04;
    float xmin[10]={
        -0.40000E-02,-0.60045E-01,-0.59988E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59988E-01, 0.30005E-01, 0.39988E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.74349970E-04, 0.33680953E-01, 0.64129643E-01,-0.14655454E-02,
        -0.14775859E-02, 0.96987339E-03, 0.74167491E-03, 0.70034061E-03,
        -0.61725070E-04, 0.27911025E-03, 0.53887884E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_175                                =v_p_e_q1ex_175                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_175                                ;
}
float l_e_q1ex_175                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1653177E-02;
    float xmin[10]={
        -0.40000E-02,-0.60045E-01,-0.59988E-01,-0.30029E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59988E-01, 0.30005E-01, 0.39988E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16206052E-02,-0.35049797E-02,-0.62434020E-03,-0.18697216E-02,
        -0.71901712E-03,-0.18104735E-02,-0.20729892E-02, 0.70411107E-03,
         0.96874559E-04, 0.43813675E-03, 0.15726873E-04, 0.58159210E-04,
        -0.10257364E-03, 0.18332622E-03, 0.36472164E-03, 0.45127893E-03,
         0.11423998E-03,-0.66476976E-04, 0.63525367E-03,-0.31849401E-03,
        -0.27472686E-03, 0.46200524E-03, 0.30674381E-03, 0.13024994E-03,
         0.30797080E-03,-0.36304086E-03, 0.48830498E-04,-0.46979805E-03,
        -0.89509349E-03,-0.54453558E-03, 0.12741140E-02, 0.69518725E-03,
        -0.21636346E-02,-0.49094739E-03,-0.38532683E-03, 0.57538105E-04,
         0.69245911E-03,-0.36347404E-03, 0.13300197E-02, 0.44649222E-03,
        -0.11554075E-02, 0.58028911E-03,-0.14048562E-03, 0.11893515E-02,
        -0.57590770E-03, 0.66924881E-03,-0.33561920E-03, 0.12444172E-02,
         0.11157653E-02, 0.83704083E-03,-0.16439069E-02, 0.17224661E-02,
        -0.10995569E-02, 0.10599508E-02, 0.15481119E-02, 0.90690033E-03,
         0.20096579E-02, 0.59406576E-03,-0.95851981E-03,-0.16400261E-02,
         0.12656328E-02,-0.47844613E-03,-0.64384658E-04, 0.85929496E-03,
         0.43244136E-03, 0.13886741E-02,-0.33492190E-02,-0.52759016E-03,
        -0.62608108E-03,-0.18515773E-02, 0.12072614E-03, 0.68216037E-03,
        -0.21745281E-02, 0.63618203E-03,-0.95603603E-03,-0.60949952E-03,
        -0.11686517E-02,-0.14289730E-02, 0.44324083E-03,-0.72541984E-03,
        -0.79847508E-04, 0.39028881E-04, 0.42264423E-04, 0.43042557E-04,
        -0.57123831E-04,-0.84373940E-04, 0.24007409E-03, 0.15671617E-03,
        -0.88301097E-03,-0.17257678E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]        *x34        
        +coeff[  5]*x11*x21*x31    *x51
        +coeff[  6]*x11*x23        *x52
        +coeff[  7]*x13*x23            
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[  8]        *x31        
        +coeff[  9]        *x32        
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]    *x21        *x52
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 17]        *x31    *x52
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]*x11    *x31*x41*x51
        +coeff[ 20]*x11*x21        *x52
        +coeff[ 21]*x11        *x41*x52
        +coeff[ 22]*x11            *x53
        +coeff[ 23]*x12*x22            
        +coeff[ 24]*x12*x21*x31        
        +coeff[ 25]*x12    *x31*x41    
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 26]    *x23*x32        
        +coeff[ 27]*x13        *x41    
        +coeff[ 28]        *x31*x44    
        +coeff[ 29]    *x23*x31    *x51
        +coeff[ 30]        *x33*x41*x51
        +coeff[ 31]        *x31*x43*x51
        +coeff[ 32]    *x21*x32    *x52
        +coeff[ 33]*x11    *x31*x42*x51
        +coeff[ 34]*x11        *x43*x51
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 35]*x11    *x31    *x53
        +coeff[ 36]*x12*x22    *x41    
        +coeff[ 37]*x12        *x43    
        +coeff[ 38]*x12    *x31*x41*x51
        +coeff[ 39]*x12        *x41*x52
        +coeff[ 40]    *x22*x33*x41    
        +coeff[ 41]    *x23*x32    *x51
        +coeff[ 42]    *x23*x31    *x52
        +coeff[ 43]    *x22*x31*x41*x52
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 44]    *x21*x31*x42*x52
        +coeff[ 45]            *x42*x54
        +coeff[ 46]*x11*x24        *x51
        +coeff[ 47]*x11*x21*x33    *x51
        +coeff[ 48]*x11*x21        *x54
        +coeff[ 49]*x12*x21*x32    *x51
        +coeff[ 50]    *x24*x32*x41    
        +coeff[ 51]    *x22*x34*x41    
        +coeff[ 52]        *x34*x43    
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 53]*x13*x21*x31    *x51
        +coeff[ 54]    *x21*x34    *x52
        +coeff[ 55]    *x21*x32*x42*x52
        +coeff[ 56]*x11*x21*x34*x41    
        +coeff[ 57]*x11    *x34*x41*x51
        +coeff[ 58]*x11*x22*x31*x42*x51
        +coeff[ 59]*x11*x21*x32*x42*x51
        +coeff[ 60]*x11*x22    *x43*x51
        +coeff[ 61]*x11*x24        *x52
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 62]*x11*x23        *x53
        +coeff[ 63]*x11*x22*x31    *x53
        +coeff[ 64]*x12    *x34*x41    
        +coeff[ 65]*x12*x22*x31*x42    
        +coeff[ 66]*x12    *x31*x43*x51
        +coeff[ 67]*x12    *x33    *x52
        +coeff[ 68]*x13*x22*x31*x41    
        +coeff[ 69]*x13*x21*x32*x41    
        +coeff[ 70]*x13        *x44    
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 71]        *x34*x42*x52
        +coeff[ 72]        *x32*x44*x52
        +coeff[ 73]*x13*x21        *x53
        +coeff[ 74]    *x21*x34    *x53
        +coeff[ 75]*x13        *x41*x53
        +coeff[ 76]    *x23*x31*x41*x53
        +coeff[ 77]    *x22*x32*x41*x53
        +coeff[ 78]        *x34*x41*x53
        +coeff[ 79]        *x31*x43*x54
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 80]                *x51
        +coeff[ 81]*x11                
        +coeff[ 82]        *x31    *x51
        +coeff[ 83]            *x41*x51
        +coeff[ 84]                *x52
        +coeff[ 85]*x11            *x51
        +coeff[ 86]        *x32*x41    
        +coeff[ 87]    *x22        *x51
        +coeff[ 88]        *x31*x41*x51
    ;
    v_l_e_q1ex_175                                =v_l_e_q1ex_175                                
        +coeff[ 89]            *x41*x52
        ;

    return v_l_e_q1ex_175                                ;
}
float x_e_q1ex_150                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.6253715E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30022E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.61435276E-03, 0.30338385E-02, 0.12450010E+00, 0.10615105E-02,
        -0.14017265E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_150                                ;
}
float t_e_q1ex_150                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3971574E-05;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30022E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.49499054E-05,-0.18947275E-02, 0.13055386E-02, 0.21780387E-02,
        -0.68536110E-03,-0.60898048E-03,-0.11571053E-02,-0.96686685E-03,
         0.67565234E-04,-0.81553793E-04,-0.11109964E-02,-0.73946686E-03,
        -0.12384882E-02,-0.10124054E-02,-0.61581231E-04,-0.54468488E-04,
        -0.21503982E-04, 0.95310614E-04,-0.41096986E-03,-0.63604466E-03,
         0.90022921E-04,-0.34140037E-04, 0.60309423E-04, 0.25520774E-04,
        -0.77811885E-04,-0.46846759E-04,-0.31042014E-05,-0.49949917E-05,
         0.14965974E-04, 0.29166127E-04, 0.29879559E-05,-0.13999389E-04,
        -0.20544925E-04, 0.65726049E-05, 0.13944056E-04,-0.17803202E-04,
         0.31525964E-04, 0.56064790E-04, 0.19717545E-04, 0.15758387E-04,
         0.47762998E-04, 0.17587279E-04,-0.23854114E-04,-0.21109840E-04,
         0.17143259E-05,-0.12168573E-05, 0.16233309E-05, 0.50496910E-05,
         0.22039053E-05,-0.22718878E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_150                                =v_t_e_q1ex_150                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_150                                =v_t_e_q1ex_150                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x32*x42*x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x21*x32    *x53
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1ex_150                                =v_t_e_q1ex_150                                
        +coeff[ 26]            *x41*x51
        +coeff[ 27]    *x22    *x41    
        +coeff[ 28]    *x23        *x51
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x21*x33        
        +coeff[ 31]*x11*x21*x32*x41    
        +coeff[ 32]*x13        *x42    
        +coeff[ 33]*x12*x22        *x51
        +coeff[ 34]*x13*x21*x31*x41    
    ;
    v_t_e_q1ex_150                                =v_t_e_q1ex_150                                
        +coeff[ 35]*x12*x22*x31    *x51
        +coeff[ 36]*x13*x21    *x41*x51
        +coeff[ 37]    *x21*x33*x41*x51
        +coeff[ 38]*x12*x21    *x42*x51
        +coeff[ 39]*x11    *x32*x42*x51
        +coeff[ 40]    *x21*x31*x43*x51
        +coeff[ 41]    *x21*x33    *x52
        +coeff[ 42]    *x21*x31*x42*x52
        +coeff[ 43]*x11*x21    *x41*x53
    ;
    v_t_e_q1ex_150                                =v_t_e_q1ex_150                                
        +coeff[ 44]            *x41    
        +coeff[ 45]*x11    *x31        
        +coeff[ 46]        *x32        
        +coeff[ 47]*x13                
        +coeff[ 48]    *x22*x31        
        +coeff[ 49]*x12        *x41    
        ;

    return v_t_e_q1ex_150                                ;
}
float y_e_q1ex_150                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.1120193E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30022E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.13563479E-03, 0.90892963E-01, 0.75733513E-01,-0.62639068E-03,
        -0.65576320E-03, 0.12619450E-03,-0.28724487E-04,-0.13453390E-03,
         0.54637563E-04, 0.30784613E-04,-0.16549324E-03, 0.11924282E-04,
         0.28890158E-05, 0.50586590E-04,-0.18660961E-04,-0.12553214E-03,
        -0.57980436E-04,-0.98013195E-04,-0.16899651E-03,-0.45786010E-05,
        -0.11766280E-04, 0.19881712E-04, 0.15751080E-04, 0.13679561E-04,
         0.94039569E-05,-0.86312566E-05,-0.10334315E-03,-0.25485446E-04,
        -0.12868314E-04,-0.75049934E-05, 0.10588526E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_150                                =v_y_e_q1ex_150                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x31    *x52
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]            *x43*x52
        +coeff[ 12]    *x22    *x41    
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x33        
    ;
    v_y_e_q1ex_150                                =v_y_e_q1ex_150                                
        +coeff[ 17]    *x22*x31*x44    
        +coeff[ 18]    *x22*x32*x45    
        +coeff[ 19]            *x42*x51
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]            *x41*x52
        +coeff[ 22]*x11        *x43    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]        *x31*x44    
        +coeff[ 25]*x11        *x42*x51
    ;
    v_y_e_q1ex_150                                =v_y_e_q1ex_150                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]        *x33*x42    
        +coeff[ 28]*x13        *x41    
        +coeff[ 29]*x11    *x31    *x52
        +coeff[ 30]        *x34*x42    
        ;

    return v_y_e_q1ex_150                                ;
}
float p_e_q1ex_150                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.1150733E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30022E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.13053713E-03, 0.33510305E-01, 0.63977487E-01,-0.14599902E-02,
        -0.14737927E-02, 0.97409228E-03, 0.74440811E-03, 0.69973868E-03,
        -0.64457206E-04, 0.27604701E-03, 0.53792662E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_150                                =v_p_e_q1ex_150                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x32*x41    
        ;

    return v_p_e_q1ex_150                                ;
}
float l_e_q1ex_150                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1629865E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30022E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17028990E-02,-0.34862289E-02,-0.99562679E-03,-0.19385647E-02,
         0.33261004E-03, 0.14515821E-03, 0.38282605E-03, 0.10085239E-03,
         0.16434345E-03,-0.20049477E-03, 0.10228371E-03,-0.31702904E-03,
        -0.23980957E-03,-0.35646086E-03,-0.99024968E-03, 0.28970890E-03,
         0.30958513E-03,-0.72295353E-03,-0.55341960E-04, 0.38457135E-03,
        -0.15618259E-02, 0.93033173E-04, 0.88835033E-04,-0.22262854E-03,
        -0.10145651E-02,-0.21205794E-03, 0.98872860E-03, 0.90551318E-03,
        -0.53427787E-03,-0.93519357E-05,-0.11248541E-02,-0.27094231E-03,
         0.61014219E-03,-0.83713920E-03,-0.34963529E-03,-0.26424904E-02,
         0.42036150E-04, 0.21615682E-04, 0.20564946E-02, 0.42515018E-03,
         0.14540325E-02, 0.67029655E-03, 0.76515489E-03,-0.18315301E-02,
         0.15854480E-02, 0.65455277E-03,-0.61621633E-03,-0.15033279E-02,
        -0.84569782E-03, 0.38414227E-03,-0.60677371E-03, 0.60624839E-03,
        -0.64841897E-03, 0.92932535E-03, 0.76078880E-03, 0.54617092E-03,
         0.16363597E-02,-0.16975703E-02,-0.11414305E-03, 0.81013900E-03,
        -0.11169849E-02, 0.21301772E-03, 0.11528705E-02, 0.11248017E-02,
        -0.20156822E-02,-0.12559013E-02, 0.45996506E-03, 0.63315831E-03,
        -0.76854980E-03, 0.25793882E-02, 0.11111116E-02, 0.97641401E-03,
         0.14179758E-02,-0.88392274E-03, 0.11569830E-02,-0.14503273E-02,
        -0.75138622E-03,-0.61826030E-03, 0.13281535E-02,-0.25386729E-02,
        -0.70323283E-03, 0.10911657E-02, 0.64121507E-03,-0.10481263E-02,
         0.37013588E-02,-0.67119917E-03, 0.15583890E-02, 0.10417422E-02,
         0.13993534E-02, 0.10342794E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x31*x41    
        +coeff[  3]            *x42    
        +coeff[  4]            *x41*x51
        +coeff[  5]*x11    *x31*x41*x52
        +coeff[  6]*x13    *x32        
        +coeff[  7]                *x51
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[  8]    *x21*x31        
        +coeff[  9]        *x32        
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]*x11        *x42    
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]*x11        *x41*x51
        +coeff[ 16]*x13                
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 17]        *x32*x42    
        +coeff[ 18]            *x44    
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]        *x31*x41*x52
        +coeff[ 21]            *x41*x53
        +coeff[ 22]*x11    *x33        
        +coeff[ 23]*x12    *x31*x41    
        +coeff[ 24]*x12        *x41*x51
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]        *x32*x43    
        +coeff[ 28]    *x23*x31    *x51
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]    *x21*x31*x41*x52
        +coeff[ 31]*x11    *x34        
        +coeff[ 32]*x11*x23        *x51
        +coeff[ 33]*x11    *x33    *x51
        +coeff[ 34]*x11*x21*x31*x41*x51
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 35]*x11*x22        *x52
        +coeff[ 36]*x11    *x31    *x53
        +coeff[ 37]*x12        *x42*x51
        +coeff[ 38]    *x22*x31*x43    
        +coeff[ 39]        *x33*x43    
        +coeff[ 40]        *x32*x44    
        +coeff[ 41]*x13*x21        *x51
        +coeff[ 42]*x13    *x31    *x51
        +coeff[ 43]        *x32*x43*x51
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 44]        *x33*x41*x52
        +coeff[ 45]        *x31*x43*x52
        +coeff[ 46]*x11*x22*x32*x41    
        +coeff[ 47]*x11*x21*x31*x41*x52
        +coeff[ 48]*x11    *x31*x42*x52
        +coeff[ 49]*x12*x22*x32        
        +coeff[ 50]*x12*x23    *x41    
        +coeff[ 51]*x12    *x32*x41*x51
        +coeff[ 52]*x12*x21*x31    *x52
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 53]*x12        *x41*x53
        +coeff[ 54]*x13*x21*x31*x41    
        +coeff[ 55]*x13    *x31*x42    
        +coeff[ 56]    *x23*x31*x43    
        +coeff[ 57]        *x34*x43    
        +coeff[ 58]    *x22*x31*x44    
        +coeff[ 59]    *x23*x33    *x51
        +coeff[ 60]    *x22*x34    *x51
        +coeff[ 61]    *x23*x32    *x52
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 62]    *x22*x33    *x52
        +coeff[ 63]        *x34*x41*x52
        +coeff[ 64]    *x22*x31*x42*x52
        +coeff[ 65]        *x32*x43*x52
        +coeff[ 66]    *x22*x32    *x53
        +coeff[ 67]        *x34    *x53
        +coeff[ 68]    *x21*x32    *x54
        +coeff[ 69]*x11*x22*x34        
        +coeff[ 70]*x11*x24        *x52
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 71]*x11*x23    *x41*x52
        +coeff[ 72]*x11    *x33*x41*x52
        +coeff[ 73]*x11*x22    *x41*x53
        +coeff[ 74]*x11*x22        *x54
        +coeff[ 75]*x11    *x31*x41*x54
        +coeff[ 76]*x12*x23*x31    *x51
        +coeff[ 77]*x12    *x34    *x51
        +coeff[ 78]*x12*x22    *x42*x51
        +coeff[ 79]*x13*x22*x32        
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 80]*x13*x21*x32*x41    
        +coeff[ 81]    *x23*x34*x41    
        +coeff[ 82]*x13*x22    *x42    
        +coeff[ 83]    *x24*x32*x42    
        +coeff[ 84]    *x22*x32*x44    
        +coeff[ 85]*x13    *x31*x42*x51
        +coeff[ 86]        *x34*x43*x51
        +coeff[ 87]*x13*x22        *x52
        +coeff[ 88]*x13    *x31*x41*x52
    ;
    v_l_e_q1ex_150                                =v_l_e_q1ex_150                                
        +coeff[ 89]    *x21*x33*x42*x52
        ;

    return v_l_e_q1ex_150                                ;
}
float x_e_q1ex_125                                (float *x,int m){
    int ncoeff=  5;
    float avdat= -0.7394460E-04;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  6]={
         0.71893373E-04, 0.30400923E-02, 0.12468763E+00, 0.10535924E-02,
        -0.13926168E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        ;

    return v_x_e_q1ex_125                                ;
}
float t_e_q1ex_125                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2245588E-05;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.25047450E-05,-0.18829610E-02, 0.16743254E-02, 0.21747651E-02,
        -0.69116801E-03,-0.60072879E-03,-0.11327295E-02,-0.95904118E-03,
         0.68281523E-04,-0.89254354E-04,-0.11177387E-02,-0.72044501E-03,
        -0.12274808E-02,-0.10111039E-02,-0.50742605E-04,-0.39985887E-04,
        -0.27610489E-04, 0.59728671E-04,-0.41586143E-03,-0.63030911E-03,
         0.11042391E-03,-0.20281082E-04, 0.48778409E-04, 0.28898792E-04,
        -0.62921099E-04,-0.50570587E-04, 0.10528814E-03,-0.49147930E-05,
         0.96548138E-05,-0.57603734E-05, 0.22986058E-04, 0.73600127E-05,
        -0.22660897E-04, 0.16237142E-04,-0.24781481E-04,-0.27317707E-04,
         0.20843867E-04,-0.92149530E-05,-0.11699613E-04, 0.19470897E-04,
        -0.96065023E-05,-0.23215976E-04, 0.19598003E-04, 0.72550436E-04,
        -0.21773705E-04, 0.36120418E-04,-0.18901455E-04,-0.16469796E-05,
         0.17497817E-05,-0.13620637E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q1ex_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_125                                =v_t_e_q1ex_125                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]    *x23*x31*x41    
        +coeff[ 11]    *x23    *x42    
        +coeff[ 12]    *x21*x32*x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q1ex_125                                =v_t_e_q1ex_125                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x23*x32        
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x32*x42*x51
        +coeff[ 21]*x11    *x32        
        +coeff[ 22]    *x21    *x42*x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11*x22*x31*x41    
        +coeff[ 25]*x11*x22    *x42    
    ;
    v_t_e_q1ex_125                                =v_t_e_q1ex_125                                
        +coeff[ 26]    *x21*x31*x43*x51
        +coeff[ 27]    *x22            
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]*x11            *x52
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]*x11*x22*x32        
        +coeff[ 33]*x12*x22    *x41    
        +coeff[ 34]*x11    *x32*x42    
    ;
    v_t_e_q1ex_125                                =v_t_e_q1ex_125                                
        +coeff[ 35]*x11    *x31*x43    
        +coeff[ 36]*x12*x21    *x41*x51
        +coeff[ 37]    *x23    *x41*x51
        +coeff[ 38]*x12    *x31*x41*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x22    *x41*x52
        +coeff[ 41]*x11*x21        *x53
        +coeff[ 42]*x13*x22        *x51
        +coeff[ 43]    *x21*x33*x41*x51
    ;
    v_t_e_q1ex_125                                =v_t_e_q1ex_125                                
        +coeff[ 44]*x11*x22    *x42*x51
        +coeff[ 45]    *x23    *x42*x51
        +coeff[ 46]*x12*x21    *x41*x52
        +coeff[ 47]    *x21*x31        
        +coeff[ 48]        *x32        
        +coeff[ 49]            *x42    
        ;

    return v_t_e_q1ex_125                                ;
}
float y_e_q1ex_125                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.3234509E-03;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.29295144E-03, 0.90825371E-01, 0.75596668E-01,-0.61680120E-03,
        -0.65319729E-03, 0.11613681E-03,-0.19493667E-04,-0.12185846E-03,
         0.44165972E-04, 0.51573741E-04, 0.28809494E-04,-0.18218733E-03,
         0.22984406E-04,-0.11092056E-04,-0.25320755E-04,-0.12880884E-03,
         0.30114476E-04, 0.25150513E-04, 0.39059260E-05, 0.97089533E-05,
        -0.19364867E-04,-0.78295861E-05, 0.16295331E-04,-0.16640188E-04,
        -0.14202463E-04, 0.15006868E-04,-0.12793721E-03,-0.71222683E-04,
        -0.11481165E-04, 0.24017765E-04,-0.14673593E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_125                                =v_y_e_q1ex_125                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]    *x24*x31        
        +coeff[ 12]            *x43*x52
        +coeff[ 13]    *x22    *x41    
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x24*x31*x42    
    ;
    v_y_e_q1ex_125                                =v_y_e_q1ex_125                                
        +coeff[ 17]    *x24*x33        
        +coeff[ 18]            *x42    
        +coeff[ 19]    *x21    *x41    
        +coeff[ 20]*x11*x21    *x41    
        +coeff[ 21]*x12        *x41    
        +coeff[ 22]            *x41*x52
        +coeff[ 23]    *x21*x32*x41    
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_q1ex_125                                =v_y_e_q1ex_125                                
        +coeff[ 26]    *x22*x31*x42    
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x12        *x42*x51
        +coeff[ 29]*x11*x21    *x41*x52
        +coeff[ 30]*x11    *x31*x41*x52
        ;

    return v_y_e_q1ex_125                                ;
}
float p_e_q1ex_125                                (float *x,int m){
    int ncoeff= 12;
    float avdat=  0.1681099E-03;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
        -0.14884467E-03, 0.33250984E-01, 0.63682750E-01,-0.14496912E-02,
        -0.14607901E-02, 0.97291532E-03,-0.56396148E-04, 0.68838749E-03,
        -0.51262876E-04, 0.79329021E-03, 0.52491447E-03, 0.31244665E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_125                                =v_p_e_q1ex_125                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x43    
        ;

    return v_p_e_q1ex_125                                ;
}
float l_e_q1ex_125                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1657177E-02;
    float xmin[10]={
        -0.39992E-02,-0.60060E-01,-0.59964E-01,-0.30032E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59986E-01, 0.30044E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.17429583E-02, 0.36290676E-04,-0.36486040E-02,-0.97418629E-03,
        -0.18198399E-02, 0.34109493E-04,-0.39719354E-04, 0.12281523E-02,
         0.39025903E-03,-0.13778848E-03,-0.54572633E-03,-0.28512490E-03,
        -0.19408263E-03,-0.67223045E-04,-0.10755183E-02,-0.52269455E-03,
        -0.11811945E-03, 0.35304518E-03, 0.43321037E-04,-0.15446809E-02,
         0.32973543E-03, 0.92643994E-03, 0.18507481E-03,-0.17874689E-02,
         0.18289474E-03, 0.53455256E-03, 0.44402038E-03, 0.96644682E-04,
         0.27937678E-03,-0.89718036E-04, 0.29537096E-03, 0.11323823E-02,
         0.54661091E-03, 0.11094509E-02, 0.37224265E-03,-0.24194298E-03,
        -0.19581843E-03, 0.58800465E-03,-0.37075180E-03, 0.21903322E-02,
        -0.56511827E-03, 0.32913682E-03, 0.33006415E-03, 0.82645193E-03,
         0.43743371E-03,-0.59274147E-03,-0.62520575E-03, 0.19869520E-03,
        -0.12398391E-02, 0.61342021E-03, 0.20091271E-02, 0.92074391E-04,
         0.10417571E-02,-0.31461177E-03, 0.54532947E-03,-0.10643766E-02,
         0.65517583E-03,-0.71054057E-03,-0.86448621E-03,-0.10389014E-02,
        -0.32910836E-03, 0.12448665E-02, 0.67594246E-03, 0.85111015E-03,
         0.10410484E-02,-0.50588633E-03,-0.45459156E-03, 0.11306769E-02,
        -0.98845118E-03,-0.23554891E-03,-0.10772063E-03,-0.78649301E-03,
         0.84778399E-03,-0.25511766E-03,-0.95747027E-03,-0.88948919E-03,
         0.59676479E-03,-0.73216564E-03, 0.23041226E-03, 0.72067569E-03,
        -0.13746793E-02, 0.11975077E-02,-0.46196234E-03, 0.21968931E-02,
        -0.13492889E-02, 0.65710989E-03,-0.33617564E-02, 0.20495236E-02,
         0.21518923E-02,-0.12915187E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x22*x32        
        +coeff[  7]*x11*x21*x31*x41    
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[  8]*x12        *x42    
        +coeff[  9]        *x32        
        +coeff[ 10]                *x52
        +coeff[ 11]*x11            *x51
        +coeff[ 12]    *x23            
        +coeff[ 13]        *x33        
        +coeff[ 14]    *x21*x31*x41    
        +coeff[ 15]    *x21    *x42    
        +coeff[ 16]            *x43    
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]                *x53
        +coeff[ 19]*x11*x21        *x51
        +coeff[ 20]*x12            *x51
        +coeff[ 21]    *x21*x31*x42    
        +coeff[ 22]    *x21    *x43    
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]    *x21    *x42*x51
        +coeff[ 25]    *x22        *x52
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 26]                *x54
        +coeff[ 27]*x12*x21    *x41    
        +coeff[ 28]*x12    *x31*x41    
        +coeff[ 29]*x12    *x31    *x51
        +coeff[ 30]*x12        *x41*x51
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]        *x34*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]*x13            *x51
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 35]    *x21*x33    *x51
        +coeff[ 36]        *x34    *x51
        +coeff[ 37]    *x23    *x41*x51
        +coeff[ 38]    *x21    *x43*x51
        +coeff[ 39]*x11*x23        *x51
        +coeff[ 40]*x11*x22*x31    *x51
        +coeff[ 41]*x11*x21*x31    *x52
        +coeff[ 42]*x11*x21    *x41*x52
        +coeff[ 43]*x12*x21*x31*x41    
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 44]*x12*x21    *x42    
        +coeff[ 45]*x12*x22        *x51
        +coeff[ 46]*x12    *x31*x41*x51
        +coeff[ 47]*x13        *x42    
        +coeff[ 48]    *x23*x31*x42    
        +coeff[ 49]*x13*x21        *x51
        +coeff[ 50]    *x23*x31*x41*x51
        +coeff[ 51]    *x22*x32*x41*x51
        +coeff[ 52]    *x21*x33*x41*x51
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 53]        *x34*x41*x51
        +coeff[ 54]    *x23    *x42*x51
        +coeff[ 55]    *x21*x32*x42*x51
        +coeff[ 56]        *x33*x42*x51
        +coeff[ 57]    *x21    *x44*x51
        +coeff[ 58]    *x21    *x43*x52
        +coeff[ 59]    *x21*x31*x41*x53
        +coeff[ 60]        *x32    *x54
        +coeff[ 61]*x11    *x34*x41    
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 62]*x11*x22*x31*x42    
        +coeff[ 63]*x11*x23    *x41*x51
        +coeff[ 64]*x11*x22*x31*x41*x51
        +coeff[ 65]*x11*x21*x32*x41*x51
        +coeff[ 66]*x11    *x31*x43*x51
        +coeff[ 67]*x11    *x31*x42*x52
        +coeff[ 68]*x11*x21    *x41*x53
        +coeff[ 69]*x11    *x31    *x54
        +coeff[ 70]*x11        *x41*x54
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 71]*x12    *x31*x42*x51
        +coeff[ 72]*x12*x21    *x41*x52
        +coeff[ 73]*x13    *x33        
        +coeff[ 74]*x13*x21*x31*x41    
        +coeff[ 75]*x13    *x32*x41    
        +coeff[ 76]*x13*x21    *x42    
        +coeff[ 77]        *x34*x43    
        +coeff[ 78]    *x24*x32    *x51
        +coeff[ 79]*x13*x21    *x41*x51
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 80]    *x24*x31*x41*x51
        +coeff[ 81]    *x22*x31*x43*x51
        +coeff[ 82]*x13*x21        *x52
        +coeff[ 83]    *x21*x31*x43*x52
        +coeff[ 84]    *x21*x31*x41*x54
        +coeff[ 85]*x11*x22*x31*x43    
        +coeff[ 86]*x11*x23    *x42*x51
        +coeff[ 87]*x11    *x33*x42*x51
        +coeff[ 88]*x11*x21    *x44*x51
    ;
    v_l_e_q1ex_125                                =v_l_e_q1ex_125                                
        +coeff[ 89]*x11    *x31*x44*x51
        ;

    return v_l_e_q1ex_125                                ;
}
float x_e_q1ex_100                                (float *x,int m){
    int ncoeff=  6;
    float avdat= -0.6339449E-03;
    float xmin[10]={
        -0.39998E-02,-0.60043E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[  7]={
         0.65773854E-03, 0.12495212E+00, 0.58803380E-06, 0.30485154E-02,
         0.10420834E-02,-0.14660864E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x51 = x5;

//                 function

    float v_x_e_q1ex_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]*x13                
        +coeff[  3]*x11                
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x23            
        ;

    return v_x_e_q1ex_100                                ;
}
float t_e_q1ex_100                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1439432E-04;
    float xmin[10]={
        -0.39998E-02,-0.60043E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15068840E-04,-0.18620543E-02, 0.22170688E-02, 0.21508571E-02,
        -0.67014631E-03,-0.59231481E-03,-0.11125626E-02,-0.93541335E-03,
         0.72911760E-04,-0.81470622E-04,-0.55617696E-04,-0.55679586E-04,
        -0.32806242E-04, 0.15187732E-03,-0.11097362E-02,-0.74219680E-03,
        -0.12053933E-02,-0.98891149E-03,-0.32791519E-04, 0.28987932E-04,
         0.71979019E-04,-0.41411142E-03,-0.62959635E-03,-0.48810894E-05,
        -0.76714787E-05, 0.65117288E-05,-0.51417424E-04,-0.47858997E-04,
        -0.30211211E-04, 0.10792318E-04, 0.39808405E-04,-0.17260465E-04,
        -0.44976656E-04, 0.42539206E-04, 0.28549320E-06,-0.67769279E-05,
         0.18233026E-05, 0.13140404E-05,-0.32925070E-05,-0.32418222E-05,
         0.15629503E-04,-0.37632701E-05,-0.34446991E-05, 0.10482931E-04,
        -0.63465250E-05,-0.67320639E-05,-0.37424816E-05, 0.66701623E-05,
        -0.78469502E-05,-0.42248626E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q1ex_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x32        
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q1ex_100                                =v_t_e_q1ex_100                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]*x11*x22            
        +coeff[ 11]*x11    *x31*x41    
        +coeff[ 12]*x11        *x42    
        +coeff[ 13]    *x21*x31*x41*x51
        +coeff[ 14]    *x23*x31*x41    
        +coeff[ 15]    *x23    *x42    
        +coeff[ 16]    *x21*x32*x42    
    ;
    v_t_e_q1ex_100                                =v_t_e_q1ex_100                                
        +coeff[ 17]    *x21*x31*x43    
        +coeff[ 18]*x11    *x32        
        +coeff[ 19]    *x21*x32    *x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x23*x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x22*x31        
        +coeff[ 24]*x11*x22    *x41    
        +coeff[ 25]    *x23        *x51
    ;
    v_t_e_q1ex_100                                =v_t_e_q1ex_100                                
        +coeff[ 26]*x11*x22*x31*x41    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]    *x21    *x43*x51
        +coeff[ 29]*x12        *x41*x52
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]    *x22*x33    *x51
        +coeff[ 32]*x12*x21*x31*x41*x51
        +coeff[ 33]    *x21*x32*x42*x51
        +coeff[ 34]*x11    *x31        
    ;
    v_t_e_q1ex_100                                =v_t_e_q1ex_100                                
        +coeff[ 35]        *x31*x41    
        +coeff[ 36]        *x31    *x51
        +coeff[ 37]                *x52
        +coeff[ 38]*x12*x21            
        +coeff[ 39]        *x32*x41    
        +coeff[ 40]    *x21    *x41*x51
        +coeff[ 41]*x11            *x52
        +coeff[ 42]*x12*x22            
        +coeff[ 43]*x12*x21*x31        
    ;
    v_t_e_q1ex_100                                =v_t_e_q1ex_100                                
        +coeff[ 44]*x11*x22*x31        
        +coeff[ 45]    *x23*x31        
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]*x13            *x51
        +coeff[ 49]*x11*x21*x31    *x51
        ;

    return v_t_e_q1ex_100                                ;
}
float y_e_q1ex_100                                (float *x,int m){
    int ncoeff= 23;
    float avdat= -0.1539318E-03;
    float xmin[10]={
        -0.39998E-02,-0.60043E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
         0.12369432E-03, 0.90658084E-01, 0.75452760E-01,-0.61011879E-03,
        -0.64264116E-03, 0.81915729E-04,-0.65534798E-04,-0.13385083E-03,
         0.59361682E-04, 0.54980734E-04,-0.13140401E-03, 0.19237983E-04,
        -0.22633241E-04, 0.22372609E-04, 0.26617059E-04,-0.14061376E-03,
        -0.13399051E-03,-0.63548752E-04,-0.13196433E-04, 0.33333745E-04,
        -0.34438919E-04, 0.73524166E-05, 0.19174355E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q1ex_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22*x31        
        +coeff[  7]    *x24    *x41    
    ;
    v_y_e_q1ex_100                                =v_y_e_q1ex_100                                
        +coeff[  8]            *x43    
        +coeff[  9]        *x32*x41    
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x22    *x41    
        +coeff[ 12]*x11*x21*x31        
        +coeff[ 13]            *x41*x52
        +coeff[ 14]        *x31    *x52
        +coeff[ 15]    *x22*x32*x41    
        +coeff[ 16]    *x22*x31*x44    
    ;
    v_y_e_q1ex_100                                =v_y_e_q1ex_100                                
        +coeff[ 17]    *x24*x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31*x44    
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]            *x44*x51
        +coeff[ 22]*x11    *x31*x43*x51
        ;

    return v_y_e_q1ex_100                                ;
}
float p_e_q1ex_100                                (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.1599895E-03;
    float xmin[10]={
        -0.39998E-02,-0.60043E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.14059243E-03, 0.32895375E-01, 0.63313201E-01,-0.14296776E-02,
        -0.14431634E-02, 0.97211235E-03,-0.57168894E-04, 0.67768199E-03,
        -0.85814580E-04, 0.79233333E-03, 0.54260809E-03, 0.31119064E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q1ex_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]        *x31    *x51
        +coeff[  4]            *x41*x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q1ex_100                                =v_p_e_q1ex_100                                
        +coeff[  8]        *x34*x41    
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x43    
        ;

    return v_p_e_q1ex_100                                ;
}
float l_e_q1ex_100                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.1662744E-02;
    float xmin[10]={
        -0.39998E-02,-0.60043E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60067E-01, 0.59984E-01, 0.30029E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.16992737E-02,-0.37013565E-02,-0.21717254E-03,-0.60097943E-03,
        -0.17559869E-02,-0.13165508E-04, 0.25136181E-03,-0.23567276E-02,
        -0.30662279E-03, 0.88842987E-03,-0.44207947E-03, 0.18473681E-03,
        -0.65787777E-03, 0.41147231E-03,-0.22522880E-03,-0.78401424E-03,
         0.23230239E-03, 0.37061982E-03, 0.32641739E-03, 0.53612388E-04,
        -0.32332758E-03,-0.20875022E-03, 0.37479372E-03,-0.60241541E-03,
         0.91009344E-04, 0.12242339E-02, 0.14021826E-02,-0.81205080E-05,
        -0.45497942E-03,-0.40900178E-03, 0.59809099E-03,-0.41707745E-03,
         0.27160684E-03,-0.98724602E-04, 0.37434552E-03, 0.66135102E-03,
        -0.76226640E-03,-0.15687421E-02, 0.85673807E-03, 0.13138280E-02,
        -0.12030794E-02,-0.18225572E-02, 0.98856643E-03,-0.11285186E-02,
        -0.63264830E-03, 0.29350512E-03,-0.54363592E-03, 0.43617652E-03,
         0.23256138E-02,-0.15451659E-02, 0.98745758E-03,-0.96047763E-03,
        -0.45220126E-03,-0.58323424E-03, 0.78514323E-03,-0.33970850E-02,
        -0.18498764E-02, 0.42446080E-03, 0.65586448E-03, 0.15651123E-03,
         0.10291607E-02,-0.19368206E-03,-0.42822858E-03, 0.91377943E-03,
        -0.11276250E-02, 0.12882379E-02, 0.87975734E-03, 0.17832931E-02,
        -0.53149031E-03,-0.24223111E-02, 0.16002578E-02, 0.51887473E-03,
        -0.17490617E-02, 0.26329483E-02,-0.88256237E-03,-0.14722022E-02,
         0.52980788E-03, 0.11565019E-02,-0.85676863E-03, 0.11751945E-02,
        -0.30152639E-02, 0.11683230E-02, 0.69924881E-03,-0.30567045E-02,
         0.66114700E-03,-0.53840491E-03,-0.52391111E-04,-0.11275144E-03,
        -0.83810723E-04, 0.74670221E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q1ex_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]    *x21*x31        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22*x32        
        +coeff[  6]*x11    *x33        
        +coeff[  7]*x11    *x31*x42*x53
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[  8]*x12    *x33*x41*x51
        +coeff[  9]    *x22*x31*x42*x53
        +coeff[ 10]        *x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]    *x21*x31*x41    
        +coeff[ 13]    *x21        *x52
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]*x13                
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 17]    *x24            
        +coeff[ 18]        *x34        
        +coeff[ 19]    *x21*x32*x41    
        +coeff[ 20]        *x33*x41    
        +coeff[ 21]*x11*x21*x32        
        +coeff[ 22]*x11*x21*x31*x41    
        +coeff[ 23]*x11    *x31*x41*x51
        +coeff[ 24]*x12*x21    *x41    
        +coeff[ 25]    *x21*x32*x42    
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]    *x21    *x44    
        +coeff[ 28]    *x21*x33    *x51
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]    *x21*x32    *x52
        +coeff[ 31]    *x22        *x53
        +coeff[ 32]            *x42*x53
        +coeff[ 33]*x11*x24            
        +coeff[ 34]*x11*x21*x33        
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 35]*x11*x23    *x41    
        +coeff[ 36]*x11*x21*x31*x42    
        +coeff[ 37]*x11    *x32*x42    
        +coeff[ 38]*x11    *x31*x42*x51
        +coeff[ 39]*x11*x22        *x52
        +coeff[ 40]*x11*x21    *x41*x52
        +coeff[ 41]*x11        *x42*x52
        +coeff[ 42]*x12*x21    *x42    
        +coeff[ 43]*x12*x21        *x52
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 44]    *x23*x32*x41    
        +coeff[ 45]*x13        *x42    
        +coeff[ 46]*x13*x21        *x51
        +coeff[ 47]    *x23*x31*x41*x51
        +coeff[ 48]    *x22*x32*x41*x51
        +coeff[ 49]    *x22    *x43*x51
        +coeff[ 50]    *x22    *x41*x53
        +coeff[ 51]*x11*x22*x31*x41*x51
        +coeff[ 52]*x12*x22*x32        
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 53]*x12    *x31*x42*x51
        +coeff[ 54]*x12*x21    *x41*x52
        +coeff[ 55]    *x21*x34*x42    
        +coeff[ 56]    *x21*x33*x43    
        +coeff[ 57]*x13*x21*x31    *x51
        +coeff[ 58]    *x24*x32    *x51
        +coeff[ 59]    *x22*x34    *x51
        +coeff[ 60]*x13    *x31*x41*x51
        +coeff[ 61]*x13            *x53
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 62]        *x34    *x53
        +coeff[ 63]    *x21*x31*x42*x53
        +coeff[ 64]*x11*x22*x33*x41    
        +coeff[ 65]*x11*x24    *x42    
        +coeff[ 66]*x11    *x34*x42    
        +coeff[ 67]*x11*x22*x31*x43    
        +coeff[ 68]*x11    *x33*x43    
        +coeff[ 69]*x11*x23    *x41*x52
        +coeff[ 70]*x11        *x44*x52
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 71]*x11*x23        *x53
        +coeff[ 72]*x11*x22        *x54
        +coeff[ 73]*x11*x21    *x41*x54
        +coeff[ 74]*x12    *x31*x43*x51
        +coeff[ 75]*x12*x21    *x42*x52
        +coeff[ 76]*x12*x21    *x41*x53
        +coeff[ 77]*x12*x21        *x54
        +coeff[ 78]*x13*x24            
        +coeff[ 79]*x13*x23    *x41    
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 80]    *x23*x31*x43*x51
        +coeff[ 81]    *x21*x33*x43*x51
        +coeff[ 82]*x13    *x32    *x52
        +coeff[ 83]    *x22*x32*x41*x53
        +coeff[ 84]        *x33*x42*x53
        +coeff[ 85]        *x31*x43*x54
        +coeff[ 86]    *x21            
        +coeff[ 87]    *x21    *x41    
        +coeff[ 88]    *x21        *x51
    ;
    v_l_e_q1ex_100                                =v_l_e_q1ex_100                                
        +coeff[ 89]            *x41*x51
        ;

    return v_l_e_q1ex_100                                ;
}
