float x_e_q2en_1200                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1005289E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.10077743E-02, 0.13051587E+00, 0.58980370E-02,-0.11949731E-02,
        -0.93126466E-03,-0.24142419E-02,-0.18945914E-02,-0.36810875E-05,
         0.20221533E-03,-0.84098097E-03,-0.23535384E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_1200                                =v_x_e_q2en_1200                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_1200                                ;
}
float t_e_q2en_1200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1971692E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.19811493E-03,-0.20496240E-02, 0.23488343E-01, 0.22833794E-02,
         0.10646437E-03,-0.29921293E-03,-0.68725337E-03,-0.74423006E-03,
         0.12310942E-04,-0.33468386E-03,-0.29966421E-03,-0.12388360E-03,
        -0.10997051E-02,-0.85935305E-03,-0.27513908E-04,-0.69059123E-03,
        -0.26097827E-04,-0.18999897E-04, 0.10206266E-03, 0.67768618E-04,
        -0.35925501E-03,-0.79593796E-03, 0.12288331E-04,-0.12503284E-04,
         0.56147010E-05,-0.91396314E-05, 0.12780702E-04,-0.43938635E-05,
        -0.21705866E-05,-0.81049991E-04,-0.68959896E-04, 0.80815271E-05,
        -0.40017887E-04, 0.16275342E-04, 0.36107958E-04, 0.22161048E-05,
        -0.10335034E-04, 0.23852681E-05, 0.89226251E-05,-0.16936259E-04,
         0.45161560E-05, 0.55449800E-05,-0.52238156E-05,-0.88222287E-05,
         0.30160483E-04, 0.85969405E-05, 0.47221924E-05, 0.10146692E-04,
         0.13179003E-04,-0.11330789E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_1200                                =v_t_e_q2en_1200                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_1200                                =v_t_e_q2en_1200                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2en_1200                                =v_t_e_q2en_1200                                
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x21        *x53
        +coeff[ 28]*x11*x22*x32        
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]    *x23*x32*x41    
        +coeff[ 32]    *x22*x32*x42    
        +coeff[ 33]*x12*x22*x31    *x51
        +coeff[ 34]    *x23        *x53
    ;
    v_t_e_q2en_1200                                =v_t_e_q2en_1200                                
        +coeff[ 35]    *x21*x32    *x53
        +coeff[ 36]*x11*x21            
        +coeff[ 37]    *x22*x31        
        +coeff[ 38]*x13*x21            
        +coeff[ 39]    *x22*x31*x41    
        +coeff[ 40]    *x21    *x43    
        +coeff[ 41]*x13            *x51
        +coeff[ 42]*x12*x21        *x51
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_q2en_1200                                =v_t_e_q2en_1200                                
        +coeff[ 44]    *x21*x32    *x51
        +coeff[ 45]*x11*x21        *x52
        +coeff[ 46]*x11*x23*x31        
        +coeff[ 47]*x11*x23    *x41    
        +coeff[ 48]*x12*x21*x31*x41    
        +coeff[ 49]*x11*x21*x32*x41    
        ;

    return v_t_e_q2en_1200                                ;
}
float y_e_q2en_1200                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.1953420E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.92737908E-04, 0.21236432E+00, 0.12484381E+00,-0.29360880E-02,
        -0.28485225E-02,-0.21862409E-03,-0.14802624E-03, 0.15048900E-03,
        -0.25338665E-03, 0.20004739E-03,-0.36684465E-04, 0.12013289E-03,
         0.11250322E-03, 0.20158894E-04,-0.44536800E-03, 0.76250428E-04,
        -0.97104139E-05,-0.30524214E-04, 0.84597617E-04, 0.84897765E-04,
        -0.41215823E-03,-0.34791790E-03, 0.83477162E-05,-0.30522345E-03,
         0.52563566E-04,-0.11909023E-03,-0.11148611E-03,-0.78989753E-04,
         0.25232857E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]            *x43    
    ;
    v_y_e_q2en_1200                                =v_y_e_q2en_1200                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]    *x24    *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q2en_1200                                =v_y_e_q2en_1200                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]            *x42*x51
        +coeff[ 23]    *x22*x31*x42    
        +coeff[ 24]        *x34*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q2en_1200                                =v_y_e_q2en_1200                                
        +coeff[ 26]*x11*x23    *x41    
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]    *x21*x32*x43    
        ;

    return v_y_e_q2en_1200                                ;
}
float p_e_q2en_1200                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4308230E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.30515141E-04, 0.75744204E-02, 0.25919138E-01, 0.67063246E-03,
         0.15516298E-03,-0.28174356E-03,-0.23759210E-03,-0.29338128E-03,
        -0.17667878E-03,-0.39088045E-04,-0.16651046E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_1200                                =v_p_e_q2en_1200                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_1200                                ;
}
float l_e_q2en_1200                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2457720E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24589635E-02,-0.34050571E-02,-0.11864289E-02,-0.45705754E-02,
        -0.52332329E-02, 0.82520500E-03,-0.80686831E-03, 0.20504334E-03,
         0.10059811E-03, 0.32899665E-04,-0.38109673E-03,-0.42377706E-03,
        -0.59221544E-04,-0.27152119E-03,-0.53030257E-04,-0.44637572E-03,
        -0.13681132E-02,-0.26543977E-03,-0.71158182E-04, 0.30448541E-03,
         0.30944846E-03,-0.30312309E-03,-0.28932790E-03, 0.68803958E-03,
         0.27995938E-03,-0.61192486E-03, 0.81945630E-03,-0.35067863E-03,
         0.56310394E-03, 0.35572627E-02, 0.46938012E-03,-0.58588095E-03,
        -0.60512230E-03,-0.22863546E-03, 0.32575399E-03,-0.17659789E-02,
         0.15461617E-02, 0.43059606E-03,-0.72638784E-03, 0.22770025E-03,
         0.58306579E-03,-0.12685228E-02, 0.22788382E-03, 0.97956904E-03,
        -0.97962969E-03,-0.18078830E-02, 0.10382450E-03, 0.12096923E-02,
         0.17590169E-02,-0.19691451E-02,-0.23581169E-02, 0.13352494E-02,
         0.11511655E-02, 0.15986874E-02,-0.13305135E-02, 0.62244496E-03,
        -0.12927150E-02, 0.17833951E-02, 0.57082798E-03,-0.19685184E-02,
         0.27956904E-02,-0.80652186E-03,-0.32570090E-02,-0.38028387E-02,
        -0.35097611E-02, 0.86708256E-03,-0.31751537E-03, 0.10788309E-02,
        -0.24270427E-02,-0.13209867E-02, 0.23296913E-02,-0.41129958E-03,
        -0.14791523E-05, 0.28998914E-03,-0.37287889E-03,-0.18003906E-03,
        -0.45080559E-03, 0.27889031E-03, 0.20738452E-03,-0.21491373E-03,
        -0.39986087E-03,-0.10658971E-03, 0.11699903E-03,-0.94689334E-04,
        -0.26411450E-03, 0.21878660E-03,-0.19679455E-04,-0.20765787E-04,
        -0.38555614E-03, 0.39160394E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_1200                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x23*x32        
        +coeff[  6]*x12    *x32*x41*x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[  8]                *x51
        +coeff[  9]*x11            *x51
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]        *x33*x41    
        +coeff[ 15]            *x44    
        +coeff[ 16]*x11    *x32*x41    
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 17]*x11    *x31*x42    
        +coeff[ 18]*x11        *x43    
        +coeff[ 19]*x11*x22        *x51
        +coeff[ 20]*x11    *x31*x41*x51
        +coeff[ 21]*x11*x21        *x52
        +coeff[ 22]*x11        *x41*x52
        +coeff[ 23]*x12    *x31*x41    
        +coeff[ 24]    *x21*x33*x41    
        +coeff[ 25]    *x21*x31*x42*x51
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 26]        *x33    *x52
        +coeff[ 27]            *x43*x52
        +coeff[ 28]*x11    *x34        
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x12*x21*x31    *x51
        +coeff[ 31]*x12        *x42*x51
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]        *x31*x44*x51
        +coeff[ 34]*x11*x24    *x41    
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 35]*x11*x21*x31*x42*x51
        +coeff[ 36]*x11    *x32*x41*x52
        +coeff[ 37]*x11*x21*x31    *x53
        +coeff[ 38]*x11*x21    *x41*x53
        +coeff[ 39]*x12*x24            
        +coeff[ 40]*x12*x22        *x52
        +coeff[ 41]    *x21*x34*x42    
        +coeff[ 42]    *x22*x33    *x52
        +coeff[ 43]    *x24    *x41*x52
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 44]    *x21*x33    *x53
        +coeff[ 45]        *x33*x41*x53
        +coeff[ 46]        *x32*x42*x53
        +coeff[ 47]        *x31*x43*x53
        +coeff[ 48]    *x22*x31    *x54
        +coeff[ 49]        *x33    *x54
        +coeff[ 50]*x11*x24*x31*x41    
        +coeff[ 51]*x11    *x34*x42    
        +coeff[ 52]*x11    *x33*x41*x52
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 53]*x11*x22    *x42*x52
        +coeff[ 54]*x11        *x44*x52
        +coeff[ 55]*x12*x21*x34        
        +coeff[ 56]*x12*x22*x32    *x51
        +coeff[ 57]*x12*x22    *x42*x51
        +coeff[ 58]*x12*x21    *x41*x53
        +coeff[ 59]*x13*x22*x31*x41    
        +coeff[ 60]    *x23*x34    *x51
        +coeff[ 61]    *x23*x32*x42*x51
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 62]    *x21*x34*x42*x51
        +coeff[ 63]        *x34*x43*x51
        +coeff[ 64]        *x33*x44*x51
        +coeff[ 65]*x13        *x42*x52
        +coeff[ 66]    *x24    *x42*x52
        +coeff[ 67]*x13    *x31    *x53
        +coeff[ 68]    *x23*x32    *x53
        +coeff[ 69]    *x23*x31*x41*x53
        +coeff[ 70]    *x21*x32*x42*x53
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 71]            *x44*x54
        +coeff[ 72]*x11                
        +coeff[ 73]*x11        *x41    
        +coeff[ 74]    *x22*x31        
        +coeff[ 75]    *x22    *x41    
        +coeff[ 76]    *x21*x31*x41    
        +coeff[ 77]    *x21*x31    *x51
        +coeff[ 78]        *x31*x41*x51
        +coeff[ 79]*x11*x22            
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 80]*x11    *x31    *x51
        +coeff[ 81]*x11        *x41*x51
        +coeff[ 82]*x11            *x52
        +coeff[ 83]*x12*x21            
        +coeff[ 84]    *x24            
        +coeff[ 85]    *x23*x31        
        +coeff[ 86]    *x23    *x41    
        +coeff[ 87]    *x21*x32*x41    
        +coeff[ 88]    *x21*x31*x42    
    ;
    v_l_e_q2en_1200                                =v_l_e_q2en_1200                                
        +coeff[ 89]        *x32*x42    
        ;

    return v_l_e_q2en_1200                                ;
}
float x_e_q2en_1100                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.8602251E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.84582734E-03,-0.11945935E-02, 0.13052973E+00, 0.58962046E-02,
        -0.92251180E-03,-0.24022958E-02,-0.18875175E-02, 0.12809839E-04,
         0.20766002E-03,-0.84515387E-03,-0.23803585E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_1100                                =v_x_e_q2en_1100                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_1100                                ;
}
float t_e_q2en_1100                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1462754E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.14429804E-03,-0.20489064E-02, 0.23497337E-01, 0.22714727E-02,
         0.10888761E-03,-0.29195144E-03,-0.67073148E-03,-0.73853607E-03,
         0.89992363E-05,-0.36102880E-03,-0.28154670E-03,-0.11713917E-03,
        -0.11391734E-02,-0.87035151E-03,-0.33687636E-04,-0.69878687E-03,
        -0.44064738E-04,-0.29225199E-04, 0.11570452E-03, 0.58713118E-04,
         0.12446350E-04,-0.34529800E-03,-0.80539618E-03, 0.89649166E-05,
         0.20514775E-04,-0.18660563E-06, 0.24800208E-05,-0.27570968E-04,
        -0.54906395E-05, 0.82676070E-05, 0.29497694E-04, 0.29737279E-04,
        -0.21985261E-04,-0.17369466E-04,-0.19407300E-04,-0.22273131E-04,
         0.18218583E-04, 0.18775385E-04, 0.34928573E-04, 0.21249078E-04,
         0.18206685E-04, 0.30800806E-04, 0.57906567E-06,-0.27900098E-05,
        -0.63208436E-05, 0.65976283E-05,-0.47545245E-05,-0.13090662E-05,
         0.36485053E-05,-0.32956389E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_1100                                =v_t_e_q2en_1100                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_1100                                =v_t_e_q2en_1100                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x13                
    ;
    v_t_e_q2en_1100                                =v_t_e_q2en_1100                                
        +coeff[ 26]    *x22*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]*x13*x21            
        +coeff[ 29]    *x22*x31*x41    
        +coeff[ 30]    *x23        *x51
        +coeff[ 31]    *x21*x32    *x51
        +coeff[ 32]*x11*x23*x31        
        +coeff[ 33]    *x22*x33        
        +coeff[ 34]*x11*x22    *x42    
    ;
    v_t_e_q2en_1100                                =v_t_e_q2en_1100                                
        +coeff[ 35]    *x21    *x42*x52
        +coeff[ 36]*x13*x23            
        +coeff[ 37]    *x22*x32*x41*x51
        +coeff[ 38]*x11*x22    *x42*x51
        +coeff[ 39]*x11    *x31*x43*x51
        +coeff[ 40]*x12*x22        *x52
        +coeff[ 41]    *x21    *x42*x53
        +coeff[ 42]        *x31        
        +coeff[ 43]    *x22            
    ;
    v_t_e_q2en_1100                                =v_t_e_q2en_1100                                
        +coeff[ 44]*x12*x21            
        +coeff[ 45]*x11*x21*x31        
        +coeff[ 46]*x11*x21    *x41    
        +coeff[ 47]    *x22    *x41    
        +coeff[ 48]        *x31*x42    
        +coeff[ 49]*x11*x21        *x51
        ;

    return v_t_e_q2en_1100                                ;
}
float y_e_q2en_1100                                (float *x,int m){
    int ncoeff= 29;
    float avdat=  0.1990157E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.32227073E-03, 0.21234365E+00, 0.12489299E+00,-0.29419374E-02,
        -0.28524580E-02,-0.38250134E-03,-0.30729393E-03, 0.99144963E-04,
         0.12631144E-03,-0.51523457E-04, 0.98498764E-04, 0.87517095E-04,
         0.37794728E-05,-0.36894804E-03,-0.29113655E-04,-0.75794487E-04,
         0.89696761E-04,-0.35412572E-03,-0.16869265E-05, 0.12931338E-04,
        -0.17622340E-04, 0.73920324E-04, 0.31552769E-04, 0.86497283E-04,
        -0.13335374E-03,-0.10252748E-03,-0.57156038E-04, 0.56927442E-04,
         0.43589600E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]            *x43    
    ;
    v_y_e_q2en_1100                                =v_y_e_q2en_1100                                
        +coeff[  8]        *x31*x42    
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q2en_1100                                =v_y_e_q2en_1100                                
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x24    *x41*x51
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x22    *x41*x51
        +coeff[ 22]        *x32*x41*x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_q2en_1100                                =v_y_e_q2en_1100                                
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]    *x22    *x41*x52
        +coeff[ 28]        *x33    *x52
        ;

    return v_y_e_q2en_1100                                ;
}
float p_e_q2en_1100                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1866627E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.28103641E-05, 0.75693442E-02, 0.25904167E-01, 0.67080598E-03,
         0.15483622E-03,-0.28046570E-03,-0.23455193E-03,-0.29193281E-03,
        -0.17621755E-03,-0.32669781E-04,-0.16685123E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_1100                                =v_p_e_q2en_1100                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_1100                                ;
}
float l_e_q2en_1100                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2449288E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24248888E-02,-0.95482930E-04,-0.35512890E-02,-0.76473976E-03,
        -0.45248126E-02,-0.53758002E-02,-0.55684836E-03,-0.13354378E-03,
        -0.21616677E-03,-0.20587433E-04,-0.77148667E-04, 0.60532335E-03,
        -0.87394197E-04, 0.38384242E-03, 0.21534592E-03, 0.12863158E-02,
         0.27796908E-03, 0.85035477E-04, 0.13542827E-03,-0.41045042E-03,
         0.13885851E-02,-0.10889422E-03,-0.60801930E-03,-0.12615434E-02,
         0.85754902E-03,-0.36674607E-03, 0.43441055E-03, 0.32816496E-03,
         0.31972825E-03,-0.17579898E-03,-0.29741353E-03, 0.56402793E-03,
        -0.35706032E-02,-0.70128619E-03,-0.54604391E-03,-0.11428327E-02,
        -0.28205471E-03, 0.65290944E-04,-0.42127032E-03, 0.37149811E-03,
        -0.37949247E-03, 0.98216650E-03, 0.30391803E-02, 0.94815146E-03,
        -0.18521437E-02,-0.99816732E-03, 0.51806832E-03, 0.36168264E-03,
         0.17094937E-02, 0.10718885E-02, 0.93312230E-03,-0.81841467E-03,
        -0.53990236E-03,-0.10071081E-02, 0.82944735E-03, 0.77623042E-03,
         0.42858475E-03,-0.12613190E-02, 0.15151912E-02, 0.29328614E-02,
         0.56605681E-03,-0.57292340E-03,-0.10126116E-02, 0.84733719E-03,
         0.21400542E-02, 0.16869691E-02, 0.10809097E-02, 0.27270962E-02,
        -0.17208538E-02,-0.15319946E-02,-0.86128310E-03, 0.49000367E-03,
         0.11194182E-02,-0.89700910E-03, 0.61573315E-03,-0.13859993E-02,
        -0.13686715E-02,-0.10612750E-02,-0.37243741E-02, 0.11264960E-02,
         0.17646141E-02, 0.75386971E-03,-0.23983051E-02,-0.31314888E-04,
        -0.33923236E-04,-0.19182422E-03,-0.55515531E-04, 0.99333993E-04,
         0.20441681E-03,-0.91148257E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_1100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x23*x33        
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[  8]*x12                
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x21*x31    *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]        *x31*x41*x51
        +coeff[ 14]            *x42*x51
        +coeff[ 15]        *x31    *x52
        +coeff[ 16]*x11*x22            
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x24            
        +coeff[ 19]    *x22*x32        
        +coeff[ 20]    *x21*x31*x42    
        +coeff[ 21]    *x22*x31    *x51
        +coeff[ 22]*x11*x22*x31        
        +coeff[ 23]*x11*x21*x32        
        +coeff[ 24]*x11*x21*x31    *x51
        +coeff[ 25]*x11    *x32    *x51
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 26]*x11        *x41*x52
        +coeff[ 27]*x11            *x53
        +coeff[ 28]*x12*x22            
        +coeff[ 29]*x13*x21            
        +coeff[ 30]    *x24*x31        
        +coeff[ 31]    *x22*x32    *x51
        +coeff[ 32]    *x21*x31*x42*x51
        +coeff[ 33]        *x32*x42*x51
        +coeff[ 34]    *x21*x31    *x53
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 35]        *x31    *x54
        +coeff[ 36]*x11*x21*x33        
        +coeff[ 37]*x12    *x33        
        +coeff[ 38]*x12    *x31*x41*x51
        +coeff[ 39]*x12*x21        *x52
        +coeff[ 40]*x13*x22            
        +coeff[ 41]*x13    *x32        
        +coeff[ 42]    *x22*x31*x41*x52
        +coeff[ 43]        *x33*x41*x52
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 44]    *x22    *x42*x52
        +coeff[ 45]    *x21*x31*x42*x52
        +coeff[ 46]    *x21    *x43*x52
        +coeff[ 47]            *x44*x52
        +coeff[ 48]        *x31*x41*x54
        +coeff[ 49]*x11    *x34    *x51
        +coeff[ 50]*x11*x22*x31    *x52
        +coeff[ 51]*x11        *x43*x52
        +coeff[ 52]*x11*x22        *x53
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 53]*x11*x21*x31    *x53
        +coeff[ 54]*x12    *x31*x43    
        +coeff[ 55]*x12*x22*x31    *x51
        +coeff[ 56]*x12*x21*x31    *x52
        +coeff[ 57]*x12    *x31*x41*x52
        +coeff[ 58]*x13*x21*x32        
        +coeff[ 59]    *x21*x33*x42*x51
        +coeff[ 60]    *x22*x31*x42*x52
        +coeff[ 61]*x13            *x53
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 62]    *x21*x33    *x53
        +coeff[ 63]    *x22    *x42*x53
        +coeff[ 64]    *x21*x31*x42*x53
        +coeff[ 65]*x11*x23    *x42*x51
        +coeff[ 66]*x11*x21*x32*x42*x51
        +coeff[ 67]*x11*x21*x31*x43*x51
        +coeff[ 68]*x11*x22*x32    *x52
        +coeff[ 69]*x11*x22*x31*x41*x52
        +coeff[ 70]*x11    *x32*x42*x52
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 71]*x11        *x42*x54
        +coeff[ 72]*x12*x21*x33*x41    
        +coeff[ 73]*x12    *x33    *x52
        +coeff[ 74]*x13*x24            
        +coeff[ 75]*x13    *x32*x42    
        +coeff[ 76]    *x23*x33*x42    
        +coeff[ 77]        *x34*x44    
        +coeff[ 78]    *x24*x31*x41*x52
        +coeff[ 79]    *x22*x32*x42*x52
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 80]        *x32*x44*x52
        +coeff[ 81]    *x23*x31    *x54
        +coeff[ 82]        *x33*x41*x54
        +coeff[ 83]    *x21            
        +coeff[ 84]            *x41    
        +coeff[ 85]*x11                
        +coeff[ 86]            *x41*x51
        +coeff[ 87]                *x52
        +coeff[ 88]*x11*x21            
    ;
    v_l_e_q2en_1100                                =v_l_e_q2en_1100                                
        +coeff[ 89]*x11        *x41    
        ;

    return v_l_e_q2en_1100                                ;
}
float x_e_q2en_1000                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.6071448E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.57029614E-03, 0.13052392E+00, 0.58941771E-02,-0.11944289E-02,
        -0.92292897E-03,-0.23960280E-02,-0.18884349E-02, 0.13229405E-04,
         0.20616449E-03,-0.84185600E-03,-0.24236010E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_1000                                =v_x_e_q2en_1000                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_1000                                ;
}
float t_e_q2en_1000                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1050718E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.98413395E-04,-0.20489702E-02, 0.23510488E-01, 0.22823922E-02,
         0.10840314E-03,-0.29242894E-03,-0.70162630E-03,-0.74804283E-03,
        -0.79084648E-05,-0.35409804E-03,-0.28769008E-03,-0.12540641E-03,
        -0.11197001E-02,-0.86761586E-03,-0.31003212E-04,-0.67317154E-03,
        -0.29742316E-04,-0.23574867E-04, 0.12016993E-03, 0.66417248E-04,
        -0.30646430E-03,-0.75757300E-03,-0.67186279E-05,-0.76841352E-05,
         0.40707626E-04,-0.15330346E-04,-0.61282757E-04,-0.39632603E-04,
        -0.12434223E-04, 0.16080430E-04,-0.13899133E-04, 0.50709317E-04,
         0.19223855E-04, 0.14696686E-04,-0.96151143E-06, 0.15841525E-05,
         0.53834701E-05,-0.26662622E-05,-0.15279073E-05, 0.31759018E-05,
        -0.50855842E-05,-0.45932775E-05,-0.74954332E-05,-0.61570431E-05,
         0.10906759E-05,-0.39746233E-05, 0.53590397E-05, 0.80813124E-05,
         0.13331693E-04,-0.53412377E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_1000                                =v_t_e_q2en_1000                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_1000                                =v_t_e_q2en_1000                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x11    *x32    *x52
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2en_1000                                =v_t_e_q2en_1000                                
        +coeff[ 26]*x11*x22*x31*x41    
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]*x11*x22    *x41*x51
        +coeff[ 29]    *x22    *x41*x52
        +coeff[ 30]    *x22*x33    *x51
        +coeff[ 31]    *x21*x32*x42*x51
        +coeff[ 32]*x12*x21        *x53
        +coeff[ 33]*x11*x22        *x53
        +coeff[ 34]        *x31        
    ;
    v_t_e_q2en_1000                                =v_t_e_q2en_1000                                
        +coeff[ 35]*x11    *x31        
        +coeff[ 36]    *x22*x31        
        +coeff[ 37]        *x32*x41    
        +coeff[ 38]    *x21*x31    *x51
        +coeff[ 39]    *x21    *x41*x51
        +coeff[ 40]*x11            *x52
        +coeff[ 41]*x11*x23            
        +coeff[ 42]    *x23*x31        
        +coeff[ 43]*x11    *x33        
    ;
    v_t_e_q2en_1000                                =v_t_e_q2en_1000                                
        +coeff[ 44]*x12*x21    *x41    
        +coeff[ 45]    *x23    *x41    
        +coeff[ 46]*x11*x21    *x42    
        +coeff[ 47]    *x21*x31*x42    
        +coeff[ 48]    *x21*x32    *x51
        +coeff[ 49]    *x22    *x41*x51
        ;

    return v_t_e_q2en_1000                                ;
}
float y_e_q2en_1000                                (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.2475490E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.20237564E-03, 0.21221995E+00, 0.12487298E+00,-0.29413546E-02,
        -0.28557056E-02,-0.38097368E-03,-0.27173173E-03, 0.13737715E-03,
        -0.34309665E-04, 0.11666757E-03, 0.11515986E-03,-0.10361233E-04,
         0.41357584E-05,-0.40164811E-03, 0.11101657E-03,-0.15543763E-04,
        -0.11432668E-04, 0.10340717E-03, 0.96561846E-04,-0.34699490E-03,
        -0.13591713E-03,-0.89262176E-04, 0.15526362E-04, 0.87743960E-04,
        -0.11052570E-03,-0.11096548E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_1000                                =v_y_e_q2en_1000                                
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_1000                                =v_y_e_q2en_1000                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]*x11*x23    *x41    
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]        *x31*x41*x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
        ;

    return v_y_e_q2en_1000                                ;
}
float p_e_q2en_1000                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1425842E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.19518571E-04, 0.75617679E-02, 0.25879478E-01, 0.67270669E-03,
         0.15592118E-03,-0.28006063E-03,-0.23271456E-03,-0.28928832E-03,
        -0.17700173E-03,-0.30854164E-04,-0.16207676E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_1000                                =v_p_e_q2en_1000                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_1000                                ;
}
float l_e_q2en_1000                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2427795E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24373473E-02,-0.34735093E-02,-0.62748743E-03,-0.43117614E-02,
        -0.55065886E-02,-0.56062494E-04,-0.60190941E-03,-0.12590722E-03,
        -0.25547735E-03, 0.59004256E-03,-0.16523771E-04, 0.49500220E-03,
         0.39540092E-03,-0.46489868E-03, 0.50999474E-03,-0.33496326E-03,
        -0.32513295E-03,-0.20670201E-04,-0.61552314E-03,-0.91124192E-03,
         0.23542534E-03,-0.52858825E-03, 0.10114135E-02, 0.18283669E-02,
         0.82850296E-04,-0.31041953E-03,-0.12516277E-02,-0.36738563E-03,
         0.12237217E-02, 0.20220215E-02, 0.21205503E-02, 0.35462703E-03,
        -0.61486819E-03, 0.59448509E-03, 0.24060300E-03, 0.27362315E-03,
        -0.35436919E-04, 0.21787295E-02, 0.36148082E-04, 0.92176319E-03,
        -0.41101553E-03,-0.12307120E-02,-0.78121998E-03, 0.96624508E-03,
         0.70981309E-03,-0.49015961E-03,-0.13586872E-02, 0.46312888E-03,
         0.25101651E-02,-0.13053812E-02, 0.14151641E-02, 0.32537300E-03,
         0.66563085E-03,-0.44670178E-04, 0.20377508E-03, 0.17339225E-03,
         0.87599103E-04, 0.49830123E-03, 0.10219987E-03,-0.11363416E-03,
         0.28558268E-03,-0.27265001E-03,-0.12946974E-03, 0.49273560E-04,
        -0.16500398E-03,-0.88910580E-04, 0.24508761E-03,-0.11467399E-03,
         0.21326869E-03,-0.14544740E-03,-0.58645132E-03, 0.35124220E-03,
         0.30265481E-03,-0.25556496E-03, 0.10379123E-02,-0.11239660E-02,
        -0.10584000E-02,-0.35737838E-04,-0.13872895E-03,-0.16202887E-03,
         0.21946453E-03,-0.27834330E-03,-0.59798197E-03, 0.15113674E-03,
        -0.16574592E-03, 0.62522589E-03, 0.19829544E-03, 0.65568689E-03,
         0.27159831E-03,-0.10809317E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_1000                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22*x34        
        +coeff[  7]*x11*x21            
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[  8]    *x21*x31    *x51
        +coeff[  9]*x11*x21    *x41    
        +coeff[ 10]*x11*x21        *x51
        +coeff[ 11]*x12            *x51
        +coeff[ 12]    *x21*x33        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x22*x31    *x51
        +coeff[ 15]        *x33    *x51
        +coeff[ 16]    *x21*x31    *x52
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 17]*x12*x21        *x51
        +coeff[ 18]    *x24        *x51
        +coeff[ 19]*x11*x21    *x43    
        +coeff[ 20]*x11        *x41*x53
        +coeff[ 21]*x12    *x32    *x51
        +coeff[ 22]*x11*x23*x32        
        +coeff[ 23]*x11*x22    *x43    
        +coeff[ 24]*x11    *x32*x43    
        +coeff[ 25]*x11*x23    *x41*x51
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 26]*x11*x21*x32    *x52
        +coeff[ 27]*x11*x21*x31*x41*x52
        +coeff[ 28]*x11*x21    *x42*x52
        +coeff[ 29]*x11*x21*x31    *x53
        +coeff[ 30]*x11    *x31*x41*x53
        +coeff[ 31]*x12    *x31*x43    
        +coeff[ 32]*x12*x22*x31    *x51
        +coeff[ 33]*x12*x21        *x53
        +coeff[ 34]    *x23    *x44    
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 35]    *x24*x32    *x51
        +coeff[ 36]*x13    *x31*x41*x51
        +coeff[ 37]    *x22*x32*x42*x51
        +coeff[ 38]        *x33*x43*x51
        +coeff[ 39]    *x23        *x54
        +coeff[ 40]    *x21*x32    *x54
        +coeff[ 41]*x11*x21*x33*x42    
        +coeff[ 42]*x11    *x34*x41*x51
        +coeff[ 43]*x11*x23    *x42*x51
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 44]*x11*x22    *x43*x51
        +coeff[ 45]*x11    *x34    *x52
        +coeff[ 46]*x11*x21*x32*x41*x52
        +coeff[ 47]*x12    *x31*x41*x53
        +coeff[ 48]    *x23*x31*x43*x51
        +coeff[ 49]    *x21*x33*x43*x51
        +coeff[ 50]    *x23*x32*x41*x52
        +coeff[ 51]*x13        *x42*x52
        +coeff[ 52]    *x21    *x44*x53
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 53]    *x21            
        +coeff[ 54]    *x22*x31        
        +coeff[ 55]    *x21*x32        
        +coeff[ 56]        *x33        
        +coeff[ 57]    *x21*x31*x41    
        +coeff[ 58]        *x31*x42    
        +coeff[ 59]    *x21    *x41*x51
        +coeff[ 60]        *x31*x41*x51
        +coeff[ 61]    *x21        *x52
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 62]*x12    *x31        
        +coeff[ 63]*x13                
        +coeff[ 64]        *x33*x41    
        +coeff[ 65]    *x23        *x51
        +coeff[ 66]        *x32*x41*x51
        +coeff[ 67]            *x43*x51
        +coeff[ 68]        *x32    *x52
        +coeff[ 69]    *x21    *x41*x52
        +coeff[ 70]*x11*x22    *x41    
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 71]*x11*x21*x31*x41    
        +coeff[ 72]*x11    *x32*x41    
        +coeff[ 73]*x11*x21    *x42    
        +coeff[ 74]*x11    *x31*x42    
        +coeff[ 75]*x11*x21*x31    *x51
        +coeff[ 76]*x11    *x31*x41*x51
        +coeff[ 77]*x11    *x31    *x52
        +coeff[ 78]*x11        *x41*x52
        +coeff[ 79]*x12            *x52
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 80]*x13*x21            
        +coeff[ 81]*x13    *x31        
        +coeff[ 82]    *x24*x31        
        +coeff[ 83]    *x23*x32        
        +coeff[ 84]    *x24    *x41    
        +coeff[ 85]    *x23*x31*x41    
        +coeff[ 86]        *x34*x41    
        +coeff[ 87]    *x22*x31*x42    
        +coeff[ 88]    *x22    *x43    
    ;
    v_l_e_q2en_1000                                =v_l_e_q2en_1000                                
        +coeff[ 89]    *x21*x31*x43    
        ;

    return v_l_e_q2en_1000                                ;
}
float x_e_q2en_900                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4266146E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.39672083E-03, 0.13055076E+00, 0.58954577E-02,-0.11922999E-02,
        -0.92249154E-03,-0.23970697E-02,-0.18841928E-02,-0.11170805E-05,
         0.20679567E-03,-0.84158278E-03,-0.23687427E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_900                                =v_x_e_q2en_900                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_900                                ;
}
float t_e_q2en_900                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.7108434E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.65616827E-04,-0.20491118E-02, 0.23528960E-01, 0.22774511E-02,
         0.10945211E-03,-0.29744036E-03,-0.69112377E-03,-0.74956723E-03,
         0.75286794E-05,-0.33644726E-03,-0.29060396E-03,-0.12103483E-03,
        -0.10850519E-02,-0.84663345E-03,-0.30074269E-04,-0.71573205E-03,
        -0.31982807E-04,-0.25304640E-04, 0.12747958E-03, 0.67009845E-04,
        -0.34111759E-03,-0.81607851E-03, 0.30584474E-05, 0.23236771E-04,
        -0.16180866E-04,-0.82420747E-05, 0.42245406E-05, 0.27562393E-04,
        -0.18190040E-05,-0.54136188E-04,-0.38812326E-04, 0.13328868E-04,
        -0.68895142E-05,-0.26321488E-04, 0.58370239E-04, 0.21340975E-04,
        -0.88232184E-06, 0.89495927E-06,-0.54929801E-05,-0.19865497E-05,
        -0.22103591E-05,-0.55395190E-05,-0.19944966E-05, 0.26347686E-05,
        -0.28213265E-05, 0.40407253E-05, 0.94291172E-05,-0.76159549E-05,
         0.39249089E-05, 0.47119856E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_900                                =v_t_e_q2en_900                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_900                                =v_t_e_q2en_900                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x12*x21*x32    *x51
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]    *x21*x31    *x51
    ;
    v_t_e_q2en_900                                =v_t_e_q2en_900                                
        +coeff[ 26]*x11*x22*x31        
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x22    *x41*x51
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]*x11*x21*x32    *x51
        +coeff[ 32]*x13            *x52
        +coeff[ 33]*x12*x22    *x41*x51
        +coeff[ 34]    *x21*x32*x42*x51
    ;
    v_t_e_q2en_900                                =v_t_e_q2en_900                                
        +coeff[ 35]    *x23        *x53
        +coeff[ 36]                *x51
        +coeff[ 37]    *x22            
        +coeff[ 38]    *x21    *x41    
        +coeff[ 39]        *x31    *x51
        +coeff[ 40]                *x52
        +coeff[ 41]        *x31*x42    
        +coeff[ 42]            *x43    
        +coeff[ 43]*x11        *x41*x51
    ;
    v_t_e_q2en_900                                =v_t_e_q2en_900                                
        +coeff[ 44]*x12*x21*x31        
        +coeff[ 45]*x12    *x32        
        +coeff[ 46]*x12*x21    *x41    
        +coeff[ 47]*x11*x22    *x41    
        +coeff[ 48]        *x33*x41    
        +coeff[ 49]    *x22    *x42    
        ;

    return v_t_e_q2en_900                                ;
}
float y_e_q2en_900                                (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.2450944E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.16834466E-03, 0.21225050E+00, 0.12483311E+00,-0.29310863E-02,
        -0.28548331E-02,-0.26809998E-03,-0.16146387E-03, 0.19099713E-03,
        -0.81071725E-04, 0.11050332E-03, 0.11029056E-03,-0.25226578E-04,
        -0.24211779E-05,-0.48068885E-03, 0.13887216E-03,-0.10272912E-05,
        -0.82068240E-04, 0.36604863E-04, 0.89042653E-04,-0.37763189E-03,
        -0.40592812E-03, 0.17584462E-04, 0.81831204E-04,-0.14426391E-06,
         0.19604428E-04, 0.31799089E-04, 0.14939280E-04,-0.69573420E-04,
         0.47215737E-04,-0.30604372E-03,-0.47963487E-04,-0.15992539E-03,
         0.23418279E-04, 0.36404970E-04, 0.29713014E-04, 0.88871835E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_900                                =v_y_e_q2en_900                                
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_900                                =v_y_e_q2en_900                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x21*x31*x41    
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]            *x42*x51
        +coeff[ 24]        *x31*x41*x51
        +coeff[ 25]        *x31*x42*x51
    ;
    v_y_e_q2en_900                                =v_y_e_q2en_900                                
        +coeff[ 26]*x11        *x42*x51
        +coeff[ 27]    *x22    *x43    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x22*x33        
        +coeff[ 32]*x12        *x43    
        +coeff[ 33]*x11    *x31*x42*x51
        +coeff[ 34]*x11*x23    *x42    
    ;
    v_y_e_q2en_900                                =v_y_e_q2en_900                                
        +coeff[ 35]    *x24    *x41*x51
        ;

    return v_y_e_q2en_900                                ;
}
float p_e_q2en_900                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.1337345E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.55588212E-05, 0.75522987E-02, 0.25874566E-01, 0.67290425E-03,
         0.15649704E-03,-0.27958077E-03,-0.23638186E-03,-0.29460227E-03,
        -0.17967160E-03,-0.35394722E-04,-0.16375459E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_900                                =v_p_e_q2en_900                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_900                                ;
}
float l_e_q2en_900                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2414529E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24525048E-02,-0.35814187E-02,-0.10925711E-02,-0.42719739E-02,
        -0.52477713E-02,-0.17232155E-03,-0.10457801E-02, 0.60243363E-03,
         0.52332896E-04,-0.16083629E-03, 0.26981917E-03,-0.24609495E-03,
         0.10561349E-03, 0.32205656E-03, 0.81482896E-03,-0.65319403E-03,
        -0.69581764E-03,-0.16135993E-03,-0.17837058E-03, 0.66810235E-03,
         0.33144734E-03,-0.30616959E-03,-0.28747728E-03,-0.36262718E-03,
        -0.10399118E-02,-0.23254806E-03,-0.50866732E-03,-0.85651304E-03,
        -0.14022675E-02, 0.66852354E-03,-0.36959440E-03, 0.23056846E-02,
         0.12770505E-02,-0.99780395E-04, 0.10880075E-02,-0.11441427E-03,
        -0.31448973E-03,-0.99079125E-03, 0.45433218E-03,-0.16892789E-02,
         0.57928078E-03, 0.10879941E-02,-0.18095147E-02,-0.37000835E-03,
         0.31711764E-03, 0.11256423E-03, 0.73108729E-03,-0.41489225E-03,
        -0.59135025E-03, 0.15562753E-02,-0.10258354E-02, 0.71527460E-03,
        -0.64493431E-03, 0.74557692E-03, 0.16285899E-02,-0.30085042E-02,
        -0.26321723E-02,-0.27597456E-02, 0.11807855E-02, 0.12939830E-02,
        -0.12189291E-02, 0.19630168E-02, 0.79862977E-03,-0.11624309E-02,
         0.20290567E-02, 0.16559666E-02,-0.62028004E-03,-0.16186381E-02,
         0.58304134E-03, 0.10444089E-02, 0.61512185E-03,-0.92651707E-03,
        -0.73007314E-03, 0.41122708E-03, 0.65177868E-04,-0.23527259E-03,
        -0.19212533E-03, 0.93213508E-04, 0.97683100E-04,-0.81325721E-04,
         0.15424764E-03,-0.98641438E-04, 0.19866033E-03,-0.10427076E-03,
         0.27978147E-03,-0.35322478E-03, 0.33255140E-03,-0.16698430E-03,
         0.22831580E-03, 0.25754789E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_900                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x21*x31*x42    
        +coeff[  7]    *x23*x33        
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[  8]                *x51
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x32    *x51
        +coeff[ 13]    *x21    *x41*x51
        +coeff[ 14]*x11*x21*x31        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]*x11        *x41*x51
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 17]*x12    *x31        
        +coeff[ 18]*x12            *x51
        +coeff[ 19]*x11    *x31*x41*x51
        +coeff[ 20]*x12    *x31*x41    
        +coeff[ 21]*x12    *x31    *x51
        +coeff[ 22]*x13*x21            
        +coeff[ 23]        *x34*x41    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]    *x22    *x41*x52
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 26]    *x21        *x54
        +coeff[ 27]*x11*x21*x33        
        +coeff[ 28]*x11*x21*x31*x42    
        +coeff[ 29]*x11*x21*x32    *x51
        +coeff[ 30]*x11    *x33    *x51
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11*x21    *x41*x52
        +coeff[ 33]*x11    *x31    *x53
        +coeff[ 34]*x11        *x41*x53
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 35]*x12*x21*x32        
        +coeff[ 36]*x12*x21*x31*x41    
        +coeff[ 37]*x12*x21    *x41*x51
        +coeff[ 38]*x12            *x53
        +coeff[ 39]*x11*x22*x32*x41    
        +coeff[ 40]*x11    *x32*x43    
        +coeff[ 41]*x11*x22*x32    *x51
        +coeff[ 42]*x11    *x31*x43*x51
        +coeff[ 43]*x11    *x32    *x53
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 44]*x11    *x31    *x54
        +coeff[ 45]*x12*x24            
        +coeff[ 46]*x12*x22*x32        
        +coeff[ 47]*x12        *x44    
        +coeff[ 48]*x13*x22*x31        
        +coeff[ 49]    *x22*x32*x42*x51
        +coeff[ 50]    *x24    *x41*x52
        +coeff[ 51]    *x22*x31*x41*x53
        +coeff[ 52]    *x21    *x43*x53
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 53]    *x23        *x54
        +coeff[ 54]*x11*x22*x34        
        +coeff[ 55]*x11*x22*x33    *x51
        +coeff[ 56]*x11    *x31*x44*x51
        +coeff[ 57]*x11*x21*x32*x41*x52
        +coeff[ 58]*x11    *x33*x41*x52
        +coeff[ 59]*x11*x22*x31    *x53
        +coeff[ 60]*x11    *x32*x41*x53
        +coeff[ 61]*x12*x21*x31*x43    
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 62]*x12    *x31*x44    
        +coeff[ 63]*x12    *x31*x43*x51
        +coeff[ 64]*x12*x21*x32    *x52
        +coeff[ 65]*x12*x22    *x41*x52
        +coeff[ 66]*x13*x24            
        +coeff[ 67]*x13*x22*x32        
        +coeff[ 68]*x13*x22    *x42    
        +coeff[ 69]*x13*x22*x31    *x51
        +coeff[ 70]*x13    *x32    *x52
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 71]        *x34*x42*x52
        +coeff[ 72]    *x22*x32*x41*x53
        +coeff[ 73]            *x44*x54
        +coeff[ 74]    *x21            
        +coeff[ 75]*x11                
        +coeff[ 76]    *x21*x31        
        +coeff[ 77]*x11    *x31        
        +coeff[ 78]*x11        *x41    
        +coeff[ 79]*x11            *x51
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 80]    *x22*x31        
        +coeff[ 81]        *x33        
        +coeff[ 82]    *x21    *x42    
        +coeff[ 83]    *x21*x31    *x51
        +coeff[ 84]        *x31*x41*x51
        +coeff[ 85]    *x21        *x52
        +coeff[ 86]*x11*x22            
        +coeff[ 87]*x11*x21        *x51
        +coeff[ 88]*x13                
    ;
    v_l_e_q2en_900                                =v_l_e_q2en_900                                
        +coeff[ 89]    *x23*x31        
        ;

    return v_l_e_q2en_900                                ;
}
float x_e_q2en_800                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.7257545E-05;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.93394638E-05, 0.13058105E+00, 0.58937008E-02,-0.11911457E-02,
        -0.92036743E-03,-0.23902755E-02,-0.18791918E-02, 0.72729659E-06,
         0.20417335E-03,-0.83841401E-03,-0.23908018E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_800                                =v_x_e_q2en_800                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_800                                ;
}
float t_e_q2en_800                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1730047E-04;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.16260627E-04,-0.20461690E-02, 0.23551282E-01, 0.22770485E-02,
         0.10902716E-03,-0.29239155E-03,-0.68232190E-03,-0.75528736E-03,
        -0.33545030E-05,-0.35060878E-03,-0.28220550E-03,-0.12632618E-03,
        -0.10921091E-02,-0.84144139E-03,-0.33812397E-04,-0.70498104E-03,
        -0.21778915E-04,-0.29561008E-04, 0.68288893E-04, 0.61197839E-04,
        -0.34285831E-03,-0.78908680E-03, 0.45547346E-04,-0.17964245E-04,
        -0.60066950E-05,-0.25983525E-05, 0.35504017E-04,-0.56694422E-04,
        -0.21181409E-04,-0.30781019E-04, 0.21356082E-04,-0.89127579E-05,
         0.27119215E-04, 0.20954469E-04, 0.27180447E-04, 0.21028611E-04,
        -0.33809036E-04, 0.10693810E-03, 0.55802386E-04, 0.15622865E-04,
        -0.27328012E-05,-0.44043095E-05, 0.22879346E-05,-0.23157343E-05,
         0.31632399E-05,-0.81272492E-05,-0.10919520E-04, 0.40745986E-05,
         0.32427754E-05,-0.46028281E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_800                                =v_t_e_q2en_800                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_800                                =v_t_e_q2en_800                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x23    *x41    
    ;
    v_t_e_q2en_800                                =v_t_e_q2en_800                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]*x11*x22*x31*x41    
        +coeff[ 28]*x11    *x33*x41    
        +coeff[ 29]*x11*x22    *x42    
        +coeff[ 30]    *x23*x31    *x51
        +coeff[ 31]*x11*x21        *x53
        +coeff[ 32]*x11*x22*x33        
        +coeff[ 33]    *x23    *x43    
        +coeff[ 34]*x12*x23        *x51
    ;
    v_t_e_q2en_800                                =v_t_e_q2en_800                                
        +coeff[ 35]*x13*x21*x31    *x51
        +coeff[ 36]*x12*x21*x32    *x51
        +coeff[ 37]    *x23*x31*x41*x51
        +coeff[ 38]    *x23    *x42*x51
        +coeff[ 39]*x11*x21    *x41*x53
        +coeff[ 40]    *x22            
        +coeff[ 41]*x12        *x41    
        +coeff[ 42]            *x43    
        +coeff[ 43]    *x22        *x51
    ;
    v_t_e_q2en_800                                =v_t_e_q2en_800                                
        +coeff[ 44]*x11    *x31    *x51
        +coeff[ 45]    *x21*x31    *x51
        +coeff[ 46]*x11*x22*x31        
        +coeff[ 47]    *x23*x31        
        +coeff[ 48]*x11*x21*x32        
        +coeff[ 49]    *x22*x31*x41    
        ;

    return v_t_e_q2en_800                                ;
}
float y_e_q2en_800                                (float *x,int m){
    int ncoeff= 32;
    float avdat=  0.9358152E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
        -0.97617001E-03, 0.21227369E+00, 0.12485310E+00,-0.29277911E-02,
        -0.28535605E-02,-0.29538164E-03,-0.22924571E-03, 0.15951152E-03,
        -0.40980332E-04, 0.12061011E-03, 0.11338851E-03, 0.13776767E-04,
         0.99040553E-05,-0.44210252E-03, 0.82842074E-04,-0.22924662E-04,
         0.31735699E-05,-0.40972259E-06, 0.97821307E-04,-0.40535728E-03,
        -0.28090546E-03,-0.13354186E-03, 0.47141384E-04, 0.19473378E-04,
        -0.27090719E-03,-0.19309853E-04,-0.11179191E-03,-0.65218948E-04,
        -0.89083449E-04,-0.62515937E-04,-0.78735742E-04, 0.97078955E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_800                                =v_y_e_q2en_800                                
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_800                                =v_y_e_q2en_800                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]*x11*x23    *x41    
        +coeff[ 22]    *x22*x31*x44    
        +coeff[ 23]*x11*x21    *x42    
        +coeff[ 24]    *x22*x31*x42    
        +coeff[ 25]        *x31*x43*x51
    ;
    v_y_e_q2en_800                                =v_y_e_q2en_800                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]*x11*x21*x32*x41    
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]        *x32*x43*x51
        +coeff[ 30]    *x22*x31*x42*x51
        +coeff[ 31]    *x24    *x41*x51
        ;

    return v_y_e_q2en_800                                ;
}
float p_e_q2en_800                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.1586724E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.16293832E-03, 0.75418004E-02, 0.25856312E-01, 0.67335315E-03,
         0.15723197E-03,-0.27837398E-03,-0.23553004E-03,-0.29319446E-03,
        -0.17798276E-03,-0.34910212E-04,-0.16340835E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_800                                =v_p_e_q2en_800                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_800                                ;
}
float l_e_q2en_800                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2410826E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24476747E-02,-0.37642259E-02,-0.10693730E-02,-0.41490202E-02,
        -0.53021018E-02, 0.65108109E-03, 0.87434932E-03,-0.79992664E-04,
        -0.93229050E-04, 0.10057285E-02, 0.45541226E-03, 0.66857314E-03,
         0.78680669E-05,-0.24094807E-03, 0.99998491E-03, 0.35880716E-03,
         0.32315584E-03, 0.16224710E-03,-0.45120847E-03,-0.33107700E-03,
        -0.12393086E-03,-0.59976312E-03, 0.61355031E-03,-0.70581300E-03,
         0.24724181E-03, 0.99028694E-03,-0.61265624E-03,-0.96896588E-03,
         0.66664862E-03, 0.16899131E-02, 0.78260672E-03, 0.13716168E-02,
        -0.24304872E-03,-0.31954453E-02,-0.87776571E-03,-0.15809555E-02,
         0.84820401E-03, 0.71937271E-03,-0.60336734E-03,-0.15022822E-02,
         0.15248121E-02, 0.22773856E-03, 0.46810135E-03,-0.13061900E-02,
        -0.13413070E-02,-0.96309278E-03, 0.37204887E-03,-0.69416390E-03,
         0.32652782E-02,-0.19006220E-03, 0.12891187E-03,-0.11919677E-03,
         0.56143574E-04,-0.32206677E-03, 0.15472998E-03,-0.11132855E-03,
         0.85957930E-04, 0.49058264E-04,-0.14298524E-03, 0.16929317E-03,
         0.70156864E-04, 0.19146236E-03, 0.24069827E-03, 0.14668316E-03,
        -0.24755759E-03, 0.54647389E-03,-0.14440005E-04, 0.31889288E-03,
        -0.65016357E-03, 0.78261347E-03,-0.18447606E-03,-0.16450699E-03,
         0.16810221E-03,-0.14944169E-03,-0.16798168E-03, 0.19698050E-03,
         0.42794546E-03,-0.65710861E-03,-0.21414479E-03, 0.81029732E-03,
        -0.62940916E-03, 0.86234120E-03,-0.52407751E-03,-0.32475943E-03,
        -0.96255108E-05,-0.29899389E-03,-0.24715642E-03, 0.35674323E-03,
        -0.33683813E-03,-0.37428088E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_800                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]            *x42*x51
        +coeff[  6]*x11*x21*x31*x42    
        +coeff[  7]    *x21        *x51
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[  8]*x12                
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]    *x24            
        +coeff[ 11]    *x22*x31*x41    
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]    *x22        *x52
        +coeff[ 14]        *x32    *x52
        +coeff[ 15]*x11*x21*x32        
        +coeff[ 16]*x11*x21*x31    *x51
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 17]*x11    *x32    *x51
        +coeff[ 18]*x12*x21    *x41    
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]        *x33*x41*x51
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]            *x42*x53
        +coeff[ 24]*x11    *x33    *x51
        +coeff[ 25]*x11    *x31*x41*x52
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 26]*x13    *x31*x41    
        +coeff[ 27]    *x22*x33*x41    
        +coeff[ 28]    *x24*x31    *x51
        +coeff[ 29]    *x23*x31*x41*x51
        +coeff[ 30]        *x33*x42*x51
        +coeff[ 31]    *x22*x31*x41*x52
        +coeff[ 32]        *x33    *x53
        +coeff[ 33]    *x21*x31*x41*x53
        +coeff[ 34]        *x32    *x54
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 35]*x11*x24    *x41    
        +coeff[ 36]*x11*x22    *x43    
        +coeff[ 37]*x11*x24        *x51
        +coeff[ 38]*x11*x22        *x53
        +coeff[ 39]*x13*x21    *x41*x51
        +coeff[ 40]    *x21*x34*x41*x51
        +coeff[ 41]    *x23    *x43*x51
        +coeff[ 42]    *x21*x33    *x53
        +coeff[ 43]*x12    *x34*x41    
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 44]*x12    *x33*x42    
        +coeff[ 45]*x12*x23    *x41*x51
        +coeff[ 46]    *x21*x34*x42*x51
        +coeff[ 47]*x13*x22        *x52
        +coeff[ 48]    *x21*x33*x41*x53
        +coeff[ 49]    *x21*x31        
        +coeff[ 50]    *x21    *x41    
        +coeff[ 51]        *x31    *x51
        +coeff[ 52]            *x41*x51
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 53]*x11    *x31        
        +coeff[ 54]*x11            *x51
        +coeff[ 55]    *x21*x32        
        +coeff[ 56]            *x41*x52
        +coeff[ 57]*x11    *x32        
        +coeff[ 58]*x11        *x42    
        +coeff[ 59]*x11            *x52
        +coeff[ 60]*x12    *x31        
        +coeff[ 61]    *x23*x31        
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 62]    *x22    *x41*x51
        +coeff[ 63]    *x21*x31    *x52
        +coeff[ 64]        *x31*x41*x52
        +coeff[ 65]*x11*x22    *x41    
        +coeff[ 66]*x11    *x32*x41    
        +coeff[ 67]*x11    *x31*x42    
        +coeff[ 68]*x11*x22        *x51
        +coeff[ 69]*x11*x21    *x41*x51
        +coeff[ 70]*x11    *x31*x41*x51
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 71]*x11*x21        *x52
        +coeff[ 72]*x11    *x31    *x52
        +coeff[ 73]*x12*x22            
        +coeff[ 74]*x12        *x41*x51
        +coeff[ 75]*x13    *x31        
        +coeff[ 76]    *x24*x31        
        +coeff[ 77]    *x23*x32        
        +coeff[ 78]    *x22*x33        
        +coeff[ 79]    *x21*x34        
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 80]    *x23*x31*x41    
        +coeff[ 81]    *x21*x33*x41    
        +coeff[ 82]    *x22*x31*x42    
        +coeff[ 83]    *x21*x31*x43    
        +coeff[ 84]        *x32*x43    
        +coeff[ 85]    *x21    *x43*x51
        +coeff[ 86]    *x22*x31    *x52
        +coeff[ 87]    *x22    *x41*x52
        +coeff[ 88]        *x32*x41*x52
    ;
    v_l_e_q2en_800                                =v_l_e_q2en_800                                
        +coeff[ 89]        *x31*x41*x53
        ;

    return v_l_e_q2en_800                                ;
}
float x_e_q2en_700                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.6390757E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.64314983E-03, 0.13060570E+00, 0.58892411E-02,-0.11912897E-02,
        -0.91685436E-03,-0.23978697E-02,-0.18817717E-02, 0.35381870E-05,
         0.20574166E-03,-0.83929388E-03,-0.23437539E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_700                                =v_x_e_q2en_700                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_700                                ;
}
float t_e_q2en_700                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1061771E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.10718190E-03,-0.20472261E-02, 0.23571473E-01, 0.22692496E-02,
         0.10626120E-03,-0.27738142E-03,-0.66744181E-03,-0.73707890E-03,
         0.70854810E-06,-0.37040637E-03,-0.26998078E-03,-0.12356578E-03,
        -0.11244424E-02,-0.87799330E-03,-0.36503327E-04,-0.71495684E-03,
        -0.27961363E-04,-0.24963098E-04, 0.33001354E-04, 0.10187925E-03,
         0.68951180E-04,-0.34931695E-03,-0.81028457E-03, 0.11024582E-04,
        -0.15414015E-05,-0.14021665E-04,-0.43883206E-05,-0.59393960E-05,
        -0.16966407E-04, 0.62206340E-07, 0.30999312E-04, 0.28307490E-04,
        -0.56582034E-04,-0.44329205E-04,-0.12728664E-04, 0.18712830E-04,
         0.22614253E-04,-0.16995218E-05, 0.69311545E-05,-0.10193028E-04,
         0.23065190E-05,-0.51413244E-05, 0.56072872E-05, 0.56852828E-05,
         0.43521486E-05,-0.35935698E-05,-0.19051287E-04,-0.14230569E-04,
         0.19557223E-04, 0.16655349E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_700                                =v_t_e_q2en_700                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_700                                =v_t_e_q2en_700                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]*x12*x21*x32    *x51
        +coeff[ 24]        *x31*x41    
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2en_700                                =v_t_e_q2en_700                                
        +coeff[ 26]*x11*x21    *x41    
        +coeff[ 27]*x11            *x52
        +coeff[ 28]    *x22*x31*x41    
        +coeff[ 29]*x12*x21        *x51
        +coeff[ 30]    *x21*x32    *x51
        +coeff[ 31]*x11*x23    *x41    
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]*x12*x22        *x51
    ;
    v_t_e_q2en_700                                =v_t_e_q2en_700                                
        +coeff[ 35]    *x22        *x53
        +coeff[ 36]*x11*x22*x32    *x51
        +coeff[ 37]                *x52
        +coeff[ 38]*x12*x21            
        +coeff[ 39]    *x22        *x51
        +coeff[ 40]        *x32    *x51
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x31*x43    
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_t_e_q2en_700                                =v_t_e_q2en_700                                
        +coeff[ 44]*x11*x21        *x52
        +coeff[ 45]    *x21    *x41*x52
        +coeff[ 46]*x12*x23            
        +coeff[ 47]*x11*x21*x32*x41    
        +coeff[ 48]    *x22*x32*x41    
        +coeff[ 49]    *x22*x31*x42    
        ;

    return v_t_e_q2en_700                                ;
}
float y_e_q2en_700                                (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.4038849E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.47060222E-03, 0.21216558E+00, 0.12480491E+00,-0.29303988E-02,
        -0.28501321E-02,-0.14387621E-03,-0.17921320E-03, 0.14655355E-03,
        -0.20257320E-03, 0.21196534E-03,-0.49574461E-04, 0.11830437E-03,
         0.10569966E-03,-0.49747073E-03,-0.10648374E-03, 0.24671330E-04,
        -0.36309798E-05, 0.89388581E-04,-0.41913838E-03,-0.49426744E-03,
         0.11674944E-05, 0.30364299E-05, 0.85086540E-04,-0.81439948E-05,
        -0.99943918E-05,-0.13463455E-04, 0.73941912E-04,-0.43355842E-03,
        -0.46016718E-04, 0.37643429E-04,-0.16198405E-03,-0.68133457E-04,
        -0.22842163E-04,-0.20299567E-04, 0.33098760E-04, 0.24514204E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]            *x43    
    ;
    v_y_e_q2en_700                                =v_y_e_q2en_700                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]*x11*x23    *x41    
        +coeff[ 15]    *x24    *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q2en_700                                =v_y_e_q2en_700                                
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]    *x21*x31        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x12        *x41    
    ;
    v_y_e_q2en_700                                =v_y_e_q2en_700                                
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x11*x22    *x42    
        +coeff[ 29]*x11    *x32*x42    
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x11        *x45    
        +coeff[ 33]    *x23    *x43    
        +coeff[ 34]    *x21*x31*x43*x51
    ;
    v_y_e_q2en_700                                =v_y_e_q2en_700                                
        +coeff[ 35]*x11*x24    *x41    
        ;

    return v_y_e_q2en_700                                ;
}
float p_e_q2en_700                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.7446238E-04;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.81958562E-04, 0.75241337E-02, 0.25824692E-01, 0.67568984E-03,
         0.15857060E-03,-0.27608089E-03,-0.23452422E-03,-0.29159468E-03,
        -0.17614415E-03,-0.40182618E-04,-0.16020253E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_700                                =v_p_e_q2en_700                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_700                                ;
}
float l_e_q2en_700                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2414695E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.23236049E-02,-0.32757723E-02,-0.97771536E-03,-0.41093505E-02,
        -0.53106230E-02,-0.13409976E-03, 0.63991093E-03,-0.20874713E-02,
         0.88803667E-04,-0.23578902E-03, 0.15456995E-03, 0.14172461E-02,
         0.15725982E-02,-0.62174699E-03, 0.46245896E-03,-0.42050163E-03,
         0.52450987E-03,-0.11525264E-02,-0.99369604E-03, 0.53280342E-03,
         0.53343870E-03, 0.18714690E-03, 0.21233023E-02,-0.41697402E-02,
        -0.17667738E-02, 0.82171685E-03,-0.18844466E-02,-0.94806001E-03,
        -0.58312766E-03,-0.92079514E-03, 0.61152829E-03, 0.58500003E-03,
        -0.36995675E-03, 0.68041240E-03, 0.19478850E-02,-0.23056061E-02,
         0.17301474E-02,-0.60075574E-03,-0.66125684E-03,-0.47298125E-03,
        -0.19317850E-02,-0.94019907E-03,-0.10867309E-02, 0.29521025E-03,
        -0.18388343E-03, 0.14288694E-02,-0.21982772E-03, 0.97029045E-03,
        -0.12898769E-02, 0.10913979E-02,-0.40257550E-02,-0.66280640E-02,
         0.42409770E-03, 0.25955986E-02, 0.73467774E-04, 0.59504719E-05,
        -0.85521679E-04, 0.16843466E-03, 0.13071715E-04, 0.21552188E-03,
        -0.10944777E-03, 0.99184974E-04, 0.39774636E-03,-0.74554264E-03,
        -0.95585136E-04,-0.21575348E-03, 0.99086923E-04,-0.13573302E-03,
        -0.18470729E-03,-0.52911352E-03, 0.71556552E-03, 0.25197980E-03,
         0.37816944E-03,-0.21804185E-03, 0.19166965E-03, 0.11945349E-03,
         0.21268512E-03, 0.45677286E-03, 0.85399799E-04,-0.25429646E-03,
         0.10370573E-03, 0.17884104E-03, 0.30611732E-03,-0.10188544E-02,
         0.42159559E-03, 0.27783675E-03, 0.31223841E-03,-0.11702102E-03,
        -0.60971203E-03, 0.41628009E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_700                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]*x11*x24    *x41    
        +coeff[  7]*x11    *x32*x43    
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[  8]                *x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]            *x43*x51
        +coeff[ 11]        *x31*x41*x52
        +coeff[ 12]*x11*x21*x31*x41    
        +coeff[ 13]*x11    *x31*x42    
        +coeff[ 14]*x12        *x42    
        +coeff[ 15]*x12*x21        *x51
        +coeff[ 16]*x12            *x52
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 17]    *x21*x31*x42*x51
        +coeff[ 18]*x11*x24            
        +coeff[ 19]*x11        *x44    
        +coeff[ 20]*x13*x22            
        +coeff[ 21]    *x23    *x42*x51
        +coeff[ 22]    *x21*x32*x41*x52
        +coeff[ 23]    *x21    *x43*x52
        +coeff[ 24]        *x31*x43*x52
        +coeff[ 25]    *x21*x31    *x54
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 26]*x11*x23*x31*x41    
        +coeff[ 27]*x12*x24            
        +coeff[ 28]*x12*x22    *x41*x51
        +coeff[ 29]*x12            *x54
        +coeff[ 30]*x13*x21*x32        
        +coeff[ 31]*x13    *x32*x41    
        +coeff[ 32]*x13*x22        *x51
        +coeff[ 33]    *x23*x33    *x51
        +coeff[ 34]*x13*x21    *x41*x51
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 35]    *x23*x31*x41*x52
        +coeff[ 36]    *x21*x31*x43*x52
        +coeff[ 37]    *x21    *x43*x53
        +coeff[ 38]        *x33    *x54
        +coeff[ 39]        *x32*x41*x54
        +coeff[ 40]*x11*x23*x31*x41*x51
        +coeff[ 41]*x11    *x34*x41*x51
        +coeff[ 42]*x11*x23    *x42*x51
        +coeff[ 43]*x11*x22    *x43*x51
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 44]*x12*x22*x33        
        +coeff[ 45]*x12*x22*x31    *x52
        +coeff[ 46]    *x21*x34*x43    
        +coeff[ 47]*x13        *x44    
        +coeff[ 48]        *x34*x44    
        +coeff[ 49]*x13        *x43*x51
        +coeff[ 50]    *x23*x32*x41*x52
        +coeff[ 51]    *x23*x31*x42*x52
        +coeff[ 52]    *x24        *x54
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 53]    *x21    *x43*x54
        +coeff[ 54]*x11                
        +coeff[ 55]*x11*x21            
        +coeff[ 56]*x11            *x51
        +coeff[ 57]    *x22*x31        
        +coeff[ 58]        *x33        
        +coeff[ 59]    *x21    *x42    
        +coeff[ 60]        *x32    *x51
        +coeff[ 61]        *x31*x41*x51
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 62]*x11*x22            
        +coeff[ 63]*x11        *x42    
        +coeff[ 64]*x11*x21        *x51
        +coeff[ 65]*x11        *x41*x51
        +coeff[ 66]*x12        *x41    
        +coeff[ 67]*x13                
        +coeff[ 68]    *x21*x33        
        +coeff[ 69]        *x33*x41    
        +coeff[ 70]    *x21*x31*x42    
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 71]        *x32*x42    
        +coeff[ 72]    *x21    *x43    
        +coeff[ 73]            *x44    
        +coeff[ 74]    *x22*x31    *x51
        +coeff[ 75]        *x32*x41*x51
        +coeff[ 76]    *x21    *x42*x51
        +coeff[ 77]    *x21    *x41*x52
        +coeff[ 78]    *x21        *x53
        +coeff[ 79]*x11*x23            
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 80]*x11        *x43    
        +coeff[ 81]*x11*x21*x31    *x51
        +coeff[ 82]*x11    *x32    *x51
        +coeff[ 83]*x11*x21    *x41*x51
        +coeff[ 84]*x11    *x31*x41*x51
        +coeff[ 85]*x11        *x42*x51
        +coeff[ 86]*x12*x22            
        +coeff[ 87]*x12    *x31    *x51
        +coeff[ 88]    *x24*x31        
    ;
    v_l_e_q2en_700                                =v_l_e_q2en_700                                
        +coeff[ 89]    *x22*x33        
        ;

    return v_l_e_q2en_700                                ;
}
float x_e_q2en_600                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1168794E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.91133945E-04, 0.13067347E+00, 0.58921599E-02,-0.11905706E-02,
        -0.92113874E-03,-0.24012423E-02,-0.18836902E-02,-0.84103140E-05,
         0.20295744E-03,-0.83117298E-03,-0.23163864E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_600                                =v_x_e_q2en_600                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_600                                ;
}
float t_e_q2en_600                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1920618E-04;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.15079514E-04,-0.20483676E-02, 0.23632098E-01, 0.22737496E-02,
         0.10963755E-03,-0.30694978E-03,-0.68949326E-03,-0.76643040E-03,
         0.26831606E-05,-0.30744847E-03,-0.30735822E-03,-0.12787853E-03,
        -0.10859402E-02,-0.83319697E-03,-0.32965567E-04,-0.69841486E-03,
        -0.42559495E-04,-0.31229276E-04, 0.80036858E-04, 0.75240954E-04,
        -0.79816173E-05,-0.34044750E-03,-0.76119375E-03, 0.16583475E-04,
         0.91684178E-05,-0.13084934E-04,-0.56984945E-05,-0.22043740E-04,
        -0.43566659E-04, 0.59660942E-04, 0.24788345E-04,-0.15657504E-05,
        -0.65597527E-06, 0.17046585E-05, 0.34110478E-05, 0.27551509E-05,
        -0.41686703E-05, 0.11697024E-05, 0.29588116E-05,-0.25142588E-05,
        -0.27330927E-05, 0.28900206E-05, 0.87864910E-05,-0.92340797E-05,
        -0.12821783E-04,-0.73570277E-05,-0.18169927E-04,-0.14254092E-04,
         0.18405952E-04, 0.17725357E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_600                                =v_t_e_q2en_600                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_600                                =v_t_e_q2en_600                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x11*x22*x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x21*x31        
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2en_600                                =v_t_e_q2en_600                                
        +coeff[ 26]*x11            *x52
        +coeff[ 27]    *x21*x33        
        +coeff[ 28]*x11*x22*x31*x42    
        +coeff[ 29]    *x21*x31*x43*x51
        +coeff[ 30]    *x21*x32    *x53
        +coeff[ 31]    *x22            
        +coeff[ 32]*x11    *x31        
        +coeff[ 33]            *x42    
        +coeff[ 34]*x13                
    ;
    v_t_e_q2en_600                                =v_t_e_q2en_600                                
        +coeff[ 35]*x12    *x31        
        +coeff[ 36]*x11*x21*x31        
        +coeff[ 37]    *x22    *x41    
        +coeff[ 38]    *x22        *x51
        +coeff[ 39]*x11        *x41*x51
        +coeff[ 40]    *x21    *x41*x51
        +coeff[ 41]            *x41*x52
        +coeff[ 42]*x11*x22*x31        
        +coeff[ 43]*x11*x21*x32        
    ;
    v_t_e_q2en_600                                =v_t_e_q2en_600                                
        +coeff[ 44]*x11*x21*x31*x41    
        +coeff[ 45]*x11    *x32*x41    
        +coeff[ 46]    *x21*x32*x41    
        +coeff[ 47]    *x21*x31*x42    
        +coeff[ 48]    *x23        *x51
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_q2en_600                                ;
}
float y_e_q2en_600                                (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.8621293E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.95448946E-03, 0.21212485E+00, 0.12481564E+00,-0.29237734E-02,
        -0.28520059E-02, 0.14364046E-03,-0.41744296E-03,-0.27390619E-03,
         0.10939338E-03,-0.16167291E-04, 0.11792186E-03, 0.10880957E-03,
         0.33313212E-04,-0.42368803E-03,-0.98250195E-04, 0.42969430E-04,
        -0.16468013E-04,-0.52710879E-04, 0.31029580E-04, 0.93526876E-04,
        -0.28860415E-03,-0.11787186E-03, 0.78013491E-05, 0.60539187E-04,
         0.22224370E-03,-0.19450160E-03,-0.25602669E-04,-0.92106769E-04,
        -0.84138192E-04,-0.19853167E-04,-0.10193734E-03,-0.14797646E-03,
         0.88927452E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q2en_600                                =v_y_e_q2en_600                                
        +coeff[  8]            *x43    
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]    *x24*x32*x41    
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q2en_600                                =v_y_e_q2en_600                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]        *x31*x41    
        +coeff[ 23]        *x31*x42*x51
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_q2en_600                                =v_y_e_q2en_600                                
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]    *x22*x33        
        +coeff[ 28]*x11*x23    *x41    
        +coeff[ 29]    *x21    *x42*x52
        +coeff[ 30]        *x31*x44*x51
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x24    *x41*x51
        ;

    return v_y_e_q2en_600                                ;
}
float p_e_q2en_600                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1179511E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.12899919E-03, 0.75036823E-02, 0.25789859E-01, 0.67555672E-03,
         0.15778640E-03,-0.27997154E-03,-0.23356457E-03,-0.29260383E-03,
        -0.17788973E-03,-0.33396624E-04,-0.16342662E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_600                                =v_p_e_q2en_600                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_600                                ;
}
float l_e_q2en_600                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2405290E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24431322E-02,-0.34728246E-02,-0.81862987E-03,-0.44251918E-02,
        -0.55157207E-02,-0.37148039E-03,-0.10993344E-02, 0.82513653E-02,
        -0.20983625E-03,-0.20698614E-03, 0.10603454E-03, 0.27434362E-03,
         0.33916655E-04,-0.47476774E-05, 0.30940055E-03, 0.34438600E-03,
        -0.10046249E-02, 0.54721115E-03,-0.47166224E-03, 0.68292755E-03,
        -0.60095277E-03, 0.13397471E-03,-0.10619112E-02,-0.22367049E-03,
         0.69406454E-03,-0.18724364E-02,-0.49066852E-03, 0.10657167E-02,
        -0.41499824E-03,-0.45715974E-03, 0.24518790E-03,-0.64007816E-03,
        -0.60113659E-03, 0.18390549E-02, 0.12794845E-02, 0.59484394E-03,
        -0.16266684E-03,-0.12535278E-02,-0.15904244E-03, 0.59651176E-03,
         0.24663634E-03, 0.16863750E-02, 0.51336637E-03,-0.10722419E-02,
         0.15309544E-02,-0.11424406E-02, 0.17389759E-02,-0.10525918E-02,
        -0.33867499E-03, 0.11405918E-02, 0.68112183E-03,-0.38093099E-03,
         0.15367501E-02, 0.58120780E-03, 0.60030230E-03, 0.33095962E-03,
        -0.12656966E-02,-0.47230133E-03,-0.22233312E-02, 0.23484263E-02,
        -0.24564080E-02, 0.14470385E-02, 0.12498023E-02,-0.19261128E-02,
         0.61702495E-03,-0.10615211E-02, 0.74210242E-04, 0.61297510E-03,
         0.18095183E-02,-0.48308112E-02, 0.73006342E-03,-0.14225000E-02,
        -0.68769272E-03, 0.15815472E-02, 0.66627486E-03, 0.11555612E-02,
         0.15505604E-03, 0.13154103E-03,-0.52682219E-04, 0.24356300E-03,
        -0.26573977E-03,-0.10318469E-03,-0.18771042E-03,-0.94534786E-04,
        -0.13019942E-03,-0.13834618E-03,-0.10319300E-02,-0.45256344E-04,
        -0.25488259E-03,-0.12087030E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_600                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x33        
        +coeff[  6]    *x22*x32        
        +coeff[  7]    *x22*x32*x42    
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[  8]    *x21        *x51
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]                *x53
        +coeff[ 13]*x11        *x41*x51
        +coeff[ 14]    *x21*x33        
        +coeff[ 15]    *x21*x32*x41    
        +coeff[ 16]        *x32*x42    
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 17]    *x22        *x52
        +coeff[ 18]    *x21    *x41*x52
        +coeff[ 19]    *x21        *x53
        +coeff[ 20]*x11    *x32*x41    
        +coeff[ 21]*x11        *x43    
        +coeff[ 22]*x11*x21    *x41*x51
        +coeff[ 23]*x12*x21    *x41    
        +coeff[ 24]    *x24    *x41    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 26]    *x22    *x42*x51
        +coeff[ 27]        *x31*x43*x51
        +coeff[ 28]    *x23        *x52
        +coeff[ 29]        *x31*x41*x53
        +coeff[ 30]            *x42*x53
        +coeff[ 31]*x11*x22*x32        
        +coeff[ 32]*x11*x22*x31*x41    
        +coeff[ 33]*x11    *x31*x43    
        +coeff[ 34]*x11    *x31*x42*x51
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 35]*x11*x22        *x52
        +coeff[ 36]*x11    *x31    *x53
        +coeff[ 37]*x12*x21*x31*x41    
        +coeff[ 38]*x12    *x31*x41*x51
        +coeff[ 39]*x12        *x42*x51
        +coeff[ 40]*x13    *x31*x41    
        +coeff[ 41]    *x24*x31*x41    
        +coeff[ 42]*x13        *x41*x51
        +coeff[ 43]    *x23    *x41*x52
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 44]    *x21*x31*x41*x53
        +coeff[ 45]    *x22        *x54
        +coeff[ 46]    *x21    *x41*x54
        +coeff[ 47]*x11*x22*x32    *x51
        +coeff[ 48]*x11*x21*x31*x42*x51
        +coeff[ 49]*x11*x21    *x43*x51
        +coeff[ 50]*x11*x22        *x53
        +coeff[ 51]*x12*x21        *x53
        +coeff[ 52]    *x24*x32*x41    
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 53]    *x24*x32    *x51
        +coeff[ 54]    *x23*x32    *x52
        +coeff[ 55]*x13        *x41*x52
        +coeff[ 56]    *x21*x32    *x54
        +coeff[ 57]        *x31*x42*x54
        +coeff[ 58]*x11*x23*x31*x42    
        +coeff[ 59]*x11*x21*x33*x42    
        +coeff[ 60]*x11*x23*x31*x41*x51
        +coeff[ 61]*x11*x21*x33*x41*x51
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 62]*x11*x21*x31*x43*x51
        +coeff[ 63]*x11*x21*x31*x42*x52
        +coeff[ 64]*x11*x21*x32    *x53
        +coeff[ 65]*x12*x23*x32        
        +coeff[ 66]*x12*x22*x31    *x52
        +coeff[ 67]*x12    *x31    *x54
        +coeff[ 68]    *x24*x34        
        +coeff[ 69]    *x22*x34*x42    
        +coeff[ 70]*x13        *x44    
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 71]    *x24    *x44    
        +coeff[ 72]    *x23*x33*x41*x51
        +coeff[ 73]    *x21*x34*x42*x51
        +coeff[ 74]*x13*x21*x31    *x52
        +coeff[ 75]    *x24    *x42*x52
        +coeff[ 76]        *x31        
        +coeff[ 77]    *x21    *x41    
        +coeff[ 78]*x11            *x51
        +coeff[ 79]    *x21*x32        
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 80]    *x22    *x41    
        +coeff[ 81]    *x21    *x42    
        +coeff[ 82]        *x31*x42    
        +coeff[ 83]        *x32    *x51
        +coeff[ 84]        *x31    *x52
        +coeff[ 85]*x11*x22            
        +coeff[ 86]*x11    *x31*x41    
        +coeff[ 87]*x11        *x42    
        +coeff[ 88]*x11    *x31    *x51
    ;
    v_l_e_q2en_600                                =v_l_e_q2en_600                                
        +coeff[ 89]*x11            *x52
        ;

    return v_l_e_q2en_600                                ;
}
float x_e_q2en_500                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1352809E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.13475670E-02, 0.13075155E+00, 0.58877617E-02,-0.11887284E-02,
        -0.91935170E-03,-0.23972169E-02,-0.18821743E-02, 0.11890679E-05,
         0.20610815E-03,-0.83529740E-03,-0.23497261E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_500                                =v_x_e_q2en_500                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_500                                ;
}
float t_e_q2en_500                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2274169E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.22639288E-03,-0.20458214E-02, 0.23678737E-01, 0.22761929E-02,
         0.10700348E-03,-0.29023434E-03,-0.68455742E-03,-0.74269471E-03,
         0.52194464E-05,-0.33106681E-03,-0.28656871E-03,-0.12670744E-03,
        -0.11050657E-02,-0.86875312E-03,-0.30433646E-04,-0.72168285E-03,
        -0.32586238E-04,-0.23484847E-04, 0.11524510E-03, 0.82605438E-04,
        -0.33532840E-03,-0.81778481E-03, 0.44118387E-04,-0.16116770E-04,
        -0.60680431E-05, 0.48964353E-05,-0.65311860E-05, 0.49096648E-05,
        -0.92586415E-05, 0.11428743E-04, 0.24622419E-04,-0.60214221E-04,
        -0.51005387E-04, 0.84751600E-05, 0.12382139E-04, 0.96443664E-05,
         0.17420414E-04, 0.30298512E-04,-0.17947754E-04, 0.22133205E-04,
        -0.96346812E-05, 0.30484073E-05,-0.23683399E-05,-0.29616376E-05,
        -0.78026342E-05,-0.92038017E-05,-0.25547597E-05, 0.13377733E-04,
         0.50967824E-05, 0.20895381E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_500                                =v_t_e_q2en_500                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_500                                =v_t_e_q2en_500                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x22    *x41    
        +coeff[ 25]    *x22        *x51
    ;
    v_t_e_q2en_500                                =v_t_e_q2en_500                                
        +coeff[ 26]*x11            *x52
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]*x11*x22        *x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11*x22    *x42    
        +coeff[ 33]*x11*x23        *x51
        +coeff[ 34]*x12*x21*x33        
    ;
    v_t_e_q2en_500                                =v_t_e_q2en_500                                
        +coeff[ 35]*x11*x22*x33        
        +coeff[ 36]*x12*x23        *x51
        +coeff[ 37]*x12*x22    *x41*x51
        +coeff[ 38]*x11*x23    *x41*x51
        +coeff[ 39]*x12    *x31*x42*x51
        +coeff[ 40]*x11*x21*x31        
        +coeff[ 41]        *x32*x41    
        +coeff[ 42]        *x32    *x51
        +coeff[ 43]    *x22*x32        
    ;
    v_t_e_q2en_500                                =v_t_e_q2en_500                                
        +coeff[ 44]*x12*x21    *x41    
        +coeff[ 45]    *x23    *x41    
        +coeff[ 46]*x11        *x43    
        +coeff[ 47]    *x21    *x43    
        +coeff[ 48]*x11    *x32    *x51
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_q2en_500                                ;
}
float y_e_q2en_500                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.4937531E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.44499044E-03, 0.21213904E+00, 0.12474395E+00,-0.29369157E-02,
        -0.28479968E-02,-0.31443863E-03,-0.25327879E-03, 0.13521478E-03,
        -0.20308018E-04, 0.12143551E-03, 0.11338462E-03,-0.13682027E-05,
         0.53255037E-04,-0.43664745E-03, 0.11379943E-03,-0.20517526E-04,
        -0.35695000E-04, 0.10733840E-03, 0.91902177E-04,-0.34484779E-03,
        -0.34785425E-03,-0.29296498E-03, 0.21902553E-04, 0.57601454E-04,
         0.10239310E-04,-0.28859407E-04,-0.18375187E-04, 0.64428561E-04,
         0.36247362E-04,-0.79123354E-04,-0.11032897E-03,-0.11435070E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_500                                =v_y_e_q2en_500                                
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_500                                =v_y_e_q2en_500                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]    *x22*x32*x41    
        +coeff[ 21]    *x22*x33*x42    
        +coeff[ 22]            *x42    
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]            *x42*x51
        +coeff[ 25]            *x44    
    ;
    v_y_e_q2en_500                                =v_y_e_q2en_500                                
        +coeff[ 26]        *x32*x42    
        +coeff[ 27]        *x33*x42    
        +coeff[ 28]*x11*x21*x31*x42    
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x11*x23    *x41    
        +coeff[ 31]*x11*x23*x31        
        ;

    return v_y_e_q2en_500                                ;
}
float p_e_q2en_500                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.8145932E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.75167700E-04, 0.74721598E-02, 0.25751397E-01, 0.67956018E-03,
         0.16043849E-03,-0.28070118E-03,-0.23688481E-03,-0.29201794E-03,
        -0.17626157E-03,-0.37148253E-04,-0.16075205E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_500                                =v_p_e_q2en_500                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_500                                ;
}
float l_e_q2en_500                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2410123E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.23525760E-02,-0.35106800E-02,-0.10077066E-02,-0.42301300E-02,
        -0.52243941E-02,-0.12205549E-03, 0.12344627E-02,-0.84885280E-04,
        -0.15007648E-03, 0.10127575E-03, 0.11117452E-04,-0.15818141E-04,
         0.36300754E-03, 0.20463608E-03,-0.23772362E-03, 0.77663432E-03,
        -0.39462204E-03, 0.30938137E-03, 0.20233463E-03,-0.20814190E-04,
         0.17621266E-03, 0.12168124E-02,-0.85994485E-03, 0.81415143E-03,
        -0.18528686E-02, 0.50511671E-03,-0.92254445E-03, 0.61442278E-03,
         0.15802197E-02, 0.22319137E-03, 0.16548128E-02, 0.15481489E-02,
         0.36841590E-03,-0.97270584E-03, 0.40315615E-03,-0.12094409E-02,
         0.93665934E-03,-0.18543260E-02,-0.89249347E-03, 0.71415241E-03,
        -0.96638495E-03, 0.46022405E-03,-0.51734503E-03,-0.79231989E-03,
        -0.59706502E-03, 0.27835733E-03,-0.60214312E-03, 0.15803949E-02,
        -0.69852118E-04, 0.36951373E-02, 0.31840671E-02, 0.27667743E-02,
        -0.14295410E-02, 0.25945451E-03, 0.75937749E-03,-0.65277849E-03,
        -0.52415025E-02,-0.21085623E-02,-0.15327435E-02, 0.12042308E-02,
         0.12190831E-02,-0.32544148E-02,-0.33671877E-02, 0.21281107E-02,
         0.15106390E-02, 0.29521144E-02,-0.15958793E-02, 0.56168152E-03,
        -0.54260914E-03,-0.19637670E-02,-0.61494636E-03,-0.20896143E-02,
        -0.78041415E-03,-0.41846139E-02, 0.27564007E-02, 0.16961751E-02,
         0.69963356E-03,-0.10114207E-04, 0.10271442E-03,-0.98461882E-04,
         0.59064103E-04, 0.51406925E-04,-0.17278026E-03,-0.25381974E-03,
         0.95349795E-04,-0.25713310E-03,-0.28850575E-03,-0.18180959E-03,
        -0.15006123E-03, 0.24730462E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_500                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x33*x42    
        +coeff[  6]*x11*x21*x32    *x51
        +coeff[  7]    *x21            
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[  8]            *x41    
        +coeff[  9]    *x21    *x41    
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]            *x43    
        +coeff[ 13]                *x53
        +coeff[ 14]*x12        *x41    
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]    *x23    *x41    
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]            *x43*x51
        +coeff[ 19]                *x54
        +coeff[ 20]*x11*x21*x32        
        +coeff[ 21]*x11*x22        *x51
        +coeff[ 22]    *x24        *x51
        +coeff[ 23]        *x32*x41*x52
        +coeff[ 24]    *x22        *x53
        +coeff[ 25]*x11*x21*x33        
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 26]*x11*x22    *x41*x51
        +coeff[ 27]*x11        *x41*x53
        +coeff[ 28]*x12    *x31*x41*x51
        +coeff[ 29]*x12        *x42*x51
        +coeff[ 30]    *x22*x33*x41    
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]    *x23        *x53
        +coeff[ 33]    *x21*x31*x41*x53
        +coeff[ 34]        *x32*x41*x53
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 35]            *x43*x53
        +coeff[ 36]*x11*x24*x31        
        +coeff[ 37]*x11*x22*x32    *x51
        +coeff[ 38]*x11*x21*x33    *x51
        +coeff[ 39]*x11    *x34    *x51
        +coeff[ 40]*x11*x21    *x42*x52
        +coeff[ 41]*x11*x21*x31    *x53
        +coeff[ 42]*x12    *x32    *x52
        +coeff[ 43]*x13*x22*x31        
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 44]*x13*x21*x31*x41    
        +coeff[ 45]    *x23*x33*x41    
        +coeff[ 46]*x13*x22        *x51
        +coeff[ 47]    *x22*x34    *x51
        +coeff[ 48]    *x21*x32*x43*x51
        +coeff[ 49]    *x21*x33*x41*x52
        +coeff[ 50]    *x21*x32*x42*x52
        +coeff[ 51]    *x24        *x53
        +coeff[ 52]    *x22*x32    *x53
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 53]    *x23    *x41*x53
        +coeff[ 54]    *x21*x31*x42*x53
        +coeff[ 55]*x11    *x34*x41*x51
        +coeff[ 56]*x11*x21*x32*x42*x51
        +coeff[ 57]*x11*x21*x31*x43*x51
        +coeff[ 58]*x11*x21*x31*x41*x53
        +coeff[ 59]*x12*x22*x33        
        +coeff[ 60]*x12*x24    *x41    
        +coeff[ 61]*x12*x21*x33*x41    
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 62]*x12*x21*x32*x42    
        +coeff[ 63]*x12*x23    *x41*x51
        +coeff[ 64]*x12*x22    *x42*x51
        +coeff[ 65]*x12*x21*x31*x42*x51
        +coeff[ 66]*x12    *x32*x42*x51
        +coeff[ 67]*x12*x23        *x52
        +coeff[ 68]*x12*x22    *x41*x52
        +coeff[ 69]*x12    *x31*x41*x53
        +coeff[ 70]    *x23*x33*x42    
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 71]        *x34*x43*x51
        +coeff[ 72]*x13*x21*x31    *x52
        +coeff[ 73]    *x22*x33*x41*x52
        +coeff[ 74]        *x32*x43*x53
        +coeff[ 75]    *x22*x31*x41*x54
        +coeff[ 76]        *x33*x41*x54
        +coeff[ 77]        *x31        
        +coeff[ 78]                *x51
        +coeff[ 79]*x11                
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 80]    *x21*x31        
        +coeff[ 81]        *x31    *x51
        +coeff[ 82]*x11            *x51
        +coeff[ 83]    *x22*x31        
        +coeff[ 84]    *x21*x32        
        +coeff[ 85]    *x22    *x41    
        +coeff[ 86]    *x21*x31    *x51
        +coeff[ 87]    *x21    *x41*x51
        +coeff[ 88]            *x41*x52
    ;
    v_l_e_q2en_500                                =v_l_e_q2en_500                                
        +coeff[ 89]*x11    *x32        
        ;

    return v_l_e_q2en_500                                ;
}
float x_e_q2en_450                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4950569E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.50435466E-03, 0.13078243E+00, 0.58853775E-02,-0.11864393E-02,
        -0.91245543E-03,-0.23935314E-02,-0.18761355E-02,-0.69910116E-05,
         0.20703544E-03,-0.83310454E-03,-0.23396207E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_450                                =v_x_e_q2en_450                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_450                                ;
}
float t_e_q2en_450                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.8108619E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.82167666E-04,-0.20464237E-02, 0.23716738E-01, 0.22838123E-02,
         0.10926677E-03,-0.29325896E-03,-0.71085163E-03,-0.75506978E-03,
        -0.33689690E-06,-0.35225344E-03,-0.27713747E-03,-0.12622950E-03,
        -0.10726292E-02,-0.82137255E-03,-0.33436361E-04,-0.67612936E-03,
        -0.16962324E-04,-0.21218664E-04, 0.11059143E-03, 0.82718310E-04,
        -0.32886129E-03,-0.79664635E-03,-0.71053719E-05, 0.48548482E-04,
        -0.14231302E-04,-0.53138801E-05, 0.17408076E-04, 0.85110651E-05,
        -0.14288058E-04,-0.16142467E-04, 0.84501216E-05,-0.56359833E-04,
        -0.19538544E-04,-0.35441390E-04, 0.10203231E-04, 0.24347824E-04,
         0.88356892E-05, 0.41207709E-05,-0.63851456E-06, 0.71991633E-06,
         0.17964405E-05,-0.64591263E-05, 0.26854050E-05, 0.25014645E-05,
         0.27713220E-05,-0.39545657E-05,-0.52806236E-05,-0.46590640E-05,
         0.70572146E-05, 0.61175742E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_450                                =v_t_e_q2en_450                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_450                                =v_t_e_q2en_450                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x11    *x32    *x52
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]            *x41*x52
    ;
    v_t_e_q2en_450                                =v_t_e_q2en_450                                
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]*x11*x22        *x51
        +coeff[ 28]*x11*x21    *x41*x51
        +coeff[ 29]*x11*x21        *x52
        +coeff[ 30]*x11*x21*x33        
        +coeff[ 31]*x11*x22*x31*x41    
        +coeff[ 32]*x11    *x33*x41    
        +coeff[ 33]*x11*x22    *x42    
        +coeff[ 34]    *x22    *x42*x51
    ;
    v_t_e_q2en_450                                =v_t_e_q2en_450                                
        +coeff[ 35]*x11*x21*x31*x41*x52
        +coeff[ 36]*x11*x21            
        +coeff[ 37]    *x22            
        +coeff[ 38]*x11    *x31        
        +coeff[ 39]        *x32        
        +coeff[ 40]    *x21    *x41    
        +coeff[ 41]*x12*x21            
        +coeff[ 42]*x12        *x41    
        +coeff[ 43]    *x21*x31    *x51
    ;
    v_t_e_q2en_450                                =v_t_e_q2en_450                                
        +coeff[ 44]*x11        *x41*x51
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x13*x21            
        +coeff[ 47]*x12*x22            
        +coeff[ 48]*x11*x22*x31        
        +coeff[ 49]    *x22*x32        
        ;

    return v_t_e_q2en_450                                ;
}
float y_e_q2en_450                                (float *x,int m){
    int ncoeff= 34;
    float avdat= -0.8002702E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 35]={
         0.77567488E-03, 0.21214050E+00, 0.12471303E+00,-0.29340365E-02,
        -0.28401159E-02,-0.36378016E-03,-0.28732320E-03, 0.14393448E-03,
        -0.70210255E-04, 0.11230982E-03, 0.11430489E-03, 0.38612880E-05,
        -0.29434957E-04,-0.40527264E-03, 0.92326511E-04,-0.26002224E-04,
        -0.20787664E-04, 0.83772968E-04, 0.75579053E-04,-0.36807719E-03,
         0.10406051E-04, 0.76127099E-05, 0.13769634E-04,-0.42190782E-05,
         0.78041987E-04,-0.20961457E-04,-0.12287626E-03, 0.19623325E-04,
        -0.84357329E-04,-0.92220544E-04,-0.38128281E-04,-0.66687753E-04,
         0.40293042E-04,-0.55979439E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_450                                =v_y_e_q2en_450                                
        +coeff[  8]*x11*x21*x31        
        +coeff[  9]            *x41*x52
        +coeff[ 10]        *x31    *x52
        +coeff[ 11]            *x45    
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]            *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_450                                =v_y_e_q2en_450                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x24    *x41    
        +coeff[ 20]*x11        *x42    
        +coeff[ 21]            *x44    
        +coeff[ 22]    *x21    *x44    
        +coeff[ 23]*x11        *x42*x51
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]*x11*x21    *x41*x51
    ;
    v_y_e_q2en_450                                =v_y_e_q2en_450                                
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]*x11        *x41*x52
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x12    *x31*x42    
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]*x11*x21*x33        
        +coeff[ 33]*x11    *x31*x43*x51
        ;

    return v_y_e_q2en_450                                ;
}
float p_e_q2en_450                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.7964957E-04;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.76550059E-04, 0.74493159E-02, 0.25720481E-01, 0.67954633E-03,
         0.16082381E-03,-0.27917547E-03,-0.23313152E-03,-0.29056249E-03,
        -0.17603257E-03,-0.30174200E-04,-0.16514912E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_450                                =v_p_e_q2en_450                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_450                                ;
}
float l_e_q2en_450                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2443467E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.22985074E-02,-0.31676746E-02,-0.52878977E-03,-0.44105011E-02,
        -0.52826754E-02, 0.19982831E-03,-0.44719459E-03, 0.10265929E-02,
         0.79078600E-03,-0.14386197E-02, 0.10276601E-02, 0.15372093E-03,
         0.68816189E-04,-0.58880894E-04, 0.30305699E-03,-0.51850624E-04,
         0.23802041E-03, 0.47727849E-03,-0.26377500E-03, 0.22125525E-03,
        -0.37845489E-03,-0.51410822E-03,-0.16575449E-03,-0.80527418E-03,
        -0.71459636E-03,-0.96043549E-03, 0.69931062E-04,-0.10444733E-02,
        -0.10241377E-02, 0.12205676E-02, 0.94789080E-03, 0.16643307E-04,
         0.10409892E-02,-0.30191572E-04, 0.67948544E-03,-0.10726926E-02,
         0.94524742E-03, 0.57371106E-03, 0.11380123E-02, 0.12969383E-03,
        -0.53185725E-03, 0.81575761E-03, 0.42805923E-02, 0.12099231E-02,
         0.12219536E-03, 0.29455323E-02, 0.31257903E-02, 0.10452325E-02,
        -0.77956333E-03, 0.10782977E-02, 0.60322753E-03,-0.17883851E-02,
         0.16614683E-02, 0.87259972E-03, 0.15560731E-02,-0.30308345E-02,
        -0.38560124E-02,-0.95399923E-03,-0.44545851E-03, 0.11220369E-02,
         0.29774869E-02,-0.29262058E-02, 0.35143874E-03, 0.67091687E-03,
        -0.14706850E-02,-0.79960085E-03,-0.16287880E-02,-0.23577039E-02,
        -0.11121691E-04,-0.72062037E-04,-0.11470672E-03,-0.73470648E-04,
         0.80402038E-04,-0.52317719E-04, 0.11194949E-02,-0.23957781E-03,
        -0.11807396E-03,-0.27427750E-03, 0.19672816E-03,-0.14778371E-03,
         0.17386805E-04,-0.85089887E-04, 0.11504869E-03,-0.13192385E-03,
         0.13900659E-03, 0.36257150E-03, 0.19398081E-03, 0.38724137E-03,
         0.57940715E-03, 0.20487110E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_450                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]            *x42*x51
        +coeff[  6]*x11            *x52
        +coeff[  7]*x11    *x31*x42*x51
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[  8]*x13    *x31*x42    
        +coeff[  9]    *x22*x34    *x51
        +coeff[ 10]*x13*x21    *x42*x51
        +coeff[ 11]                *x51
        +coeff[ 12]*x11                
        +coeff[ 13]    *x21    *x41    
        +coeff[ 14]            *x41*x51
        +coeff[ 15]*x12                
        +coeff[ 16]    *x23            
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 17]    *x21*x31*x41    
        +coeff[ 18]            *x43    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]*x12            *x51
        +coeff[ 21]    *x22*x32        
        +coeff[ 22]        *x34        
        +coeff[ 23]        *x32*x42    
        +coeff[ 24]    *x22*x31    *x51
        +coeff[ 25]    *x21    *x42*x51
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 26]*x11*x21*x32        
        +coeff[ 27]*x12        *x41*x51
        +coeff[ 28]    *x24    *x41    
        +coeff[ 29]    *x21*x32*x41*x51
        +coeff[ 30]    *x21*x31*x42*x51
        +coeff[ 31]    *x21*x32    *x52
        +coeff[ 32]*x11    *x31*x41*x52
        +coeff[ 33]*x12    *x33        
        +coeff[ 34]*x12    *x32    *x51
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 35]*x13    *x31    *x51
        +coeff[ 36]    *x22*x33    *x51
        +coeff[ 37]    *x23    *x42*x51
        +coeff[ 38]    *x21    *x44*x51
        +coeff[ 39]*x13            *x52
        +coeff[ 40]    *x24        *x52
        +coeff[ 41]    *x23    *x41*x52
        +coeff[ 42]        *x32*x42*x52
        +coeff[ 43]        *x31*x43*x52
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 44]    *x22        *x54
        +coeff[ 45]*x11*x21*x32*x42    
        +coeff[ 46]*x11*x21*x31*x43    
        +coeff[ 47]*x11*x21    *x44    
        +coeff[ 48]*x12*x22    *x42    
        +coeff[ 49]*x12        *x41*x53
        +coeff[ 50]*x13*x21    *x41*x51
        +coeff[ 51]    *x24*x31*x41*x51
        +coeff[ 52]    *x21*x32*x42*x52
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 53]    *x22*x31*x41*x53
        +coeff[ 54]*x11    *x34*x42    
        +coeff[ 55]*x11    *x33*x41*x52
        +coeff[ 56]*x11    *x32*x42*x52
        +coeff[ 57]*x12*x21*x32*x42    
        +coeff[ 58]*x13    *x34        
        +coeff[ 59]*x13    *x32    *x52
        +coeff[ 60]    *x24    *x42*x52
        +coeff[ 61]        *x34*x42*x52
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 62]*x13    *x31    *x53
        +coeff[ 63]    *x24*x31    *x53
        +coeff[ 64]*x13        *x41*x53
        +coeff[ 65]    *x22    *x43*x53
        +coeff[ 66]    *x23    *x41*x54
        +coeff[ 67]    *x22    *x42*x54
        +coeff[ 68]    *x21            
        +coeff[ 69]                *x52
        +coeff[ 70]*x11*x21            
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 71]*x11    *x31        
        +coeff[ 72]    *x21*x32        
        +coeff[ 73]        *x33        
        +coeff[ 74]    *x22    *x41    
        +coeff[ 75]        *x32*x41    
        +coeff[ 76]    *x21    *x42    
        +coeff[ 77]        *x31*x42    
        +coeff[ 78]        *x31*x41*x51
        +coeff[ 79]    *x21        *x52
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 80]        *x31    *x52
        +coeff[ 81]            *x41*x52
        +coeff[ 82]*x11*x22            
        +coeff[ 83]*x11    *x32        
        +coeff[ 84]*x11*x21    *x41    
        +coeff[ 85]*x11    *x31*x41    
        +coeff[ 86]*x11        *x42    
        +coeff[ 87]*x11    *x31    *x51
        +coeff[ 88]*x11        *x41*x51
    ;
    v_l_e_q2en_450                                =v_l_e_q2en_450                                
        +coeff[ 89]*x12    *x31        
        ;

    return v_l_e_q2en_450                                ;
}
float x_e_q2en_449                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1163018E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.11487829E-02, 0.13198736E+00, 0.58409278E-02,-0.11472384E-02,
        -0.26158355E-02,-0.18643128E-02, 0.12266330E-03, 0.20213528E-03,
        -0.95023273E-03,-0.10826704E-02,-0.23257729E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_q2en_449                                =v_x_e_q2en_449                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_449                                ;
}
float t_e_q2en_449                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2312798E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.22836514E-03,-0.20268192E-02, 0.24215534E-01, 0.22303057E-02,
         0.10681088E-03,-0.29232935E-03,-0.76309161E-03,-0.73707977E-03,
         0.19819576E-04,-0.43427275E-03,-0.35601604E-03,-0.12085111E-03,
        -0.12357502E-02,-0.86383219E-03,-0.24331057E-04,-0.74215583E-03,
        -0.26764845E-04,-0.16670241E-04, 0.35879515E-04, 0.12025083E-03,
         0.71544186E-04,-0.30574276E-05,-0.42448018E-03,-0.93338225E-03,
         0.34448806E-05, 0.25880477E-05,-0.94626403E-05,-0.32762759E-06,
        -0.10661558E-04, 0.46403522E-04,-0.93085153E-04,-0.62559273E-04,
         0.15369253E-04,-0.10690080E-04,-0.19156500E-05,-0.78794546E-05,
        -0.14757501E-05, 0.29776959E-05,-0.39782412E-05, 0.59636991E-05,
         0.25503716E-05, 0.51113134E-05, 0.20127376E-04, 0.88197430E-05,
        -0.56247241E-05,-0.44964900E-05, 0.74828272E-05,-0.36229467E-04,
        -0.95846644E-05,-0.52639962E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_449                                =v_t_e_q2en_449                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_449                                =v_t_e_q2en_449                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x21*x31*x41*x51
        +coeff[ 20]    *x21    *x42*x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]*x12*x21*x32    *x51
        +coeff[ 25]    *x23*x32    *x51
    ;
    v_t_e_q2en_449                                =v_t_e_q2en_449                                
        +coeff[ 26]*x12*x21            
        +coeff[ 27]    *x22*x31        
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x21*x32    *x51
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x12*x22        *x51
        +coeff[ 33]    *x23*x31    *x51
        +coeff[ 34]                *x51
    ;
    v_t_e_q2en_449                                =v_t_e_q2en_449                                
        +coeff[ 35]    *x21*x31        
        +coeff[ 36]            *x43    
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]*x11            *x52
        +coeff[ 39]*x12*x21*x31        
        +coeff[ 40]*x13        *x41    
        +coeff[ 41]*x11*x21*x31*x41    
        +coeff[ 42]    *x21*x31*x42    
        +coeff[ 43]    *x21    *x43    
    ;
    v_t_e_q2en_449                                =v_t_e_q2en_449                                
        +coeff[ 44]*x11*x21*x31    *x51
        +coeff[ 45]    *x22*x31    *x51
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]*x11*x22*x32        
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]*x11*x23    *x41    
        ;

    return v_t_e_q2en_449                                ;
}
float y_e_q2en_449                                (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.9534820E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.96788863E-03, 0.21147224E+00, 0.13791411E+00,-0.29021834E-02,
        -0.31314166E-02,-0.19520591E-03,-0.34055812E-03, 0.21251119E-03,
        -0.30534432E-03, 0.11926331E-03,-0.58468857E-04, 0.10558860E-03,
         0.11989525E-03, 0.11727308E-04,-0.49339421E-03,-0.33350382E-05,
        -0.90194568E-04, 0.91387468E-04,-0.47830946E-03,-0.21535129E-03,
         0.40890813E-04, 0.31908283E-04, 0.78002260E-04,-0.88575534E-05,
        -0.10430747E-04, 0.57126679E-04, 0.14206968E-04,-0.33850482E-03,
        -0.62409352E-04,-0.37757152E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_449                                =v_y_e_q2en_449                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_449                                =v_y_e_q2en_449                                
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22*x33        
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]    *x22*x31*x44    
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]            *x44    
        +coeff[ 24]            *x43*x51
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_q2en_449                                =v_y_e_q2en_449                                
        +coeff[ 26]        *x31*x44    
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x11*x21*x32*x42    
        ;

    return v_y_e_q2en_449                                ;
}
float p_e_q2en_449                                (float *x,int m){
    int ncoeff= 12;
    float avdat= -0.1398410E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
         0.14090267E-03,-0.44128365E-06, 0.82578100E-02, 0.25766492E-01,
         0.66844159E-03, 0.17208590E-03,-0.26852184E-03,-0.24805416E-03,
        -0.28500808E-03,-0.15910088E-03,-0.27445778E-04,-0.17347964E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]        *x31        
        +coeff[  3]            *x41    
        +coeff[  4]            *x41*x51
        +coeff[  5]        *x31    *x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x24*x31        
    ;
    v_p_e_q2en_449                                =v_p_e_q2en_449                                
        +coeff[  8]        *x31*x42    
        +coeff[  9]            *x43    
        +coeff[ 10]    *x22*x32*x41    
        +coeff[ 11]        *x34*x41    
        ;

    return v_p_e_q2en_449                                ;
}
float l_e_q2en_449                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2376458E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24539134E-02,-0.33620016E-02,-0.13357881E-02,-0.48414827E-02,
        -0.54698088E-02,-0.55125775E-03,-0.11757467E-03, 0.10594290E-03,
         0.16233505E-03,-0.13719205E-02, 0.11281738E-04, 0.46368269E-03,
        -0.50166895E-03, 0.25697553E-02,-0.34500429E-03,-0.18185555E-03,
        -0.37295984E-04, 0.22767647E-03, 0.78223116E-03, 0.53755747E-03,
         0.99992054E-03, 0.11721660E-02, 0.10717770E-02, 0.81762375E-03,
        -0.11242364E-02, 0.71618031E-03,-0.11847748E-02,-0.88634546E-03,
         0.56285405E-03, 0.91662689E-04,-0.27667154E-02, 0.18399387E-02,
        -0.15863384E-02, 0.76281471E-03, 0.50138944E-03, 0.20566511E-03,
         0.15917588E-02, 0.11038873E-02,-0.10895198E-02,-0.57823356E-03,
         0.29858737E-03, 0.55590639E-03, 0.10150255E-02,-0.48837299E-03,
        -0.43649078E-03,-0.33467781E-03, 0.51963684E-03, 0.35665943E-02,
         0.10732050E-02,-0.65358827E-03,-0.36182484E-03,-0.39633308E-03,
        -0.40973921E-03,-0.96862047E-03, 0.75501477E-03, 0.10582173E-02,
        -0.20116137E-02, 0.59582852E-03, 0.89921150E-03, 0.14940582E-02,
        -0.32799391E-03,-0.11405685E-02, 0.12052589E-02,-0.11535721E-02,
         0.30780165E-03,-0.37281614E-04,-0.61975770E-04, 0.40181671E-03,
        -0.68273890E-04,-0.81407969E-04,-0.93122995E-04,-0.69351471E-03,
        -0.26849573E-03,-0.12125800E-03, 0.85709755E-04,-0.40320578E-03,
        -0.22695318E-03,-0.12242723E-03, 0.40183781E-03, 0.11499417E-03,
         0.54907100E-03,-0.10403156E-03, 0.67888170E-04, 0.75647869E-04,
        -0.32807075E-03,-0.44385370E-03, 0.28700114E-03,-0.22176185E-03,
         0.65057183E-03, 0.40968493E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_449                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x22        *x51
        +coeff[  6]    *x21*x33        
        +coeff[  7]            *x41    
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[  8]                *x51
        +coeff[  9]    *x22*x31        
        +coeff[ 10]        *x33        
        +coeff[ 11]*x12    *x31        
        +coeff[ 12]    *x22*x31    *x51
        +coeff[ 13]        *x32*x41*x51
        +coeff[ 14]        *x31*x42*x51
        +coeff[ 15]*x11    *x33        
        +coeff[ 16]*x11*x22        *x51
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 17]*x11        *x42*x51
        +coeff[ 18]*x11    *x31    *x52
        +coeff[ 19]*x12    *x31*x41    
        +coeff[ 20]    *x24*x31        
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]        *x34*x41    
        +coeff[ 23]    *x24        *x51
        +coeff[ 24]*x11*x21*x33        
        +coeff[ 25]*x11*x21*x32*x41    
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 26]*x11    *x33    *x51
        +coeff[ 27]*x12    *x33        
        +coeff[ 28]*x12*x21    *x41*x51
        +coeff[ 29]*x13    *x31    *x51
        +coeff[ 30]        *x34*x41*x51
        +coeff[ 31]    *x22*x31*x42*x51
        +coeff[ 32]    *x21*x33    *x52
        +coeff[ 33]    *x21*x31    *x54
        +coeff[ 34]*x11    *x34*x41    
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 35]*x11*x22*x31*x41*x51
        +coeff[ 36]*x11*x22    *x42*x51
        +coeff[ 37]*x11*x21*x31*x42*x51
        +coeff[ 38]*x11        *x44*x51
        +coeff[ 39]*x11*x21*x31    *x53
        +coeff[ 40]*x12*x22*x32        
        +coeff[ 41]*x12    *x34        
        +coeff[ 42]*x12*x23        *x51
        +coeff[ 43]*x12*x21*x32    *x51
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 44]*x12*x21        *x53
        +coeff[ 45]*x12        *x41*x53
        +coeff[ 46]*x13*x21    *x42    
        +coeff[ 47]    *x22*x33*x42    
        +coeff[ 48]    *x21*x34*x42    
        +coeff[ 49]*x13    *x31    *x52
        +coeff[ 50]    *x21*x34    *x52
        +coeff[ 51]    *x21    *x44*x52
        +coeff[ 52]        *x31*x44*x52
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 53]        *x32*x41*x54
        +coeff[ 54]            *x43*x54
        +coeff[ 55]*x11*x23*x33        
        +coeff[ 56]*x11*x22*x31*x42*x51
        +coeff[ 57]*x11    *x31*x43*x52
        +coeff[ 58]*x12*x23*x31*x41    
        +coeff[ 59]*x12    *x33    *x52
        +coeff[ 60]    *x24*x33    *x51
        +coeff[ 61]*x13    *x32*x41*x51
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 62]        *x32*x44*x52
        +coeff[ 63]    *x23*x31    *x54
        +coeff[ 64]        *x31        
        +coeff[ 65]    *x21    *x41    
        +coeff[ 66]    *x21        *x51
        +coeff[ 67]        *x31    *x51
        +coeff[ 68]            *x41*x51
        +coeff[ 69]*x11*x21            
        +coeff[ 70]*x12                
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 71]        *x32*x41    
        +coeff[ 72]        *x31*x42    
        +coeff[ 73]    *x21    *x41*x51
        +coeff[ 74]            *x42*x51
        +coeff[ 75]        *x31    *x52
        +coeff[ 76]            *x41*x52
        +coeff[ 77]                *x53
        +coeff[ 78]*x11*x21*x31        
        +coeff[ 79]*x11    *x32        
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 80]*x11    *x31    *x51
        +coeff[ 81]*x11            *x52
        +coeff[ 82]*x12*x21            
        +coeff[ 83]*x12            *x51
        +coeff[ 84]    *x24            
        +coeff[ 85]        *x33    *x51
        +coeff[ 86]    *x22    *x41*x51
        +coeff[ 87]            *x43*x51
        +coeff[ 88]    *x21*x31    *x52
    ;
    v_l_e_q2en_449                                =v_l_e_q2en_449                                
        +coeff[ 89]        *x31*x41*x52
        ;

    return v_l_e_q2en_449                                ;
}
float x_e_q2en_400                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.2512317E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.26643855E-03,-0.11395609E-02, 0.13208351E+00, 0.58325790E-02,
        -0.26098674E-02,-0.18559928E-02,-0.33887307E-03, 0.13083588E-03,
         0.19888778E-03,-0.83789119E-03,-0.10854546E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23        *x52
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_q2en_400                                =v_x_e_q2en_400                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        ;

    return v_x_e_q2en_400                                ;
}
float t_e_q2en_400                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.6676144E-04;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.68700072E-04,-0.20203961E-02, 0.24322394E-01, 0.22349642E-02,
         0.10672507E-03,-0.29716807E-03,-0.77877706E-03,-0.74242562E-03,
         0.37331620E-05,-0.39602205E-03,-0.37041976E-03,-0.11937812E-03,
        -0.11759258E-02,-0.82775619E-03,-0.36843168E-04, 0.11198369E-03,
        -0.73574233E-03,-0.33218279E-04,-0.17475826E-04, 0.48571645E-04,
        -0.42696073E-03,-0.92831720E-03,-0.12571465E-04,-0.18117880E-04,
        -0.31708707E-05, 0.58525176E-04,-0.61904329E-05,-0.23182663E-04,
         0.16506767E-04, 0.12539102E-04,-0.52694919E-04,-0.35490222E-04,
        -0.17844694E-04,-0.24805955E-04, 0.15018119E-04,-0.14977107E-04,
         0.57144684E-04, 0.52088730E-04, 0.15965143E-04,-0.15843118E-04,
        -0.18294235E-04, 0.18545838E-04, 0.21674472E-04, 0.28927998E-04,
        -0.10564321E-05,-0.23340092E-05,-0.21060216E-05,-0.18060039E-05,
         0.27965871E-05,-0.24671822E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_400                                =v_t_e_q2en_400                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q2en_400                                =v_t_e_q2en_400                                
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11        *x42    
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q2en_400                                =v_t_e_q2en_400                                
        +coeff[ 26]    *x21        *x53
        +coeff[ 27]*x13*x21*x31        
        +coeff[ 28]*x11*x21*x33        
        +coeff[ 29]*x12*x22    *x41    
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]*x11        *x42*x52
        +coeff[ 33]*x13*x22    *x41    
        +coeff[ 34]*x11*x21*x33*x41    
    ;
    v_t_e_q2en_400                                =v_t_e_q2en_400                                
        +coeff[ 35]    *x23    *x43    
        +coeff[ 36]*x12*x21*x31*x41*x51
        +coeff[ 37]    *x23    *x42*x51
        +coeff[ 38]*x13*x21        *x52
        +coeff[ 39]*x12*x21*x31    *x52
        +coeff[ 40]*x11*x22*x31    *x52
        +coeff[ 41]    *x22    *x42*x52
        +coeff[ 42]*x12*x21        *x53
        +coeff[ 43]    *x23        *x53
    ;
    v_t_e_q2en_400                                =v_t_e_q2en_400                                
        +coeff[ 44]            *x41    
        +coeff[ 45]*x11*x21            
        +coeff[ 46]    *x21*x31        
        +coeff[ 47]        *x32        
        +coeff[ 48]*x11        *x41    
        +coeff[ 49]                *x52
        ;

    return v_t_e_q2en_400                                ;
}
float y_e_q2en_400                                (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.1013308E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.10790623E-02, 0.21146959E+00, 0.13780226E+00,-0.28884513E-02,
        -0.31191993E-02,-0.21025525E-03,-0.37928880E-03, 0.20199006E-03,
        -0.27545518E-03, 0.84972169E-04,-0.52097555E-04, 0.11840709E-03,
         0.12282351E-03, 0.14838145E-04,-0.49278687E-03, 0.62895044E-04,
        -0.10384261E-04,-0.10418114E-03, 0.83451072E-04,-0.47570877E-03,
        -0.45592242E-04, 0.10392793E-03, 0.40262476E-04,-0.21885722E-04,
        -0.88072911E-05, 0.12510723E-04,-0.32606382E-04,-0.23397824E-04,
        -0.23994669E-04,-0.47421527E-04, 0.31407602E-04, 0.90763890E-04,
         0.14597248E-04, 0.91026450E-04,-0.34598773E-03,-0.15451527E-04,
        -0.17335145E-03, 0.79264340E-04,-0.67887770E-04, 0.42362070E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_400                                =v_y_e_q2en_400                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x32*x41    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q2en_400                                =v_y_e_q2en_400                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22    *x43*x51
        +coeff[ 21]    *x24    *x41*x51
        +coeff[ 22]    *x22*x31*x44    
        +coeff[ 23]    *x24*x33        
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_y_e_q2en_400                                =v_y_e_q2en_400                                
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]        *x32*x42    
        +coeff[ 28]*x11    *x31*x42    
        +coeff[ 29]        *x31*x42*x51
        +coeff[ 30]            *x45    
        +coeff[ 31]        *x31*x44    
        +coeff[ 32]*x11        *x42*x51
        +coeff[ 33]        *x32*x43    
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_y_e_q2en_400                                =v_y_e_q2en_400                                
        +coeff[ 35]*x11        *x41*x52
        +coeff[ 36]    *x22*x33        
        +coeff[ 37]    *x22    *x44    
        +coeff[ 38]*x11*x23*x31        
        +coeff[ 39]*x11*x21    *x41*x52
        ;

    return v_y_e_q2en_400                                ;
}
float p_e_q2en_400                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1555726E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.16252727E-03, 0.82255071E-02, 0.25745176E-01, 0.67130983E-03,
         0.17246018E-03,-0.26660220E-03,-0.24946107E-03,-0.28762026E-03,
        -0.15757975E-03,-0.31289841E-04,-0.17353053E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_400                                =v_p_e_q2en_400                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_400                                ;
}
float l_e_q2en_400                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2393292E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24869943E-02,-0.37502118E-02,-0.11720326E-02,-0.42474810E-02,
        -0.54576984E-02, 0.48161266E-03,-0.22518795E-03,-0.55818065E-04,
        -0.10110671E-03,-0.16322512E-03, 0.35418410E-03,-0.98323049E-04,
         0.27811807E-03,-0.86363096E-03,-0.35867287E-03, 0.14765422E-03,
         0.26222822E-03,-0.16961375E-03,-0.52911422E-04,-0.18414180E-03,
         0.72760517E-05,-0.15653938E-02,-0.31362474E-03, 0.22113156E-03,
         0.40291890E-03, 0.89735928E-04, 0.19624105E-02, 0.83996827E-03,
        -0.26819966E-03, 0.95845270E-03,-0.76264719E-03, 0.48678057E-03,
         0.18331138E-02, 0.40661712E-03, 0.28376112E-03, 0.99624984E-03,
        -0.15440672E-03,-0.18684505E-03,-0.11615156E-02, 0.59351930E-03,
        -0.90776366E-03, 0.43920550E-03, 0.73629728E-03, 0.11876507E-02,
        -0.86722750E-03,-0.14822377E-02,-0.32117486E-02,-0.14844271E-02,
         0.33800554E-03,-0.51423634E-03,-0.25824655E-03, 0.17600362E-02,
        -0.56479481E-03,-0.29524282E-03,-0.17516420E-02, 0.12743160E-02,
        -0.26904766E-02,-0.37195216E-03, 0.64004265E-03,-0.14175217E-02,
         0.92982873E-03,-0.16775627E-03, 0.72981732E-03,-0.44403237E-03,
         0.27778097E-02,-0.12339873E-02,-0.63966954E-03, 0.18418576E-03,
         0.10758807E-02,-0.12359414E-02,-0.43802077E-03, 0.18061660E-02,
         0.52870726E-02,-0.18567241E-02,-0.15200670E-02,-0.22636638E-02,
         0.10916159E-02, 0.14840912E-03,-0.70507776E-04, 0.62326275E-04,
        -0.19766588E-03, 0.25262142E-03, 0.74084511E-03,-0.13430956E-03,
         0.23573796E-03,-0.38742588E-03, 0.32705069E-03,-0.20798073E-03,
        -0.19435312E-03, 0.14030056E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_400                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31*x43*x51
        +coeff[  6]        *x31        
        +coeff[  7]            *x41    
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[  8]                *x51
        +coeff[  9]*x11                
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]    *x22*x31        
        +coeff[ 13]        *x32*x41    
        +coeff[ 14]    *x21        *x52
        +coeff[ 15]            *x41*x52
        +coeff[ 16]                *x53
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 17]*x11    *x32        
        +coeff[ 18]*x11*x21        *x51
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]*x11            *x52
        +coeff[ 21]        *x31*x43    
        +coeff[ 22]            *x41*x53
        +coeff[ 23]*x11*x22    *x41    
        +coeff[ 24]*x11    *x32    *x51
        +coeff[ 25]*x12        *x41*x51
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 26]        *x34*x41    
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]*x13            *x51
        +coeff[ 29]    *x21    *x42*x52
        +coeff[ 30]*x11*x21*x32    *x51
        +coeff[ 31]*x11*x21    *x42*x51
        +coeff[ 32]*x11*x22        *x52
        +coeff[ 33]*x12*x21*x31*x41    
        +coeff[ 34]*x12    *x32*x41    
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 35]*x12    *x31*x41*x51
        +coeff[ 36]*x13*x21    *x41    
        +coeff[ 37]        *x34*x42    
        +coeff[ 38]        *x32*x44    
        +coeff[ 39]    *x24        *x52
        +coeff[ 40]        *x32    *x54
        +coeff[ 41]*x11*x24        *x51
        +coeff[ 42]*x11*x23*x31    *x51
        +coeff[ 43]*x11*x23    *x41*x51
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 44]*x11*x21    *x43*x51
        +coeff[ 45]*x12    *x32*x41*x51
        +coeff[ 46]    *x23*x33*x41    
        +coeff[ 47]    *x23*x32*x42    
        +coeff[ 48]    *x21*x33*x43    
        +coeff[ 49]    *x24    *x42*x51
        +coeff[ 50]    *x21*x33*x42*x51
        +coeff[ 51]    *x22    *x44*x51
        +coeff[ 52]    *x24*x31    *x52
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 53]    *x23*x32    *x52
        +coeff[ 54]        *x33*x41*x53
        +coeff[ 55]    *x21*x31*x42*x53
        +coeff[ 56]        *x32*x42*x53
        +coeff[ 57]*x11*x24*x32        
        +coeff[ 58]*x11*x21*x32*x43    
        +coeff[ 59]*x11*x24        *x52
        +coeff[ 60]*x11*x21*x32*x41*x52
        +coeff[ 61]*x12*x24*x31        
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 62]*x12*x24    *x41    
        +coeff[ 63]*x12*x21*x33*x41    
        +coeff[ 64]*x12    *x33*x42    
        +coeff[ 65]*x12    *x31*x44    
        +coeff[ 66]*x12*x23*x31    *x51
        +coeff[ 67]*x12    *x34    *x51
        +coeff[ 68]*x13*x21*x33        
        +coeff[ 69]*x13*x23    *x41    
        +coeff[ 70]*x13*x21*x32*x41    
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 71]    *x24*x33    *x51
        +coeff[ 72]    *x22*x34*x41*x51
        +coeff[ 73]    *x22*x32*x43*x51
        +coeff[ 74]    *x24*x31    *x53
        +coeff[ 75]    *x22*x32*x41*x53
        +coeff[ 76]        *x34    *x54
        +coeff[ 77]    *x21            
        +coeff[ 78]                *x52
        +coeff[ 79]*x11*x21            
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 80]    *x21*x32        
        +coeff[ 81]        *x33        
        +coeff[ 82]    *x21*x31*x41    
        +coeff[ 83]    *x22        *x51
        +coeff[ 84]        *x32    *x51
        +coeff[ 85]*x11*x21*x31        
        +coeff[ 86]*x11*x21    *x41    
        +coeff[ 87]*x12    *x31        
        +coeff[ 88]*x12        *x41    
    ;
    v_l_e_q2en_400                                =v_l_e_q2en_400                                
        +coeff[ 89]*x13                
        ;

    return v_l_e_q2en_400                                ;
}
float x_e_q2en_350                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.2860262E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.28714442E-03, 0.13243166E+00, 0.58208406E-02,-0.11307107E-02,
        -0.90594694E-03,-0.25984661E-02,-0.18460420E-02,-0.65217801E-05,
         0.20439923E-03,-0.10058769E-02,-0.22737338E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_350                                =v_x_e_q2en_350                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_350                                ;
}
float t_e_q2en_350                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.6411083E-04;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.64255371E-04,-0.20098502E-02, 0.24474995E-01, 0.22347239E-02,
         0.11043899E-03,-0.29097477E-03,-0.74470963E-03,-0.73306967E-03,
         0.11885914E-04,-0.42152623E-03,-0.35616462E-03,-0.12211877E-03,
        -0.11962507E-02,-0.82895521E-03,-0.36884732E-04, 0.68803747E-04,
        -0.77721017E-03, 0.14724537E-03,-0.49905753E-04,-0.34095374E-04,
         0.31950622E-04, 0.14267020E-04,-0.46003822E-03,-0.98970637E-03,
         0.15830237E-03,-0.27654607E-04,-0.74830705E-05,-0.81065064E-05,
         0.12911241E-04, 0.17444258E-04,-0.24288765E-04, 0.60576585E-04,
        -0.22181554E-04, 0.23369807E-04,-0.15681017E-04,-0.29035664E-04,
         0.94630707E-06, 0.72285360E-06, 0.45296947E-05,-0.66031234E-05,
        -0.41052067E-05, 0.31562624E-05, 0.30173087E-05, 0.81710123E-05,
        -0.60864832E-05,-0.75532089E-05,-0.56018735E-05, 0.59896902E-05,
        -0.55419191E-05, 0.64867572E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_q2en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_350                                =v_t_e_q2en_350                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21*x31*x43    
    ;
    v_t_e_q2en_350                                =v_t_e_q2en_350                                
        +coeff[ 17]    *x23    *x42*x51
        +coeff[ 18]*x11    *x31*x41    
        +coeff[ 19]*x11        *x42    
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]*x13    *x32        
        +coeff[ 22]    *x21*x33*x41    
        +coeff[ 23]    *x21*x32*x42    
        +coeff[ 24]    *x23*x31*x41*x51
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2en_350                                =v_t_e_q2en_350                                
        +coeff[ 26]*x11            *x52
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]*x12*x22    *x41    
        +coeff[ 29]*x13*x22*x31        
        +coeff[ 30]*x12*x21    *x43    
        +coeff[ 31]    *x23*x32    *x51
        +coeff[ 32]*x11*x23    *x41*x51
        +coeff[ 33]*x12*x21    *x42*x51
        +coeff[ 34]*x12*x21*x31    *x52
    ;
    v_t_e_q2en_350                                =v_t_e_q2en_350                                
        +coeff[ 35]    *x21*x32*x41*x52
        +coeff[ 36]*x11    *x31        
        +coeff[ 37]    *x21*x31        
        +coeff[ 38]    *x21    *x41    
        +coeff[ 39]*x13                
        +coeff[ 40]*x12*x21            
        +coeff[ 41]*x11        *x41*x51
        +coeff[ 42]    *x21    *x41*x51
        +coeff[ 43]*x13*x21            
    ;
    v_t_e_q2en_350                                =v_t_e_q2en_350                                
        +coeff[ 44]*x11*x23            
        +coeff[ 45]*x12*x21*x31        
        +coeff[ 46]*x11    *x33        
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]*x13            *x51
        +coeff[ 49]*x11*x22        *x51
        ;

    return v_t_e_q2en_350                                ;
}
float y_e_q2en_350                                (float *x,int m){
    int ncoeff= 31;
    float avdat=  0.2092681E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
        -0.20318523E-02, 0.21104738E+00, 0.13760985E+00,-0.28799539E-02,
        -0.31184196E-02,-0.20776645E-03,-0.34958019E-04, 0.12493152E-03,
        -0.28734142E-03, 0.23712307E-03,-0.17575421E-04, 0.10890757E-03,
         0.12303206E-03, 0.10623464E-03,-0.49135165E-03,-0.69105350E-04,
         0.72867937E-04,-0.11787986E-04,-0.84152067E-04, 0.83784063E-04,
        -0.34058164E-03,-0.12947686E-03, 0.41495139E-04,-0.17719717E-03,
         0.11123880E-04,-0.88173192E-05, 0.63170795E-04,-0.34725518E-03,
        -0.42913962E-03,-0.18536697E-03,-0.33350698E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]            *x43    
    ;
    v_y_e_q2en_350                                =v_y_e_q2en_350                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]    *x24    *x43    
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_q2en_350                                =v_y_e_q2en_350                                
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]*x11*x23*x31        
        +coeff[ 22]    *x22    *x43*x51
        +coeff[ 23]*x11*x23    *x43    
        +coeff[ 24]    *x21*x31*x41    
        +coeff[ 25]            *x43*x51
    ;
    v_y_e_q2en_350                                =v_y_e_q2en_350                                
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x22*x31*x42    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]    *x22*x33        
        +coeff[ 30]*x12    *x31*x42    
        ;

    return v_y_e_q2en_350                                ;
}
float p_e_q2en_350                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.2202681E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.21633469E-03, 0.81800818E-02, 0.25668314E-01, 0.67172816E-03,
         0.17437304E-03,-0.26473589E-03,-0.24685991E-03,-0.28735155E-03,
        -0.15951447E-03,-0.31716194E-04,-0.17315784E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_350                                =v_p_e_q2en_350                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_350                                ;
}
float l_e_q2en_350                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2429202E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24583186E-02,-0.90942405E-04,-0.34567632E-02,-0.98491313E-04,
        -0.13801709E-02,-0.47913417E-02,-0.58378628E-02,-0.20066786E-03,
        -0.74098777E-03, 0.52480312E-03,-0.50535233E-03, 0.16588943E-03,
        -0.62554015E-03, 0.21974843E-03, 0.66522509E-03, 0.32881214E-03,
        -0.55666908E-03, 0.23121473E-02,-0.70472213E-03, 0.85241091E-03,
         0.11257796E-02, 0.33405438E-03, 0.14650363E-03, 0.23343199E-03,
        -0.16800753E-02,-0.23025412E-02,-0.37397875E-03,-0.11847030E-02,
         0.17462710E-02,-0.18701321E-02, 0.94185688E-03, 0.11696711E-03,
        -0.22063470E-02, 0.51935902E-03,-0.22745745E-02,-0.16552225E-02,
         0.33057523E-02, 0.31770332E-03, 0.22243229E-02,-0.11669209E-02,
         0.95805019E-03, 0.13278388E-03,-0.13844992E-02, 0.84136310E-03,
        -0.24062712E-02,-0.15712406E-02,-0.69467869E-03,-0.21831405E-02,
         0.23763259E-02,-0.41617683E-03, 0.23832556E-03, 0.29171165E-03,
         0.92788268E-05,-0.22539540E-04,-0.31394733E-03,-0.90181318E-04,
         0.13516990E-03, 0.11420290E-03,-0.34347721E-03,-0.10131941E-03,
         0.16409848E-03, 0.86191576E-04,-0.11581946E-03, 0.23299383E-03,
        -0.48052549E-03,-0.10979785E-03, 0.53007843E-03, 0.83591556E-03,
         0.51356730E-03, 0.11734341E-02, 0.26520892E-03, 0.49228704E-03,
        -0.18206608E-03,-0.15546795E-03,-0.10116460E-03, 0.23977208E-03,
        -0.13969014E-03, 0.91890597E-05, 0.51002839E-03,-0.98577038E-04,
         0.24567189E-03,-0.34033033E-03, 0.72050854E-04,-0.17185304E-03,
        -0.25186571E-03, 0.43333307E-03, 0.27853093E-04,-0.81646212E-04,
         0.74233400E-03, 0.64079487E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_350                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]    *x21*x31        
        +coeff[  4]        *x32        
        +coeff[  5]        *x31*x41    
        +coeff[  6]            *x42    
        +coeff[  7]    *x23            
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[  8]    *x21*x32        
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]*x13                
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]*x11*x21    *x41*x51
        +coeff[ 14]*x12    *x31*x41    
        +coeff[ 15]        *x33*x42    
        +coeff[ 16]        *x33*x41*x51
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 17]*x11*x23*x31        
        +coeff[ 18]*x11*x21*x33        
        +coeff[ 19]*x11*x22*x31*x41    
        +coeff[ 20]*x11*x21*x32    *x51
        +coeff[ 21]*x11            *x54
        +coeff[ 22]*x12    *x31    *x52
        +coeff[ 23]*x13    *x32        
        +coeff[ 24]        *x34*x42    
        +coeff[ 25]        *x33*x43    
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 26]    *x21    *x43*x52
        +coeff[ 27]*x11*x22        *x53
        +coeff[ 28]*x12*x22*x32        
        +coeff[ 29]*x12*x22        *x52
        +coeff[ 30]*x12            *x54
        +coeff[ 31]    *x24*x31    *x52
        +coeff[ 32]    *x22*x32*x41*x52
        +coeff[ 33]        *x33    *x54
        +coeff[ 34]*x11*x23*x33        
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 35]*x11*x24    *x41*x51
        +coeff[ 36]*x11*x21*x33*x41*x51
        +coeff[ 37]*x11*x22    *x43*x51
        +coeff[ 38]*x11*x21*x33    *x52
        +coeff[ 39]*x11*x21*x31    *x54
        +coeff[ 40]*x12*x22*x31*x42    
        +coeff[ 41]*x12    *x33*x42    
        +coeff[ 42]*x12    *x31    *x54
        +coeff[ 43]*x13*x21*x33        
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 44]        *x34*x44    
        +coeff[ 45]*x13*x21*x31*x41*x51
        +coeff[ 46]*x13    *x31*x41*x52
        +coeff[ 47]    *x23*x32*x41*x52
        +coeff[ 48]    *x23    *x43*x52
        +coeff[ 49]*x13*x21        *x53
        +coeff[ 50]    *x22    *x42*x54
        +coeff[ 51]    *x21            
        +coeff[ 52]            *x41    
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 53]*x11                
        +coeff[ 54]                *x52
        +coeff[ 55]*x11            *x51
        +coeff[ 56]    *x22*x31        
        +coeff[ 57]        *x32*x41    
        +coeff[ 58]    *x21    *x42    
        +coeff[ 59]    *x21        *x52
        +coeff[ 60]            *x41*x52
        +coeff[ 61]*x11        *x42    
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 62]*x11    *x31    *x51
        +coeff[ 63]*x11        *x41*x51
        +coeff[ 64]*x11            *x52
        +coeff[ 65]*x12        *x41    
        +coeff[ 66]        *x34        
        +coeff[ 67]    *x22*x31*x41    
        +coeff[ 68]    *x22    *x42    
        +coeff[ 69]        *x32*x42    
        +coeff[ 70]            *x44    
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 71]    *x22        *x52
        +coeff[ 72]    *x21*x31    *x52
        +coeff[ 73]        *x32    *x52
        +coeff[ 74]        *x31*x41*x52
        +coeff[ 75]            *x42*x52
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]            *x41*x53
        +coeff[ 78]*x11*x22        *x51
        +coeff[ 79]*x11*x21        *x52
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 80]*x11            *x53
        +coeff[ 81]*x12    *x32        
        +coeff[ 82]*x12    *x31    *x51
        +coeff[ 83]*x12        *x41*x51
        +coeff[ 84]    *x22*x33        
        +coeff[ 85]    *x21*x34        
        +coeff[ 86]    *x24    *x41    
        +coeff[ 87]    *x23*x31*x41    
        +coeff[ 88]    *x22*x32*x41    
    ;
    v_l_e_q2en_350                                =v_l_e_q2en_350                                
        +coeff[ 89]    *x21*x32*x42    
        ;

    return v_l_e_q2en_350                                ;
}
float x_e_q2en_300                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.8721764E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.89707633E-03, 0.13280593E+00, 0.58059632E-02,-0.11178767E-02,
        -0.90414990E-03,-0.25794436E-02,-0.18371849E-02, 0.17389559E-04,
         0.20189218E-03,-0.10035251E-02,-0.23320659E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_300                                =v_x_e_q2en_300                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_300                                ;
}
float t_e_q2en_300                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1592338E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.16303825E-03,-0.20108661E-02, 0.24673363E-01, 0.22137901E-02,
         0.10792003E-03,-0.27929212E-03,-0.75679598E-03,-0.71988016E-03,
         0.13811605E-04,-0.42043539E-03,-0.35519979E-03,-0.12272088E-03,
        -0.87138487E-03,-0.12044655E-02,-0.74622704E-03,-0.23137187E-04,
        -0.40057705E-04,-0.27465880E-04, 0.12939601E-03, 0.81964179E-04,
        -0.24624015E-06,-0.43539374E-03,-0.95267245E-03,-0.13791132E-04,
        -0.37929613E-05, 0.83723089E-05,-0.88021858E-06,-0.61378620E-06,
        -0.21195769E-04,-0.76167237E-06,-0.16802467E-04, 0.16262520E-04,
         0.38797472E-04,-0.35538535E-05, 0.11888693E-04, 0.38295589E-05,
         0.16175298E-05, 0.60512892E-04, 0.76577526E-05, 0.50464978E-05,
         0.54109241E-05,-0.10427510E-04,-0.14226127E-04,-0.10288697E-04,
        -0.37083646E-04, 0.10738544E-04,-0.27203101E-04, 0.47858944E-05,
         0.42292427E-05, 0.43059740E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_300                                =v_t_e_q2en_300                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11*x22            
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_300                                =v_t_e_q2en_300                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]    *x21*x32    *x53
        +coeff[ 25]    *x22            
    ;
    v_t_e_q2en_300                                =v_t_e_q2en_300                                
        +coeff[ 26]            *x42    
        +coeff[ 27]*x12*x21            
        +coeff[ 28]*x11    *x32        
        +coeff[ 29]    *x22    *x41    
        +coeff[ 30]    *x22    *x42    
        +coeff[ 31]    *x22*x32*x41    
        +coeff[ 32]    *x23        *x53
        +coeff[ 33]        *x31*x41    
        +coeff[ 34]*x11*x21*x31*x41    
    ;
    v_t_e_q2en_300                                =v_t_e_q2en_300                                
        +coeff[ 35]*x11*x21    *x42    
        +coeff[ 36]    *x22*x31    *x51
        +coeff[ 37]    *x21*x32    *x51
        +coeff[ 38]*x11    *x31*x41*x51
        +coeff[ 39]        *x31*x42*x51
        +coeff[ 40]*x11*x21        *x52
        +coeff[ 41]*x13*x22            
        +coeff[ 42]*x12*x23            
        +coeff[ 43]*x13*x21    *x41    
    ;
    v_t_e_q2en_300                                =v_t_e_q2en_300                                
        +coeff[ 44]*x11*x22*x31*x41    
        +coeff[ 45]*x11*x21*x32*x41    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]*x12*x22        *x51
        +coeff[ 48]*x11    *x33    *x51
        +coeff[ 49]    *x21*x33    *x51
        ;

    return v_t_e_q2en_300                                ;
}
float y_e_q2en_300                                (float *x,int m){
    int ncoeff= 28;
    float avdat=  0.8826037E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
        -0.87260414E-03, 0.21104021E+00, 0.13742305E+00,-0.28930043E-02,
        -0.31117317E-02,-0.23941300E-03,-0.29429948E-03, 0.18255324E-03,
        -0.35529491E-03, 0.88414075E-04,-0.20978327E-04, 0.11771365E-03,
         0.12958016E-03, 0.12971931E-05,-0.46568218E-03,-0.21863225E-04,
        -0.15109352E-04, 0.10563039E-03, 0.86326509E-04, 0.71739014E-04,
         0.16294760E-04,-0.19392287E-03,-0.28893669E-03,-0.15226492E-04,
        -0.15223444E-03,-0.12228571E-03,-0.12357807E-03, 0.45529883E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_300                                =v_y_e_q2en_300                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_300                                =v_y_e_q2en_300                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x21*x33*x45    
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]    *x22*x32*x41    
        +coeff[ 23]*x11        *x41*x52
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]*x11*x23    *x41    
    ;
    v_y_e_q2en_300                                =v_y_e_q2en_300                                
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]        *x32*x45    
        ;

    return v_y_e_q2en_300                                ;
}
float p_e_q2en_300                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.1256913E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.12565701E-03, 0.81239268E-02, 0.25628736E-01, 0.67219551E-03,
         0.17386030E-03,-0.26042765E-03,-0.24191960E-03,-0.28221318E-03,
        -0.15768279E-03,-0.30107205E-04,-0.16812232E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_300                                =v_p_e_q2en_300                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_300                                ;
}
float l_e_q2en_300                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2397120E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.22758786E-02,-0.30504249E-02,-0.12073029E-02,-0.40357113E-02,
        -0.51132231E-02, 0.32248342E-03,-0.39380512E-03,-0.13015602E-02,
         0.36519233E-03,-0.11437593E-02, 0.56227640E-03,-0.88619003E-04,
         0.42682432E-03,-0.43410013E-03, 0.40982879E-03, 0.11184750E-02,
         0.30780684E-04, 0.10120983E-02, 0.11613085E-02, 0.76636032E-03,
        -0.30190649E-03, 0.43918070E-03, 0.12928816E-02,-0.99846791E-03,
        -0.45741882E-03, 0.26137903E-03,-0.10238767E-02, 0.56110870E-03,
         0.98532392E-03,-0.22237883E-02, 0.24493055E-02, 0.62531146E-03,
        -0.89981317E-04,-0.62977476E-03, 0.64867781E-03,-0.15390136E-02,
        -0.87492610E-03, 0.16272234E-03,-0.11895540E-02,-0.43408744E-03,
        -0.27423649E-03,-0.91142580E-03,-0.93689701E-03,-0.20832171E-04,
        -0.10487483E-02, 0.10960688E-02, 0.13115504E-02, 0.63726469E-03,
        -0.75086969E-03,-0.10787784E-02, 0.16501053E-02,-0.22630871E-02,
         0.13888457E-02,-0.71914343E-03, 0.17425094E-02, 0.11001469E-02,
         0.19042710E-03,-0.93463896E-04, 0.27944666E-03,-0.15416613E-03,
        -0.13442595E-04, 0.13220246E-03, 0.59658527E-04,-0.40358494E-03,
        -0.22224220E-03, 0.14254473E-03, 0.68744397E-04,-0.15278981E-03,
        -0.53711137E-03,-0.22890655E-03,-0.69518399E-04, 0.12417161E-03,
         0.75053533E-04, 0.13265110E-03, 0.12738713E-03,-0.46961883E-03,
         0.29187254E-03,-0.54977817E-03, 0.23625324E-03, 0.12140240E-03,
        -0.34364749E-03,-0.22057841E-03,-0.19416276E-03, 0.19557544E-03,
        -0.19448063E-03, 0.14199813E-03,-0.12892472E-03, 0.17280775E-03,
        -0.17545641E-03,-0.49620320E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_300                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x23            
        +coeff[  6]    *x21    *x42    
        +coeff[  7]*x12*x21*x31*x41    
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[  8]    *x23*x32*x41    
        +coeff[  9]    *x24    *x42    
        +coeff[ 10]*x13        *x42*x52
        +coeff[ 11]                *x51
        +coeff[ 12]*x11*x21            
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]        *x31*x41*x51
        +coeff[ 15]            *x42*x51
        +coeff[ 16]                *x53
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 17]*x11    *x31*x41    
        +coeff[ 18]*x11    *x31    *x51
        +coeff[ 19]*x11        *x41*x51
        +coeff[ 20]    *x21    *x41*x52
        +coeff[ 21]        *x31    *x53
        +coeff[ 22]*x11    *x31    *x52
        +coeff[ 23]*x12    *x31*x41    
        +coeff[ 24]*x13*x21            
        +coeff[ 25]    *x21*x33    *x51
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 26]            *x44*x51
        +coeff[ 27]    *x22        *x53
        +coeff[ 28]*x11    *x34        
        +coeff[ 29]*x11    *x31*x43    
        +coeff[ 30]*x11*x21*x31*x41*x51
        +coeff[ 31]*x11*x21    *x42*x51
        +coeff[ 32]*x12*x23            
        +coeff[ 33]*x12*x21    *x42    
        +coeff[ 34]    *x21*x32*x43    
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 35]*x13    *x31    *x51
        +coeff[ 36]*x13        *x41*x51
        +coeff[ 37]    *x24    *x41*x51
        +coeff[ 38]        *x31*x43*x52
        +coeff[ 39]            *x44*x52
        +coeff[ 40]    *x22        *x54
        +coeff[ 41]*x11    *x33    *x52
        +coeff[ 42]*x11    *x31    *x54
        +coeff[ 43]*x12*x24            
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 44]*x12    *x32*x42    
        +coeff[ 45]*x13    *x32*x41    
        +coeff[ 46]    *x23*x33*x41    
        +coeff[ 47]*x13    *x31*x42    
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]    *x24*x31    *x52
        +coeff[ 50]*x11    *x33*x43    
        +coeff[ 51]*x11*x21*x31*x43*x51
        +coeff[ 52]*x11*x21    *x43*x52
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 53]*x12*x24        *x51
        +coeff[ 54]*x12*x22*x31    *x52
        +coeff[ 55]*x12*x22    *x41*x52
        +coeff[ 56]*x13*x23*x31        
        +coeff[ 57]*x11                
        +coeff[ 58]    *x21        *x51
        +coeff[ 59]        *x31    *x51
        +coeff[ 60]*x11        *x41    
        +coeff[ 61]*x12                
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 62]    *x21*x32        
        +coeff[ 63]    *x21*x31*x41    
        +coeff[ 64]        *x31*x42    
        +coeff[ 65]        *x32    *x51
        +coeff[ 66]        *x31    *x52
        +coeff[ 67]            *x41*x52
        +coeff[ 68]*x11    *x32        
        +coeff[ 69]*x11*x21    *x41    
        +coeff[ 70]*x11        *x42    
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 71]*x11            *x52
        +coeff[ 72]*x12        *x41    
        +coeff[ 73]*x12            *x51
        +coeff[ 74]*x13                
        +coeff[ 75]    *x22*x32        
        +coeff[ 76]        *x34        
        +coeff[ 77]    *x22*x31*x41    
        +coeff[ 78]    *x21*x31*x41*x51
        +coeff[ 79]            *x43*x51
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 80]    *x21        *x53
        +coeff[ 81]*x11*x22*x31        
        +coeff[ 82]*x11*x21*x32        
        +coeff[ 83]*x11    *x33        
        +coeff[ 84]*x11*x21    *x42    
        +coeff[ 85]*x11*x21*x31    *x51
        +coeff[ 86]*x11    *x31*x41*x51
        +coeff[ 87]*x11*x21        *x52
        +coeff[ 88]*x11        *x41*x52
    ;
    v_l_e_q2en_300                                =v_l_e_q2en_300                                
        +coeff[ 89]*x12*x22            
        ;

    return v_l_e_q2en_300                                ;
}
float x_e_q2en_250                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1431060E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.14610745E-02, 0.13333921E+00, 0.57899100E-02,-0.11011696E-02,
        -0.89917734E-03,-0.25818858E-02,-0.18361523E-02,-0.22488764E-05,
         0.20162738E-03,-0.10053928E-02,-0.23023445E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x23            
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x21*x32    *x52
    ;
    v_x_e_q2en_250                                =v_x_e_q2en_250                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_250                                ;
}
float t_e_q2en_250                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.2672359E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.27274329E-03,-0.20031140E-02, 0.24957253E-01, 0.21956186E-02,
         0.10395908E-03,-0.28659054E-03,-0.76506764E-03,-0.72605966E-03,
         0.94922025E-05,-0.42128764E-03,-0.35125413E-03,-0.12345746E-03,
        -0.11979399E-02,-0.83693705E-03,-0.22640566E-04,-0.71258302E-03,
        -0.27854827E-04,-0.18302842E-04, 0.13561534E-03, 0.97138873E-04,
         0.40467562E-05,-0.41347003E-03,-0.91891096E-03, 0.78398043E-05,
        -0.15309623E-05,-0.44903518E-06, 0.90021276E-05,-0.17933551E-04,
         0.57981862E-04,-0.21343147E-04,-0.10408607E-03,-0.80490092E-04,
         0.38826871E-04, 0.13068134E-04, 0.79834695E-06,-0.25556145E-06,
        -0.29477160E-05, 0.23942257E-05,-0.27658859E-05,-0.71681488E-05,
         0.17621309E-04,-0.43119267E-05, 0.66581674E-05,-0.36403644E-05,
         0.62956842E-05,-0.55886594E-05, 0.12879166E-04, 0.10467738E-04,
        -0.69581320E-05, 0.22035409E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_250                                =v_t_e_q2en_250                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_250                                =v_t_e_q2en_250                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]*x13    *x32        
        +coeff[ 21]    *x21*x33*x41    
        +coeff[ 22]    *x21*x32*x42    
        +coeff[ 23]    *x23*x32    *x51
        +coeff[ 24]*x11        *x41    
        +coeff[ 25]*x13                
    ;
    v_t_e_q2en_250                                =v_t_e_q2en_250                                
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]*x11*x22*x32        
        +coeff[ 30]*x11*x22*x31*x41    
        +coeff[ 31]*x11*x22    *x42    
        +coeff[ 32]    *x21*x31*x42*x51
        +coeff[ 33]    *x23        *x53
        +coeff[ 34]                *x51
    ;
    v_t_e_q2en_250                                =v_t_e_q2en_250                                
        +coeff[ 35]*x12                
        +coeff[ 36]*x11*x21    *x41    
        +coeff[ 37]    *x21    *x41*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]    *x22*x31    *x51
        +coeff[ 42]*x11*x21    *x41*x51
        +coeff[ 43]*x12            *x52
    ;
    v_t_e_q2en_250                                =v_t_e_q2en_250                                
        +coeff[ 44]*x11            *x53
        +coeff[ 45]*x12*x22    *x41    
        +coeff[ 46]*x11*x23    *x41    
        +coeff[ 47]*x12*x21    *x42    
        +coeff[ 48]*x12    *x31*x41*x51
        +coeff[ 49]    *x21*x32*x41*x51
        ;

    return v_t_e_q2en_250                                ;
}
float y_e_q2en_250                                (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.9890061E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.96979755E-03, 0.21062279E+00, 0.13713565E+00,-0.28603759E-02,
        -0.30967267E-02,-0.18773924E-03,-0.15308178E-04, 0.95450974E-04,
        -0.26038819E-03, 0.20308136E-03,-0.59956405E-04, 0.10743350E-03,
         0.11999906E-03,-0.10225477E-04,-0.48142215E-03,-0.51342198E-04,
        -0.10069283E-04,-0.53618060E-04, 0.25858855E-04, 0.87592161E-04,
        -0.34209850E-03,-0.47926710E-03,-0.21114499E-03,-0.14030533E-05,
         0.71079972E-04, 0.16764396E-04,-0.32392695E-05, 0.53725912E-05,
        -0.27892631E-04, 0.21031312E-04,-0.35308371E-03,-0.43631397E-04,
        -0.35514091E-04,-0.41907231E-04,-0.27857572E-04,-0.43497930E-04,
        -0.29984816E-04, 0.85554231E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x22    *x43    
        +coeff[  7]            *x43    
    ;
    v_y_e_q2en_250                                =v_y_e_q2en_250                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]        *x31*x42    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]    *x24    *x43    
        +coeff[ 16]        *x33        
    ;
    v_y_e_q2en_250                                =v_y_e_q2en_250                                
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]    *x22    *x41*x51
        +coeff[ 19]    *x22*x31    *x51
        +coeff[ 20]    *x24    *x41    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]*x11                
        +coeff[ 24]        *x32*x41    
        +coeff[ 25]*x11        *x42    
    ;
    v_y_e_q2en_250                                =v_y_e_q2en_250                                
        +coeff[ 26]            *x42*x51
        +coeff[ 27]        *x31*x41*x51
        +coeff[ 28]            *x43*x51
        +coeff[ 29]*x11*x21    *x42    
        +coeff[ 30]    *x22*x31*x42    
        +coeff[ 31]        *x31*x43*x51
        +coeff[ 32]    *x21    *x42*x52
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]*x12*x22    *x41    
    ;
    v_y_e_q2en_250                                =v_y_e_q2en_250                                
        +coeff[ 35]*x11*x21    *x41*x52
        +coeff[ 36]    *x21*x31*x45    
        +coeff[ 37]    *x24    *x41*x51
        ;

    return v_y_e_q2en_250                                ;
}
float p_e_q2en_250                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.7362146E-04;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.71772360E-04, 0.80448082E-02, 0.25533203E-01, 0.67383936E-03,
         0.17741532E-03,-0.26240881E-03,-0.24257838E-03,-0.28468270E-03,
        -0.16109634E-03,-0.29576137E-04,-0.16966215E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_250                                =v_p_e_q2en_250                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_250                                ;
}
float l_e_q2en_250                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2362468E-02;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.22252901E-02,-0.21896347E-04,-0.34133794E-02,-0.11695822E-02,
        -0.46499148E-02,-0.50613438E-02, 0.26085083E-04, 0.57610963E-03,
        -0.13193495E-03, 0.51093724E-03, 0.58018364E-03,-0.25595419E-03,
         0.23039451E-03,-0.23281061E-03,-0.47685546E-04, 0.31431997E-03,
         0.32977259E-03,-0.38084664E-03,-0.35058073E-03,-0.32437581E-03,
         0.15423619E-03, 0.48389443E-03, 0.60096616E-03,-0.60792745E-03,
         0.37152614E-03, 0.36306036E-03, 0.19581856E-03,-0.44371717E-03,
        -0.27427825E-03,-0.31656865E-03,-0.11541168E-02,-0.82299032E-03,
         0.11419833E-02,-0.53526700E-03, 0.27615903E-02,-0.12805445E-03,
        -0.50561200E-03,-0.26178526E-03, 0.38584811E-03,-0.11383211E-03,
        -0.16663652E-02,-0.17103355E-03,-0.14092994E-02,-0.26543465E-03,
         0.47351912E-03, 0.11846428E-02,-0.52336568E-03, 0.64232200E-03,
        -0.24866415E-02,-0.12101076E-02, 0.41609394E-03, 0.60073100E-03,
         0.15447669E-02,-0.37829578E-02,-0.30052969E-02, 0.12645152E-02,
         0.91841113E-03, 0.16495015E-02, 0.10123502E-02,-0.15338369E-02,
         0.10301711E-02, 0.25214392E-03, 0.27570585E-03,-0.11792580E-03,
        -0.14684389E-03, 0.14428209E-03,-0.11259071E-03,-0.10589259E-03,
         0.44739234E-04, 0.52999362E-03,-0.46896562E-03, 0.27049915E-03,
        -0.16992750E-03,-0.19370520E-03,-0.99122124E-04, 0.12134192E-03,
         0.15444559E-03,-0.17303000E-03, 0.22677080E-03, 0.46964621E-03,
         0.67100761E-03, 0.22294642E-03,-0.37724164E-03, 0.10325284E-02,
         0.68932516E-03, 0.46125695E-03, 0.10435152E-02,-0.58191386E-03,
        -0.24045266E-03,-0.26545269E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_250                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11        *x41    
        +coeff[  7]            *x42*x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[  8]*x11*x21    *x41    
        +coeff[  9]        *x31*x42*x51
        +coeff[ 10]*x11*x21        *x52
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]            *x43    
        +coeff[ 13]    *x22        *x51
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]        *x32    *x51
        +coeff[ 16]        *x31*x41*x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 17]*x11    *x32        
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x22        *x52
        +coeff[ 20]            *x42*x52
        +coeff[ 21]*x11*x22    *x41    
        +coeff[ 22]*x11    *x32*x41    
        +coeff[ 23]*x11        *x43    
        +coeff[ 24]*x11            *x53
        +coeff[ 25]*x12*x22            
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 26]*x12*x21        *x51
        +coeff[ 27]*x12            *x52
        +coeff[ 28]    *x21    *x43*x51
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]*x11    *x33*x41    
        +coeff[ 31]*x11    *x33    *x51
        +coeff[ 32]*x11*x22    *x41*x51
        +coeff[ 33]*x11    *x32*x41*x51
        +coeff[ 34]*x11*x21    *x42*x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 35]*x11*x21    *x41*x52
        +coeff[ 36]*x12        *x43    
        +coeff[ 37]*x12            *x53
        +coeff[ 38]    *x21*x32*x41*x52
        +coeff[ 39]        *x33*x41*x52
        +coeff[ 40]        *x32    *x54
        +coeff[ 41]*x11*x24*x31        
        +coeff[ 42]*x11    *x31*x44    
        +coeff[ 43]*x11*x24        *x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 44]*x12    *x34        
        +coeff[ 45]*x13    *x32    *x51
        +coeff[ 46]    *x21*x33    *x53
        +coeff[ 47]*x11*x24*x31    *x51
        +coeff[ 48]*x11*x23*x32    *x51
        +coeff[ 49]*x11*x21*x32*x41*x52
        +coeff[ 50]*x11*x23        *x53
        +coeff[ 51]    *x23*x31*x44    
        +coeff[ 52]*x13*x21*x32    *x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 53]*x13*x21    *x42*x51
        +coeff[ 54]    *x23*x31*x42*x52
        +coeff[ 55]*x13*x21        *x53
        +coeff[ 56]        *x34*x41*x53
        +coeff[ 57]    *x22*x31*x42*x53
        +coeff[ 58]    *x23*x31    *x54
        +coeff[ 59]    *x23    *x41*x54
        +coeff[ 60]    *x21    *x43*x54
        +coeff[ 61]*x11                
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 62]                *x52
        +coeff[ 63]*x11*x21            
        +coeff[ 64]*x11            *x51
        +coeff[ 65]    *x23            
        +coeff[ 66]        *x33        
        +coeff[ 67]    *x22    *x41    
        +coeff[ 68]        *x31*x42    
        +coeff[ 69]*x11    *x31*x41    
        +coeff[ 70]*x11*x21        *x51
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 71]*x11    *x31    *x51
        +coeff[ 72]*x11        *x41*x51
        +coeff[ 73]*x11            *x52
        +coeff[ 74]*x12*x21            
        +coeff[ 75]*x12    *x31        
        +coeff[ 76]*x12        *x41    
        +coeff[ 77]*x13                
        +coeff[ 78]    *x23    *x41    
        +coeff[ 79]        *x33*x41    
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 80]    *x21*x31*x42    
        +coeff[ 81]    *x22    *x41*x51
        +coeff[ 82]    *x21*x31    *x52
        +coeff[ 83]        *x32    *x52
        +coeff[ 84]*x11*x22*x31        
        +coeff[ 85]*x11    *x33        
        +coeff[ 86]*x11    *x31*x42    
        +coeff[ 87]*x11    *x32    *x51
        +coeff[ 88]*x11    *x31    *x52
    ;
    v_l_e_q2en_250                                =v_l_e_q2en_250                                
        +coeff[ 89]*x11        *x41*x52
        ;

    return v_l_e_q2en_250                                ;
}
float x_e_q2en_200                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1645645E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.20108989E-03,-0.10777938E-02, 0.13414121E+00, 0.57592406E-02,
        -0.25754431E-02,-0.18316460E-02, 0.13277610E-03, 0.19958807E-03,
        -0.94449427E-03,-0.10771988E-02,-0.22803925E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_q2en_200                                =v_x_e_q2en_200                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_200                                ;
}
float t_e_q2en_200                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.4329544E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.49887829E-04,-0.19883576E-02, 0.25382053E-01, 0.21824457E-02,
         0.10572554E-03,-0.28982284E-03,-0.75648690E-03,-0.73144992E-03,
         0.11439964E-05,-0.41171184E-03,-0.34804776E-03,-0.11983929E-03,
        -0.11959922E-02,-0.82799268E-03,-0.32173455E-04,-0.74041763E-03,
        -0.34607969E-04,-0.20691283E-04, 0.12141398E-03, 0.70208720E-04,
        -0.41801456E-03,-0.93635812E-03,-0.13954722E-04,-0.19922983E-04,
         0.74675418E-05, 0.69785259E-04, 0.96908634E-05, 0.17691696E-04,
        -0.25542016E-04,-0.17633505E-04,-0.28296099E-04,-0.43266186E-05,
         0.37948005E-05,-0.84736193E-05,-0.49629575E-05, 0.73287824E-05,
        -0.39568235E-05, 0.58009605E-05, 0.20705465E-04,-0.53663111E-05,
        -0.43359319E-05, 0.54916964E-05, 0.71022528E-05,-0.14686173E-04,
         0.16587725E-04,-0.46482430E-04,-0.42404507E-04,-0.40963037E-05,
         0.14307565E-04, 0.52907562E-05,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_200                                =v_t_e_q2en_200                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_200                                =v_t_e_q2en_200                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x21*x32    *x51
    ;
    v_t_e_q2en_200                                =v_t_e_q2en_200                                
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x22        *x53
        +coeff[ 28]*x12*x21*x32    *x51
        +coeff[ 29]*x11*x22*x31    *x52
        +coeff[ 30]    *x22    *x42*x52
        +coeff[ 31]    *x22            
        +coeff[ 32]                *x52
        +coeff[ 33]    *x22        *x51
        +coeff[ 34]*x11            *x52
    ;
    v_t_e_q2en_200                                =v_t_e_q2en_200                                
        +coeff[ 35]*x12*x22            
        +coeff[ 36]*x12    *x32        
        +coeff[ 37]    *x22    *x42    
        +coeff[ 38]    *x23        *x51
        +coeff[ 39]*x12    *x31    *x51
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x21        *x53
        +coeff[ 43]*x13*x21    *x41    
    ;
    v_t_e_q2en_200                                =v_t_e_q2en_200                                
        +coeff[ 44]*x12*x21*x31*x41    
        +coeff[ 45]*x11*x22*x31*x41    
        +coeff[ 46]*x11*x22    *x42    
        +coeff[ 47]*x11*x21*x31*x42    
        +coeff[ 48]*x11*x21    *x43    
        +coeff[ 49]    *x23    *x41*x51
        ;

    return v_t_e_q2en_200                                ;
}
float y_e_q2en_200                                (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.3072685E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
        -0.11725160E-03, 0.21022743E+00, 0.13667442E+00,-0.28647245E-02,
        -0.30745352E-02,-0.23557150E-03,-0.30699986E-03,-0.31450874E-03,
        -0.45596945E-03, 0.17090763E-03,-0.46791884E-04, 0.11987085E-03,
         0.12151101E-03,-0.10971065E-04, 0.13821148E-04,-0.13067490E-04,
        -0.80559563E-04, 0.10002604E-03, 0.85519350E-04,-0.32189663E-03,
        -0.16342709E-03,-0.18210080E-03, 0.11209873E-03, 0.56934558E-04,
         0.18762313E-06, 0.28118819E-04, 0.24532124E-04,-0.27708908E-04,
        -0.73873067E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_q2en_200                                =v_y_e_q2en_200                                
        +coeff[  8]    *x24*x31        
        +coeff[  9]        *x31*x42    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x45    
        +coeff[ 14]*x11*x21    *x43    
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_200                                =v_y_e_q2en_200                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]    *x22*x33        
        +coeff[ 21]    *x22*x31*x44    
        +coeff[ 22]            *x43    
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]*x11        *x42    
        +coeff[ 25]*x11    *x31*x43    
    ;
    v_y_e_q2en_200                                =v_y_e_q2en_200                                
        +coeff[ 26]*x12        *x41*x51
        +coeff[ 27]*x11    *x31*x42*x51
        +coeff[ 28]*x11*x23*x31        
        ;

    return v_y_e_q2en_200                                ;
}
float p_e_q2en_200                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4605014E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.29556686E-04, 0.79227341E-02, 0.25410524E-01, 0.67892153E-03,
         0.18021466E-03,-0.25916594E-03,-0.23702957E-03,-0.28217715E-03,
        -0.15794869E-03,-0.28611123E-04,-0.17073599E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_200                                =v_p_e_q2en_200                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_200                                ;
}
float l_e_q2en_200                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2435248E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.23684904E-02, 0.88272918E-05,-0.35219654E-02,-0.89780398E-03,
        -0.40409388E-02,-0.51453435E-02,-0.32473399E-04,-0.10710749E-02,
         0.69965114E-04,-0.12317352E-03,-0.65957734E-04,-0.72319541E-04,
        -0.89024456E-04,-0.76085344E-05, 0.60237019E-03, 0.36527572E-03,
        -0.78076212E-03, 0.75885956E-03,-0.78528689E-03, 0.22585559E-03,
         0.30864228E-03,-0.30668659E-03, 0.11508155E-02, 0.12399366E-02,
         0.97531115E-03,-0.10429034E-03,-0.11423024E-02,-0.66982931E-03,
        -0.13104149E-02, 0.30087403E-03,-0.19245244E-02,-0.92613115E-03,
        -0.10900212E-02,-0.62694179E-03,-0.57064160E-03,-0.37994902E-03,
        -0.11219343E-02, 0.75874978E-03,-0.15722152E-02, 0.11573308E-03,
         0.27659927E-02,-0.38405648E-04, 0.20438402E-02,-0.11986091E-02,
        -0.10467926E-02, 0.17145732E-02, 0.85081719E-03, 0.71110920E-03,
        -0.88935031E-03,-0.11018431E-02,-0.10499525E-02, 0.78154460E-03,
         0.13933233E-02, 0.18162073E-02, 0.19886210E-02, 0.13083672E-02,
        -0.96090784E-03,-0.16672411E-02, 0.18080577E-03,-0.42581442E-03,
         0.90708127E-05,-0.14153376E-03,-0.10521468E-02, 0.98455981E-04,
         0.24473201E-03, 0.11123432E-04, 0.68970287E-04, 0.88313398E-04,
        -0.12318451E-03, 0.30388465E-03, 0.15385641E-03, 0.16197249E-03,
         0.88096611E-04, 0.35591976E-03,-0.14770728E-03, 0.36930130E-03,
        -0.19133807E-03,-0.20069072E-03, 0.35785233E-04,-0.35909025E-03,
        -0.44798723E-03,-0.16525936E-03, 0.98112672E-04, 0.20567600E-03,
         0.35377534E-03, 0.14727157E-03,-0.24797439E-03,-0.10718541E-03,
         0.26100597E-03, 0.31049017E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_200                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x12*x22        *x51
        +coeff[  7]        *x32*x42*x52
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[  8]*x11*x22*x31*x42    
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41    
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x12                
        +coeff[ 13]*x11*x21*x31        
        +coeff[ 14]*x12    *x31        
        +coeff[ 15]    *x23        *x51
        +coeff[ 16]        *x31*x41*x52
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 17]    *x21        *x53
        +coeff[ 18]*x11    *x31*x42    
        +coeff[ 19]*x11    *x31*x41*x51
        +coeff[ 20]*x13    *x31        
        +coeff[ 21]    *x23*x32        
        +coeff[ 22]    *x21*x34        
        +coeff[ 23]    *x22*x31    *x52
        +coeff[ 24]    *x21*x32    *x52
        +coeff[ 25]    *x21*x31*x41*x52
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 26]*x11*x21    *x42*x51
        +coeff[ 27]*x11*x21*x31    *x52
        +coeff[ 28]*x11        *x42*x52
        +coeff[ 29]*x12*x23            
        +coeff[ 30]*x12*x22*x31        
        +coeff[ 31]*x12*x21*x32        
        +coeff[ 32]*x12        *x42*x51
        +coeff[ 33]*x12    *x31    *x52
        +coeff[ 34]    *x21*x33    *x52
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 35]    *x23        *x53
        +coeff[ 36]    *x22*x33    *x52
        +coeff[ 37]    *x23*x31*x41*x52
        +coeff[ 38]    *x23    *x42*x52
        +coeff[ 39]*x11*x24*x31*x41    
        +coeff[ 40]*x11*x22*x32*x42    
        +coeff[ 41]*x11    *x34*x41*x51
        +coeff[ 42]*x11    *x33*x41*x52
        +coeff[ 43]*x11        *x43*x53
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 44]*x11    *x31*x41*x54
        +coeff[ 45]*x12*x24*x31        
        +coeff[ 46]*x12*x23*x32        
        +coeff[ 47]*x12*x21*x32*x42    
        +coeff[ 48]*x12*x24        *x51
        +coeff[ 49]*x12*x21*x33    *x51
        +coeff[ 50]*x13*x22    *x42    
        +coeff[ 51]*x13    *x32*x41*x51
        +coeff[ 52]*x13*x21    *x42*x51
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 53]*x13        *x42*x52
        +coeff[ 54]    *x22*x32*x42*x52
        +coeff[ 55]    *x24    *x41*x53
        +coeff[ 56]    *x22*x32    *x54
        +coeff[ 57]        *x32*x42*x54
        +coeff[ 58]    *x21*x31        
        +coeff[ 59]    *x21        *x51
        +coeff[ 60]                *x52
        +coeff[ 61]*x11        *x41    
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 62]    *x21*x32        
        +coeff[ 63]    *x22    *x41    
        +coeff[ 64]    *x21    *x42    
        +coeff[ 65]    *x22        *x51
        +coeff[ 66]    *x21*x31    *x51
        +coeff[ 67]        *x32    *x51
        +coeff[ 68]    *x21    *x41*x51
        +coeff[ 69]            *x42*x51
        +coeff[ 70]    *x21        *x52
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 71]        *x31    *x52
        +coeff[ 72]            *x41*x52
        +coeff[ 73]*x11        *x41*x51
        +coeff[ 74]*x12        *x41    
        +coeff[ 75]*x12            *x51
        +coeff[ 76]    *x23*x31        
        +coeff[ 77]        *x31*x43    
        +coeff[ 78]    *x22*x31    *x51
        +coeff[ 79]        *x33    *x51
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 80]    *x22    *x41*x51
        +coeff[ 81]    *x21    *x42*x51
        +coeff[ 82]        *x31*x42*x51
        +coeff[ 83]    *x22        *x52
        +coeff[ 84]        *x31    *x53
        +coeff[ 85]            *x41*x53
        +coeff[ 86]*x11*x22*x31        
        +coeff[ 87]*x11*x21*x32        
        +coeff[ 88]*x11    *x33        
    ;
    v_l_e_q2en_200                                =v_l_e_q2en_200                                
        +coeff[ 89]*x11*x22    *x41    
        ;

    return v_l_e_q2en_200                                ;
}
float x_e_q2en_175                                (float *x,int m){
    int ncoeff= 12;
    float avdat=  0.7801082E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 13]={
        -0.80585701E-03,-0.10579737E-02, 0.13468821E+00, 0.57390076E-02,
        -0.25401884E-02,-0.18135330E-02, 0.72163953E-05, 0.11761758E-03,
         0.20248017E-03,-0.92689105E-03,-0.10579353E-02,-0.23254627E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x21*x32    *x52
        +coeff[  7]    *x23*x32        
    ;
    v_x_e_q2en_175                                =v_x_e_q2en_175                                
        +coeff[  8]*x11            *x51
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        ;

    return v_x_e_q2en_175                                ;
}
float t_e_q2en_175                                (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.1582883E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.16281489E-03,-0.19821725E-02, 0.25686435E-01, 0.21734266E-02,
         0.10539883E-03,-0.29535289E-03,-0.76253328E-03,-0.74435218E-03,
        -0.67951205E-05,-0.38882325E-03,-0.35400430E-03,-0.12165568E-03,
        -0.11677626E-02,-0.80005394E-03,-0.30105579E-04,-0.70579804E-03,
        -0.38828097E-04,-0.19112667E-04, 0.10636282E-03, 0.68950678E-04,
        -0.39685681E-03,-0.87892916E-03,-0.27048425E-04, 0.29829605E-04,
         0.45424105E-04,-0.11655305E-04, 0.63961584E-05, 0.17233655E-04,
         0.19153269E-04,-0.77540128E-04,-0.61043618E-04, 0.16672642E-05,
         0.20150246E-05, 0.19206166E-05,-0.41299904E-06, 0.43980799E-05,
        -0.44661288E-05, 0.17262923E-05,-0.39218517E-05, 0.74486588E-05,
         0.89942623E-05, 0.14755615E-04,-0.46299770E-05, 0.79707188E-05,
        -0.41820222E-05, 0.11034876E-04,-0.22360933E-04,-0.64739393E-05,
         0.12560300E-04, 0.10445569E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_175                                =v_t_e_q2en_175                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_175                                =v_t_e_q2en_175                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]*x11*x22        *x52
        +coeff[ 23]*x12*x23        *x51
        +coeff[ 24]    *x23*x32    *x51
        +coeff[ 25]*x11    *x32        
    ;
    v_t_e_q2en_175                                =v_t_e_q2en_175                                
        +coeff[ 26]    *x23*x31        
        +coeff[ 27]*x11*x21    *x41*x51
        +coeff[ 28]*x13*x22            
        +coeff[ 29]*x11*x22*x31*x41    
        +coeff[ 30]*x11*x22    *x42    
        +coeff[ 31]*x11    *x31        
        +coeff[ 32]    *x21    *x41    
        +coeff[ 33]            *x42    
        +coeff[ 34]                *x52
    ;
    v_t_e_q2en_175                                =v_t_e_q2en_175                                
        +coeff[ 35]    *x22        *x51
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]        *x31*x41*x51
        +coeff[ 38]            *x42*x51
        +coeff[ 39]*x11*x22        *x51
        +coeff[ 40]*x11*x21*x31    *x51
        +coeff[ 41]    *x21*x32    *x51
        +coeff[ 42]    *x22    *x41*x51
        +coeff[ 43]*x11    *x31*x41*x51
    ;
    v_t_e_q2en_175                                =v_t_e_q2en_175                                
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x11*x22*x32        
        +coeff[ 47]*x12*x22    *x41    
        +coeff[ 48]*x13    *x31*x41    
        +coeff[ 49]*x12*x21    *x42    
        ;

    return v_t_e_q2en_175                                ;
}
float y_e_q2en_175                                (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.7114171E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.58349367E-03, 0.20985791E+00, 0.13637099E+00,-0.28216606E-02,
        -0.30651796E-02,-0.28273629E-03,-0.29828132E-03, 0.16726145E-03,
        -0.33956763E-03, 0.10180295E-03,-0.44932283E-04, 0.97643526E-04,
         0.12531750E-03,-0.27708016E-04,-0.41621449E-03,-0.24394923E-04,
        -0.62569750E-04, 0.93273316E-04,-0.24605417E-03, 0.97282627E-05,
         0.81780898E-04,-0.12975317E-04,-0.11838621E-04, 0.93723938E-04,
         0.21481697E-04, 0.43075401E-04,-0.12716980E-03,-0.50234881E-04,
        -0.71814538E-04, 0.14083035E-04, 0.39118364E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_175                                =v_y_e_q2en_175                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_175                                =v_y_e_q2en_175                                
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]    *x22    *x43*x51
        +coeff[ 20]    *x24    *x41*x51
        +coeff[ 21]    *x21    *x43    
        +coeff[ 22]*x11        *x41*x51
        +coeff[ 23]    *x22    *x43    
        +coeff[ 24]*x11    *x31*x41*x51
        +coeff[ 25]        *x34*x41    
    ;
    v_y_e_q2en_175                                =v_y_e_q2en_175                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]        *x31*x42*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]*x13        *x42    
        +coeff[ 30]*x11*x21    *x43*x51
        ;

    return v_y_e_q2en_175                                ;
}
float p_e_q2en_175                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.4341915E-04;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.28035078E-04, 0.78368783E-02, 0.25310203E-01, 0.67964062E-03,
         0.18038019E-03,-0.25512022E-03,-0.23533768E-03,-0.28265611E-03,
        -0.15635621E-03,-0.30566909E-04,-0.17172337E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_175                                =v_p_e_q2en_175                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_175                                ;
}
float l_e_q2en_175                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2370364E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.22899257E-02,-0.35049482E-02,-0.38222500E-03,-0.44727428E-02,
        -0.53948825E-02,-0.70920913E-03,-0.26744113E-02, 0.12355912E-03,
         0.10755083E-03, 0.15371646E-03, 0.12168278E-03,-0.20784436E-03,
         0.38280498E-03, 0.14243209E-03,-0.49208818E-04, 0.86383836E-03,
        -0.69832837E-04,-0.24529736E-03, 0.51883009E-03, 0.97984448E-04,
        -0.17674114E-03,-0.29031560E-02,-0.19147515E-03, 0.21144487E-03,
        -0.12566687E-02,-0.67879230E-03,-0.21644874E-03, 0.10340803E-03,
         0.64978225E-03,-0.57457321E-04,-0.27159131E-02,-0.13153726E-03,
         0.24849383E-03,-0.11732779E-02, 0.71541598E-03, 0.90233010E-03,
        -0.32577614E-03,-0.91972906E-03, 0.88654464E-03, 0.10659309E-02,
         0.14694098E-02,-0.10017285E-02,-0.12436414E-02, 0.89292065E-03,
         0.26473060E-03, 0.19187756E-02,-0.65451779E-03,-0.86629990E-03,
         0.50651602E-03, 0.46178632E-03, 0.12261318E-02, 0.45637274E-03,
         0.17486606E-02,-0.68652706E-03,-0.13395797E-02, 0.19989288E-02,
         0.16496036E-02,-0.28672493E-02, 0.44897420E-03, 0.17943352E-02,
        -0.10534438E-02, 0.15213557E-02, 0.14661005E-02,-0.79615490E-03,
        -0.20830110E-02, 0.17253831E-02, 0.33002968E-02,-0.15898152E-02,
         0.35238543E-02, 0.41983142E-02,-0.57854801E-02,-0.16550518E-02,
        -0.16506653E-02, 0.86783362E-03, 0.24712265E-02,-0.13653673E-02,
         0.57812635E-03, 0.60592371E-03,-0.93452650E-03,-0.11249619E-03,
         0.27957269E-04, 0.20373469E-03, 0.16723225E-03,-0.11726964E-03,
         0.12770556E-03,-0.15169042E-03, 0.14756428E-03, 0.10217726E-03,
         0.35730476E-03,-0.14775062E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_175                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x34        
        +coeff[  6]*x11*x21*x31    *x51
        +coeff[  7]        *x31        
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[  8]            *x41    
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]        *x32    *x51
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]*x12            *x51
        +coeff[ 14]    *x23        *x51
        +coeff[ 15]    *x21*x31*x41*x51
        +coeff[ 16]    *x21    *x41*x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 17]*x11*x22*x31        
        +coeff[ 18]*x11        *x42*x51
        +coeff[ 19]*x12*x21*x31        
        +coeff[ 20]*x13        *x41    
        +coeff[ 21]    *x21*x32    *x52
        +coeff[ 22]        *x33    *x52
        +coeff[ 23]    *x22        *x53
        +coeff[ 24]*x11*x22    *x41*x51
        +coeff[ 25]*x11*x21    *x42*x51
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 26]*x11*x21        *x53
        +coeff[ 27]*x12    *x32*x41    
        +coeff[ 28]*x12*x21    *x41*x51
        +coeff[ 29]*x13    *x31*x41    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22*x33*x41    
        +coeff[ 32]    *x23*x32    *x51
        +coeff[ 33]*x13        *x41*x51
        +coeff[ 34]    *x23*x31    *x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 35]        *x33*x41*x52
        +coeff[ 36]            *x44*x52
        +coeff[ 37]    *x21*x32    *x53
        +coeff[ 38]            *x42*x54
        +coeff[ 39]*x11*x23*x31    *x51
        +coeff[ 40]*x11*x21*x33    *x51
        +coeff[ 41]*x11*x22    *x42*x51
        +coeff[ 42]*x11*x23        *x52
        +coeff[ 43]*x11*x21    *x42*x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 44]*x11        *x41*x54
        +coeff[ 45]*x12*x21*x32    *x51
        +coeff[ 46]*x12    *x31*x41*x52
        +coeff[ 47]*x12*x21        *x53
        +coeff[ 48]*x13*x23            
        +coeff[ 49]    *x23*x34        
        +coeff[ 50]*x13*x21*x31    *x51
        +coeff[ 51]    *x23*x31*x42*x51
        +coeff[ 52]    *x21*x34    *x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 53]    *x21*x31*x43*x52
        +coeff[ 54]*x11*x24    *x42    
        +coeff[ 55]*x11*x24*x31    *x51
        +coeff[ 56]*x11*x22*x32*x41*x51
        +coeff[ 57]*x11*x22*x31*x42*x51
        +coeff[ 58]*x11    *x31*x41*x54
        +coeff[ 59]*x12*x22    *x42*x51
        +coeff[ 60]*x12*x23        *x52
        +coeff[ 61]*x12*x21*x32    *x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 62]*x12    *x32*x41*x52
        +coeff[ 63]*x12        *x42*x53
        +coeff[ 64]*x13*x22*x31*x41    
        +coeff[ 65]    *x23*x32*x43    
        +coeff[ 66]*x13*x22    *x41*x51
        +coeff[ 67]    *x23*x33    *x52
        +coeff[ 68]    *x24*x31*x41*x52
        +coeff[ 69]    *x23*x32*x41*x52
        +coeff[ 70]    *x22*x33*x41*x52
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 71]    *x21*x34*x41*x52
        +coeff[ 72]        *x34*x42*x52
        +coeff[ 73]*x13*x21        *x53
        +coeff[ 74]    *x23*x32    *x53
        +coeff[ 75]    *x21*x34    *x53
        +coeff[ 76]        *x33*x42*x53
        +coeff[ 77]    *x21    *x44*x53
        +coeff[ 78]    *x22*x32    *x54
        +coeff[ 79]                *x51
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 80]*x11                
        +coeff[ 81]    *x21    *x41    
        +coeff[ 82]    *x21        *x51
        +coeff[ 83]    *x22        *x51
        +coeff[ 84]    *x21*x31    *x51
        +coeff[ 85]            *x41*x52
        +coeff[ 86]                *x53
        +coeff[ 87]*x11*x21*x31        
        +coeff[ 88]*x11        *x41*x51
    ;
    v_l_e_q2en_175                                =v_l_e_q2en_175                                
        +coeff[ 89]*x12        *x41    
        ;

    return v_l_e_q2en_175                                ;
}
float x_e_q2en_150                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.3289128E-03;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.31657101E-03, 0.13544279E+00, 0.57142759E-02,-0.10347365E-02,
        -0.25229382E-02,-0.18071743E-02, 0.13313341E-03, 0.19971727E-03,
        -0.93245256E-03,-0.10477019E-02,-0.22256045E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_q2en_150                                =v_x_e_q2en_150                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_150                                ;
}
float t_e_q2en_150                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5834433E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55046716E-04,-0.19682692E-02, 0.26087597E-01, 0.21418764E-02,
         0.10464276E-03,-0.29032436E-03,-0.74741448E-03,-0.72277937E-03,
         0.44840522E-05,-0.38207482E-03,-0.35275461E-03,-0.11369753E-03,
        -0.11551172E-02,-0.81737567E-03,-0.27586651E-04,-0.71599236E-03,
        -0.21158292E-04,-0.13691094E-04, 0.13202749E-03, 0.89588233E-04,
        -0.40684934E-03,-0.89329056E-03,-0.15529242E-04,-0.13795898E-04,
         0.71579248E-05,-0.14549924E-04, 0.54746113E-04,-0.89476111E-04,
        -0.53529438E-04,-0.73316087E-05, 0.18821064E-05, 0.22097661E-05,
        -0.12388924E-06,-0.72401679E-06, 0.66163757E-05,-0.56723302E-05,
        -0.98591848E-07, 0.68177246E-05, 0.83031437E-05, 0.28797511E-04,
         0.42664194E-07,-0.95750202E-05, 0.50585959E-05,-0.81028684E-05,
         0.14394232E-04,-0.23093682E-04, 0.49971618E-05, 0.55048719E-06,
        -0.15243727E-04,-0.14228632E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_t_e_q2en_150                                =v_t_e_q2en_150                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x23*x32        
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]    *x21*x31*x43    
        +coeff[ 16]*x11    *x31*x41    
    ;
    v_t_e_q2en_150                                =v_t_e_q2en_150                                
        +coeff[ 17]*x11        *x42    
        +coeff[ 18]    *x21*x31*x41*x51
        +coeff[ 19]    *x21    *x42*x51
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]*x11*x21    *x42    
    ;
    v_t_e_q2en_150                                =v_t_e_q2en_150                                
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]*x11*x22*x31*x41    
        +coeff[ 28]*x11*x22    *x42    
        +coeff[ 29]    *x23        *x53
        +coeff[ 30]*x11*x21            
        +coeff[ 31]        *x32        
        +coeff[ 32]    *x21    *x41    
        +coeff[ 33]            *x41*x51
        +coeff[ 34]*x13*x21            
    ;
    v_t_e_q2en_150                                =v_t_e_q2en_150                                
        +coeff[ 35]*x11*x23            
        +coeff[ 36]*x12*x21*x31        
        +coeff[ 37]    *x23*x31        
        +coeff[ 38]    *x23    *x41    
        +coeff[ 39]    *x23        *x51
        +coeff[ 40]*x12    *x31    *x51
        +coeff[ 41]    *x22*x31    *x51
        +coeff[ 42]*x11*x21    *x41*x51
        +coeff[ 43]    *x22    *x41*x51
    ;
    v_t_e_q2en_150                                =v_t_e_q2en_150                                
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x11*x22*x32        
        +coeff[ 46]*x11*x21*x33        
        +coeff[ 47]*x13    *x31*x41    
        +coeff[ 48]*x13        *x42    
        +coeff[ 49]*x11    *x31*x43    
        ;

    return v_t_e_q2en_150                                ;
}
float y_e_q2en_150                                (float *x,int m){
    int ncoeff= 30;
    float avdat=  0.6294393E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
        -0.16118001E-03, 0.20945686E+00, 0.13595697E+00,-0.28289275E-02,
        -0.30462993E-02,-0.28465182E-03,-0.28793799E-03, 0.15026816E-03,
        -0.33462630E-03, 0.93907271E-04,-0.40636842E-04, 0.92295108E-04,
         0.12592030E-03,-0.24790730E-04,-0.40342161E-03,-0.23888510E-04,
        -0.47044632E-04, 0.94404852E-04, 0.79663972E-04,-0.19059956E-03,
         0.10829892E-04,-0.16354730E-04, 0.70277820E-04,-0.11762779E-03,
        -0.89113477E-04,-0.36206427E-04, 0.32206030E-04,-0.84946740E-04,
         0.47553291E-04, 0.49236427E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22*x31        
        +coeff[  6]    *x24    *x41    
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_150                                =v_y_e_q2en_150                                
        +coeff[  8]    *x22    *x41    
        +coeff[  9]            *x43    
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]*x11*x21    *x43    
        +coeff[ 14]    *x24*x31        
        +coeff[ 15]        *x33        
        +coeff[ 16]*x11*x21    *x41    
    ;
    v_y_e_q2en_150                                =v_y_e_q2en_150                                
        +coeff[ 17]    *x22    *x41*x51
        +coeff[ 18]    *x22*x31    *x51
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]*x11        *x43    
        +coeff[ 21]*x11        *x42*x51
        +coeff[ 22]    *x22    *x43    
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23    *x41    
        +coeff[ 25]    *x22    *x44    
    ;
    v_y_e_q2en_150                                =v_y_e_q2en_150                                
        +coeff[ 26]        *x32*x44    
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]        *x32*x41*x52
        +coeff[ 29]*x13*x21    *x41    
        ;

    return v_y_e_q2en_150                                ;
}
float p_e_q2en_150                                (float *x,int m){
    int ncoeff= 11;
    float avdat=  0.1583529E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
        -0.27093693E-04, 0.77232518E-02, 0.25192436E-01, 0.68252842E-03,
         0.18401637E-03,-0.25374047E-03,-0.23261516E-03,-0.28028616E-03,
        -0.15868832E-03,-0.30197330E-04,-0.16829564E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_150                                =v_p_e_q2en_150                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_150                                ;
}
float l_e_q2en_150                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2338657E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24311461E-02, 0.15176744E-03,-0.36775745E-02,-0.14975212E-02,
        -0.46016802E-02,-0.57921298E-02,-0.39039608E-03, 0.50887495E-04,
         0.13499822E-03,-0.68774505E-03, 0.37727546E-03, 0.17517041E-03,
        -0.90743073E-04,-0.60437032E-03, 0.37439878E-03, 0.29065029E-03,
         0.55521721E-03,-0.32861871E-03, 0.29284219E-03, 0.65635151E-03,
        -0.86444226E-03,-0.11035412E-02, 0.43848323E-03,-0.11791795E-02,
         0.40844479E-03,-0.90148597E-03,-0.68646407E-03,-0.13172517E-03,
        -0.23844895E-03,-0.89946232E-03,-0.91174064E-03,-0.11641114E-02,
         0.70567586E-03, 0.85688877E-03,-0.58500725E-03, 0.51195611E-03,
         0.30297237E-02, 0.59334892E-04, 0.10601740E-02,-0.53715700E-03,
         0.11041913E-02, 0.22148843E-03, 0.23937985E-03,-0.12039664E-02,
        -0.21938907E-03,-0.12612379E-02, 0.90808445E-03, 0.92589151E-03,
        -0.12124120E-02, 0.85716922E-03, 0.17442652E-02, 0.67588809E-03,
         0.10541932E-02, 0.13613014E-02,-0.10484206E-02,-0.33681949E-02,
        -0.28665529E-02, 0.31742416E-02, 0.31923954E-02, 0.13985180E-02,
        -0.59585142E-04, 0.10490686E-03,-0.28303433E-04, 0.58912647E-04,
        -0.20753597E-03, 0.14727500E-03,-0.11712570E-03, 0.83720806E-04,
         0.81821760E-04, 0.76655277E-04,-0.30681799E-03, 0.21572188E-03,
         0.86700238E-04, 0.14790654E-03, 0.32779781E-03, 0.37597070E-03,
         0.38581828E-03, 0.63367584E-03, 0.53618086E-03,-0.14682190E-03,
        -0.75002073E-03, 0.25758735E-03,-0.40956945E-03,-0.21972122E-03,
        -0.41769346E-03, 0.17564115E-03, 0.15261107E-03, 0.87819382E-04,
        -0.15855365E-03, 0.16266228E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_150                                =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x41*x51
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[  8]                *x52
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]*x11    *x31*x41    
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]*x11        *x41*x51
        +coeff[ 15]*x13                
        +coeff[ 16]            *x44    
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 17]        *x32*x41*x51
        +coeff[ 18]*x13    *x31        
        +coeff[ 19]    *x22    *x43    
        +coeff[ 20]        *x32*x43    
        +coeff[ 21]    *x23*x31    *x51
        +coeff[ 22]    *x22*x31    *x52
        +coeff[ 23]    *x21*x31*x41*x52
        +coeff[ 24]    *x21    *x42*x52
        +coeff[ 25]        *x31*x42*x52
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 26]    *x21        *x54
        +coeff[ 27]*x11*x22    *x42    
        +coeff[ 28]*x11        *x44    
        +coeff[ 29]*x11*x22    *x41*x51
        +coeff[ 30]*x11*x21*x31*x41*x51
        +coeff[ 31]*x11*x22        *x52
        +coeff[ 32]*x12*x21*x31*x41    
        +coeff[ 33]*x12*x21    *x41*x51
        +coeff[ 34]*x12        *x41*x52
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 35]*x13*x21        *x51
        +coeff[ 36]    *x24    *x41*x51
        +coeff[ 37]*x13            *x52
        +coeff[ 38]*x11*x21    *x44    
        +coeff[ 39]*x12*x21*x32*x41    
        +coeff[ 40]*x12*x21*x31*x41*x51
        +coeff[ 41]*x12*x21        *x53
        +coeff[ 42]*x13*x21*x31*x41    
        +coeff[ 43]*x13*x21    *x42    
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 44]    *x23*x31*x43    
        +coeff[ 45]    *x23    *x44    
        +coeff[ 46]    *x23*x33    *x51
        +coeff[ 47]    *x23        *x54
        +coeff[ 48]*x11*x22*x31*x42*x51
        +coeff[ 49]*x11    *x33*x41*x52
        +coeff[ 50]*x11*x22        *x54
        +coeff[ 51]*x12    *x34*x41    
        +coeff[ 52]*x12*x23    *x42    
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 53]*x12*x22    *x42*x51
        +coeff[ 54]*x12*x23        *x52
        +coeff[ 55]    *x24*x32*x41*x51
        +coeff[ 56]    *x24    *x43*x51
        +coeff[ 57]    *x22*x32*x43*x51
        +coeff[ 58]    *x22*x33*x41*x52
        +coeff[ 59]    *x22*x32    *x54
        +coeff[ 60]    *x21            
        +coeff[ 61]            *x41    
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 62]*x11                
        +coeff[ 63]    *x21        *x51
        +coeff[ 64]*x11    *x31        
        +coeff[ 65]    *x21*x32        
        +coeff[ 66]            *x43    
        +coeff[ 67]        *x32    *x51
        +coeff[ 68]*x11    *x32        
        +coeff[ 69]*x11    *x31    *x51
        +coeff[ 70]*x11            *x52
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 71]*x12*x21            
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]    *x23*x31        
        +coeff[ 74]    *x22*x32        
        +coeff[ 75]        *x34        
        +coeff[ 76]    *x21*x32*x41    
        +coeff[ 77]        *x32*x42    
        +coeff[ 78]        *x31*x43    
        +coeff[ 79]    *x22*x31    *x51
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 80]    *x22    *x41*x51
        +coeff[ 81]            *x43*x51
        +coeff[ 82]        *x32    *x52
        +coeff[ 83]    *x21    *x41*x52
        +coeff[ 84]        *x31*x41*x52
        +coeff[ 85]*x11    *x33        
        +coeff[ 86]*x11*x21    *x42    
        +coeff[ 87]*x11    *x31*x41*x51
        +coeff[ 88]*x11        *x42*x51
    ;
    v_l_e_q2en_150                                =v_l_e_q2en_150                                
        +coeff[ 89]*x11*x21        *x52
        ;

    return v_l_e_q2en_150                                ;
}
float x_e_q2en_125                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.3264678E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.33763863E-03, 0.13647799E+00, 0.56723421E-02,-0.10014832E-02,
        -0.24852343E-02,-0.17786070E-02, 0.12480555E-03, 0.19685682E-03,
        -0.91798365E-03,-0.10291067E-02,-0.22713281E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_q2en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_q2en_125                                =v_x_e_q2en_125                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        ;

    return v_x_e_q2en_125                                ;
}
float t_e_q2en_125                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.5341657E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55501700E-04,-0.19516471E-02, 0.26652751E-01, 0.21243903E-02,
         0.10178298E-03,-0.73421083E-03,-0.72416989E-03,-0.38119339E-03,
        -0.28765452E-03,-0.34795995E-03,-0.11362304E-03,-0.27063350E-04,
        -0.11422179E-02,-0.80017297E-03,-0.74532256E-03,-0.28300379E-04,
        -0.18675830E-04, 0.11464971E-03, 0.73150870E-04,-0.79612191E-05,
        -0.42870294E-03,-0.93851541E-03, 0.18708852E-04, 0.16816595E-05,
        -0.69122511E-05,-0.69076391E-05,-0.25030135E-04,-0.28055696E-04,
         0.43037833E-07, 0.80334638E-06,-0.25112540E-05, 0.19980209E-05,
         0.39288838E-06,-0.45161742E-05, 0.34164348E-05, 0.12919210E-04,
         0.68794188E-05, 0.49487771E-05,-0.41149447E-05, 0.65353870E-05,
         0.17548442E-04,-0.33545371E-05,-0.76396063E-05, 0.33464203E-04,
         0.10335563E-04, 0.11839220E-04,-0.43110125E-04,-0.77381068E-04,
         0.14396506E-04,-0.54115291E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q2en_125                                =v_t_e_q2en_125                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]*x11*x22            
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        +coeff[ 14]    *x21*x31*x43    
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q2en_125                                =v_t_e_q2en_125                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]*x13    *x32        
        +coeff[ 20]    *x21*x33*x41    
        +coeff[ 21]    *x21*x32*x42    
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x13                
        +coeff[ 24]*x11    *x32        
        +coeff[ 25]*x11            *x52
    ;
    v_t_e_q2en_125                                =v_t_e_q2en_125                                
        +coeff[ 26]*x12*x21*x31    *x51
        +coeff[ 27]*x11*x21        *x53
        +coeff[ 28]    *x21*x32    *x53
        +coeff[ 29]                *x51
        +coeff[ 30]    *x22            
        +coeff[ 31]        *x32        
        +coeff[ 32]*x12*x21            
        +coeff[ 33]    *x22*x31        
        +coeff[ 34]        *x31*x42    
    ;
    v_t_e_q2en_125                                =v_t_e_q2en_125                                
        +coeff[ 35]*x11*x21        *x51
        +coeff[ 36]    *x21*x31    *x51
        +coeff[ 37]*x12*x21    *x41    
        +coeff[ 38]*x12        *x42    
        +coeff[ 39]*x11*x22        *x51
        +coeff[ 40]    *x23        *x51
        +coeff[ 41]*x12    *x31    *x51
        +coeff[ 42]*x11*x21*x31    *x51
        +coeff[ 43]    *x21*x32    *x51
    ;
    v_t_e_q2en_125                                =v_t_e_q2en_125                                
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x13*x22            
        +coeff[ 46]*x11*x22*x32        
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]*x12*x21    *x42    
        +coeff[ 49]*x11*x22    *x42    
        ;

    return v_t_e_q2en_125                                ;
}
float y_e_q2en_125                                (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.4542531E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.47517387E-03, 0.20891777E+00, 0.13531011E+00,-0.27890082E-02,
        -0.30246018E-02, 0.15261165E-03,-0.34338818E-03,-0.28633984E-03,
         0.99004799E-04,-0.53041593E-04, 0.11228255E-03, 0.12155125E-03,
         0.54706156E-05,-0.38529641E-03,-0.30843825E-04,-0.73315234E-04,
         0.93308518E-04,-0.25334518E-03, 0.34622019E-04,-0.13758809E-04,
         0.71042487E-04,-0.12966737E-03,-0.21997881E-04,-0.96671814E-04,
        -0.63055326E-04,-0.21939493E-04,-0.26228707E-04, 0.51611856E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]        *x31*x42    
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x22*x31        
    ;
    v_y_e_q2en_125                                =v_y_e_q2en_125                                
        +coeff[  8]            *x43    
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_y_e_q2en_125                                =v_y_e_q2en_125                                
        +coeff[ 17]    *x24    *x41    
        +coeff[ 18]    *x22    *x43*x51
        +coeff[ 19]*x12        *x41    
        +coeff[ 20]    *x22    *x41*x51
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]*x12        *x41*x51
        +coeff[ 23]    *x22*x33        
        +coeff[ 24]*x11*x23*x31        
        +coeff[ 25]*x12        *x42*x51
    ;
    v_y_e_q2en_125                                =v_y_e_q2en_125                                
        +coeff[ 26]    *x23    *x42*x51
        +coeff[ 27]    *x22    *x45    
        ;

    return v_y_e_q2en_125                                ;
}
float p_e_q2en_125                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.5994714E-04;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.61101520E-04, 0.75587365E-02, 0.25025763E-01, 0.68647217E-03,
         0.18720909E-03,-0.24568377E-03,-0.22633943E-03,-0.27861251E-03,
        -0.15572320E-03,-0.30411018E-04,-0.16913268E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_125                                =v_p_e_q2en_125                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_125                                ;
}
float l_e_q2en_125                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2385043E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.22901359E-02,-0.11080015E-03,-0.36324472E-02,-0.79234148E-03,
        -0.42182547E-02,-0.50887843E-02, 0.42661824E-03, 0.50212158E-03,
        -0.19854878E-03, 0.21258388E-03,-0.26137210E-03, 0.65404066E-03,
        -0.19761721E-03, 0.14867779E-03,-0.73082477E-03,-0.48534109E-03,
        -0.56813628E-03,-0.45915425E-03,-0.10754749E-03, 0.16119167E-03,
         0.26567356E-03,-0.48250833E-03,-0.32859971E-03, 0.35262271E-03,
        -0.26387288E-03, 0.24830797E-02, 0.16692362E-04, 0.28102155E-03,
         0.11404182E-02,-0.82401239E-03,-0.14055519E-03,-0.32454473E-03,
         0.36205639E-03,-0.44108403E-03,-0.66921499E-03, 0.39077250E-03,
        -0.68944762E-03,-0.75020647E-03,-0.44861130E-03,-0.79574436E-03,
         0.37382881E-03, 0.39439212E-03, 0.65935723E-03, 0.90039044E-03,
         0.17683663E-02, 0.16771753E-02, 0.52495371E-03,-0.60511299E-03,
        -0.80865587E-03,-0.12351726E-02,-0.57708431E-03, 0.12319913E-02,
         0.10820059E-02,-0.10441206E-02, 0.62204554E-03,-0.77716517E-03,
        -0.61266526E-03,-0.20421136E-02,-0.16136079E-02, 0.58727886E-03,
        -0.52553555E-03,-0.22875275E-02, 0.15200718E-02, 0.22088103E-02,
        -0.12074175E-02, 0.82194601E-03, 0.19710979E-02,-0.84320339E-03,
        -0.32047869E-02,-0.25941748E-02,-0.46276112E-03, 0.31358343E-07,
         0.13116334E-03,-0.11493242E-03, 0.81847815E-04,-0.68475092E-04,
        -0.11006273E-03, 0.19225734E-03, 0.25954616E-03,-0.13278547E-03,
         0.13596198E-03, 0.55330594E-04,-0.85374850E-04, 0.19053800E-03,
         0.25217357E-03, 0.99708319E-04, 0.40577186E-03, 0.20882994E-03,
         0.95777250E-04, 0.18466095E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_125                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x12            *x53
        +coeff[  7]        *x31*x43*x53
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[  8]*x11            *x51
        +coeff[  9]*x12                
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]        *x32*x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]*x11*x21        *x51
        +coeff[ 15]        *x32    *x52
        +coeff[ 16]        *x31*x41*x52
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 17]*x11*x21*x32        
        +coeff[ 18]*x11    *x33        
        +coeff[ 19]*x11*x22    *x41    
        +coeff[ 20]*x11*x21    *x41*x51
        +coeff[ 21]*x12    *x32        
        +coeff[ 22]*x12    *x31    *x51
        +coeff[ 23]*x13*x21            
        +coeff[ 24]*x13        *x41    
        +coeff[ 25]        *x33*x42    
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]*x13            *x51
        +coeff[ 28]*x11*x23        *x51
        +coeff[ 29]*x11*x22*x31    *x51
        +coeff[ 30]*x11    *x33    *x51
        +coeff[ 31]*x11    *x31*x42*x51
        +coeff[ 32]*x11        *x43*x51
        +coeff[ 33]*x12    *x33        
        +coeff[ 34]*x12*x22        *x51
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 35]*x12        *x42*x51
        +coeff[ 36]*x12        *x41*x52
        +coeff[ 37]    *x23*x33        
        +coeff[ 38]    *x21*x34*x41    
        +coeff[ 39]        *x34*x41*x51
        +coeff[ 40]    *x24        *x52
        +coeff[ 41]    *x23        *x53
        +coeff[ 42]*x11    *x34*x41    
        +coeff[ 43]*x11*x22*x31*x42    
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 44]*x11*x23*x31    *x51
        +coeff[ 45]*x11*x23    *x41*x51
        +coeff[ 46]*x11*x22*x31*x41*x51
        +coeff[ 47]*x11*x21*x31*x42*x51
        +coeff[ 48]*x11*x22        *x53
        +coeff[ 49]*x11*x21    *x41*x53
        +coeff[ 50]*x12    *x33*x41    
        +coeff[ 51]*x12    *x32*x41*x51
        +coeff[ 52]*x12*x21    *x42*x51
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 53]        *x33*x44    
        +coeff[ 54]*x13*x22        *x51
        +coeff[ 55]*x13*x21*x31    *x51
        +coeff[ 56]    *x23*x33    *x51
        +coeff[ 57]    *x24*x31*x41*x51
        +coeff[ 58]    *x22*x32*x42*x51
        +coeff[ 59]    *x23    *x43*x51
        +coeff[ 60]*x13*x21        *x52
        +coeff[ 61]    *x21    *x44*x52
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 62]    *x21    *x42*x54
        +coeff[ 63]*x11    *x33*x42*x51
        +coeff[ 64]*x12    *x33*x42    
        +coeff[ 65]*x12*x22*x31    *x52
        +coeff[ 66]    *x23*x33*x41*x51
        +coeff[ 67]*x13    *x31    *x53
        +coeff[ 68]    *x21*x33*x41*x53
        +coeff[ 69]    *x21*x32*x42*x53
        +coeff[ 70]            *x44*x54
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 71]*x11                
        +coeff[ 72]    *x21*x31        
        +coeff[ 73]    *x21        *x51
        +coeff[ 74]        *x31    *x51
        +coeff[ 75]        *x33        
        +coeff[ 76]    *x22    *x41    
        +coeff[ 77]    *x21    *x42    
        +coeff[ 78]        *x31*x41*x51
        +coeff[ 79]        *x31    *x52
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 80]            *x41*x52
        +coeff[ 81]                *x53
        +coeff[ 82]*x11*x22            
        +coeff[ 83]*x11    *x32        
        +coeff[ 84]*x11    *x31*x41    
        +coeff[ 85]*x11        *x42    
        +coeff[ 86]*x11    *x31    *x51
        +coeff[ 87]*x12    *x31        
        +coeff[ 88]*x12        *x41    
    ;
    v_l_e_q2en_125                                =v_l_e_q2en_125                                
        +coeff[ 89]    *x22*x31*x41    
        ;

    return v_l_e_q2en_125                                ;
}
float x_e_q2en_100                                (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.1011002E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.10489076E-02, 0.13799560E+00, 0.56126621E-02,-0.95019367E-03,
         0.19794487E-03,-0.90948428E-03,-0.24370961E-02,-0.17547590E-02,
        -0.46463961E-05,-0.58358441E-04, 0.10677260E-03,-0.97979046E-03,
        -0.22096490E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_x_e_q2en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x21        *x51
        +coeff[  3]*x11                
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_q2en_100                                =v_x_e_q2en_100                                
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x54
        ;

    return v_x_e_q2en_100                                ;
}
float t_e_q2en_100                                (float *x,int m){
    int ncoeff= 50;
    float avdat= -0.1971384E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.20507122E-03,-0.19231951E-02, 0.27495835E-01, 0.20893214E-02,
         0.10372321E-03,-0.72833843E-03,-0.71171753E-03,-0.38931350E-03,
        -0.28041069E-03,-0.33762364E-03,-0.11449202E-03,-0.11349235E-02,
        -0.79655228E-03,-0.68685703E-03,-0.28054381E-04,-0.39777336E-04,
        -0.22816408E-04, 0.12571119E-03, 0.83416584E-04,-0.39179233E-03,
        -0.85129956E-03, 0.38762249E-04, 0.32013981E-04,-0.19244344E-04,
         0.16623468E-04, 0.22317970E-06, 0.13416477E-04,-0.52734536E-05,
        -0.64209175E-05, 0.22166366E-05,-0.41635444E-05,-0.19790104E-04,
         0.90627991E-05,-0.60528760E-05,-0.71702270E-05, 0.18243296E-04,
        -0.85157299E-05, 0.25139991E-04,-0.66534371E-05,-0.19088764E-04,
         0.60418147E-05,-0.14184337E-04,-0.12376614E-04, 0.62376512E-05,
         0.51551178E-05,-0.11486308E-04, 0.45248721E-05,-0.29753593E-04,
        -0.75027756E-05,-0.30005160E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_q2en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x21*x31*x41    
        +coeff[  6]    *x21    *x42    
        +coeff[  7]    *x23*x32        
    ;
    v_t_e_q2en_100                                =v_t_e_q2en_100                                
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        +coeff[ 13]    *x21*x31*x43    
        +coeff[ 14]*x11*x22            
        +coeff[ 15]*x11    *x31*x41    
        +coeff[ 16]*x11        *x42    
    ;
    v_t_e_q2en_100                                =v_t_e_q2en_100                                
        +coeff[ 17]    *x21*x31*x41*x51
        +coeff[ 18]    *x21    *x42*x51
        +coeff[ 19]    *x21*x33*x41    
        +coeff[ 20]    *x21*x32*x42    
        +coeff[ 21]*x12*x21*x32    *x51
        +coeff[ 22]    *x23*x32    *x51
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x23        *x52
        +coeff[ 25]        *x31        
    ;
    v_t_e_q2en_100                                =v_t_e_q2en_100                                
        +coeff[ 26]*x11*x21            
        +coeff[ 27]*x11        *x41    
        +coeff[ 28]    *x22*x31        
        +coeff[ 29]*x12        *x41    
        +coeff[ 30]*x11            *x52
        +coeff[ 31]*x11*x23            
        +coeff[ 32]*x11    *x32*x41    
        +coeff[ 33]*x13            *x51
        +coeff[ 34]*x12*x21        *x51
    ;
    v_t_e_q2en_100                                =v_t_e_q2en_100                                
        +coeff[ 35]    *x23        *x51
        +coeff[ 36]    *x22*x31    *x51
        +coeff[ 37]    *x21*x32    *x51
        +coeff[ 38]    *x22    *x41*x51
        +coeff[ 39]*x11*x21        *x52
        +coeff[ 40]*x11            *x53
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]*x11*x22    *x42    
        +coeff[ 43]    *x22    *x43    
    ;
    v_t_e_q2en_100                                =v_t_e_q2en_100                                
        +coeff[ 44]*x11*x23        *x51
        +coeff[ 45]    *x22*x31*x41*x51
        +coeff[ 46]    *x21*x31    *x53
        +coeff[ 47]*x12*x22*x31*x41    
        +coeff[ 48]    *x22*x33*x41    
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_q2en_100                                ;
}
float y_e_q2en_100                                (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1658222E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.15234818E-02, 0.20799406E+00, 0.13444394E+00,-0.27592874E-02,
        -0.29806113E-02,-0.34761551E-03,-0.26494882E-03, 0.15483398E-03,
         0.30577907E-04,-0.42086656E-04, 0.12899436E-03, 0.11336238E-03,
        -0.36985643E-04,-0.36957691E-03,-0.17643499E-04,-0.73972922E-04,
         0.87053435E-04, 0.79233090E-04,-0.22562465E-03, 0.97192242E-04,
         0.11588621E-04,-0.25052771E-05,-0.21279384E-04,-0.14496844E-03,
        -0.66833745E-04, 0.18968789E-04,-0.12127167E-03,-0.59531510E-04,
        -0.63003215E-04, 0.30135099E-04, 0.62622261E-04, 0.32650933E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_q2en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_q2en_100                                =v_y_e_q2en_100                                
        +coeff[  8]            *x45    
        +coeff[  9]*x11*x21*x31        
        +coeff[ 10]            *x41*x52
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]*x11*x21    *x43    
        +coeff[ 13]    *x24*x31        
        +coeff[ 14]        *x33        
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x22    *x41*x51
    ;
    v_y_e_q2en_100                                =v_y_e_q2en_100                                
        +coeff[ 17]    *x22*x31    *x51
        +coeff[ 18]    *x24    *x41    
        +coeff[ 19]            *x43    
        +coeff[ 20]            *x44*x51
        +coeff[ 21]        *x33*x42    
        +coeff[ 22]    *x21    *x43*x51
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]*x11*x21*x31*x42    
        +coeff[ 25]*x11    *x32*x42    
    ;
    v_y_e_q2en_100                                =v_y_e_q2en_100                                
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]            *x43*x52
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]    *x21*x31*x41*x52
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]*x11    *x31*x43*x51
        ;

    return v_y_e_q2en_100                                ;
}
float p_e_q2en_100                                (float *x,int m){
    int ncoeff= 11;
    float avdat= -0.1687167E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 12]={
         0.15354433E-03, 0.73215491E-02, 0.24770768E-01, 0.69097383E-03,
         0.19095073E-03,-0.24164509E-03,-0.21601406E-03,-0.27096021E-03,
        -0.15598258E-03,-0.19462521E-04,-0.16466828E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;

//                 function

    float v_p_e_q2en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x24*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_p_e_q2en_100                                =v_p_e_q2en_100                                
        +coeff[  8]            *x43    
        +coeff[  9]    *x22*x32*x41    
        +coeff[ 10]        *x34*x41    
        ;

    return v_p_e_q2en_100                                ;
}
float l_e_q2en_100                                (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.2378378E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.24180145E-02,-0.35926523E-02,-0.12319876E-02,-0.42169620E-02,
        -0.51589496E-02,-0.23078320E-03, 0.26540182E-03, 0.14475998E-03,
        -0.12686709E-03, 0.41873468E-03,-0.91796269E-03, 0.14788740E-03,
         0.10817597E-03,-0.25585960E-03,-0.60226273E-03, 0.37959751E-03,
         0.76320540E-03,-0.28691863E-03,-0.44984132E-03, 0.52759817E-04,
        -0.21208422E-02,-0.15890439E-02,-0.16111716E-03,-0.38749978E-03,
         0.58237929E-03,-0.13320082E-02,-0.18800766E-02, 0.95444691E-03,
        -0.54648746E-03, 0.26607795E-02, 0.10093724E-02, 0.21538383E-02,
        -0.26952257E-03, 0.13422364E-03,-0.84857026E-03, 0.42641794E-03,
        -0.41496917E-03,-0.41904350E-03,-0.70497318E-03, 0.50274463E-03,
         0.11678162E-02, 0.22063823E-02, 0.14664715E-02,-0.20603852E-02,
         0.21147766E-03,-0.97590906E-03,-0.51091902E-03, 0.59640879E-03,
        -0.69506437E-03,-0.29487140E-03, 0.24938243E-02, 0.12098115E-02,
         0.55270502E-03, 0.31552222E-02,-0.19214362E-02,-0.30783718E-02,
         0.19731572E-03,-0.27038821E-02,-0.16095473E-02, 0.81130874E-03,
         0.17846978E-02,-0.15792690E-02, 0.76427369E-03,-0.10104250E-02,
         0.87321398E-03, 0.92526322E-03, 0.31744582E-02,-0.21655203E-02,
         0.17476889E-02,-0.51460993E-04,-0.12129557E-03, 0.26383789E-03,
         0.39984923E-04,-0.15413351E-03, 0.98960751E-04,-0.48894767E-03,
         0.26926090E-03, 0.91206864E-04,-0.18318236E-03, 0.92032680E-03,
        -0.55726414E-04,-0.46168640E-03,-0.11326348E-03, 0.24158326E-03,
         0.11816664E-03,-0.38598900E-03, 0.13800969E-03,-0.42699679E-03,
        -0.33389695E-03, 0.28862173E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_q2en_100                                =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]    *x21*x31        
        +coeff[  6]*x11*x21            
        +coeff[  7]*x11    *x31        
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[  8]            *x42*x51
        +coeff[  9]    *x21        *x52
        +coeff[ 10]*x11*x21    *x41    
        +coeff[ 11]*x12        *x41    
        +coeff[ 12]*x13                
        +coeff[ 13]        *x33*x41    
        +coeff[ 14]            *x44    
        +coeff[ 15]    *x21    *x42*x51
        +coeff[ 16]            *x42*x52
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 17]                *x54
        +coeff[ 18]*x11*x21*x32        
        +coeff[ 19]*x11    *x33        
        +coeff[ 20]*x11    *x32*x41    
        +coeff[ 21]*x11    *x31*x42    
        +coeff[ 22]*x12*x21        *x51
        +coeff[ 23]    *x23        *x52
        +coeff[ 24]        *x31*x41*x53
        +coeff[ 25]*x11*x24            
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 26]*x11*x23*x31        
        +coeff[ 27]*x11*x21*x32*x41    
        +coeff[ 28]*x11*x21*x31*x42    
        +coeff[ 29]*x11    *x31*x42*x51
        +coeff[ 30]*x11*x22        *x52
        +coeff[ 31]*x11*x21*x31    *x52
        +coeff[ 32]*x11        *x42*x52
        +coeff[ 33]*x12    *x32    *x51
        +coeff[ 34]    *x23*x32*x41    
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 35]    *x23    *x43    
        +coeff[ 36]    *x22*x31*x42*x51
        +coeff[ 37]*x11*x24*x31        
        +coeff[ 38]*x12*x22*x32        
        +coeff[ 39]*x12*x22        *x52
        +coeff[ 40]*x12*x21    *x41*x52
        +coeff[ 41]*x13    *x32*x41    
        +coeff[ 42]*x13    *x31*x42    
        +coeff[ 43]    *x21*x34*x42    
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 44]    *x23*x31*x43    
        +coeff[ 45]    *x23*x33    *x51
        +coeff[ 46]    *x21*x33*x42*x51
        +coeff[ 47]        *x32*x44*x51
        +coeff[ 48]    *x23*x31*x41*x52
        +coeff[ 49]        *x34    *x53
        +coeff[ 50]    *x21*x31*x42*x53
        +coeff[ 51]    *x21    *x43*x53
        +coeff[ 52]            *x44*x53
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 53]*x11*x23*x33        
        +coeff[ 54]*x11    *x31*x44*x51
        +coeff[ 55]*x11*x21*x33    *x52
        +coeff[ 56]*x11    *x32*x41*x53
        +coeff[ 57]*x11    *x31*x42*x53
        +coeff[ 58]*x11*x22        *x54
        +coeff[ 59]*x12*x21*x34        
        +coeff[ 60]*x12*x22*x32    *x51
        +coeff[ 61]*x12*x22    *x42*x51
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 62]*x12        *x44*x51
        +coeff[ 63]*x12*x21    *x42*x52
        +coeff[ 64]*x13*x23    *x41    
        +coeff[ 65]*x13        *x44    
        +coeff[ 66]    *x23*x32*x42*x51
        +coeff[ 67]    *x21*x34*x42*x51
        +coeff[ 68]    *x22*x31*x42*x53
        +coeff[ 69]    *x21    *x41    
        +coeff[ 70]    *x21        *x51
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 71]            *x41*x51
        +coeff[ 72]*x11            *x51
        +coeff[ 73]    *x23            
        +coeff[ 74]    *x21*x32        
        +coeff[ 75]    *x21*x31*x41    
        +coeff[ 76]    *x21    *x42    
        +coeff[ 77]        *x31*x42    
        +coeff[ 78]    *x21    *x41*x51
        +coeff[ 79]*x11*x22            
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 80]*x11    *x32        
        +coeff[ 81]*x11        *x42    
        +coeff[ 82]*x12*x21            
        +coeff[ 83]        *x34        
        +coeff[ 84]    *x22    *x42    
        +coeff[ 85]        *x31*x43    
        +coeff[ 86]        *x32*x41*x51
        +coeff[ 87]            *x43*x51
        +coeff[ 88]    *x21    *x41*x52
    ;
    v_l_e_q2en_100                                =v_l_e_q2en_100                                
        +coeff[ 89]        *x31*x41*x52
        ;

    return v_l_e_q2en_100                                ;
}
