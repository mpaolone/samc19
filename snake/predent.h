float x_e_predent_1200                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067178E+01;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.71990426E-03,-0.30962070E-02, 0.90861186E-01, 0.43786434E-02,
        -0.22700955E-02,-0.17361443E-02,-0.47435585E-03, 0.24546069E-03,
        -0.71303401E-03,-0.86020416E-03,-0.27344972E-03,-0.17634651E-02,
        -0.14517170E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_1200                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_1200                             =v_x_e_predent_1200                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_1200                             ;
}
float t_e_predent_1200                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3775888E+01;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.44119429E-01,-0.37427522E-01, 0.81030858E+00, 0.16729476E+00,
         0.37503287E-01,-0.15103211E-01,-0.13030463E-03, 0.26520772E-01,
         0.16360315E-01,-0.18240858E-01,-0.19236550E-01,-0.98119797E-02,
         0.27513711E-02,-0.56490940E-02,-0.74773994E-02, 0.98414300E-03,
         0.53766876E-03,-0.30269944E-02,-0.18641338E-01,-0.14496124E-01,
         0.50793546E-02,-0.81935787E-03,-0.31674726E-03, 0.26153505E-03,
        -0.14964557E-02,-0.58408929E-02,-0.31122237E-01,-0.24250388E-01,
         0.39646905E-03, 0.37796164E-03, 0.27940830E-02, 0.19766057E-02,
        -0.19941509E-01,-0.16949827E-01, 0.15824402E-02, 0.14667160E-04,
         0.32342659E-03,-0.67500822E-03,-0.51506615E-03,-0.21759476E-03,
        -0.11525451E-02,-0.77285100E-03, 0.37855672E-03,-0.87894360E-02,
         0.77638819E-04,-0.15692433E-03, 0.94390940E-04,-0.89986570E-03,
        -0.25335542E-03, 0.13400904E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_1200                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]                *x51
        +coeff[  7]    *x23            
    ;
    v_t_e_predent_1200                             =v_t_e_predent_1200                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_1200                             =v_t_e_predent_1200                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x32*x42    
        +coeff[ 22]    *x22*x32    *x52
        +coeff[ 23]        *x32        
        +coeff[ 24]*x11*x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_predent_1200                             =v_t_e_predent_1200                             
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_t_e_predent_1200                             =v_t_e_predent_1200                             
        +coeff[ 35]                *x52
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]*x11            *x52
        +coeff[ 40]        *x31*x43    
        +coeff[ 41]    *x22        *x52
        +coeff[ 42]    *x21        *x53
        +coeff[ 43]    *x21*x33*x41    
    ;
    v_t_e_predent_1200                             =v_t_e_predent_1200                             
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]    *x23    *x41    
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]    *x22*x31    *x51
        ;

    return v_t_e_predent_1200                             ;
}
float y_e_predent_1200                             (float *x,int m){
    int ncoeff= 41;
    float avdat= -0.2317579E-03;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.16037514E-03, 0.14799209E+00, 0.56598403E-01, 0.88484045E-02,
         0.38859574E-02,-0.18410985E-02,-0.11468959E-02,-0.18039235E-02,
        -0.13442365E-03,-0.45418879E-03,-0.11098559E-02,-0.50877611E-03,
        -0.31378050E-03,-0.10296298E-02,-0.25219456E-03,-0.27464377E-02,
        -0.10164408E-02,-0.78577595E-03,-0.52058072E-04,-0.19799751E-03,
        -0.95670504E-04,-0.49138656E-04, 0.20481208E-03,-0.98499922E-05,
         0.23281400E-03, 0.15779014E-03,-0.15187274E-02,-0.15874509E-02,
        -0.40112400E-05,-0.28512939E-04,-0.40771050E-03, 0.42306321E-04,
        -0.33617593E-03,-0.37506397E-05, 0.11999447E-03, 0.19833476E-04,
        -0.24076657E-03,-0.78553647E-04, 0.31801370E-04, 0.24160207E-04,
        -0.40962590E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_1200                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_1200                             =v_y_e_predent_1200                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_1200                             =v_y_e_predent_1200                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_1200                             =v_y_e_predent_1200                             
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]            *x45*x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]        *x31*x44    
        +coeff[ 31]        *x33    *x51
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]                *x51
        +coeff[ 34]            *x43*x51
    ;
    v_y_e_predent_1200                             =v_y_e_predent_1200                             
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]        *x33*x42    
        +coeff[ 37]        *x34*x41    
        +coeff[ 38]            *x41*x53
        +coeff[ 39]        *x31    *x53
        +coeff[ 40]*x11*x23*x31        
        ;

    return v_y_e_predent_1200                             ;
}
float p_e_predent_1200                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1942851E-04;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
         0.18403038E-04,-0.71599849E-01,-0.76211073E-01,-0.14289671E-01,
        -0.15251650E-01, 0.66595185E-02, 0.11503234E-01,-0.54660728E-02,
        -0.19888107E-05, 0.71151229E-03,-0.42567477E-02, 0.76636224E-03,
         0.14951949E-02,-0.21720137E-02,-0.13066530E-02, 0.58975077E-03,
        -0.57335588E-03, 0.15699925E-04,-0.58644958E-03,-0.11975148E-02,
         0.34620755E-03,-0.75530645E-03, 0.41023505E-03,-0.97316218E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_1200                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_1200                             =v_p_e_predent_1200                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_1200                             =v_p_e_predent_1200                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_1200                             ;
}
float l_e_predent_1200                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4432700E-02;
    float xmin[10]={
        -0.39996E-02,-0.60058E-01,-0.53973E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39977E-02, 0.60060E-01, 0.53976E-01, 0.30021E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44053597E-02,-0.89880470E-02,-0.18227141E-02,-0.57897973E-02,
        -0.58942721E-02, 0.54395036E-03,-0.80302916E-03, 0.16673429E-03,
         0.13653180E-03, 0.65059241E-04,-0.36606722E-03,-0.51466684E-03,
         0.10584592E-02,-0.18819714E-03, 0.52857643E-03, 0.56079833E-03,
        -0.37634102E-03, 0.65174873E-03,-0.20507090E-04,-0.14483995E-02,
        -0.43220576E-02, 0.84678852E-03,-0.43649337E-03,-0.30590585E-02,
        -0.89701469E-04,-0.79870207E-03,-0.70908503E-03, 0.19558250E-03,
         0.42352200E-03,-0.12640231E-02,-0.12891878E-02, 0.93290320E-03,
        -0.10749073E-02, 0.16026763E-03,-0.40809222E-03,-0.26476278E-03,
        -0.84130828E-04,-0.75502758E-03, 0.78015612E-04, 0.42793350E-03,
        -0.22415959E-03, 0.30030796E-03,-0.10804276E-02,-0.46504429E-03,
        -0.12041888E-02,-0.43226650E-03, 0.19531746E-02, 0.70732378E-03,
        -0.15772750E-03, 0.10477483E-03,-0.71738631E-03, 0.29348271E-03,
         0.30451207E-04,-0.26126544E-03,-0.46990986E-03, 0.36127047E-03,
         0.84027229E-03,-0.38041195E-03, 0.68145164E-03,-0.52392745E-03,
        -0.25065560E-03, 0.49448921E-03, 0.68392686E-03,-0.81263461E-04,
         0.71424880E-03,-0.38283065E-03, 0.64740580E-03,-0.26115362E-03,
         0.34174285E-03,-0.42791758E-04,-0.16516421E-03,-0.82033681E-03,
         0.72529278E-03,-0.67048456E-03, 0.22053500E-02, 0.16272466E-02,
         0.62192010E-03,-0.34886601E-03,-0.60493313E-03,-0.28593652E-02,
         0.12291284E-03, 0.55019226E-03, 0.39515848E-03,-0.28393290E-03,
        -0.33049448E-03,-0.80545462E-03, 0.10651011E-02, 0.58263546E-03,
        -0.96761720E-03, 0.80236990E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_1200                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]        *x31        
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[  8]                *x51
        +coeff[  9]*x11            *x51
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]        *x32*x41*x51
        +coeff[ 15]*x11*x21*x32        
        +coeff[ 16]*x11*x21        *x52
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 17]*x12    *x31*x41    
        +coeff[ 18]    *x21*x33*x41    
        +coeff[ 19]        *x33*x41*x51
        +coeff[ 20]        *x31*x44*x51
        +coeff[ 21]*x11*x23*x32        
        +coeff[ 22]*x11    *x34*x41    
        +coeff[ 23]*x11*x21*x31*x42*x51
        +coeff[ 24]*x11    *x32*x42*x51
        +coeff[ 25]*x11*x22*x31    *x52
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 26]*x11*x21    *x41*x53
        +coeff[ 27]*x11        *x41*x54
        +coeff[ 28]*x12*x24            
        +coeff[ 29]*x12    *x32*x41*x51
        +coeff[ 30]    *x21*x34*x42    
        +coeff[ 31]    *x23    *x43*x51
        +coeff[ 32]*x12*x22*x32    *x51
        +coeff[ 33]*x11        *x41    
        +coeff[ 34]    *x21*x31*x41    
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 35]    *x21    *x42    
        +coeff[ 36]            *x43    
        +coeff[ 37]*x11*x22            
        +coeff[ 38]*x11    *x32        
        +coeff[ 39]*x11    *x31*x41    
        +coeff[ 40]    *x24            
        +coeff[ 41]    *x23*x31        
        +coeff[ 42]    *x21*x32*x41    
        +coeff[ 43]        *x33*x41    
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 44]    *x21*x31*x42    
        +coeff[ 45]            *x44    
        +coeff[ 46]        *x31*x42*x51
        +coeff[ 47]*x11*x22*x31        
        +coeff[ 48]*x11    *x33        
        +coeff[ 49]*x11*x22    *x41    
        +coeff[ 50]*x11    *x32*x41    
        +coeff[ 51]*x11*x22        *x51
        +coeff[ 52]*x11*x21*x31    *x51
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 53]*x12    *x31    *x51
        +coeff[ 54]    *x23    *x42    
        +coeff[ 55]    *x22    *x43    
        +coeff[ 56]    *x21    *x44    
        +coeff[ 57]    *x22*x31*x41*x51
        +coeff[ 58]    *x22*x31    *x52
        +coeff[ 59]        *x33    *x52
        +coeff[ 60]            *x43*x52
        +coeff[ 61]    *x22        *x53
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 62]*x11    *x34        
        +coeff[ 63]*x11    *x33*x41    
        +coeff[ 64]*x11    *x32*x42    
        +coeff[ 65]*x11*x21*x32    *x51
        +coeff[ 66]*x11*x22        *x52
        +coeff[ 67]*x11    *x32    *x52
        +coeff[ 68]*x12*x23            
        +coeff[ 69]*x12*x21*x31*x41    
        +coeff[ 70]*x12    *x32*x41    
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 71]*x12*x21    *x42    
        +coeff[ 72]*x13*x22            
        +coeff[ 73]*x13    *x32        
        +coeff[ 74]    *x23*x32*x41    
        +coeff[ 75]    *x23*x31*x42    
        +coeff[ 76]    *x24    *x41*x51
        +coeff[ 77]        *x33*x42*x51
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]        *x32*x43*x51
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 80]    *x22*x31*x41*x52
        +coeff[ 81]        *x33*x41*x52
        +coeff[ 82]        *x32    *x54
        +coeff[ 83]            *x42*x54
        +coeff[ 84]*x11*x22*x33        
        +coeff[ 85]*x11*x21*x34        
        +coeff[ 86]*x11*x22*x32*x41    
        +coeff[ 87]*x11*x23*x31    *x51
        +coeff[ 88]*x11*x21*x32*x41*x51
    ;
    v_l_e_predent_1200                             =v_l_e_predent_1200                             
        +coeff[ 89]*x11    *x31*x43*x51
        ;

    return v_l_e_predent_1200                             ;
}
float x_e_predent_1100                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067076E+01;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.60680148E-03,-0.30971058E-02, 0.90882435E-01, 0.43776808E-02,
        -0.22591990E-02,-0.17454616E-02,-0.51576376E-03, 0.25302247E-03,
        -0.69978874E-03,-0.83557819E-03,-0.26994490E-03,-0.17754602E-02,
        -0.14366183E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_1100                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_1100                             =v_x_e_predent_1100                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_1100                             ;
}
float t_e_predent_1100                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3776664E+01;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.44971991E-01,-0.37432026E-01, 0.81063837E+00, 0.16727670E+00,
         0.37322361E-01,-0.15089812E-01, 0.26626216E-01,-0.11836946E-03,
         0.16342599E-01,-0.17894700E-01,-0.19323060E-01,-0.10019433E-01,
         0.27266094E-02,-0.55602458E-02,-0.73287738E-02, 0.47287255E-03,
         0.53096010E-03,-0.29896095E-02,-0.17744541E-01,-0.14228171E-01,
         0.55998042E-02,-0.31858496E-01,-0.24380844E-01, 0.23629388E-03,
         0.31144050E-03, 0.16636217E-03,-0.12914594E-02,-0.59111919E-02,
        -0.17122092E-01, 0.32031205E-02, 0.22866724E-02,-0.79263386E-03,
        -0.85902913E-02,-0.19948639E-01, 0.33218041E-03,-0.73050032E-03,
        -0.60249033E-03, 0.29248995E-03, 0.94374653E-03, 0.34245881E-03,
        -0.18770270E-03,-0.28549516E-03, 0.17330036E-03,-0.10879915E-03,
        -0.42289763E-03, 0.33553003E-03,-0.16866752E-03, 0.23286197E-03,
         0.26942891E-03, 0.22037134E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_1100                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_1100                             =v_t_e_predent_1100                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_1100                             =v_t_e_predent_1100                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x22*x32    *x52
        +coeff[ 24]*x12                
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_1100                             =v_t_e_predent_1100                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_t_e_predent_1100                             =v_t_e_predent_1100                             
        +coeff[ 35]*x11    *x31*x41    
        +coeff[ 36]*x11        *x42    
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]    *x21*x32    *x51
        +coeff[ 39]*x13    *x32        
        +coeff[ 40]*x11*x22*x32        
        +coeff[ 41]*x11    *x32    *x52
        +coeff[ 42]*x12*x21            
        +coeff[ 43]*x11*x21*x31        
    ;
    v_t_e_predent_1100                             =v_t_e_predent_1100                             
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]*x12*x22            
        +coeff[ 46]    *x23*x31        
        +coeff[ 47]*x11*x21*x31*x41    
        +coeff[ 48]        *x33*x41    
        +coeff[ 49]    *x21*x31*x42    
        ;

    return v_t_e_predent_1100                             ;
}
float y_e_predent_1100                             (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.6700754E-04;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.15637535E-03, 0.14789948E+00, 0.56561377E-01, 0.88491933E-02,
         0.38919041E-02,-0.18574386E-02,-0.11373147E-02,-0.18033985E-02,
        -0.13743485E-03,-0.41622983E-03,-0.11391932E-02,-0.52098260E-03,
        -0.31364334E-03,-0.10370291E-02,-0.24569477E-03,-0.27761618E-02,
        -0.10338472E-02,-0.78448158E-03,-0.14715744E-03,-0.40235350E-03,
        -0.92004047E-04,-0.24949779E-04, 0.36541061E-03,-0.51917982E-05,
         0.24226696E-03, 0.24262850E-04, 0.17504882E-03,-0.14202739E-02,
        -0.15446137E-02,-0.41301814E-05,-0.49664573E-05,-0.40439624E-03,
        -0.37020346E-03,-0.55798191E-04, 0.11937843E-03, 0.19953819E-04,
         0.37114351E-04,-0.23697010E-03,-0.63827829E-04, 0.24255305E-04,
         0.21597150E-04, 0.32989275E-04,-0.69667622E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_1100                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_1100                             =v_y_e_predent_1100                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_1100                             =v_y_e_predent_1100                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_predent_1100                             =v_y_e_predent_1100                             
        +coeff[ 26]        *x32*x41*x51
        +coeff[ 27]    *x22    *x43    
        +coeff[ 28]    *x22*x32*x41    
        +coeff[ 29]            *x45*x51
        +coeff[ 30]                *x51
        +coeff[ 31]        *x31*x44    
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]*x11*x23*x31        
        +coeff[ 34]            *x43*x51
    ;
    v_y_e_predent_1100                             =v_y_e_predent_1100                             
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]        *x33    *x51
        +coeff[ 37]        *x33*x42    
        +coeff[ 38]        *x34*x41    
        +coeff[ 39]            *x41*x53
        +coeff[ 40]        *x31    *x53
        +coeff[ 41]    *x22    *x41*x52
        +coeff[ 42]    *x24    *x41*x51
        ;

    return v_y_e_predent_1100                             ;
}
float p_e_predent_1100                             (float *x,int m){
    int ncoeff= 24;
    float avdat= -0.2698381E-03;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
         0.31116151E-03,-0.71654551E-01,-0.76261736E-01,-0.14307452E-01,
        -0.15266626E-01, 0.66679465E-02, 0.11506477E-01,-0.54810243E-02,
        -0.10045829E-04, 0.71019051E-03,-0.42634243E-02, 0.76544442E-03,
         0.15061555E-02,-0.21471961E-02,-0.12947088E-02, 0.59937313E-03,
        -0.57049835E-03,-0.14246771E-04,-0.61002240E-03,-0.11814169E-02,
         0.34264007E-03,-0.76236477E-03, 0.40281215E-03,-0.98281901E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_1100                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_1100                             =v_p_e_predent_1100                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_1100                             =v_p_e_predent_1100                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_1100                             ;
}
float l_e_predent_1100                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4421120E-02;
    float xmin[10]={
        -0.39999E-02,-0.60067E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60054E-01, 0.53997E-01, 0.30012E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44373889E-02,-0.94529204E-02,-0.14560226E-02,-0.61187036E-02,
        -0.61736917E-02, 0.80076966E-03,-0.36862184E-03,-0.26687086E-03,
         0.32715459E-03, 0.12059798E-02, 0.40387880E-03, 0.60273317E-03,
         0.35476971E-05, 0.79733506E-03,-0.10701600E-02, 0.29811959E-03,
         0.12136651E-02,-0.44384820E-03,-0.92992553E-03, 0.16197485E-02,
        -0.25771775E-02, 0.16946624E-02, 0.92369749E-03,-0.37723855E-03,
         0.39996722E-03, 0.51451492E-03, 0.12930377E-02,-0.15526020E-02,
        -0.99883555E-03, 0.23516978E-02,-0.14208364E-02, 0.55449660E-03,
        -0.16587575E-03,-0.13632049E-03, 0.15499679E-03,-0.51657576E-03,
        -0.21336757E-03,-0.24795381E-03,-0.17314995E-03, 0.32702251E-03,
         0.20209874E-03, 0.17718821E-03,-0.28020094E-03, 0.59913663E-03,
         0.17032791E-03, 0.93313138E-04, 0.28809696E-03,-0.47003399E-04,
         0.27087453E-03, 0.85066201E-03, 0.24727522E-03, 0.31876756E-03,
        -0.32892768E-03,-0.51803939E-03, 0.10177832E-02,-0.21374112E-03,
        -0.22470916E-03, 0.47742997E-03, 0.55966503E-03,-0.19988476E-03,
         0.25189845E-03, 0.24564029E-03,-0.21058327E-03, 0.27267978E-03,
         0.29482206E-03, 0.22097027E-03,-0.15511837E-03, 0.20561682E-03,
        -0.57735498E-03, 0.57944510E-03,-0.12451111E-03,-0.61047327E-03,
        -0.49185316E-03, 0.64899778E-03, 0.88114996E-03, 0.47795882E-03,
        -0.74266829E-03,-0.69879234E-03,-0.72243193E-03, 0.48176775E-03,
        -0.32454514E-03,-0.17383704E-02,-0.10371418E-02,-0.48161624E-03,
        -0.42278256E-03, 0.32396981E-03, 0.15069146E-03,-0.63299667E-03,
        -0.13297820E-03, 0.85019809E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_1100                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]*x12                
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[  8]    *x21*x31    *x51
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]            *x42*x51
        +coeff[ 11]        *x31    *x52
        +coeff[ 12]    *x23*x31        
        +coeff[ 13]    *x21*x31*x42    
        +coeff[ 14]*x11*x21*x32        
        +coeff[ 15]*x11*x24            
        +coeff[ 16]*x12*x21*x31*x41    
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 17]*x12    *x31    *x52
        +coeff[ 18]    *x23*x33        
        +coeff[ 19]    *x22*x31*x41*x52
        +coeff[ 20]        *x33*x41*x52
        +coeff[ 21]    *x21    *x43*x52
        +coeff[ 22]*x11    *x34    *x51
        +coeff[ 23]*x11*x22        *x53
        +coeff[ 24]*x12*x24            
        +coeff[ 25]*x12*x22*x31    *x51
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 26]*x13*x21*x32        
        +coeff[ 27]        *x33    *x54
        +coeff[ 28]*x11*x23*x33        
        +coeff[ 29]*x12*x23*x32        
        +coeff[ 30]*x12*x22*x31*x41*x51
        +coeff[ 31]    *x23*x31    *x54
        +coeff[ 32]        *x31        
        +coeff[ 33]*x11                
        +coeff[ 34]                *x52
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 35]*x11    *x31        
        +coeff[ 36]*x11        *x41    
        +coeff[ 37]    *x23            
        +coeff[ 38]    *x22*x31        
        +coeff[ 39]    *x21*x32        
        +coeff[ 40]        *x33        
        +coeff[ 41]    *x21    *x42    
        +coeff[ 42]            *x43    
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 44]    *x21        *x52
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]*x11        *x42    
        +coeff[ 47]*x13                
        +coeff[ 48]    *x24            
        +coeff[ 49]        *x33*x41    
        +coeff[ 50]    *x22    *x42    
        +coeff[ 51]        *x31*x43    
        +coeff[ 52]        *x32    *x52
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 53]    *x21    *x41*x52
        +coeff[ 54]        *x31*x41*x52
        +coeff[ 55]*x11*x23            
        +coeff[ 56]*x11*x22*x31        
        +coeff[ 57]*x11    *x33        
        +coeff[ 58]*x11    *x32*x41    
        +coeff[ 59]*x11*x21    *x42    
        +coeff[ 60]*x11    *x31*x42    
        +coeff[ 61]*x11*x21*x31    *x51
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 62]*x11    *x32    *x51
        +coeff[ 63]*x11    *x31    *x52
        +coeff[ 64]*x12    *x32        
        +coeff[ 65]*x12    *x31*x41    
        +coeff[ 66]*x12        *x41*x51
        +coeff[ 67]*x13    *x31        
        +coeff[ 68]    *x21*x34        
        +coeff[ 69]        *x32*x43    
        +coeff[ 70]*x13            *x51
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 71]        *x32*x42*x51
        +coeff[ 72]        *x31*x43*x51
        +coeff[ 73]        *x33    *x52
        +coeff[ 74]        *x31*x42*x52
        +coeff[ 75]            *x43*x52
        +coeff[ 76]    *x21*x31    *x53
        +coeff[ 77]    *x21    *x41*x53
        +coeff[ 78]        *x31*x41*x53
        +coeff[ 79]*x11*x23*x31        
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 80]*x11*x21*x31*x42    
        +coeff[ 81]*x11    *x32*x42    
        +coeff[ 82]*x11    *x31*x43    
        +coeff[ 83]*x11        *x44    
        +coeff[ 84]*x11    *x32    *x52
        +coeff[ 85]*x11        *x42*x52
        +coeff[ 86]*x11        *x41*x53
        +coeff[ 87]*x12*x21*x32        
        +coeff[ 88]*x12            *x53
    ;
    v_l_e_predent_1100                             =v_l_e_predent_1100                             
        +coeff[ 89]*x13    *x32        
        ;

    return v_l_e_predent_1100                             ;
}
float x_e_predent_1000                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066906E+01;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.42111051E-03,-0.30960455E-02, 0.90907499E-01, 0.43696272E-02,
        -0.22532421E-02,-0.17339912E-02,-0.51460043E-03, 0.25413802E-03,
        -0.70106023E-03,-0.84524200E-03,-0.27953633E-03,-0.17846804E-02,
        -0.14598772E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_1000                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_1000                             =v_x_e_predent_1000                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_1000                             ;
}
float t_e_predent_1000                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3778449E+01;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.46957977E-01,-0.37378628E-01, 0.81083405E+00, 0.16761628E+00,
         0.37036430E-01,-0.15003801E-01, 0.26765384E-01,-0.10847789E-03,
         0.16293216E-01,-0.18358959E-01,-0.19292204E-01,-0.10057738E-01,
         0.27402167E-02,-0.56633302E-02,-0.74133701E-02,-0.30265942E-02,
         0.10454882E-02, 0.56956406E-03,-0.18343685E-01,-0.14563595E-01,
         0.54651038E-02,-0.31623684E-01,-0.24395453E-01, 0.68165871E-04,
         0.30542017E-03,-0.16317759E-02,-0.60403594E-02,-0.16982235E-01,
         0.37837811E-03, 0.30897358E-02, 0.23023607E-02,-0.77715743E-03,
        -0.78971479E-02,-0.19442208E-01,-0.10859879E-03,-0.59877755E-03,
        -0.53787342E-03, 0.29105161E-03,-0.19232096E-03,-0.12695810E-02,
         0.11986167E-02, 0.26074608E-03, 0.14178020E-03,-0.29370122E-03,
        -0.89277432E-03, 0.30008165E-03,-0.16016074E-03, 0.26559664E-03,
         0.11493313E-03,-0.56162529E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_1000                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_1000                             =v_t_e_predent_1000                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31*x41    
    ;
    v_t_e_predent_1000                             =v_t_e_predent_1000                             
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x22*x32    *x52
        +coeff[ 24]        *x32        
        +coeff[ 25]*x11*x23            
    ;
    v_t_e_predent_1000                             =v_t_e_predent_1000                             
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x12                
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]*x12*x21*x32    *x51
    ;
    v_t_e_predent_1000                             =v_t_e_predent_1000                             
        +coeff[ 35]*x11    *x31*x41    
        +coeff[ 36]*x11        *x42    
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]*x11            *x52
        +coeff[ 39]        *x31*x43    
        +coeff[ 40]    *x21*x32    *x51
        +coeff[ 41]    *x22*x33*x41    
        +coeff[ 42]*x12*x21            
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_predent_1000                             =v_t_e_predent_1000                             
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]*x12*x21        *x51
        +coeff[ 46]    *x22*x31    *x51
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_predent_1000                             ;
}
float y_e_predent_1000                             (float *x,int m){
    int ncoeff= 41;
    float avdat=  0.3707839E-04;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
        -0.67919841E-05, 0.14772828E+00, 0.56511056E-01, 0.88358549E-02,
         0.38931728E-02,-0.18195488E-02,-0.11416974E-02,-0.18739020E-02,
        -0.14758483E-03,-0.33311063E-03,-0.11968652E-02,-0.51085767E-03,
        -0.31372000E-03,-0.10299284E-02,-0.25647206E-03,-0.27046883E-02,
        -0.10444820E-02,-0.79170079E-03, 0.10498737E-05,-0.11773646E-03,
        -0.15706562E-04,-0.16335060E-04, 0.13366072E-03,-0.15305657E-04,
         0.23673363E-03, 0.16182479E-03,-0.15525440E-02,-0.16127005E-02,
        -0.12560851E-04, 0.16352831E-04,-0.33816823E-03, 0.41382496E-04,
        -0.36219117E-03, 0.14510947E-03, 0.23652879E-04,-0.13615526E-03,
         0.32378335E-04, 0.29598306E-04, 0.19407044E-04,-0.45859946E-04,
        -0.69241163E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_1000                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_1000                             =v_y_e_predent_1000                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_1000                             =v_y_e_predent_1000                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_1000                             =v_y_e_predent_1000                             
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]            *x45*x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]        *x31*x44    
        +coeff[ 31]        *x33    *x51
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_predent_1000                             =v_y_e_predent_1000                             
        +coeff[ 35]        *x33*x42    
        +coeff[ 36]            *x41*x53
        +coeff[ 37]*x11*x21*x31*x42    
        +coeff[ 38]        *x31    *x53
        +coeff[ 39]*x11*x23*x31        
        +coeff[ 40]    *x22    *x43*x51
        ;

    return v_y_e_predent_1000                             ;
}
float p_e_predent_1000                             (float *x,int m){
    int ncoeff= 24;
    float avdat= -0.2851580E-03;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
         0.26727005E-03,-0.71701005E-01,-0.76293603E-01,-0.14323786E-01,
        -0.15281305E-01, 0.66708606E-02, 0.11505943E-01,-0.54768766E-02,
        -0.15305295E-04, 0.71468973E-03,-0.42588422E-02, 0.77352102E-03,
         0.15030411E-02,-0.21547019E-02,-0.12987053E-02, 0.58831181E-03,
        -0.56804181E-03,-0.24556348E-04,-0.58502471E-03,-0.11708406E-02,
         0.34859701E-03,-0.76222100E-03, 0.41661749E-03,-0.97727450E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_1000                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_1000                             =v_p_e_predent_1000                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_1000                             =v_p_e_predent_1000                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_1000                             ;
}
float l_e_predent_1000                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4407282E-02;
    float xmin[10]={
        -0.39998E-02,-0.60064E-01,-0.53989E-01,-0.30011E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30023E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.43948535E-02,-0.91640297E-02,-0.10391331E-02,-0.56321537E-02,
        -0.62333923E-02, 0.30851745E-03,-0.59391244E-03, 0.94592622E-04,
        -0.14608341E-04, 0.79752397E-04, 0.13290097E-02, 0.66871301E-03,
        -0.56513364E-03, 0.39649845E-03,-0.65738661E-03,-0.59200214E-04,
         0.27822619E-03, 0.26217412E-03,-0.10826590E-02,-0.99343271E-03,
         0.28052568E-03, 0.39371685E-03, 0.86392241E-03, 0.19989028E-02,
         0.21902651E-02,-0.10025438E-03,-0.14473755E-02, 0.28105140E-04,
        -0.74553303E-04,-0.74560077E-04, 0.48761992E-03,-0.21984131E-05,
         0.20859484E-03, 0.24042805E-03,-0.84197309E-04, 0.29342639E-03,
        -0.25309381E-03, 0.14502117E-03, 0.27286820E-03, 0.13748889E-04,
        -0.59822155E-03, 0.10327130E-02, 0.41499524E-03,-0.10314351E-02,
        -0.10003836E-02,-0.21390618E-03,-0.17203380E-03,-0.17986784E-03,
         0.25037557E-03,-0.22618670E-03,-0.49422268E-03, 0.30354515E-03,
        -0.27097889E-04, 0.11357474E-03, 0.63180353E-03, 0.90524663E-04,
        -0.55993919E-03,-0.36964857E-03, 0.65894559E-03, 0.19462722E-03,
        -0.58297551E-03,-0.41164074E-03,-0.57718897E-03,-0.41557077E-03,
         0.55332470E-03,-0.60834113E-03, 0.66868030E-03,-0.32910900E-03,
         0.64966804E-03,-0.26416784E-03, 0.25351462E-03, 0.32061458E-03,
         0.75942837E-03, 0.38017918E-03,-0.56444452E-03,-0.48788643E-03,
         0.26177347E-03,-0.68454311E-03, 0.85706683E-03, 0.69948146E-03,
        -0.13877599E-02, 0.14371175E-03, 0.14661340E-03,-0.60188788E-03,
         0.18860347E-03,-0.97335025E-03,-0.52669755E-03, 0.97720581E-03,
         0.86690078E-03,-0.94163942E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_predent_1000                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]*x12            *x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[  8]        *x31    *x51
        +coeff[  9]    *x21*x31    *x51
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]    *x21*x33        
        +coeff[ 14]        *x34        
        +coeff[ 15]    *x21*x31    *x52
        +coeff[ 16]*x12*x21        *x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 17]    *x22*x32    *x51
        +coeff[ 18]    *x22*x31*x41*x51
        +coeff[ 19]        *x33*x41*x51
        +coeff[ 20]*x11*x21        *x53
        +coeff[ 21]*x11        *x41*x53
        +coeff[ 22]*x11*x21    *x42*x52
        +coeff[ 23]*x11*x21*x31    *x53
        +coeff[ 24]*x11    *x31*x41*x53
        +coeff[ 25]*x13    *x31*x41*x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 26]*x12    *x34    *x51
        +coeff[ 27]                *x51
        +coeff[ 28]*x12                
        +coeff[ 29]*x11    *x32        
        +coeff[ 30]*x11*x21    *x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x13                
        +coeff[ 33]    *x24            
        +coeff[ 34]    *x23        *x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 35]    *x22*x31    *x51
        +coeff[ 36]        *x33    *x51
        +coeff[ 37]    *x21        *x53
        +coeff[ 38]*x11*x23            
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]*x11    *x32*x41    
        +coeff[ 41]*x11    *x31*x42    
        +coeff[ 42]*x11        *x43    
        +coeff[ 43]*x11*x21*x31    *x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 44]*x11    *x31*x41*x51
        +coeff[ 45]*x11*x21        *x52
        +coeff[ 46]*x11        *x41*x52
        +coeff[ 47]*x12    *x31    *x51
        +coeff[ 48]*x13*x21            
        +coeff[ 49]*x13    *x31        
        +coeff[ 50]    *x24*x31        
        +coeff[ 51]    *x22*x33        
        +coeff[ 52]    *x24    *x41    
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 53]    *x23    *x42    
        +coeff[ 54]    *x22*x31*x42    
        +coeff[ 55]*x13            *x51
        +coeff[ 56]    *x24        *x51
        +coeff[ 57]    *x23    *x41*x51
        +coeff[ 58]    *x21    *x43*x51
        +coeff[ 59]    *x23        *x52
        +coeff[ 60]    *x21*x31    *x53
        +coeff[ 61]    *x21    *x41*x53
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 62]            *x42*x53
        +coeff[ 63]*x11*x24            
        +coeff[ 64]*x11*x22*x32        
        +coeff[ 65]*x11*x21    *x43    
        +coeff[ 66]*x11    *x33    *x51
        +coeff[ 67]*x11    *x32*x41*x51
        +coeff[ 68]*x11    *x31*x41*x52
        +coeff[ 69]*x12*x23            
        +coeff[ 70]*x12*x21*x32        
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 71]*x12    *x32*x41    
        +coeff[ 72]*x12    *x32    *x51
        +coeff[ 73]*x12            *x53
        +coeff[ 74]*x13    *x32        
        +coeff[ 75]*x13    *x31*x41    
        +coeff[ 76]    *x21*x32*x43    
        +coeff[ 77]*x13    *x31    *x51
        +coeff[ 78]    *x21*x34    *x51
        +coeff[ 79]        *x34*x41*x51
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 80]    *x23    *x42*x51
        +coeff[ 81]    *x21    *x44*x51
        +coeff[ 82]*x13            *x52
        +coeff[ 83]    *x23*x31    *x52
        +coeff[ 84]        *x34    *x52
        +coeff[ 85]    *x21*x32    *x53
        +coeff[ 86]        *x32*x41*x53
        +coeff[ 87]    *x21    *x42*x53
        +coeff[ 88]*x11    *x34*x41    
    ;
    v_l_e_predent_1000                             =v_l_e_predent_1000                             
        +coeff[ 89]*x11*x22*x31*x42    
        ;

    return v_l_e_predent_1000                             ;
}
float x_e_predent_900                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066764E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.28307369E-03,-0.30942892E-02, 0.90953857E-01, 0.43709613E-02,
        -0.22797289E-02,-0.17510955E-02,-0.47750838E-03, 0.25058544E-03,
        -0.71206433E-03,-0.85306651E-03,-0.27150279E-03,-0.17088131E-02,
        -0.13990504E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_900                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_900                             =v_x_e_predent_900                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_900                             ;
}
float t_e_predent_900                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3779990E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.48431266E-01,-0.37485246E-01, 0.81140321E+00, 0.16775665E+00,
         0.37241116E-01,-0.15141174E-01, 0.26634317E-01,-0.13749677E-03,
         0.16326906E-01,-0.18167039E-01,-0.19383416E-01,-0.96041281E-02,
         0.27254266E-02,-0.55636708E-02,-0.74062576E-02,-0.29853648E-02,
         0.93787094E-03, 0.42384371E-03,-0.18336346E-01,-0.13780721E-01,
         0.53901467E-02,-0.20998397E-02, 0.16424118E-03, 0.15563169E-03,
        -0.55948007E-02,-0.30741991E-01,-0.23722790E-01,-0.17581539E-01,
         0.38044201E-03, 0.37694708E-03,-0.13282696E-02, 0.33291278E-02,
         0.24622919E-02,-0.94099279E-03,-0.88979481E-02,-0.20608597E-01,
         0.66688095E-04, 0.48496213E-03, 0.26166197E-03,-0.68520731E-03,
        -0.48881321E-03,-0.68126258E-03, 0.11445497E-02,-0.23922173E-03,
        -0.14841222E-03,-0.15595240E-03,-0.69294918E-04,-0.17739249E-03,
         0.80802558E-04,-0.16190702E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_900                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_900                             =v_t_e_predent_900                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31*x41    
    ;
    v_t_e_predent_900                             =v_t_e_predent_900                             
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x32*x42    
        +coeff[ 22]    *x22*x32    *x52
        +coeff[ 23]        *x32        
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_t_e_predent_900                             =v_t_e_predent_900                             
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]*x11*x23            
        +coeff[ 31]    *x21*x31*x41*x51
        +coeff[ 32]    *x21    *x42*x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]    *x21*x33*x41    
    ;
    v_t_e_predent_900                             =v_t_e_predent_900                             
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]*x12*x21*x32    *x51
        +coeff[ 37]    *x23*x32    *x51
        +coeff[ 38]*x12*x21            
        +coeff[ 39]*x11    *x31*x41    
        +coeff[ 40]*x11        *x42    
        +coeff[ 41]        *x31*x43    
        +coeff[ 42]    *x21*x32    *x51
        +coeff[ 43]*x11    *x32    *x52
    ;
    v_t_e_predent_900                             =v_t_e_predent_900                             
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]        *x31*x42    
        +coeff[ 46]            *x43    
        +coeff[ 47]    *x21*x31    *x51
        +coeff[ 48]*x11*x22*x31        
        +coeff[ 49]*x11*x22    *x41    
        ;

    return v_t_e_predent_900                             ;
}
float y_e_predent_900                             (float *x,int m){
    int ncoeff= 41;
    float avdat=  0.1267385E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 42]={
         0.34743272E-04, 0.14767534E+00, 0.56437589E-01, 0.88625764E-02,
         0.39149383E-02,-0.18749174E-02,-0.11794083E-02,-0.19581320E-02,
        -0.74504875E-04,-0.23850796E-03,-0.12072769E-02,-0.51365705E-03,
        -0.31469771E-03,-0.11033997E-02,-0.24739854E-03,-0.10156771E-02,
        -0.76008670E-03,-0.33427117E-03,-0.15549814E-03,-0.62955914E-05,
         0.25983073E-03, 0.16715423E-03,-0.20880259E-02,-0.16114894E-03,
        -0.15378818E-02,-0.37795972E-03,-0.19287192E-04,-0.30676491E-03,
        -0.93186274E-03,-0.10093286E-03,-0.11877448E-02, 0.32807951E-04,
        -0.55763964E-03, 0.34760822E-05, 0.84209787E-05, 0.92548953E-05,
         0.15239422E-03,-0.17024194E-03, 0.27175834E-04,-0.47709495E-04,
        -0.31458876E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_predent_900                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_900                             =v_y_e_predent_900                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_900                             =v_y_e_predent_900                             
        +coeff[ 17]    *x24*x31*x42    
        +coeff[ 18]    *x24    *x45    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]        *x31*x42*x51
        +coeff[ 21]        *x32*x41*x51
        +coeff[ 22]    *x22*x31*x42    
        +coeff[ 23]        *x33*x42    
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_predent_900                             =v_y_e_predent_900                             
        +coeff[ 26]            *x45*x51
        +coeff[ 27]    *x22    *x45    
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x33    *x51
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]                *x51
        +coeff[ 34]    *x21*x31*x41    
    ;
    v_y_e_predent_900                             =v_y_e_predent_900                             
        +coeff[ 35]        *x31*x41*x51
        +coeff[ 36]            *x43*x51
        +coeff[ 37]        *x31*x44    
        +coeff[ 38]        *x33    *x51
        +coeff[ 39]*x11*x23*x31        
        +coeff[ 40]    *x22    *x43*x51
        ;

    return v_y_e_predent_900                             ;
}
float p_e_predent_900                             (float *x,int m){
    int ncoeff= 24;
    float avdat= -0.8156340E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
         0.48346355E-04,-0.71745984E-01,-0.76416619E-01,-0.14341406E-01,
        -0.15319999E-01, 0.66653523E-02, 0.11510748E-01,-0.54826322E-02,
         0.19797002E-04, 0.71459572E-03,-0.43030186E-02, 0.76883426E-03,
         0.15050297E-02,-0.21454135E-02,-0.12934826E-02, 0.58898807E-03,
        -0.56653959E-03,-0.27512329E-04,-0.58106746E-03,-0.11592984E-02,
         0.35356177E-03,-0.76128519E-03, 0.42022348E-03,-0.96095534E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_900                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_900                             =v_p_e_predent_900                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_900                             =v_p_e_predent_900                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_900                             ;
}
float l_e_predent_900                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4391035E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53978E-01,-0.30020E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.30035E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44018887E-02,-0.91435239E-02,-0.17424939E-02,-0.55134716E-02,
        -0.60871546E-02,-0.15160633E-03, 0.72930061E-03,-0.77054749E-03,
         0.25229558E-03, 0.40223076E-04, 0.58224692E-03,-0.47019566E-03,
        -0.10741558E-02,-0.30142148E-04,-0.41569813E-03,-0.35653680E-03,
        -0.46846259E-03,-0.62794727E-03,-0.73856802E-03,-0.37816909E-03,
         0.20523134E-02,-0.10370688E-02, 0.35187387E-03, 0.78943663E-03,
        -0.89022062E-04, 0.12843601E-02,-0.76005241E-03, 0.55797398E-03,
        -0.37287147E-03,-0.33334082E-02,-0.22709572E-02, 0.94793097E-03,
        -0.59682483E-04,-0.75970245E-04,-0.19208812E-03,-0.16651741E-03,
         0.39829867E-03, 0.63249121E-04,-0.34913630E-03, 0.77007753E-04,
         0.32749953E-03, 0.23380743E-03, 0.54083401E-04,-0.14043995E-03,
         0.64266962E-03, 0.91639126E-03, 0.16286622E-04,-0.71472221E-03,
         0.33396381E-03,-0.60775488E-04, 0.19527080E-03, 0.13844084E-03,
         0.20157752E-03,-0.10552919E-03,-0.18617878E-03, 0.53286145E-03,
         0.17611394E-03, 0.23396937E-03, 0.16809526E-03,-0.23951083E-03,
         0.29603593E-03,-0.23519350E-03,-0.33631298E-03, 0.47952161E-03,
        -0.21607232E-03,-0.42850032E-03, 0.40619622E-03,-0.85158623E-03,
         0.27935437E-03,-0.14264639E-03,-0.46559586E-03,-0.13925903E-02,
         0.67280792E-03, 0.15423584E-03,-0.57677884E-03, 0.28173337E-03,
         0.29467751E-03, 0.29697828E-03,-0.54189254E-03, 0.53099653E-03,
         0.62664265E-04,-0.51649829E-03, 0.98905829E-03, 0.56932826E-03,
        -0.55234606E-03, 0.41224176E-03,-0.21397615E-03, 0.61994756E-03,
        -0.46144042E-03, 0.36734389E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_predent_900                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]        *x31    *x51
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[  8]        *x32    *x51
        +coeff[  9]                *x51
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]*x11        *x41*x51
        +coeff[ 12]    *x21*x31*x42    
        +coeff[ 13]*x11    *x31*x41*x51
        +coeff[ 14]*x12        *x42    
        +coeff[ 15]*x12    *x31    *x51
        +coeff[ 16]    *x21    *x41*x53
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 17]*x11*x23    *x41    
        +coeff[ 18]*x11*x21*x31*x42    
        +coeff[ 19]*x11    *x33    *x51
        +coeff[ 20]*x11    *x31*x42*x51
        +coeff[ 21]*x12*x21    *x41*x51
        +coeff[ 22]*x12            *x53
        +coeff[ 23]    *x23*x33        
        +coeff[ 24]    *x23    *x43    
        +coeff[ 25]*x11*x22    *x42*x51
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 26]*x11*x22    *x41*x52
        +coeff[ 27]*x12*x22*x32        
        +coeff[ 28]*x13        *x43    
        +coeff[ 29]*x11*x22*x33    *x51
        +coeff[ 30]*x11    *x31*x44*x51
        +coeff[ 31]*x11*x22*x31    *x53
        +coeff[ 32]        *x31        
        +coeff[ 33]            *x41    
        +coeff[ 34]    *x21*x31        
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 35]    *x21    *x41    
        +coeff[ 36]*x11    *x31        
        +coeff[ 37]*x11        *x41    
        +coeff[ 38]*x11            *x51
        +coeff[ 39]*x12                
        +coeff[ 40]    *x21*x32        
        +coeff[ 41]    *x21*x31*x41    
        +coeff[ 42]        *x32*x41    
        +coeff[ 43]    *x21*x31    *x51
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 44]    *x21    *x41*x51
        +coeff[ 45]        *x31*x41*x51
        +coeff[ 46]            *x42*x51
        +coeff[ 47]    *x21        *x52
        +coeff[ 48]*x12*x21            
        +coeff[ 49]*x12    *x31        
        +coeff[ 50]            *x44    
        +coeff[ 51]    *x22        *x52
        +coeff[ 52]    *x21*x31    *x52
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 53]            *x41*x53
        +coeff[ 54]*x11    *x33        
        +coeff[ 55]*x11        *x43    
        +coeff[ 56]*x11*x21*x31    *x51
        +coeff[ 57]*x11    *x32    *x51
        +coeff[ 58]*x11*x21        *x52
        +coeff[ 59]*x11        *x41*x52
        +coeff[ 60]*x12    *x31*x41    
        +coeff[ 61]*x13*x21            
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 62]*x13    *x31        
        +coeff[ 63]    *x22*x32*x41    
        +coeff[ 64]    *x21*x33*x41    
        +coeff[ 65]        *x34*x41    
        +coeff[ 66]    *x23    *x42    
        +coeff[ 67]    *x21*x32*x42    
        +coeff[ 68]*x13            *x51
        +coeff[ 69]        *x32*x42*x51
        +coeff[ 70]    *x21    *x43*x51
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 71]        *x31*x43*x51
        +coeff[ 72]    *x23        *x52
        +coeff[ 73]    *x22*x31    *x52
        +coeff[ 74]    *x22    *x41*x52
        +coeff[ 75]    *x21    *x42*x52
        +coeff[ 76]            *x43*x52
        +coeff[ 77]*x11*x22*x32        
        +coeff[ 78]*x11*x21*x33        
        +coeff[ 79]*x11    *x33*x41    
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 80]*x11*x22    *x42    
        +coeff[ 81]*x11    *x31*x43    
        +coeff[ 82]*x11*x22*x31    *x51
        +coeff[ 83]*x11*x21*x32    *x51
        +coeff[ 84]*x11    *x32*x41*x51
        +coeff[ 85]*x11    *x31*x41*x52
        +coeff[ 86]*x11*x21        *x53
        +coeff[ 87]*x11        *x41*x53
        +coeff[ 88]*x12*x23            
    ;
    v_l_e_predent_900                             =v_l_e_predent_900                             
        +coeff[ 89]*x12*x22    *x41    
        ;

    return v_l_e_predent_900                             ;
}
float x_e_predent_800                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066504E+01;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.45324086E-04,-0.30935090E-02, 0.91010362E-01, 0.43730303E-02,
        -0.22912938E-02,-0.17630305E-02,-0.47006886E-03, 0.24853460E-03,
        -0.70964469E-03,-0.85585914E-03,-0.28018281E-03,-0.16960670E-02,
        -0.13901509E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_800                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_800                             =v_x_e_predent_800                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_800                             ;
}
float t_e_predent_800                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3781934E+01;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.50163832E-01,-0.37403971E-01, 0.81215042E+00, 0.16807371E+00,
         0.37129540E-01,-0.15109198E-01, 0.26870739E-01,-0.11401482E-03,
         0.16301397E-01,-0.18001644E-01,-0.19537054E-01,-0.10104335E-01,
         0.26940478E-02,-0.56275842E-02,-0.72588543E-02,-0.30916152E-02,
         0.88338787E-03, 0.53430902E-03,-0.18233089E-01,-0.14334857E-01,
         0.56763100E-02,-0.31070964E-01,-0.23830095E-01, 0.76487886E-05,
         0.28893771E-03,-0.14620007E-02,-0.60200728E-02,-0.17643906E-01,
         0.37039962E-03, 0.34418125E-02, 0.25294733E-02,-0.86687854E-03,
        -0.86698867E-02,-0.20000789E-01,-0.23645179E-03, 0.19930338E-03,
        -0.73876139E-03,-0.55529748E-03, 0.21184943E-03, 0.14182410E-02,
        -0.28988581E-04,-0.62073181E-04,-0.23240226E-03,-0.15495172E-03,
        -0.14442072E-03,-0.81021601E-03, 0.22779116E-03,-0.90143544E-03,
         0.17644736E-03, 0.19495247E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_800                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_800                             =v_t_e_predent_800                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31*x41    
    ;
    v_t_e_predent_800                             =v_t_e_predent_800                             
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x22*x32    *x52
        +coeff[ 24]        *x32        
        +coeff[ 25]*x11*x23            
    ;
    v_t_e_predent_800                             =v_t_e_predent_800                             
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x12                
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_t_e_predent_800                             =v_t_e_predent_800                             
        +coeff[ 35]*x12*x21            
        +coeff[ 36]*x11    *x31*x41    
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]*x11*x21        *x51
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x11    *x32    *x52
        +coeff[ 41]    *x21    *x41    
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]*x11            *x52
    ;
    v_t_e_predent_800                             =v_t_e_predent_800                             
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]    *x21    *x43    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]*x11*x21*x31    *x51
        +coeff[ 49]*x11*x23*x31        
        ;

    return v_t_e_predent_800                             ;
}
float y_e_predent_800                             (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.7755931E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.80107432E-03, 0.14752631E+00, 0.56352954E-01, 0.88694720E-02,
         0.39011957E-02,-0.18336917E-02,-0.11367996E-02,-0.18404735E-02,
        -0.12312518E-03,-0.37661719E-03,-0.11219513E-02,-0.50988863E-03,
        -0.31423708E-03,-0.10502638E-02,-0.25577590E-03,-0.28021038E-02,
        -0.10432802E-02,-0.78932720E-03,-0.13907088E-03,-0.46251036E-03,
        -0.91267495E-04,-0.48402235E-04, 0.42976998E-03,-0.17846765E-04,
         0.24084079E-03, 0.15362089E-03,-0.14370248E-02,-0.15677855E-02,
         0.37344398E-04,-0.20654395E-04,-0.34486776E-03, 0.39449267E-04,
        -0.34828964E-03, 0.73630341E-04, 0.20264168E-04,-0.19857373E-03,
        -0.82181083E-04, 0.34736378E-04, 0.43202635E-04, 0.19630652E-04,
        -0.40234449E-04, 0.15677655E-04, 0.15643307E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_800                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_800                             =v_y_e_predent_800                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_800                             =v_y_e_predent_800                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_800                             =v_y_e_predent_800                             
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]            *x45*x51
        +coeff[ 29]    *x22    *x41*x51
        +coeff[ 30]        *x31*x44    
        +coeff[ 31]        *x33    *x51
        +coeff[ 32]    *x22*x33        
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_predent_800                             =v_y_e_predent_800                             
        +coeff[ 35]        *x33*x42    
        +coeff[ 36]        *x34*x41    
        +coeff[ 37]            *x41*x53
        +coeff[ 38]*x11*x21*x31*x42    
        +coeff[ 39]        *x31    *x53
        +coeff[ 40]*x11*x23*x31        
        +coeff[ 41]*x11*x21    *x44    
        +coeff[ 42]*x13*x21    *x41    
        ;

    return v_y_e_predent_800                             ;
}
float p_e_predent_800                             (float *x,int m){
    int ncoeff= 24;
    float avdat= -0.1509954E-03;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
         0.16826708E-03,-0.71825191E-01,-0.76527864E-01,-0.14373155E-01,
        -0.15334370E-01, 0.66715004E-02, 0.11514552E-01,-0.55084121E-02,
         0.19294519E-04, 0.71149104E-03,-0.43083769E-02, 0.76742133E-03,
         0.14976718E-02,-0.21720449E-02,-0.13046877E-02, 0.59503643E-03,
        -0.57533669E-03, 0.20025682E-04,-0.59658888E-03,-0.12148175E-02,
         0.35040808E-03,-0.76847995E-03, 0.41929493E-03,-0.10216557E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_800                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_800                             =v_p_e_predent_800                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_800                             =v_p_e_predent_800                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_800                             ;
}
float l_e_predent_800                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4379472E-02;
    float xmin[10]={
        -0.39991E-02,-0.60051E-01,-0.53999E-01,-0.30034E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60054E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44224951E-02,-0.95182583E-02,-0.16166517E-02,-0.55783289E-02,
        -0.60585230E-02, 0.62701269E-03,-0.52427745E-03, 0.64314634E-03,
        -0.16297380E-03, 0.12212592E-02,-0.11822395E-03, 0.61656121E-03,
         0.10903092E-02, 0.37260080E-03, 0.36834259E-03,-0.42058900E-03,
         0.11882599E-02, 0.13414854E-02, 0.58576209E-03,-0.81922702E-03,
         0.45316771E-03,-0.42382322E-03,-0.81373530E-03,-0.38041743E-02,
        -0.27686299E-02, 0.98843222E-04,-0.86520893E-04,-0.26822666E-03,
        -0.11343996E-03,-0.51766496E-04, 0.12827522E-03, 0.92831126E-03,
        -0.22732027E-03, 0.22954619E-03,-0.76429405E-05, 0.64601067E-04,
        -0.28918311E-03, 0.12079710E-03,-0.30288144E-03, 0.14430647E-03,
         0.20085300E-03,-0.25532997E-03, 0.17727794E-03,-0.38230061E-03,
        -0.23562640E-03, 0.19758874E-03, 0.51258970E-03,-0.68513444E-03,
         0.81853679E-03,-0.73722372E-03, 0.77940279E-03,-0.55500260E-03,
        -0.46628830E-03, 0.98790170E-03,-0.18031172E-03, 0.75876788E-03,
        -0.29665531E-03,-0.20136053E-03, 0.37826074E-03,-0.97159245E-04,
        -0.24649480E-03, 0.27893978E-03,-0.32750310E-03, 0.72915619E-03,
         0.65695442E-03,-0.32757508E-03,-0.34162024E-03, 0.10202208E-02,
        -0.13872013E-03, 0.59991732E-03,-0.58486732E-03, 0.11633299E-03,
         0.85959962E-03,-0.38510808E-03, 0.35861769E-03, 0.47427931E-03,
         0.12843919E-03,-0.59782737E-03,-0.46869268E-03,-0.59165608E-03,
        -0.44716810E-03, 0.17213254E-03,-0.47494323E-03, 0.35998199E-03,
        -0.54565439E-03, 0.89891185E-03,-0.81655465E-03, 0.76572766E-03,
         0.56378468E-03,-0.88312448E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_800                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x42*x51
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[  8]*x12                
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]*x11*x21*x31        
        +coeff[ 11]    *x24            
        +coeff[ 12]    *x22*x31*x41    
        +coeff[ 13]*x11*x21*x31    *x51
        +coeff[ 14]        *x34    *x51
        +coeff[ 15]        *x33*x41*x51
        +coeff[ 16]*x11*x21*x31*x42    
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 17]    *x22*x32*x42    
        +coeff[ 18]    *x24*x31    *x51
        +coeff[ 19]    *x23*x32    *x51
        +coeff[ 20]*x11*x21*x34        
        +coeff[ 21]*x11    *x33*x41*x51
        +coeff[ 22]*x12*x21    *x41*x52
        +coeff[ 23]    *x22*x32*x43*x51
        +coeff[ 24]    *x22*x31*x42*x53
        +coeff[ 25]    *x21    *x41    
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 26]        *x31    *x51
        +coeff[ 27]*x11    *x31        
        +coeff[ 28]    *x21*x32        
        +coeff[ 29]    *x22    *x41    
        +coeff[ 30]*x11    *x31    *x51
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]        *x32    *x52
        +coeff[ 34]            *x41*x53
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 35]*x11*x22*x31        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]*x11        *x43    
        +coeff[ 38]*x11*x22        *x51
        +coeff[ 39]*x11    *x32    *x51
        +coeff[ 40]*x11        *x42*x51
        +coeff[ 41]*x11*x21        *x52
        +coeff[ 42]*x11    *x31    *x52
        +coeff[ 43]*x12    *x31*x41    
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 44]*x12        *x41*x51
        +coeff[ 45]*x13    *x31        
        +coeff[ 46]    *x24*x31        
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x34        
        +coeff[ 49]    *x23*x31*x41    
        +coeff[ 50]    *x21*x33*x41    
        +coeff[ 51]    *x22*x31*x42    
        +coeff[ 52]    *x21*x31*x43    
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 53]    *x21*x32*x41*x51
        +coeff[ 54]    *x21*x31*x42*x51
        +coeff[ 55]        *x32*x42*x51
        +coeff[ 56]    *x21    *x43*x51
        +coeff[ 57]    *x22*x31    *x52
        +coeff[ 58]    *x22    *x41*x52
        +coeff[ 59]        *x32*x41*x52
        +coeff[ 60]    *x22        *x53
        +coeff[ 61]    *x21*x31    *x53
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 62]            *x42*x53
        +coeff[ 63]*x11*x21*x33        
        +coeff[ 64]*x11*x21*x32*x41    
        +coeff[ 65]*x11*x22        *x52
        +coeff[ 66]*x11*x21    *x41*x52
        +coeff[ 67]*x11    *x31*x41*x52
        +coeff[ 68]*x11*x21        *x53
        +coeff[ 69]*x11            *x54
        +coeff[ 70]*x12*x22*x31        
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 71]*x12    *x33        
        +coeff[ 72]*x12*x21*x31*x41    
        +coeff[ 73]*x12    *x32*x41    
        +coeff[ 74]*x12*x21    *x42    
        +coeff[ 75]*x12*x22        *x51
        +coeff[ 76]*x12*x21*x31    *x51
        +coeff[ 77]*x12    *x32    *x51
        +coeff[ 78]*x12*x21    *x41*x51
        +coeff[ 79]*x12    *x31*x41*x51
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 80]*x13*x21*x31        
        +coeff[ 81]*x13    *x32        
        +coeff[ 82]*x13    *x31*x41    
        +coeff[ 83]    *x23*x31*x41*x51
        +coeff[ 84]*x13            *x52
        +coeff[ 85]    *x23*x31    *x52
        +coeff[ 86]    *x21*x33    *x52
        +coeff[ 87]    *x22*x31*x41*x52
        +coeff[ 88]    *x21*x32    *x53
    ;
    v_l_e_predent_800                             =v_l_e_predent_800                             
        +coeff[ 89]    *x22    *x41*x53
        ;

    return v_l_e_predent_800                             ;
}
float x_e_predent_700                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066898E+01;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.44158188E-03,-0.30943346E-02, 0.91058299E-01, 0.43707378E-02,
        -0.22475529E-02,-0.17156752E-02,-0.51818084E-03, 0.24890661E-03,
        -0.68695913E-03,-0.82532899E-03,-0.27431111E-03,-0.17709188E-02,
        -0.14639515E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_700                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_700                             =v_x_e_predent_700                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_700                             ;
}
float t_e_predent_700                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3778613E+01;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.46882279E-01,-0.37489180E-01, 0.81271607E+00, 0.16861947E+00,
         0.37167385E-01,-0.15220056E-01, 0.27363600E-01,-0.54867443E-04,
         0.16303210E-01,-0.17845716E-01,-0.19125443E-01,-0.10430318E-01,
         0.26268889E-02,-0.57304758E-02,-0.70564798E-02, 0.84440247E-03,
         0.58004737E-03,-0.30145813E-02,-0.18547243E-01,-0.14422010E-01,
         0.57535376E-02,-0.31703845E-01,-0.24418106E-01, 0.55182783E-03,
         0.29940417E-03,-0.16662318E-02,-0.62652524E-02,-0.17052086E-01,
         0.37885952E-03, 0.31279053E-02, 0.22677830E-02,-0.11177653E-02,
        -0.87516448E-02,-0.19840609E-01, 0.35206725E-04,-0.57309354E-03,
        -0.47523991E-03, 0.31270960E-03, 0.10974105E-02, 0.50214533E-03,
        -0.35432298E-03, 0.48222669E-03,-0.15965736E-03, 0.30351387E-03,
         0.13804038E-03,-0.73378329E-03,-0.70637342E-03, 0.19935962E-03,
        -0.62447722E-03,-0.30699209E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_700                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_700                             =v_t_e_predent_700                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_700                             =v_t_e_predent_700                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23*x31*x41    
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x22*x32    *x52
        +coeff[ 24]        *x32        
        +coeff[ 25]*x11*x23            
    ;
    v_t_e_predent_700                             =v_t_e_predent_700                             
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x12                
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_t_e_predent_700                             =v_t_e_predent_700                             
        +coeff[ 35]*x11    *x31*x41    
        +coeff[ 36]*x11        *x42    
        +coeff[ 37]*x11*x21        *x51
        +coeff[ 38]    *x21*x32    *x51
        +coeff[ 39]*x11*x23    *x41    
        +coeff[ 40]*x11    *x32    *x52
        +coeff[ 41]*x12*x21            
        +coeff[ 42]*x12            *x51
        +coeff[ 43]*x13*x21            
    ;
    v_t_e_predent_700                             =v_t_e_predent_700                             
        +coeff[ 44]*x11*x22    *x41    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]        *x31*x43    
        +coeff[ 47]*x11    *x32    *x51
        +coeff[ 48]*x12*x23            
        +coeff[ 49]*x11*x21*x32*x41    
        ;

    return v_t_e_predent_700                             ;
}
float y_e_predent_700                             (float *x,int m){
    int ncoeff= 42;
    float avdat= -0.3560753E-03;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 43]={
         0.39920604E-03, 0.14731425E+00, 0.56232899E-01, 0.88604232E-02,
         0.39065522E-02,-0.18152879E-02,-0.11248299E-02,-0.18823532E-02,
        -0.13210883E-03,-0.31140473E-03,-0.11917565E-02,-0.51194249E-03,
        -0.31901663E-03,-0.10529440E-02,-0.25531944E-03,-0.10460881E-02,
        -0.81857643E-03,-0.21526117E-04, 0.24633980E-03, 0.16978763E-03,
        -0.15628710E-02,-0.27308641E-02,-0.13440309E-03,-0.16925593E-02,
        -0.37000544E-03,-0.10983957E-04, 0.19001156E-04,-0.32689114E-03,
         0.39142717E-04, 0.22172553E-05,-0.95092064E-05, 0.13289585E-03,
         0.23129081E-04, 0.66203153E-04, 0.27466460E-04, 0.67307563E-04,
         0.18187911E-04,-0.42211355E-04,-0.85099027E-05, 0.16720889E-04,
        -0.24012736E-04,-0.56441084E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_700                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_700                             =v_y_e_predent_700                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_700                             =v_y_e_predent_700                             
        +coeff[ 17]*x11*x21*x31        
        +coeff[ 18]        *x31*x42*x51
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]    *x22*x33        
        +coeff[ 25]            *x45*x51
    ;
    v_y_e_predent_700                             =v_y_e_predent_700                             
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]        *x31*x44    
        +coeff[ 28]        *x33    *x51
        +coeff[ 29]                *x51
        +coeff[ 30]*x12        *x41    
        +coeff[ 31]            *x43*x51
        +coeff[ 32]    *x22*x31    *x51
        +coeff[ 33]*x11*x21    *x43    
        +coeff[ 34]            *x41*x53
    ;
    v_y_e_predent_700                             =v_y_e_predent_700                             
        +coeff[ 35]*x11*x21*x31*x42    
        +coeff[ 36]        *x31    *x53
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]*x11        *x45    
        +coeff[ 39]    *x23*x31*x42    
        +coeff[ 40]*x13*x21    *x41    
        +coeff[ 41]    *x24    *x41*x51
        ;

    return v_y_e_predent_700                             ;
}
float p_e_predent_700                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.4906435E-05;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.31808755E-04,-0.71905129E-01,-0.76671705E-01,-0.14406456E-01,
        -0.15393807E-01, 0.66759172E-02, 0.11518518E-01,-0.55260090E-02,
         0.57313482E-04, 0.71587740E-03,-0.43618311E-02, 0.77285554E-03,
         0.15065185E-02,-0.21559624E-02,-0.12887897E-02, 0.59347518E-03,
        -0.56806096E-03,-0.13390659E-04,-0.58820803E-03,-0.11797051E-02,
         0.35518300E-03,-0.76887588E-03, 0.40658598E-03,-0.97793282E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_700                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_700                             =v_p_e_predent_700                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_700                             =v_p_e_predent_700                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_700                             ;
}
float l_e_predent_700                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4381801E-02;
    float xmin[10]={
        -0.39994E-02,-0.60047E-01,-0.53987E-01,-0.30014E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.43007503E-02,-0.88586556E-02,-0.15567259E-02,-0.55086007E-02,
        -0.62612770E-02, 0.47420894E-03,-0.10090329E-02, 0.11280735E-03,
        -0.48351471E-03, 0.99320209E-03, 0.82043908E-03,-0.30385758E-03,
         0.64748130E-03,-0.82946103E-03, 0.59225713E-03, 0.62134088E-03,
        -0.91623765E-03, 0.54116844E-03,-0.10567911E-02, 0.47961791E-03,
        -0.21850113E-02, 0.36017827E-04,-0.52519393E-03, 0.27941100E-02,
        -0.76842087E-03,-0.20251623E-02,-0.33829696E-02,-0.26087667E-03,
         0.25975907E-02, 0.28727540E-04, 0.73089592E-04, 0.25186231E-03,
         0.20271322E-04, 0.20897947E-04,-0.18259622E-03,-0.10665420E-02,
        -0.10686841E-04, 0.17111796E-03, 0.18801453E-03,-0.48212314E-03,
         0.36686318E-03, 0.10807384E-02,-0.81801397E-03,-0.21424128E-02,
         0.45253555E-03, 0.72457670E-03, 0.64214028E-03,-0.40357091E-03,
        -0.24775570E-03,-0.19767415E-03,-0.45252641E-03, 0.12600409E-02,
         0.50494960E-03,-0.92838251E-04, 0.42523857E-03,-0.74683216E-04,
        -0.63011231E-03,-0.53081132E-03, 0.27182393E-03, 0.34683477E-03,
        -0.52581995E-03, 0.10072801E-02, 0.71737581E-05, 0.31487446E-03,
         0.11317073E-02,-0.91088511E-03,-0.52645081E-03, 0.39117821E-03,
         0.32116656E-03,-0.50744892E-03, 0.35758183E-03,-0.33720199E-03,
        -0.63384895E-03,-0.38501908E-03,-0.32331795E-03, 0.75169036E-03,
         0.54532179E-03,-0.66381681E-03, 0.19355636E-03, 0.56020281E-03,
        -0.23081456E-02,-0.92774292E-03, 0.41510569E-03, 0.21657388E-03,
         0.29400381E-03, 0.13341464E-02,-0.10925476E-02, 0.12738316E-02,
         0.11977610E-02, 0.25197486E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_700                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x51
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]            *x42*x51
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]        *x31*x41*x52
        +coeff[ 13]*x11*x22    *x41    
        +coeff[ 14]*x11    *x32*x41    
        +coeff[ 15]*x12        *x42    
        +coeff[ 16]    *x22*x31*x41*x51
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 17]*x13        *x42    
        +coeff[ 18]    *x21*x32*x41*x52
        +coeff[ 19]    *x21*x31    *x54
        +coeff[ 20]*x11    *x32*x43    
        +coeff[ 21]*x11    *x31*x44    
        +coeff[ 22]*x12*x24            
        +coeff[ 23]*x11*x24*x31*x41    
        +coeff[ 24]*x11*x22*x31*x43    
        +coeff[ 25]*x13*x22*x31*x41    
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 26]*x13*x21*x31*x41*x51
        +coeff[ 27]*x13*x21        *x53
        +coeff[ 28]    *x21*x32*x41*x54
        +coeff[ 29]*x11                
        +coeff[ 30]    *x21    *x41    
        +coeff[ 31]            *x41*x51
        +coeff[ 32]    *x23            
        +coeff[ 33]        *x31    *x52
        +coeff[ 34]            *x41*x52
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 35]*x11        *x42    
        +coeff[ 36]*x12    *x31        
        +coeff[ 37]*x12        *x41    
        +coeff[ 38]    *x22*x31    *x51
        +coeff[ 39]    *x22    *x41*x51
        +coeff[ 40]    *x21    *x42*x51
        +coeff[ 41]*x11*x21*x31*x41    
        +coeff[ 42]*x11    *x31*x42    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 44]*x11    *x32    *x51
        +coeff[ 45]*x11    *x31*x41*x51
        +coeff[ 46]*x11        *x42*x51
        +coeff[ 47]*x12*x21        *x51
        +coeff[ 48]*x12        *x41*x51
        +coeff[ 49]*x12            *x52
        +coeff[ 50]    *x23*x31*x41    
        +coeff[ 51]    *x21*x31*x43    
        +coeff[ 52]    *x21    *x44    
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 53]*x13            *x51
        +coeff[ 54]    *x24        *x51
        +coeff[ 55]    *x21*x32*x41*x51
        +coeff[ 56]    *x21*x31*x42*x51
        +coeff[ 57]            *x44*x51
        +coeff[ 58]    *x22*x31    *x52
        +coeff[ 59]    *x21*x32    *x52
        +coeff[ 60]        *x33    *x52
        +coeff[ 61]*x11        *x44    
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 62]*x11    *x33    *x51
        +coeff[ 63]*x11*x22    *x41*x51
        +coeff[ 64]*x11*x21*x31*x41*x51
        +coeff[ 65]*x11    *x32*x41*x51
        +coeff[ 66]*x11*x21    *x42*x51
        +coeff[ 67]*x11        *x43*x51
        +coeff[ 68]*x11        *x42*x52
        +coeff[ 69]*x11        *x41*x53
        +coeff[ 70]*x12    *x33        
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 71]*x12*x21    *x41*x51
        +coeff[ 72]*x12    *x31*x41*x51
        +coeff[ 73]*x12        *x42*x51
        +coeff[ 74]    *x23*x33        
        +coeff[ 75]    *x21*x31*x44    
        +coeff[ 76]*x13        *x41*x51
        +coeff[ 77]    *x22*x31*x42*x51
        +coeff[ 78]    *x24        *x52
        +coeff[ 79]    *x21*x33    *x52
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 80]    *x21*x31*x42*x52
        +coeff[ 81]    *x21    *x43*x52
        +coeff[ 82]            *x44*x52
        +coeff[ 83]    *x21*x32    *x53
        +coeff[ 84]*x11*x23*x32        
        +coeff[ 85]*x11*x24    *x41    
        +coeff[ 86]*x11*x23*x31*x41    
        +coeff[ 87]*x11*x22*x31*x42    
        +coeff[ 88]*x11*x22    *x43    
    ;
    v_l_e_predent_700                             =v_l_e_predent_700                             
        +coeff[ 89]*x11*x24        *x51
        ;

    return v_l_e_predent_700                             ;
}
float x_e_predent_600                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066585E+01;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.10816739E-03,-0.30915071E-02, 0.91187738E-01, 0.43673138E-02,
        -0.22971923E-02,-0.17586024E-02,-0.42287909E-03, 0.24827919E-03,
        -0.72587217E-03,-0.87483320E-03,-0.27809385E-03,-0.17002923E-02,
        -0.13941516E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_600                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_600                             =v_x_e_predent_600                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_600                             ;
}
float t_e_predent_600                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3782207E+01;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.50592985E-01,-0.37400581E-01, 0.81429118E+00, 0.16895171E+00,
         0.37196442E-01,-0.15122529E-01, 0.26855430E-01, 0.16485309E-01,
        -0.18232347E-01,-0.19738324E-01,-0.91765570E-02, 0.27087475E-02,
        -0.55780434E-02,-0.77791489E-02, 0.78844139E-03, 0.50305243E-03,
        -0.31167127E-02,-0.18159732E-01,-0.14445273E-01, 0.55575445E-02,
        -0.58437465E-04, 0.12233686E-03,-0.14597310E-02,-0.57703485E-02,
        -0.30969910E-01,-0.23709405E-01,-0.17248664E-01, 0.37201794E-03,
         0.26123740E-02, 0.24656563E-02,-0.91117213E-03,-0.85715279E-02,
        -0.19272868E-01, 0.31136093E-03,-0.13515797E-03, 0.22972458E-03,
        -0.71244384E-03,-0.48163315E-03, 0.32787459E-03, 0.10935717E-02,
        -0.28929758E-03, 0.14958272E-02, 0.79657446E-04,-0.20276253E-03,
         0.87375476E-04,-0.13556566E-03,-0.22927916E-03,-0.25323773E-03,
         0.21442126E-03,-0.38061891E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_600                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]    *x22        *x51
    ;
    v_t_e_predent_600                             =v_t_e_predent_600                             
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11            *x51
        +coeff[ 12]*x11*x22            
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x31*x41    
        +coeff[ 15]            *x42    
        +coeff[ 16]    *x21        *x52
    ;
    v_t_e_predent_600                             =v_t_e_predent_600                             
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]        *x32    *x52
        +coeff[ 21]        *x32        
        +coeff[ 22]*x11*x23            
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_predent_600                             =v_t_e_predent_600                             
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]*x12                
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x22        *x52
        +coeff[ 31]    *x21*x33*x41    
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]                *x51
    ;
    v_t_e_predent_600                             =v_t_e_predent_600                             
        +coeff[ 35]*x12*x21            
        +coeff[ 36]*x11    *x31*x41    
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]*x11*x21        *x51
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x11*x22*x32        
        +coeff[ 41]    *x21*x31*x43*x51
        +coeff[ 42]    *x21*x31        
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_predent_600                             =v_t_e_predent_600                             
        +coeff[ 44]    *x22    *x41    
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x11*x21*x32        
        +coeff[ 47]    *x21*x33        
        +coeff[ 48]*x11*x21    *x42    
        +coeff[ 49]        *x31*x43    
        ;

    return v_t_e_predent_600                             ;
}
float y_e_predent_600                             (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.6683526E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.73057268E-03, 0.14703058E+00, 0.56086916E-01, 0.88979639E-02,
         0.39306339E-02,-0.18253799E-02,-0.11571443E-02,-0.18689743E-02,
        -0.17965365E-03,-0.34572207E-03,-0.11861407E-02,-0.51184284E-03,
        -0.31646655E-03,-0.10123806E-02,-0.26278268E-03,-0.10365971E-02,
        -0.80177461E-03, 0.10218886E-05,-0.15357644E-02,-0.26632457E-02,
        -0.12539072E-03,-0.16747662E-02,-0.35298371E-03, 0.23117883E-03,
        -0.17734013E-04, 0.16325201E-03,-0.38852185E-03,-0.70599199E-04,
         0.20607020E-04, 0.38925878E-05, 0.95701711E-04, 0.24771412E-04,
         0.34727378E-04,-0.22722425E-04,-0.25095118E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_predent_600                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_600                             =v_y_e_predent_600                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_600                             =v_y_e_predent_600                             
        +coeff[ 17]*x11*x21*x31        
        +coeff[ 18]    *x22    *x43    
        +coeff[ 19]    *x22*x31*x42    
        +coeff[ 20]        *x33*x42    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]        *x31*x42*x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_600                             =v_y_e_predent_600                             
        +coeff[ 26]        *x31*x44    
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]            *x45*x51
        +coeff[ 29]                *x51
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x22*x31    *x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]*x11*x21*x32*x41    
        +coeff[ 34]*x12*x22    *x41    
        ;

    return v_y_e_predent_600                             ;
}
float p_e_predent_600                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.2607264E-03;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.29558584E-03,-0.72037369E-01,-0.76865196E-01,-0.14450043E-01,
        -0.15436356E-01, 0.66782776E-02, 0.11525616E-01,-0.55306731E-02,
         0.18085559E-04, 0.15248975E-02, 0.71406527E-03,-0.43429234E-02,
         0.77717914E-03,-0.21511519E-02,-0.12985920E-02, 0.60692977E-03,
        -0.56389836E-03, 0.90987596E-05,-0.58012165E-03,-0.11793940E-02,
         0.35526062E-03,-0.78026159E-03, 0.41494524E-03,-0.10267590E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_600                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_600                             =v_p_e_predent_600                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]    *x22*x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_600                             =v_p_e_predent_600                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_600                             ;
}
float l_e_predent_600                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4403698E-02;
    float xmin[10]={
        -0.39996E-02,-0.60069E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60046E-01, 0.54000E-01, 0.30031E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44332575E-02,-0.92451265E-02,-0.15523731E-02,-0.61674062E-02,
        -0.66574235E-02, 0.41784867E-03,-0.36601161E-03, 0.41130767E-02,
         0.15149264E-03, 0.31895292E-04,-0.65299675E-04, 0.89354886E-04,
        -0.39620660E-03, 0.42542000E-03,-0.40113961E-03,-0.25310376E-03,
        -0.10797995E-02, 0.10213822E-02, 0.52865176E-03,-0.84451772E-03,
         0.10230594E-02, 0.10812945E-02,-0.12619521E-02, 0.13953147E-02,
        -0.93523070E-03, 0.14644241E-02,-0.14810943E-02,-0.15703829E-02,
        -0.11929835E-03,-0.13358581E-02, 0.21788571E-03, 0.17907319E-02,
         0.31098723E-03,-0.22183360E-03, 0.13369201E-03,-0.75367978E-03,
        -0.36447405E-03,-0.32324012E-03,-0.57680958E-04, 0.77812263E-03,
         0.54118820E-04, 0.12254030E-02, 0.73431968E-03, 0.15797145E-02,
         0.11468750E-02, 0.34591567E-03,-0.44197391E-03,-0.27348573E-03,
        -0.32921988E-03, 0.20600506E-03,-0.18647248E-03, 0.27171196E-03,
         0.61661872E-03,-0.11668466E-02,-0.65853976E-03,-0.37941179E-03,
         0.16565161E-03,-0.14099746E-02,-0.38120136E-03, 0.11855561E-02,
         0.28341991E-03,-0.68274670E-03,-0.17442898E-03,-0.11951182E-03,
        -0.61265193E-03,-0.69603458E-03, 0.79148864E-04, 0.16961350E-02,
         0.16636375E-04, 0.14232146E-02, 0.26102577E-03, 0.73769264E-03,
         0.13662096E-03,-0.19687039E-03, 0.29242382E-03,-0.29712077E-03,
        -0.36706511E-03, 0.32754481E-03, 0.97247050E-03,-0.70443726E-03,
         0.40760974E-03, 0.14571410E-03, 0.31543960E-03,-0.28815502E-03,
        -0.14987957E-02, 0.54894557E-03,-0.50080154E-03,-0.55878825E-03,
        -0.93568873E-03, 0.67466166E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_600                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]    *x22*x32*x42    
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[  8]        *x31        
        +coeff[  9]                *x51
        +coeff[ 10]    *x21        *x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]        *x33        
        +coeff[ 13]        *x31*x41*x51
        +coeff[ 14]        *x32*x42    
        +coeff[ 15]    *x21    *x41*x52
        +coeff[ 16]*x11*x21    *x41*x51
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 17]*x12*x21*x31        
        +coeff[ 18]            *x42*x53
        +coeff[ 19]*x12*x21*x31*x41    
        +coeff[ 20]*x12        *x42*x51
        +coeff[ 21]    *x24*x31*x41    
        +coeff[ 22]    *x22        *x54
        +coeff[ 23]    *x21    *x41*x54
        +coeff[ 24]*x11    *x34*x41    
        +coeff[ 25]*x11*x21    *x43*x51
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 26]*x12*x23*x31        
        +coeff[ 27]    *x24    *x42*x51
        +coeff[ 28]        *x32*x44*x51
        +coeff[ 29]    *x23*x32    *x52
        +coeff[ 30]    *x22*x32    *x53
        +coeff[ 31]    *x23*x33    *x52
        +coeff[ 32]    *x21*x32        
        +coeff[ 33]*x11*x22            
        +coeff[ 34]*x11*x21    *x41    
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 35]*x11    *x31*x41    
        +coeff[ 36]*x11    *x31    *x51
        +coeff[ 37]*x12*x21            
        +coeff[ 38]*x12            *x51
        +coeff[ 39]    *x22*x31*x41    
        +coeff[ 40]    *x21*x32*x41    
        +coeff[ 41]        *x31*x43    
        +coeff[ 42]            *x44    
        +coeff[ 43]    *x21*x31*x41*x51
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x11    *x33        
        +coeff[ 47]*x11    *x31*x42    
        +coeff[ 48]*x11    *x32    *x51
        +coeff[ 49]*x11        *x41*x52
        +coeff[ 50]*x12*x21        *x51
        +coeff[ 51]*x13*x21            
        +coeff[ 52]    *x24    *x41    
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 53]    *x22*x32*x41    
        +coeff[ 54]    *x22*x31*x42    
        +coeff[ 55]    *x22    *x43    
        +coeff[ 56]*x13            *x51
        +coeff[ 57]    *x22*x31*x41*x51
        +coeff[ 58]        *x33*x41*x51
        +coeff[ 59]        *x31*x43*x51
        +coeff[ 60]    *x21    *x42*x52
        +coeff[ 61]        *x31*x42*x52
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 62]    *x21    *x41*x53
        +coeff[ 63]            *x41*x54
        +coeff[ 64]*x11*x22*x32        
        +coeff[ 65]*x11*x22*x31*x41    
        +coeff[ 66]*x11    *x32*x42    
        +coeff[ 67]*x11    *x31*x43    
        +coeff[ 68]*x11        *x44    
        +coeff[ 69]*x11    *x31*x42*x51
        +coeff[ 70]*x11        *x43*x51
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 71]*x11*x22        *x52
        +coeff[ 72]*x11*x21        *x53
        +coeff[ 73]*x11            *x54
        +coeff[ 74]*x12*x23            
        +coeff[ 75]*x12*x21*x32        
        +coeff[ 76]*x12*x22        *x51
        +coeff[ 77]*x12    *x31    *x52
        +coeff[ 78]    *x24*x32        
        +coeff[ 79]    *x22*x34        
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 80]*x13        *x42    
        +coeff[ 81]    *x21*x34    *x51
        +coeff[ 82]*x13        *x41*x51
        +coeff[ 83]    *x23*x31*x41*x51
        +coeff[ 84]    *x21*x33*x41*x51
        +coeff[ 85]    *x23    *x42*x51
        +coeff[ 86]    *x24        *x52
        +coeff[ 87]    *x21*x33    *x52
        +coeff[ 88]    *x23    *x41*x52
    ;
    v_l_e_predent_600                             =v_l_e_predent_600                             
        +coeff[ 89]    *x21*x32*x41*x52
        ;

    return v_l_e_predent_600                             ;
}
float x_e_predent_500                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067349E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.88571518E-03,-0.30912729E-02, 0.91313921E-01, 0.43608500E-02,
        -0.22912989E-02,-0.17499421E-02,-0.46437449E-03, 0.25371552E-03,
        -0.70214248E-03,-0.85382845E-03,-0.27842683E-03,-0.17092119E-02,
        -0.14181671E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_500                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_500                             =v_x_e_predent_500                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_500                             ;
}
float t_e_predent_500                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3774660E+01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.42983346E-01,-0.37336964E-01, 0.81576818E+00, 0.16975784E+00,
         0.37135664E-01,-0.15168275E-01, 0.27454738E-01,-0.11321252E-03,
         0.16488876E-01,-0.18257406E-01,-0.19395282E-01,-0.98095983E-02,
         0.27841977E-02,-0.57257586E-02,-0.73198271E-02, 0.61481964E-03,
         0.50216273E-03,-0.30632934E-02,-0.17996721E-01,-0.14341072E-01,
         0.56918478E-02, 0.16135242E-04, 0.20178483E-03,-0.14847694E-02,
        -0.61048134E-02,-0.31228969E-01,-0.24419520E-01,-0.17784862E-01,
         0.40312586E-03, 0.43486856E-03, 0.33297506E-02, 0.24462263E-02,
        -0.80928340E-03,-0.84478380E-02,-0.20389240E-01, 0.91745234E-04,
         0.30389830E-03,-0.77773101E-03,-0.54885016E-03, 0.10892882E-02,
         0.14759215E-03, 0.51647535E-03, 0.12406717E-03,-0.29872771E-03,
        -0.15296100E-03, 0.93093448E-04, 0.11556322E-03,-0.21816044E-03,
         0.15258201E-03, 0.13376906E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_500                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_500                             =v_t_e_predent_500                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_500                             =v_t_e_predent_500                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x32    *x52
        +coeff[ 22]        *x32        
        +coeff[ 23]*x11*x23            
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_t_e_predent_500                             =v_t_e_predent_500                             
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x33*x41    
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_predent_500                             =v_t_e_predent_500                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x11    *x32    *x52
        +coeff[ 41]    *x21*x32*x43    
        +coeff[ 42]*x11*x21*x31        
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_predent_500                             =v_t_e_predent_500                             
        +coeff[ 44]    *x22    *x41    
        +coeff[ 45]        *x32*x41    
        +coeff[ 46]        *x31*x41*x51
        +coeff[ 47]*x11            *x52
        +coeff[ 48]*x12*x21*x31        
        +coeff[ 49]*x11*x22*x31        
        ;

    return v_t_e_predent_500                             ;
}
float y_e_predent_500                             (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.5203641E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.48609232E-03, 0.14671034E+00, 0.55834927E-01, 0.88987267E-02,
         0.39272802E-02,-0.18123904E-02,-0.11267465E-02,-0.18006301E-02,
        -0.14895049E-03,-0.42596392E-03,-0.11192892E-02,-0.51264465E-03,
        -0.31597054E-03,-0.10258043E-02,-0.24444732E-03,-0.10510006E-02,
        -0.80761017E-03, 0.11064761E-05,-0.15902240E-02,-0.27703200E-02,
        -0.23972435E-03,-0.17104519E-02,-0.38469542E-03,-0.25348232E-04,
         0.23937215E-03, 0.17007906E-03,-0.40893391E-03,-0.80995262E-04,
        -0.24785211E-05, 0.46671862E-05, 0.11523820E-03, 0.23831693E-04,
         0.38657719E-04, 0.85310312E-04,-0.68952577E-04, 0.23698965E-04,
         0.82183215E-04, 0.16995400E-04,-0.47116369E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_500                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_500                             =v_y_e_predent_500                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]        *x33        
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_500                             =v_y_e_predent_500                             
        +coeff[ 17]*x11*x21*x31        
        +coeff[ 18]    *x22    *x43    
        +coeff[ 19]    *x22*x31*x42    
        +coeff[ 20]        *x33*x42    
        +coeff[ 21]    *x22*x32*x41    
        +coeff[ 22]    *x22*x33        
        +coeff[ 23]    *x24    *x41*x51
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_500                             =v_y_e_predent_500                             
        +coeff[ 26]        *x31*x44    
        +coeff[ 27]*x11*x23*x31        
        +coeff[ 28]            *x45*x51
        +coeff[ 29]        *x31*x41    
        +coeff[ 30]            *x43*x51
        +coeff[ 31]    *x22*x31    *x51
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]*x11*x21    *x43    
        +coeff[ 34]        *x34*x41    
    ;
    v_y_e_predent_500                             =v_y_e_predent_500                             
        +coeff[ 35]            *x41*x53
        +coeff[ 36]*x11*x21*x31*x42    
        +coeff[ 37]        *x31    *x53
        +coeff[ 38]*x11*x23    *x41    
        ;

    return v_y_e_predent_500                             ;
}
float p_e_predent_500                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1958572E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.17899903E-03,-0.72194621E-01,-0.77183723E-01,-0.14508559E-01,
        -0.15543717E-01, 0.66928333E-02, 0.11543338E-01,-0.55706808E-02,
         0.22014381E-04, 0.71614905E-03,-0.43657962E-02, 0.77499537E-03,
         0.15250038E-02,-0.21613124E-02,-0.12970638E-02, 0.60181099E-03,
        -0.56330656E-03,-0.70579772E-05,-0.59241673E-03,-0.11837492E-02,
         0.34547228E-03,-0.77770301E-03, 0.40486804E-03,-0.10123430E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_500                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_500                             =v_p_e_predent_500                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_500                             =v_p_e_predent_500                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_500                             ;
}
float l_e_predent_500                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4401238E-02;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53985E-01,-0.30037E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60060E-01, 0.53992E-01, 0.30021E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44151116E-02,-0.93224468E-02,-0.18828944E-02,-0.54722214E-02,
        -0.59599434E-02, 0.62224490E-03,-0.16941072E-02, 0.37877719E-04,
        -0.98006029E-04, 0.22801172E-03, 0.12102748E-03, 0.32297827E-03,
         0.38839594E-03,-0.80816506E-04,-0.95795294E-04,-0.47897286E-03,
         0.58319536E-03,-0.28811806E-03,-0.31108226E-03, 0.44479952E-04,
         0.80389855E-03,-0.16083363E-02, 0.12284972E-02,-0.57307706E-03,
         0.12162382E-02, 0.96489454E-03, 0.87467540E-03,-0.12740681E-02,
        -0.10759262E-02,-0.73030510E-03, 0.24872045E-02, 0.15703393E-02,
         0.59789605E-03,-0.72114175E-03,-0.45672040E-02,-0.12704171E-03,
         0.97440340E-03, 0.28989094E-02,-0.15241138E-02, 0.10209926E-02,
        -0.19559374E-02,-0.12697741E-03,-0.24280696E-03, 0.44847387E-04,
         0.18267153E-03, 0.39514364E-03,-0.49169757E-03,-0.51711703E-03,
         0.24933546E-03, 0.33823427E-03, 0.21461943E-03, 0.75517622E-04,
         0.35682527E-03, 0.71878935E-03, 0.15427639E-03, 0.11545902E-03,
        -0.24886252E-03, 0.28257488E-03, 0.32283418E-03,-0.19598498E-03,
         0.52704813E-03,-0.12672997E-03, 0.19780031E-03,-0.69153582E-03,
        -0.32484277E-05,-0.53526682E-03, 0.49549853E-03,-0.11714153E-03,
        -0.56226953E-03, 0.11055857E-02,-0.49746613E-03, 0.18111544E-03,
         0.86105632E-03,-0.61036326E-03, 0.59557613E-04,-0.19215295E-03,
         0.28687619E-03,-0.73170225E-03,-0.18968038E-02, 0.67639089E-03,
         0.26913191E-03, 0.38875919E-03, 0.34333466E-03, 0.38073133E-03,
         0.43036131E-03, 0.43322801E-03, 0.61392668E-03, 0.71713777E-03,
         0.69306575E-03, 0.28850909E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_500                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[  8]        *x31        
        +coeff[  9]                *x51
        +coeff[ 10]        *x31*x42    
        +coeff[ 11]        *x31*x41*x51
        +coeff[ 12]            *x42*x51
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]*x12    *x31        
        +coeff[ 15]*x12        *x41    
        +coeff[ 16]    *x22*x32        
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]            *x43*x51
        +coeff[ 19]    *x21        *x53
        +coeff[ 20]    *x24        *x51
        +coeff[ 21]    *x22*x32    *x51
        +coeff[ 22]*x11*x21*x32    *x51
        +coeff[ 23]*x11*x22    *x41*x51
        +coeff[ 24]*x11        *x41*x53
        +coeff[ 25]    *x22*x33*x41    
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 26]        *x32*x41*x53
        +coeff[ 27]    *x22        *x54
        +coeff[ 28]*x11*x21    *x42*x52
        +coeff[ 29]*x13*x21*x31*x41    
        +coeff[ 30]    *x22*x34    *x51
        +coeff[ 31]    *x21*x33*x41*x52
        +coeff[ 32]*x11*x23*x33        
        +coeff[ 33]*x11    *x34*x41*x51
        +coeff[ 34]*x11*x21*x32*x42*x51
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 35]*x11*x21*x31*x43*x51
        +coeff[ 36]*x12*x21    *x42*x52
        +coeff[ 37]    *x23*x33*x41*x51
        +coeff[ 38]*x13*x21*x31    *x52
        +coeff[ 39]    *x24*x32    *x52
        +coeff[ 40]    *x21*x33*x41*x53
        +coeff[ 41]    *x21            
        +coeff[ 42]*x12                
        +coeff[ 43]    *x23            
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 44]*x11    *x32        
        +coeff[ 45]*x11        *x42    
        +coeff[ 46]*x11        *x41*x51
        +coeff[ 47]*x11            *x52
        +coeff[ 48]        *x34        
        +coeff[ 49]    *x21*x32*x41    
        +coeff[ 50]    *x23        *x51
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x22    *x41*x51
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 53]    *x22        *x52
        +coeff[ 54]    *x21*x31    *x52
        +coeff[ 55]        *x31    *x53
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x22*x31        
        +coeff[ 58]*x11*x22        *x51
        +coeff[ 59]*x11*x21*x31    *x51
        +coeff[ 60]*x12*x22            
        +coeff[ 61]*x13    *x31        
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 62]    *x21*x34        
        +coeff[ 63]    *x24    *x41    
        +coeff[ 64]    *x22*x32*x41    
        +coeff[ 65]    *x22*x31*x42    
        +coeff[ 66]    *x22    *x43    
        +coeff[ 67]*x13            *x51
        +coeff[ 68]    *x22*x31*x41*x51
        +coeff[ 69]    *x21*x31*x42*x51
        +coeff[ 70]        *x32*x42*x51
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 71]    *x23        *x52
        +coeff[ 72]        *x32*x41*x52
        +coeff[ 73]    *x21*x31    *x53
        +coeff[ 74]    *x21    *x41*x53
        +coeff[ 75]            *x41*x54
        +coeff[ 76]*x11*x22    *x42    
        +coeff[ 77]*x11        *x44    
        +coeff[ 78]*x11*x21*x31*x41*x51
        +coeff[ 79]*x11*x21*x31    *x52
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 80]*x11        *x42*x52
        +coeff[ 81]*x11            *x54
        +coeff[ 82]*x12    *x33        
        +coeff[ 83]*x12*x22    *x41    
        +coeff[ 84]*x12        *x43    
        +coeff[ 85]*x12*x22        *x51
        +coeff[ 86]*x12*x21*x31    *x51
        +coeff[ 87]*x12*x21    *x41*x51
        +coeff[ 88]*x12    *x31*x41*x51
    ;
    v_l_e_predent_500                             =v_l_e_predent_500                             
        +coeff[ 89]*x12    *x31    *x52
        ;

    return v_l_e_predent_500                             ;
}
float x_e_predent_450                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066807E+01;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.35386009E-03,-0.30901749E-02, 0.91398932E-01, 0.43600644E-02,
        -0.23130146E-02,-0.17820554E-02,-0.48407292E-03, 0.25369122E-03,
        -0.70756522E-03,-0.84813783E-03,-0.27774533E-03,-0.16375238E-02,
        -0.13291205E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_450                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_450                             =v_x_e_predent_450                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_450                             ;
}
float t_e_predent_450                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3780297E+01;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.48501104E-01,-0.37461266E-01, 0.81687731E+00, 0.17030144E+00,
         0.37263867E-01,-0.15262838E-01, 0.27569983E-01,-0.11569695E-03,
         0.16443038E-01,-0.18910600E-01,-0.19637924E-01,-0.10196288E-01,
         0.27652658E-02,-0.57009337E-02,-0.71770409E-02,-0.31022911E-02,
         0.81075937E-03, 0.45986651E-03,-0.17860375E-01,-0.14526094E-01,
         0.54295114E-02, 0.12308951E-03, 0.16518249E-03,-0.12156181E-02,
        -0.59618433E-02,-0.30513305E-01,-0.23506055E-01, 0.35615970E-03,
         0.37232702E-03, 0.32776566E-02, 0.25195139E-02,-0.92187547E-03,
        -0.19653501E-01,-0.16695332E-01, 0.24374673E-03,-0.61196001E-03,
        -0.42778606E-03,-0.20798734E-03,-0.47548881E-03, 0.86838251E-03,
        -0.40091833E-03,-0.81347227E-02,-0.52268995E-03, 0.14015571E-03,
         0.13435671E-03, 0.12999952E-03,-0.89012479E-04, 0.88157780E-04,
         0.36527644E-03, 0.23992643E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_450                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_450                             =v_t_e_predent_450                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]    *x21        *x52
        +coeff[ 16]        *x31*x41    
    ;
    v_t_e_predent_450                             =v_t_e_predent_450                             
        +coeff[ 17]            *x42    
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x32    *x52
        +coeff[ 22]        *x32        
        +coeff[ 23]*x11*x23            
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_t_e_predent_450                             =v_t_e_predent_450                             
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]*x12                
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x23*x32    *x51
    ;
    v_t_e_predent_450                             =v_t_e_predent_450                             
        +coeff[ 35]*x11    *x31*x41    
        +coeff[ 36]*x11        *x42    
        +coeff[ 37]*x11            *x52
        +coeff[ 38]        *x31*x43    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x21*x33*x41    
        +coeff[ 42]*x11*x23        *x52
        +coeff[ 43]*x13                
    ;
    v_t_e_predent_450                             =v_t_e_predent_450                             
        +coeff[ 44]*x12*x21            
        +coeff[ 45]*x11*x21*x31        
        +coeff[ 46]            *x41*x52
        +coeff[ 47]*x13    *x31        
        +coeff[ 48]*x11*x21*x31*x41    
        +coeff[ 49]*x11*x21    *x42    
        ;

    return v_t_e_predent_450                             ;
}
float y_e_predent_450                             (float *x,int m){
    int ncoeff= 45;
    float avdat= -0.3751633E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 46]={
         0.35742577E-03, 0.14652543E+00, 0.55701204E-01, 0.88985376E-02,
         0.39297380E-02,-0.21915687E-02,-0.13269682E-02,-0.21044726E-02,
         0.33187622E-04, 0.63323341E-05,-0.12943440E-02,-0.27542221E-03,
        -0.51583099E-03,-0.31516346E-03,-0.12529676E-02,-0.43291910E-03,
        -0.75201603E-03,-0.62944350E-03,-0.10989739E-02,-0.30458730E-02,
        -0.83859812E-03,-0.19817412E-03,-0.87274006E-04,-0.15030696E-04,
         0.24815471E-03, 0.15295585E-03,-0.43937151E-03,-0.28224096E-04,
        -0.28388028E-02,-0.95743156E-03,-0.14400849E-02,-0.17236214E-02,
         0.14082268E-03, 0.12702537E-04, 0.36455669E-04,-0.15476960E-03,
        -0.41071573E-03, 0.23520763E-04, 0.34592187E-04, 0.30529001E-04,
         0.22479515E-04,-0.44389493E-04,-0.22626880E-04,-0.65668770E-04,
         0.52819771E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_450                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_450                             =v_y_e_predent_450                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_450                             =v_y_e_predent_450                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_450                             =v_y_e_predent_450                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24*x31*x42    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]            *x43*x51
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_predent_450                             =v_y_e_predent_450                             
        +coeff[ 35]    *x22*x33        
        +coeff[ 36]    *x22*x34*x41    
        +coeff[ 37]    *x22*x31    *x51
        +coeff[ 38]            *x41*x53
        +coeff[ 39]*x11*x21*x31*x42    
        +coeff[ 40]        *x31    *x53
        +coeff[ 41]*x11*x23*x31        
        +coeff[ 42]*x11    *x31*x43*x51
        +coeff[ 43]    *x24    *x41*x51
    ;
    v_y_e_predent_450                             =v_y_e_predent_450                             
        +coeff[ 44]    *x22*x32*x41*x51
        ;

    return v_y_e_predent_450                             ;
}
float p_e_predent_450                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.3471845E-03;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.33971263E-03,-0.72311811E-01,-0.77397615E-01,-0.14542586E-01,
        -0.15605604E-01, 0.66923797E-02, 0.11552865E-01,-0.55808690E-02,
         0.17150207E-04, 0.71983045E-03,-0.43641273E-02, 0.77671953E-03,
         0.15310295E-02,-0.21573468E-02,-0.12949287E-02, 0.60182734E-03,
        -0.56366692E-03,-0.20018222E-04,-0.58301567E-03,-0.11718890E-02,
         0.35617340E-03,-0.79885492E-03, 0.42528828E-03,-0.10343775E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_450                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_450                             =v_p_e_predent_450                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]*x11    *x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]        *x31*x42    
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x21*x31    *x51
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_450                             =v_p_e_predent_450                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_450                             ;
}
float l_e_predent_450                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4454290E-02;
    float xmin[10]={
        -0.39997E-02,-0.60053E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.30026E-01, 0.39976E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.42791204E-02,-0.44871886E-04,-0.89131966E-02,-0.10604373E-02,
        -0.56630122E-02,-0.61146794E-02, 0.35041885E-03,-0.47593433E-03,
         0.30364492E-03,-0.43416239E-03, 0.14661734E-03,-0.27385086E-04,
         0.87236060E-03,-0.21832270E-03,-0.71801122E-04,-0.33762466E-03,
         0.34301376E-03, 0.13497313E-02, 0.11597653E-02,-0.12876555E-04,
        -0.77171187E-03, 0.94534468E-03, 0.49320457E-03, 0.20436000E-02,
         0.55796257E-03, 0.17930578E-02, 0.10004602E-02, 0.13560661E-02,
         0.11561217E-02, 0.22746129E-03, 0.83778863E-03,-0.10817958E-02,
        -0.18068758E-02,-0.12156511E-03, 0.46534133E-04,-0.37090995E-03,
         0.80277139E-04, 0.29926849E-03, 0.17296617E-03, 0.93545386E-03,
        -0.88042136E-04,-0.21392097E-03,-0.17729071E-03, 0.12521184E-03,
         0.20987212E-03,-0.63399450E-04, 0.14492724E-03, 0.12426046E-03,
         0.20119124E-03, 0.14342384E-03, 0.20171655E-03,-0.14609496E-03,
        -0.31187109E-03, 0.38313709E-03,-0.41618856E-03, 0.95495931E-03,
         0.64301444E-03,-0.94400340E-04, 0.24905446E-03, 0.41683207E-03,
        -0.13893575E-03, 0.19040360E-03,-0.21464514E-03, 0.48503661E-03,
         0.23420030E-03,-0.10934372E-03, 0.62312739E-03, 0.27112570E-03,
        -0.46821221E-03, 0.37450681E-03,-0.24521782E-03,-0.18019562E-03,
        -0.77437342E-03,-0.46894446E-03,-0.40144718E-03, 0.32342793E-03,
        -0.10644284E-02, 0.63113304E-03, 0.60876244E-03,-0.65224554E-03,
        -0.77770685E-03,-0.33331019E-03,-0.94460370E-03, 0.28370295E-03,
        -0.55595418E-03,-0.45329300E-04,-0.70516346E-03,-0.44812472E-03,
         0.28143890E-03, 0.19574653E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_450                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[  8]            *x42*x51
        +coeff[  9]*x11            *x54
        +coeff[ 10]                *x51
        +coeff[ 11]*x12                
        +coeff[ 12]        *x31*x41*x51
        +coeff[ 13]    *x22*x32        
        +coeff[ 14]        *x32*x42    
        +coeff[ 15]    *x21    *x41*x52
        +coeff[ 16]*x11    *x31*x42    
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 17]    *x21*x32*x41*x51
        +coeff[ 18]    *x21*x31*x42*x51
        +coeff[ 19]*x11*x23        *x51
        +coeff[ 20]*x11*x22    *x41*x51
        +coeff[ 21]*x11    *x31*x42*x51
        +coeff[ 22]*x12*x21*x31*x41    
        +coeff[ 23]*x11*x21*x32*x42    
        +coeff[ 24]*x11*x21*x31*x41*x52
        +coeff[ 25]*x12    *x31*x43    
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 26]*x12*x22*x31    *x51
        +coeff[ 27]*x13*x21    *x41*x51
        +coeff[ 28]*x11*x23    *x42*x51
        +coeff[ 29]*x12*x23*x32        
        +coeff[ 30]*x12    *x31*x44    
        +coeff[ 31]*x13*x22*x31    *x51
        +coeff[ 32]    *x23*x31*x43*x51
        +coeff[ 33]    *x21            
        +coeff[ 34]*x11                
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 35]        *x31    *x51
        +coeff[ 36]            *x41*x51
        +coeff[ 37]    *x23            
        +coeff[ 38]    *x22*x31        
        +coeff[ 39]    *x22    *x41    
        +coeff[ 40]    *x21*x31*x41    
        +coeff[ 41]            *x43    
        +coeff[ 42]            *x41*x52
        +coeff[ 43]*x11*x22            
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 44]*x11*x21*x31        
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]*x11*x21    *x41    
        +coeff[ 47]*x11    *x31*x41    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]*x12    *x31        
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x12            *x51
        +coeff[ 52]    *x23*x31        
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 53]    *x21*x33        
        +coeff[ 54]        *x34        
        +coeff[ 55]    *x22*x31*x41    
        +coeff[ 56]    *x22    *x42    
        +coeff[ 57]    *x21*x32    *x51
        +coeff[ 58]        *x33    *x51
        +coeff[ 59]    *x21*x31*x41*x51
        +coeff[ 60]    *x22        *x52
        +coeff[ 61]        *x32    *x52
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 62]        *x31*x41*x52
        +coeff[ 63]        *x31    *x53
        +coeff[ 64]            *x41*x53
        +coeff[ 65]                *x54
        +coeff[ 66]*x11*x21*x31*x41    
        +coeff[ 67]*x11*x21*x31    *x51
        +coeff[ 68]*x11*x21    *x41*x51
        +coeff[ 69]*x11            *x53
        +coeff[ 70]*x12*x22            
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 71]*x12*x21*x31        
        +coeff[ 72]*x12    *x31*x41    
        +coeff[ 73]*x12    *x31    *x51
        +coeff[ 74]*x12        *x41*x51
        +coeff[ 75]*x13*x21            
        +coeff[ 76]    *x24    *x41    
        +coeff[ 77]    *x23*x31*x41    
        +coeff[ 78]    *x22*x32*x41    
        +coeff[ 79]        *x34*x41    
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 80]        *x33*x42    
        +coeff[ 81]*x13            *x51
        +coeff[ 82]    *x22*x32    *x51
        +coeff[ 83]        *x34    *x51
        +coeff[ 84]    *x22*x31*x41*x51
        +coeff[ 85]        *x32*x42*x51
        +coeff[ 86]        *x31*x43*x51
        +coeff[ 87]    *x22*x31    *x52
        +coeff[ 88]    *x21*x32    *x52
    ;
    v_l_e_predent_450                             =v_l_e_predent_450                             
        +coeff[ 89]        *x33    *x52
        ;

    return v_l_e_predent_450                             ;
}
float x_e_predent_449                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067248E+01;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.77794609E-03,-0.30424106E-02, 0.92368923E-01, 0.42908727E-02,
        -0.24682779E-02,-0.17367662E-02,-0.62310882E-03, 0.24372958E-03,
        -0.70163084E-03,-0.10044127E-02,-0.26786141E-03,-0.19276342E-02,
        -0.14058662E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_449                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_449                             =v_x_e_predent_449                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_449                             ;
}
float t_e_predent_449                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3776892E+01;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.45232423E-01,-0.36850952E-01, 0.82594419E+00, 0.17402025E+00,
         0.36274422E-01,-0.15073966E-01, 0.28644549E-01,-0.16799604E-03,
         0.16399633E-01,-0.20473592E-01,-0.19377453E-01,-0.12239377E-01,
         0.26627285E-02,-0.57804128E-02,-0.91865528E-02, 0.91938768E-03,
         0.42237993E-03,-0.29455121E-02,-0.20274401E-01,-0.14520403E-01,
         0.58504799E-02,-0.38382714E-04,-0.34607291E-01,-0.24055315E-01,
        -0.27826967E-03, 0.16539653E-03,-0.16228346E-02,-0.71627568E-02,
         0.36866657E-03, 0.41344433E-03, 0.33871885E-02, 0.20617715E-02,
        -0.78154256E-03,-0.22991231E-01,-0.18206723E-01, 0.23920233E-03,
        -0.74597291E-03,-0.46714602E-03,-0.48096423E-03, 0.12562951E-02,
        -0.15268008E-04,-0.10564789E-01,-0.13427618E-03,-0.53880676E-05,
         0.17097419E-03,-0.12286018E-03,-0.30885593E-03,-0.91099930E-04,
        -0.10686858E-03, 0.18449398E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_449                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_449                             =v_t_e_predent_449                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_449                             =v_t_e_predent_449                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x22*x32    *x52
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_449                             =v_t_e_predent_449                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_449                             =v_t_e_predent_449                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x11    *x31*x41    
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]        *x31*x43    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x21*x33*x41    
        +coeff[ 42]    *x21*x31        
        +coeff[ 43]*x13                
    ;
    v_t_e_predent_449                             =v_t_e_predent_449                             
        +coeff[ 44]*x12*x21            
        +coeff[ 45]    *x22*x31        
        +coeff[ 46]*x11    *x32        
        +coeff[ 47]    *x21*x31    *x51
        +coeff[ 48]*x11            *x52
        +coeff[ 49]*x12*x21*x31        
        ;

    return v_t_e_predent_449                             ;
}
float y_e_predent_449                             (float *x,int m){
    int ncoeff= 44;
    float avdat= -0.8170342E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 45]={
         0.82436507E-03, 0.14764963E+00, 0.62372498E-01, 0.87915407E-02,
         0.42894892E-02,-0.20424256E-02,-0.14179429E-02,-0.22763170E-02,
        -0.13162772E-04,-0.14076456E-04,-0.15352853E-02,-0.36197031E-03,
        -0.51075872E-03,-0.34694074E-03,-0.11573744E-02,-0.11175654E-02,
        -0.87348465E-03,-0.74094336E-03,-0.76980778E-03,-0.30446628E-02,
        -0.78754913E-03,-0.23167401E-03, 0.73403891E-04,-0.53771528E-05,
         0.27596971E-03, 0.20844504E-03,-0.91529021E-03, 0.53156855E-05,
        -0.24328204E-02,-0.59005583E-03,-0.11700426E-02,-0.18116918E-02,
         0.27065948E-04, 0.50371291E-04,-0.56062260E-03,-0.28722489E-03,
        -0.58906291E-04,-0.48181353E-03, 0.11522170E-03, 0.25088500E-04,
         0.31519914E-04, 0.19444511E-04,-0.11163456E-04,-0.61783590E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_449                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_449                             =v_y_e_predent_449                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_449                             =v_y_e_predent_449                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_449                             =v_y_e_predent_449                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24*x31*x42    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]    *x22    *x41*x51
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_predent_449                             =v_y_e_predent_449                             
        +coeff[ 35]    *x22*x33        
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]    *x22*x34*x41    
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x22*x31    *x51
        +coeff[ 40]            *x41*x53
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]            *x44*x52
        +coeff[ 43]    *x24    *x41*x51
        ;

    return v_y_e_predent_449                             ;
}
float p_e_predent_449                             (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.2221959E-03;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.22852291E-03,-0.79249367E-01,-0.75628847E-01,-0.16126283E-01,
        -0.15420195E-01, 0.73316544E-02, 0.11426550E-01,-0.48822849E-02,
        -0.56179520E-02, 0.77576027E-03, 0.74925087E-03, 0.15579886E-02,
        -0.23348634E-02,-0.12788979E-02, 0.69613371E-03,-0.55525365E-03,
         0.24858842E-04,-0.66593400E-03,-0.14273261E-02, 0.38414396E-03,
        -0.89526043E-03, 0.40244311E-03,-0.10460824E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_449                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_predent_449                             =v_p_e_predent_449                             
        +coeff[  8]    *x22    *x41    
        +coeff[  9]*x11    *x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]            *x41*x52
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_predent_449                             =v_p_e_predent_449                             
        +coeff[ 17]        *x33    *x52
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x23    *x41    
        ;

    return v_p_e_predent_449                             ;
}
float l_e_predent_449                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4421339E-02;
    float xmin[10]={
        -0.39990E-02,-0.60067E-01,-0.59981E-01,-0.30024E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60054E-01, 0.59989E-01, 0.30025E-01, 0.39975E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44488315E-02,-0.92967805E-02,-0.20051734E-02,-0.60411338E-02,
        -0.62658992E-02, 0.71685790E-03, 0.85449516E-04, 0.86396554E-03,
        -0.95720566E-03, 0.71627164E-03, 0.40022816E-03, 0.50044246E-03,
         0.53637795E-03,-0.11694205E-02, 0.35446408E-03, 0.23491206E-03,
         0.76733204E-03, 0.71386195E-03, 0.76415506E-03, 0.85586612E-03,
        -0.37558985E-03,-0.82194374E-03,-0.16124556E-02, 0.24583591E-02,
         0.24119809E-02,-0.10965989E-03,-0.16178169E-03,-0.43895960E-03,
         0.16566759E-03,-0.49549591E-04,-0.23954312E-03, 0.92406546E-04,
        -0.12544632E-02, 0.14667958E-03,-0.34695354E-03,-0.21157517E-04,
         0.28336822E-03, 0.17381954E-02, 0.50053024E-03, 0.37565737E-03,
        -0.11378797E-03,-0.27004504E-03,-0.28998562E-03, 0.24669248E-03,
         0.36203748E-03,-0.65799621E-04,-0.26919585E-03,-0.19194232E-03,
         0.34938750E-03, 0.48723689E-03,-0.49260445E-03, 0.95550017E-03,
         0.65694097E-03,-0.51632256E-03,-0.35017036E-03, 0.84440585E-03,
        -0.36153919E-03,-0.60252409E-03, 0.30879691E-03,-0.48485078E-03,
        -0.60279685E-03,-0.69795595E-03,-0.94514654E-03,-0.78366703E-03,
         0.54893546E-04,-0.39469398E-03, 0.20532633E-03, 0.76395395E-03,
        -0.52025175E-03, 0.30448416E-03, 0.56776212E-03, 0.29099963E-03,
        -0.45507986E-03,-0.29156712E-03, 0.12700680E-02,-0.12249976E-02,
         0.71792019E-03, 0.31920092E-03,-0.12508536E-02, 0.33737766E-03,
         0.11334281E-02,-0.95406215E-03, 0.61242218E-03, 0.31883133E-03,
        -0.10781486E-02, 0.81010693E-03,-0.11390379E-02,-0.16166280E-03,
        -0.10557689E-02,-0.12223806E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_449                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]            *x41    
        +coeff[  7]    *x21*x31        
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]        *x32    *x51
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]*x12    *x31        
        +coeff[ 13]    *x21*x33        
        +coeff[ 14]    *x22*x31    *x51
        +coeff[ 15]    *x24*x31        
        +coeff[ 16]*x11*x21*x32*x41    
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 17]*x11    *x34*x41    
        +coeff[ 18]*x11    *x31*x42*x52
        +coeff[ 19]*x12*x23        *x51
        +coeff[ 20]*x12*x21        *x53
        +coeff[ 21]    *x24*x32*x41    
        +coeff[ 22]*x11*x22*x33    *x51
        +coeff[ 23]*x12    *x34*x41    
        +coeff[ 24]*x12    *x31    *x54
        +coeff[ 25]    *x21        *x51
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 26]    *x23            
        +coeff[ 27]    *x22*x31        
        +coeff[ 28]    *x21*x32        
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]*x11            *x52
        +coeff[ 31]*x13                
        +coeff[ 32]    *x23*x31        
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]        *x31*x43    
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 35]    *x23        *x51
        +coeff[ 36]    *x21*x32    *x51
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]            *x42*x52
        +coeff[ 40]                *x54
        +coeff[ 41]*x11*x23            
        +coeff[ 42]*x11*x21*x32        
        +coeff[ 43]*x11*x21    *x42    
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 44]*x11*x22        *x51
        +coeff[ 45]*x11    *x32    *x51
        +coeff[ 46]*x11        *x42*x51
        +coeff[ 47]*x12*x22            
        +coeff[ 48]*x12    *x32        
        +coeff[ 49]*x12    *x31*x41    
        +coeff[ 50]    *x23*x31*x41    
        +coeff[ 51]    *x21*x32*x42    
        +coeff[ 52]        *x33*x42    
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 53]    *x21    *x44    
        +coeff[ 54]        *x31*x44    
        +coeff[ 55]    *x24        *x51
        +coeff[ 56]    *x23*x31    *x51
        +coeff[ 57]    *x22*x32    *x51
        +coeff[ 58]    *x21*x33    *x51
        +coeff[ 59]        *x32*x42*x51
        +coeff[ 60]    *x22*x31    *x52
        +coeff[ 61]    *x21*x32    *x52
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 62]        *x32*x41*x52
        +coeff[ 63]        *x31*x42*x52
        +coeff[ 64]            *x43*x52
        +coeff[ 65]        *x32    *x53
        +coeff[ 66]    *x21        *x54
        +coeff[ 67]*x11*x23*x31        
        +coeff[ 68]*x11*x21*x33        
        +coeff[ 69]*x11    *x33*x41    
        +coeff[ 70]*x11*x22*x31    *x51
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 71]*x11    *x32    *x52
        +coeff[ 72]*x11*x21        *x53
        +coeff[ 73]*x12    *x33        
        +coeff[ 74]*x12*x21*x31*x41    
        +coeff[ 75]*x12    *x32*x41    
        +coeff[ 76]*x12*x21    *x42    
        +coeff[ 77]*x12*x21    *x41*x51
        +coeff[ 78]*x12    *x31    *x52
        +coeff[ 79]*x12        *x41*x52
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 80]    *x23*x33        
        +coeff[ 81]    *x23*x32*x41    
        +coeff[ 82]    *x21*x34*x41    
        +coeff[ 83]*x13*x21        *x51
        +coeff[ 84]    *x24*x31    *x51
        +coeff[ 85]    *x23*x32    *x51
        +coeff[ 86]    *x21*x34    *x51
        +coeff[ 87]*x13        *x41*x51
        +coeff[ 88]    *x22*x32*x41*x51
    ;
    v_l_e_predent_449                             =v_l_e_predent_449                             
        +coeff[ 89]        *x34*x41*x51
        ;

    return v_l_e_predent_449                             ;
}
float x_e_predent_400                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066225E+01;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
        -0.24593569E-03,-0.30337705E-02, 0.92589684E-01, 0.42732963E-02,
        -0.24886534E-02,-0.17364842E-02,-0.58604981E-03, 0.24268792E-03,
        -0.70762890E-03,-0.10259253E-02,-0.26767523E-03,-0.18603605E-02,
        -0.13691853E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_400                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_400                             =v_x_e_predent_400                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_400                             ;
}
float t_e_predent_400                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3786688E+01;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.55092484E-01,-0.36741205E-01, 0.82827526E+00, 0.17518108E+00,
         0.35771847E-01,-0.15160963E-01, 0.29019030E-01,-0.13206058E-03,
         0.16298538E-01,-0.19994611E-01,-0.19039057E-01,-0.11856124E-01,
         0.26574316E-02,-0.58020437E-02,-0.94046909E-02, 0.93076099E-03,
         0.56660449E-03,-0.29604887E-02,-0.20555386E-01,-0.14504808E-01,
         0.58464208E-02,-0.46669899E-06,-0.34272052E-01,-0.23843834E-01,
        -0.16185640E-03, 0.32563898E-03,-0.16475441E-02,-0.75803325E-02,
         0.37290971E-03, 0.36810858E-02, 0.22982485E-02,-0.81849849E-03,
        -0.24200654E-01,-0.18590791E-01, 0.89751986E-04,-0.36698158E-03,
         0.23260262E-03,-0.72011352E-03,-0.46965174E-03, 0.16362551E-02,
         0.43855573E-03,-0.11619792E-01,-0.28491649E-03,-0.30909275E-03,
         0.31543511E-03, 0.11356486E-03,-0.18920301E-03,-0.77630411E-03,
        -0.71859267E-03, 0.33080165E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_400                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_400                             =v_t_e_predent_400                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_400                             =v_t_e_predent_400                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x22*x32    *x52
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_400                             =v_t_e_predent_400                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x12                
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]*x13*x21        *x51
    ;
    v_t_e_predent_400                             =v_t_e_predent_400                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]    *x21        *x53
        +coeff[ 41]    *x21*x33*x41    
        +coeff[ 42]*x13            *x52
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_predent_400                             =v_t_e_predent_400                             
        +coeff[ 44]*x11*x21        *x51
        +coeff[ 45]        *x31*x41*x51
        +coeff[ 46]*x11*x21*x32        
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]*x11*x21        *x52
        ;

    return v_t_e_predent_400                             ;
}
float y_e_predent_400                             (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.8493165E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.89010521E-03, 0.14754514E+00, 0.62199753E-01, 0.88033006E-02,
         0.43088375E-02,-0.18423845E-02,-0.12833965E-02,-0.22259485E-02,
        -0.90464280E-04,-0.11497093E-02,-0.15103604E-02,-0.34432992E-03,
        -0.50511974E-03,-0.34525522E-03,-0.25978379E-02,-0.11031646E-02,
        -0.90066425E-03,-0.35778506E-03,-0.55967638E-03, 0.10204579E-03,
         0.27740825E-04,-0.63256230E-04,-0.58148330E-05, 0.31596858E-05,
        -0.13046892E-02,-0.19455412E-02,-0.52351080E-03, 0.24132308E-03,
         0.19625488E-03,-0.66039807E-04,-0.23053297E-04,-0.96747332E-03,
         0.26459478E-04, 0.15347204E-03, 0.18479273E-04, 0.32691176E-04,
        -0.10410745E-03, 0.20769665E-04,-0.25521222E-04,-0.63256761E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_400                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_400                             =v_y_e_predent_400                             
        +coeff[  8]        *x32*x43    
        +coeff[  9]            *x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]    *x22*x31*x42    
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_400                             =v_y_e_predent_400                             
        +coeff[ 17]    *x22    *x45    
        +coeff[ 18]    *x22*x32*x43    
        +coeff[ 19]    *x24*x32*x41    
        +coeff[ 20]    *x24*x33        
        +coeff[ 21]    *x22*x32*x45    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x22    *x43    
        +coeff[ 25]    *x22*x32*x41    
    ;
    v_y_e_predent_400                             =v_y_e_predent_400                             
        +coeff[ 26]    *x22*x33        
        +coeff[ 27]        *x31*x42*x51
        +coeff[ 28]        *x32*x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]            *x45*x51
        +coeff[ 31]    *x22*x31*x44    
        +coeff[ 32]        *x33    *x53
        +coeff[ 33]            *x43*x51
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_predent_400                             =v_y_e_predent_400                             
        +coeff[ 35]        *x33    *x51
        +coeff[ 36]        *x33*x42    
        +coeff[ 37]            *x41*x53
        +coeff[ 38]*x11*x21*x32*x41    
        +coeff[ 39]    *x22    *x43*x51
        ;

    return v_y_e_predent_400                             ;
}
float p_e_predent_400                             (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.2168668E-03;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.24457087E-03,-0.79279333E-01,-0.75711511E-01,-0.16166611E-01,
        -0.15483106E-01, 0.73338011E-02, 0.11435268E-01,-0.49148044E-02,
        -0.56504277E-02, 0.77252428E-03, 0.74369414E-03, 0.15664137E-02,
        -0.23381202E-02,-0.12680658E-02, 0.71195164E-03,-0.56914188E-03,
         0.57425706E-04,-0.66959835E-03,-0.14506432E-02, 0.39505327E-03,
        -0.91848348E-03, 0.41436843E-03,-0.10461758E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_400                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_predent_400                             =v_p_e_predent_400                             
        +coeff[  8]    *x22    *x41    
        +coeff[  9]*x11    *x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]            *x41*x52
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_predent_400                             =v_p_e_predent_400                             
        +coeff[ 17]        *x33    *x52
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x23    *x41    
        ;

    return v_p_e_predent_400                             ;
}
float l_e_predent_400                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4439997E-02;
    float xmin[10]={
        -0.39997E-02,-0.60048E-01,-0.59983E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60034E-01, 0.59995E-01, 0.30049E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.45180921E-02, 0.13688226E-03,-0.95879491E-02,-0.20455811E-02,
        -0.59745898E-02,-0.61967778E-02, 0.57246903E-03,-0.87199936E-03,
        -0.21336049E-03,-0.10132173E-03, 0.36163270E-03,-0.77470184E-04,
        -0.10444483E-03, 0.20253960E-03,-0.78035059E-03,-0.48562585E-03,
         0.47621675E-03, 0.49844739E-03, 0.35138111E-03,-0.86190633E-03,
         0.76878211E-03,-0.26278096E-03, 0.10970441E-03,-0.71449386E-03,
        -0.12715167E-02, 0.35399757E-02,-0.24466608E-02,-0.16906131E-02,
         0.21961413E-02,-0.17905327E-02, 0.10975073E-02, 0.39075962E-02,
        -0.77361992E-03, 0.15870032E-02,-0.23109205E-02, 0.13879262E-02,
         0.95238461E-03,-0.73132796E-05,-0.20606736E-03,-0.21877534E-03,
         0.26634603E-03,-0.19253782E-03,-0.80374297E-03, 0.39353102E-03,
         0.47911974E-03, 0.20305377E-03, 0.74021990E-03,-0.26619743E-03,
         0.14973940E-03, 0.26632007E-03, 0.23393385E-03, 0.64329553E-03,
         0.38049111E-03,-0.91856855E-04, 0.55283913E-03,-0.35796320E-03,
         0.20726472E-03, 0.26005029E-03, 0.26152420E-03, 0.12338207E-02,
        -0.13541471E-03, 0.17332916E-03, 0.66716631E-03,-0.40248819E-03,
         0.43960754E-03, 0.12758236E-02,-0.94892073E-03, 0.96541626E-03,
        -0.29821627E-03,-0.47881142E-03,-0.65323699E-03,-0.34882998E-03,
        -0.17458877E-02, 0.37580365E-03, 0.22692746E-02, 0.34139448E-03,
         0.58994675E-03, 0.40260385E-03,-0.75189356E-03,-0.40439572E-03,
        -0.20402682E-03,-0.33852074E-03,-0.59918809E-03, 0.29664676E-03,
        -0.94642141E-03, 0.90531941E-03,-0.18707601E-02,-0.79474313E-03,
        -0.34159675E-03, 0.29413216E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_400                             =avdat
        +coeff[  0]                    
        +coeff[  1]                *x51
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[  8]        *x31*x41*x51
        +coeff[  9]        *x31        
        +coeff[ 10]            *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11    *x32        
        +coeff[ 13]*x11            *x52
        +coeff[ 14]        *x31*x43    
        +coeff[ 15]*x11*x23            
        +coeff[ 16]*x11    *x32    *x51
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 17]    *x22*x31*x42    
        +coeff[ 18]        *x32*x41*x52
        +coeff[ 19]*x11*x23        *x51
        +coeff[ 20]*x11*x22        *x52
        +coeff[ 21]*x11        *x41*x53
        +coeff[ 22]*x12    *x31*x41*x51
        +coeff[ 23]        *x34*x42    
        +coeff[ 24]*x12    *x32*x41*x51
        +coeff[ 25]    *x22    *x44*x51
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 26]*x13*x21        *x52
        +coeff[ 27]*x11*x24    *x42    
        +coeff[ 28]*x11*x23    *x42*x51
        +coeff[ 29]*x11*x21*x32*x42*x51
        +coeff[ 30]*x12*x22*x32    *x51
        +coeff[ 31]*x12*x22*x31*x41*x51
        +coeff[ 32]*x12*x21*x32    *x52
        +coeff[ 33]*x12*x22        *x53
        +coeff[ 34]*x13*x23    *x41    
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 35]    *x22*x34*x41*x51
        +coeff[ 36]*x13*x21    *x41*x52
        +coeff[ 37]            *x41    
        +coeff[ 38]*x11                
        +coeff[ 39]                *x52
        +coeff[ 40]*x11        *x41    
        +coeff[ 41]*x11            *x51
        +coeff[ 42]        *x32*x41    
        +coeff[ 43]    *x21    *x42    
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 44]        *x32    *x51
        +coeff[ 45]*x11*x21*x31        
        +coeff[ 46]*x11*x21    *x41    
        +coeff[ 47]*x12            *x51
        +coeff[ 48]*x13                
        +coeff[ 49]    *x21*x33        
        +coeff[ 50]        *x34        
        +coeff[ 51]    *x22*x31*x41    
        +coeff[ 52]    *x22    *x42    
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 53]    *x21*x31*x42    
        +coeff[ 54]    *x22        *x52
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x22    *x41    
        +coeff[ 57]*x11*x22        *x51
        +coeff[ 58]*x11*x21*x31    *x51
        +coeff[ 59]*x11*x21        *x52
        +coeff[ 60]*x12*x21*x31        
        +coeff[ 61]*x12*x21    *x41    
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 62]*x13*x21            
        +coeff[ 63]*x13        *x41    
        +coeff[ 64]    *x24    *x41    
        +coeff[ 65]        *x34*x41    
        +coeff[ 66]    *x22    *x43    
        +coeff[ 67]    *x21*x31*x43    
        +coeff[ 68]        *x32*x43    
        +coeff[ 69]        *x31*x44    
        +coeff[ 70]    *x22*x31*x41*x51
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 71]    *x21*x32*x41*x51
        +coeff[ 72]    *x22    *x42*x51
        +coeff[ 73]        *x32*x42*x51
        +coeff[ 74]        *x31*x43*x51
        +coeff[ 75]            *x44*x51
        +coeff[ 76]        *x33    *x52
        +coeff[ 77]    *x22    *x41*x52
        +coeff[ 78]    *x21*x31*x41*x52
        +coeff[ 79]        *x32    *x53
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 80]    *x21        *x54
        +coeff[ 81]        *x31    *x54
        +coeff[ 82]*x11*x23*x31        
        +coeff[ 83]*x11*x21*x33        
        +coeff[ 84]*x11*x21*x32*x41    
        +coeff[ 85]*x11*x22    *x42    
        +coeff[ 86]*x11*x21*x31*x42    
        +coeff[ 87]*x11*x21    *x43    
        +coeff[ 88]*x11    *x32    *x52
    ;
    v_l_e_predent_400                             =v_l_e_predent_400                             
        +coeff[ 89]*x11*x21        *x53
        ;

    return v_l_e_predent_400                             ;
}
float x_e_predent_350                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066315E+01;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
        -0.14668988E-03,-0.30245516E-02, 0.92920907E-01, 0.42555546E-02,
        -0.24841365E-02,-0.17374483E-02,-0.57660131E-03, 0.24975822E-03,
        -0.70231181E-03,-0.10242633E-02,-0.26644053E-03,-0.18365644E-02,
        -0.13494993E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_350                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_350                             =v_x_e_predent_350                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_350                             ;
}
float t_e_predent_350                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3786080E+01;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.54352526E-01,-0.36636215E-01, 0.83188260E+00, 0.17680611E+00,
         0.35727225E-01,-0.15100809E-01, 0.29582176E-01,-0.13735895E-03,
         0.16274180E-01,-0.19846180E-01,-0.19161444E-01,-0.12324168E-01,
         0.26927406E-02,-0.57604797E-02,-0.91776084E-02, 0.96265780E-03,
         0.46839475E-03,-0.29536099E-02,-0.20536726E-01,-0.14921076E-01,
         0.59360689E-02,-0.17741037E-04,-0.34537189E-01,-0.23862639E-01,
         0.11546963E-03, 0.19316385E-03,-0.16497653E-02,-0.76043657E-02,
         0.30151437E-03, 0.36159347E-03, 0.38617535E-02, 0.24633373E-02,
        -0.95763447E-03,-0.24335485E-01,-0.19092839E-01,-0.25202319E-03,
         0.21775620E-03,-0.86195173E-03,-0.58414735E-03,-0.20301930E-03,
        -0.48969121E-03, 0.17092415E-02,-0.19887896E-03,-0.11324697E-01,
        -0.25358155E-04,-0.26605205E-03, 0.10340740E-03, 0.20151855E-03,
        -0.12792714E-03, 0.15942566E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_350                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_350                             =v_t_e_predent_350                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_350                             =v_t_e_predent_350                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x22*x32    *x52
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_350                             =v_t_e_predent_350                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_350                             =v_t_e_predent_350                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]*x11            *x52
        +coeff[ 40]        *x31*x43    
        +coeff[ 41]    *x21*x32    *x51
        +coeff[ 42]*x11*x22*x32        
        +coeff[ 43]    *x21*x33*x41    
    ;
    v_t_e_predent_350                             =v_t_e_predent_350                             
        +coeff[ 44]        *x31        
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]    *x22    *x41    
        +coeff[ 47]*x12*x22            
        +coeff[ 48]*x12*x21*x31        
        +coeff[ 49]*x11*x22*x31        
        ;

    return v_t_e_predent_350                             ;
}
float y_e_predent_350                             (float *x,int m){
    int ncoeff= 44;
    float avdat=  0.1355523E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 45]={
        -0.13264015E-02, 0.14710160E+00, 0.61950210E-01, 0.88247936E-02,
         0.42904853E-02,-0.18813465E-02,-0.12918452E-02,-0.19696443E-02,
        -0.15019013E-03,-0.47346522E-03,-0.13793047E-02,-0.33437993E-03,
        -0.50726929E-03,-0.34442334E-03,-0.10042742E-02,-0.29166483E-02,
        -0.10165843E-02,-0.85185480E-03, 0.12419448E-04,-0.44462693E-03,
        -0.27483731E-03,-0.82449209E-04, 0.35327117E-03, 0.29248797E-05,
         0.26539998E-03, 0.17712457E-03,-0.18192395E-02, 0.25788526E-06,
        -0.82322505E-04,-0.32712510E-03,-0.97767901E-04,-0.36467663E-04,
        -0.41227753E-03, 0.51872797E-04,-0.14879792E-02,-0.30526097E-03,
        -0.46533035E-03,-0.70251561E-04, 0.25705733E-05, 0.11412591E-03,
         0.30263980E-04,-0.85555759E-04, 0.14703115E-04, 0.60743565E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_350                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_350                             =v_y_e_predent_350                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_350                             =v_y_e_predent_350                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_350                             =v_y_e_predent_350                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x24    *x43    
        +coeff[ 29]    *x24*x31*x42    
        +coeff[ 30]    *x24    *x45    
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]        *x31*x44    
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]    *x22    *x43    
    ;
    v_y_e_predent_350                             =v_y_e_predent_350                             
        +coeff[ 35]        *x33*x42    
        +coeff[ 36]    *x22*x33        
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]                *x51
        +coeff[ 39]            *x43*x51
        +coeff[ 40]    *x22*x31    *x51
        +coeff[ 41]        *x34*x41    
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x22*x32*x41*x51
        ;

    return v_y_e_predent_350                             ;
}
float p_e_predent_350                             (float *x,int m){
    int ncoeff= 23;
    float avdat= -0.8439134E-03;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
         0.81044820E-03,-0.79307266E-01,-0.75691119E-01,-0.16259778E-01,
        -0.15541584E-01, 0.73232385E-02, 0.11409939E-01,-0.49422504E-02,
        -0.56818761E-02, 0.77503320E-03, 0.74780319E-03, 0.15798194E-02,
        -0.23205797E-02,-0.12574169E-02, 0.70829410E-03,-0.55884232E-03,
         0.22333770E-04,-0.67058636E-03,-0.14153086E-02, 0.37957862E-03,
        -0.90837223E-03, 0.39767564E-03,-0.10825804E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_350                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_predent_350                             =v_p_e_predent_350                             
        +coeff[  8]    *x22    *x41    
        +coeff[  9]*x11    *x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]            *x41*x52
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_predent_350                             =v_p_e_predent_350                             
        +coeff[ 17]        *x33    *x52
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x23    *x41    
        ;

    return v_p_e_predent_350                             ;
}
float l_e_predent_350                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4493224E-02;
    float xmin[10]={
        -0.39987E-02,-0.60051E-01,-0.59952E-01,-0.30003E-01,-0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.59997E-01, 0.30006E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44243732E-02,-0.94584133E-02,-0.17373074E-02,-0.65976628E-02,
        -0.63425349E-02, 0.61196013E-03,-0.34168811E-03,-0.84013329E-04,
        -0.59722199E-04,-0.13069756E-03, 0.10218284E-02,-0.92966820E-05,
         0.37285654E-03, 0.14624698E-02, 0.10184930E-02, 0.62738662E-03,
         0.65003172E-03, 0.26948686E-04, 0.92187140E-03,-0.67977450E-03,
        -0.67928876E-03, 0.17142270E-02,-0.25977180E-02, 0.87424793E-03,
        -0.15507644E-02, 0.71609137E-03, 0.16608788E-02,-0.19115075E-02,
         0.33974131E-04,-0.24617766E-03, 0.17804725E-03, 0.17009159E-03,
         0.79927128E-03,-0.70990168E-03, 0.39021429E-03,-0.13166894E-03,
        -0.20568202E-03, 0.60319344E-04,-0.57570473E-03,-0.14920948E-02,
         0.27438349E-03,-0.98757082E-04, 0.28835751E-04, 0.11762683E-02,
        -0.18432910E-03,-0.27595140E-03, 0.18585339E-03, 0.14604970E-03,
        -0.81517283E-04, 0.14590014E-02, 0.86878671E-03, 0.61574689E-03,
         0.41612348E-03, 0.12129117E-02, 0.63917559E-03, 0.74946851E-03,
        -0.16203236E-02, 0.49757626E-03,-0.65394765E-03,-0.73348958E-03,
         0.85542345E-03,-0.22388851E-03,-0.52242959E-03,-0.25610504E-03,
         0.70644217E-03, 0.69962000E-03,-0.10809021E-03, 0.59551158E-03,
        -0.37702289E-03, 0.40427429E-03, 0.15785216E-03, 0.26872699E-03,
        -0.41452216E-03,-0.54016750E-03,-0.17229680E-03, 0.50054537E-03,
        -0.20404604E-03, 0.55443245E-03, 0.11190117E-02,-0.45110253E-03,
         0.57933206E-03, 0.90548606E-03,-0.10361583E-02, 0.58999239E-03,
        -0.44342430E-03, 0.54683903E-03,-0.10615233E-02, 0.70643658E-03,
        -0.11304895E-02,-0.78117754E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_350                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]        *x31        
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[  8]    *x21*x31        
        +coeff[  9]    *x21*x32        
        +coeff[ 10]        *x31*x41*x51
        +coeff[ 11]            *x42*x51
        +coeff[ 12]*x13                
        +coeff[ 13]    *x22*x31*x41    
        +coeff[ 14]    *x22    *x42    
        +coeff[ 15]*x12    *x31*x41    
        +coeff[ 16]    *x22*x32    *x51
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 17]        *x34    *x51
        +coeff[ 18]*x11*x23*x31        
        +coeff[ 19]*x11*x21*x33        
        +coeff[ 20]*x12    *x31    *x52
        +coeff[ 21]    *x24*x31    *x52
        +coeff[ 22]    *x22*x32*x41*x52
        +coeff[ 23]*x11*x24*x31*x41    
        +coeff[ 24]*x11*x24    *x41*x51
        +coeff[ 25]*x11*x21*x33    *x52
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 26]    *x23    *x43*x52
        +coeff[ 27]    *x21*x32*x41*x54
        +coeff[ 28]            *x41    
        +coeff[ 29]*x11                
        +coeff[ 30]            *x41*x52
        +coeff[ 31]*x11    *x32        
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11    *x31    *x51
        +coeff[ 34]*x11        *x41*x51
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 35]*x11            *x52
        +coeff[ 36]*x12        *x41    
        +coeff[ 37]            *x44    
        +coeff[ 38]    *x21*x31    *x52
        +coeff[ 39]        *x32    *x52
        +coeff[ 40]        *x31*x41*x52
        +coeff[ 41]*x11*x22        *x51
        +coeff[ 42]*x11*x21*x31    *x51
        +coeff[ 43]*x11*x21    *x41*x51
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 44]*x11*x21        *x52
        +coeff[ 45]*x12        *x41*x51
        +coeff[ 46]*x12            *x52
        +coeff[ 47]    *x24*x31        
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]    *x22*x32*x41    
        +coeff[ 50]    *x22*x31*x42    
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]        *x32*x43    
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 53]    *x22*x31*x41*x51
        +coeff[ 54]    *x22    *x42*x51
        +coeff[ 55]        *x32*x42*x51
        +coeff[ 56]    *x22*x31    *x52
        +coeff[ 57]        *x33    *x52
        +coeff[ 58]    *x22        *x53
        +coeff[ 59]        *x31*x41*x53
        +coeff[ 60]*x11*x21*x32*x41    
        +coeff[ 61]*x11*x21    *x43    
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 62]*x11        *x44    
        +coeff[ 63]*x11*x22*x31    *x51
        +coeff[ 64]*x11*x21*x32    *x51
        +coeff[ 65]*x11*x21*x31*x41*x51
        +coeff[ 66]*x11    *x32*x41*x51
        +coeff[ 67]*x11    *x31*x42*x51
        +coeff[ 68]*x11*x21    *x41*x52
        +coeff[ 69]*x11    *x31    *x53
        +coeff[ 70]*x12            *x53
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 71]    *x24*x32        
        +coeff[ 72]*x13        *x42    
        +coeff[ 73]        *x34*x42    
        +coeff[ 74]*x13*x21        *x51
        +coeff[ 75]*x13    *x31    *x51
        +coeff[ 76]    *x24*x31    *x51
        +coeff[ 77]    *x24    *x41*x51
        +coeff[ 78]    *x22*x31*x42*x51
        +coeff[ 79]        *x33*x42*x51
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 80]    *x23*x31    *x52
        +coeff[ 81]        *x34    *x52
        +coeff[ 82]        *x31*x43*x52
        +coeff[ 83]        *x32    *x54
        +coeff[ 84]*x11*x24    *x41    
        +coeff[ 85]*x11*x22*x32*x41    
        +coeff[ 86]*x11*x23*x31    *x51
        +coeff[ 87]*x11*x21*x33    *x51
        +coeff[ 88]*x11*x23    *x41*x51
    ;
    v_l_e_predent_350                             =v_l_e_predent_350                             
        +coeff[ 89]*x11*x21    *x43*x51
        ;

    return v_l_e_predent_350                             ;
}
float x_e_predent_300                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067039E+01;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.59688237E-03,-0.30093777E-02, 0.93347825E-01, 0.42333743E-02,
        -0.24816308E-02,-0.17160068E-02,-0.59017411E-03, 0.24318795E-03,
        -0.69099624E-03,-0.10103929E-02,-0.26638273E-03,-0.18241652E-02,
        -0.13927052E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_300                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_300                             =v_x_e_predent_300                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_300                             ;
}
float t_e_predent_300                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3780458E+01;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.48668627E-01,-0.36595535E-01, 0.83651197E+00, 0.17920218E+00,
         0.35656214E-01,-0.15245123E-01, 0.30444058E-01,-0.11227386E-03,
         0.16195638E-01,-0.20406419E-01,-0.18947721E-01,-0.12309890E-01,
         0.26509471E-02,-0.57289368E-02,-0.90670213E-02, 0.92264672E-03,
         0.65166532E-03,-0.30191515E-02,-0.20510837E-01,-0.15715525E-01,
         0.56599835E-02,-0.41680396E-03,-0.25805202E-04, 0.41977735E-03,
        -0.15910224E-02,-0.77935145E-02,-0.34322537E-01,-0.24566393E-01,
         0.34676559E-03, 0.36759183E-02, 0.18589760E-02,-0.91706071E-03,
        -0.10837136E-01,-0.23638815E-01,-0.18238183E-01, 0.58704487E-03,
        -0.19156316E-03,-0.72712026E-03,-0.45190952E-03, 0.15072606E-02,
        -0.13456892E-03,-0.76021295E-03, 0.10352856E-02, 0.15006286E-03,
        -0.22020266E-04,-0.23290612E-03, 0.10028931E-03,-0.12273944E-02,
        -0.10415488E-02, 0.26919245E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_300                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_300                             =v_t_e_predent_300                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_300                             =v_t_e_predent_300                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]    *x22*x32    *x52
        +coeff[ 23]        *x32        
        +coeff[ 24]*x11*x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_predent_300                             =v_t_e_predent_300                             
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12                
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x21*x33*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_300                             =v_t_e_predent_300                             
        +coeff[ 35]*x11*x21        *x53
        +coeff[ 36]    *x23*x32    *x51
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x22*x31*x43    
        +coeff[ 42]    *x21    *x42*x53
        +coeff[ 43]*x12*x21            
    ;
    v_t_e_predent_300                             =v_t_e_predent_300                             
        +coeff[ 44]    *x22*x31        
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]    *x22    *x41    
        +coeff[ 47]        *x32*x42    
        +coeff[ 48]        *x31*x43    
        +coeff[ 49]*x11*x21        *x52
        ;

    return v_t_e_predent_300                             ;
}
float y_e_predent_300                             (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.6669372E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.66502026E-03, 0.14692476E+00, 0.61673176E-01, 0.88145351E-02,
         0.43085786E-02,-0.18969473E-02,-0.12858027E-02,-0.22256793E-02,
        -0.17674274E-04,-0.30111396E-03,-0.14726439E-02,-0.33980104E-03,
        -0.50537201E-03,-0.34172120E-03,-0.11253734E-02,-0.24898935E-02,
        -0.10929249E-02,-0.90685778E-03,-0.51468483E-03,-0.42293203E-03,
         0.91423994E-04, 0.37952883E-04,-0.30191283E-03,-0.78791691E-05,
        -0.19036956E-02,-0.11620880E-02, 0.26008353E-03,-0.13928183E-04,
         0.19276088E-03,-0.11664937E-02,-0.52815152E-03,-0.59827526E-04,
         0.19696112E-04, 0.27985225E-04, 0.71809541E-05, 0.10003788E-03,
         0.21443862E-04, 0.27094793E-04,-0.10291423E-03, 0.17242461E-04,
         0.42776483E-04, 0.19320009E-04, 0.19247891E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_300                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_300                             =v_y_e_predent_300                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_300                             =v_y_e_predent_300                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x22*x32*x41    
        +coeff[ 25]    *x22*x31*x44    
    ;
    v_y_e_predent_300                             =v_y_e_predent_300                             
        +coeff[ 26]        *x31*x42*x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]        *x32*x41*x51
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]            *x45*x51
        +coeff[ 33]        *x33    *x53
        +coeff[ 34]    *x21*x31*x41    
    ;
    v_y_e_predent_300                             =v_y_e_predent_300                             
        +coeff[ 35]            *x43*x51
        +coeff[ 36]    *x22*x31    *x51
        +coeff[ 37]        *x33    *x51
        +coeff[ 38]        *x33*x42    
        +coeff[ 39]            *x41*x53
        +coeff[ 40]*x11*x21*x31*x42    
        +coeff[ 41]*x13*x21    *x41    
        +coeff[ 42]        *x32*x45    
        ;

    return v_y_e_predent_300                             ;
}
float p_e_predent_300                             (float *x,int m){
    int ncoeff= 23;
    float avdat= -0.2667060E-03;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
         0.25883812E-03,-0.79373986E-01,-0.75828269E-01,-0.16369425E-01,
        -0.15677381E-01, 0.73213428E-02, 0.11421997E-01,-0.50052698E-02,
        -0.57591358E-02, 0.77355385E-03, 0.74667041E-03, 0.16042803E-02,
        -0.23220922E-02,-0.12636533E-02, 0.73362957E-03,-0.55392971E-03,
         0.70650945E-04,-0.66911458E-03,-0.14537781E-02, 0.39076945E-03,
        -0.93916885E-03, 0.41731319E-03,-0.10862392E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_300                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22*x31        
    ;
    v_p_e_predent_300                             =v_p_e_predent_300                             
        +coeff[  8]    *x22    *x41    
        +coeff[  9]*x11    *x31        
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]    *x21    *x41*x51
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]            *x41*x52
        +coeff[ 16]        *x34*x41    
    ;
    v_p_e_predent_300                             =v_p_e_predent_300                             
        +coeff[ 17]        *x33    *x52
        +coeff[ 18]        *x32*x41    
        +coeff[ 19]*x11*x21*x31        
        +coeff[ 20]    *x23*x31        
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x23    *x41    
        ;

    return v_p_e_predent_300                             ;
}
float l_e_predent_300                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4493509E-02;
    float xmin[10]={
        -0.39996E-02,-0.60039E-01,-0.59976E-01,-0.30033E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30030E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44144611E-02,-0.91171591E-02,-0.21064910E-02,-0.56250882E-02,
        -0.60031842E-02, 0.10344191E-02,-0.93221833E-03, 0.13743131E-02,
         0.69713592E-05, 0.94414732E-04,-0.19114399E-04,-0.16064367E-03,
         0.31087911E-03,-0.54896448E-03, 0.73877833E-03, 0.74015756E-03,
        -0.62130729E-03,-0.52099244E-03,-0.53246820E-03,-0.44506224E-03,
        -0.49457786E-03,-0.10106409E-02,-0.16028648E-02, 0.99465251E-03,
         0.52242575E-03,-0.43060639E-03,-0.42218564E-03, 0.15643779E-02,
        -0.15123781E-02,-0.10811901E-03,-0.40961462E-04,-0.15161416E-03,
        -0.84692983E-04, 0.10104730E-03, 0.63296226E-04,-0.19324837E-03,
         0.38740566E-03, 0.85263513E-03, 0.18957438E-04, 0.92518155E-03,
         0.26277278E-03, 0.40601828E-03, 0.38845319E-03, 0.29155044E-03,
        -0.27365875E-03,-0.39823298E-03, 0.42567647E-03,-0.12745180E-03,
        -0.20360172E-03, 0.51393832E-04, 0.66061586E-03,-0.21628152E-03,
         0.35352982E-03,-0.10035432E-03,-0.16492585E-03,-0.22388800E-03,
         0.26635636E-03, 0.22303990E-03, 0.55167248E-03,-0.81328541E-03,
        -0.31792736E-03,-0.53813390E-03,-0.46097994E-03,-0.28628085E-03,
         0.46814661E-03,-0.53915817E-04, 0.44332395E-04, 0.42707784E-03,
         0.20107366E-03,-0.87234360E-03,-0.16757768E-02,-0.33583441E-04,
         0.12769084E-02, 0.76865521E-03, 0.45945033E-03, 0.48446283E-03,
        -0.37883259E-04, 0.32486577E-03,-0.74448955E-03,-0.51185978E-03,
         0.27965032E-03,-0.49678731E-03, 0.31819745E-03, 0.66250563E-03,
         0.19815189E-03,-0.37196703E-03, 0.79830928E-03,-0.42565633E-03,
        -0.12076318E-02,-0.22373340E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_300                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x42*x51
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[  8]                *x53
        +coeff[  9]        *x31*x43*x51
        +coeff[ 10]    *x21            
        +coeff[ 11]    *x21*x31        
        +coeff[ 12]    *x23            
        +coeff[ 13]    *x21    *x42    
        +coeff[ 14]        *x32    *x51
        +coeff[ 15]        *x31*x41*x51
        +coeff[ 16]*x11            *x52
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]*x12    *x31*x41    
        +coeff[ 19]*x13*x21            
        +coeff[ 20]    *x21*x31*x43    
        +coeff[ 21]            *x44*x51
        +coeff[ 22]*x12*x21*x31*x41    
        +coeff[ 23]    *x23*x32*x41    
        +coeff[ 24]*x13            *x52
        +coeff[ 25]    *x21    *x41*x54
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 26]*x12*x24            
        +coeff[ 27]    *x23*x33*x41    
        +coeff[ 28]*x12*x23    *x42    
        +coeff[ 29]                *x51
        +coeff[ 30]*x11                
        +coeff[ 31]        *x31    *x51
        +coeff[ 32]            *x41*x51
        +coeff[ 33]*x11    *x31        
        +coeff[ 34]    *x22    *x41    
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 35]        *x31*x42    
        +coeff[ 36]    *x21*x31    *x51
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]*x11    *x31    *x51
        +coeff[ 40]*x11        *x41*x51
        +coeff[ 41]    *x23*x31        
        +coeff[ 42]        *x34        
        +coeff[ 43]            *x43*x51
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 44]    *x22        *x52
        +coeff[ 45]        *x31*x41*x52
        +coeff[ 46]        *x31    *x53
        +coeff[ 47]*x11*x22*x31        
        +coeff[ 48]*x11*x21*x32        
        +coeff[ 49]*x11    *x33        
        +coeff[ 50]*x11    *x32*x41    
        +coeff[ 51]*x11*x21    *x42    
        +coeff[ 52]*x11    *x31*x42    
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 53]*x11        *x43    
        +coeff[ 54]*x11        *x41*x52
        +coeff[ 55]*x12*x21*x31        
        +coeff[ 56]*x12*x21        *x51
        +coeff[ 57]    *x21*x34        
        +coeff[ 58]    *x23    *x42    
        +coeff[ 59]    *x22    *x43    
        +coeff[ 60]    *x23*x31    *x51
        +coeff[ 61]        *x34    *x51
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 62]    *x22*x31    *x52
        +coeff[ 63]    *x21    *x42*x52
        +coeff[ 64]    *x22        *x53
        +coeff[ 65]        *x31    *x54
        +coeff[ 66]*x11*x22*x32        
        +coeff[ 67]*x11    *x34        
        +coeff[ 68]*x11    *x33*x41    
        +coeff[ 69]*x11    *x32*x42    
        +coeff[ 70]*x11    *x31*x43    
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 71]*x11*x21*x32    *x51
        +coeff[ 72]*x11*x21*x31*x41*x51
        +coeff[ 73]*x11*x21    *x42*x51
        +coeff[ 74]*x11        *x42*x52
        +coeff[ 75]*x11            *x54
        +coeff[ 76]*x12    *x33        
        +coeff[ 77]*x12*x22    *x41    
        +coeff[ 78]*x12    *x32*x41    
        +coeff[ 79]*x12    *x31*x42    
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 80]*x12        *x43    
        +coeff[ 81]*x12*x22        *x51
        +coeff[ 82]*x12*x21    *x41*x51
        +coeff[ 83]*x12    *x31    *x52
        +coeff[ 84]*x12            *x53
        +coeff[ 85]    *x22*x34        
        +coeff[ 86]    *x24*x31*x41    
        +coeff[ 87]    *x24    *x42    
        +coeff[ 88]*x13    *x31    *x51
    ;
    v_l_e_predent_300                             =v_l_e_predent_300                             
        +coeff[ 89]    *x21*x34    *x51
        ;

    return v_l_e_predent_300                             ;
}
float x_e_predent_250                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6067408E+01;
    float xmin[10]={
        -0.39971E-02,-0.60047E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.96474303E-03,-0.29911252E-02, 0.93963504E-01, 0.42054965E-02,
        -0.24568234E-02,-0.17234365E-02,-0.59198966E-03, 0.24463862E-03,
        -0.69470558E-03,-0.10088066E-02,-0.26911942E-03,-0.18483293E-02,
        -0.13467921E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_250                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_250                             =v_x_e_predent_250                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_250                             ;
}
float t_e_predent_250                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3778216E+01;
    float xmin[10]={
        -0.39971E-02,-0.60047E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.46387590E-01,-0.36335137E-01, 0.84314471E+00, 0.18186890E+00,
         0.35237223E-01,-0.15278284E-01, 0.31113444E-01,-0.11305148E-03,
         0.16260711E-01,-0.20757739E-01,-0.19263055E-01,-0.11890781E-01,
         0.26537233E-02,-0.58907755E-02,-0.93332594E-02, 0.76050841E-03,
         0.45984640E-03,-0.30229639E-02,-0.20519914E-01,-0.14864151E-01,
         0.56842226E-02, 0.43480439E-04, 0.15507106E-03,-0.73540560E-02,
        -0.33833664E-01,-0.23582026E-01, 0.35232317E-03, 0.12367819E-04,
         0.41212121E-03,-0.15171516E-02, 0.36546607E-02, 0.27006655E-02,
        -0.99706207E-03,-0.10068882E-01,-0.17656239E-01, 0.44778257E-03,
         0.11043239E-03, 0.16880053E-03,-0.81666757E-03,-0.58407855E-03,
         0.13644977E-02, 0.17718504E-03,-0.22722257E-01,-0.88944107E-04,
        -0.18050103E-04,-0.41273507E-03, 0.37932303E-03,-0.56781009E-03,
         0.39292607E-03, 0.39688742E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_250                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_250                             =v_t_e_predent_250                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_250                             =v_t_e_predent_250                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]        *x32        
        +coeff[ 23]    *x22*x32        
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_t_e_predent_250                             =v_t_e_predent_250                             
        +coeff[ 26]*x12                
        +coeff[ 27]                *x52
        +coeff[ 28]*x11*x21        *x51
        +coeff[ 29]*x11*x23            
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x33*x41    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_250                             =v_t_e_predent_250                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11*x21*x31        
        +coeff[ 38]*x11    *x31*x41    
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]    *x21*x32    *x51
        +coeff[ 41]*x13    *x32        
        +coeff[ 42]    *x21*x32*x42    
        +coeff[ 43]*x11        *x41    
    ;
    v_t_e_predent_250                             =v_t_e_predent_250                             
        +coeff[ 44]*x13                
        +coeff[ 45]*x11    *x32        
        +coeff[ 46]        *x33*x41    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]*x12*x21*x32        
        +coeff[ 49]*x12*x21    *x42    
        ;

    return v_t_e_predent_250                             ;
}
float y_e_predent_250                             (float *x,int m){
    int ncoeff= 39;
    float avdat= -0.3984730E-03;
    float xmin[10]={
        -0.39971E-02,-0.60047E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 40]={
         0.38711351E-03, 0.14637947E+00, 0.61254136E-01, 0.88142352E-02,
         0.42985086E-02,-0.18282186E-02,-0.12681256E-02,-0.19412889E-02,
        -0.15083420E-03,-0.51233644E-03,-0.13319433E-02,-0.32913862E-03,
        -0.50876138E-03,-0.34483461E-03,-0.10002299E-02,-0.10903482E-02,
        -0.90268481E-03,-0.46133869E-05, 0.25862243E-03, 0.18789059E-03,
        -0.16000309E-02,-0.30959155E-02,-0.30992730E-03,-0.21032284E-02,
        -0.99062985E-04,-0.51972340E-03, 0.35329729E-05,-0.19362391E-04,
        -0.44103427E-03, 0.47423015E-04,-0.53448613E-04, 0.61464066E-05,
         0.94304094E-04, 0.10430940E-04, 0.22330238E-04,-0.11524989E-04,
         0.29598152E-04, 0.17902852E-04,-0.18919931E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_250                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_250                             =v_y_e_predent_250                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_250                             =v_y_e_predent_250                             
        +coeff[ 17]*x11*x21*x31        
        +coeff[ 18]        *x31*x42*x51
        +coeff[ 19]        *x32*x41*x51
        +coeff[ 20]    *x22    *x43    
        +coeff[ 21]    *x22*x31*x42    
        +coeff[ 22]        *x33*x42    
        +coeff[ 23]    *x22*x32*x41    
        +coeff[ 24]        *x34*x41    
        +coeff[ 25]    *x22*x33        
    ;
    v_y_e_predent_250                             =v_y_e_predent_250                             
        +coeff[ 26]            *x45*x51
        +coeff[ 27]    *x22    *x41*x51
        +coeff[ 28]        *x31*x44    
        +coeff[ 29]        *x33    *x51
        +coeff[ 30]*x11*x23*x31        
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]            *x43*x51
        +coeff[ 33]*x11*x21    *x42    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_predent_250                             =v_y_e_predent_250                             
        +coeff[ 35]        *x31*x43*x51
        +coeff[ 36]            *x41*x53
        +coeff[ 37]        *x31    *x53
        +coeff[ 38]*x12*x22    *x41    
        ;

    return v_y_e_predent_250                             ;
}
float p_e_predent_250                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.4835758E-03;
    float xmin[10]={
        -0.39971E-02,-0.60047E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.47461962E-03,-0.79464883E-01,-0.75907640E-01,-0.16506672E-01,
        -0.15811699E-01, 0.73099653E-02, 0.11412941E-01,-0.58386861E-02,
        -0.19872205E-04,-0.50736680E-02, 0.16311344E-02, 0.77082834E-03,
         0.74260525E-03, 0.75307459E-03,-0.22992077E-02,-0.12504047E-02,
        -0.54939539E-03, 0.91827176E-04,-0.63901261E-03,-0.14607027E-02,
         0.39225310E-03,-0.97248558E-03, 0.41495814E-03,-0.11173877E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_250                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_250                             =v_p_e_predent_250                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_250                             =v_p_e_predent_250                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_250                             ;
}
float l_e_predent_250                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4493957E-02;
    float xmin[10]={
        -0.39971E-02,-0.60047E-01,-0.59994E-01,-0.30019E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.30016E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44009727E-02,-0.95498264E-02,-0.19847688E-02,-0.61034779E-02,
        -0.58322260E-02, 0.39955330E-03,-0.76856790E-03, 0.41568474E-03,
         0.71429205E-03, 0.39542708E-03,-0.21044635E-02,-0.59563772E-05,
         0.10183988E-03,-0.90127513E-04, 0.66334778E-03, 0.47017861E-03,
        -0.35508705E-03, 0.11634654E-02,-0.17599300E-02, 0.46433407E-03,
         0.18029937E-02, 0.13349118E-02, 0.11525241E-03, 0.80003490E-04,
         0.30744905E-03, 0.38557072E-03, 0.89714675E-04,-0.18787965E-03,
        -0.22400076E-03, 0.37705587E-03,-0.17672457E-03, 0.12497745E-03,
        -0.45592027E-04,-0.15838459E-03, 0.46092970E-03, 0.32410768E-03,
         0.32902241E-03,-0.77926210E-03, 0.13163892E-02, 0.71957707E-03,
         0.63362031E-03,-0.55126665E-03, 0.64124522E-03,-0.66537969E-03,
         0.30862438E-03, 0.28198655E-03, 0.37365410E-03,-0.12931474E-03,
        -0.34793842E-03,-0.33772804E-03,-0.34066977E-03, 0.35812473E-03,
        -0.38845639E-03,-0.58511685E-03,-0.23909433E-03, 0.40617431E-03,
         0.23568602E-03,-0.33487770E-03,-0.40142171E-03,-0.45522614E-03,
         0.64851937E-03,-0.89007936E-03,-0.55125653E-03,-0.59382239E-03,
        -0.52789197E-03,-0.27216197E-03, 0.32064866E-03,-0.43017435E-03,
        -0.32878731E-03,-0.44713978E-03, 0.35327731E-03,-0.56393142E-03,
         0.44189836E-03,-0.32324390E-03, 0.56116452E-03, 0.76218683E-03,
         0.51011459E-03, 0.74379065E-03,-0.48624261E-03, 0.12190744E-02,
        -0.26693324E-04,-0.10235400E-02,-0.66061394E-03,-0.10837336E-02,
         0.10943924E-02,-0.10090939E-02,-0.59453625E-03,-0.25515648E-03,
         0.40050584E-03,-0.30184124E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_250                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]        *x32    *x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[  8]            *x42*x51
        +coeff[  9]        *x31*x41*x53
        +coeff[ 10]*x11*x21*x32*x41*x52
        +coeff[ 11]        *x31    *x51
        +coeff[ 12]*x11    *x31        
        +coeff[ 13]*x11        *x41    
        +coeff[ 14]        *x31*x42*x51
        +coeff[ 15]*x11*x21        *x52
        +coeff[ 16]    *x21*x33    *x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 17]*x11*x22    *x41*x51
        +coeff[ 18]        *x32    *x54
        +coeff[ 19]            *x42*x54
        +coeff[ 20]*x13    *x32    *x51
        +coeff[ 21]*x13*x21        *x53
        +coeff[ 22]            *x41    
        +coeff[ 23]    *x21        *x51
        +coeff[ 24]    *x21    *x41*x51
        +coeff[ 25]        *x31*x41*x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 26]    *x21        *x52
        +coeff[ 27]*x11    *x32        
        +coeff[ 28]*x11*x21    *x41    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]*x11        *x41*x51
        +coeff[ 31]*x12    *x31        
        +coeff[ 32]*x12        *x41    
        +coeff[ 33]*x12            *x51
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 35]        *x33*x41    
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]    *x21*x31    *x52
        +coeff[ 38]        *x32    *x52
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]*x11    *x32*x41    
        +coeff[ 41]*x11        *x43    
        +coeff[ 42]*x11*x22        *x51
        +coeff[ 43]*x11    *x32    *x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 44]*x11            *x53
        +coeff[ 45]*x12*x22            
        +coeff[ 46]*x12    *x32        
        +coeff[ 47]*x12        *x42    
        +coeff[ 48]*x12            *x52
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]    *x24    *x41    
        +coeff[ 51]    *x21    *x44    
        +coeff[ 52]*x13            *x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 53]    *x21    *x43*x51
        +coeff[ 54]    *x23        *x52
        +coeff[ 55]    *x21*x31*x41*x52
        +coeff[ 56]*x11*x22*x32        
        +coeff[ 57]*x11    *x33*x41    
        +coeff[ 58]*x11*x21*x31*x42    
        +coeff[ 59]*x11*x23        *x51
        +coeff[ 60]*x11*x22*x31    *x51
        +coeff[ 61]*x11*x21*x32    *x51
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 62]*x11    *x33    *x51
        +coeff[ 63]*x11*x21*x31*x41*x51
        +coeff[ 64]*x11    *x32*x41*x51
        +coeff[ 65]*x11    *x32    *x52
        +coeff[ 66]*x12*x22    *x41    
        +coeff[ 67]*x12*x21    *x42    
        +coeff[ 68]*x12*x21    *x41*x51
        +coeff[ 69]*x12        *x41*x52
        +coeff[ 70]    *x23*x33        
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 71]*x13*x21        *x51
        +coeff[ 72]    *x23*x31    *x52
        +coeff[ 73]    *x21*x33    *x52
        +coeff[ 74]    *x21*x32*x41*x52
        +coeff[ 75]    *x21*x31    *x54
        +coeff[ 76]*x11*x24*x31        
        +coeff[ 77]*x11*x23*x32        
        +coeff[ 78]*x11*x21*x34        
        +coeff[ 79]*x11    *x33*x42    
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 80]*x11    *x32*x43    
        +coeff[ 81]*x11    *x31*x44    
        +coeff[ 82]*x11*x24        *x51
        +coeff[ 83]*x11*x22*x32    *x51
        +coeff[ 84]*x11*x21*x31*x42*x51
        +coeff[ 85]*x11*x22    *x41*x52
        +coeff[ 86]*x11*x21*x31*x41*x52
        +coeff[ 87]*x11*x21*x31    *x53
        +coeff[ 88]*x11*x21    *x41*x53
    ;
    v_l_e_predent_250                             =v_l_e_predent_250                             
        +coeff[ 89]*x11    *x31    *x54
        ;

    return v_l_e_predent_250                             ;
}
float x_e_predent_200                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066544E+01;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.10977621E-03,-0.29653946E-02, 0.94862461E-01, 0.41622263E-02,
        -0.24512168E-02,-0.17234250E-02,-0.56881795E-03, 0.24176521E-03,
        -0.69881947E-03,-0.10153117E-02,-0.26471727E-03,-0.18620705E-02,
        -0.13566689E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_200                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_200                             =v_x_e_predent_200                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_200                             ;
}
float t_e_predent_200                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3787399E+01;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.55562824E-01,-0.36018807E-01, 0.85275263E+00, 0.18624189E+00,
         0.34843042E-01,-0.15306654E-01, 0.32541741E-01,-0.12492425E-03,
         0.16220450E-01,-0.19896289E-01,-0.19135125E-01,-0.12252239E-01,
         0.26304158E-02,-0.60360497E-02,-0.90084486E-02, 0.78276021E-03,
         0.70579152E-03,-0.29200942E-02,-0.21377662E-01,-0.15437128E-01,
         0.57000713E-02, 0.17236192E-03,-0.34612995E-01,-0.23894154E-01,
        -0.38412859E-03, 0.29403187E-03,-0.16485751E-02,-0.79169888E-02,
         0.29094404E-03, 0.39599111E-03, 0.34458532E-02, 0.22061581E-02,
        -0.79190754E-03,-0.23912625E-01,-0.18703302E-01,-0.33448529E-03,
         0.24620612E-03,-0.74425590E-03,-0.48233967E-03, 0.16441132E-02,
        -0.10961947E-01, 0.13743080E-03, 0.18932812E-03,-0.33890706E-03,
         0.18262214E-03,-0.18745284E-03, 0.25559519E-03,-0.15613386E-03,
        -0.23191201E-03,-0.27938490E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_200                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_200                             =v_t_e_predent_200                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_200                             =v_t_e_predent_200                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x22*x32    *x52
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_200                             =v_t_e_predent_200                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_200                             =v_t_e_predent_200                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]*x11    *x32    *x52
        +coeff[ 42]    *x21*x31        
        +coeff[ 43]*x11    *x32        
    ;
    v_t_e_predent_200                             =v_t_e_predent_200                             
        +coeff[ 44]*x11*x21    *x41    
        +coeff[ 45]*x11            *x52
        +coeff[ 46]*x12*x22            
        +coeff[ 47]*x11*x22*x31        
        +coeff[ 48]    *x21*x33        
        +coeff[ 49]        *x32*x42    
        ;

    return v_t_e_predent_200                             ;
}
float y_e_predent_200                             (float *x,int m){
    int ncoeff= 47;
    float avdat= -0.4961738E-04;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 48]={
        -0.46317771E-04, 0.14577270E+00, 0.60644902E-01, 0.88070072E-02,
         0.42984188E-02,-0.20513639E-02,-0.14112339E-02,-0.22447307E-02,
        -0.30815838E-05, 0.11013344E-05,-0.15094529E-02,-0.35475273E-03,
        -0.50111790E-03,-0.34282618E-03,-0.11623837E-02,-0.15285228E-02,
        -0.91436662E-03,-0.78563415E-03,-0.68420253E-03,-0.19935006E-02,
        -0.57342718E-03,-0.14260906E-03, 0.61314109E-04,-0.29636858E-05,
         0.26819919E-03, 0.19807993E-03,-0.13870029E-02, 0.36046006E-05,
        -0.19353582E-02,-0.62094943E-03,-0.10322080E-02,-0.82653988E-03,
        -0.65212557E-05, 0.28659450E-04, 0.48597234E-04,-0.66873699E-03,
        -0.34825326E-03,-0.79335048E-04, 0.11485339E-03, 0.21417150E-04,
         0.64851374E-04, 0.11391167E-04, 0.26378968E-04, 0.71293791E-04,
         0.17157574E-04,-0.28806349E-04,-0.58433579E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_200                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_200                             =v_y_e_predent_200                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_200                             =v_y_e_predent_200                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_200                             =v_y_e_predent_200                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24*x31*x42    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]                *x51
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_predent_200                             =v_y_e_predent_200                             
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22*x33        
        +coeff[ 37]*x11*x23*x31        
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x22*x31    *x51
        +coeff[ 40]*x11*x21    *x43    
        +coeff[ 41]*x11    *x31*x43    
        +coeff[ 42]            *x41*x53
        +coeff[ 43]*x11*x21*x31*x42    
    ;
    v_y_e_predent_200                             =v_y_e_predent_200                             
        +coeff[ 44]        *x31    *x53
        +coeff[ 45]*x11*x23    *x41    
        +coeff[ 46]    *x24    *x41*x51
        ;

    return v_y_e_predent_200                             ;
}
float p_e_predent_200                             (float *x,int m){
    int ncoeff= 25;
    float avdat= -0.2772200E-03;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
         0.33656735E-03,-0.79569705E-01,-0.76091461E-01,-0.16949814E-01,
        -0.16023485E-01, 0.72877873E-02, 0.11388914E-01,-0.59718848E-02,
        -0.29022620E-04,-0.51791780E-02, 0.76350156E-03, 0.73528604E-03,
         0.16505978E-02,-0.46351906E-05,-0.22852910E-02,-0.12502794E-02,
         0.76274044E-03,-0.11780992E-02,-0.56504086E-03, 0.50281102E-04,
        -0.87593170E-03,-0.64114633E-03,-0.14140107E-02, 0.40400741E-03,
         0.41831608E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_200                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_200                             =v_p_e_predent_200                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]    *x21*x33    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_predent_200                             =v_p_e_predent_200                             
        +coeff[ 17]    *x23    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]    *x25*x31        
        +coeff[ 21]        *x33    *x52
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]*x11*x21    *x41    
        ;

    return v_p_e_predent_200                             ;
}
float l_e_predent_200                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4620448E-02;
    float xmin[10]={
        -0.39998E-02,-0.60035E-01,-0.59996E-01,-0.30044E-01,-0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60069E-01, 0.59976E-01, 0.30008E-01, 0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44593569E-02,-0.97634979E-02,-0.14927664E-02,-0.55351038E-02,
        -0.58028949E-02, 0.53366087E-03,-0.55039412E-03, 0.12688429E-03,
         0.55198005E-04, 0.19422112E-03,-0.91509707E-03,-0.58405305E-03,
        -0.49896352E-03, 0.56090043E-03, 0.57706749E-03,-0.42306536E-03,
        -0.67840400E-03,-0.84084057E-03,-0.87246823E-03,-0.81550248E-03,
         0.36586350E-03,-0.35833899E-03, 0.45401314E-02,-0.67796878E-03,
        -0.90524228E-03,-0.59769192E-03,-0.22937325E-02,-0.13366269E-02,
        -0.17196069E-04,-0.82175648E-05,-0.24032602E-03, 0.96283518E-04,
         0.38871454E-03,-0.24239699E-03, 0.14777954E-03,-0.12584586E-02,
         0.74249273E-03, 0.57919411E-03, 0.45891742E-04, 0.32698287E-03,
        -0.11015982E-03,-0.36535849E-03,-0.12037368E-03,-0.17335295E-03,
        -0.30300827E-03, 0.33207930E-03, 0.23256025E-03,-0.17931483E-03,
         0.19843118E-03,-0.17503985E-03,-0.32309661E-03, 0.10087462E-03,
        -0.36990707E-03,-0.22949616E-03,-0.18525806E-03,-0.43486562E-03,
         0.23108919E-03,-0.24782997E-03, 0.17918712E-03, 0.22141123E-03,
         0.31598800E-03,-0.46656627E-03, 0.10168591E-02,-0.19320081E-02,
        -0.98905060E-03,-0.12038558E-03, 0.41285417E-04, 0.57930360E-03,
        -0.67116460E-03, 0.72611688E-03, 0.42132466E-03, 0.15369324E-02,
        -0.13611421E-02, 0.53973350E-03,-0.51197509E-03, 0.16936476E-03,
        -0.29131214E-03, 0.36066139E-03, 0.88676301E-04,-0.65467675E-03,
         0.36657101E-03,-0.14843389E-03, 0.83185814E-03, 0.32796923E-03,
        -0.25106073E-03,-0.12122656E-03, 0.34203081E-03, 0.50380267E-03,
         0.51884574E-03, 0.67149987E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_200                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x53
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[  8]                *x51
        +coeff[  9]*x12    *x31        
        +coeff[ 10]        *x32*x42    
        +coeff[ 11]        *x31*x43    
        +coeff[ 12]        *x32    *x52
        +coeff[ 13]    *x21        *x53
        +coeff[ 14]*x11*x22    *x41    
        +coeff[ 15]*x11*x21*x33        
        +coeff[ 16]*x12*x22        *x51
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 17]*x11    *x32*x42*x51
        +coeff[ 18]*x11*x22    *x41*x52
        +coeff[ 19]*x12        *x42*x52
        +coeff[ 20]*x13    *x33        
        +coeff[ 21]*x13    *x31*x42    
        +coeff[ 22]    *x23*x31*x41*x52
        +coeff[ 23]    *x21*x32    *x54
        +coeff[ 24]*x11*x21*x31    *x54
        +coeff[ 25]*x12*x22*x33        
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 26]*x12*x21*x33    *x51
        +coeff[ 27]*x12        *x44*x51
        +coeff[ 28]    *x21            
        +coeff[ 29]            *x41    
        +coeff[ 30]    *x21        *x51
        +coeff[ 31]            *x41*x51
        +coeff[ 32]                *x52
        +coeff[ 33]*x11        *x41    
        +coeff[ 34]*x11            *x51
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 35]    *x21*x32        
        +coeff[ 36]    *x21*x31*x41    
        +coeff[ 37]    *x21    *x42    
        +coeff[ 38]    *x21    *x41*x51
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]            *x42*x51
        +coeff[ 41]*x11        *x42    
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]*x12*x21            
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 44]*x12        *x41    
        +coeff[ 45]*x12            *x51
        +coeff[ 46]    *x22    *x42    
        +coeff[ 47]    *x21    *x42*x51
        +coeff[ 48]    *x22        *x52
        +coeff[ 49]    *x21*x31    *x52
        +coeff[ 50]        *x31*x41*x52
        +coeff[ 51]        *x31    *x53
        +coeff[ 52]                *x54
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 53]*x11*x22*x31        
        +coeff[ 54]*x11*x21*x32        
        +coeff[ 55]*x11    *x31*x42    
        +coeff[ 56]*x11        *x43    
        +coeff[ 57]*x11*x22        *x51
        +coeff[ 58]*x11*x21        *x52
        +coeff[ 59]*x13    *x31        
        +coeff[ 60]    *x24*x31        
        +coeff[ 61]    *x22*x33        
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 62]    *x21*x34        
        +coeff[ 63]    *x23*x31*x41    
        +coeff[ 64]    *x23    *x42    
        +coeff[ 65]    *x22    *x43    
        +coeff[ 66]    *x23*x31    *x51
        +coeff[ 67]        *x34    *x51
        +coeff[ 68]    *x21*x32*x41*x51
        +coeff[ 69]            *x44*x51
        +coeff[ 70]    *x22*x31    *x52
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 71]    *x21*x32    *x52
        +coeff[ 72]    *x21*x31*x41*x52
        +coeff[ 73]            *x43*x52
        +coeff[ 74]        *x32    *x53
        +coeff[ 75]    *x21        *x54
        +coeff[ 76]            *x41*x54
        +coeff[ 77]*x11*x23*x31        
        +coeff[ 78]*x11*x22*x32        
        +coeff[ 79]*x11*x22*x31*x41    
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 80]*x11        *x44    
        +coeff[ 81]*x11*x22*x31    *x51
        +coeff[ 82]*x11    *x32*x41*x51
        +coeff[ 83]*x11    *x31*x42*x51
        +coeff[ 84]*x11        *x43*x51
        +coeff[ 85]*x11    *x32    *x52
        +coeff[ 86]*x11    *x31*x41*x52
        +coeff[ 87]*x12*x23            
        +coeff[ 88]*x12*x22    *x41    
    ;
    v_l_e_predent_200                             =v_l_e_predent_200                             
        +coeff[ 89]*x12*x21*x31    *x51
        ;

    return v_l_e_predent_200                             ;
}
float x_e_predent_175                             (float *x,int m){
    int ncoeff= 14;
    float avdat= -0.6065904E+01;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 15]={
        -0.57365629E-03,-0.29446529E-02, 0.95505595E-01, 0.41266289E-02,
        -0.24590394E-02,-0.17420073E-02,-0.55088889E-03, 0.24411324E-03,
        -0.11974881E-04,-0.69499400E-03,-0.10128567E-02,-0.25963978E-03,
        -0.17938549E-02,-0.13057769E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_175                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_175                             =v_x_e_predent_175                             
        +coeff[  8]    *x23        *x52
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x21        *x52
        +coeff[ 12]    *x23*x31*x41    
        +coeff[ 13]    *x23    *x42    
        ;

    return v_x_e_predent_175                             ;
}
float t_e_predent_175                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3793465E+01;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.62035277E-01,-0.35773557E-01, 0.85954976E+00, 0.18943682E+00,
         0.34343924E-01,-0.15198093E-01, 0.33506583E-01,-0.14264097E-03,
         0.16254924E-01,-0.19878052E-01,-0.19190455E-01,-0.11888564E-01,
         0.26543497E-02,-0.61256341E-02,-0.91326116E-02, 0.11494071E-02,
         0.66164124E-03,-0.29768804E-02,-0.21884207E-01,-0.15513981E-01,
         0.58498196E-02,-0.96607500E-05,-0.34777243E-01,-0.23682123E-01,
        -0.29882172E-03, 0.41278292E-03,-0.18917293E-02,-0.79752114E-02,
         0.34807139E-03, 0.43971266E-03, 0.33158914E-02, 0.20946483E-02,
        -0.88543585E-03,-0.23005657E-01,-0.18062918E-01, 0.72788954E-03,
         0.20247047E-03,-0.79181587E-03,-0.54175645E-03,-0.24606954E-03,
        -0.10562884E-01, 0.10728447E-03,-0.22623071E-03,-0.90286419E-04,
         0.15962873E-03, 0.17522606E-03,-0.10757805E-02,-0.10895546E-02,
         0.26144422E-03, 0.87995251E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_175                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_175                             =v_t_e_predent_175                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_175                             =v_t_e_predent_175                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x22*x32    *x52
        +coeff[ 25]        *x32        
    ;
    v_t_e_predent_175                             =v_t_e_predent_175                             
        +coeff[ 26]*x11*x23            
        +coeff[ 27]    *x22*x32        
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_175                             =v_t_e_predent_175                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]*x11            *x52
        +coeff[ 40]    *x21*x33*x41    
        +coeff[ 41]    *x21*x31        
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]*x11    *x31    *x51
    ;
    v_t_e_predent_175                             =v_t_e_predent_175                             
        +coeff[ 44]        *x31*x41*x51
        +coeff[ 45]    *x21*x32*x41    
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]*x12*x21        *x51
        +coeff[ 49]    *x21*x32    *x51
        ;

    return v_t_e_predent_175                             ;
}
float y_e_predent_175                             (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2379064E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.15056750E-03, 0.14521199E+00, 0.60180999E-01, 0.88220611E-02,
         0.43034712E-02,-0.18539772E-02,-0.12987545E-02,-0.19392798E-02,
        -0.14156285E-02,-0.14236382E-03,-0.33067679E-03,-0.50550589E-03,
        -0.34491046E-03,-0.99276146E-03,-0.31725252E-02,-0.11144992E-02,
        -0.88009355E-03, 0.22381864E-04, 0.17059091E-03,-0.54059627E-04,
        -0.66161592E-04, 0.14352581E-05, 0.26416659E-03,-0.26192121E-04,
         0.19591278E-03,-0.16451300E-02,-0.19895961E-02, 0.86731716E-05,
        -0.53281406E-05, 0.41170140E-04,-0.46402702E-03,-0.64866661E-04,
         0.10322787E-03,-0.40298875E-03, 0.26842325E-04,-0.43041536E-03,
        -0.25951065E-03, 0.26370104E-04, 0.16451695E-04,-0.19721216E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_175                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_175                             =v_y_e_predent_175                             
        +coeff[  8]        *x32*x41    
        +coeff[  9]            *x45    
        +coeff[ 10]        *x33        
        +coeff[ 11]            *x41*x52
        +coeff[ 12]        *x31    *x52
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x22*x31*x42    
        +coeff[ 15]    *x24    *x41    
        +coeff[ 16]    *x24*x31        
    ;
    v_y_e_predent_175                             =v_y_e_predent_175                             
        +coeff[ 17]    *x22    *x45    
        +coeff[ 18]    *x22*x33*x42    
        +coeff[ 19]    *x24*x32*x41    
        +coeff[ 20]    *x24*x33        
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]        *x31*x42*x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]        *x32*x41*x51
        +coeff[ 25]    *x22    *x43    
    ;
    v_y_e_predent_175                             =v_y_e_predent_175                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]                *x51
        +coeff[ 29]        *x33    *x51
        +coeff[ 30]    *x22*x33        
        +coeff[ 31]*x11*x23*x31        
        +coeff[ 32]            *x43*x51
        +coeff[ 33]        *x31*x44    
        +coeff[ 34]    *x22*x31    *x51
    ;
    v_y_e_predent_175                             =v_y_e_predent_175                             
        +coeff[ 35]        *x32*x43    
        +coeff[ 36]        *x33*x42    
        +coeff[ 37]            *x41*x53
        +coeff[ 38]        *x31    *x53
        +coeff[ 39]*x12*x22    *x41    
        ;

    return v_y_e_predent_175                             ;
}
float p_e_predent_175                             (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.3672464E-03;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.31871424E-03,-0.79652846E-01,-0.76197237E-01,-0.16879691E-01,
        -0.16188571E-01, 0.72772601E-02, 0.11376919E-01,-0.60520810E-02,
        -0.49730799E-04,-0.52360357E-02, 0.75841125E-03, 0.73550135E-03,
         0.16828820E-02,-0.65419510E-04,-0.22623308E-02,-0.12446155E-02,
         0.82040997E-03,-0.54767943E-03, 0.20932795E-04,-0.64905576E-03,
        -0.13672049E-02, 0.40481522E-03,-0.10311852E-02, 0.41500956E-03,
        -0.11774939E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_175                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_175                             =v_p_e_predent_175                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]    *x23*x31    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_predent_175                             =v_p_e_predent_175                             
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33    *x52
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23    *x41    
        ;

    return v_p_e_predent_175                             ;
}
float l_e_predent_175                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4551320E-02;
    float xmin[10]={
        -0.40000E-02,-0.60063E-01,-0.59988E-01,-0.30035E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59988E-01, 0.29999E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.44986010E-02, 0.70909911E-04,-0.98864743E-02,-0.13859639E-02,
        -0.57228492E-02,-0.62787849E-02, 0.46880209E-03,-0.89815381E-03,
        -0.32068373E-03, 0.11937659E-03,-0.56532928E-03, 0.21082384E-03,
        -0.12818119E-02, 0.53164409E-03, 0.26417751E-03, 0.41364989E-03,
        -0.80486963E-03,-0.24210406E-02, 0.55182719E-03, 0.41940744E-03,
         0.84824726E-03,-0.26996110E-02, 0.13518862E-02,-0.10802245E-02,
        -0.11198716E-02, 0.18519396E-02,-0.57625439E-03, 0.22705665E-02,
         0.16442527E-02, 0.31916883E-02, 0.49765557E-02,-0.18544090E-02,
        -0.15795606E-02,-0.59166824E-03, 0.16695009E-03, 0.15497058E-03,
         0.10962226E-03,-0.64955377E-04, 0.18281833E-03,-0.12930350E-03,
         0.29043760E-04, 0.64685394E-03, 0.11761061E-05, 0.60682592E-03,
         0.14204867E-03, 0.58287726E-03, 0.13483866E-03, 0.65528764E-03,
         0.57625363E-03, 0.12953617E-03, 0.16229108E-03, 0.17615419E-03,
        -0.23416939E-03,-0.62384427E-03, 0.63447806E-03,-0.56230515E-03,
         0.64387114E-03, 0.64865855E-03,-0.34037777E-03, 0.44045260E-03,
         0.53966756E-03, 0.51008799E-04, 0.56193670E-03, 0.15182536E-03,
        -0.57696522E-03,-0.35473774E-03,-0.58794231E-03,-0.13362343E-03,
         0.94496948E-03,-0.73586903E-05,-0.74633578E-03, 0.46336497E-03,
         0.31670442E-03,-0.14735124E-03,-0.35273796E-03, 0.64048317E-03,
         0.29607702E-03, 0.16867966E-03,-0.45525879E-03,-0.47953377E-03,
         0.81908295E-03,-0.12343373E-02,-0.36636775E-03,-0.31164027E-03,
         0.94013318E-03,-0.65872475E-04,-0.90513245E-03,-0.18236442E-02,
        -0.74955210E-03, 0.17393041E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_175                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[  8]    *x21    *x42    
        +coeff[  9]        *x31*x41*x51
        +coeff[ 10]        *x34        
        +coeff[ 11]    *x21    *x41*x52
        +coeff[ 12]*x11*x21*x31    *x51
        +coeff[ 13]*x11        *x42*x51
        +coeff[ 14]*x11*x21        *x52
        +coeff[ 15]*x13*x21            
        +coeff[ 16]        *x31*x43*x51
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 17]    *x21*x32    *x52
        +coeff[ 18]*x11*x22    *x41*x51
        +coeff[ 19]*x12*x22        *x51
        +coeff[ 20]*x12        *x41*x52
        +coeff[ 21]    *x23*x32*x41    
        +coeff[ 22]*x11*x21*x33    *x51
        +coeff[ 23]*x11*x22    *x42*x51
        +coeff[ 24]*x11*x23        *x52
        +coeff[ 25]*x12*x21*x32    *x51
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 26]*x13        *x43    
        +coeff[ 27]    *x21*x34    *x52
        +coeff[ 28]*x11*x24*x31    *x51
        +coeff[ 29]*x11*x22*x32*x41*x51
        +coeff[ 30]    *x23*x32*x41*x52
        +coeff[ 31]        *x34*x42*x52
        +coeff[ 32]    *x21*x34    *x53
        +coeff[ 33]*x13        *x41*x53
        +coeff[ 34]        *x31        
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 35]    *x21    *x41    
        +coeff[ 36]    *x21        *x51
        +coeff[ 37]        *x33        
        +coeff[ 38]        *x32*x41    
        +coeff[ 39]        *x31    *x52
        +coeff[ 40]            *x41*x52
        +coeff[ 41]*x11*x21*x31        
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]        *x32*x42    
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 44]    *x23        *x51
        +coeff[ 45]    *x21*x31*x41*x51
        +coeff[ 46]        *x31*x42*x51
        +coeff[ 47]    *x21*x31    *x52
        +coeff[ 48]            *x42*x52
        +coeff[ 49]*x11    *x33        
        +coeff[ 50]*x11        *x43    
        +coeff[ 51]*x11        *x41*x52
        +coeff[ 52]*x12    *x31*x41    
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 53]*x12*x21        *x51
        +coeff[ 54]    *x21    *x44    
        +coeff[ 55]    *x22*x31*x41*x51
        +coeff[ 56]        *x33*x41*x51
        +coeff[ 57]    *x21*x31*x42*x51
        +coeff[ 58]    *x21*x31*x41*x52
        +coeff[ 59]            *x43*x52
        +coeff[ 60]    *x22        *x53
        +coeff[ 61]    *x21    *x41*x53
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 62]        *x31*x41*x53
        +coeff[ 63]    *x21        *x54
        +coeff[ 64]            *x41*x54
        +coeff[ 65]*x11*x24            
        +coeff[ 66]*x11*x23*x31        
        +coeff[ 67]*x11*x21*x33        
        +coeff[ 68]*x11*x21*x32*x41    
        +coeff[ 69]*x11*x21*x31*x41*x51
        +coeff[ 70]*x11*x21    *x42*x51
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 71]*x11    *x31*x41*x52
        +coeff[ 72]*x11*x21        *x53
        +coeff[ 73]*x12*x22    *x41    
        +coeff[ 74]*x12        *x43    
        +coeff[ 75]*x12*x21    *x41*x51
        +coeff[ 76]*x13*x22            
        +coeff[ 77]    *x24*x32        
        +coeff[ 78]*x13*x21    *x41    
        +coeff[ 79]*x13    *x31*x41    
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 80]    *x24    *x42    
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22    *x44    
        +coeff[ 83]    *x24*x31    *x51
        +coeff[ 84]    *x23*x32    *x51
        +coeff[ 85]    *x24        *x52
        +coeff[ 86]    *x23*x31    *x52
        +coeff[ 87]    *x21*x32*x41*x52
        +coeff[ 88]    *x22    *x42*x52
    ;
    v_l_e_predent_175                             =v_l_e_predent_175                             
        +coeff[ 89]        *x33    *x53
        ;

    return v_l_e_predent_175                             ;
}
float x_e_predent_150                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066705E+01;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.23571207E-03,-0.29179999E-02, 0.96365944E-01, 0.40846560E-02,
        -0.24256229E-02,-0.17118768E-02,-0.55604120E-03, 0.24063999E-03,
        -0.68939279E-03,-0.99088834E-03,-0.25336942E-03,-0.18171462E-02,
        -0.13387104E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_150                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_150                             =v_x_e_predent_150                             
        +coeff[  8]    *x23            
        +coeff[  9]    *x21*x32        
        +coeff[ 10]    *x21        *x52
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_150                             ;
}
float t_e_predent_150                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3787541E+01;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.56111772E-01,-0.35536330E-01, 0.86874175E+00, 0.19384760E+00,
         0.33709735E-01,-0.15233250E-01, 0.35017006E-01,-0.11435999E-03,
         0.16139008E-01,-0.20104041E-01,-0.18969817E-01,-0.11872579E-01,
         0.26066641E-02,-0.61578327E-02,-0.89584701E-02, 0.11468017E-02,
         0.73922740E-03,-0.27925568E-02,-0.21674262E-01,-0.16005391E-01,
         0.58867242E-02,-0.71358058E-03,-0.12233962E-03, 0.50136709E-03,
        -0.19156161E-02,-0.81029609E-02,-0.34187313E-01,-0.23894858E-01,
         0.35390808E-03, 0.46454647E-03, 0.36623790E-02, 0.25597461E-02,
        -0.76434773E-03,-0.99520152E-02,-0.17653575E-01,-0.22372972E-03,
         0.20926152E-03,-0.10160377E-02,-0.49681962E-03, 0.13891705E-02,
         0.80958249E-04,-0.22275567E-01,-0.34082861E-03, 0.16049415E-03,
        -0.10188652E-03,-0.19856596E-03,-0.11116368E-02,-0.10385901E-02,
         0.26536145E-03, 0.51153445E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_150                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_150                             =v_t_e_predent_150                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_150                             =v_t_e_predent_150                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]    *x22*x32    *x52
        +coeff[ 23]        *x32        
        +coeff[ 24]*x11*x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_predent_150                             =v_t_e_predent_150                             
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x33*x41    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_150                             =v_t_e_predent_150                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]    *x21*x32    *x51
        +coeff[ 40]*x13    *x32        
        +coeff[ 41]    *x21*x32*x42    
        +coeff[ 42]*x11    *x32        
        +coeff[ 43]    *x21    *x41*x51
    ;
    v_t_e_predent_150                             =v_t_e_predent_150                             
        +coeff[ 44]*x11            *x52
        +coeff[ 45]*x11*x21    *x42    
        +coeff[ 46]        *x32*x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x13    *x31*x41    
        ;

    return v_t_e_predent_150                             ;
}
float y_e_predent_150                             (float *x,int m){
    int ncoeff= 45;
    float avdat= -0.7221617E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 46]={
         0.72933167E-05, 0.14461309E+00, 0.59613090E-01, 0.88090207E-02,
         0.42977226E-02,-0.21376829E-02,-0.14290675E-02,-0.22003092E-02,
         0.17993671E-05,-0.59998952E-05,-0.14817677E-02,-0.34415198E-03,
        -0.50671858E-03,-0.34122731E-03,-0.11480581E-02,-0.86713606E-03,
        -0.90105995E-03,-0.77869795E-03,-0.10165757E-02,-0.33457680E-02,
        -0.85728668E-03,-0.19005888E-03,-0.11586417E-03,-0.46328641E-05,
         0.25996944E-03, 0.18166387E-03,-0.77984057E-03,-0.22896573E-04,
        -0.29433386E-02,-0.80663210E-03,-0.13592452E-02,-0.20086812E-02,
        -0.51914553E-05,-0.46699670E-05, 0.47311962E-04,-0.30993152E-03,
        -0.52328898E-04,-0.55375084E-03, 0.14815577E-03, 0.29595780E-04,
        -0.29134960E-03, 0.26286041E-04, 0.23927056E-04,-0.58688289E-04,
         0.62594336E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_150                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_150                             =v_y_e_predent_150                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_150                             =v_y_e_predent_150                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_150                             =v_y_e_predent_150                             
        +coeff[ 26]    *x22*x32*x41    
        +coeff[ 27]            *x45*x51
        +coeff[ 28]    *x22*x31*x44    
        +coeff[ 29]    *x24    *x43    
        +coeff[ 30]    *x24*x31*x42    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]                *x51
        +coeff[ 33]    *x22    *x41*x51
        +coeff[ 34]        *x33    *x51
    ;
    v_y_e_predent_150                             =v_y_e_predent_150                             
        +coeff[ 35]    *x22*x33        
        +coeff[ 36]*x11*x23*x31        
        +coeff[ 37]    *x22*x34*x41    
        +coeff[ 38]            *x43*x51
        +coeff[ 39]    *x22*x31    *x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]            *x41*x53
        +coeff[ 42]        *x31    *x53
        +coeff[ 43]    *x22    *x43*x51
    ;
    v_y_e_predent_150                             =v_y_e_predent_150                             
        +coeff[ 44]    *x22*x32*x41*x51
        ;

    return v_y_e_predent_150                             ;
}
float p_e_predent_150                             (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.9171115E-04;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.52267202E-04,-0.79762921E-01,-0.76345026E-01,-0.17100662E-01,
        -0.16399233E-01, 0.72638695E-02, 0.11371692E-01,-0.61978875E-02,
        -0.49969440E-04,-0.53578163E-02, 0.75727061E-03, 0.72990265E-03,
         0.17076245E-02,-0.38150120E-04,-0.22810872E-02,-0.12450673E-02,
         0.82466990E-03,-0.54977398E-03, 0.70401125E-04,-0.64565835E-03,
        -0.14297687E-02, 0.39274007E-03,-0.10553980E-02, 0.41055056E-03,
        -0.12342272E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_150                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_150                             =v_p_e_predent_150                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]*x11    *x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]    *x23*x31    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x21*x31    *x51
    ;
    v_p_e_predent_150                             =v_p_e_predent_150                             
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33    *x52
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21*x31        
        +coeff[ 22]    *x23*x31        
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]    *x23    *x41    
        ;

    return v_p_e_predent_150                             ;
}
float l_e_predent_150                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4575332E-02;
    float xmin[10]={
        -0.39990E-02,-0.60060E-01,-0.59999E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60049E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.46856175E-02,-0.10247027E-01,-0.23453098E-02,-0.62390356E-02,
        -0.63695335E-02, 0.55859971E-03,-0.87930873E-03, 0.23889587E-03,
         0.51049591E-03, 0.96378155E-03, 0.36924900E-03, 0.11812459E-03,
        -0.29939692E-03,-0.51232311E-03, 0.35515302E-03,-0.43851582E-03,
        -0.38243525E-04,-0.18801834E-02, 0.29212169E-03, 0.80391142E-03,
         0.38325164E-03,-0.68127164E-02,-0.54157176E-02, 0.19772446E-02,
         0.53117052E-04, 0.81231856E-05, 0.10695276E-03,-0.19422466E-03,
         0.12793964E-03, 0.36774963E-03,-0.38035790E-03, 0.70291245E-03,
        -0.53414569E-03,-0.99693506E-03, 0.52940639E-04, 0.43986796E-03,
         0.17429424E-03, 0.47520510E-03, 0.78805140E-03, 0.28807946E-03,
         0.32732182E-03,-0.26488869E-03,-0.23600481E-03, 0.21189316E-03,
         0.17461530E-03,-0.17381795E-04,-0.33813182E-03, 0.21847984E-03,
        -0.16623372E-03, 0.23452567E-03, 0.30306782E-03, 0.18930688E-03,
         0.36542045E-03,-0.77012111E-03,-0.35215658E-03,-0.19177765E-02,
        -0.70832053E-03, 0.87522320E-04,-0.56850445E-03, 0.24784785E-02,
         0.12526320E-02,-0.80449245E-03,-0.58461138E-03,-0.12308477E-02,
         0.41656769E-03,-0.10587341E-03,-0.68452809E-03,-0.79252396E-03,
        -0.46975186E-03,-0.78621350E-03, 0.36760207E-03, 0.18025652E-03,
         0.87524054E-03, 0.55787532E-03,-0.55182673E-03,-0.62703423E-03,
        -0.26687075E-03,-0.40148688E-03, 0.44526308E-03,-0.15623186E-03,
         0.78278698E-03, 0.48864377E-03, 0.16310920E-02,-0.41246670E-03,
         0.42761504E-03, 0.15280279E-02, 0.43109080E-03, 0.54378738E-03,
         0.30210419E-03, 0.36551288E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_150                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]            *x42*x51
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[  8]        *x33*x41*x51
        +coeff[  9]        *x32    *x53
        +coeff[ 10]                *x51
        +coeff[ 11]            *x41*x51
        +coeff[ 12]    *x21    *x41*x51
        +coeff[ 13]*x11*x21        *x51
        +coeff[ 14]            *x44    
        +coeff[ 15]    *x23*x31    *x51
        +coeff[ 16]*x11        *x44    
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 17]*x11*x21*x31*x41*x51
        +coeff[ 18]*x11        *x43*x51
        +coeff[ 19]*x12*x21    *x41*x51
        +coeff[ 20]    *x22        *x54
        +coeff[ 21]    *x21*x32*x41*x53
        +coeff[ 22]    *x21*x31*x42*x53
        +coeff[ 23]*x12*x22    *x42*x51
        +coeff[ 24]        *x31        
        +coeff[ 25]*x11                
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 26]    *x21        *x51
        +coeff[ 27]*x11    *x31        
        +coeff[ 28]    *x22    *x41    
        +coeff[ 29]        *x32*x41    
        +coeff[ 30]    *x21    *x42    
        +coeff[ 31]        *x31*x41*x51
        +coeff[ 32]                *x53
        +coeff[ 33]*x11            *x52
        +coeff[ 34]*x12        *x41    
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 35]*x13                
        +coeff[ 36]    *x23*x31        
        +coeff[ 37]        *x34        
        +coeff[ 38]    *x22*x31*x41    
        +coeff[ 39]    *x22    *x41*x51
        +coeff[ 40]    *x21*x31*x41*x51
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]    *x21    *x41*x52
        +coeff[ 43]*x11    *x33        
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 44]*x11*x21    *x42    
        +coeff[ 45]*x11    *x31*x41*x51
        +coeff[ 46]*x11        *x42*x51
        +coeff[ 47]*x11*x21        *x52
        +coeff[ 48]*x13*x21            
        +coeff[ 49]*x13    *x31        
        +coeff[ 50]    *x24*x31        
        +coeff[ 51]    *x21*x34        
        +coeff[ 52]    *x21*x33*x41    
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 53]    *x22*x31*x42    
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]        *x32*x43    
        +coeff[ 56]        *x31*x44    
        +coeff[ 57]*x13            *x51
        +coeff[ 58]    *x22*x32    *x51
        +coeff[ 59]    *x21*x32*x41*x51
        +coeff[ 60]    *x21*x31*x42*x51
        +coeff[ 61]    *x21    *x43*x51
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 62]        *x31*x43*x51
        +coeff[ 63]    *x21*x31*x41*x52
        +coeff[ 64]    *x22        *x53
        +coeff[ 65]    *x21        *x54
        +coeff[ 66]*x11*x21*x32    *x51
        +coeff[ 67]*x11    *x33    *x51
        +coeff[ 68]*x11*x22    *x41*x51
        +coeff[ 69]*x11*x21    *x42*x51
        +coeff[ 70]*x11*x21        *x53
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 71]*x11    *x31    *x53
        +coeff[ 72]*x11            *x54
        +coeff[ 73]*x12*x21    *x42    
        +coeff[ 74]*x12*x22        *x51
        +coeff[ 75]*x12    *x32    *x51
        +coeff[ 76]*x12*x21        *x52
        +coeff[ 77]*x12        *x41*x52
        +coeff[ 78]*x12            *x53
        +coeff[ 79]*x13*x22            
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 80]    *x24*x32        
        +coeff[ 81]    *x23*x32*x41    
        +coeff[ 82]    *x22*x33*x41    
        +coeff[ 83]*x13        *x42    
        +coeff[ 84]    *x24    *x42    
        +coeff[ 85]    *x22*x32*x42    
        +coeff[ 86]*x13*x21        *x51
        +coeff[ 87]*x13    *x31    *x51
        +coeff[ 88]*x13        *x41*x51
    ;
    v_l_e_predent_150                             =v_l_e_predent_150                             
        +coeff[ 89]        *x31*x43*x52
        ;

    return v_l_e_predent_150                             ;
}
float x_e_predent_125                             (float *x,int m){
    int ncoeff= 13;
    float avdat= -0.6066721E+01;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 14]={
         0.26822736E-03,-0.28816622E-02, 0.97573377E-01, 0.40218364E-02,
        -0.24586807E-02,-0.17267874E-02,-0.50844683E-03, 0.23626081E-03,
        -0.25258321E-03,-0.68934006E-03,-0.10032183E-02,-0.16791817E-02,
        -0.12558572E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_125                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]    *x21*x31*x41    
        +coeff[  5]    *x21    *x42    
        +coeff[  6]    *x23*x32        
        +coeff[  7]*x11            *x51
    ;
    v_x_e_predent_125                             =v_x_e_predent_125                             
        +coeff[  8]    *x21        *x52
        +coeff[  9]    *x23            
        +coeff[ 10]    *x21*x32        
        +coeff[ 11]    *x23*x31*x41    
        +coeff[ 12]    *x23    *x42    
        ;

    return v_x_e_predent_125                             ;
}
float t_e_predent_125                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3789183E+01;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.57587564E-01,-0.35254505E-01, 0.88177234E+00, 0.19987662E+00,
         0.32885905E-01,-0.15520069E-01, 0.37035171E-01,-0.13679276E-03,
         0.16127508E-01,-0.19892544E-01,-0.19130582E-01,-0.11847644E-01,
         0.25667532E-02,-0.61522238E-02,-0.89881346E-02, 0.10272295E-02,
         0.57888660E-03,-0.27855677E-02,-0.22397650E-01,-0.15962467E-01,
         0.60049286E-02,-0.81184648E-04, 0.61111583E-03, 0.29026432E-03,
        -0.16260678E-02,-0.78654122E-02,-0.33909492E-01,-0.23583556E-01,
         0.35598286E-03, 0.42445856E-03, 0.26724043E-02, 0.26401170E-02,
        -0.93359541E-03,-0.23442719E-01,-0.18620312E-01, 0.32988097E-04,
         0.36553826E-03,-0.66774496E-03,-0.43523789E-03,-0.19580498E-03,
         0.13335646E-02,-0.35636287E-03,-0.10639332E-01, 0.18014342E-02,
         0.15918919E-03, 0.50516543E-03, 0.26019453E-03,-0.36302677E-03,
         0.30834333E-03,-0.35007537E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_predent_125                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_125                             =v_t_e_predent_125                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_125                             =v_t_e_predent_125                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]        *x32    *x52
        +coeff[ 22]    *x22*x33*x41    
        +coeff[ 23]        *x32        
        +coeff[ 24]*x11*x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_predent_125                             =v_t_e_predent_125                             
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_predent_125                             =v_t_e_predent_125                             
        +coeff[ 35]    *x23*x32    *x51
        +coeff[ 36]*x12*x21            
        +coeff[ 37]*x11    *x31*x41    
        +coeff[ 38]*x11        *x42    
        +coeff[ 39]*x11            *x52
        +coeff[ 40]    *x21*x32    *x51
        +coeff[ 41]*x13    *x32        
        +coeff[ 42]    *x21*x33*x41    
        +coeff[ 43]    *x21*x31*x43*x51
    ;
    v_t_e_predent_125                             =v_t_e_predent_125                             
        +coeff[ 44]*x13                
        +coeff[ 45]*x11*x21*x31*x41    
        +coeff[ 46]*x11*x21    *x42    
        +coeff[ 47]        *x31*x43    
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x11*x22*x32        
        ;

    return v_t_e_predent_125                             ;
}
float y_e_predent_125                             (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.2341608E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.24341115E-03, 0.14371470E+00, 0.58739584E-01, 0.88366792E-02,
         0.43156883E-02,-0.18903904E-02,-0.12925341E-02,-0.18805309E-02,
        -0.10668650E-03,-0.48937323E-03,-0.12861883E-02,-0.32063585E-03,
        -0.50244300E-03,-0.33859789E-03,-0.99723914E-03,-0.31877847E-02,
        -0.11498148E-02,-0.91563363E-03,-0.10422531E-03,-0.20179062E-03,
        -0.79598372E-04,-0.45383542E-04, 0.18430249E-03, 0.11573642E-04,
         0.25530352E-03, 0.19438620E-03,-0.15808543E-02,-0.19933060E-02,
        -0.56771329E-04, 0.18938506E-04, 0.49088769E-04,-0.40527363E-03,
         0.44424072E-04,-0.46939039E-03,-0.97654665E-05, 0.99446261E-04,
         0.25555530E-04,-0.29681390E-03,-0.10246349E-03,-0.86132844E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_y_e_predent_125                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_125                             =v_y_e_predent_125                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_125                             =v_y_e_predent_125                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21    *x41    
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_125                             =v_y_e_predent_125                             
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]*x11*x23*x31        
        +coeff[ 29]            *x45*x51
        +coeff[ 30]    *x22    *x41*x51
        +coeff[ 31]        *x31*x44    
        +coeff[ 32]        *x33    *x51
        +coeff[ 33]    *x22*x33        
        +coeff[ 34]*x12        *x41    
    ;
    v_y_e_predent_125                             =v_y_e_predent_125                             
        +coeff[ 35]            *x43*x51
        +coeff[ 36]    *x22*x31    *x51
        +coeff[ 37]        *x33*x42    
        +coeff[ 38]        *x34*x41    
        +coeff[ 39]    *x24    *x41*x51
        ;

    return v_y_e_predent_125                             ;
}
float p_e_predent_125                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.1238802E-03;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.13707779E-03,-0.79886548E-01,-0.76591171E-01,-0.17377794E-01,
        -0.16702646E-01, 0.72399476E-02, 0.11358060E-01,-0.63817292E-02,
        -0.70672861E-04,-0.54989993E-02, 0.17690887E-02, 0.75036206E-03,
         0.72958530E-03, 0.83983055E-03,-0.22651169E-02,-0.12418937E-02,
        -0.55020500E-03, 0.52853815E-04,-0.65751409E-03,-0.13990954E-02,
         0.40697449E-03,-0.11322118E-02, 0.42985968E-03,-0.13074853E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_125                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_125                             =v_p_e_predent_125                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]            *x41*x52
    ;
    v_p_e_predent_125                             =v_p_e_predent_125                             
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]        *x33    *x52
        +coeff[ 19]        *x32*x41    
        +coeff[ 20]*x11*x21*x31        
        +coeff[ 21]    *x23*x31        
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x23    *x41    
        ;

    return v_p_e_predent_125                             ;
}
float l_e_predent_125                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4684381E-02;
    float xmin[10]={
        -0.39992E-02,-0.60047E-01,-0.59964E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60057E-01, 0.59982E-01, 0.30031E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.47550378E-02,-0.54083277E-04,-0.10563930E-01,-0.17812978E-02,
        -0.58852602E-02,-0.60596750E-02, 0.73320506E-03,-0.78608829E-03,
         0.29660307E-03, 0.37402703E-03, 0.56901079E-03,-0.40725979E-03,
         0.31531122E-03,-0.18908756E-03, 0.21173568E-02, 0.10285318E-02,
        -0.55719208E-03,-0.15142888E-02, 0.65247057E-03,-0.18504980E-02,
        -0.96710859E-03, 0.11898546E-02, 0.71907492E-03,-0.39630658E-02,
        -0.25311313E-02, 0.19369688E-02, 0.44413148E-02, 0.23523630E-02,
        -0.15926170E-02,-0.88812393E-03, 0.10065523E-02,-0.13479121E-02,
         0.87498185E-04,-0.42927274E-03, 0.40230178E-04, 0.16095904E-03,
         0.34206442E-03,-0.10883973E-03,-0.93511713E-03, 0.20184868E-03,
         0.40755860E-03,-0.76463778E-03, 0.25951260E-03,-0.99519639E-04,
         0.52765561E-04,-0.85406398E-04, 0.16824686E-02, 0.89123257E-03,
        -0.40999067E-03, 0.89272100E-04,-0.12467898E-03, 0.32315255E-03,
        -0.29918601E-03,-0.25673810E-03, 0.23068789E-03,-0.31387765E-03,
         0.16629558E-03, 0.26300189E-03,-0.28818648E-03, 0.46988737E-03,
        -0.61584020E-03,-0.48060337E-03,-0.37206252E-03,-0.51991164E-03,
        -0.12064181E-03, 0.17363612E-03,-0.11201041E-03,-0.31264071E-03,
         0.12675987E-03, 0.23390455E-02, 0.17195143E-02,-0.56498160E-03,
         0.86112181E-03, 0.14063681E-03, 0.11163692E-02,-0.49469672E-03,
         0.59241743E-03, 0.59520308E-03, 0.22955966E-03,-0.29013387E-03,
        -0.40466213E-03, 0.26404671E-03, 0.85711287E-03,-0.33840819E-03,
         0.14851656E-02,-0.10326084E-02, 0.95369108E-03,-0.17434505E-02,
        -0.13472746E-02,-0.25018325E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_125                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]    *x22            
        +coeff[  3]        *x32        
        +coeff[  4]        *x31*x41    
        +coeff[  5]            *x42    
        +coeff[  6]*x11*x21            
        +coeff[  7]    *x22        *x51
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[  8]                *x53
        +coeff[  9]        *x31*x41*x53
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]*x11*x21*x32        
        +coeff[ 12]*x12        *x42    
        +coeff[ 13]*x12    *x31    *x51
        +coeff[ 14]        *x33*x42    
        +coeff[ 15]    *x22*x31*x41*x51
        +coeff[ 16]*x11*x22*x31    *x51
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 17]*x11*x21*x32    *x51
        +coeff[ 18]*x12        *x42*x51
        +coeff[ 19]        *x34*x41*x51
        +coeff[ 20]*x11*x22        *x53
        +coeff[ 21]*x12    *x32*x41*x51
        +coeff[ 22]*x13*x22        *x51
        +coeff[ 23]    *x23*x33    *x51
        +coeff[ 24]    *x24*x31*x41*x51
        +coeff[ 25]*x11*x23*x32    *x51
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 26]*x11    *x33*x42*x51
        +coeff[ 27]*x11    *x32*x43*x51
        +coeff[ 28]*x12    *x33*x42    
        +coeff[ 29]*x12        *x43*x52
        +coeff[ 30]    *x24        *x54
        +coeff[ 31]    *x22*x32    *x54
        +coeff[ 32]*x11                
        +coeff[ 33]                *x52
        +coeff[ 34]*x11    *x31        
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 35]*x11        *x41    
        +coeff[ 36]    *x21*x32        
        +coeff[ 37]        *x33        
        +coeff[ 38]    *x21*x31    *x51
        +coeff[ 39]    *x21    *x41*x51
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]*x11*x22            
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]    *x23*x31        
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 44]    *x23    *x41    
        +coeff[ 45]    *x21*x32*x41    
        +coeff[ 46]    *x21*x31*x42    
        +coeff[ 47]        *x32*x41*x51
        +coeff[ 48]        *x31*x41*x52
        +coeff[ 49]    *x21        *x53
        +coeff[ 50]            *x41*x53
        +coeff[ 51]                *x54
        +coeff[ 52]*x11    *x33        
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 53]*x11    *x32    *x51
        +coeff[ 54]*x11*x21    *x41*x51
        +coeff[ 55]*x11*x21        *x52
        +coeff[ 56]*x11    *x31    *x52
        +coeff[ 57]*x12*x22            
        +coeff[ 58]*x12    *x32        
        +coeff[ 59]    *x24*x31        
        +coeff[ 60]    *x23*x32        
        +coeff[ 61]    *x22*x33        
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 62]*x13        *x41    
        +coeff[ 63]    *x23*x31*x41    
        +coeff[ 64]    *x22*x31*x42    
        +coeff[ 65]        *x32*x43    
        +coeff[ 66]    *x21    *x44    
        +coeff[ 67]        *x31*x44    
        +coeff[ 68]*x13            *x51
        +coeff[ 69]    *x23*x31    *x51
        +coeff[ 70]    *x21*x33    *x51
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 71]        *x31*x42*x52
        +coeff[ 72]*x11*x24            
        +coeff[ 73]*x11    *x34        
        +coeff[ 74]*x11*x22*x31*x41    
        +coeff[ 75]*x11    *x33*x41    
        +coeff[ 76]*x11*x22    *x42    
        +coeff[ 77]*x11    *x32*x41*x51
        +coeff[ 78]*x11*x21*x31    *x52
        +coeff[ 79]*x12*x21    *x42    
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 80]*x12*x21*x31    *x51
        +coeff[ 81]*x12    *x31    *x52
        +coeff[ 82]    *x24*x32        
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]    *x24*x31*x41    
        +coeff[ 85]    *x23*x32*x41    
        +coeff[ 86]    *x24    *x42    
        +coeff[ 87]    *x23*x31*x42    
        +coeff[ 88]    *x21*x33*x42    
    ;
    v_l_e_predent_125                             =v_l_e_predent_125                             
        +coeff[ 89]*x13    *x31    *x51
        ;

    return v_l_e_predent_125                             ;
}
float x_e_predent_100                             (float *x,int m){
    int ncoeff= 15;
    float avdat= -0.6067134E+01;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 16]={
         0.70193032E-03,-0.28225817E-02, 0.99367313E-01, 0.39271181E-02,
         0.23732152E-03,-0.67094481E-03,-0.23785748E-02,-0.16833227E-02,
        -0.16160757E-04,-0.21204007E-05,-0.55388734E-03,-0.95652055E-03,
        -0.24932218E-03,-0.17389674E-02,-0.12963739E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_predent_100                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x11            *x51
        +coeff[  5]    *x23            
        +coeff[  6]    *x21*x31*x41    
        +coeff[  7]    *x21    *x42    
    ;
    v_x_e_predent_100                             =v_x_e_predent_100                             
        +coeff[  8]*x12*x21*x32        
        +coeff[  9]    *x21*x32    *x52
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]    *x21*x32        
        +coeff[ 12]    *x21        *x52
        +coeff[ 13]    *x23*x31*x41    
        +coeff[ 14]    *x23    *x42    
        ;

    return v_x_e_predent_100                             ;
}
float t_e_predent_100                             (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.3787585E+01;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
        -0.55886954E-01,-0.34528177E-01, 0.90122354E+00, 0.20927940E+00,
         0.32095619E-01,-0.15405150E-01, 0.40205210E-01,-0.12890741E-03,
         0.15897701E-01,-0.19516857E-01,-0.18860800E-01,-0.11912148E-01,
         0.25988794E-02,-0.63612345E-02,-0.88861426E-02, 0.13892131E-02,
         0.67258446E-03,-0.27754165E-02,-0.23609225E-01,-0.16272247E-01,
         0.58633648E-02,-0.11772208E-02,-0.38712448E-03, 0.46758453E-03,
        -0.20478403E-02,-0.81985369E-02,-0.34638483E-01,-0.24085296E-01,
         0.31091811E-03, 0.59698778E-03,-0.84339740E-03,-0.17661950E-01,
         0.35108037E-02, 0.25346545E-02,-0.99197803E-02,-0.21951795E-01,
         0.40180629E-03, 0.18477700E-03,-0.36801292E-04, 0.21200288E-03,
        -0.38745056E-03,-0.66674768E-03,-0.41972881E-03,-0.14332970E-03,
        -0.86411630E-03,-0.12764635E-02,-0.10848565E-03, 0.11448525E-02,
         0.27472561E-03,-0.31598592E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_t_e_predent_100                             =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]    *x22            
        +coeff[  4]    *x21        *x51
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x23            
        +coeff[  7]                *x51
    ;
    v_t_e_predent_100                             =v_t_e_predent_100                             
        +coeff[  8]    *x22        *x51
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11            *x51
        +coeff[ 13]*x11*x22            
        +coeff[ 14]    *x21*x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]            *x42    
    ;
    v_t_e_predent_100                             =v_t_e_predent_100                             
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x22*x33*x41    
        +coeff[ 22]    *x22*x32    *x52
        +coeff[ 23]        *x32        
        +coeff[ 24]*x11*x23            
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_predent_100                             =v_t_e_predent_100                             
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]*x12                
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x22        *x52
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x21    *x42*x51
        +coeff[ 34]    *x21*x33*x41    
    ;
    v_t_e_predent_100                             =v_t_e_predent_100                             
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]*x12*x21*x32    *x51
        +coeff[ 37]    *x23*x32    *x51
        +coeff[ 38]        *x31        
        +coeff[ 39]*x12*x21            
        +coeff[ 40]*x11    *x32        
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]*x11        *x42    
        +coeff[ 43]*x11            *x52
    ;
    v_t_e_predent_100                             =v_t_e_predent_100                             
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]        *x31*x43    
        +coeff[ 46]*x12*x21        *x51
        +coeff[ 47]    *x21*x32    *x51
        +coeff[ 48]*x12*x23    *x41    
        +coeff[ 49]    *x22*x32*x42    
        ;

    return v_t_e_predent_100                             ;
}
float y_e_predent_100                             (float *x,int m){
    int ncoeff= 44;
    float avdat= -0.1038737E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 45]={
         0.95150375E-03, 0.14235945E+00, 0.57518657E-01, 0.88376431E-02,
         0.43118224E-02,-0.19178543E-02,-0.12908863E-02,-0.18450216E-02,
        -0.66257191E-04,-0.23059141E-03,-0.13054259E-02,-0.30845523E-03,
        -0.50577387E-03,-0.33944452E-03,-0.10233879E-02,-0.32667101E-02,
        -0.11919277E-02,-0.93037786E-03,-0.17971938E-03,-0.80406584E-03,
        -0.22692695E-04,-0.32635260E-04, 0.73124736E-03, 0.35510106E-06,
         0.24184863E-03, 0.17601998E-03,-0.15348922E-02,-0.19573481E-02,
        -0.49218541E-03,-0.59045433E-05,-0.55380328E-05, 0.32708980E-04,
        -0.40897899E-03, 0.45361547E-04,-0.51845440E-04, 0.88149491E-05,
         0.10630002E-03, 0.20661959E-04,-0.26590622E-03,-0.90675749E-04,
         0.21351112E-04, 0.16317357E-04,-0.22419127E-03,-0.65621542E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_predent_100                             =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]            *x41*x51
        +coeff[  4]        *x31    *x51
        +coeff[  5]    *x22    *x41    
        +coeff[  6]    *x22*x31        
        +coeff[  7]        *x31*x42    
    ;
    v_y_e_predent_100                             =v_y_e_predent_100                             
        +coeff[  8]            *x45    
        +coeff[  9]        *x32*x43    
        +coeff[ 10]        *x32*x41    
        +coeff[ 11]        *x33        
        +coeff[ 12]            *x41*x52
        +coeff[ 13]        *x31    *x52
        +coeff[ 14]            *x43    
        +coeff[ 15]    *x22*x31*x42    
        +coeff[ 16]    *x24    *x41    
    ;
    v_y_e_predent_100                             =v_y_e_predent_100                             
        +coeff[ 17]    *x24*x31        
        +coeff[ 18]    *x22    *x45    
        +coeff[ 19]    *x22*x32*x43    
        +coeff[ 20]    *x24*x32*x41    
        +coeff[ 21]    *x24*x33        
        +coeff[ 22]    *x22*x32*x45    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]        *x31*x42*x51
        +coeff[ 25]        *x32*x41*x51
    ;
    v_y_e_predent_100                             =v_y_e_predent_100                             
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x22*x32*x41    
        +coeff[ 28]    *x22*x33        
        +coeff[ 29]            *x45*x51
        +coeff[ 30]                *x51
        +coeff[ 31]    *x22    *x41*x51
        +coeff[ 32]        *x31*x44    
        +coeff[ 33]        *x33    *x51
        +coeff[ 34]*x11*x23*x31        
    ;
    v_y_e_predent_100                             =v_y_e_predent_100                             
        +coeff[ 35]*x11*x21    *x41    
        +coeff[ 36]            *x43*x51
        +coeff[ 37]    *x22*x31    *x51
        +coeff[ 38]        *x33*x42    
        +coeff[ 39]        *x34*x41    
        +coeff[ 40]            *x41*x53
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]        *x32*x45    
        +coeff[ 43]    *x24    *x41*x51
        ;

    return v_y_e_predent_100                             ;
}
float p_e_predent_100                             (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.6561837E-03;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.60213316E-03,-0.80130517E-01,-0.76910533E-01,-0.18103315E-01,
        -0.17160466E-01, 0.72091012E-02, 0.11330811E-01,-0.66505838E-02,
        -0.70906935E-04,-0.57339771E-02, 0.18417254E-02, 0.73879072E-03,
         0.71694196E-03, 0.89970126E-03,-0.22252507E-02,-0.12305923E-02,
        -0.13661011E-02,-0.54753484E-03, 0.51885825E-04,-0.10448198E-02,
        -0.63464598E-03,-0.13614711E-02, 0.41396709E-03, 0.43666904E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_predent_100                             =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_predent_100                             =v_p_e_predent_100                             
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x22*x31        
        +coeff[ 10]    *x21    *x41*x51
        +coeff[ 11]*x11    *x31        
        +coeff[ 12]*x11        *x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]        *x31*x42    
        +coeff[ 15]            *x43    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_predent_100                             =v_p_e_predent_100                             
        +coeff[ 17]            *x41*x52
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]    *x25*x31        
        +coeff[ 20]        *x33    *x52
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]*x11*x21    *x41    
        ;

    return v_p_e_predent_100                             ;
}
float l_e_predent_100                             (float *x,int m){
    int ncoeff= 90;
    float avdat= -0.4751895E-02;
    float xmin[10]={
        -0.39997E-02,-0.60027E-01,-0.59994E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60060E-01, 0.59982E-01, 0.30010E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 91]={
         0.47502089E-02,-0.10521796E-01,-0.19884866E-02,-0.57685999E-02,
        -0.59894458E-02, 0.82362356E-03,-0.41034110E-03, 0.46691403E-05,
        -0.14913712E-03,-0.14436221E-02, 0.13845587E-03,-0.41646927E-03,
         0.36640363E-03, 0.76126703E-03, 0.76294638E-03,-0.11393795E-02,
         0.33837247E-02,-0.26677316E-03, 0.55460405E-03,-0.51944127E-03,
        -0.30843557E-02,-0.19163386E-02, 0.46124225E-03,-0.17451495E-03,
        -0.26299604E-03, 0.26255695E-03,-0.11753306E-02, 0.43359160E-03,
        -0.12657395E-04, 0.72451984E-03, 0.96889366E-04, 0.20593927E-03,
         0.31705911E-03,-0.31788292E-03, 0.37661172E-03,-0.36084789E-03,
         0.85514609E-03, 0.90709866E-04,-0.65460399E-03, 0.67917386E-03,
        -0.18670537E-04,-0.22930207E-03,-0.20471390E-03,-0.18566073E-03,
         0.23620174E-03, 0.26996111E-03,-0.26655971E-03,-0.43909191E-03,
         0.15832733E-03, 0.31139574E-03, 0.67877304E-03,-0.23978896E-03,
         0.13497322E-02, 0.25790391E-03,-0.36290349E-03,-0.52326423E-03,
         0.11264131E-02,-0.41929141E-03,-0.46260501E-03,-0.76146261E-03,
         0.11430335E-02,-0.77683944E-04, 0.11121581E-02, 0.59303630E-03,
        -0.37074092E-03, 0.21819396E-03,-0.57121372E-03, 0.54360338E-03,
         0.47402811E-03, 0.46317372E-03, 0.42121546E-03, 0.10652175E-02,
        -0.49593556E-03, 0.56083355E-03,-0.46591493E-03,-0.17787471E-03,
         0.43157372E-03,-0.13667414E-02,-0.75820385E-03,-0.15786578E-03,
         0.38269736E-03,-0.12496775E-02, 0.87681058E-03, 0.60426479E-03,
        -0.18220835E-02,-0.23321600E-02,-0.73798897E-03, 0.79166860E-03,
         0.82000130E-03,-0.86347357E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_predent_100                             =avdat
        +coeff[  0]                    
        +coeff[  1]    *x22            
        +coeff[  2]        *x32        
        +coeff[  3]        *x31*x41    
        +coeff[  4]            *x42    
        +coeff[  5]*x11*x21            
        +coeff[  6]    *x22        *x51
        +coeff[  7]                *x51
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[  8]    *x21*x31        
        +coeff[  9]*x11*x21    *x41    
        +coeff[ 10]*x13                
        +coeff[ 11]*x11*x21*x32        
        +coeff[ 12]*x11    *x33        
        +coeff[ 13]        *x31*x41*x53
        +coeff[ 14]            *x42*x53
        +coeff[ 15]*x11*x24            
        +coeff[ 16]*x11*x21*x32*x41    
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 17]*x11            *x54
        +coeff[ 18]*x12    *x32    *x51
        +coeff[ 19]    *x22    *x43*x51
        +coeff[ 20]*x11*x21*x34*x41    
        +coeff[ 21]*x11    *x31*x44*x51
        +coeff[ 22]            *x41*x51
        +coeff[ 23]*x11    *x31        
        +coeff[ 24]*x11        *x41    
        +coeff[ 25]*x11            *x51
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 26]    *x21*x31*x41    
        +coeff[ 27]    *x21        *x52
        +coeff[ 28]                *x53
        +coeff[ 29]*x11*x22            
        +coeff[ 30]*x12        *x41    
        +coeff[ 31]        *x34        
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]        *x33*x41    
        +coeff[ 34]    *x22    *x42    
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 35]            *x44    
        +coeff[ 36]    *x22    *x41*x51
        +coeff[ 37]    *x21    *x42*x51
        +coeff[ 38]            *x43*x51
        +coeff[ 39]            *x42*x52
        +coeff[ 40]    *x21        *x53
        +coeff[ 41]        *x31    *x53
        +coeff[ 42]                *x54
        +coeff[ 43]*x11*x22*x31        
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 44]*x11    *x31    *x52
        +coeff[ 45]*x12*x21    *x41    
        +coeff[ 46]*x12*x21        *x51
        +coeff[ 47]    *x23*x32        
        +coeff[ 48]    *x21*x34        
        +coeff[ 49]*x13        *x41    
        +coeff[ 50]    *x21*x33*x41    
        +coeff[ 51]    *x21*x32*x42    
        +coeff[ 52]    *x21*x31*x43    
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 53]    *x21    *x44    
        +coeff[ 54]*x13            *x51
        +coeff[ 55]    *x23*x31    *x51
        +coeff[ 56]    *x22*x32    *x51
        +coeff[ 57]    *x21*x33    *x51
        +coeff[ 58]        *x34    *x51
        +coeff[ 59]    *x23    *x41*x51
        +coeff[ 60]    *x22*x31*x41*x51
        +coeff[ 61]        *x33*x41*x51
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 62]        *x32*x42*x51
        +coeff[ 63]        *x31*x43*x51
        +coeff[ 64]    *x23        *x52
        +coeff[ 65]    *x22*x31    *x52
        +coeff[ 66]    *x22        *x53
        +coeff[ 67]    *x21*x31    *x53
        +coeff[ 68]    *x21    *x41*x53
        +coeff[ 69]*x11*x23    *x41    
        +coeff[ 70]*x11*x21    *x43    
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 71]*x11    *x31*x42*x51
        +coeff[ 72]*x11    *x31    *x53
        +coeff[ 73]*x12*x21*x32        
        +coeff[ 74]*x12*x21        *x52
        +coeff[ 75]    *x23*x33        
        +coeff[ 76]*x13*x21    *x41    
        +coeff[ 77]    *x23*x32*x41    
        +coeff[ 78]    *x23*x31*x42    
        +coeff[ 79]*x13*x21        *x51
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 80]*x13    *x31    *x51
        +coeff[ 81]    *x24    *x41*x51
        +coeff[ 82]        *x34*x41*x51
        +coeff[ 83]    *x23    *x42*x51
        +coeff[ 84]        *x33*x42*x51
        +coeff[ 85]        *x32*x43*x51
        +coeff[ 86]        *x31*x44*x51
        +coeff[ 87]    *x22*x31*x41*x52
        +coeff[ 88]        *x33    *x53
    ;
    v_l_e_predent_100                             =v_l_e_predent_100                             
        +coeff[ 89]        *x32*x41*x53
        ;

    return v_l_e_predent_100                             ;
}
