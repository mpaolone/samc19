float x_e_dq_1200                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5062243E+00;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.47635594E-02,-0.94394637E-02, 0.95895015E-01, 0.19090500E+00,
         0.24722338E-01, 0.14727225E-01,-0.32772031E-02,-0.32483265E-02,
        -0.77603110E-02,-0.79451241E-02,-0.10149061E-02,-0.17769806E-02,
         0.18097106E-02,-0.37343216E-02,-0.80406270E-03,-0.56185026E-03,
        -0.37792241E-03, 0.14674342E-03,-0.92890579E-03,-0.33628871E-03,
        -0.55019488E-02,-0.34082984E-02,-0.29853464E-02,-0.42502382E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_1200                                  =v_x_e_dq_1200                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_1200                                  =v_x_e_dq_1200                                  
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_1200                                  ;
}
float t_e_dq_1200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5719933E+00;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.44632093E-02, 0.35736938E-02,-0.10170781E+00, 0.24308912E-01,
        -0.75415177E-02,-0.23325596E-02,-0.10489911E-02,-0.94705628E-03,
         0.30017735E-02, 0.48279422E-03, 0.10864973E-02, 0.84229099E-03,
         0.29985956E-03, 0.46098896E-03, 0.87312328E-04,-0.15884082E-03,
         0.80965180E-03, 0.48306058E-03, 0.84680459E-03,-0.50605304E-03,
         0.21373623E-02, 0.12301415E-03, 0.37156092E-03, 0.99810124E-04,
        -0.12410284E-03, 0.38513768E-03, 0.33537977E-03, 0.12445188E-03,
        -0.50621608E-03,-0.51297439E-03,-0.15778425E-03, 0.24548844E-02,
         0.90133894E-04,-0.50604871E-04,-0.10384334E-03, 0.11295391E-02,
         0.13286405E-02, 0.42393926E-03,-0.39053854E-03,-0.22669534E-03,
         0.21748578E-04, 0.43966364E-04, 0.45365250E-04, 0.62450410E-04,
         0.10107577E-03, 0.10143569E-03, 0.61576618E-04, 0.58688132E-04,
         0.62277781E-04, 0.13511178E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]                *x52
    ;
    v_t_e_dq_1200                                  =v_t_e_dq_1200                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_1200                                  =v_t_e_dq_1200                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x22        *x51
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_dq_1200                                  =v_t_e_dq_1200                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x22        *x52
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]    *x21        *x53
    ;
    v_t_e_dq_1200                                  =v_t_e_dq_1200                                  
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x21*x31*x43    
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]                *x53
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_dq_1200                                  =v_t_e_dq_1200                                  
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]        *x32*x42    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]        *x32    *x52
        +coeff[ 48]            *x42*x52
        +coeff[ 49]*x11*x22*x31*x41    
        ;

    return v_t_e_dq_1200                                  ;
}
float y_e_dq_1200                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1245892E-03;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.68479130E-04, 0.11562818E+00,-0.77543236E-01,-0.66206768E-01,
        -0.22667276E-01, 0.47853012E-01, 0.25551077E-01,-0.31566866E-01,
        -0.13010345E-01,-0.71563832E-02, 0.24443190E-02,-0.96362848E-02,
        -0.22613124E-02,-0.30963351E-02,-0.19188030E-03,-0.55304816E-03,
         0.87540312E-03,-0.55867881E-02,-0.11744819E-02, 0.16514534E-02,
        -0.17546871E-02, 0.55283275E-02,-0.55851969E-02, 0.50920271E-03,
         0.17777977E-02,-0.20546347E-02,-0.11924220E-02, 0.21561554E-02,
         0.26628620E-02, 0.16753623E-02,-0.40458096E-02,-0.50475160E-02,
        -0.33335336E-02, 0.12525397E-02,-0.31806677E-03, 0.10795341E-02,
         0.32142902E-03,-0.37078315E-03, 0.67179598E-03, 0.32533656E-03,
        -0.17857839E-02,-0.15295113E-02, 0.10241081E-03, 0.33595126E-04,
        -0.26396910E-04,-0.33927895E-03, 0.17479953E-03,-0.81970630E-03,
         0.48195946E-03, 0.19094388E-04,-0.45363919E-04,-0.46734269E-04,
         0.51299954E-03,-0.68716588E-04,-0.36065836E-03,-0.14290272E-03,
         0.13076785E-03, 0.67528291E-03, 0.12251032E-02, 0.45476691E-03,
        -0.53469848E-03,-0.85992888E-02, 0.46578731E-04,-0.86739874E-02,
         0.69825168E-04, 0.28024076E-02, 0.41072490E-02, 0.48203035E-02,
         0.29935027E-02, 0.13787433E-02,-0.20365010E-02,-0.18234410E-04,
        -0.24139053E-04, 0.24365672E-04,-0.35536170E-04, 0.30738892E-03,
         0.49267484E-04, 0.25734393E-03, 0.18288143E-02,-0.45212600E-03,
         0.18558715E-02,-0.65362474E-04, 0.30623714E-03, 0.10174752E-02,
        -0.45571700E-03, 0.18102644E-03,-0.50866994E-03, 0.31612132E-03,
        -0.18107764E-02,-0.15301238E-03,-0.26013465E-02,-0.43714228E-02,
         0.17363518E-03,-0.13321865E-02,-0.11055365E-02, 0.98753335E-05,
        -0.12524727E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x23    *x41*x51
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 44]                *x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]*x11        *x41*x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 53]*x11*x21    *x41*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 62]*x11*x21    *x45    
        +coeff[ 63]    *x22*x32*x43    
        +coeff[ 64]*x11*x21*x32*x43    
        +coeff[ 65]    *x23    *x45    
        +coeff[ 66]    *x23*x31*x44    
        +coeff[ 67]    *x23*x32*x43    
        +coeff[ 68]    *x23*x33*x42    
        +coeff[ 69]    *x23*x34*x41    
        +coeff[ 70]    *x24    *x45    
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x11    *x31*x42    
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]*x11*x21    *x43    
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]*x11*x21*x32*x41    
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]    *x23    *x43    
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 80]    *x21*x32*x43    
        +coeff[ 81]*x11*x22    *x41*x51
        +coeff[ 82]        *x31*x44*x51
        +coeff[ 83]    *x21*x33*x42    
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]        *x32*x43*x51
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x22    *x45    
    ;
    v_y_e_dq_1200                                  =v_y_e_dq_1200                                  
        +coeff[ 89]    *x24*x31    *x51
        +coeff[ 90]    *x24*x31*x42    
        +coeff[ 91]    *x22*x33*x42    
        +coeff[ 92]*x11*x23    *x43    
        +coeff[ 93]    *x24*x32*x41    
        +coeff[ 94]    *x22*x34*x41    
        +coeff[ 95]        *x31*x41    
        +coeff[ 96]    *x21*x31*x41    
        ;

    return v_y_e_dq_1200                                  ;
}
float p_e_dq_1200                                  (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.4175218E-04;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.39004048E-04,-0.24322543E-01, 0.12205947E-01,-0.53269397E-02,
        -0.14917192E-01, 0.47350610E-02, 0.11727101E-01,-0.86626662E-02,
        -0.26257208E-02,-0.54544216E-03,-0.25136259E-02, 0.51303173E-03,
        -0.21455791E-02,-0.13485672E-02,-0.38054431E-03, 0.24052402E-02,
        -0.10098589E-02,-0.94706856E-03, 0.50621373E-04, 0.25005775E-03,
        -0.29391487E-03,-0.13305264E-02, 0.54041512E-03,-0.91441470E-03,
         0.87461877E-03, 0.12362289E-03, 0.53915952E-03,-0.25129330E-03,
        -0.17554143E-02,-0.10244955E-02,-0.59937965E-05,-0.26203220E-03,
         0.29852730E-03, 0.16388869E-03,-0.18596482E-03, 0.10788499E-03,
        -0.40399982E-03,-0.50410005E-03,-0.17669627E-03, 0.45957716E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_1200                                  =v_p_e_dq_1200                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_1200                                  =v_p_e_dq_1200                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_1200                                  =v_p_e_dq_1200                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]        *x31    *x52
        +coeff[ 31]            *x41*x52
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dq_1200                                  =v_p_e_dq_1200                                  
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x23*x31    *x51
        +coeff[ 37]    *x25    *x41    
        +coeff[ 38]    *x22*x31    *x52
        +coeff[ 39]    *x22    *x41*x52
        ;

    return v_p_e_dq_1200                                  ;
}
float l_e_dq_1200                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1830476E-01;
    float xmin[10]={
        -0.39990E-02,-0.60008E-01,-0.53981E-01,-0.30051E-01,-0.39991E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60060E-01, 0.53976E-01, 0.30019E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.18088365E-01,-0.31138590E+00,-0.25444303E-01, 0.12401858E-01,
        -0.28727336E-01,-0.16836107E-01,-0.57356083E-02,-0.54990374E-02,
         0.19579844E-02, 0.93519259E-02, 0.85392613E-02,-0.99286146E-03,
        -0.17610072E-03, 0.15217514E-02,-0.69156400E-03, 0.33231827E-02,
        -0.19645079E-02,-0.23716269E-02, 0.21626221E-02, 0.42434768E-02,
         0.47748533E-03, 0.37199270E-03, 0.55290567E-03, 0.45353279E-03,
         0.40188569E-03,-0.30542995E-03, 0.16204042E-02,-0.71778521E-03,
         0.58638020E-02,-0.15787375E-02, 0.18878533E-02, 0.14662715E-02,
         0.40060122E-03,-0.84574701E-03, 0.84123644E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_1200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_1200                                  =v_l_e_dq_1200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dq_1200                                  =v_l_e_dq_1200                                  
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]            *x42*x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11    *x31*x41    
        +coeff[ 25]*x11*x21        *x51
    ;
    v_l_e_dq_1200                                  =v_l_e_dq_1200                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]        *x31*x41*x53
        +coeff[ 30]    *x24*x32        
        +coeff[ 31]    *x24    *x42    
        +coeff[ 32]*x13            *x52
        +coeff[ 33]    *x23        *x53
        +coeff[ 34]*x11*x23*x32        
        ;

    return v_l_e_dq_1200                                  ;
}
float x_e_dq_1100                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5069471E+00;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.54199588E-02,-0.94332220E-02, 0.95902197E-01, 0.19106917E+00,
         0.24740933E-01, 0.14750138E-01,-0.32679471E-02,-0.32283056E-02,
        -0.76717385E-02,-0.80047576E-02,-0.98937622E-03,-0.17430806E-02,
         0.18356538E-02,-0.37235445E-02,-0.78987057E-03,-0.59735926E-03,
        -0.39470498E-03, 0.11244715E-03,-0.88084256E-03,-0.31576082E-03,
        -0.55488441E-02,-0.33483601E-02,-0.31899863E-02,-0.43998091E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_1100                                  =v_x_e_dq_1100                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_1100                                  =v_x_e_dq_1100                                  
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_1100                                  ;
}
float t_e_dq_1100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5721644E+00;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.43787276E-02, 0.35741506E-02,-0.10175164E+00, 0.24331007E-01,
        -0.75974599E-02,-0.23356848E-02,-0.10010806E-02,-0.95421122E-03,
         0.23881465E-02, 0.47716021E-03, 0.10257795E-02, 0.10177001E-02,
         0.32469197E-03, 0.43303519E-03, 0.96337557E-04,-0.14687984E-03,
         0.68674772E-03, 0.46127682E-03, 0.86658850E-03,-0.43688773E-03,
         0.21801544E-02, 0.33677736E-03, 0.29898112E-04,-0.18557785E-03,
         0.37447270E-03, 0.24068197E-03, 0.12121873E-03,-0.66820788E-03,
        -0.39226803E-03, 0.28184280E-02, 0.11634931E-03, 0.88888606E-04,
        -0.37001610E-04,-0.15838278E-03, 0.21141488E-02, 0.19349467E-02,
         0.54023601E-03,-0.35327065E-03,-0.26229367E-03, 0.58213402E-04,
         0.62764360E-04, 0.47675967E-04, 0.66545610E-04, 0.61903607E-04,
        -0.63244770E-04, 0.91383071E-03, 0.24206846E-03, 0.31148957E-03,
        -0.28494044E-03,-0.11957351E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]                *x52
    ;
    v_t_e_dq_1100                                  =v_t_e_dq_1100                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_1100                                  =v_t_e_dq_1100                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x23            
        +coeff[ 22]    *x22        *x51
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_t_e_dq_1100                                  =v_t_e_dq_1100                                  
        +coeff[ 26]    *x21*x32    *x51
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]*x11*x22            
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_dq_1100                                  =v_t_e_dq_1100                                  
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x11    *x31*x41    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]                *x53
        +coeff[ 42]        *x32    *x52
        +coeff[ 43]            *x42*x52
    ;
    v_t_e_dq_1100                                  =v_t_e_dq_1100                                  
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]    *x21*x33*x41    
        +coeff[ 46]    *x22*x32    *x51
        +coeff[ 47]    *x23*x31*x41*x51
        +coeff[ 48]    *x23    *x42*x51
        +coeff[ 49]*x12                
        ;

    return v_t_e_dq_1100                                  ;
}
float y_e_dq_1100                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1038165E-02;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.90610824E-03, 0.11525556E+00,-0.77759869E-01,-0.65837026E-01,
        -0.22610186E-01, 0.47821075E-01, 0.25568387E-01,-0.31685486E-01,
        -0.13059204E-01,-0.71525797E-02, 0.24263458E-02,-0.97157592E-02,
        -0.22676769E-02,-0.30961426E-02,-0.25007551E-03,-0.41180357E-03,
         0.86657354E-03,-0.57365778E-02, 0.16233703E-02,-0.17632562E-02,
         0.50155520E-02,-0.55537093E-02,-0.11880905E-02, 0.51680778E-03,
         0.17168480E-02,-0.20211092E-02,-0.11784491E-02, 0.64395636E-03,
         0.15019102E-02, 0.65920182E-03,-0.42126304E-02,-0.47870963E-02,
        -0.27394108E-02, 0.12471449E-02,-0.35682926E-03, 0.61494451E-04,
        -0.47585265E-04, 0.11780523E-02, 0.27594730E-03,-0.32408306E-03,
         0.70847804E-03, 0.34918357E-03,-0.16381663E-02,-0.15140135E-02,
        -0.87681561E-04,-0.28913314E-03, 0.18241195E-03,-0.76294813E-03,
        -0.11442888E-03, 0.47385969E-03, 0.35193476E-04,-0.55925982E-04,
         0.65724074E-03,-0.19077848E-03, 0.48137153E-03, 0.14075506E-03,
         0.61931118E-03, 0.24873848E-03, 0.36466043E-02, 0.23257644E-02,
        -0.56533300E-03,-0.84100273E-02,-0.90920134E-02, 0.80612364E-04,
        -0.21167987E-02, 0.16891050E-04, 0.85805041E-05,-0.42136715E-04,
        -0.25112031E-04,-0.27419852E-04, 0.20614734E-04, 0.33397599E-04,
         0.36095062E-04, 0.24738349E-05, 0.15021258E-02, 0.49036444E-04,
         0.37677763E-02, 0.21396973E-02, 0.45174439E-02, 0.25288090E-02,
         0.58880203E-04,-0.44983361E-03, 0.68399130E-03,-0.47427526E-03,
         0.38253461E-03,-0.14821189E-02,-0.17997832E-03,-0.30150255E-02,
        -0.47783493E-02, 0.98123906E-04,-0.18920120E-02,-0.13576474E-02,
        -0.49136896E-04,-0.24155859E-05, 0.92332621E-05, 0.15599253E-04,
        -0.25467449E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x23    *x41*x51
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 35]    *x21            
        +coeff[ 36]                *x51
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x24    *x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 44]            *x45*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]*x11*x22    *x41*x51
        +coeff[ 49]    *x22    *x41*x52
        +coeff[ 50]    *x22            
        +coeff[ 51]*x12        *x41    
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x23    *x45    
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]        *x31*x41    
        +coeff[ 66]    *x21        *x51
        +coeff[ 67]*x11        *x41*x51
        +coeff[ 68]*x12    *x31        
        +coeff[ 69]*x11    *x31    *x51
        +coeff[ 70]*x11        *x42*x51
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 71]            *x42*x52
        +coeff[ 72]        *x31*x41*x52
        +coeff[ 73]        *x34*x41    
        +coeff[ 74]    *x21    *x45    
        +coeff[ 75]        *x31    *x53
        +coeff[ 76]    *x21*x31*x44    
        +coeff[ 77]    *x23    *x43    
        +coeff[ 78]    *x21*x32*x43    
        +coeff[ 79]    *x21*x33*x42    
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 80]    *x22*x31    *x52
        +coeff[ 81]    *x22    *x43*x51
        +coeff[ 82]    *x21*x34*x41    
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x23*x33        
        +coeff[ 85]    *x22    *x45    
        +coeff[ 86]    *x24*x31    *x51
        +coeff[ 87]    *x24*x31*x42    
        +coeff[ 88]    *x22*x33*x42    
    ;
    v_y_e_dq_1100                                  =v_y_e_dq_1100                                  
        +coeff[ 89]*x11    *x31*x42*x52
        +coeff[ 90]    *x24*x32*x41    
        +coeff[ 91]    *x22*x34*x41    
        +coeff[ 92]*x13*x21    *x41*x51
        +coeff[ 93]*x11                
        +coeff[ 94]        *x32        
        +coeff[ 95]    *x22    *x42    
        +coeff[ 96]*x11*x22*x31        
        ;

    return v_y_e_dq_1100                                  ;
}
float p_e_dq_1100                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2364257E-03;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.22412695E-03,-0.24369026E-01, 0.12121672E-01,-0.53275093E-02,
        -0.14797761E-01, 0.47681215E-02, 0.11715446E-01,-0.86327503E-02,
        -0.26385246E-02,-0.51755615E-03,-0.25316440E-02, 0.51130657E-03,
        -0.21648796E-02,-0.13385704E-02,-0.39085309E-03, 0.18769373E-02,
        -0.95585670E-03, 0.71862327E-04, 0.24780058E-03,-0.29861744E-03,
        -0.13364056E-02, 0.53175946E-03,-0.10227953E-02,-0.93580870E-03,
         0.89746097E-03, 0.11886230E-03,-0.24597172E-03,-0.25449175E-03,
         0.56473573E-03, 0.30038969E-03,-0.18116963E-03,-0.50888906E-04,
         0.10911884E-03,-0.17746673E-02,-0.10248161E-02,-0.37199890E-03,
        -0.18063444E-03, 0.46297046E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_1100                                  =v_p_e_dq_1100                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_dq_1100                                  =v_p_e_dq_1100                                  
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_1100                                  =v_p_e_dq_1100                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x24    *x41    
        +coeff[ 32]    *x21    *x41*x52
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dq_1100                                  =v_p_e_dq_1100                                  
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_1100                                  ;
}
float l_e_dq_1100                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1854046E-01;
    float xmin[10]={
        -0.39999E-02,-0.60047E-01,-0.53982E-01,-0.30052E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39993E-02, 0.60048E-01, 0.53997E-01, 0.29989E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.18534042E-01,-0.31119466E+00,-0.25440425E-01, 0.12201249E-01,
        -0.29242478E-01,-0.16596789E-01,-0.52781473E-02,-0.52364785E-02,
         0.21154834E-02, 0.92020761E-02, 0.81707807E-02,-0.10896740E-03,
         0.99758900E-05, 0.29501878E-02,-0.93663245E-03, 0.25274677E-02,
        -0.20114209E-02,-0.24063226E-02, 0.15307904E-02, 0.68483828E-03,
         0.95250091E-03,-0.18600440E-03,-0.79612219E-03, 0.38747958E-03,
         0.91586704E-03,-0.60742104E-03,-0.11864551E-02,-0.68363850E-03,
         0.78873429E-02, 0.58493181E-02, 0.11627666E-02, 0.71110000E-03,
         0.64174575E-03,-0.98604965E-03,-0.54959522E-03,-0.10220843E-02,
         0.68825652E-03, 0.33851571E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_1100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_1100                                  =v_l_e_dq_1100                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dq_1100                                  =v_l_e_dq_1100                                  
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]*x12                
        +coeff[ 22]    *x23            
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_dq_1100                                  =v_l_e_dq_1100                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]            *x42*x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x22*x32    *x51
        +coeff[ 31]        *x31*x42*x52
        +coeff[ 32]*x13    *x32        
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]*x11*x24*x31        
    ;
    v_l_e_dq_1100                                  =v_l_e_dq_1100                                  
        +coeff[ 35]*x11*x23*x32        
        +coeff[ 36]*x11    *x34    *x51
        +coeff[ 37]    *x21*x32*x42*x52
        ;

    return v_l_e_dq_1100                                  ;
}
float x_e_dq_1000                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5060959E+00;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.44117160E-02,-0.94370292E-02, 0.95902584E-01, 0.19120347E+00,
         0.24712285E-01, 0.14745802E-01,-0.32722531E-02,-0.32095469E-02,
        -0.77842637E-02,-0.79957228E-02,-0.99748361E-03,-0.17809354E-02,
         0.17982459E-02,-0.37504025E-02,-0.79135940E-03,-0.56255684E-03,
        -0.23022514E-03,-0.37823795E-03, 0.44403641E-05,-0.77621435E-03,
        -0.53428314E-02,-0.33276218E-02,-0.30354024E-02,-0.40090326E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_1000                                  =v_x_e_dq_1000                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_1000                                  =v_x_e_dq_1000                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_1000                                  ;
}
float t_e_dq_1000                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5720530E+00;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.45060436E-02, 0.35739816E-02,-0.10175062E+00, 0.24325473E-01,
        -0.75387927E-02,-0.23265660E-02,-0.94884116E-03,-0.99929678E-03,
         0.24395590E-02, 0.99396030E-03, 0.10143497E-02, 0.33735225E-03,
         0.46801544E-03, 0.44689793E-03, 0.85597938E-04,-0.14761098E-03,
         0.70517097E-03, 0.47287211E-03, 0.83226938E-03,-0.50842215E-03,
         0.11153920E-03, 0.22693775E-02, 0.11923129E-03, 0.31411371E-03,
         0.48998856E-04,-0.17280491E-03, 0.37124666E-03,-0.52237255E-03,
        -0.56844094E-03,-0.16788827E-03, 0.26716979E-02, 0.19771857E-02,
         0.47714441E-03,-0.39477934E-04, 0.27582044E-03, 0.81404767E-04,
         0.20100756E-02,-0.37330730E-03,-0.23732671E-03, 0.86526445E-04,
         0.45487304E-04, 0.43517684E-04,-0.37351645E-04, 0.66840614E-04,
         0.76984688E-04,-0.83317667E-04, 0.24158126E-04, 0.12505896E-03,
         0.87027706E-03, 0.17815747E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_1000                                  =v_t_e_dq_1000                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_1000                                  =v_t_e_dq_1000                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_1000                                  =v_t_e_dq_1000                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x22    *x42*x51
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_dq_1000                                  =v_t_e_dq_1000                                  
        +coeff[ 35]        *x32*x42    
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]        *x31*x41*x51
        +coeff[ 41]                *x53
        +coeff[ 42]*x11*x21    *x42    
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_dq_1000                                  =v_t_e_dq_1000                                  
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x13    *x31*x41    
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_dq_1000                                  ;
}
float y_e_dq_1000                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.5862499E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.54635061E-03, 0.11476212E+00,-0.78048594E-01,-0.66124886E-01,
        -0.22597853E-01, 0.47892664E-01, 0.25584323E-01,-0.31624846E-01,
        -0.12959152E-01,-0.71482658E-02, 0.24343131E-02,-0.94855744E-02,
        -0.22668589E-02,-0.30866596E-02,-0.14597228E-03,-0.60203805E-03,
         0.87688467E-03,-0.55285268E-02,-0.11619360E-02, 0.16101281E-02,
        -0.17491793E-02, 0.54432070E-02,-0.56149513E-02, 0.56112470E-03,
         0.17208958E-02,-0.23066953E-02,-0.11879596E-02, 0.21852350E-02,
         0.20585181E-02, 0.16640780E-02,-0.39144810E-02,-0.54230331E-02,
        -0.32891233E-02,-0.25612678E-03, 0.12112543E-02, 0.32746143E-03,
        -0.34710712E-03, 0.73103106E-03, 0.33184534E-03,-0.16549142E-02,
        -0.15738667E-02, 0.12711970E-02, 0.82338112E-04, 0.21562118E-04,
        -0.51071900E-04,-0.48585801E-03, 0.16842149E-03,-0.87315933E-03,
         0.45797281E-03,-0.14224158E-04,-0.55104414E-04, 0.48772679E-03,
        -0.59185291E-04,-0.48607416E-03, 0.49276720E-03,-0.21112348E-03,
         0.10741804E-03, 0.64669835E-03, 0.27509916E-03, 0.21640141E-03,
         0.34100192E-02, 0.16779684E-02,-0.24264966E-03,-0.82952362E-02,
        -0.85750595E-02,-0.22404375E-02, 0.12389160E-04,-0.22260496E-04,
        -0.29119412E-04,-0.36620324E-04, 0.30264864E-04, 0.56535773E-04,
         0.28273603E-02,-0.83028026E-04, 0.20918066E-02,-0.87038061E-04,
         0.16986377E-02, 0.11386505E-03,-0.58851356E-03, 0.32186991E-03,
        -0.18016993E-02,-0.15796789E-03,-0.24515116E-02,-0.40356857E-02,
        -0.15473203E-03,-0.16109425E-02,-0.91368845E-03, 0.18880514E-02,
         0.86698873E-03, 0.75032195E-03,-0.74697868E-03, 0.15540008E-04,
         0.30806461E-05,-0.10012460E-04, 0.11312929E-04,-0.10946336E-04,
        -0.25071244E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 44]*x11        *x41*x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]                *x51
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 62]    *x24    *x41*x51
        +coeff[ 63]    *x22*x31*x44    
        +coeff[ 64]    *x22*x32*x43    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]    *x22            
        +coeff[ 67]*x12    *x31        
        +coeff[ 68]*x11    *x31    *x51
        +coeff[ 69]*x11*x22*x31        
        +coeff[ 70]*x12*x21    *x41    
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 71]        *x31    *x53
        +coeff[ 72]    *x21*x31*x44    
        +coeff[ 73]*x11*x23*x31        
        +coeff[ 74]    *x21*x32*x43    
        +coeff[ 75]*x11*x22    *x41*x51
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]    *x22    *x43*x51
        +coeff[ 78]    *x22*x31*x42*x51
        +coeff[ 79]    *x23*x33        
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 80]    *x22    *x45    
        +coeff[ 81]    *x24*x31    *x51
        +coeff[ 82]    *x24*x31*x42    
        +coeff[ 83]    *x22*x33*x42    
        +coeff[ 84]    *x21*x32*x43*x51
        +coeff[ 85]    *x24*x32*x41    
        +coeff[ 86]    *x22*x34*x41    
        +coeff[ 87]    *x23    *x45    
        +coeff[ 88]    *x23*x32*x43    
    ;
    v_y_e_dq_1000                                  =v_y_e_dq_1000                                  
        +coeff[ 89]    *x21*x34*x43    
        +coeff[ 90]    *x24    *x43*x51
        +coeff[ 91]            *x42    
        +coeff[ 92]        *x31*x41    
        +coeff[ 93]    *x21    *x42    
        +coeff[ 94]*x11    *x31*x41    
        +coeff[ 95]        *x31*x41*x51
        +coeff[ 96]        *x32*x42    
        ;

    return v_y_e_dq_1000                                  ;
}
float p_e_dq_1000                                  (float *x,int m){
    int ncoeff= 40;
    float avdat= -0.1759192E-03;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
         0.17299739E-03,-0.24424631E-01, 0.12013509E-01,-0.53234822E-02,
        -0.14799043E-01, 0.47674314E-02, 0.11716568E-01,-0.85871127E-02,
        -0.26236686E-02,-0.52940665E-03,-0.25270397E-02, 0.51352481E-03,
        -0.21636970E-02,-0.13284563E-02,-0.38043747E-03, 0.53425628E-03,
         0.18900646E-02,-0.18676061E-05,-0.96939359E-03, 0.33595999E-04,
         0.24920286E-03,-0.29822890E-03,-0.12985157E-02,-0.10270155E-02,
        -0.94979163E-03, 0.86604699E-03, 0.11806226E-03,-0.25701488E-03,
        -0.40260659E-03,-0.12175357E-04,-0.25091137E-03, 0.54437725E-03,
         0.29344999E-03,-0.18836324E-03,-0.72200222E-04, 0.10806604E-03,
        -0.17422698E-02,-0.10374906E-02,-0.16092924E-03, 0.45808259E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_1000                                  =v_p_e_dq_1000                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]*x11*x21    *x41    
        +coeff[ 16]    *x23    *x41    
    ;
    v_p_e_dq_1000                                  =v_p_e_dq_1000                                  
        +coeff[ 17]*x11    *x33        
        +coeff[ 18]    *x22*x32*x41    
        +coeff[ 19]        *x34*x41    
        +coeff[ 20]*x11    *x31        
        +coeff[ 21]        *x33        
        +coeff[ 22]        *x32*x41    
        +coeff[ 23]    *x22*x31    *x51
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x23    *x41*x51
    ;
    v_p_e_dq_1000                                  =v_p_e_dq_1000                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]    *x23*x31    *x51
        +coeff[ 29]        *x31    *x52
        +coeff[ 30]            *x41*x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x24    *x41    
    ;
    v_p_e_dq_1000                                  =v_p_e_dq_1000                                  
        +coeff[ 35]    *x21    *x41*x52
        +coeff[ 36]    *x22*x31*x42    
        +coeff[ 37]    *x22    *x43    
        +coeff[ 38]    *x22*x31    *x52
        +coeff[ 39]    *x22    *x41*x52
        ;

    return v_p_e_dq_1000                                  ;
}
float l_e_dq_1000                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.1807922E-01;
    float xmin[10]={
        -0.39998E-02,-0.60012E-01,-0.53989E-01,-0.30022E-01,-0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60030E-01, 0.53992E-01, 0.30002E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.17898757E-01,-0.31125006E+00,-0.25303744E-01, 0.12478909E-01,
        -0.28364081E-01,-0.16498178E-01,-0.54779123E-02,-0.53719631E-02,
         0.20652481E-02, 0.90448167E-02, 0.75764451E-02,-0.24964197E-02,
        -0.50261820E-03,-0.59991743E-03, 0.28526168E-02,-0.82874106E-03,
         0.29975676E-02,-0.19571462E-02,-0.98960986E-03, 0.13424429E-02,
         0.26987420E-03, 0.73449481E-02, 0.92567607E-04, 0.66825404E-03,
         0.53150812E-03,-0.81651297E-03,-0.32229247E-03, 0.81796553E-02,
         0.97712094E-03, 0.72932628E-03, 0.11810452E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dq_1000                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_1000                                  =v_l_e_dq_1000                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]    *x22*x32        
        +coeff[ 13]        *x34        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_1000                                  =v_l_e_dq_1000                                  
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23*x33*x41    
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x23        *x51
    ;
    v_l_e_dq_1000                                  =v_l_e_dq_1000                                  
        +coeff[ 26]*x12            *x52
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x22*x32    *x51
        +coeff[ 29]        *x34*x41*x51
        +coeff[ 30]    *x23        *x54
        ;

    return v_l_e_dq_1000                                  ;
}
float x_e_dq_900                                  (float *x,int m){
    int ncoeff= 25;
    float avdat=  0.5083773E+00;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 26]={
        -0.66645960E-02,-0.94410479E-02, 0.95888518E-01, 0.19134329E+00,
         0.24732897E-01, 0.14786568E-01,-0.32848075E-02,-0.31963666E-02,
        -0.78398408E-02,-0.79360781E-02,-0.99733146E-03,-0.17743030E-02,
         0.18016845E-02,-0.36763039E-02,-0.78980072E-03,-0.57968398E-03,
        -0.35499848E-03,-0.10292939E-04,-0.86878403E-03,-0.52817632E-02,
        -0.32737968E-02,-0.27777250E-02,-0.20011750E-02,-0.97278180E-03,
        -0.36759488E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_900                                  =v_x_e_dq_900                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_900                                  =v_x_e_dq_900                                  
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x21*x32*x42*x52
        +coeff[ 23]    *x21*x31*x43*x52
        +coeff[ 24]    *x23*x32*x42    
        ;

    return v_x_e_dq_900                                  ;
}
float t_e_dq_900                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5718501E+00;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47620293E-02, 0.35741399E-02,-0.10185632E+00, 0.24321444E-01,
        -0.75382967E-02,-0.23364180E-02,-0.91181789E-03,-0.10253068E-02,
         0.24438235E-02, 0.10045202E-02, 0.10032048E-02, 0.29450905E-03,
         0.49539714E-03, 0.42972740E-03, 0.11793936E-03,-0.16233194E-03,
         0.68373868E-03, 0.49754005E-03,-0.53806562E-03, 0.21998317E-02,
         0.34355279E-03, 0.11741724E-03,-0.17921731E-03, 0.81645907E-03,
         0.10674633E-03,-0.53283077E-03,-0.55140577E-03,-0.17319211E-03,
         0.26834242E-02,-0.43430494E-03, 0.12096059E-03, 0.37098888E-03,
         0.26190159E-03, 0.21954842E-02, 0.19826076E-02, 0.49765431E-03,
        -0.28858503E-03, 0.78454119E-04,-0.38841823E-04, 0.50544459E-04,
         0.66113789E-04, 0.68343958E-04, 0.43477699E-04, 0.51197152E-04,
        -0.68664151E-04, 0.31701686E-04, 0.12142872E-03, 0.91278012E-03,
         0.14291063E-03,-0.11771784E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_900                                  =v_t_e_dq_900                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_900                                  =v_t_e_dq_900                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x23        *x51
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x23            
        +coeff[ 21]    *x22        *x51
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]    *x22    *x42    
        +coeff[ 24]    *x21*x32    *x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_t_e_dq_900                                  =v_t_e_dq_900                                  
        +coeff[ 26]    *x21    *x42*x51
        +coeff[ 27]    *x22        *x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23        *x52
        +coeff[ 30]*x11*x22            
        +coeff[ 31]    *x22*x32        
        +coeff[ 32]    *x22*x31*x41    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_dq_900                                  =v_t_e_dq_900                                  
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]    *x21*x31*x41*x52
        +coeff[ 37]*x11        *x42    
        +coeff[ 38]*x11*x21        *x51
        +coeff[ 39]        *x31*x41*x51
        +coeff[ 40]                *x53
        +coeff[ 41]*x11*x23            
        +coeff[ 42]        *x33*x41    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_dq_900                                  =v_t_e_dq_900                                  
        +coeff[ 44]    *x21        *x53
        +coeff[ 45]*x13    *x31*x41    
        +coeff[ 46]*x11*x22*x31*x41    
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_dq_900                                  ;
}
float y_e_dq_900                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1546756E-03;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.70209506E-04, 0.11417195E+00,-0.78378245E-01,-0.65715663E-01,
        -0.22539772E-01, 0.47862813E-01, 0.25602913E-01,-0.31161359E-01,
        -0.12724971E-01,-0.71499869E-02, 0.24174787E-02,-0.89072911E-02,
        -0.22619106E-02,-0.30838135E-02,-0.46150893E-03,-0.11608325E-02,
         0.87240950E-03,-0.54034791E-02, 0.16323599E-02,-0.17704258E-02,
         0.49908874E-02,-0.52731889E-02,-0.10988907E-02, 0.55655203E-03,
         0.16986396E-02,-0.22952608E-02,-0.13190894E-02, 0.44146526E-03,
         0.12349125E-02, 0.73531928E-03,-0.63332519E-02,-0.95128128E-02,
        -0.55771731E-02, 0.10998390E-02, 0.26401831E-03,-0.34584024E-03,
         0.71469758E-03, 0.32895533E-03,-0.20877724E-02,-0.17874229E-02,
        -0.12506837E-02, 0.12624883E-02,-0.40169230E-04, 0.52597135E-03,
         0.30748539E-04,-0.22165359E-04,-0.13121265E-02, 0.18606168E-03,
        -0.51311083E-04,-0.44481731E-04,-0.33642405E-04, 0.56873483E-03,
        -0.67332265E-04,-0.13310559E-02, 0.45917914E-03, 0.12494506E-03,
         0.63006236E-03, 0.26260779E-03, 0.24340625E-02, 0.36518357E-02,
         0.18287827E-03, 0.21705499E-02,-0.22051745E-03,-0.56399399E-03,
         0.14776763E-04,-0.20767109E-04,-0.27963219E-04,-0.19179087E-03,
         0.16347525E-02, 0.54139000E-04, 0.44049793E-02,-0.76674223E-04,
         0.43688212E-02,-0.75512820E-04, 0.29398745E-02, 0.61033930E-04,
         0.69096067E-03, 0.36736170E-03,-0.36518539E-02, 0.10174470E-02,
         0.13279157E-02,-0.27615333E-02,-0.14987616E-02,-0.86501089E-03,
        -0.22680031E-02, 0.11834316E-04,-0.36019524E-05,-0.13157416E-04,
         0.44387061E-05, 0.92399014E-05, 0.13833861E-04,-0.19407569E-04,
         0.18184492E-04, 0.18918698E-04, 0.17613531E-04, 0.22676957E-04,
        -0.46981262E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]    *x21*x33        
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]        *x32*x41*x51
        +coeff[ 37]    *x21    *x41*x52
        +coeff[ 38]    *x24    *x41    
        +coeff[ 39]    *x24*x31        
        +coeff[ 40]    *x22*x33        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x22    *x41*x52
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 44]    *x21            
        +coeff[ 45]                *x51
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11        *x41*x51
        +coeff[ 50]*x11    *x31    *x51
        +coeff[ 51]            *x43*x51
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]            *x41*x53
        +coeff[ 56]*x11*x21*x31*x42    
        +coeff[ 57]*x11*x21*x32*x41    
        +coeff[ 58]    *x23    *x43    
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x22    *x43*x51
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 62]    *x24    *x41*x51
        +coeff[ 63]    *x24*x31*x42*x51
        +coeff[ 64]    *x22            
        +coeff[ 65]*x12    *x31        
        +coeff[ 66]*x11*x22*x31        
        +coeff[ 67]        *x34*x41    
        +coeff[ 68]    *x21    *x45    
        +coeff[ 69]        *x31    *x53
        +coeff[ 70]    *x21*x31*x44    
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 71]*x11*x23*x31        
        +coeff[ 72]    *x21*x32*x43    
        +coeff[ 73]*x11*x22    *x41*x51
        +coeff[ 74]    *x21*x33*x42    
        +coeff[ 75]    *x22*x31    *x52
        +coeff[ 76]    *x21*x34*x41    
        +coeff[ 77]    *x23*x33        
        +coeff[ 78]    *x22*x31*x44    
        +coeff[ 79]        *x33*x44    
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 80]    *x24    *x43    
        +coeff[ 81]    *x22*x32*x43    
        +coeff[ 82]    *x21*x33*x44    
        +coeff[ 83]    *x24    *x43*x51
        +coeff[ 84]    *x24    *x45    
        +coeff[ 85]        *x31*x41    
        +coeff[ 86]*x11*x21            
        +coeff[ 87]        *x31*x41*x51
        +coeff[ 88]            *x44    
    ;
    v_y_e_dq_900                                  =v_y_e_dq_900                                  
        +coeff[ 89]*x11*x21        *x51
        +coeff[ 90]*x12        *x42    
        +coeff[ 91]*x11        *x42*x51
        +coeff[ 92]*x12*x21    *x41    
        +coeff[ 93]    *x21*x31    *x52
        +coeff[ 94]*x13        *x41    
        +coeff[ 95]*x12        *x41*x51
        +coeff[ 96]        *x32*x41*x52
        ;

    return v_y_e_dq_900                                  ;
}
float p_e_dq_900                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.4996528E-04;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.39048089E-04,-0.24492348E-01, 0.11913010E-01,-0.53220009E-02,
        -0.14885847E-01, 0.47640260E-02, 0.11709864E-01,-0.86508812E-02,
        -0.26234516E-02,-0.52829453E-03,-0.25184364E-02, 0.50902949E-03,
        -0.21546492E-02,-0.13292843E-02,-0.37132640E-03, 0.24096556E-02,
        -0.93842205E-03, 0.68278998E-04, 0.24812884E-03,-0.29821647E-03,
        -0.13354438E-02, 0.53520442E-03,-0.10264504E-02,-0.94660383E-03,
         0.87848125E-03, 0.11846620E-03, 0.56125235E-03,-0.25182377E-03,
        -0.17498370E-02,-0.10388923E-02,-0.24748428E-03, 0.29325942E-03,
        -0.16965096E-03, 0.10548925E-03,-0.41048476E-03,-0.50266861E-03,
        -0.19516834E-03, 0.45878915E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_900                                  =v_p_e_dq_900                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_dq_900                                  =v_p_e_dq_900                                  
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_900                                  =v_p_e_dq_900                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]            *x41*x52
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x21    *x41*x52
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_900                                  =v_p_e_dq_900                                  
        +coeff[ 35]    *x25    *x41    
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_900                                  ;
}
float l_e_dq_900                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.1998733E-01;
    float xmin[10]={
        -0.39997E-02,-0.60065E-01,-0.53970E-01,-0.30020E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60037E-01, 0.53997E-01, 0.29984E-01, 0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.19995671E-01,-0.31208816E+00,-0.25396766E-01, 0.12395716E-01,
        -0.28636007E-01,-0.17087867E-01,-0.51710200E-02,-0.53056967E-02,
         0.20882334E-02, 0.73577762E-02, 0.85868863E-02, 0.23546313E-03,
         0.63841890E-05, 0.16240556E-02,-0.26318901E-02,-0.85118238E-03,
         0.34888142E-02,-0.21452485E-02, 0.14265068E-03, 0.16897134E-02,
        -0.21597282E-03, 0.45097389E-03, 0.51704294E-03, 0.60014694E-03,
        -0.28648009E-03,-0.64325565E-03, 0.84372889E-02, 0.40004775E-02,
         0.52580540E-02,-0.54713170E-03, 0.65847283E-03, 0.36699211E-02,
         0.40067723E-02,-0.26649560E-02, 0.14674610E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_900                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_900                                  =v_l_e_dq_900                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_900                                  =v_l_e_dq_900                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]        *x31    *x51
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]    *x21        *x52
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11        *x41*x51
        +coeff[ 25]    *x21*x31*x41*x51
    ;
    v_l_e_dq_900                                  =v_l_e_dq_900                                  
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x11*x23    *x41    
        +coeff[ 30]*x12        *x42*x51
        +coeff[ 31]    *x21*x34*x42    
        +coeff[ 32]    *x23    *x44    
        +coeff[ 33]        *x33*x43*x51
        +coeff[ 34]    *x21*x31*x44*x51
        ;

    return v_l_e_dq_900                                  ;
}
float x_e_dq_800                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5082942E+00;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.65031708E-02,-0.94353789E-02, 0.95880345E-01, 0.19153209E+00,
         0.24727495E-01, 0.14773733E-01,-0.32837458E-02,-0.31960802E-02,
        -0.78581162E-02,-0.81217727E-02,-0.99646102E-03,-0.17439214E-02,
         0.18269315E-02,-0.37766183E-02,-0.79424633E-03,-0.55989221E-03,
        -0.21968117E-03,-0.36592429E-03,-0.55639528E-04,-0.90200827E-03,
        -0.52475827E-02,-0.31245393E-02,-0.29281734E-02,-0.37449852E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_800                                  =v_x_e_dq_800                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_800                                  =v_x_e_dq_800                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_800                                  ;
}
float t_e_dq_800                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5715351E+00;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.50601792E-02, 0.35731117E-02,-0.10186198E+00, 0.24326345E-01,
        -0.75257677E-02,-0.23499404E-02,-0.91777631E-03,-0.10111275E-02,
         0.23785366E-02, 0.10650804E-02, 0.10073924E-02, 0.30308194E-03,
         0.46955832E-03, 0.44990389E-03, 0.10272175E-03,-0.16189339E-03,
         0.69265073E-03, 0.47487003E-03, 0.84869767E-03,-0.54176978E-03,
         0.21314151E-02, 0.11655581E-03, 0.32887631E-03, 0.49818547E-04,
        -0.17686692E-03, 0.40096141E-03, 0.27081696E-03, 0.89801615E-04,
        -0.53018628E-03,-0.58661209E-03, 0.26732536E-02, 0.20729478E-02,
         0.41641810E-03,-0.38099868E-03, 0.85488879E-04, 0.12560231E-03,
        -0.15782646E-03, 0.20678935E-02, 0.16541932E-03,-0.28249636E-03,
         0.62391817E-04,-0.27184029E-04, 0.83118648E-04, 0.41617397E-04,
         0.51357427E-04, 0.47102367E-04, 0.62373816E-04,-0.61467996E-04,
         0.97739371E-03,-0.16094524E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_800                                  =v_t_e_dq_800                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_800                                  =v_t_e_dq_800                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]*x11*x22            
        +coeff[ 22]    *x23            
        +coeff[ 23]    *x22        *x51
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_dq_800                                  =v_t_e_dq_800                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x22    *x42*x51
        +coeff[ 33]    *x23        *x52
        +coeff[ 34]*x11        *x42    
    ;
    v_t_e_dq_800                                  =v_t_e_dq_800                                  
        +coeff[ 35]        *x32*x42    
        +coeff[ 36]    *x22        *x52
        +coeff[ 37]    *x21*x32*x42    
        +coeff[ 38]    *x22*x32    *x51
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_800                                  =v_t_e_dq_800                                  
        +coeff[ 44]*x11*x23            
        +coeff[ 45]        *x33*x41    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x31*x41*x51
        ;

    return v_t_e_dq_800                                  ;
}
float y_e_dq_800                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.6964882E-03;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.69670583E-03, 0.11362374E+00,-0.78744829E-01,-0.65963335E-01,
        -0.22530923E-01, 0.47918763E-01, 0.25621952E-01,-0.31590898E-01,
        -0.13017824E-01,-0.71687442E-02, 0.24282129E-02,-0.95647518E-02,
        -0.22642089E-02,-0.30993086E-02,-0.23444030E-03,-0.68767485E-03,
         0.86910580E-03,-0.55668592E-02,-0.11721180E-02, 0.16130536E-02,
        -0.17529916E-02, 0.54134317E-02,-0.55260644E-02, 0.50779374E-03,
         0.17400761E-02,-0.23252291E-02,-0.13202890E-02, 0.20124952E-02,
         0.25108196E-02, 0.11362211E-02,-0.42900951E-02,-0.51343003E-02,
        -0.32855235E-02,-0.35872025E-03, 0.11748826E-02, 0.28574772E-03,
        -0.33925395E-03, 0.72929438E-03, 0.32826429E-03,-0.16957220E-02,
        -0.14839852E-02, 0.12262693E-02,-0.41086601E-04,-0.46752385E-03,
         0.18168698E-03,-0.79177378E-03,-0.12065290E-03, 0.46113002E-03,
        -0.55268494E-04,-0.32164873E-04, 0.61376754E-03,-0.39881383E-03,
         0.49964897E-03, 0.11880444E-03, 0.69348549E-03, 0.30729576E-03,
        -0.20030211E-03, 0.14485900E-02, 0.12821563E-02, 0.35335444E-03,
        -0.17780626E-03,-0.81747165E-02,-0.84419632E-02,-0.20709939E-02,
        -0.74693794E-05, 0.60145076E-05,-0.31222906E-04,-0.25297613E-04,
        -0.33384589E-04,-0.11685566E-03, 0.57406571E-04, 0.17668118E-02,
         0.22919080E-02, 0.16132147E-02, 0.36136710E-03, 0.74142683E-03,
         0.85058164E-04,-0.74037031E-04,-0.14948584E-02,-0.10665828E-03,
        -0.74063413E-04,-0.26909783E-02,-0.43846094E-02,-0.13938528E-02,
        -0.12196263E-02, 0.27509017E-02,-0.14390511E-03, 0.45103752E-02,
         0.45766360E-02, 0.18075036E-02,-0.11074810E-02,-0.68640593E-03,
        -0.73402261E-05,-0.17279333E-04,-0.24121937E-04,-0.20765372E-04,
        -0.69503716E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]*x11*x22    *x41*x51
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]*x11    *x31    *x51
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 53]            *x41*x53
        +coeff[ 54]*x11*x21*x31*x42    
        +coeff[ 55]*x11*x21*x32*x41    
        +coeff[ 56]    *x23    *x43    
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x23*x32*x41    
        +coeff[ 59]    *x23*x33        
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x24    *x45    
        +coeff[ 64]    *x21            
        +coeff[ 65]                *x51
        +coeff[ 66]*x11        *x41*x51
        +coeff[ 67]*x12    *x31        
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]        *x34*x41    
        +coeff[ 70]        *x31    *x53
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 71]    *x21*x31*x44    
        +coeff[ 72]    *x21*x32*x43    
        +coeff[ 73]    *x21*x33*x42    
        +coeff[ 74]    *x22    *x43*x51
        +coeff[ 75]    *x21*x34*x41    
        +coeff[ 76]    *x21    *x41*x53
        +coeff[ 77]    *x22*x31*x42*x51
        +coeff[ 78]    *x22    *x45    
        +coeff[ 79]    *x21    *x45*x51
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 80]*x11*x21*x32*x41*x51
        +coeff[ 81]    *x24*x31*x42    
        +coeff[ 82]    *x22*x33*x42    
        +coeff[ 83]    *x24*x32*x41    
        +coeff[ 84]    *x22*x34*x41    
        +coeff[ 85]    *x23    *x45    
        +coeff[ 86]    *x21*x34*x41*x51
        +coeff[ 87]    *x23*x31*x44    
        +coeff[ 88]    *x23*x32*x43    
    ;
    v_y_e_dq_800                                  =v_y_e_dq_800                                  
        +coeff[ 89]    *x23*x33*x42    
        +coeff[ 90]    *x24    *x43*x51
        +coeff[ 91]    *x24*x31*x42*x51
        +coeff[ 92]*x11*x21        *x51
        +coeff[ 93]    *x21    *x42*x51
        +coeff[ 94]*x11*x21    *x41*x51
        +coeff[ 95]*x11        *x41*x52
        +coeff[ 96]    *x21*x31*x42*x51
        ;

    return v_y_e_dq_800                                  ;
}
float p_e_dq_800                                  (float *x,int m){
    int ncoeff= 40;
    float avdat=  0.7921478E-04;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 41]={
        -0.81267783E-04,-0.24581745E-01, 0.11773256E-01,-0.53225388E-02,
        -0.14935644E-01, 0.47673094E-02, 0.11716021E-01,-0.86135920E-02,
        -0.26019434E-02,-0.53094607E-03,-0.25156238E-02, 0.50905324E-03,
        -0.21668526E-02,-0.13315157E-02,-0.36618198E-03, 0.24495996E-02,
         0.22927420E-05,-0.10277847E-02, 0.21073962E-04,-0.29610729E-03,
        -0.12943998E-02, 0.53349946E-03,-0.10409391E-02,-0.94543729E-03,
         0.84235059E-03, 0.24726833E-03, 0.11684774E-03, 0.82702661E-03,
        -0.25765493E-03,-0.17066945E-02,-0.10991192E-02,-0.43031044E-03,
         0.46596583E-03,-0.25255480E-03, 0.36980500E-03,-0.17952568E-03,
         0.10840516E-03,-0.50409394E-03,-0.19817713E-03, 0.46074469E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_800                                  =v_p_e_dq_800                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_dq_800                                  =v_p_e_dq_800                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11    *x31        
    ;
    v_p_e_dq_800                                  =v_p_e_dq_800                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x21*x31    *x52
        +coeff[ 29]    *x22*x31*x42    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x23*x31    *x51
        +coeff[ 32]    *x21    *x45    
        +coeff[ 33]            *x41*x52
        +coeff[ 34]    *x21*x32*x41    
    ;
    v_p_e_dq_800                                  =v_p_e_dq_800                                  
        +coeff[ 35]*x11*x22    *x41    
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x25    *x41    
        +coeff[ 38]    *x22*x31    *x52
        +coeff[ 39]    *x22    *x41*x52
        ;

    return v_p_e_dq_800                                  ;
}
float l_e_dq_800                                  (float *x,int m){
    int ncoeff= 33;
    float avdat= -0.2047581E-01;
    float xmin[10]={
        -0.39991E-02,-0.60017E-01,-0.53981E-01,-0.30020E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60020E-01, 0.53992E-01, 0.30025E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 34]={
         0.20429825E-01,-0.31207106E+00,-0.25401155E-01, 0.12413545E-01,
        -0.29638033E-01,-0.16922308E-01,-0.56123859E-02,-0.53438726E-02,
         0.20515278E-02, 0.96977437E-02, 0.87196250E-02, 0.60036842E-03,
        -0.15045077E-04, 0.89101674E-03,-0.25270805E-02,-0.77221182E-03,
         0.37013970E-02,-0.22923043E-02, 0.17531768E-02, 0.74689521E-03,
         0.47329515E-02, 0.13022036E-02, 0.63565566E-03, 0.61382854E-03,
         0.98007708E-03, 0.20021144E-02, 0.58742492E-02, 0.11871980E-02,
         0.12165294E-02,-0.12405849E-02,-0.16251993E-02, 0.83377666E-03,
         0.21415097E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_800                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_800                                  =v_l_e_dq_800                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_800                                  =v_l_e_dq_800                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]        *x32*x42*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x24            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dq_800                                  =v_l_e_dq_800                                  
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x22*x32    *x51
        +coeff[ 28]    *x24    *x42    
        +coeff[ 29]    *x23*x32    *x51
        +coeff[ 30]    *x21*x31*x41*x53
        +coeff[ 31]*x11*x21*x33    *x51
        +coeff[ 32]    *x21*x34*x42    
        ;

    return v_l_e_dq_800                                  ;
}
float x_e_dq_700                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5077526E+00;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.60219299E-02,-0.94237281E-02, 0.95885240E-01, 0.19162714E+00,
         0.24755863E-01, 0.14805559E-01,-0.32838867E-02,-0.31827253E-02,
        -0.77339509E-02,-0.78676147E-02,-0.10087283E-02,-0.17546624E-02,
         0.18103442E-02,-0.36274984E-02,-0.79897238E-03,-0.55761950E-03,
        -0.40871967E-03, 0.12508656E-03,-0.93030254E-03,-0.55807908E-02,
        -0.36004421E-02,-0.29012028E-02,-0.72489114E-03,-0.38749978E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_700                                  =v_x_e_dq_700                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_700                                  =v_x_e_dq_700                                  
        +coeff[ 17]    *x23            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32    *x52
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_700                                  ;
}
float t_e_dq_700                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5718375E+00;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.47185551E-02,-0.10198132E+00, 0.24316393E-01,-0.75386707E-02,
        -0.15460549E-05, 0.35743280E-02,-0.23564892E-02,-0.10406009E-02,
        -0.93240675E-03, 0.29977891E-02, 0.46087123E-03, 0.11133150E-02,
         0.89211139E-03, 0.29949629E-03, 0.45715959E-03, 0.12729884E-03,
        -0.15599802E-03, 0.77418168E-03, 0.50169020E-03, 0.79334521E-03,
        -0.55421871E-03, 0.21571969E-02, 0.12331748E-03, 0.35520998E-03,
         0.10673709E-03,-0.15097742E-03, 0.36944301E-03, 0.13167455E-03,
        -0.50797866E-03,-0.51767431E-03, 0.25184206E-02, 0.12571322E-02,
        -0.44984560E-03, 0.79199788E-04, 0.26489611E-03,-0.14313355E-03,
         0.10886785E-02,-0.24302240E-03, 0.17145880E-04, 0.30267719E-03,
        -0.24743602E-03, 0.51517232E-04,-0.32843622E-04, 0.71609378E-04,
         0.43307511E-04, 0.62214465E-04, 0.78526835E-04, 0.54224067E-04,
         0.69050300E-04,-0.83217921E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_700                                  =v_t_e_dq_700                                  
        +coeff[  8]                *x52
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]            *x42    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]*x11*x21            
        +coeff[ 14]            *x42*x51
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dq_700                                  =v_t_e_dq_700                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_700                                  =v_t_e_dq_700                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x21*x32    *x51
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x23        *x52
        +coeff[ 33]*x11        *x42    
        +coeff[ 34]    *x22*x31*x41    
    ;
    v_t_e_dq_700                                  =v_t_e_dq_700                                  
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]        *x33*x41*x51
        +coeff[ 39]    *x22    *x42*x51
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]        *x31*x41*x51
    ;
    v_t_e_dq_700                                  =v_t_e_dq_700                                  
        +coeff[ 44]                *x53
        +coeff[ 45]*x11*x23            
        +coeff[ 46]        *x33*x41    
        +coeff[ 47]*x11*x22        *x51
        +coeff[ 48]            *x42*x52
        +coeff[ 49]    *x21        *x53
        ;

    return v_t_e_dq_700                                  ;
}
float y_e_dq_700                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.3663409E-03;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.38664902E-03, 0.11273324E+00,-0.79317905E-01,-0.65748103E-01,
        -0.22449404E-01, 0.47960885E-01, 0.25634622E-01,-0.31376909E-01,
        -0.12602921E-01,-0.71616569E-02, 0.24107923E-02,-0.91398433E-02,
        -0.22593427E-02,-0.31011340E-02,-0.26957865E-03,-0.10292935E-02,
         0.86538278E-03,-0.53782263E-02,-0.11120463E-02, 0.16367284E-02,
        -0.17495067E-02, 0.50568786E-02,-0.54638158E-02, 0.51538018E-03,
         0.16516662E-02,-0.22374298E-02,-0.11814394E-02, 0.57130511E-03,
         0.11505360E-02, 0.82944834E-03,-0.48024198E-02,-0.83717825E-02,
        -0.53014634E-02, 0.28314130E-03, 0.12352796E-02, 0.23759717E-03,
        -0.34215950E-03, 0.70949562E-03, 0.34463455E-03,-0.19338655E-02,
        -0.20031019E-02, 0.12536476E-02, 0.37290579E-05,-0.19435271E-04,
         0.16077036E-04,-0.19995528E-04,-0.89643989E-03, 0.18476565E-03,
        -0.13275590E-02, 0.51547075E-03, 0.56071475E-03,-0.94244821E-03,
         0.36346208E-03,-0.29332255E-03, 0.12904979E-03, 0.69521513E-03,
         0.28170121E-03, 0.22251934E-02, 0.38375799E-02, 0.20070821E-02,
        -0.28552863E-03,-0.58380067E-02,-0.51416364E-02,-0.88154040E-04,
         0.73504855E-03,-0.93657814E-03,-0.70213196E-05,-0.38660117E-04,
        -0.18208131E-04,-0.31678988E-04,-0.27703160E-04,-0.62705491E-04,
         0.16352582E-02, 0.51434119E-04, 0.43241186E-02, 0.43246349E-04,
         0.43415204E-02,-0.31376598E-03, 0.25923448E-02, 0.48766964E-04,
         0.63210068E-03,-0.64225425E-03, 0.43809065E-03,-0.16364580E-02,
         0.32161106E-03,-0.18749574E-03, 0.48623257E-04,-0.17821208E-02,
         0.15327442E-03, 0.28324136E-03,-0.13173684E-03,-0.76431606E-03,
        -0.94100596E-05,-0.20537685E-04, 0.18418974E-04, 0.21477912E-04,
         0.22060283E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 44]                *x51
        +coeff[ 45]*x11        *x41*x51
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]    *x22*x33        
        +coeff[ 49]    *x22    *x41*x52
        +coeff[ 50]            *x43*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]*x11*x21*x32*x41    
        +coeff[ 57]    *x23    *x43    
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x23*x31*x44    
        +coeff[ 64]    *x23*x32*x43    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]    *x22            
        +coeff[ 67]*x12        *x41    
        +coeff[ 68]*x12    *x31        
        +coeff[ 69]*x11    *x31    *x51
        +coeff[ 70]*x11*x22*x31        
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 71]*x11*x21    *x41*x51
        +coeff[ 72]    *x21    *x45    
        +coeff[ 73]        *x31    *x53
        +coeff[ 74]    *x21*x31*x44    
        +coeff[ 75]*x11    *x31*x42*x51
        +coeff[ 76]    *x21*x32*x43    
        +coeff[ 77]*x11*x22    *x41*x51
        +coeff[ 78]    *x21*x33*x42    
        +coeff[ 79]    *x22    *x43*x51
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 80]    *x21*x34*x41    
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x23*x33        
        +coeff[ 83]    *x22    *x45    
        +coeff[ 84]        *x33*x44    
        +coeff[ 85]    *x24*x31    *x51
        +coeff[ 86]*x13        *x43    
        +coeff[ 87]    *x22*x33*x42    
        +coeff[ 88]*x11*x23    *x43    
    ;
    v_y_e_dq_700                                  =v_y_e_dq_700                                  
        +coeff[ 89]*x11*x24    *x41*x51
        +coeff[ 90]    *x21    *x45*x52
        +coeff[ 91]    *x24    *x43*x51
        +coeff[ 92]        *x31*x41    
        +coeff[ 93]    *x22    *x42    
        +coeff[ 94]*x11        *x42*x51
        +coeff[ 95]*x12*x21    *x41    
        +coeff[ 96]*x11    *x31*x41*x51
        ;

    return v_y_e_dq_700                                  ;
}
float p_e_dq_700                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.8869400E-04;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.87833490E-04,-0.24662623E-01, 0.11618707E-01,-0.52998844E-02,
        -0.14856300E-01, 0.47653085E-02, 0.11710197E-01,-0.88437954E-02,
        -0.26273241E-02,-0.32743029E-03,-0.28432989E-02, 0.51053578E-03,
        -0.24307261E-02,-0.14306798E-02,-0.38457039E-03, 0.24074467E-02,
        -0.10402342E-02,-0.22508341E-03,-0.21414757E-04, 0.24829642E-03,
        -0.27838643E-03,-0.13720796E-02, 0.53690805E-03,-0.94786257E-03,
         0.86673949E-03, 0.11907285E-03,-0.25999817E-03,-0.13909562E-02,
        -0.22941743E-03, 0.47571131E-03, 0.29045829E-03,-0.17654848E-03,
        -0.40675991E-03,-0.38229636E-03,-0.48088527E-03, 0.31581937E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_700                                  =v_p_e_dq_700                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_700                                  =v_p_e_dq_700                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_700                                  =v_p_e_dq_700                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x24*x31*x42*x52
        +coeff[ 28]            *x41*x52
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x22    *x43    
        +coeff[ 33]    *x23*x31    *x51
        +coeff[ 34]    *x25    *x41    
    ;
    v_p_e_dq_700                                  =v_p_e_dq_700                                  
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dq_700                                  ;
}
float l_e_dq_700                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1952075E-01;
    float xmin[10]={
        -0.39994E-02,-0.60026E-01,-0.53986E-01,-0.30014E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60050E-01, 0.53996E-01, 0.30030E-01, 0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.19296456E-01,-0.31251886E+00,-0.25378594E-01, 0.12432121E-01,
        -0.28534740E-01,-0.17061941E-01,-0.54039415E-02,-0.50927410E-02,
         0.19620112E-02, 0.63240360E-02, 0.86591514E-02, 0.15011507E-03,
         0.12590483E-03, 0.12412560E-02,-0.26224535E-02,-0.85712911E-03,
         0.33047535E-02,-0.20348111E-02, 0.32450707E-03, 0.19026918E-02,
         0.50882861E-03, 0.35107488E-03, 0.48263257E-03,-0.18263180E-02,
         0.74511691E-03, 0.82257697E-02, 0.27985589E-02, 0.75497455E-02,
         0.36141899E-03,-0.20511255E-02, 0.26283849E-02,-0.37190190E-03,
         0.11297928E-02, 0.45109428E-02, 0.36503673E-02, 0.79854084E-02,
         0.47798743E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dq_700                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_700                                  =v_l_e_dq_700                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_700                                  =v_l_e_dq_700                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x21*x31*x41*x51
        +coeff[ 24]        *x31*x41*x52
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dq_700                                  =v_l_e_dq_700                                  
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]        *x34    *x51
        +coeff[ 29]    *x22*x31*x41*x51
        +coeff[ 30]    *x23*x31*x41*x51
        +coeff[ 31]*x12            *x54
        +coeff[ 32]    *x23*x32*x42    
        +coeff[ 33]    *x23    *x44    
        +coeff[ 34]    *x21*x32*x44    
    ;
    v_l_e_dq_700                                  =v_l_e_dq_700                                  
        +coeff[ 35]    *x21*x32*x42*x52
        +coeff[ 36]    *x21*x31*x43*x52
        ;

    return v_l_e_dq_700                                  ;
}
float x_e_dq_600                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5079997E+00;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.65702037E-02,-0.94212880E-02, 0.95900424E-01, 0.19198938E+00,
         0.24754820E-01, 0.14827832E-01,-0.32689006E-02,-0.31623850E-02,
        -0.79470174E-02,-0.79812352E-02,-0.10126025E-02,-0.17414216E-02,
         0.18443819E-02,-0.37306121E-02,-0.80039294E-03,-0.56171376E-03,
        -0.23359472E-03,-0.38407333E-03, 0.31033582E-04,-0.91476325E-03,
        -0.51996033E-02,-0.33989490E-02,-0.27631733E-02,-0.35611219E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_600                                  =v_x_e_dq_600                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_600                                  =v_x_e_dq_600                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_600                                  ;
}
float t_e_dq_600                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5713496E+00;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.51400107E-02,-0.10209014E+00, 0.24324775E-01,-0.75893966E-02,
        -0.41725739E-05, 0.35740060E-02,-0.23552631E-02,-0.93384722E-03,
        -0.10351852E-02, 0.30566936E-02, 0.11804312E-02, 0.81762997E-03,
         0.31430076E-03, 0.46946248E-03, 0.45531895E-03, 0.11375617E-03,
        -0.16347434E-03, 0.81587723E-03, 0.48056341E-03, 0.79550897E-03,
        -0.55377482E-03, 0.20795725E-02, 0.10955134E-03, 0.36125444E-03,
         0.98505538E-04,-0.14432744E-03, 0.37605135E-03, 0.27076306E-03,
         0.13854550E-03,-0.48335886E-03,-0.50215254E-03,-0.15729091E-03,
         0.24153660E-02, 0.12890721E-02, 0.79557001E-04, 0.10467616E-02,
        -0.28589758E-03, 0.33584874E-04, 0.29934847E-03,-0.38892054E-03,
        -0.28145051E-03, 0.52244675E-04,-0.44086333E-04, 0.69309470E-04,
         0.35077577E-04, 0.50716335E-04, 0.70389964E-04,-0.56071778E-04,
         0.59786118E-04, 0.61630082E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]                *x52
    ;
    v_t_e_dq_600                                  =v_t_e_dq_600                                  
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42    
        +coeff[ 14]            *x42*x51
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dq_600                                  =v_t_e_dq_600                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_600                                  =v_t_e_dq_600                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x21*x32    *x51
        +coeff[ 29]    *x21*x31*x41*x51
        +coeff[ 30]    *x21    *x42*x51
        +coeff[ 31]    *x22        *x52
        +coeff[ 32]    *x23*x31*x41    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]*x11        *x42    
    ;
    v_t_e_dq_600                                  =v_t_e_dq_600                                  
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]        *x31*x41*x51
    ;
    v_t_e_dq_600                                  =v_t_e_dq_600                                  
        +coeff[ 44]                *x53
        +coeff[ 45]*x11*x23            
        +coeff[ 46]        *x33*x41    
        +coeff[ 47]*x11*x21    *x42    
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]        *x32    *x52
        ;

    return v_t_e_dq_600                                  ;
}
float y_e_dq_600                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.1759610E-03;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.18274096E-03, 0.11154038E+00,-0.79976171E-01,-0.65855272E-01,
        -0.22585955E-01, 0.47977019E-01, 0.25673628E-01,-0.31499837E-01,
        -0.13003750E-01,-0.72020097E-02, 0.24163453E-02,-0.96904468E-02,
        -0.22644219E-02,-0.30986913E-02,-0.16223187E-03,-0.57943957E-03,
         0.85913582E-03,-0.56013744E-02, 0.16243327E-02,-0.17478486E-02,
         0.53894203E-02,-0.55636195E-02,-0.11800585E-02, 0.51812996E-03,
         0.19761478E-02,-0.22765116E-02,-0.12122543E-02, 0.19685600E-02,
         0.38373000E-02, 0.18758081E-02,-0.43600411E-02,-0.49378015E-02,
        -0.35230594E-02,-0.32719888E-03, 0.12118404E-02, 0.47076400E-03,
        -0.33786817E-03, 0.71310566E-03, 0.33916556E-03,-0.18180690E-02,
        -0.15483769E-02, 0.12801582E-02,-0.32966925E-05,-0.53824457E-04,
        -0.20767568E-03, 0.18331945E-03,-0.77797612E-03, 0.48471426E-03,
        -0.42963904E-04, 0.57635840E-03,-0.60049362E-04,-0.31742512E-03,
         0.45735689E-03,-0.12617606E-03, 0.12691515E-03, 0.63147425E-03,
         0.29067765E-03,-0.88497472E-03, 0.10111891E-03,-0.23680902E-03,
        -0.87438179E-02,-0.83795879E-02, 0.26423666E-02, 0.77239005E-02,
         0.75317658E-02,-0.43634945E-03, 0.50165411E-02, 0.13895945E-02,
        -0.39405408E-03,-0.18335837E-02, 0.80911968E-05,-0.19502304E-04,
        -0.28730979E-04, 0.39227383E-04, 0.54971941E-04, 0.97153039E-03,
        -0.64789478E-04, 0.83200815E-04,-0.37455771E-03,-0.17057598E-02,
        -0.12874180E-03,-0.25407113E-02,-0.46844976E-02,-0.13775724E-02,
        -0.97340695E-03,-0.87890035E-03,-0.36701888E-05,-0.13366931E-04,
         0.39193487E-05,-0.10635672E-04, 0.18128057E-04, 0.22426235E-04,
         0.52169271E-04,-0.27159303E-04, 0.10314709E-04,-0.32375192E-04,
         0.60721199E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]*x11        *x41*x51
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 44]        *x31*x44    
        +coeff[ 45]        *x33    *x51
        +coeff[ 46]    *x22*x33        
        +coeff[ 47]    *x22    *x41*x52
        +coeff[ 48]*x12        *x41    
        +coeff[ 49]            *x43*x51
        +coeff[ 50]*x11*x21    *x41*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]*x11*x21*x32*x41    
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]    *x22    *x43*x51
        +coeff[ 59]    *x24    *x41*x51
        +coeff[ 60]    *x22*x31*x44    
        +coeff[ 61]    *x22*x32*x43    
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 62]    *x23    *x45    
        +coeff[ 63]    *x23*x31*x44    
        +coeff[ 64]    *x23*x32*x43    
        +coeff[ 65]    *x21*x34*x43    
        +coeff[ 66]    *x23*x33*x42    
        +coeff[ 67]    *x23*x34*x41    
        +coeff[ 68]    *x24*x31*x42*x51
        +coeff[ 69]    *x24    *x45    
        +coeff[ 70]    *x21            
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 71]*x12    *x31        
        +coeff[ 72]*x11    *x31    *x51
        +coeff[ 73]*x12        *x41*x51
        +coeff[ 74]        *x31    *x53
        +coeff[ 75]    *x21*x32*x43    
        +coeff[ 76]*x11*x22    *x41*x51
        +coeff[ 77]    *x21    *x41*x53
        +coeff[ 78]    *x22*x31*x42*x51
        +coeff[ 79]    *x22    *x45    
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 80]    *x24*x31    *x51
        +coeff[ 81]    *x24*x31*x42    
        +coeff[ 82]    *x22*x33*x42    
        +coeff[ 83]    *x24*x32*x41    
        +coeff[ 84]    *x22*x34*x41    
        +coeff[ 85]    *x24    *x43*x51
        +coeff[ 86]                *x51
        +coeff[ 87]    *x22            
        +coeff[ 88]*x11*x21            
    ;
    v_y_e_dq_600                                  =v_y_e_dq_600                                  
        +coeff[ 89]    *x21    *x42    
        +coeff[ 90]*x11    *x31*x42    
        +coeff[ 91]    *x24            
        +coeff[ 92]    *x21    *x44    
        +coeff[ 93]*x11*x22*x31        
        +coeff[ 94]*x11        *x44    
        +coeff[ 95]    *x23    *x42    
        +coeff[ 96]    *x21*x31*x42*x51
        ;

    return v_y_e_dq_600                                  ;
}
float p_e_dq_600                                  (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.5761526E-04;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.57927224E-04,-0.24842573E-01, 0.11334307E-01,-0.53066309E-02,
        -0.14753519E-01, 0.47669634E-02, 0.11706691E-01,-0.86523406E-02,
        -0.26082245E-02,-0.50178607E-03,-0.25217088E-02, 0.51016605E-03,
        -0.21652961E-02,-0.13271996E-02,-0.37345345E-03, 0.18669855E-02,
        -0.25388649E-05,-0.91912370E-03, 0.59200476E-04,-0.29341810E-03,
        -0.13289669E-02, 0.53275586E-03,-0.10510826E-02,-0.97591011E-03,
         0.87270426E-03, 0.24889092E-03, 0.11639227E-03,-0.25394911E-03,
        -0.25101745E-03, 0.55042043E-03, 0.29747799E-03,-0.18724153E-03,
         0.11255107E-03,-0.17119747E-02,-0.10458690E-02,-0.38404993E-03,
        -0.19481589E-03, 0.44812501E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_600                                  =v_p_e_dq_600                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]*x11    *x33        
    ;
    v_p_e_dq_600                                  =v_p_e_dq_600                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11    *x31        
    ;
    v_p_e_dq_600                                  =v_p_e_dq_600                                  
        +coeff[ 26]*x11*x21*x31        
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]            *x41*x52
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x21    *x41*x52
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dq_600                                  =v_p_e_dq_600                                  
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_600                                  ;
}
float l_e_dq_600                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.2064383E-01;
    float xmin[10]={
        -0.39998E-02,-0.60031E-01,-0.53998E-01,-0.30005E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39973E-02, 0.60045E-01, 0.53996E-01, 0.30000E-01, 0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.20643471E-01,-0.31273994E+00,-0.25325991E-01, 0.12417577E-01,
        -0.29061556E-01,-0.16875759E-01,-0.56521306E-02,-0.54714116E-02,
         0.20903551E-02, 0.58556362E-02, 0.49993233E-02, 0.24496767E-03,
         0.22282810E-02,-0.28027464E-02,-0.86016947E-03, 0.22682501E-02,
        -0.23583816E-02, 0.12215462E-02, 0.82430575E-03, 0.64173387E-02,
        -0.46975871E-04,-0.15846977E-03,-0.15234557E-03, 0.52569009E-03,
         0.46762111E-03, 0.14760455E-02, 0.76623862E-02, 0.11763313E-01,
         0.11087601E-01, 0.41702022E-02, 0.10173694E-02, 0.12978006E-02,
         0.12818874E-02, 0.40457062E-02, 0.59458325E-02,-0.14713737E-02,
        -0.29689134E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dq_600                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_600                                  =v_l_e_dq_600                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]        *x34        
        +coeff[ 12]    *x23*x32        
        +coeff[ 13]        *x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dq_600                                  =v_l_e_dq_600                                  
        +coeff[ 17]        *x31*x41*x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]        *x31        
        +coeff[ 21]                *x52
        +coeff[ 22]*x11    *x31        
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dq_600                                  =v_l_e_dq_600                                  
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x21*x32*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]    *x21    *x44    
        +coeff[ 30]    *x22*x32    *x51
        +coeff[ 31]    *x24*x32        
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]    *x23*x33*x41    
        +coeff[ 34]    *x21*x33*x41*x52
    ;
    v_l_e_dq_600                                  =v_l_e_dq_600                                  
        +coeff[ 35]        *x33*x42*x52
        +coeff[ 36]    *x21*x31*x41*x54
        ;

    return v_l_e_dq_600                                  ;
}
float x_e_dq_500                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5063860E+00;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.55864099E-02,-0.94224978E-02, 0.95911264E-01, 0.19227436E+00,
         0.24755005E-01, 0.14882178E-01,-0.32766361E-02,-0.31601693E-02,
        -0.77225417E-02,-0.80198208E-02,-0.10131528E-02,-0.17468047E-02,
         0.18647125E-02,-0.37384240E-02,-0.80555340E-03,-0.53915405E-03,
        -0.22908540E-03,-0.36702480E-03, 0.31232004E-04,-0.83309651E-03,
        -0.55741514E-02,-0.32827691E-02,-0.31126956E-02,-0.42963005E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_500                                  =v_x_e_dq_500                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_500                                  =v_x_e_dq_500                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_500                                  ;
}
float t_e_dq_500                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5720954E+00;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.42421715E-02, 0.35669229E-02,-0.10219206E+00, 0.24323698E-01,
        -0.75356397E-02,-0.23817236E-02,-0.91157376E-03,-0.99932228E-03,
         0.24121860E-02, 0.10527943E-02, 0.10286577E-02, 0.31630683E-03,
         0.45377773E-03, 0.43303473E-03, 0.13680097E-03,-0.15073792E-03,
         0.66990033E-03, 0.48216790E-03, 0.83283527E-03,-0.54419960E-03,
         0.12221029E-03, 0.22410594E-02, 0.11304212E-03, 0.30879659E-03,
         0.11099734E-03,-0.18061465E-03, 0.39499658E-03,-0.18239568E-03,
         0.27405524E-02, 0.89430163E-04,-0.43796863E-04, 0.24297005E-03,
        -0.50574861E-03,-0.56263490E-03, 0.21765216E-02, 0.20406111E-02,
        -0.17271088E-03, 0.44656852E-04, 0.40766390E-03,-0.39765396E-03,
        -0.23669856E-03, 0.62197920E-04, 0.48846377E-04, 0.69545487E-04,
         0.55413340E-04,-0.61639323E-04,-0.78065306E-04, 0.94323902E-03,
         0.15713459E-03,-0.11227097E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_500                                  =v_t_e_dq_500                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_500                                  =v_t_e_dq_500                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_500                                  =v_t_e_dq_500                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22        *x52
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]*x11        *x42    
        +coeff[ 30]*x11*x21        *x51
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x21    *x42*x51
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_dq_500                                  =v_t_e_dq_500                                  
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_500                                  =v_t_e_dq_500                                  
        +coeff[ 44]*x11*x23            
        +coeff[ 45]*x11*x21    *x42    
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_dq_500                                  ;
}
float y_e_dq_500                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3460673E-03;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.38042205E-03, 0.10997473E+00,-0.80957532E-01,-0.65860048E-01,
        -0.22354748E-01, 0.48071567E-01, 0.25717931E-01,-0.31671688E-01,
        -0.12931962E-01,-0.71661174E-02, 0.24032632E-02,-0.96102767E-02,
        -0.22603755E-02,-0.18944289E-03,-0.45309323E-03, 0.85876556E-03,
        -0.57333787E-02, 0.16700724E-02,-0.31161073E-02,-0.17579690E-02,
         0.54613124E-02,-0.55956501E-02,-0.11697018E-02, 0.55006403E-03,
         0.17015425E-02,-0.21141709E-02,-0.11847944E-02, 0.21154555E-02,
         0.22142367E-02, 0.17165162E-02,-0.41984078E-02,-0.50401329E-02,
        -0.28990402E-02,-0.23662907E-03, 0.12211151E-02, 0.27297807E-03,
        -0.32608208E-03, 0.71736495E-03, 0.32086021E-03,-0.17596734E-02,
        -0.15893430E-02, 0.12818892E-02, 0.32526525E-04,-0.36323845E-03,
         0.18238713E-03,-0.89339342E-03, 0.51832729E-03, 0.21145413E-04,
        -0.98950104E-05,-0.58784222E-04,-0.44912307E-04,-0.30752759E-04,
         0.60799066E-03,-0.60049970E-04,-0.31975267E-03, 0.38785202E-03,
         0.12779243E-03, 0.55595633E-03, 0.21744194E-03, 0.11452982E-03,
         0.25654600E-02, 0.10255313E-02,-0.49860444E-03,-0.86960057E-02,
        -0.91574760E-02,-0.21086789E-02, 0.87317976E-05,-0.21648488E-04,
        -0.25779245E-04,-0.12661598E-04, 0.56897683E-04, 0.29584313E-04,
         0.24216077E-02,-0.66575041E-04, 0.23842985E-02,-0.75303120E-04,
         0.18894686E-02,-0.23611234E-04,-0.47946817E-03,-0.72364914E-04,
        -0.59556513E-03, 0.39402061E-03,-0.15314172E-02,-0.16968715E-03,
         0.95464129E-04,-0.27356485E-02,-0.46128952E-02,-0.15945882E-02,
        -0.13897176E-02, 0.21831219E-02, 0.20628932E-02, 0.18181027E-02,
         0.29914596E-03, 0.51195628E-03,-0.99684303E-05, 0.29482604E-04,
         0.15247091E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]        *x33        
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]    *x21            
        +coeff[ 48]                *x51
        +coeff[ 49]*x12        *x41    
        +coeff[ 50]*x11        *x41*x51
        +coeff[ 51]*x11    *x31    *x51
        +coeff[ 52]            *x43*x51
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 53]*x11*x21    *x41*x51
        +coeff[ 54]        *x33*x42    
        +coeff[ 55]*x11*x21    *x43    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23    *x43    
        +coeff[ 60]    *x23*x31*x42    
        +coeff[ 61]    *x23*x32*x41    
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 62]    *x24    *x41*x51
        +coeff[ 63]    *x22*x31*x44    
        +coeff[ 64]    *x22*x32*x43    
        +coeff[ 65]    *x24    *x45    
        +coeff[ 66]    *x22            
        +coeff[ 67]*x12    *x31        
        +coeff[ 68]*x11*x22*x31        
        +coeff[ 69]        *x34*x41    
        +coeff[ 70]        *x31    *x53
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 71]    *x22    *x44    
        +coeff[ 72]    *x21*x31*x44    
        +coeff[ 73]*x11*x23*x31        
        +coeff[ 74]    *x21*x32*x43    
        +coeff[ 75]*x11*x22    *x41*x51
        +coeff[ 76]    *x21*x33*x42    
        +coeff[ 77]            *x42*x53
        +coeff[ 78]    *x22    *x43*x51
        +coeff[ 79]*x11*x21    *x41*x52
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 80]    *x22*x31*x42*x51
        +coeff[ 81]    *x23*x33        
        +coeff[ 82]    *x22    *x45    
        +coeff[ 83]    *x24*x31    *x51
        +coeff[ 84]    *x21*x31*x44*x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x33*x42    
        +coeff[ 87]    *x24*x32*x41    
        +coeff[ 88]    *x22*x34*x41    
    ;
    v_y_e_dq_500                                  =v_y_e_dq_500                                  
        +coeff[ 89]    *x23    *x45    
        +coeff[ 90]    *x23*x31*x44    
        +coeff[ 91]    *x23*x32*x43    
        +coeff[ 92]    *x21*x34*x43    
        +coeff[ 93]    *x23*x34*x41    
        +coeff[ 94]    *x23            
        +coeff[ 95]*x12*x21    *x41    
        +coeff[ 96]*x12    *x31*x41    
        ;

    return v_y_e_dq_500                                  ;
}
float p_e_dq_500                                  (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.7458325E-04;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.77641424E-04,-0.25044279E-01, 0.10993516E-01,-0.52883783E-02,
        -0.14724711E-01, 0.47384026E-02, 0.11711095E-01,-0.86822575E-02,
        -0.25956370E-02,-0.50399627E-03,-0.25174562E-02, 0.50701952E-03,
        -0.21513454E-02,-0.13487947E-02,-0.36203954E-03, 0.18640714E-02,
        -0.90816745E-03, 0.83666040E-04, 0.24811263E-03,-0.29269559E-03,
        -0.13634670E-02, 0.53157745E-03,-0.10388395E-02,-0.95306721E-03,
         0.85884251E-03, 0.11364359E-03, 0.55300724E-03,-0.27056760E-03,
        -0.17108634E-02,-0.98049373E-03,-0.25353476E-03, 0.29785302E-03,
         0.13905716E-03,-0.17644178E-03, 0.91677459E-04,-0.41507746E-03,
        -0.18153575E-03, 0.45326637E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_500                                  =v_p_e_dq_500                                  
        +coeff[  8]    *x21    *x41*x51
        +coeff[  9]    *x24*x31        
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_dq_500                                  =v_p_e_dq_500                                  
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_500                                  =v_p_e_dq_500                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x31    *x52
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x22    *x43    
        +coeff[ 30]            *x41*x52
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]        *x31*x42*x51
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21    *x41*x52
    ;
    v_p_e_dq_500                                  =v_p_e_dq_500                                  
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_500                                  ;
}
float l_e_dq_500                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.1836904E-01;
    float xmin[10]={
        -0.39997E-02,-0.60031E-01,-0.53987E-01,-0.30021E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60035E-01, 0.53992E-01, 0.30003E-01, 0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.18287217E-01,-0.31336775E+00,-0.25464682E-01, 0.12375498E-01,
        -0.28918123E-01,-0.16897177E-01,-0.53171259E-02,-0.53196745E-02,
         0.20834289E-02, 0.83395271E-02, 0.82693333E-02,-0.22813224E-02,
         0.40030680E-03, 0.14109320E-02,-0.29125637E-02,-0.83095761E-03,
         0.35771641E-02,-0.16640800E-03, 0.14197457E-02, 0.90924592E-03,
        -0.13677808E-03, 0.60175615E-03, 0.82084158E-03, 0.61686826E-03,
        -0.29638366E-03, 0.72247596E-02, 0.59640356E-02,-0.10086772E-02,
         0.29459740E-02,-0.93660457E-03, 0.92076341E-03, 0.12497341E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_500                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_500                                  =v_l_e_dq_500                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22        *x51
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_500                                  =v_l_e_dq_500                                  
        +coeff[ 17]    *x23            
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]                *x52
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]            *x43*x51
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dq_500                                  =v_l_e_dq_500                                  
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]        *x33*x42    
        +coeff[ 28]    *x21*x31*x43    
        +coeff[ 29]*x12    *x32*x41    
        +coeff[ 30]    *x24*x32        
        +coeff[ 31]    *x22    *x44    
        ;

    return v_l_e_dq_500                                  ;
}
float x_e_dq_450                                  (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.5066682E+00;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.63720900E-02,-0.94215907E-02, 0.95894873E-01, 0.19233084E+00,
         0.24780976E-01, 0.14920502E-01,-0.32769414E-02,-0.31416095E-02,
        -0.10067970E-02,-0.17261341E-02, 0.18463210E-02,-0.76623741E-02,
        -0.78039295E-02,-0.14464773E-04,-0.11114979E-02,-0.79228438E-03,
         0.39009107E-03,-0.32028626E-02,-0.56018226E-03,-0.20952156E-03,
        -0.36254214E-03,-0.84944803E-03,-0.59534381E-02,-0.37308882E-02,
        -0.26987449E-02,-0.36998393E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_450                                  =v_x_e_dq_450                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]        *x31*x41    
        +coeff[ 10]    *x22        *x51
        +coeff[ 11]    *x21*x31*x41    
        +coeff[ 12]    *x21    *x42    
        +coeff[ 13]*x12*x21*x32        
        +coeff[ 14]    *x23*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]    *x23            
    ;
    v_x_e_dq_450                                  =v_x_e_dq_450                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x42*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x32*x42    
        ;

    return v_x_e_dq_450                                  ;
}
float t_e_dq_450                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5712786E+00;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.49139084E-02, 0.35661943E-02,-0.10230858E+00, 0.24329897E-01,
        -0.75639775E-02,-0.23981926E-02,-0.10123386E-02,-0.93212770E-03,
         0.25301625E-02, 0.44412958E-03, 0.11069435E-02, 0.10404168E-02,
         0.33192575E-03, 0.42056909E-03, 0.12820016E-03,-0.16282708E-03,
         0.67045266E-03, 0.75475960E-04, 0.47646448E-03, 0.79539634E-03,
        -0.52486762E-03, 0.15271484E-03, 0.21325168E-02, 0.13251336E-03,
         0.30702763E-03,-0.18005603E-03, 0.37148560E-03, 0.22022788E-03,
         0.26061633E-02,-0.51297699E-04,-0.49763662E-03,-0.56666107E-03,
        -0.14473101E-03, 0.20505500E-02, 0.18912512E-02,-0.15961392E-03,
         0.10307239E-03, 0.38109493E-03,-0.37490518E-03,-0.20909557E-03,
         0.75902200E-04, 0.44059248E-04, 0.32807417E-04, 0.62766878E-04,
         0.44399669E-04,-0.64703636E-04, 0.20515829E-04, 0.13888360E-03,
         0.86257741E-03, 0.13580201E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]        *x31*x41    
        +coeff[  7]                *x52
    ;
    v_t_e_dq_450                                  =v_t_e_dq_450                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]            *x42    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_450                                  =v_t_e_dq_450                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x23            
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_450                                  =v_t_e_dq_450                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]*x11*x21        *x51
        +coeff[ 30]    *x21*x31*x41*x51
        +coeff[ 31]    *x21    *x42*x51
        +coeff[ 32]    *x22        *x52
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_dq_450                                  =v_t_e_dq_450                                  
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]        *x33*x41*x51
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x11        *x42    
        +coeff[ 41]                *x53
        +coeff[ 42]        *x33*x41    
        +coeff[ 43]*x11*x22        *x51
    ;
    v_t_e_dq_450                                  =v_t_e_dq_450                                  
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]*x13    *x31*x41    
        +coeff[ 47]*x11*x22*x31*x41    
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]    *x22*x32    *x51
        ;

    return v_t_e_dq_450                                  ;
}
float y_e_dq_450                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2585872E-03;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.35823215E-03, 0.10878279E+00,-0.81616215E-01,-0.65746211E-01,
        -0.22450356E-01, 0.48094444E-01, 0.25745152E-01,-0.31660091E-01,
        -0.13026728E-01,-0.72099282E-02, 0.24058339E-02,-0.96055837E-02,
        -0.22560798E-02,-0.30966834E-02,-0.21519042E-03,-0.57253905E-03,
         0.85455942E-03,-0.55904780E-02,-0.11754280E-02, 0.15960347E-02,
        -0.17728664E-02, 0.53640087E-02,-0.55221077E-02, 0.50789595E-03,
         0.19247921E-02,-0.19326700E-02,-0.11493267E-02, 0.20771178E-02,
         0.38033589E-02, 0.18575680E-02,-0.42330604E-02,-0.49061757E-02,
        -0.31966849E-02, 0.52688672E-04,-0.41924533E-04, 0.11850004E-02,
         0.50147757E-03,-0.35052950E-03, 0.76809758E-03, 0.34948770E-03,
        -0.17359923E-02,-0.15140508E-02,-0.79938467E-03, 0.12759516E-02,
        -0.64936607E-05,-0.47629750E-04,-0.43104819E-03, 0.17616696E-03,
         0.49436372E-03, 0.28627033E-04,-0.58394380E-04, 0.63484861E-03,
        -0.62770196E-04,-0.46096341E-03, 0.43793934E-03,-0.12390905E-03,
         0.97126045E-04, 0.73987758E-03, 0.35257370E-03,-0.73601732E-05,
         0.78967941E-03,-0.62033167E-03,-0.86408174E-02,-0.86406432E-02,
         0.24341813E-02, 0.63735880E-02,-0.23772544E-03, 0.67126541E-02,
         0.34075265E-02,-0.20266352E-02, 0.16691951E-04, 0.29169347E-04,
        -0.26851472E-04,-0.31908883E-04,-0.22174368E-04,-0.52526389E-06,
         0.26395732E-04,-0.32200005E-04, 0.54363587E-04,-0.85179236E-04,
         0.82910170E-04, 0.61310857E-04,-0.63495903E-03, 0.28362594E-03,
         0.88616944E-04,-0.78075129E-03,-0.17176063E-02,-0.29297429E-03,
         0.35265094E-03,-0.23293805E-03,-0.28278597E-02,-0.44803284E-02,
         0.10174887E-03, 0.16099140E-03,-0.16669641E-02,-0.11115728E-02,
        -0.33581827E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x21            
        +coeff[ 34]                *x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x21    *x41*x52
        +coeff[ 40]    *x24    *x41    
        +coeff[ 41]    *x24*x31        
        +coeff[ 42]    *x22*x33        
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 44]            *x45*x51
        +coeff[ 45]*x11        *x41*x51
        +coeff[ 46]        *x31*x44    
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x12        *x41    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x23*x31*x42    
        +coeff[ 60]    *x23*x32*x41    
        +coeff[ 61]    *x24    *x41*x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 62]    *x22*x31*x44    
        +coeff[ 63]    *x22*x32*x43    
        +coeff[ 64]    *x23    *x45    
        +coeff[ 65]    *x23*x31*x44    
        +coeff[ 66]    *x21*x33*x44    
        +coeff[ 67]    *x23*x32*x43    
        +coeff[ 68]    *x23*x33*x42    
        +coeff[ 69]    *x24    *x45    
        +coeff[ 70]        *x31*x41    
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 71]    *x22    *x42    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x11*x21    *x42    
        +coeff[ 75]*x11    *x31*x42    
        +coeff[ 76]    *x21    *x42*x51
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]        *x31    *x53
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 80]        *x31*x44*x51
        +coeff[ 81]    *x22*x31    *x52
        +coeff[ 82]    *x22    *x43*x51
        +coeff[ 83]    *x21*x34*x41    
        +coeff[ 84]    *x21    *x41*x53
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x22    *x45    
        +coeff[ 87]    *x22*x32*x41*x51
        +coeff[ 88]        *x33*x44    
    ;
    v_y_e_dq_450                                  =v_y_e_dq_450                                  
        +coeff[ 89]    *x24*x31    *x51
        +coeff[ 90]    *x24*x31*x42    
        +coeff[ 91]    *x22*x33*x42    
        +coeff[ 92]*x11    *x31*x42*x52
        +coeff[ 93]*x11*x23    *x43    
        +coeff[ 94]    *x24*x32*x41    
        +coeff[ 95]    *x22*x34*x41    
        +coeff[ 96]    *x24*x33        
        ;

    return v_y_e_dq_450                                  ;
}
float p_e_dq_450                                  (float *x,int m){
    int ncoeff= 43;
    float avdat=  0.8576920E-04;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 44]={
        -0.93436756E-04,-0.25183817E-01, 0.10731449E-01,-0.14828135E-01,
         0.47303825E-02, 0.11700424E-01,-0.86426046E-02,-0.25790690E-02,
         0.25100319E-03, 0.23288336E-04,-0.52242272E-03,-0.23016323E-03,
        -0.53565712E-02,-0.24847626E-02, 0.50824659E-03,-0.21387436E-02,
        -0.13327305E-02,-0.34537999E-03, 0.24467658E-02,-0.93784131E-03,
         0.72553696E-04, 0.25026238E-03,-0.29624748E-03,-0.13493468E-02,
         0.53020834E-03,-0.10313044E-02,-0.95455686E-03, 0.82740904E-03,
         0.11217739E-03,-0.25353488E-03,-0.24758314E-03, 0.54852717E-03,
         0.29348780E-03, 0.14689929E-03,-0.17378133E-03,-0.85459633E-05,
         0.10451133E-03,-0.17996709E-02,-0.10582702E-02,-0.42908639E-03,
        -0.57486288E-03,-0.17123272E-03, 0.44915150E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21    *x41    
        +coeff[  4]        *x31    *x51
        +coeff[  5]            *x41*x51
        +coeff[  6]    *x22    *x41    
        +coeff[  7]    *x21    *x41*x51
    ;
    v_p_e_dq_450                                  =v_p_e_dq_450                                  
        +coeff[  8]    *x23*x31        
        +coeff[  9]    *x21*x33        
        +coeff[ 10]    *x24*x31        
        +coeff[ 11]    *x25*x31        
        +coeff[ 12]    *x21*x31        
        +coeff[ 13]    *x22*x31        
        +coeff[ 14]*x11        *x41    
        +coeff[ 15]        *x31*x42    
        +coeff[ 16]            *x43    
    ;
    v_p_e_dq_450                                  =v_p_e_dq_450                                  
        +coeff[ 17]    *x21*x31    *x51
        +coeff[ 18]    *x23    *x41    
        +coeff[ 19]    *x22*x32*x41    
        +coeff[ 20]        *x34*x41    
        +coeff[ 21]*x11    *x31        
        +coeff[ 22]        *x33        
        +coeff[ 23]        *x32*x41    
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_p_e_dq_450                                  =v_p_e_dq_450                                  
        +coeff[ 26]    *x22    *x41*x51
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]*x11*x21*x31        
        +coeff[ 29]    *x21*x31    *x52
        +coeff[ 30]            *x41*x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]        *x31*x42*x51
        +coeff[ 34]*x11*x22    *x41    
    ;
    v_p_e_dq_450                                  =v_p_e_dq_450                                  
        +coeff[ 35]    *x24    *x41    
        +coeff[ 36]    *x21    *x41*x52
        +coeff[ 37]    *x22*x31*x42    
        +coeff[ 38]    *x22    *x43    
        +coeff[ 39]    *x23*x31    *x51
        +coeff[ 40]    *x25    *x41    
        +coeff[ 41]    *x22*x31    *x52
        +coeff[ 42]    *x22    *x41*x52
        ;

    return v_p_e_dq_450                                  ;
}
float l_e_dq_450                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2022063E-01;
    float xmin[10]={
        -0.39997E-02,-0.60038E-01,-0.53979E-01,-0.30035E-01,-0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60062E-01, 0.53984E-01, 0.29979E-01, 0.39967E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.19844834E-01,-0.31351551E+00,-0.25494663E-01, 0.12425384E-01,
        -0.28560746E-01,-0.17020850E-01,-0.55464301E-02,-0.52197203E-02,
         0.20329331E-02, 0.83160633E-02, 0.77468888E-02,-0.61517861E-03,
        -0.32634052E-03, 0.25438077E-02,-0.79683342E-03, 0.29714105E-02,
        -0.16598199E-02,-0.20086626E-02,-0.40269445E-03, 0.12042478E-02,
         0.51557063E-03, 0.75271528E-03, 0.80410478E-03,-0.30609625E-03,
         0.82545225E-02, 0.69483998E-02, 0.22826369E-02, 0.90328767E-03,
        -0.16793101E-02, 0.67776064E-02, 0.44528362E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_450                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_450                                  =v_l_e_dq_450                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dq_450                                  =v_l_e_dq_450                                  
        +coeff[ 17]        *x32        
        +coeff[ 18]    *x23            
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11            *x52
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dq_450                                  =v_l_e_dq_450                                  
        +coeff[ 26]    *x21*x31*x43    
        +coeff[ 27]        *x34    *x51
        +coeff[ 28]    *x22*x34    *x51
        +coeff[ 29]    *x21*x32*x42*x52
        +coeff[ 30]    *x21*x31*x43*x52
        ;

    return v_l_e_dq_450                                  ;
}
float x_e_dq_449                                  (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5077705E+00;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.61172135E-02,-0.92914850E-02, 0.95924586E-01, 0.19458921E+00,
         0.24724303E-01, 0.15174313E-01,-0.32760154E-02,-0.32337096E-02,
        -0.89070434E-02,-0.82039023E-02,-0.99096552E-03,-0.19674599E-02,
         0.18636952E-02,-0.45318808E-02,-0.97742095E-03,-0.58937620E-03,
        -0.39312593E-03,-0.89064083E-03,-0.28265885E-03,-0.53224885E-02,
        -0.28133343E-02,-0.29706182E-02,-0.40686466E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_449                                  =v_x_e_dq_449                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_449                                  =v_x_e_dq_449                                  
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23        *x52
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_dq_449                                  ;
}
float t_e_dq_449                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5714681E+00;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.48495997E-02, 0.35169784E-02,-0.10328302E+00, 0.24301754E-01,
        -0.74881930E-02,-0.24315014E-02,-0.95262344E-03,-0.11500751E-02,
         0.33962894E-02, 0.10767815E-02, 0.10445885E-02, 0.29709272E-03,
         0.50330465E-03, 0.46402233E-03, 0.13018478E-03,-0.15347620E-03,
         0.96982002E-03, 0.46717923E-03, 0.86336758E-03,-0.55508094E-03,
         0.15201750E-03, 0.20643044E-02, 0.11456669E-03, 0.33714491E-03,
         0.10836155E-03,-0.15844402E-03, 0.44639249E-03, 0.29761449E-03,
        -0.55290398E-03,-0.53595105E-03, 0.26822616E-02, 0.74781412E-04,
        -0.51860046E-04,-0.16447864E-03, 0.12792581E-02, 0.13103053E-02,
        -0.26691117E-03,-0.74909854E-04, 0.40682990E-03,-0.37491039E-03,
        -0.27639134E-03, 0.59335773E-04, 0.13475017E-03, 0.45682060E-04,
         0.54646487E-04, 0.93166396E-04, 0.46316636E-04, 0.84780426E-04,
         0.76455122E-04,-0.66855748E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_449                                  =v_t_e_dq_449                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_449                                  =v_t_e_dq_449                                  
        +coeff[ 17]    *x21        *x52
        +coeff[ 18]    *x22    *x42    
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x23            
        +coeff[ 24]    *x22        *x51
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_449                                  =v_t_e_dq_449                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]    *x22        *x52
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_dq_449                                  =v_t_e_dq_449                                  
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_449                                  =v_t_e_dq_449                                  
        +coeff[ 44]*x11*x23            
        +coeff[ 45]        *x33*x41    
        +coeff[ 46]*x11*x22        *x51
        +coeff[ 47]        *x32    *x52
        +coeff[ 48]            *x42*x52
        +coeff[ 49]    *x21        *x53
        ;

    return v_t_e_dq_449                                  ;
}
float y_e_dq_449                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9277693E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.92256226E-03, 0.11641754E+00,-0.86426854E-01,-0.66880822E-01,
        -0.25325337E-01, 0.47515959E-01, 0.28136181E-01,-0.32332312E-01,
        -0.14667380E-01,-0.71271043E-02, 0.23870019E-02,-0.10442934E-01,
        -0.24953133E-02,-0.24003471E-03,-0.75001694E-03, 0.94987411E-03,
        -0.67829220E-02,-0.15871018E-02, 0.16273232E-02,-0.30784274E-02,
        -0.19199494E-02, 0.51850574E-02,-0.54686861E-02, 0.56955731E-03,
         0.18670224E-02,-0.19983540E-02,-0.12613602E-02, 0.68661047E-03,
         0.15898731E-02, 0.91953570E-03, 0.33361421E-03,-0.42535309E-02,
        -0.58825342E-02,-0.40757759E-02,-0.45675042E-03, 0.12884865E-02,
        -0.34129078E-03, 0.87530905E-03, 0.33809085E-03,-0.17202680E-02,
        -0.16939895E-02, 0.12731635E-02,-0.66073320E-04,-0.49845211E-03,
         0.23808995E-03,-0.11292124E-02, 0.50389679E-03,-0.51462375E-04,
        -0.42632630E-04, 0.65826578E-03,-0.60779021E-04,-0.51170477E-03,
         0.46909356E-03,-0.19246268E-03, 0.11329195E-03, 0.73745457E-03,
         0.33406966E-03, 0.39769923E-02,-0.16211996E-04, 0.27449734E-02,
        -0.53653924E-03,-0.89044757E-02,-0.10034667E-01, 0.43216904E-03,
        -0.22836712E-02,-0.27543691E-04,-0.36671740E-04, 0.13107372E-02,
         0.54254037E-04, 0.41069635E-02, 0.19250517E-02, 0.51491107E-02,
        -0.82019942E-04, 0.12242273E-03, 0.32419139E-02,-0.46300376E-03,
         0.91277523E-03,-0.62753615E-03, 0.61729609E-03,-0.15243882E-02,
        -0.23215717E-03,-0.32530047E-02,-0.56130379E-02,-0.26208174E-03,
        -0.21700677E-02,-0.13881533E-02, 0.65215289E-06,-0.77623954E-05,
        -0.59803660E-05, 0.44858800E-04, 0.36953086E-05,-0.30986401E-04,
        -0.51874893E-04,-0.24899820E-04,-0.79759055E-04, 0.16605358E-04,
         0.19170091E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x21*x33        
        +coeff[ 31]    *x22    *x43    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]        *x31*x44    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 44]        *x33    *x51
        +coeff[ 45]    *x22*x33        
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]*x12        *x41    
        +coeff[ 48]*x11        *x41*x51
        +coeff[ 49]            *x43*x51
        +coeff[ 50]*x11*x21    *x41*x51
        +coeff[ 51]        *x33*x42    
        +coeff[ 52]*x11*x21    *x43    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 53]        *x34*x41    
        +coeff[ 54]            *x41*x53
        +coeff[ 55]*x11*x21*x31*x42    
        +coeff[ 56]*x11*x21*x32*x41    
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]*x11    *x33    *x51
        +coeff[ 59]    *x23*x32*x41    
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 62]    *x22*x32*x43    
        +coeff[ 63]    *x23    *x45    
        +coeff[ 64]    *x24    *x45    
        +coeff[ 65]*x12    *x31        
        +coeff[ 66]*x11*x22*x31        
        +coeff[ 67]    *x21    *x45    
        +coeff[ 68]        *x31    *x53
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x23    *x43    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 71]    *x21*x32*x43    
        +coeff[ 72]*x11*x22    *x41*x51
        +coeff[ 73]    *x21    *x44*x51
        +coeff[ 74]    *x21*x33*x42    
        +coeff[ 75]    *x22    *x43*x51
        +coeff[ 76]    *x21*x34*x41    
        +coeff[ 77]    *x22*x31*x42*x51
        +coeff[ 78]    *x23*x33        
        +coeff[ 79]    *x22    *x45    
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 80]    *x24*x31    *x51
        +coeff[ 81]    *x24*x31*x42    
        +coeff[ 82]    *x22*x33*x42    
        +coeff[ 83]    *x21*x32*x43*x51
        +coeff[ 84]    *x24*x32*x41    
        +coeff[ 85]    *x22*x34*x41    
        +coeff[ 86]                *x51
        +coeff[ 87]    *x21    *x42    
        +coeff[ 88]                *x52
    ;
    v_y_e_dq_449                                  =v_y_e_dq_449                                  
        +coeff[ 89]            *x42*x51
        +coeff[ 90]            *x44    
        +coeff[ 91]*x11    *x31    *x51
        +coeff[ 92]    *x21    *x42*x51
        +coeff[ 93]*x11*x21*x31*x41    
        +coeff[ 94]            *x44*x51
        +coeff[ 95]*x11*x21*x31    *x51
        +coeff[ 96]    *x21*x31    *x52
        ;

    return v_y_e_dq_449                                  ;
}
float p_e_dq_449                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1359661E-03;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.13397804E-03,-0.27020952E-01, 0.12425949E-01,-0.59709963E-02,
        -0.15007475E-01, 0.52333972E-02, 0.11658788E-01,-0.88961981E-02,
        -0.57995395E-03,-0.26479424E-02,-0.28827530E-02, 0.50215109E-03,
        -0.23714346E-02,-0.13235654E-02,-0.41487999E-03, 0.19450343E-02,
        -0.11246487E-02, 0.78486431E-04, 0.27026178E-03,-0.39722180E-03,
        -0.16183597E-02, 0.53138996E-03,-0.11563562E-02,-0.93026791E-03,
         0.91459788E-03, 0.12827071E-03,-0.28238056E-03,-0.24972676E-03,
         0.53658179E-03, 0.25317469E-03,-0.19457572E-03, 0.11199295E-03,
        -0.18936009E-02,-0.10314778E-02,-0.44192621E-03,-0.19823045E-03,
         0.47281268E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_449                                  =v_p_e_dq_449                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x32*x41    
    ;
    v_p_e_dq_449                                  =v_p_e_dq_449                                  
        +coeff[ 17]        *x34*x41    
        +coeff[ 18]*x11    *x31        
        +coeff[ 19]        *x33        
        +coeff[ 20]        *x32*x41    
        +coeff[ 21]*x11*x21    *x41    
        +coeff[ 22]    *x22*x31    *x51
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_449                                  =v_p_e_dq_449                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x21    *x41*x52
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_449                                  =v_p_e_dq_449                                  
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dq_449                                  ;
}
float l_e_dq_449                                  (float *x,int m){
    int ncoeff= 28;
    float avdat= -0.2052000E-01;
    float xmin[10]={
        -0.39990E-02,-0.59852E-01,-0.59980E-01,-0.30023E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60049E-01, 0.59989E-01, 0.30023E-01, 0.39987E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 29]={
         0.19911002E-01,-0.31624359E+00,-0.25543403E-01, 0.12217989E-01,
        -0.29412339E-01,-0.16772551E-01,-0.56649148E-02,-0.52590338E-02,
         0.19737508E-02, 0.10031473E-01, 0.71409428E-02, 0.12695241E-03,
         0.23250339E-03, 0.32954302E-02,-0.31563772E-02,-0.76213287E-03,
        -0.90819650E-03, 0.27959051E-02,-0.18617103E-02, 0.14230054E-02,
         0.70811243E-03, 0.62346441E-03, 0.70938031E-03, 0.72726677E-03,
         0.82073985E-02, 0.60545206E-02, 0.85957423E-02, 0.83078826E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_449                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_449                                  =v_l_e_dq_449                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dq_449                                  =v_l_e_dq_449                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]*x11*x22            
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dq_449                                  =v_l_e_dq_449                                  
        +coeff[ 26]    *x21*x32*x42    
        +coeff[ 27]    *x21*x33*x43    
        ;

    return v_l_e_dq_449                                  ;
}
float x_e_dq_400                                  (float *x,int m){
    int ncoeff= 24;
    float avdat=  0.5094558E+00;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 25]={
        -0.81047975E-02,-0.92685074E-02, 0.95882609E-01, 0.19546072E+00,
         0.24744282E-01, 0.15302732E-01,-0.32847640E-02,-0.31904755E-02,
        -0.90372823E-02,-0.81059877E-02,-0.10129311E-02,-0.19283721E-02,
         0.18714324E-02,-0.45659910E-02,-0.98875945E-03,-0.61585807E-03,
        -0.21148809E-03,-0.40864723E-03, 0.55636906E-06,-0.94322354E-03,
        -0.51843119E-02,-0.28880450E-02,-0.27872487E-02,-0.41359495E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_400                                  =v_x_e_dq_400                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_400                                  =v_x_e_dq_400                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x23            
        +coeff[ 19]    *x22*x31*x41    
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x21*x31*x43    
        +coeff[ 23]    *x23*x32*x42    
        ;

    return v_x_e_dq_400                                  ;
}
float t_e_dq_400                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5700075E+00;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.64663449E-02, 0.35064074E-02,-0.10369031E+00, 0.24311420E-01,
        -0.74635549E-02,-0.24713106E-02,-0.92384842E-03,-0.11773543E-02,
         0.34237087E-02, 0.10486369E-02, 0.97533828E-03, 0.29599294E-03,
         0.52024296E-03, 0.47741021E-03, 0.13768245E-03,-0.15586703E-03,
         0.10027837E-02, 0.11259574E-03, 0.48526772E-03,-0.55730384E-03,
         0.14532602E-03, 0.20786724E-02, 0.34509922E-03,-0.16598243E-03,
         0.49101299E-03, 0.83776523E-03,-0.57684310E-03,-0.54444227E-03,
        -0.17628686E-03, 0.26342233E-02, 0.11291519E-03, 0.31649548E-03,
        -0.10689903E-03, 0.12580151E-02, 0.12213426E-02,-0.37116106E-03,
        -0.33413638E-04, 0.32320945E-03,-0.40815951E-03,-0.27121927E-03,
         0.75628544E-04,-0.46033845E-04, 0.13556937E-03, 0.43705786E-04,
         0.78754121E-04, 0.52442847E-04, 0.12580212E-03, 0.53195479E-04,
         0.52164054E-04, 0.59513321E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_400                                  =v_t_e_dq_400                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_400                                  =v_t_e_dq_400                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x23        *x51
        +coeff[ 20]    *x21*x32    *x51
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]    *x23            
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]    *x22*x32        
        +coeff[ 25]    *x22    *x42    
    ;
    v_t_e_dq_400                                  =v_t_e_dq_400                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x22        *x52
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]*x11*x22            
        +coeff[ 31]    *x22*x31*x41    
        +coeff[ 32]    *x21        *x53
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_dq_400                                  =v_t_e_dq_400                                  
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]        *x33*x41*x51
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x11        *x42    
        +coeff[ 41]*x11*x21        *x51
        +coeff[ 42]        *x31*x41*x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_400                                  =v_t_e_dq_400                                  
        +coeff[ 44]*x11*x23            
        +coeff[ 45]*x11*x21*x31*x41    
        +coeff[ 46]        *x33*x41    
        +coeff[ 47]*x11*x22        *x51
        +coeff[ 48]        *x32    *x52
        +coeff[ 49]*x13    *x31*x41    
        ;

    return v_t_e_dq_400                                  ;
}
float y_e_dq_400                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1128586E-02;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.99905836E-03, 0.11596674E+00,-0.86821489E-01,-0.66908985E-01,
        -0.25314419E-01, 0.47485977E-01, 0.28128810E-01,-0.32932248E-01,
        -0.14783503E-01,-0.71236561E-02, 0.23850258E-02,-0.10790952E-01,
        -0.24860129E-02,-0.30825203E-02, 0.18998573E-04,-0.24194147E-03,
         0.94584364E-03,-0.69658458E-02,-0.16080360E-02, 0.16319802E-02,
        -0.19307393E-02, 0.51984587E-02,-0.57104165E-02, 0.57504978E-03,
         0.19157156E-02,-0.20072740E-02,-0.13412175E-02, 0.16778088E-02,
        -0.47811735E-02, 0.17559025E-02, 0.25479840E-02,-0.35620655E-02,
        -0.12214946E-01,-0.24315289E-02,-0.46609755E-03, 0.43036973E-04,
         0.41614275E-03, 0.88581810E-03, 0.12135057E-02, 0.39077844E-03,
        -0.36515261E-03, 0.87692391E-03,-0.22069311E-02, 0.34936229E-03,
        -0.32236064E-02,-0.17067661E-02, 0.13025526E-02, 0.11313505E-04,
        -0.24274101E-02,-0.28814757E-04,-0.52586006E-06, 0.23222115E-03,
        -0.12890372E-02,-0.10820535E-02, 0.53188077E-03,-0.10498089E-01,
        -0.71076415E-02, 0.23680834E-04,-0.50332150E-04,-0.39867748E-04,
         0.57696702E-03,-0.47493642E-04, 0.14000098E-03, 0.72898273E-03,
         0.38829197E-02,-0.59499172E-03, 0.33856013E-04,-0.37361176E-02,
         0.25662701E-03,-0.21184201E-02,-0.39243381E-03, 0.39331758E-03,
        -0.26220067E-04,-0.35344983E-04,-0.46062913E-04, 0.58369424E-04,
         0.33292500E-03, 0.42884718E-02, 0.25387760E-02, 0.54030274E-02,
        -0.10200271E-03, 0.12395896E-03, 0.30914890E-02, 0.65589142E-04,
        -0.40394205E-03, 0.10191724E-02,-0.49436768E-03, 0.55246463E-03,
        -0.17843950E-03,-0.12671886E-03, 0.11489049E-04, 0.91749434E-05,
         0.36060788E-06,-0.48771995E-04, 0.21646736E-04, 0.26490205E-04,
         0.41175511E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 35]    *x21            
        +coeff[ 36]    *x21    *x43    
        +coeff[ 37]    *x21*x32*x41    
        +coeff[ 38]        *x31*x42*x51
        +coeff[ 39]    *x21*x33        
        +coeff[ 40]*x11*x22    *x41    
        +coeff[ 41]        *x32*x41*x51
        +coeff[ 42]    *x22    *x43    
        +coeff[ 43]    *x21    *x41*x52
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 44]    *x22*x32*x41    
        +coeff[ 45]    *x24*x31        
        +coeff[ 46]    *x23    *x41*x51
        +coeff[ 47]            *x45*x51
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]                *x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x22    *x41*x52
        +coeff[ 55]    *x22*x31*x44    
        +coeff[ 56]    *x22*x33*x42    
        +coeff[ 57]    *x22            
        +coeff[ 58]*x12        *x41    
        +coeff[ 59]*x11        *x41*x51
        +coeff[ 60]            *x43*x51
        +coeff[ 61]*x11*x21    *x41*x51
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 62]            *x41*x53
        +coeff[ 63]*x11*x21*x31*x42    
        +coeff[ 64]    *x23*x31*x42    
        +coeff[ 65]    *x24    *x41*x51
        +coeff[ 66]*x11*x21    *x45    
        +coeff[ 67]    *x24*x31*x42    
        +coeff[ 68]*x11*x21*x32*x43    
        +coeff[ 69]    *x22*x34*x41    
        +coeff[ 70]    *x23    *x45    
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 71]*x11    *x33*x44    
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11    *x31    *x51
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]        *x31    *x53
        +coeff[ 76]*x11*x21*x32*x41    
        +coeff[ 77]    *x21*x31*x44    
        +coeff[ 78]    *x23    *x43    
        +coeff[ 79]    *x21*x32*x43    
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 80]*x11*x22    *x41*x51
        +coeff[ 81]        *x31*x44*x51
        +coeff[ 82]    *x21*x33*x42    
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]    *x22    *x43*x51
        +coeff[ 85]    *x21*x34*x41    
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_dq_400                                  =v_y_e_dq_400                                  
        +coeff[ 89]    *x21*x34*x41*x51
        +coeff[ 90]        *x31*x41    
        +coeff[ 91]    *x21    *x42    
        +coeff[ 92]            *x44    
        +coeff[ 93]*x11    *x31*x42    
        +coeff[ 94]            *x42*x52
        +coeff[ 95]*x11*x21*x31    *x51
        +coeff[ 96]*x11*x21    *x43    
        ;

    return v_y_e_dq_400                                  ;
}
float p_e_dq_400                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2165158E-03;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.19845113E-03,-0.27104067E-01, 0.12327788E-01,-0.59714066E-02,
        -0.15018119E-01, 0.52264375E-02, 0.11648363E-01,-0.89418348E-02,
        -0.55994903E-03,-0.26455834E-02,-0.28935564E-02, 0.49937039E-03,
        -0.23639856E-02,-0.13134857E-02,-0.40062988E-03, 0.19754667E-02,
        -0.11723291E-02,-0.10834388E-02, 0.78905185E-04, 0.26910059E-03,
        -0.38873332E-03,-0.16110474E-02, 0.53923944E-03,-0.94347977E-03,
         0.92308660E-03, 0.12971358E-03,-0.28796506E-03,-0.25254063E-03,
         0.56515407E-03, 0.26019267E-03,-0.19150936E-03,-0.21100135E-04,
         0.11008851E-03,-0.18946113E-02,-0.10204060E-02,-0.46155645E-03,
        -0.21601250E-03, 0.47400690E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_400                                  =v_p_e_dq_400                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_400                                  =v_p_e_dq_400                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_400                                  =v_p_e_dq_400                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x24    *x41    
        +coeff[ 32]    *x21    *x41*x52
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dq_400                                  =v_p_e_dq_400                                  
        +coeff[ 35]    *x23*x31    *x51
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_400                                  ;
}
float l_e_dq_400                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.2424343E-01;
    float xmin[10]={
        -0.39994E-02,-0.60022E-01,-0.59949E-01,-0.30035E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.40000E-02, 0.60030E-01, 0.59993E-01, 0.29983E-01, 0.39981E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.24232177E-01,-0.31799027E+00,-0.25291773E-01, 0.12070457E-01,
        -0.29990761E-01,-0.16631762E-01,-0.62910113E-02,-0.55653304E-02,
         0.20637317E-02, 0.81230160E-02, 0.94168140E-02,-0.10866986E-02,
         0.30836163E-04, 0.49413875E-03,-0.30313779E-02,-0.89585636E-03,
         0.42570219E-02,-0.22608563E-02, 0.13091557E-02, 0.50222193E-03,
         0.84740878E-03,-0.17952235E-03, 0.27043227E-03, 0.54147642E-03,
         0.33720597E-03, 0.25177731E-02, 0.14300161E-02, 0.97902725E-02,
         0.18652001E-02, 0.66522122E-02, 0.26957281E-02, 0.14764476E-02,
         0.91632467E-03, 0.72662411E-02, 0.33005974E-02,-0.26082350E-02,
         0.14481988E-02,-0.12584400E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_400                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_400                                  =v_l_e_dq_400                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_400                                  =v_l_e_dq_400                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]                *x52
        +coeff[ 22]    *x23            
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11            *x52
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dq_400                                  =v_l_e_dq_400                                  
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        +coeff[ 29]    *x21*x31*x43    
        +coeff[ 30]    *x24*x32        
        +coeff[ 31]    *x22*x32*x42    
        +coeff[ 32]*x11    *x32    *x53
        +coeff[ 33]    *x23*x32*x42    
        +coeff[ 34]    *x23    *x44    
    ;
    v_l_e_dq_400                                  =v_l_e_dq_400                                  
        +coeff[ 35]    *x23*x31*x41*x52
        +coeff[ 36]    *x22    *x42*x53
        +coeff[ 37]*x11*x23*x32    *x51
        ;

    return v_l_e_dq_400                                  ;
}
float x_e_dq_350                                  (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5082250E+00;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.64045298E-02,-0.92480974E-02, 0.95953681E-01, 0.19590780E+00,
         0.24704123E-01, 0.15322908E-01,-0.32849209E-02,-0.32348721E-02,
        -0.89077884E-02,-0.81837783E-02,-0.10095042E-02,-0.19536915E-02,
         0.18541989E-02,-0.45225169E-02,-0.99123933E-03,-0.60073764E-03,
        -0.39035143E-03,-0.95950678E-03,-0.26711842E-03,-0.51660365E-02,
        -0.26006368E-02,-0.28590991E-02,-0.43001743E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_350                                  =v_x_e_dq_350                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_350                                  =v_x_e_dq_350                                  
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23        *x52
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_dq_350                                  ;
}
float t_e_dq_350                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5706955E+00;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55524348E-02, 0.34982148E-02,-0.10379944E+00, 0.24301365E-01,
        -0.74606515E-02,-0.24971419E-02,-0.95572969E-03,-0.11317022E-02,
         0.26383859E-02, 0.93339628E-03, 0.12801923E-02, 0.32931793E-03,
         0.48391227E-03, 0.45809455E-03, 0.13190681E-03,-0.14582538E-03,
         0.84283273E-03, 0.11282575E-03, 0.46250163E-03, 0.92390564E-03,
        -0.55603194E-03, 0.12673567E-03, 0.21942100E-02, 0.25504030E-03,
        -0.17117398E-03, 0.49622374E-03, 0.31044567E-03,-0.58838143E-03,
        -0.59629494E-03,-0.16189407E-03, 0.29453430E-02, 0.10624457E-03,
         0.81186990E-04,-0.44242752E-04, 0.25458285E-02, 0.21848711E-02,
        -0.29557839E-03, 0.98903411E-06, 0.39032663E-03,-0.37169212E-03,
        -0.24663543E-03,-0.18165003E-04, 0.68268113E-04, 0.94479161E-04,
         0.42483320E-04, 0.52762571E-04, 0.82003251E-04, 0.91246162E-04,
        -0.52734671E-04, 0.12582471E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_350                                  =v_t_e_dq_350                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_350                                  =v_t_e_dq_350                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_dq_350                                  =v_t_e_dq_350                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]*x11*x22            
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_dq_350                                  =v_t_e_dq_350                                  
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]        *x33*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]    *x22    *x41    
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]        *x31*x41*x51
    ;
    v_t_e_dq_350                                  =v_t_e_dq_350                                  
        +coeff[ 44]                *x53
        +coeff[ 45]        *x33*x41    
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dq_350                                  ;
}
float y_e_dq_350                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2382125E-04;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.88828026E-04, 0.11502455E+00,-0.87513074E-01,-0.67021511E-01,
        -0.25296466E-01, 0.47454052E-01, 0.28111847E-01,-0.32846417E-01,
        -0.14724278E-01,-0.71962210E-02, 0.23774423E-02,-0.10505233E-01,
        -0.69181952E-02,-0.24879323E-02,-0.41029853E-05, 0.93805406E-03,
        -0.15851192E-02, 0.16382026E-02,-0.30725522E-02,-0.19332521E-02,
         0.52183038E-02,-0.56566657E-02, 0.56060904E-03, 0.18907409E-02,
        -0.19908510E-02,-0.13098278E-02, 0.16116144E-02, 0.90412342E-03,
        -0.48796348E-02, 0.14264509E-02,-0.35071576E-02,-0.66664354E-02,
        -0.24404696E-02,-0.47363885E-03, 0.63051964E-03, 0.13082903E-02,
         0.37098370E-03,-0.36305873E-03, 0.87542163E-03,-0.21897517E-02,
         0.34590653E-03,-0.33494520E-02,-0.16968515E-02, 0.13519869E-02,
        -0.39640028E-04,-0.24506242E-02,-0.41329968E-04, 0.23099355E-03,
        -0.13120313E-02,-0.10876670E-02, 0.53089729E-03, 0.15131041E-04,
        -0.48387035E-04,-0.34780373E-04, 0.62539231E-03,-0.28143881E-03,
        -0.43906205E-03,-0.71653565E-04,-0.34633608E-03, 0.10555761E-03,
         0.76099002E-03, 0.21632698E-02, 0.38595286E-02, 0.26071286E-02,
        -0.54747314E-03,-0.10203108E-01, 0.29798026E-04,-0.11758055E-01,
        -0.38180803E-02,-0.19749063E-02, 0.44972938E-04,-0.75345401E-05,
        -0.18104600E-04,-0.34709436E-04,-0.38908849E-04, 0.31585252E-03,
         0.57949001E-04, 0.31231556E-03, 0.40659122E-02, 0.50235949E-02,
        -0.98204575E-04, 0.31223588E-02, 0.52358813E-04,-0.47704959E-03,
         0.95720735E-03, 0.10227229E-03,-0.60686411E-03, 0.53464447E-03,
        -0.19575725E-03, 0.24758166E-03,-0.17756240E-03,-0.10409787E-03,
         0.41600492E-05, 0.14473053E-04, 0.17114458E-04,-0.16817570E-04,
        -0.23542747E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]        *x32*x41    
        +coeff[ 13]    *x21*x31    *x51
        +coeff[ 14]            *x45    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x33        
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 17]*x11*x21    *x41    
        +coeff[ 18]            *x41*x52
        +coeff[ 19]        *x31    *x52
        +coeff[ 20]    *x23    *x41    
        +coeff[ 21]            *x43    
        +coeff[ 22]*x11*x21*x31        
        +coeff[ 23]    *x23*x31        
        +coeff[ 24]    *x22    *x41*x51
        +coeff[ 25]    *x22*x31    *x51
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 26]    *x21*x31*x42    
        +coeff[ 27]    *x21*x32*x41    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x22    *x45    
        +coeff[ 31]    *x22*x33*x42    
        +coeff[ 32]    *x24*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]    *x21    *x43    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 35]        *x31*x42*x51
        +coeff[ 36]    *x21*x33        
        +coeff[ 37]*x11*x22    *x41    
        +coeff[ 38]        *x32*x41*x51
        +coeff[ 39]    *x22    *x43    
        +coeff[ 40]    *x21    *x41*x52
        +coeff[ 41]    *x22*x32*x41    
        +coeff[ 42]    *x24*x31        
        +coeff[ 43]    *x23    *x41*x51
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 44]            *x45*x51
        +coeff[ 45]    *x24    *x43    
        +coeff[ 46]*x11        *x41*x51
        +coeff[ 47]        *x33    *x51
        +coeff[ 48]    *x24    *x41    
        +coeff[ 49]    *x22*x33        
        +coeff[ 50]    *x22    *x41*x52
        +coeff[ 51]    *x21            
        +coeff[ 52]*x12        *x41    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 53]*x11    *x31    *x51
        +coeff[ 54]            *x43*x51
        +coeff[ 55]        *x31*x44    
        +coeff[ 56]        *x32*x43    
        +coeff[ 57]*x11*x21    *x41*x51
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]            *x41*x53
        +coeff[ 60]*x11*x21*x31*x42    
        +coeff[ 61]    *x23    *x43    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 62]    *x23*x31*x42    
        +coeff[ 63]    *x23*x32*x41    
        +coeff[ 64]    *x24    *x41*x51
        +coeff[ 65]    *x22*x31*x44    
        +coeff[ 66]*x11*x21    *x45    
        +coeff[ 67]    *x22*x32*x43    
        +coeff[ 68]    *x24*x31*x42    
        +coeff[ 69]    *x22*x34*x41    
        +coeff[ 70]*x11*x23*x32*x41    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 71]                *x51
        +coeff[ 72]*x12    *x31        
        +coeff[ 73]*x11*x21    *x42    
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]*x11*x21    *x43    
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]*x11*x21*x32*x41    
        +coeff[ 78]    *x21*x31*x44    
        +coeff[ 79]    *x21*x32*x43    
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 80]*x11*x22    *x41*x51
        +coeff[ 81]    *x21*x33*x42    
        +coeff[ 82]    *x22*x31    *x52
        +coeff[ 83]    *x22    *x43*x51
        +coeff[ 84]    *x21*x34*x41    
        +coeff[ 85]    *x21    *x41*x53
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_dq_350                                  =v_y_e_dq_350                                  
        +coeff[ 89]*x11*x23    *x43    
        +coeff[ 90]    *x23*x32*x41*x51
        +coeff[ 91]*x11*x23    *x41*x52
        +coeff[ 92]    *x22            
        +coeff[ 93]*x11*x21*x32        
        +coeff[ 94]*x12*x21    *x41    
        +coeff[ 95]*x12    *x31*x41    
        +coeff[ 96]        *x31*x43*x51
        ;

    return v_y_e_dq_350                                  ;
}
float p_e_dq_350                                  (float *x,int m){
    int ncoeff= 38;
    float avdat= -0.1005343E-03;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
         0.89020592E-04,-0.27243953E-01, 0.12154588E-01,-0.59790667E-02,
        -0.15038067E-01, 0.51918533E-02, 0.11628104E-01,-0.89637786E-02,
        -0.58178866E-03,-0.26367037E-02,-0.28732182E-02, 0.49874483E-03,
        -0.23354720E-02,-0.13244813E-02,-0.42228380E-03, 0.19612189E-02,
        -0.11647010E-02,-0.10627985E-02, 0.85553773E-04, 0.26758367E-03,
        -0.39227578E-03,-0.16290087E-02, 0.53338945E-03,-0.89473394E-03,
         0.12403191E-03,-0.28400839E-03,-0.43152273E-03, 0.92427782E-03,
        -0.26077245E-03, 0.53732510E-03, 0.25056233E-03, 0.17321715E-03,
        -0.18675039E-03, 0.10962361E-03,-0.18154087E-02,-0.96094341E-03,
        -0.19878555E-03, 0.50428853E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_350                                  =v_p_e_dq_350                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_350                                  =v_p_e_dq_350                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]*x11*x21*x31        
        +coeff[ 25]    *x21*x31    *x52
    ;
    v_p_e_dq_350                                  =v_p_e_dq_350                                  
        +coeff[ 26]    *x23*x31    *x51
        +coeff[ 27]    *x23    *x41*x51
        +coeff[ 28]            *x41*x52
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]        *x31*x42*x51
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x21    *x41*x52
        +coeff[ 34]    *x22*x31*x42    
    ;
    v_p_e_dq_350                                  =v_p_e_dq_350                                  
        +coeff[ 35]    *x22    *x43    
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_350                                  ;
}
float l_e_dq_350                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2212909E-01;
    float xmin[10]={
        -0.39985E-02,-0.59744E-01,-0.59952E-01,-0.30003E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60029E-01, 0.59990E-01, 0.29983E-01, 0.39993E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.21313988E-01,-0.31781170E+00,-0.25627088E-01, 0.12225556E-01,
        -0.29795766E-01,-0.16609699E-01,-0.61163194E-02,-0.55531319E-02,
         0.20093312E-02, 0.74367882E-02, 0.44533354E-02, 0.54628210E-03,
         0.25793616E-03, 0.39158384E-02,-0.33551806E-02,-0.81115495E-03,
        -0.11295715E-02, 0.21071010E-02,-0.18934354E-02, 0.16086923E-02,
         0.72450307E-03,-0.25284584E-04,-0.15956625E-03, 0.77736820E-03,
         0.45161380E-03, 0.50484663E-03, 0.21218811E-02, 0.15205517E-02,
         0.74788695E-02, 0.75757662E-02, 0.10793461E-01, 0.99825915E-02,
         0.34026066E-02,-0.54504629E-03, 0.56155901E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_350                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_350                                  =v_l_e_dq_350                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dq_350                                  =v_l_e_dq_350                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x31        
        +coeff[ 22]                *x52
        +coeff[ 23]        *x32    *x51
        +coeff[ 24]            *x42*x51
        +coeff[ 25]*x11*x22            
    ;
    v_l_e_dq_350                                  =v_l_e_dq_350                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x22    *x42    
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x32*x42    
        +coeff[ 31]    *x21*x31*x43    
        +coeff[ 32]    *x21    *x44    
        +coeff[ 33]*x12    *x31    *x52
        +coeff[ 34]    *x23*x33*x41    
        ;

    return v_l_e_dq_350                                  ;
}
float x_e_dq_300                                  (float *x,int m){
    int ncoeff= 22;
    float avdat=  0.5083282E+00;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 23]={
        -0.68263151E-02,-0.92177792E-02, 0.95915131E-01, 0.19743690E+00,
         0.24746092E-01, 0.15569585E-01,-0.32692128E-02,-0.31836459E-02,
        -0.11523370E-01,-0.82460521E-02,-0.10164196E-02,-0.19361454E-02,
         0.18576164E-02,-0.45133997E-02,-0.96321496E-03,-0.58308034E-03,
        -0.21802644E-03,-0.36479134E-03,-0.98023517E-03,-0.25553200E-02,
        -0.43735397E-02, 0.26328587E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_300                                  =v_x_e_dq_300                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_300                                  =v_x_e_dq_300                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23    *x42    
        +coeff[ 20]    *x23*x31*x43    
        +coeff[ 21]    *x21*x33*x43    
        ;

    return v_x_e_dq_300                                  ;
}
float t_e_dq_300                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5708829E+00;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.55227159E-02,-0.10451546E+00, 0.24310792E-01,-0.75323759E-02,
         0.35400251E-05, 0.34802340E-02,-0.25459072E-02,-0.93276147E-03,
        -0.11560017E-02, 0.34518505E-02, 0.10842863E-02, 0.10609473E-02,
         0.33317436E-03, 0.49927621E-03, 0.44836197E-03, 0.14796542E-03,
        -0.15822351E-03, 0.93952927E-03, 0.13806547E-03, 0.47337147E-03,
         0.89304015E-03,-0.53749222E-03, 0.18173773E-03, 0.20002734E-02,
         0.29122332E-03,-0.17695155E-03, 0.47157134E-03, 0.29670095E-03,
        -0.54425828E-03, 0.25962596E-02,-0.15684980E-04, 0.29556334E-03,
         0.10752372E-03, 0.47764417E-04,-0.71375171E-03,-0.16462314E-03,
         0.13493266E-02, 0.12775389E-02,-0.37923953E-03,-0.22438524E-04,
         0.33027038E-03,-0.37664574E-03,-0.31808324E-03, 0.61308303E-04,
         0.73989366E-04,-0.45301236E-04, 0.11635122E-03, 0.95146759E-04,
         0.53665899E-04, 0.55153414E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]                *x52
    ;
    v_t_e_dq_300                                  =v_t_e_dq_300                                  
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42    
        +coeff[ 14]            *x42*x51
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dq_300                                  =v_t_e_dq_300                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]    *x23        *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x23            
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_300                                  =v_t_e_dq_300                                  
        +coeff[ 26]    *x22*x32        
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]*x12*x21*x31*x41*x51
        +coeff[ 31]    *x23*x31*x41*x51
        +coeff[ 32]*x11*x22            
        +coeff[ 33]                *x53
        +coeff[ 34]    *x21*x31*x41*x51
    ;
    v_t_e_dq_300                                  =v_t_e_dq_300                                  
        +coeff[ 35]    *x22        *x52
        +coeff[ 36]    *x21*x32*x42    
        +coeff[ 37]    *x21*x31*x43    
        +coeff[ 38]    *x22*x31*x41*x51
        +coeff[ 39]        *x33*x41*x51
        +coeff[ 40]    *x22    *x42*x51
        +coeff[ 41]    *x23        *x52
        +coeff[ 42]    *x21*x31*x41*x52
        +coeff[ 43]*x11    *x31*x41    
    ;
    v_t_e_dq_300                                  =v_t_e_dq_300                                  
        +coeff[ 44]*x11        *x42    
        +coeff[ 45]*x11*x21        *x51
        +coeff[ 46]        *x31*x41*x51
        +coeff[ 47]        *x33*x41    
        +coeff[ 48]*x11*x22        *x51
        +coeff[ 49]        *x32    *x52
        ;

    return v_t_e_dq_300                                  ;
}
float y_e_dq_300                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.3863075E-03;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.41137377E-03, 0.11423654E+00,-0.88322327E-01,-0.67214571E-01,
        -0.25254287E-01, 0.47518436E-01, 0.28102593E-01,-0.33226967E-01,
        -0.14831915E-01,-0.71393535E-02, 0.23644024E-02,-0.10443582E-01,
        -0.24583426E-02,-0.35478693E-04,-0.64468826E-03, 0.93040994E-03,
        -0.67524211E-02,-0.15651743E-02, 0.16603640E-02,-0.30733370E-02,
        -0.19264141E-02, 0.52066687E-02,-0.56052697E-02, 0.59392548E-03,
         0.18453688E-02,-0.22647693E-02,-0.13237350E-02, 0.13492954E-02,
        -0.50459001E-02, 0.15578798E-02, 0.28211791E-02,-0.36471304E-02,
        -0.12044384E-01,-0.24393958E-02,-0.44219923E-03, 0.76616777E-03,
         0.12671497E-02, 0.34247217E-03,-0.36541829E-03, 0.88274095E-03,
        -0.20822359E-02, 0.35728412E-03,-0.35939296E-02,-0.17163322E-02,
         0.12861692E-02, 0.25020861E-02, 0.43141432E-02,-0.28561596E-02,
         0.39228311E-03, 0.55752968E-03,-0.28209129E-03, 0.22126401E-03,
        -0.12499775E-02,-0.11374617E-02,-0.10423217E-03, 0.50094229E-03,
        -0.10675410E-01,-0.64778347E-02,-0.46055045E-03, 0.39605773E-03,
        -0.13473550E-03, 0.10498187E-03, 0.63017308E-03, 0.28510627E-03,
         0.43869051E-02, 0.54273247E-02, 0.32250825E-02, 0.61585812E-03,
        -0.27313229E-03,-0.40212199E-02,-0.50140993E-03,-0.32063760E-03,
        -0.17371576E-04,-0.40087136E-04,-0.27599541E-04,-0.33495202E-04,
        -0.37703423E-04,-0.53715597E-04, 0.62460102E-04,-0.11389967E-03,
         0.10064251E-02,-0.56930369E-03,-0.17433602E-03,-0.17794077E-02,
        -0.77835232E-03, 0.50289113E-05, 0.26672935E-04,-0.16630887E-04,
         0.31814219E-04,-0.51262898E-04,-0.49542636E-04,-0.57672594E-04,
         0.41769785E-04, 0.43767659E-03,-0.53983982E-04, 0.61231287E-04,
         0.14501592E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]    *x23*x31*x42    
        +coeff[ 47]    *x24    *x43    
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]*x11*x22    *x41*x51
        +coeff[ 55]    *x22    *x41*x52
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]    *x22*x33*x42    
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]        *x34*x41    
        +coeff[ 61]            *x41*x53
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x21*x31*x44    
        +coeff[ 65]    *x21*x32*x43    
        +coeff[ 66]    *x21*x33*x42    
        +coeff[ 67]    *x23*x33        
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x24*x31*x42    
        +coeff[ 70]    *x22    *x45*x51
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 71]    *x22*x31*x44*x51
        +coeff[ 72]*x12        *x41    
        +coeff[ 73]*x11        *x41*x51
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11*x22*x31        
        +coeff[ 77]*x11*x21    *x41*x51
        +coeff[ 78]        *x31    *x53
        +coeff[ 79]    *x21*x32*x41*x51
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 80]    *x21*x34*x41    
        +coeff[ 81]    *x22*x31*x42*x51
        +coeff[ 82]    *x24*x31    *x51
        +coeff[ 83]    *x22*x34*x41    
        +coeff[ 84]    *x24    *x43*x51
        +coeff[ 85]                *x51
        +coeff[ 86]*x11*x21*x31    *x51
        +coeff[ 87]*x11        *x41*x52
        +coeff[ 88]    *x22*x31*x41*x51
    ;
    v_y_e_dq_300                                  =v_y_e_dq_300                                  
        +coeff[ 89]*x11*x23*x31        
        +coeff[ 90]    *x23*x31    *x51
        +coeff[ 91]*x12*x22    *x41    
        +coeff[ 92]    *x22*x31    *x52
        +coeff[ 93]    *x22    *x43*x51
        +coeff[ 94]*x11*x21    *x41*x52
        +coeff[ 95]    *x21    *x41*x53
        +coeff[ 96]        *x33*x42*x51
        ;

    return v_y_e_dq_300                                  ;
}
float p_e_dq_300                                  (float *x,int m){
    int ncoeff= 38;
    float avdat=  0.4115192E-04;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 39]={
        -0.47796908E-04,-0.27407728E-01, 0.11981740E-01,-0.59888815E-02,
        -0.15186539E-01, 0.52053705E-02, 0.11626027E-01,-0.90273693E-02,
        -0.64747303E-03,-0.26360685E-02,-0.28426526E-02, 0.49506541E-03,
        -0.23430553E-02,-0.13077767E-02,-0.39688649E-03, 0.25195046E-02,
        -0.11982649E-02,-0.11777183E-02, 0.94169991E-04, 0.26612799E-03,
        -0.38666374E-03,-0.16082464E-02, 0.53556851E-03,-0.94194250E-03,
         0.90222713E-03, 0.12980118E-03,-0.29722298E-03,-0.24674096E-03,
         0.55510300E-03, 0.24600295E-03,-0.18639775E-03, 0.98812321E-04,
        -0.19952569E-02,-0.10642646E-02,-0.46711633E-03,-0.52536541E-03,
        -0.20933550E-03, 0.46503884E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x25 = x24*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_300                                  =v_p_e_dq_300                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_300                                  =v_p_e_dq_300                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_300                                  =v_p_e_dq_300                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x21    *x41*x52
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_300                                  =v_p_e_dq_300                                  
        +coeff[ 35]    *x25    *x41    
        +coeff[ 36]    *x22*x31    *x52
        +coeff[ 37]    *x22    *x41*x52
        ;

    return v_p_e_dq_300                                  ;
}
float l_e_dq_300                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2191173E-01;
    float xmin[10]={
        -0.39996E-02,-0.59965E-01,-0.59968E-01,-0.30019E-01,-0.39992E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39998E-02, 0.60062E-01, 0.59993E-01, 0.30016E-01, 0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.21563129E-01,-0.32083860E+00,-0.25328312E-01, 0.12113852E-01,
        -0.29981246E-01,-0.16254174E-01,-0.56965407E-02,-0.52463491E-02,
         0.20812768E-02, 0.91343429E-02, 0.81511708E-02,-0.18083418E-03,
         0.26954402E-03, 0.21437448E-02,-0.31890089E-02,-0.81530941E-03,
         0.39965948E-02,-0.20852662E-02, 0.16515753E-02, 0.78927417E-03,
         0.84175905E-02, 0.48462213E-02, 0.22476241E-02,-0.18704074E-03,
         0.58957393E-03, 0.12473618E-02, 0.49450994E-03,-0.15922753E-02,
        -0.87911787E-03, 0.65244565E-03, 0.76561607E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_300                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_300                                  =v_l_e_dq_300                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_300                                  =v_l_e_dq_300                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x23*x31*x41    
        +coeff[ 21]    *x23    *x42    
        +coeff[ 22]        *x32*x42*x51
        +coeff[ 23]                *x52
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_dq_300                                  =v_l_e_dq_300                                  
        +coeff[ 26]        *x32    *x53
        +coeff[ 27]*x11    *x31*x43    
        +coeff[ 28]    *x23        *x53
        +coeff[ 29]    *x22*x31    *x53
        +coeff[ 30]*x11    *x34    *x52
        ;

    return v_l_e_dq_300                                  ;
}
float x_e_dq_250                                  (float *x,int m){
    int ncoeff= 21;
    float avdat=  0.5078303E+00;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 22]={
        -0.64427475E-02,-0.91643669E-02, 0.95909759E-01, 0.19914435E+00,
         0.24796421E-01, 0.15779503E-01,-0.32788576E-02,-0.31317854E-02,
        -0.10175260E-01,-0.80658654E-02,-0.10118850E-02,-0.19202325E-02,
         0.19152850E-02,-0.46427739E-02,-0.98315894E-03,-0.57122088E-03,
        -0.21020013E-03,-0.39286443E-03,-0.83493255E-03,-0.34659170E-02,
        -0.23976355E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_250                                  =v_x_e_dq_250                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_250                                  =v_x_e_dq_250                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        ;

    return v_x_e_dq_250                                  ;
}
float t_e_dq_250                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5709736E+00;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.54949592E-02, 0.34587358E-02,-0.10526710E+00, 0.24316862E-01,
        -0.74701644E-02,-0.25950863E-02,-0.95091329E-03,-0.11168801E-02,
         0.26242277E-02, 0.98922581E-03, 0.12641537E-02, 0.33366025E-03,
         0.46797047E-03, 0.42106397E-03, 0.15268616E-03,-0.14221172E-03,
         0.84064383E-03, 0.11690591E-03, 0.46379439E-03, 0.85454149E-03,
        -0.55562012E-03, 0.16033104E-03, 0.21220038E-02, 0.25454242E-03,
        -0.23128530E-03, 0.45101487E-03,-0.71428914E-03,-0.59485750E-03,
         0.29864667E-02, 0.58628607E-03, 0.10384255E-03, 0.83516024E-04,
        -0.40520168E-04, 0.30240641E-03,-0.15080677E-03, 0.24256343E-02,
         0.21960703E-02, 0.27486696E-03,-0.38538544E-03,-0.28769157E-03,
         0.61370331E-04, 0.45145465E-04, 0.75395743E-04, 0.67574321E-04,
         0.65034692E-04,-0.65890832E-04, 0.12845090E-02,-0.10829091E-03,
         0.30732033E-03, 0.30068963E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_250                                  =v_t_e_dq_250                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_250                                  =v_t_e_dq_250                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_dq_250                                  =v_t_e_dq_250                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x21    *x42*x51
        +coeff[ 28]    *x23*x31*x41    
        +coeff[ 29]    *x22    *x42*x51
        +coeff[ 30]*x11*x22            
        +coeff[ 31]*x11        *x42    
        +coeff[ 32]*x11*x21        *x51
        +coeff[ 33]    *x22*x31*x41    
        +coeff[ 34]    *x22        *x52
    ;
    v_t_e_dq_250                                  =v_t_e_dq_250                                  
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x21*x31*x43    
        +coeff[ 37]    *x22*x32    *x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x11    *x31*x41    
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]                *x53
        +coeff[ 43]        *x32    *x52
    ;
    v_t_e_dq_250                                  =v_t_e_dq_250                                  
        +coeff[ 44]            *x42*x52
        +coeff[ 45]    *x21        *x53
        +coeff[ 46]    *x21*x33*x41    
        +coeff[ 47]    *x22        *x53
        +coeff[ 48]    *x22*x32*x42    
        +coeff[ 49]    *x23*x31*x41*x51
        ;

    return v_t_e_dq_250                                  ;
}
float y_e_dq_250                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.8148319E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.90635690E-03, 0.11280740E+00,-0.89463897E-01,-0.67341782E-01,
        -0.25226777E-01, 0.47460541E-01, 0.28077573E-01,-0.33576198E-01,
        -0.14943999E-01,-0.71226400E-02, 0.23262375E-02,-0.10346072E-01,
        -0.24656628E-02,-0.24522917E-04,-0.67056844E-03, 0.91220607E-03,
        -0.66324593E-02,-0.15675507E-02, 0.16117943E-02,-0.30427787E-02,
        -0.19284398E-02, 0.52315420E-02,-0.55660238E-02, 0.56669931E-03,
         0.18914142E-02,-0.22575315E-02,-0.13488929E-02, 0.12462141E-02,
        -0.51815053E-02, 0.17031308E-02, 0.29072373E-02,-0.34286515E-02,
        -0.11164088E-01,-0.26914147E-02,-0.55396330E-03, 0.60930301E-03,
         0.12566469E-02, 0.33672753E-03,-0.33314564E-03, 0.87269058E-03,
        -0.22250761E-02, 0.34443731E-03,-0.36721956E-02,-0.16964832E-02,
         0.13472924E-02, 0.12397782E-04, 0.27063808E-02,-0.28704749E-02,
        -0.27425966E-03, 0.35501896E-04,-0.24733075E-04, 0.25196941E-03,
        -0.35616072E-03, 0.24568901E-03,-0.12077939E-02,-0.10792604E-02,
        -0.10420037E-03, 0.50731411E-03, 0.41989731E-02,-0.98183183E-02,
        -0.61307354E-02,-0.48078717E-04, 0.54940145E-03,-0.48656281E-03,
         0.46480543E-03,-0.20827749E-03, 0.12456806E-03, 0.69475587E-03,
         0.33073901E-03, 0.45823683E-02, 0.58153109E-02, 0.34875162E-02,
         0.10875063E-02, 0.60796971E-03,-0.25129906E-03,-0.42394758E-02,
        -0.16375502E-02, 0.37481776E-04,-0.39851693E-04,-0.26993615E-04,
        -0.35885758E-04,-0.35203346E-04,-0.59011963E-04, 0.60412272E-04,
        -0.70636284E-04, 0.57510166E-04, 0.10085027E-03,-0.44300029E-03,
        -0.16256461E-03,-0.70294528E-03,-0.12569249E-05, 0.84397807E-05,
         0.12874511E-04,-0.10831500E-04, 0.26284615E-04, 0.34081062E-04,
        -0.22954900E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]            *x45*x51
        +coeff[ 46]    *x23    *x43    
        +coeff[ 47]    *x24    *x43    
        +coeff[ 48]    *x23    *x45    
        +coeff[ 49]    *x21            
        +coeff[ 50]                *x51
        +coeff[ 51]    *x21    *x43    
        +coeff[ 52]        *x31*x44    
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 53]        *x33    *x51
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]    *x22*x33        
        +coeff[ 56]*x11*x22    *x41*x51
        +coeff[ 57]    *x22    *x41*x52
        +coeff[ 58]    *x23*x31*x42    
        +coeff[ 59]    *x22*x31*x44    
        +coeff[ 60]    *x22*x33*x42    
        +coeff[ 61]*x12        *x41    
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 62]            *x43*x51
        +coeff[ 63]        *x33*x42    
        +coeff[ 64]*x11*x21    *x43    
        +coeff[ 65]        *x34*x41    
        +coeff[ 66]            *x41*x53
        +coeff[ 67]*x11*x21*x31*x42    
        +coeff[ 68]*x11*x21*x32*x41    
        +coeff[ 69]    *x21*x31*x44    
        +coeff[ 70]    *x21*x32*x43    
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 71]    *x21*x33*x42    
        +coeff[ 72]    *x21*x34*x41    
        +coeff[ 73]    *x23*x33        
        +coeff[ 74]    *x24    *x41*x51
        +coeff[ 75]    *x24*x31*x42    
        +coeff[ 76]    *x22*x34*x41    
        +coeff[ 77]    *x22            
        +coeff[ 78]*x11        *x41*x51
        +coeff[ 79]*x12    *x31        
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 80]*x11    *x31    *x51
        +coeff[ 81]*x11*x22*x31        
        +coeff[ 82]*x11*x21    *x41*x51
        +coeff[ 83]        *x31    *x53
        +coeff[ 84]        *x32*x41*x52
        +coeff[ 85]    *x22*x31    *x52
        +coeff[ 86]    *x22    *x43*x51
        +coeff[ 87]    *x22*x31*x42*x51
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_dq_250                                  =v_y_e_dq_250                                  
        +coeff[ 89]    *x24    *x43*x51
        +coeff[ 90]*x11                
        +coeff[ 91]            *x42    
        +coeff[ 92]        *x31*x41    
        +coeff[ 93]*x11        *x42    
        +coeff[ 94]        *x32*x42    
        +coeff[ 95]*x11    *x31*x42    
        +coeff[ 96]    *x24            
        ;

    return v_y_e_dq_250                                  ;
}
float p_e_dq_250                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.2834987E-03;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.29403894E-03,-0.27630562E-01, 0.11704636E-01,-0.59914570E-02,
        -0.15129396E-01, 0.51849214E-02, 0.11601993E-01,-0.91441739E-02,
        -0.62536180E-03,-0.26271287E-02,-0.28742165E-02, 0.48923539E-03,
        -0.23337528E-02,-0.12970937E-02,-0.38196810E-03, 0.20248266E-02,
        -0.11978396E-02,-0.11124499E-02, 0.73525443E-04, 0.26383204E-03,
        -0.38919566E-03,-0.15903828E-02, 0.53464965E-03,-0.95231517E-03,
         0.93072309E-03, 0.13119676E-03,-0.29578182E-03,-0.24702240E-03,
         0.54559676E-03, 0.25559097E-03,-0.18910221E-03, 0.10560286E-03,
        -0.19101341E-02,-0.10291074E-02,-0.47977158E-03,-0.20385267E-03,
         0.49434433E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_250                                  =v_p_e_dq_250                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_250                                  =v_p_e_dq_250                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_250                                  =v_p_e_dq_250                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]            *x41*x52
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21    *x43    
        +coeff[ 30]*x11*x22    *x41    
        +coeff[ 31]    *x21    *x41*x52
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_250                                  =v_p_e_dq_250                                  
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dq_250                                  ;
}
float l_e_dq_250                                  (float *x,int m){
    int ncoeff= 32;
    float avdat= -0.2138966E-01;
    float xmin[10]={
        -0.39971E-02,-0.60042E-01,-0.59963E-01,-0.30006E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39994E-02, 0.60070E-01, 0.59989E-01, 0.29968E-01, 0.40000E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 33]={
         0.21109000E-01,-0.32331890E+00,-0.25372654E-01, 0.12113126E-01,
        -0.30401416E-01,-0.16293563E-01,-0.57531721E-02,-0.49021528E-02,
         0.20689880E-02, 0.10319926E-01, 0.80019776E-02,-0.13013107E-04,
         0.23094224E-03, 0.28743651E-02,-0.30361738E-02,-0.78496279E-03,
         0.34743908E-02,-0.22014827E-02, 0.13608992E-02, 0.13948228E-02,
         0.10144806E-02, 0.10985349E-02, 0.62576495E-03,-0.28776881E-03,
        -0.32015494E-03,-0.28213672E-03, 0.73032104E-02, 0.52550496E-02,
        -0.15062951E-02, 0.71624591E-03,-0.86754601E-03,-0.48572352E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;
    float x54 = x53*x5;

//                 function

    float v_l_e_dq_250                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_250                                  =v_l_e_dq_250                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_250                                  =v_l_e_dq_250                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]        *x34    *x51
        +coeff[ 21]            *x44*x51
        +coeff[ 22]*x11*x22            
        +coeff[ 23]*x11    *x32        
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]*x11        *x43    
    ;
    v_l_e_dq_250                                  =v_l_e_dq_250                                  
        +coeff[ 26]    *x23*x31*x41    
        +coeff[ 27]    *x23    *x42    
        +coeff[ 28]    *x23        *x52
        +coeff[ 29]        *x32*x41*x52
        +coeff[ 30]    *x23*x32    *x51
        +coeff[ 31]        *x32    *x54
        ;

    return v_l_e_dq_250                                  ;
}
float x_e_dq_200                                  (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5105109E+00;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.80843037E-02,-0.90987599E-02, 0.96029870E-01, 0.20070924E+00,
         0.24723733E-01, 0.15865907E-01,-0.32787086E-02,-0.31707534E-02,
        -0.88183479E-02,-0.82105501E-02,-0.10234232E-02,-0.19163312E-02,
         0.18740159E-02,-0.45142965E-02,-0.10161012E-02,-0.56488655E-03,
        -0.23378889E-03,-0.38853189E-03,-0.10618048E-02,-0.53598620E-02,
        -0.25412117E-02,-0.28571752E-02,-0.43891850E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_200                                  =v_x_e_dq_200                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_200                                  =v_x_e_dq_200                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_dq_200                                  ;
}
float t_e_dq_200                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5702196E+00;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.57077664E-02, 0.34296038E-02,-0.10577717E+00, 0.24300834E-01,
        -0.74019940E-02,-0.26601823E-02,-0.95259020E-03,-0.11684701E-02,
         0.33841589E-02, 0.11086441E-02, 0.10030271E-02, 0.34111051E-03,
         0.42562824E-03, 0.42790436E-03, 0.14723980E-03,-0.15905029E-03,
         0.95504086E-03, 0.46534187E-04, 0.45217908E-03, 0.91553252E-03,
        -0.56130160E-03, 0.15432024E-03, 0.20138780E-02, 0.26576425E-03,
        -0.24643753E-03, 0.56390604E-03, 0.38394693E-03,-0.54061256E-03,
        -0.55080507E-03,-0.17650644E-03, 0.25950959E-02, 0.11356620E-03,
         0.11885065E-02, 0.13511881E-02, 0.29847698E-03,-0.46750400E-04,
         0.47108324E-03,-0.37288695E-03,-0.28030638E-03, 0.68523164E-04,
        -0.47076617E-04, 0.37758931E-04, 0.57237001E-04, 0.12536634E-03,
         0.11897280E-03, 0.60020844E-04, 0.72820105E-04, 0.82625891E-04,
        -0.87927780E-04, 0.82945327E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_200                                  =v_t_e_dq_200                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_t_e_dq_200                                  =v_t_e_dq_200                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x22    *x42    
        +coeff[ 20]    *x23        *x51
        +coeff[ 21]    *x21*x32    *x51
        +coeff[ 22]    *x23    *x42    
        +coeff[ 23]    *x23            
        +coeff[ 24]        *x32    *x51
        +coeff[ 25]    *x22*x32        
    ;
    v_t_e_dq_200                                  =v_t_e_dq_200                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]*x11*x22            
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x22*x32    *x51
    ;
    v_t_e_dq_200                                  =v_t_e_dq_200                                  
        +coeff[ 35]    *x22*x31*x41*x51
        +coeff[ 36]    *x22    *x42*x51
        +coeff[ 37]    *x23        *x52
        +coeff[ 38]    *x21*x31*x41*x52
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]                *x53
        +coeff[ 42]*x11*x21*x31*x41    
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_dq_200                                  =v_t_e_dq_200                                  
        +coeff[ 44]        *x32*x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x13    *x31*x41    
        ;

    return v_t_e_dq_200                                  ;
}
float y_e_dq_200                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.5926385E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.53311925E-03, 0.11055195E+00,-0.91360979E-01,-0.67792490E-01,
        -0.25192400E-01, 0.47445387E-01, 0.28030014E-01,-0.33408448E-01,
        -0.14789414E-01,-0.71695466E-02, 0.22986112E-02,-0.10241564E-01,
        -0.24610753E-02,-0.30521313E-02,-0.86673244E-04,-0.74554922E-03,
         0.90308732E-03,-0.65610637E-02,-0.15468464E-02, 0.15940139E-02,
        -0.19191895E-02, 0.52806395E-02,-0.54674773E-02, 0.56308915E-03,
         0.18359121E-02,-0.19851890E-02,-0.13316509E-02, 0.14210744E-02,
        -0.54311007E-02, 0.15805491E-02, 0.25833973E-02,-0.30684739E-02,
        -0.10799469E-01,-0.25099395E-02,-0.47599853E-03, 0.75725472E-03,
         0.13023162E-02, 0.32570589E-03,-0.36167211E-03, 0.85696595E-03,
        -0.28465539E-02, 0.33695422E-03,-0.38760058E-02,-0.17689470E-02,
         0.13239665E-02, 0.25772532E-02, 0.52531267E-03, 0.38375324E-02,
        -0.24093795E-02,-0.50110219E-03, 0.41580428E-04,-0.32762604E-04,
         0.40470326E-03, 0.59858721E-03,-0.42269522E-03, 0.24016162E-03,
        -0.14121749E-02,-0.11002338E-02,-0.11908629E-03, 0.30404372E-04,
        -0.45063120E-04,-0.74367897E-04,-0.39402203E-03, 0.46063049E-03,
        -0.19962479E-03, 0.12621764E-03, 0.69222006E-03, 0.30138966E-03,
        -0.54340012E-03,-0.54030941E-03,-0.94681354E-02,-0.63725552E-02,
         0.15141995E-03,-0.39351507E-04,-0.21580165E-04,-0.30905023E-04,
         0.47836627E-04,-0.39191567E-04, 0.29150176E-04, 0.64072890E-04,
         0.39571696E-02, 0.50509991E-02, 0.32726033E-02, 0.57148562E-04,
         0.10095017E-02, 0.11630348E-03,-0.74925984E-03, 0.54873194E-03,
        -0.18267600E-03,-0.39211754E-02,-0.15826867E-02, 0.48014595E-05,
         0.39292368E-04, 0.35321678E-04,-0.20045747E-04, 0.15969530E-04,
         0.42514341E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x41*x52
        +coeff[ 14]            *x45    
        +coeff[ 15]        *x32*x43    
        +coeff[ 16]*x11    *x31        
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 17]        *x32*x41    
        +coeff[ 18]        *x33        
        +coeff[ 19]*x11*x21    *x41    
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]    *x22    *x41*x52
        +coeff[ 47]    *x23*x31*x42    
        +coeff[ 48]    *x24    *x43    
        +coeff[ 49]    *x23    *x45    
        +coeff[ 50]    *x21            
        +coeff[ 51]                *x51
        +coeff[ 52]    *x21    *x43    
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 53]            *x43*x51
        +coeff[ 54]        *x31*x44    
        +coeff[ 55]        *x33    *x51
        +coeff[ 56]    *x24    *x41    
        +coeff[ 57]    *x22*x33        
        +coeff[ 58]*x11*x22    *x41*x51
        +coeff[ 59]    *x22            
        +coeff[ 60]*x12        *x41    
        +coeff[ 61]*x11*x21    *x41*x51
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 62]        *x33*x42    
        +coeff[ 63]*x11*x21    *x43    
        +coeff[ 64]        *x34*x41    
        +coeff[ 65]            *x41*x53
        +coeff[ 66]*x11*x21*x31*x42    
        +coeff[ 67]*x11*x21*x32*x41    
        +coeff[ 68]    *x22    *x43*x51
        +coeff[ 69]    *x24    *x41*x51
        +coeff[ 70]    *x22*x31*x44    
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 71]    *x22*x33*x42    
        +coeff[ 72]    *x24*x31*x42*x51
        +coeff[ 73]*x11        *x41*x51
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11    *x32*x41    
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]*x12    *x31*x41    
        +coeff[ 79]        *x31    *x53
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 80]    *x21*x31*x44    
        +coeff[ 81]    *x21*x32*x43    
        +coeff[ 82]    *x21*x33*x42    
        +coeff[ 83]    *x22*x31    *x52
        +coeff[ 84]    *x21*x34*x41    
        +coeff[ 85]    *x21    *x41*x53
        +coeff[ 86]    *x22*x31*x42*x51
        +coeff[ 87]    *x23*x33        
        +coeff[ 88]    *x24*x31    *x51
    ;
    v_y_e_dq_200                                  =v_y_e_dq_200                                  
        +coeff[ 89]    *x24*x31*x42    
        +coeff[ 90]    *x22*x34*x41    
        +coeff[ 91]    *x21        *x51
        +coeff[ 92]        *x32*x42    
        +coeff[ 93]    *x22*x31*x41    
        +coeff[ 94]*x11*x21*x31*x41    
        +coeff[ 95]*x11*x21*x31    *x51
        +coeff[ 96]*x11*x23    *x41    
        ;

    return v_y_e_dq_200                                  ;
}
float p_e_dq_200                                  (float *x,int m){
    int ncoeff= 37;
    float avdat= -0.1540424E-03;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
         0.15284095E-03,-0.28016925E-01, 0.11228422E-01,-0.59991335E-02,
        -0.15214385E-01, 0.51550232E-02, 0.11575697E-01,-0.91496911E-02,
        -0.62563963E-03,-0.26277259E-02,-0.28400775E-02, 0.48383084E-03,
        -0.23149638E-02,-0.12951228E-02,-0.39110106E-03, 0.20498040E-02,
        -0.12287309E-02,-0.10831057E-02, 0.52562722E-04, 0.25868355E-03,
        -0.38031879E-03,-0.15704073E-02, 0.53229340E-03,-0.95875876E-03,
         0.90852525E-03, 0.12558476E-03,-0.34066875E-03,-0.66069488E-05,
        -0.24141508E-03, 0.54435380E-03, 0.26008117E-03,-0.19285131E-03,
        -0.19252495E-02,-0.10452607E-02,-0.49134734E-03,-0.19156591E-03,
         0.50487200E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_200                                  =v_p_e_dq_200                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_200                                  =v_p_e_dq_200                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_200                                  =v_p_e_dq_200                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]        *x31    *x52
        +coeff[ 28]            *x41*x52
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_200                                  =v_p_e_dq_200                                  
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dq_200                                  ;
}
float l_e_dq_200                                  (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2479810E-01;
    float xmin[10]={
        -0.39998E-02,-0.59418E-01,-0.59988E-01,-0.30046E-01,-0.39995E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.60062E-01, 0.59975E-01, 0.30008E-01, 0.39990E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.22921344E-01,-0.32533145E+00,-0.25525481E-01, 0.11943991E-01,
        -0.30850759E-01,-0.16321754E-01,-0.55657392E-02,-0.51787859E-02,
         0.20528140E-02, 0.10853685E-01, 0.89277821E-02, 0.32917247E-03,
         0.35679626E-03, 0.15752268E-02,-0.32847978E-02,-0.68256090E-03,
         0.40816073E-02,-0.21031192E-02, 0.90153835E-03,-0.29738474E-03,
         0.71514689E-03, 0.12044889E-02, 0.57508895E-03, 0.56553161E-03,
         0.55990205E-02, 0.33250342E-02,-0.73726068E-03, 0.11846382E-02,
        -0.85438724E-03, 0.75784716E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_200                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_200                                  =v_l_e_dq_200                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x21*x32        
    ;
    v_l_e_dq_200                                  =v_l_e_dq_200                                  
        +coeff[ 17]    *x22        *x51
        +coeff[ 18]    *x21        *x52
        +coeff[ 19]    *x23            
        +coeff[ 20]        *x32    *x51
        +coeff[ 21]        *x31*x41*x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
    ;
    v_l_e_dq_200                                  =v_l_e_dq_200                                  
        +coeff[ 26]*x11*x21*x31    *x52
        +coeff[ 27]    *x24    *x42    
        +coeff[ 28]*x11*x22        *x53
        +coeff[ 29]*x12*x21        *x53
        ;

    return v_l_e_dq_200                                  ;
}
float x_e_dq_175                                  (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.5098889E+00;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.67080138E-02,-0.90484796E-02, 0.96103616E-01, 0.20130779E+00,
         0.24660962E-01, 0.15967896E-01,-0.32951017E-02,-0.10006035E-02,
        -0.31356192E-02, 0.19296124E-02,-0.97038336E-02,-0.76120640E-02,
         0.45207929E-04,-0.17287290E-02,-0.19234016E-02, 0.67602820E-03,
        -0.37482781E-02,-0.19662530E-04, 0.14278246E-04,-0.10123637E-02,
        -0.57417399E-03,-0.22536394E-03,-0.41057423E-03,-0.11151108E-02,
        -0.42601107E-02,-0.31788570E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_x_e_dq_175                                  =v_x_e_dq_175                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x22        *x51
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x31*x41    
        +coeff[ 15]    *x23            
        +coeff[ 16]    *x21*x32        
    ;
    v_x_e_dq_175                                  =v_x_e_dq_175                                  
        +coeff[ 17]*x12    *x32        
        +coeff[ 18]*x12                
        +coeff[ 19]        *x32        
        +coeff[ 20]            *x42*x51
        +coeff[ 21]    *x21        *x52
        +coeff[ 22]*x11*x22            
        +coeff[ 23]    *x22*x31*x41    
        +coeff[ 24]    *x23*x31*x41    
        +coeff[ 25]    *x23    *x42    
        ;

    return v_x_e_dq_175                                  ;
}
float t_e_dq_175                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5698326E+00;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.56698378E-02, 0.34072169E-02,-0.10608194E+00, 0.24238061E-01,
        -0.73534623E-02,-0.26722155E-02,-0.94977033E-03,-0.11187340E-02,
         0.26888002E-02, 0.10128652E-02, 0.12276288E-02, 0.33397105E-03,
         0.44435129E-03, 0.42800783E-03, 0.54609327E-03, 0.17558072E-03,
        -0.14523744E-03, 0.81609422E-03, 0.15140821E-03, 0.47545691E-03,
         0.88393892E-03,-0.59787621E-03, 0.20786558E-03, 0.20734707E-02,
         0.22193229E-03,-0.20925501E-03, 0.37441796E-03,-0.53216633E-03,
        -0.52849209E-03,-0.18980309E-03, 0.27953810E-02, 0.11357452E-03,
         0.24335319E-02, 0.21117749E-02,-0.17780700E-03, 0.41480616E-03,
        -0.40020136E-03,-0.21287866E-03, 0.63730397E-04, 0.76787874E-04,
        -0.41724255E-04, 0.44959597E-04, 0.73130264E-04, 0.43725908E-04,
         0.89743342E-04, 0.70166956E-04,-0.78969249E-04, 0.11654494E-02,
         0.14300868E-03,-0.11015811E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_175                                  =v_t_e_dq_175                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dq_175                                  =v_t_e_dq_175                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]    *x23        *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x23            
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_175                                  =v_t_e_dq_175                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]*x11*x22            
        +coeff[ 32]    *x21*x32*x42    
        +coeff[ 33]    *x21*x31*x43    
        +coeff[ 34]    *x22*x31*x41*x51
    ;
    v_t_e_dq_175                                  =v_t_e_dq_175                                  
        +coeff[ 35]    *x22    *x42*x51
        +coeff[ 36]    *x23        *x52
        +coeff[ 37]    *x21*x31*x41*x52
        +coeff[ 38]*x11    *x31*x41    
        +coeff[ 39]*x11        *x42    
        +coeff[ 40]*x11*x21        *x51
        +coeff[ 41]        *x31*x41*x51
        +coeff[ 42]                *x53
        +coeff[ 43]        *x33*x41    
    ;
    v_t_e_dq_175                                  =v_t_e_dq_175                                  
        +coeff[ 44]        *x32    *x52
        +coeff[ 45]            *x42*x52
        +coeff[ 46]    *x21        *x53
        +coeff[ 47]    *x21*x33*x41    
        +coeff[ 48]    *x22*x32    *x51
        +coeff[ 49]    *x22        *x53
        ;

    return v_t_e_dq_175                                  ;
}
float y_e_dq_175                                  (float *x,int m){
    int ncoeff= 97;
    float avdat=  0.2331110E-03;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
        -0.26520749E-03, 0.10872943E+00,-0.92708476E-01,-0.67926541E-01,
        -0.25163140E-01, 0.47336791E-01, 0.27996186E-01,-0.33474535E-01,
        -0.14717032E-01,-0.70596403E-02, 0.23094462E-02,-0.10152232E-01,
        -0.24168401E-02,-0.15917698E-04,-0.67105598E-03, 0.88907446E-03,
        -0.64791096E-02,-0.15342509E-02, 0.15574489E-02,-0.30500081E-02,
        -0.18953762E-02, 0.52584400E-02,-0.54955832E-02, 0.55568991E-03,
         0.17867886E-02,-0.17928198E-02,-0.13382125E-02, 0.13601662E-02,
        -0.55433768E-02, 0.15908531E-02, 0.25573135E-02,-0.33166856E-02,
        -0.10468293E-01,-0.24160673E-02,-0.48891117E-03, 0.74204965E-03,
         0.12811086E-02, 0.34406598E-03,-0.36128782E-03, 0.85007213E-03,
        -0.25032822E-02, 0.31602767E-03,-0.39407681E-02,-0.16955783E-02,
         0.13139718E-02, 0.24032472E-02,-0.26039132E-02,-0.23954148E-03,
         0.27037269E-04,-0.21592672E-04, 0.32362790E-03, 0.61288429E-03,
        -0.39713443E-03, 0.24209465E-03,-0.12280733E-02,-0.10848880E-02,
         0.75395382E-03, 0.37608149E-02,-0.49350590E-04,-0.64268694E-04,
        -0.40605571E-03, 0.48410340E-03,-0.25489795E-03, 0.10575095E-03,
         0.67103835E-03, 0.29414648E-03,-0.55462471E-03,-0.66747249E-03,
        -0.91659306E-02,-0.20024858E-04,-0.58217687E-02, 0.15122650E-04,
        -0.34548229E-04,-0.24969393E-04,-0.40333438E-04, 0.62601488E-04,
         0.39560483E-02,-0.76852724E-04, 0.49112211E-02,-0.10634626E-03,
         0.29698240E-02, 0.89934235E-03,-0.50824345E-03, 0.50861377E-03,
        -0.17949130E-03,-0.39563542E-02,-0.14389915E-02,-0.32449266E-03,
         0.15890364E-04,-0.27513802E-05, 0.12419608E-04,-0.22560285E-04,
        -0.22584067E-04, 0.21830338E-04,-0.24645293E-04, 0.57025802E-04,
        -0.62050589E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x23    *x43    
        +coeff[ 46]    *x24    *x43    
        +coeff[ 47]    *x23    *x45    
        +coeff[ 48]    *x21            
        +coeff[ 49]                *x51
        +coeff[ 50]    *x21    *x43    
        +coeff[ 51]            *x43*x51
        +coeff[ 52]        *x31*x44    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 53]        *x33    *x51
        +coeff[ 54]    *x24    *x41    
        +coeff[ 55]    *x22*x33        
        +coeff[ 56]    *x22    *x41*x52
        +coeff[ 57]    *x23*x31*x42    
        +coeff[ 58]*x11        *x41*x51
        +coeff[ 59]*x11*x21    *x41*x51
        +coeff[ 60]        *x33*x42    
        +coeff[ 61]*x11*x21    *x43    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 62]        *x34*x41    
        +coeff[ 63]            *x41*x53
        +coeff[ 64]*x11*x21*x31*x42    
        +coeff[ 65]*x11*x21*x32*x41    
        +coeff[ 66]    *x22    *x43*x51
        +coeff[ 67]    *x24    *x41*x51
        +coeff[ 68]    *x22*x31*x44    
        +coeff[ 69]*x11    *x31    *x53
        +coeff[ 70]    *x22*x33*x42    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 71]    *x22            
        +coeff[ 72]*x12        *x41    
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x11*x22*x31        
        +coeff[ 75]        *x31    *x53
        +coeff[ 76]    *x21*x31*x44    
        +coeff[ 77]    *x23*x31    *x51
        +coeff[ 78]    *x21*x32*x43    
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 80]    *x21*x33*x42    
        +coeff[ 81]    *x21*x34*x41    
        +coeff[ 82]    *x22*x31*x42*x51
        +coeff[ 83]    *x23*x33        
        +coeff[ 84]    *x24*x31    *x51
        +coeff[ 85]    *x24*x31*x42    
        +coeff[ 86]    *x22*x34*x41    
        +coeff[ 87]    *x24    *x41*x52
        +coeff[ 88]        *x31*x41    
    ;
    v_y_e_dq_175                                  =v_y_e_dq_175                                  
        +coeff[ 89]*x11*x21            
        +coeff[ 90]            *x44    
        +coeff[ 91]*x11    *x31    *x51
        +coeff[ 92]*x11        *x43    
        +coeff[ 93]*x11*x21*x31*x41    
        +coeff[ 94]*x11        *x41*x52
        +coeff[ 95]*x11*x23    *x41    
        +coeff[ 96]    *x21*x32*x41*x51
        ;

    return v_y_e_dq_175                                  ;
}
float p_e_dq_175                                  (float *x,int m){
    int ncoeff= 36;
    float avdat=  0.8889162E-04;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
        -0.88232176E-04,-0.28295958E-01, 0.10855804E-01,-0.60083494E-02,
        -0.15267124E-01, 0.51360396E-02, 0.11529068E-01,-0.90957722E-02,
        -0.67508890E-03,-0.26159626E-02,-0.28402470E-02, 0.48347210E-03,
        -0.22806355E-02,-0.12803073E-02,-0.37401344E-03, 0.20574506E-02,
        -0.12432804E-02,-0.11381889E-02, 0.91855210E-04, 0.25727862E-03,
        -0.37356801E-03,-0.15696940E-02, 0.51899638E-03,-0.91338292E-03,
         0.91676920E-03, 0.12081073E-03,-0.33471547E-03,-0.53017412E-03,
        -0.23269604E-03, 0.53801725E-03, 0.26286044E-03,-0.18310599E-03,
        -0.18190161E-02,-0.10678821E-02,-0.50146278E-03, 0.48667076E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_175                                  =v_p_e_dq_175                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_175                                  =v_p_e_dq_175                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_175                                  =v_p_e_dq_175                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x22*x31*x42*x52
        +coeff[ 28]            *x41*x52
        +coeff[ 29]    *x21*x31*x42    
        +coeff[ 30]    *x21    *x43    
        +coeff[ 31]*x11*x22    *x41    
        +coeff[ 32]    *x22*x31*x42    
        +coeff[ 33]    *x22    *x43    
        +coeff[ 34]    *x23*x31    *x51
    ;
    v_p_e_dq_175                                  =v_p_e_dq_175                                  
        +coeff[ 35]    *x22    *x41*x52
        ;

    return v_p_e_dq_175                                  ;
}
float l_e_dq_175                                  (float *x,int m){
    int ncoeff= 30;
    float avdat= -0.2489657E-01;
    float xmin[10]={
        -0.39998E-02,-0.58938E-01,-0.59981E-01,-0.30011E-01,-0.39999E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39989E-02, 0.60040E-01, 0.59970E-01, 0.29987E-01, 0.39982E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 31]={
         0.21749994E-01,-0.32677168E+00,-0.25622766E-01, 0.11886462E-01,
        -0.31147387E-01,-0.15951976E-01,-0.58712550E-02,-0.54134778E-02,
         0.20352020E-02, 0.11474669E-01, 0.83926655E-02, 0.51969645E-03,
        -0.61672844E-03, 0.81211835E-03,-0.76035084E-03, 0.46267002E-02,
        -0.67518925E-03,-0.25305087E-02, 0.12489086E-02, 0.64552348E-03,
         0.85244002E-03, 0.17726956E-02, 0.11324291E-02, 0.21647078E-02,
        -0.14081319E-02,-0.11142936E-02, 0.12579692E-01, 0.51763728E-02,
         0.52614356E-02, 0.57036569E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_l_e_dq_175                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_175                                  =v_l_e_dq_175                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]*x11            *x51
        +coeff[ 15]    *x21*x32        
        +coeff[ 16]    *x22        *x51
    ;
    v_l_e_dq_175                                  =v_l_e_dq_175                                  
        +coeff[ 17]        *x32        
        +coeff[ 18]        *x31*x41*x51
        +coeff[ 19]*x11*x22            
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x22    *x42    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x24        *x51
        +coeff[ 25]    *x21*x32    *x52
    ;
    v_l_e_dq_175                                  =v_l_e_dq_175                                  
        +coeff[ 26]    *x23*x31*x43    
        +coeff[ 27]    *x23    *x44    
        +coeff[ 28]    *x21*x32*x44    
        +coeff[ 29]        *x34    *x53
        ;

    return v_l_e_dq_175                                  ;
}
float x_e_dq_150                                  (float *x,int m){
    int ncoeff= 20;
    float avdat=  0.5100188E+00;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
        -0.47949762E-02,-0.89912955E-02, 0.96350178E-01, 0.20212771E+00,
         0.24484366E-01, 0.15945265E-01,-0.32940514E-02,-0.31916511E-02,
        -0.99359229E-02,-0.80571258E-02,-0.10090896E-02,-0.20254087E-02,
         0.18791039E-02,-0.44927970E-02,-0.10427058E-02,-0.52360003E-03,
        -0.37657830E-03,-0.10016811E-02,-0.32220420E-02,-0.20443129E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_150                                  =v_x_e_dq_150                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]*x11*x22            
    ;
    v_x_e_dq_150                                  =v_x_e_dq_150                                  
        +coeff[ 17]    *x22*x31*x41    
        +coeff[ 18]    *x23*x31*x41    
        +coeff[ 19]    *x23    *x42    
        ;

    return v_x_e_dq_150                                  ;
}
float t_e_dq_150                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5700185E+00;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.44338102E-02,-0.10600971E+00, 0.24190260E-01,-0.72515276E-02,
        -0.21629376E-04, 0.33946494E-02,-0.26989633E-02,-0.92117424E-03,
        -0.10808643E-02, 0.27054730E-02, 0.10126499E-02, 0.11988085E-02,
         0.33647573E-03, 0.45191337E-03, 0.42973072E-03, 0.58229332E-03,
         0.20044600E-03,-0.15763014E-03, 0.79282612E-03, 0.55988523E-04,
         0.44632345E-03, 0.98093669E-03,-0.57676580E-03, 0.20758367E-03,
         0.20095045E-02, 0.18462796E-03,-0.20791736E-03, 0.40934104E-03,
        -0.55839325E-03,-0.57338353E-03,-0.20718848E-03, 0.27426388E-02,
         0.10642534E-03,-0.45485209E-04, 0.10616967E-03, 0.23624718E-02,
         0.20345817E-02, 0.18760229E-03,-0.19748823E-03, 0.43568225E-03,
        -0.39279865E-03,-0.26167464E-03, 0.53158052E-04, 0.37566602E-04,
         0.53051037E-04, 0.60315862E-04, 0.63752894E-04,-0.94471972E-04,
         0.10904223E-02,-0.17120890E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]                *x52
    ;
    v_t_e_dq_150                                  =v_t_e_dq_150                                  
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42    
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x32        
    ;
    v_t_e_dq_150                                  =v_t_e_dq_150                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x23            
    ;
    v_t_e_dq_150                                  =v_t_e_dq_150                                  
        +coeff[ 26]        *x32    *x51
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x21*x31*x41*x51
        +coeff[ 29]    *x21    *x42*x51
        +coeff[ 30]    *x22        *x52
        +coeff[ 31]    *x23*x31*x41    
        +coeff[ 32]*x11*x22            
        +coeff[ 33]*x11*x21        *x51
        +coeff[ 34]*x13        *x42    
    ;
    v_t_e_dq_150                                  =v_t_e_dq_150                                  
        +coeff[ 35]    *x21*x32*x42    
        +coeff[ 36]    *x21*x31*x43    
        +coeff[ 37]    *x22*x32    *x51
        +coeff[ 38]    *x22*x31*x41*x51
        +coeff[ 39]    *x22    *x42*x51
        +coeff[ 40]    *x23        *x52
        +coeff[ 41]    *x21*x31*x41*x52
        +coeff[ 42]*x11    *x31*x41    
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_150                                  =v_t_e_dq_150                                  
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]    *x21        *x53
        +coeff[ 48]    *x21*x33*x41    
        +coeff[ 49]        *x32*x42*x51
        ;

    return v_t_e_dq_150                                  ;
}
float y_e_dq_150                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1629109E-03;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.11984433E-03, 0.10623333E+00,-0.94618097E-01,-0.68292305E-01,
        -0.25136264E-01, 0.47332201E-01, 0.27966037E-01,-0.33045352E-01,
        -0.14505221E-01,-0.70559401E-02, 0.22915096E-02,-0.10093006E-01,
        -0.24095213E-02,-0.72803159E-04,-0.69901149E-03, 0.87946164E-03,
        -0.64922813E-02,-0.15167283E-02, 0.15416187E-02,-0.29929245E-02,
        -0.18832813E-02, 0.51855706E-02,-0.53896150E-02, 0.54619316E-03,
         0.16522589E-02,-0.20148223E-02,-0.12979812E-02, 0.12222781E-02,
        -0.49799695E-02, 0.13558093E-02, 0.24978174E-02,-0.31639216E-02,
        -0.10620235E-01,-0.26796521E-02,-0.48910663E-03, 0.56975830E-03,
         0.12241437E-02, 0.30121210E-03,-0.35756177E-03, 0.84134587E-03,
        -0.23698299E-02, 0.37189139E-03,-0.34115883E-02,-0.16083936E-02,
         0.12650767E-02,-0.27750754E-02, 0.27775972E-04,-0.18748415E-04,
         0.41006712E-03, 0.54516987E-03,-0.37632414E-03, 0.23180003E-03,
        -0.12022872E-02,-0.10104601E-02, 0.50082320E-03,-0.51195308E-04,
        -0.47968275E-04,-0.78392943E-04,-0.43619092E-03, 0.44259382E-03,
        -0.16001088E-03, 0.96222713E-04, 0.68402180E-03, 0.29233017E-03,
         0.19555842E-02, 0.35997571E-02,-0.36952808E-03,-0.91775535E-02,
        -0.43101264E-02,-0.60464456E-02,-0.16323486E-02,-0.14219621E-03,
         0.16688919E-04,-0.20003785E-04,-0.29259791E-04,-0.37910271E-04,
         0.52909181E-04, 0.35956223E-02, 0.45711235E-02,-0.73560783E-04,
         0.28050328E-02, 0.89451054E-03, 0.81408420E-04,-0.51227980E-03,
         0.50577335E-03,-0.21888273E-03,-0.78215664E-04,-0.44238748E-03,
        -0.82955676E-05, 0.10699934E-04, 0.25378167E-04, 0.31471805E-04,
         0.15087070E-04,-0.18785449E-04, 0.60761915E-04,-0.99417972E-04,
        -0.77446624E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 35]    *x21*x32*x41    
        +coeff[ 36]        *x31*x42*x51
        +coeff[ 37]    *x21*x33        
        +coeff[ 38]*x11*x22    *x41    
        +coeff[ 39]        *x32*x41*x51
        +coeff[ 40]    *x22    *x43    
        +coeff[ 41]    *x21    *x41*x52
        +coeff[ 42]    *x22*x32*x41    
        +coeff[ 43]    *x24*x31        
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 44]    *x23    *x41*x51
        +coeff[ 45]    *x24    *x43    
        +coeff[ 46]    *x21            
        +coeff[ 47]                *x51
        +coeff[ 48]    *x21    *x43    
        +coeff[ 49]            *x43*x51
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]    *x22    *x41*x52
        +coeff[ 55]*x12        *x41    
        +coeff[ 56]*x11        *x41*x51
        +coeff[ 57]*x11*x21    *x41*x51
        +coeff[ 58]        *x33*x42    
        +coeff[ 59]*x11*x21    *x43    
        +coeff[ 60]        *x34*x41    
        +coeff[ 61]            *x41*x53
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 62]*x11*x21*x31*x42    
        +coeff[ 63]*x11*x21*x32*x41    
        +coeff[ 64]    *x23    *x43    
        +coeff[ 65]    *x23*x31*x42    
        +coeff[ 66]    *x24    *x41*x51
        +coeff[ 67]    *x22*x31*x44    
        +coeff[ 68]    *x24*x31*x42    
        +coeff[ 69]    *x22*x33*x42    
        +coeff[ 70]    *x22*x34*x41    
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 71]    *x22    *x45*x51
        +coeff[ 72]    *x22            
        +coeff[ 73]*x12    *x31        
        +coeff[ 74]*x11    *x31    *x51
        +coeff[ 75]*x11*x22*x31        
        +coeff[ 76]        *x31    *x53
        +coeff[ 77]    *x21*x31*x44    
        +coeff[ 78]    *x21*x32*x43    
        +coeff[ 79]*x11*x22    *x41*x51
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 80]    *x21*x33*x42    
        +coeff[ 81]    *x21*x34*x41    
        +coeff[ 82]    *x21    *x41*x53
        +coeff[ 83]    *x22*x31*x42*x51
        +coeff[ 84]    *x23*x33        
        +coeff[ 85]    *x24*x31    *x51
        +coeff[ 86]            *x45*x52
        +coeff[ 87]    *x24    *x43*x51
        +coeff[ 88]            *x42    
    ;
    v_y_e_dq_150                                  =v_y_e_dq_150                                  
        +coeff[ 89]        *x31*x41    
        +coeff[ 90]            *x44    
        +coeff[ 91]*x12*x21    *x41    
        +coeff[ 92]*x11*x21*x31    *x51
        +coeff[ 93]*x11        *x41*x52
        +coeff[ 94]*x11*x23    *x41    
        +coeff[ 95]    *x21*x32*x41*x51
        +coeff[ 96]    *x23*x31    *x51
        ;

    return v_y_e_dq_150                                  ;
}
float p_e_dq_150                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.3523654E-04;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.37013455E-04,-0.28689256E-01, 0.10333079E-01,-0.60253325E-02,
        -0.15386134E-01, 0.51055434E-02, 0.11485715E-01,-0.89563597E-02,
        -0.66672708E-03,-0.26059488E-02,-0.27169511E-02, 0.48624558E-03,
        -0.22651218E-02,-0.12749187E-02,-0.37960970E-03, 0.20469013E-02,
        -0.12618714E-02,-0.11810227E-02, 0.77031458E-04, 0.25508340E-03,
        -0.37555391E-03,-0.15591987E-02, 0.51342876E-03,-0.88111340E-03,
         0.89047267E-03, 0.12146702E-03,-0.34971221E-03,-0.51526073E-03,
        -0.13191288E-03,-0.23145949E-03, 0.49639947E-03, 0.24575536E-03,
        -0.18993983E-03,-0.19554349E-02,-0.10788356E-02,-0.21392517E-03,
         0.48335994E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_150                                  =v_p_e_dq_150                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_150                                  =v_p_e_dq_150                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x23    *x41*x51
        +coeff[ 25]*x11*x21*x31        
    ;
    v_p_e_dq_150                                  =v_p_e_dq_150                                  
        +coeff[ 26]    *x21*x31    *x52
        +coeff[ 27]    *x23*x31    *x51
        +coeff[ 28]    *x22*x31*x42*x52
        +coeff[ 29]            *x41*x52
        +coeff[ 30]    *x21*x31*x42    
        +coeff[ 31]    *x21    *x43    
        +coeff[ 32]*x11*x22    *x41    
        +coeff[ 33]    *x22*x31*x42    
        +coeff[ 34]    *x22    *x43    
    ;
    v_p_e_dq_150                                  =v_p_e_dq_150                                  
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dq_150                                  ;
}
float l_e_dq_150                                  (float *x,int m){
    int ncoeff= 29;
    float avdat= -0.2460554E-01;
    float xmin[10]={
        -0.39990E-02,-0.57751E-01,-0.59992E-01,-0.30034E-01,-0.39989E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39997E-02, 0.60016E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 30]={
         0.18332681E-01,-0.32686132E+00,-0.25691211E-01, 0.11863766E-01,
        -0.31097934E-01,-0.15693117E-01,-0.59774723E-02,-0.52960017E-02,
         0.20368479E-02, 0.10179566E-01, 0.77599962E-02, 0.94968936E-03,
         0.43555073E-03, 0.97444199E-03,-0.35671226E-02,-0.79406594E-03,
        -0.46808983E-03, 0.43102908E-02,-0.21413371E-02, 0.15690150E-02,
         0.18186482E-02, 0.63492864E-03, 0.79141272E-03, 0.47206096E-03,
        -0.29128397E-03, 0.18154258E-02, 0.10362131E-02, 0.52751545E-02,
         0.40318640E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_150                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_150                                  =v_l_e_dq_150                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dq_150                                  =v_l_e_dq_150                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21    *x42*x52
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11*x21        *x51
        +coeff[ 25]    *x22*x31*x41    
    ;
    v_l_e_dq_150                                  =v_l_e_dq_150                                  
        +coeff[ 26]    *x22    *x42    
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]    *x23    *x42    
        ;

    return v_l_e_dq_150                                  ;
}
float x_e_dq_125                                  (float *x,int m){
    int ncoeff= 23;
    float avdat=  0.5095363E+00;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 24]={
        -0.39192084E-02,-0.88953245E-02, 0.96402951E-01, 0.20524263E+00,
         0.24483928E-01, 0.16291268E-01,-0.32794862E-02,-0.32049709E-02,
        -0.86858505E-02,-0.82667200E-02,-0.99854846E-03,-0.19767794E-02,
         0.18807828E-02,-0.43390784E-02,-0.10690832E-02,-0.53346809E-03,
        -0.23829336E-03,-0.38844388E-03,-0.11962504E-02,-0.49624685E-02,
        -0.18106493E-02,-0.27757345E-02,-0.47513274E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]            *x42    
    ;
    v_x_e_dq_125                                  =v_x_e_dq_125                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]*x11*x21            
        +coeff[ 11]        *x31*x41    
        +coeff[ 12]    *x22        *x51
        +coeff[ 13]    *x21*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]            *x42*x51
        +coeff[ 16]    *x21        *x52
    ;
    v_x_e_dq_125                                  =v_x_e_dq_125                                  
        +coeff[ 17]*x11*x22            
        +coeff[ 18]    *x22*x31*x41    
        +coeff[ 19]    *x23*x31*x41    
        +coeff[ 20]    *x23    *x42    
        +coeff[ 21]    *x21*x31*x43    
        +coeff[ 22]    *x23*x32*x42    
        ;

    return v_x_e_dq_125                                  ;
}
float t_e_dq_125                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5709059E+00;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.33578556E-02, 0.33418108E-02,-0.10728327E+00, 0.24184445E-01,
        -0.72210776E-02,-0.27918550E-02,-0.94174343E-03,-0.10503319E-02,
         0.26780011E-02, 0.10807184E-02, 0.12017089E-02, 0.35270455E-03,
         0.40738113E-03, 0.40086260E-03, 0.58675202E-03, 0.21434481E-03,
        -0.14953478E-03, 0.79054583E-03, 0.64301661E-04, 0.44807873E-03,
         0.95744169E-03,-0.60611294E-03, 0.20762091E-03, 0.19163251E-02,
         0.17716635E-03,-0.24439034E-03, 0.37707490E-03,-0.52284298E-03,
        -0.53207390E-03,-0.21245987E-03, 0.26719016E-02, 0.10079904E-03,
         0.67968787E-04, 0.23890014E-02, 0.21419844E-02, 0.23281816E-03,
        -0.18782452E-03, 0.41246729E-03,-0.38531222E-03,-0.24698404E-03,
        -0.12975547E-04, 0.45258399E-04,-0.37152007E-04, 0.44265602E-04,
        -0.57353780E-04, 0.44787477E-04, 0.79849044E-04, 0.82766623E-04,
        -0.83046201E-04, 0.11671620E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]    *x21            
        +coeff[  3]                *x51
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]        *x31*x41    
    ;
    v_t_e_dq_125                                  =v_t_e_dq_125                                  
        +coeff[  8]    *x21*x31*x41    
        +coeff[  9]    *x21    *x42    
        +coeff[ 10]    *x23*x32        
        +coeff[ 11]*x11*x21            
        +coeff[ 12]            *x42    
        +coeff[ 13]            *x42*x51
        +coeff[ 14]    *x22*x32        
        +coeff[ 15]        *x32        
        +coeff[ 16]*x11            *x51
    ;
    v_t_e_dq_125                                  =v_t_e_dq_125                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]    *x22    *x42    
        +coeff[ 21]    *x23        *x51
        +coeff[ 22]    *x21*x32    *x51
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x23            
        +coeff[ 25]        *x32    *x51
    ;
    v_t_e_dq_125                                  =v_t_e_dq_125                                  
        +coeff[ 26]    *x22*x31*x41    
        +coeff[ 27]    *x21*x31*x41*x51
        +coeff[ 28]    *x21    *x42*x51
        +coeff[ 29]    *x22        *x52
        +coeff[ 30]    *x23*x31*x41    
        +coeff[ 31]*x11*x22            
        +coeff[ 32]*x11        *x42    
        +coeff[ 33]    *x21*x32*x42    
        +coeff[ 34]    *x21*x31*x43    
    ;
    v_t_e_dq_125                                  =v_t_e_dq_125                                  
        +coeff[ 35]    *x22*x32    *x51
        +coeff[ 36]    *x22*x31*x41*x51
        +coeff[ 37]    *x22    *x42*x51
        +coeff[ 38]    *x23        *x52
        +coeff[ 39]    *x21*x31*x41*x52
        +coeff[ 40]*x12                
        +coeff[ 41]*x11    *x31*x41    
        +coeff[ 42]*x11*x21        *x51
        +coeff[ 43]                *x53
    ;
    v_t_e_dq_125                                  =v_t_e_dq_125                                  
        +coeff[ 44]*x11*x21    *x42    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]    *x21*x33*x41    
        ;

    return v_t_e_dq_125                                  ;
}
float y_e_dq_125                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.1427144E-03;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.56639223E-04, 0.10336990E+00,-0.96947968E-01,-0.68657540E-01,
        -0.25037004E-01, 0.47258895E-01, 0.27917285E-01,-0.33578318E-01,
        -0.14620725E-01,-0.70166257E-02, 0.22478874E-02,-0.10106293E-01,
        -0.24308071E-02,-0.53566146E-04,-0.35802621E-03, 0.85460622E-03,
        -0.64879712E-02,-0.15190997E-02, 0.15584003E-02,-0.30535136E-02,
        -0.18785002E-02, 0.52235899E-02,-0.53779581E-02, 0.54042129E-03,
         0.16207007E-02,-0.20234343E-02,-0.13514208E-02, 0.89838268E-03,
        -0.41334573E-02, 0.14135410E-02, 0.24115767E-02,-0.36092233E-02,
        -0.12378844E-01,-0.29452115E-02,-0.59795438E-03, 0.52303993E-04,
         0.46333644E-03, 0.12611340E-02, 0.27781131E-03,-0.35994282E-03,
         0.82432840E-03,-0.20593593E-02, 0.34616917E-03,-0.30897241E-02,
        -0.15879520E-02, 0.13186364E-02,-0.51361008E-05,-0.29518905E-02,
        -0.38084403E-04, 0.22112460E-03,-0.14419798E-03, 0.21984421E-03,
        -0.11327954E-02,-0.90401969E-03,-0.88822097E-04, 0.82254154E-03,
        -0.10939868E-01,-0.69001019E-02, 0.22729824E-04, 0.54706814E-03,
        -0.85010128E-04,-0.20428638E-03, 0.44200008E-03,-0.11527466E-03,
         0.12387986E-03, 0.62916701E-03, 0.31035169E-03, 0.37217573E-02,
        -0.30622672E-03,-0.48364112E-02,-0.17048066E-02,-0.17772915E-03,
        -0.11527863E-04, 0.28156219E-04, 0.82391271E-05,-0.33007484E-04,
        -0.47527079E-04, 0.29259367E-04,-0.20507523E-04,-0.25872350E-04,
        -0.29512510E-04, 0.52607098E-04, 0.39842054E-02, 0.22394119E-02,
         0.50233491E-02, 0.30457324E-02, 0.92330534E-03,-0.47683442E-03,
         0.49933157E-03,-0.17786934E-03,-0.38851376E-03, 0.20297233E-03,
        -0.61638112E-03, 0.24240358E-04,-0.34402067E-05, 0.77656723E-05,
         0.15094174E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21*x31*x42    
        +coeff[ 28]    *x22*x31*x42    
        +coeff[ 29]    *x21    *x45    
        +coeff[ 30]    *x23*x32*x41    
        +coeff[ 31]    *x22    *x45    
        +coeff[ 32]    *x22*x32*x43    
        +coeff[ 33]    *x24*x32*x41    
        +coeff[ 34]    *x24*x33        
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 35]    *x21            
        +coeff[ 36]    *x21*x32*x41    
        +coeff[ 37]        *x31*x42*x51
        +coeff[ 38]    *x21*x33        
        +coeff[ 39]*x11*x22    *x41    
        +coeff[ 40]        *x32*x41*x51
        +coeff[ 41]    *x22    *x43    
        +coeff[ 42]    *x21    *x41*x52
        +coeff[ 43]    *x22*x32*x41    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 44]    *x24*x31        
        +coeff[ 45]    *x23    *x41*x51
        +coeff[ 46]            *x45*x51
        +coeff[ 47]    *x24    *x43    
        +coeff[ 48]                *x51
        +coeff[ 49]    *x21    *x43    
        +coeff[ 50]        *x31*x44    
        +coeff[ 51]        *x33    *x51
        +coeff[ 52]    *x24    *x41    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 53]    *x22*x33        
        +coeff[ 54]*x11*x22    *x41*x51
        +coeff[ 55]    *x22    *x41*x52
        +coeff[ 56]    *x22*x31*x44    
        +coeff[ 57]    *x22*x33*x42    
        +coeff[ 58]    *x22            
        +coeff[ 59]            *x43*x51
        +coeff[ 60]*x11*x21    *x41*x51
        +coeff[ 61]        *x33*x42    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 62]*x11*x21    *x43    
        +coeff[ 63]        *x34*x41    
        +coeff[ 64]            *x41*x53
        +coeff[ 65]*x11*x21*x31*x42    
        +coeff[ 66]*x11*x21*x32*x41    
        +coeff[ 67]    *x23*x31*x42    
        +coeff[ 68]    *x24    *x41*x51
        +coeff[ 69]    *x24*x31*x42    
        +coeff[ 70]    *x22*x34*x41    
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 71]    *x23    *x45    
        +coeff[ 72]    *x22    *x45*x51
        +coeff[ 73]        *x31*x41    
        +coeff[ 74]    *x21        *x51
        +coeff[ 75]*x12        *x41    
        +coeff[ 76]*x11        *x41*x51
        +coeff[ 77]        *x32*x42    
        +coeff[ 78]*x12    *x31        
        +coeff[ 79]*x11    *x31    *x51
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 80]*x11*x22*x31        
        +coeff[ 81]        *x31    *x53
        +coeff[ 82]    *x21*x31*x44    
        +coeff[ 83]    *x23    *x43    
        +coeff[ 84]    *x21*x32*x43    
        +coeff[ 85]    *x21*x33*x42    
        +coeff[ 86]    *x21*x34*x41    
        +coeff[ 87]    *x22*x31*x42*x51
        +coeff[ 88]    *x23*x33        
    ;
    v_y_e_dq_125                                  =v_y_e_dq_125                                  
        +coeff[ 89]    *x24*x31    *x51
        +coeff[ 90]    *x24    *x41*x52
        +coeff[ 91]*x11    *x33*x44    
        +coeff[ 92]    *x24    *x43*x51
        +coeff[ 93]    *x22    *x42    
        +coeff[ 94]*x13                
        +coeff[ 95]        *x34        
        +coeff[ 96]            *x44*x51
        ;

    return v_y_e_dq_125                                  ;
}
float p_e_dq_125                                  (float *x,int m){
    int ncoeff= 37;
    float avdat=  0.1734312E-04;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 38]={
        -0.23260674E-04,-0.29157205E-01, 0.97482381E-02,-0.60518081E-02,
        -0.15518840E-01, 0.50705243E-02, 0.11443464E-01,-0.90549001E-02,
        -0.74450456E-03,-0.26064888E-02,-0.26614014E-02, 0.47641498E-03,
        -0.22368459E-02,-0.12532887E-02,-0.38064513E-03, 0.21028377E-02,
        -0.13159148E-02,-0.12355549E-02, 0.65365653E-04, 0.25191411E-03,
        -0.37392674E-03,-0.15217176E-02, 0.50624396E-03,-0.90775179E-03,
        -0.32397764E-03,-0.20882578E-02,-0.11343525E-02,-0.50565641E-03,
         0.95744606E-03, 0.19407616E-03,-0.23286614E-03, 0.51217398E-03,
         0.23078892E-03,-0.18710030E-03, 0.10150776E-03,-0.22118351E-03,
         0.50013000E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_125                                  =v_p_e_dq_125                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_125                                  =v_p_e_dq_125                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x21*x31    *x52
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_p_e_dq_125                                  =v_p_e_dq_125                                  
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x23*x31    *x51
        +coeff[ 28]    *x23    *x41*x51
        +coeff[ 29]*x11*x23*x31        
        +coeff[ 30]            *x41*x52
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21    *x41*x52
    ;
    v_p_e_dq_125                                  =v_p_e_dq_125                                  
        +coeff[ 35]    *x22*x31    *x52
        +coeff[ 36]    *x22    *x41*x52
        ;

    return v_p_e_dq_125                                  ;
}
float l_e_dq_125                                  (float *x,int m){
    int ncoeff= 35;
    float avdat= -0.2290628E-01;
    float xmin[10]={
        -0.39992E-02,-0.57595E-01,-0.59971E-01,-0.30030E-01,-0.39994E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39999E-02, 0.60047E-01, 0.59978E-01, 0.29981E-01, 0.39996E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 36]={
         0.15970554E-01,-0.33124357E+00,-0.25797915E-01, 0.11749979E-01,
        -0.31632014E-01,-0.15489327E-01,-0.56455047E-02,-0.50119087E-02,
         0.20148864E-02, 0.67704991E-02, 0.74707852E-02, 0.48740705E-04,
        -0.39548202E-04, 0.12955788E-02,-0.30368306E-02,-0.85192022E-03,
        -0.10659809E-02, 0.40679043E-02,-0.18021336E-02, 0.15593824E-02,
         0.85060223E-03,-0.19908007E-03, 0.61872782E-03, 0.57977583E-03,
         0.52680762E-03, 0.51980792E-03,-0.11188686E-02, 0.90909339E-02,
         0.54175488E-03, 0.67516244E-02, 0.75633582E-02,-0.84654574E-03,
         0.10871391E-02,-0.12082354E-02, 0.67382799E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_125                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_125                                  =v_l_e_dq_125                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dq_125                                  =v_l_e_dq_125                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]                *x52
        +coeff[ 22]        *x32    *x51
        +coeff[ 23]            *x42*x51
        +coeff[ 24]*x11*x22            
        +coeff[ 25]*x11    *x31*x41    
    ;
    v_l_e_dq_125                                  =v_l_e_dq_125                                  
        +coeff[ 26]    *x21*x31*x41*x51
        +coeff[ 27]    *x23*x31*x41    
        +coeff[ 28]        *x34*x41    
        +coeff[ 29]    *x23    *x42    
        +coeff[ 30]    *x21*x31*x43    
        +coeff[ 31]*x11*x21*x32    *x51
        +coeff[ 32]    *x24    *x42    
        +coeff[ 33]    *x23*x32    *x51
        +coeff[ 34]    *x21*x32*x44    
        ;

    return v_l_e_dq_125                                  ;
}
float x_e_dq_100                                  (float *x,int m){
    int ncoeff= 26;
    float avdat=  0.5096534E+00;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 27]={
        -0.34032019E-02,-0.87495642E-02, 0.96461229E-01, 0.20806071E+00,
         0.24394002E-01, 0.16671460E-01,-0.32829174E-02,-0.10091496E-02,
        -0.31684323E-02, 0.18718009E-02,-0.81962328E-02,-0.74046711E-02,
        -0.48570229E-04,-0.11367156E-02,-0.10854724E-02,-0.19398555E-02,
         0.96853438E-03,-0.36939515E-02,-0.51524711E-03,-0.24527652E-03,
        -0.39537461E-03,-0.13459531E-02,-0.55788383E-02,-0.33612470E-02,
        -0.26086406E-02,-0.42249574E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_x_e_dq_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]*x11                
        +coeff[  2]                *x51
        +coeff[  3]    *x21            
        +coeff[  4]    *x21        *x51
        +coeff[  5]    *x22            
        +coeff[  6]                *x52
        +coeff[  7]*x11*x21            
    ;
    v_x_e_dq_100                                  =v_x_e_dq_100                                  
        +coeff[  8]            *x42    
        +coeff[  9]    *x22        *x51
        +coeff[ 10]    *x21*x31*x41    
        +coeff[ 11]    *x21    *x42    
        +coeff[ 12]*x12*x21*x32        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]        *x31*x41    
        +coeff[ 16]    *x23            
    ;
    v_x_e_dq_100                                  =v_x_e_dq_100                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]            *x42*x51
        +coeff[ 19]    *x21        *x52
        +coeff[ 20]*x11*x22            
        +coeff[ 21]    *x22*x31*x41    
        +coeff[ 22]    *x23*x31*x41    
        +coeff[ 23]    *x23    *x42    
        +coeff[ 24]    *x21*x31*x43    
        +coeff[ 25]    *x23*x32*x42    
        ;

    return v_x_e_dq_100                                  ;
}
float t_e_dq_100                                  (float *x,int m){
    int ncoeff= 50;
    float avdat=  0.5707594E+00;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 51]={
         0.31780007E-02,-0.10853007E+00, 0.24171140E-01,-0.71412530E-02,
        -0.11600578E-05, 0.32785460E-02,-0.29100250E-02,-0.93875016E-03,
        -0.10887122E-02, 0.33466935E-02, 0.11261976E-02, 0.10408621E-02,
         0.30717367E-03, 0.38606074E-03, 0.39955880E-03, 0.62863866E-03,
         0.24450189E-03,-0.15609786E-03, 0.82993676E-03, 0.91119495E-04,
         0.45560676E-03, 0.91206760E-03,-0.61900634E-03, 0.24068078E-03,
         0.18681691E-02, 0.15840765E-03,-0.24810570E-03, 0.47099538E-03,
        -0.23539984E-03, 0.23773597E-02, 0.94152776E-04,-0.47862937E-04,
        -0.51796559E-03,-0.52407454E-03, 0.13324580E-02, 0.12664864E-02,
         0.23812920E-03,-0.23438982E-03, 0.33586260E-03,-0.40833032E-03,
        -0.27667420E-03, 0.61202729E-04, 0.33803997E-04, 0.66548571E-04,
         0.89875823E-04, 0.53552591E-04, 0.81572412E-04, 0.65468412E-04,
        -0.65194334E-04, 0.63815511E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_t_e_dq_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]    *x21        *x51
        +coeff[  4]*x13                
        +coeff[  5]*x11                
        +coeff[  6]    *x22            
        +coeff[  7]                *x52
    ;
    v_t_e_dq_100                                  =v_t_e_dq_100                                  
        +coeff[  8]        *x31*x41    
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x23*x32        
        +coeff[ 12]*x11*x21            
        +coeff[ 13]            *x42    
        +coeff[ 14]            *x42*x51
        +coeff[ 15]    *x22*x32        
        +coeff[ 16]        *x32        
    ;
    v_t_e_dq_100                                  =v_t_e_dq_100                                  
        +coeff[ 17]*x11            *x51
        +coeff[ 18]    *x21*x32        
        +coeff[ 19]    *x22        *x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]    *x22    *x42    
        +coeff[ 22]    *x23        *x51
        +coeff[ 23]    *x21*x32    *x51
        +coeff[ 24]    *x23    *x42    
        +coeff[ 25]    *x23            
    ;
    v_t_e_dq_100                                  =v_t_e_dq_100                                  
        +coeff[ 26]        *x32    *x51
        +coeff[ 27]    *x22*x31*x41    
        +coeff[ 28]    *x22        *x52
        +coeff[ 29]    *x23*x31*x41    
        +coeff[ 30]*x11*x22            
        +coeff[ 31]*x11*x21        *x51
        +coeff[ 32]    *x21*x31*x41*x51
        +coeff[ 33]    *x21    *x42*x51
        +coeff[ 34]    *x21*x32*x42    
    ;
    v_t_e_dq_100                                  =v_t_e_dq_100                                  
        +coeff[ 35]    *x21*x31*x43    
        +coeff[ 36]    *x22*x32    *x51
        +coeff[ 37]    *x22*x31*x41*x51
        +coeff[ 38]    *x22    *x42*x51
        +coeff[ 39]    *x23        *x52
        +coeff[ 40]    *x21*x31*x41*x52
        +coeff[ 41]*x11        *x42    
        +coeff[ 42]                *x53
        +coeff[ 43]*x11*x23            
    ;
    v_t_e_dq_100                                  =v_t_e_dq_100                                  
        +coeff[ 44]        *x33*x41    
        +coeff[ 45]*x11*x22        *x51
        +coeff[ 46]        *x32    *x52
        +coeff[ 47]            *x42*x52
        +coeff[ 48]    *x21        *x53
        +coeff[ 49]*x13    *x31*x41    
        ;

    return v_t_e_dq_100                                  ;
}
float y_e_dq_100                                  (float *x,int m){
    int ncoeff= 97;
    float avdat= -0.9876643E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 98]={
         0.92688884E-03, 0.99271394E-01,-0.10046981E+00,-0.69303758E-01,
        -0.24870394E-01, 0.47204010E-01, 0.27859375E-01,-0.34041654E-01,
        -0.14611125E-01,-0.70143761E-02, 0.21875352E-02,-0.99584386E-02,
        -0.23274976E-02,-0.52991225E-04,-0.37890757E-03, 0.82185300E-03,
        -0.64522377E-02,-0.14943539E-02, 0.15256188E-02,-0.30043831E-02,
        -0.18602532E-02, 0.58159954E-02,-0.53479420E-02, 0.52848546E-03,
         0.17069387E-02,-0.19006947E-02,-0.14611344E-02, 0.17023175E-02,
         0.28473902E-02, 0.16434571E-02,-0.16069068E-02,-0.36319129E-02,
        -0.22751456E-02,-0.77722385E-03, 0.12348850E-02, 0.39075673E-03,
        -0.36035138E-03, 0.82515337E-03, 0.35507043E-03,-0.90304238E-03,
        -0.15017027E-02, 0.13111180E-02,-0.28471191E-04, 0.38884373E-04,
        -0.29112391E-04,-0.14372620E-03, 0.22164211E-03,-0.76653977E-03,
         0.79497934E-03, 0.25084455E-04,-0.45832832E-04, 0.60016476E-03,
        -0.81277336E-04,-0.25911423E-03, 0.47558997E-03,-0.61873427E-04,
         0.10872546E-03, 0.64797868E-03, 0.28155642E-03,-0.63931824E-04,
        -0.31860513E-03,-0.11029630E-01,-0.35547542E-02,-0.12906405E-01,
        -0.55839727E-02,-0.36627208E-02, 0.23728427E-02, 0.50748959E-02,
         0.35367251E-03, 0.71634357E-02, 0.49154009E-02, 0.17002509E-02,
        -0.26124908E-03,-0.35234836E-04,-0.21554748E-04,-0.25887945E-04,
         0.46166759E-04,-0.42502441E-04, 0.52243289E-04, 0.96560450E-03,
        -0.13945661E-03,-0.60285814E-03, 0.11047028E-02,-0.81466504E-04,
         0.84287116E-04,-0.53605781E-03, 0.27718453E-03,-0.35402267E-02,
        -0.71353139E-02,-0.20673482E-02,-0.42256294E-03,-0.73262659E-03,
        -0.22921447E-05, 0.12246924E-04, 0.58295327E-05,-0.52138066E-05,
        -0.16449081E-04,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x44 = x43*x4;
    float x45 = x44*x4;
    float x51 = x5;
    float x52 = x51*x5;
    float x53 = x52*x5;

//                 function

    float v_y_e_dq_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]            *x41    
        +coeff[  2]        *x31        
        +coeff[  3]    *x21    *x41    
        +coeff[  4]    *x21*x31        
        +coeff[  5]            *x41*x51
        +coeff[  6]        *x31    *x51
        +coeff[  7]    *x22    *x41    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[  8]    *x22*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]*x11        *x41    
        +coeff[ 11]        *x31*x42    
        +coeff[ 12]    *x21*x31    *x51
        +coeff[ 13]            *x45    
        +coeff[ 14]        *x32*x43    
        +coeff[ 15]*x11    *x31        
        +coeff[ 16]        *x32*x41    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 17]        *x33        
        +coeff[ 18]*x11*x21    *x41    
        +coeff[ 19]            *x41*x52
        +coeff[ 20]        *x31    *x52
        +coeff[ 21]    *x23    *x41    
        +coeff[ 22]            *x43    
        +coeff[ 23]*x11*x21*x31        
        +coeff[ 24]    *x23*x31        
        +coeff[ 25]    *x22    *x41*x51
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 26]    *x22*x31    *x51
        +coeff[ 27]    *x21    *x43    
        +coeff[ 28]    *x21*x31*x42    
        +coeff[ 29]    *x21*x32*x41    
        +coeff[ 30]    *x22    *x43    
        +coeff[ 31]    *x22*x31*x42    
        +coeff[ 32]    *x22*x32*x41    
        +coeff[ 33]    *x24*x33        
        +coeff[ 34]        *x31*x42*x51
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 35]    *x21*x33        
        +coeff[ 36]*x11*x22    *x41    
        +coeff[ 37]        *x32*x41*x51
        +coeff[ 38]    *x21    *x41*x52
        +coeff[ 39]    *x24    *x41    
        +coeff[ 40]    *x24*x31        
        +coeff[ 41]    *x23    *x41*x51
        +coeff[ 42]            *x45*x51
        +coeff[ 43]    *x21            
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 44]                *x51
        +coeff[ 45]        *x31*x44    
        +coeff[ 46]        *x33    *x51
        +coeff[ 47]    *x22*x33        
        +coeff[ 48]    *x22    *x41*x52
        +coeff[ 49]    *x22            
        +coeff[ 50]*x11        *x41*x51
        +coeff[ 51]            *x43*x51
        +coeff[ 52]*x11*x21    *x41*x51
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 53]        *x33*x42    
        +coeff[ 54]*x11*x21    *x43    
        +coeff[ 55]        *x34*x41    
        +coeff[ 56]            *x41*x53
        +coeff[ 57]*x11*x21*x31*x42    
        +coeff[ 58]*x11*x21*x32*x41    
        +coeff[ 59]    *x22    *x43*x51
        +coeff[ 60]    *x24    *x41*x51
        +coeff[ 61]    *x22*x31*x44    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 62]    *x24    *x43    
        +coeff[ 63]    *x22*x32*x43    
        +coeff[ 64]    *x24*x31*x42    
        +coeff[ 65]    *x24*x32*x41    
        +coeff[ 66]    *x23    *x45    
        +coeff[ 67]    *x23*x31*x44    
        +coeff[ 68]    *x21*x33*x44    
        +coeff[ 69]    *x23*x32*x43    
        +coeff[ 70]    *x23*x33*x42    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 71]    *x23*x34*x41    
        +coeff[ 72]    *x24    *x45    
        +coeff[ 73]*x12        *x41    
        +coeff[ 74]*x12    *x31        
        +coeff[ 75]*x11    *x31    *x51
        +coeff[ 76]*x11    *x31*x42    
        +coeff[ 77]*x11*x22*x31        
        +coeff[ 78]        *x31    *x53
        +coeff[ 79]    *x21*x31*x44    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 80]    *x23*x31    *x51
        +coeff[ 81]    *x23    *x43    
        +coeff[ 82]    *x21*x32*x43    
        +coeff[ 83]*x11*x22    *x41*x51
        +coeff[ 84]    *x21    *x41*x53
        +coeff[ 85]    *x22*x31*x42*x51
        +coeff[ 86]    *x23*x33        
        +coeff[ 87]    *x22    *x45    
        +coeff[ 88]    *x22*x33*x42    
    ;
    v_y_e_dq_100                                  =v_y_e_dq_100                                  
        +coeff[ 89]    *x22*x34*x41    
        +coeff[ 90]    *x24    *x41*x52
        +coeff[ 91]    *x24    *x43*x51
        +coeff[ 92]*x11                
        +coeff[ 93]        *x31*x41    
        +coeff[ 94]        *x32        
        +coeff[ 95]*x11*x21            
        +coeff[ 96]    *x21*x31*x41    
        ;

    return v_y_e_dq_100                                  ;
}
float p_e_dq_100                                  (float *x,int m){
    int ncoeff= 36;
    float avdat= -0.1287478E-03;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 37]={
         0.12520429E-03,-0.29879186E-01, 0.88247424E-02,-0.59579359E-02,
        -0.15616428E-01, 0.50114864E-02, 0.11395885E-01,-0.89575220E-02,
        -0.73950959E-03,-0.25835638E-02,-0.25940351E-02, 0.46723874E-03,
        -0.21802715E-02,-0.12278559E-02,-0.32676963E-03, 0.21064423E-02,
        -0.13399795E-02,-0.12415886E-02, 0.80615937E-04, 0.24638631E-03,
        -0.35102369E-03,-0.14943783E-02, 0.50667499E-03,-0.92823367E-03,
        -0.34457503E-03,-0.21302027E-02,-0.11513487E-02,-0.59462176E-03,
         0.93697931E-03, 0.11178611E-03,-0.16724659E-03, 0.48677463E-03,
         0.23731944E-03,-0.19500793E-03, 0.11721122E-03,-0.24350126E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_p_e_dq_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]        *x31        
        +coeff[  2]            *x41    
        +coeff[  3]    *x21*x31        
        +coeff[  4]    *x21    *x41    
        +coeff[  5]        *x31    *x51
        +coeff[  6]            *x41*x51
        +coeff[  7]    *x22    *x41    
    ;
    v_p_e_dq_100                                  =v_p_e_dq_100                                  
        +coeff[  8]    *x24*x31        
        +coeff[  9]    *x21    *x41*x51
        +coeff[ 10]    *x22*x31        
        +coeff[ 11]*x11        *x41    
        +coeff[ 12]        *x31*x42    
        +coeff[ 13]            *x43    
        +coeff[ 14]    *x21*x31    *x51
        +coeff[ 15]    *x23    *x41    
        +coeff[ 16]    *x22*x31    *x51
    ;
    v_p_e_dq_100                                  =v_p_e_dq_100                                  
        +coeff[ 17]    *x22*x32*x41    
        +coeff[ 18]        *x34*x41    
        +coeff[ 19]*x11    *x31        
        +coeff[ 20]        *x33        
        +coeff[ 21]        *x32*x41    
        +coeff[ 22]*x11*x21    *x41    
        +coeff[ 23]    *x22    *x41*x51
        +coeff[ 24]    *x21*x31    *x52
        +coeff[ 25]    *x22*x31*x42    
    ;
    v_p_e_dq_100                                  =v_p_e_dq_100                                  
        +coeff[ 26]    *x22    *x43    
        +coeff[ 27]    *x23*x31    *x51
        +coeff[ 28]    *x23    *x41*x51
        +coeff[ 29]*x11*x21*x31        
        +coeff[ 30]    *x23*x31        
        +coeff[ 31]    *x21*x31*x42    
        +coeff[ 32]    *x21    *x43    
        +coeff[ 33]*x11*x22    *x41    
        +coeff[ 34]    *x21    *x41*x52
    ;
    v_p_e_dq_100                                  =v_p_e_dq_100                                  
        +coeff[ 35]    *x22*x31    *x52
        ;

    return v_p_e_dq_100                                  ;
}
float l_e_dq_100                                  (float *x,int m){
    int ncoeff= 31;
    float avdat= -0.2316446E-01;
    float xmin[10]={
        -0.39990E-02,-0.57012E-01,-0.59990E-01,-0.30045E-01,-0.39997E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.39995E-02, 0.59763E-01, 0.59992E-01, 0.30008E-01, 0.39998E-01,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 32]={
         0.15170880E-01,-0.33560491E+00,-0.25914390E-01, 0.11460764E-01,
        -0.32222155E-01,-0.15258868E-01,-0.56256866E-02,-0.49713817E-02,
         0.21517468E-02, 0.85487170E-02, 0.80046421E-02,-0.99350276E-04,
         0.22807285E-03, 0.79607335E-03,-0.32148876E-02,-0.39085647E-03,
        -0.12350148E-02, 0.42319708E-02,-0.17725691E-02, 0.15177130E-02,
         0.88779273E-03, 0.60355803E-03, 0.88590122E-03, 0.16604100E-02,
        -0.34572222E-03, 0.53746235E-02, 0.49445364E-02, 0.25697853E-02,
        -0.57792763E-03,-0.16727541E-02,-0.18001162E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
    float x5 =1.+(x[  4]-xmax[  4])*scale[  4];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x13 = x12*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x23 = x22*x2;
    float x24 = x23*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x34 = x33*x3;
    float x41 = x4;
    float x42 = x41*x4;
    float x43 = x42*x4;
    float x51 = x5;
    float x52 = x51*x5;

//                 function

    float v_l_e_dq_100                                  =avdat
        +coeff[  0]                    
        +coeff[  1]    *x21            
        +coeff[  2]                *x51
        +coeff[  3]*x11                
        +coeff[  4]    *x22            
        +coeff[  5]    *x21        *x51
        +coeff[  6]        *x31*x41    
        +coeff[  7]            *x42    
    ;
    v_l_e_dq_100                                  =v_l_e_dq_100                                  
        +coeff[  8]*x11*x21            
        +coeff[  9]    *x21*x31*x41    
        +coeff[ 10]    *x21    *x42    
        +coeff[ 11]    *x22*x32        
        +coeff[ 12]        *x34        
        +coeff[ 13]    *x23*x32        
        +coeff[ 14]        *x32        
        +coeff[ 15]*x11            *x51
        +coeff[ 16]    *x23            
    ;
    v_l_e_dq_100                                  =v_l_e_dq_100                                  
        +coeff[ 17]    *x21*x32        
        +coeff[ 18]    *x22        *x51
        +coeff[ 19]        *x31*x41*x51
        +coeff[ 20]    *x21        *x52
        +coeff[ 21]        *x32    *x51
        +coeff[ 22]            *x42*x51
        +coeff[ 23]*x11*x22            
        +coeff[ 24]*x11*x21    *x41    
        +coeff[ 25]    *x23*x31*x41    
    ;
    v_l_e_dq_100                                  =v_l_e_dq_100                                  
        +coeff[ 26]    *x23    *x42    
        +coeff[ 27]    *x21*x31*x43    
        +coeff[ 28]*x13            *x51
        +coeff[ 29]*x11*x24            
        +coeff[ 30]    *x21*x31*x42*x52
        ;

    return v_l_e_dq_100                                  ;
}
