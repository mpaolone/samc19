float x_e_corr_1102                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.5397664E-03;
    float xmin[10]={
        -0.48367E+00, 0.84552E+00,-0.37394E-01,-0.60666E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.35864E+00, 0.11290E+01, 0.38651E-01, 0.61894E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.24451304E-02, 0.25645811E-01,-0.34988035E-01, 0.62708226E-02,
         0.10980926E-01, 0.14897001E-01,-0.19227669E-01, 0.24231309E-02,
        -0.51053073E-02,-0.17827105E-02,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;

//                 function

    float v_x_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x11*x21        
        +coeff[  6]    *x22        
        +coeff[  7]*x11    *x31    
    ;
    v_x_e_corr                                =v_x_e_corr                                
        +coeff[  8]    *x21*x31    
        +coeff[  9]        *x32    
        ;

    return v_x_e_corr                                ;
}
float t_e_corr_1102                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.2173311E-02;
    float xmin[10]={
        -0.48367E+00, 0.84552E+00,-0.37394E-01,-0.60666E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.35864E+00, 0.11290E+01, 0.38651E-01, 0.61894E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
         0.34091677E-03, 0.32884139E-02,-0.28410445E-02, 0.66579110E-03,
         0.34734097E-02,-0.69025853E-02, 0.20261154E-01,-0.15690522E-01,
         0.88833282E-02,-0.13310668E-01,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x41 = x4;

//                 function

    float v_t_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x12            
        +coeff[  6]*x11*x21        
        +coeff[  7]    *x22        
    ;
    v_t_e_corr                                =v_t_e_corr                                
        +coeff[  8]*x11    *x31    
        +coeff[  9]    *x21*x31    
        ;

    return v_t_e_corr                                ;
}
float y_e_corr_1102                                (float *x,int m){
    int ncoeff= 20;
    float avdat= -0.6866083E-02;
    float xmin[10]={
        -0.48367E+00, 0.84552E+00,-0.37394E-01,-0.60666E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.35864E+00, 0.11290E+01, 0.38651E-01, 0.61894E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 21]={
         0.18458602E-03,-0.11000095E-02, 0.15838265E-02,-0.52423368E-03,
         0.57752419E-03,-0.11051942E-02, 0.19083997E-02,-0.85438119E-03,
         0.14982902E-02, 0.71955092E-05,-0.82307281E-02, 0.14194635E-01,
         0.73587435E-03, 0.19946236E-02, 0.48789009E-02,-0.88720880E-02,
        -0.63417717E-02, 0.64755846E-02,-0.18869372E-02,-0.72803313E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x12 = x11*x1;
    float x21 = x2;
    float x22 = x21*x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x33 = x32*x3;
    float x41 = x4;
    float x42 = x41*x4;

//                 function

    float v_y_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]        *x31    
        +coeff[  4]            *x41
        +coeff[  5]*x12            
        +coeff[  6]*x11*x21        
        +coeff[  7]    *x22        
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[  8]    *x21*x31    
        +coeff[  9]        *x32    
        +coeff[ 10]*x11        *x41
        +coeff[ 11]    *x21    *x41
        +coeff[ 12]            *x42
        +coeff[ 13]*x12    *x31    
        +coeff[ 14]*x11*x21*x31    
        +coeff[ 15]    *x22*x31    
        +coeff[ 16]*x11    *x32    
    ;
    v_y_e_corr                                =v_y_e_corr                                
        +coeff[ 17]    *x21*x32    
        +coeff[ 18]        *x33    
        +coeff[ 19]*x12        *x41
        ;

    return v_y_e_corr                                ;
}
float p_e_corr_1102                                (float *x,int m){
    int ncoeff= 10;
    float avdat=  0.9797192E-03;
    float xmin[10]={
        -0.48367E+00, 0.84552E+00,-0.37394E-01,-0.60666E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float xmax[10]={
         0.35864E+00, 0.11290E+01, 0.38651E-01, 0.61894E-01, 0.00000E+00,
         0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00, 0.00000E+00};
    float scale[10]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    float coeff[ 11]={
        -0.17790102E-03, 0.22716797E-02,-0.29792630E-02, 0.13125490E-02,
        -0.16380531E-02, 0.35210451E-03, 0.16696487E-01,-0.22381384E-01,
        -0.84834249E-03, 0.58101135E-03,
              0.      };
    int ientry=0;

    if (ientry==0){
        ientry=1;
        for(int i=0;i<m;i++){
            if(xmin[i]==xmax[i]) continue;
            scale[i]=2./(xmax[i]-xmin[i]);
        }
    }
//  normalize variables between -1 and +1
    float x1 =1.+(x[  0]-xmax[  0])*scale[  0];
    float x2 =1.+(x[  1]-xmax[  1])*scale[  1];
    float x3 =1.+(x[  2]-xmax[  2])*scale[  2];
    float x4 =1.+(x[  3]-xmax[  3])*scale[  3];
//  set up monomials   functions
    float x11 = x1;
    float x21 = x2;
    float x31 = x3;
    float x32 = x31*x3;
    float x41 = x4;
    float x42 = x41*x4;

//                 function

    float v_p_e_corr                                =avdat
        +coeff[  0]                
        +coeff[  1]*x11            
        +coeff[  2]    *x21        
        +coeff[  3]            *x41
        +coeff[  4]    *x21*x31    
        +coeff[  5]        *x32    
        +coeff[  6]*x11        *x41
        +coeff[  7]    *x21    *x41
    ;
    v_p_e_corr                                =v_p_e_corr                                
        +coeff[  8]        *x31*x41
        +coeff[  9]            *x42
        ;

    return v_p_e_corr                                ;
}
