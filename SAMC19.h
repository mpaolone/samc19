#include <algorithm>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include "TRandom3.h"
#include "TTree.h"
#include "TF1.h"
#include "TFile.h"
#include "TMath.h"
#include "TString.h"
#include "SAMC19_optics.h"

using namespace std;

//need gloabal TRandom3
TRandom3 *rand3 = new TRandom3();

//for materials
struct Material{
	TString name = "";
	float L = 0;
	float Zloc = 0;
	float Z = 0;
	float A = 0;
	float X0 = 0;
	float rho = 0;
	float T = 0;
	float bt = 0;
	float TR = 0;
	float angle = 0;
	float xoff = 0.0;
	
	Material() = default;
};

//these are the global variables used in generation.  They should be set initially, and then not touched by the event processor.
struct SAMCVars{
	bool IsMultiScat = 1;
	bool IsEnergyLoss = 1;
	int transOpt = 0;
	int reconOpt = 0;
	int matOpt = 0;
	int numEvents = 0;
	int setSeed = 0;
	float beamE = 0;
	float centP = 0;
	float angle = 0;
	float dpGenLim = 0;
	float thGenLim = 0;
	float phGenLim = 0;
	float bxCent = 0;
	float bxGenLim = 0.01;
	float byCent = 0;
	float byGenLim = 0.01;
	float missPointX = 0;
	float missPointY = 0;
	float mass = 0.511;
	int armv = 0;  //0 for left arm, 1 for right arm
	int numWeights = 0;
	float resScale = 1.0;
	float resOff = 0.0;
	TString inputFile = "";
	TString opticsFile = "";
	TString outputFile = "SAMCout.root";
	TString spectrometerOpt = "";
	vector<Material> vTarg = {};
	vector<Material> vPreMag = {};
	vector<Material> vPostMag = {};
	TF1 *fcauchy = NULL;
	
	SAMCVars() = default;
};

//here are all the root output variables.  See SAMC19_IO.h for how they are included into the tree.
struct SAMCRootVars{
	float dpgen = 0;
	float thgen = 0;
	float phgen = 0;
	float dp_ref = 0;
	float th_tg_ref = 0;
	float ph_tg_ref = 0;
	float y_tg_ref = 0;
	float dp_rec = 0;
	float th_tg_rec = 0;
	float ph_tg_rec = 0;
	float y_tg_rec = 0;
	float xd = 0.0;
	float yd = 0.0;
	float yd2 = 0.0;
	float thd = 0.0;
	float phd = 0.0;
	float phd2 = 0.0;
	float x_fp = 0.0;
	float y_fp = 0.0;
	float th_fp = 0.0;
	float ph_fp = 0.0;
	float beamx = 0;
	float beamy = 0;
	float zvert = 0;
	vector<float> weight = {};
	int IsPassed = 0;
	
	SAMCRootVars() = default;
};


