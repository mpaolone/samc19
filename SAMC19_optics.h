#include <cstdlib> 
#include <iostream>
#include <iomanip>
#include <vector> 
#include <fstream> 
#include "TRandom3.h"

using namespace std;

//for optics matrix:
class MatrixElement{
	private: 
		bool fIsZero = false;              // whether or not the element is zero
		int fOrder = 0;                // order of the polynomial 
        int fDefaultPolyOrder = 0;     // default order of the polynomial 
		double fV = 0;                 // its computed value
		vector<int> fPW = {};           // exponents of matrix element (e.g., D100 = { 1, 0, 0 })
		vector<double> fPoly = {};      // the associated polynomial
	public:
		void Clear(){
			fPoly.clear();
			fPW.clear();
			fIsZero = false;
			fOrder = 0;
			fDefaultPolyOrder = 0;
			fV = 0;
		}
		MatrixElement(){
			Clear();
		};
		void SetValue(double v){fV = v;}; 
		void SetBoolean(bool v){fIsZero = v;}; 
		void SetPolyOrder(int order){fOrder = order;}; 
		void SetExpoSize(int i){fPW.resize(i);}; 
		void SetPolySize(int i){fPoly.resize(i);}; 
		void SetExpo(int i,int v){fPW[i] = v;}; 
		void SetPoly(int i,double v){fPoly[i] = v;};
		int GetPolyOrder(){return fOrder;}; 
		int GetExpo(int i){return fPW[i];}; 
		int GetExpoSize(){return fPW.size();}; 
		double GetValue(){return fV;}; 
		double GetPoly(int i){return fPoly[i];};
		bool IsElementZero(){return fIsZero;}; 
		//bool Match( const MatrixElement& rhs ) const;
};

class SAMCOptics{
private:
	TRandom3 *randOpt;
	int fOpticsPackage=0;
	float fP0 = -1;
	float fScatAngle=0;
	vector<MatrixElement> fFP = {};          // transport to focal plane: fFP stores t,y,p in that order (size 3)  
	vector<MatrixElement> fY = {};
	vector<MatrixElement> fD = {};
	vector<MatrixElement> fP = {};
	vector<MatrixElement> fT = {};  // focal plane to target: Y = y, D = delta p, P = phi, T = theta 
	vector<MatrixElement> fYTA = {};
	vector<MatrixElement> fPTA = {}; // focal plane to target: YTA and PTA utilize abs(th_fp)
	vector<MatrixElement> fL = {};
	enum EFPMatrixElemTags {T000,Y000,P000};
public:
	TString opticsFileName = "";
	SAMCOptics(TString fileName): opticsFileName(fileName){
		randOpt = new TRandom3();
		loadData();
	};
	void loadData(){
		
		MatrixElement ME; // matrix element object for storing data  
		ME.SetPolyOrder(0);

		string iVar;
		int SIZE;
		int iOpt,iDEG,iI,iJ,iK;
		double iVal1,iVal2,iVal3,iVal4,iVal5,iVal6,iVal7; 
		vector<int> E;
		vector<double> V;

		// for CSR optics
		// choose DB file based on momentum  
		int MyP0; 
		ME.SetExpoSize(3); 

		ifstream infile; 
		infile.open(opticsFileName); 
		if(infile.fail()){
			cout << "Cannot open the file: " << opticsFileName << endl;
			exit(1);
		}else{
			while(!infile.eof()){
					infile >> iVar  >> iI    >> iJ    >> iK 
						   >> iVal1 >> iVal2 >> iVal3 >> iVal4 >> iVal5 >> iVal6 >> iVal7 
						   >> iDEG;  
					E.push_back(iI);
					E.push_back(iJ);
					E.push_back(iK);
					V.push_back(iVal1);
					V.push_back(iVal2);
					V.push_back(iVal3);
					V.push_back(iVal4);
					V.push_back(iVal5);
					V.push_back(iVal6);
					V.push_back(iVal7);
				SIZE = E.size();
				ME.SetExpoSize(SIZE); 
				for(int i=0;i<SIZE;i++){
					ME.SetExpo(i,E[i]);
				}
				SIZE = V.size();  
				ME.SetPolySize(SIZE); 
				for(int i=0;i<SIZE;i++){
					ME.SetPoly(i,V[i]);
					if(V[i]!=0){
						ME.SetBoolean(false);
						ME.SetPolyOrder(i+1);
					} 
				}
				if( ME.IsElementZero() ){
					SIZE = E.size();
					for(int i=0;i<SIZE;i++){
						cout << E[i] << "\t";
					}
					cout << endl;
					ME.Clear();
					V.clear();
					E.clear();
					continue;
				}
				// push back on the right matrix element vector 
				if(iVar=="t" || iVar=="y" || iVar=="p") fFP.push_back(ME); 
				if(iVar=="Y")   fY.push_back(ME); 
				if(iVar=="D")   fD.push_back(ME); 
				if(iVar=="T")   fT.push_back(ME); 
				if(iVar=="P")   fP.push_back(ME); 
				if(iVar=="YTA") fYTA.push_back(ME); 
				if(iVar=="PTA") fPTA.push_back(ME); 
				if(iVar=="L")   fL.push_back(ME); 
				ME.Clear();
				V.clear();
				E.clear();
			}
			infile.close();
			//Y is last, and needs to be popped back (read twice at end of file)
			fY.pop_back();
		}
		
	};
	void CalculateTargetCoords(const vector<double> &FPVar, vector<double> &TgVar, const float x_tg = 0.0){
		// calculates the target coordinates from the focal plane coordinates 

		const int kNUM_PRECOMP_POW = 10;         // wtf? 
		double x_fp=0,y_fp=0,th_fp=0,ph_fp=0;    // focal plane variables 
		double y_tg=0,th_tg=0,ph_tg=0,dp=0;      // target variables
		double powers[kNUM_PRECOMP_POW][5];

		// clear the target vector (to be safe) 
		TgVar.clear();

		// fill focal plane variables.  I think the standard units are meters
		x_fp  = FPVar[0]/100.; // convert to meters  
		y_fp  = FPVar[1]/100.; // convert to meters
		th_fp = FPVar[2]; 
		ph_fp = FPVar[3];
	
		// calculate the powers we need
		for(int i=0;i<kNUM_PRECOMP_POW;i++){
			powers[i][0] = pow(x_fp,i);
			powers[i][1] = pow(th_fp,i);
			powers[i][2] = pow(y_fp,i);
			powers[i][3] = pow(ph_fp,i);
			powers[i][4] = pow(fabs(th_fp),i);
		}
	
	

		// calculate the matrices we need
		CalculateMatrix(x_fp,fD);
		CalculateMatrix(x_fp,fT);
		CalculateMatrix(x_fp,fY);
		CalculateMatrix(x_fp,fP);
		// CalculateMatrix(x_fp,fYTA);
		// CalculateMatrix(x_fp,fPTA);

		// calculate the coordinates at the target
		// FIXME: what about the PTA, YTA matrix elements?? They are EMPTY! 
		th_tg = CalculateTargetVar(fT,powers);
		ph_tg = CalculateTargetVar(fP,powers);
		// ph_tg = CalculateTargetVar(fP,powers) + CalculateTargetVar(fPTA,powers);
		// y_tg  = CalculateTargetVar(fY,powers) + CalculateTargetVar(fYTA,powers);
		y_tg  = CalculateTargetVar(fY,powers);  
		dp    = CalculateTargetVar(fD,powers);
		
	    double DeltaTh = 0.61*x_tg/100.0;
	    double DeltaDp = x_tg/5.18/100.0;
	
	
		//dp += DeltaDp/2.0;
		//th_tg += DeltaTh/2.0;
		         

		// fill the vector of target variables.  note the order!  
		y_tg *= 100.;           // convert back to cm 
		TgVar.push_back(y_tg);  
		TgVar.push_back(th_tg);
		TgVar.push_back(ph_tg);
		TgVar.push_back(dp); 

	};
	float TgtCompDiff(const vector<double> fp, const vector<double> tg, const double x_tg = 0.0){
		//calculate the difference between input target variables and ones calculated by focal plane transport
	
		//A note on these hard-coded numbers:  They were determined by randomly generating a grid of the focal-plane, and transporting back to the target.  This was done with CSR Left arm optics.  MP
		float ytgscale = 0.029;
		float thtgscale = 2.79;
		float phtgscale = 6.734;
		float dpscale = 5.48;
	
		float dpin = tg[3];
		float thin = tg[1];
	
		//adjust for x_offset:
	
	    double DeltaTh = 0.61*x_tg/100.0;
	    double DeltaDp = x_tg/5.18/100.0;
	
	
		dpin -= DeltaDp/3.0;
		thin -= DeltaTh/3.0;
	
	
		vector<double> tg2;
		CalculateTargetCoords(fp, tg2);
		return fabs((tg[0] - tg2[0])*ytgscale) + fabs((thin - tg2[1])*thtgscale) + fabs((tg[2] - tg2[2])*phtgscale) + fabs((dpin - tg2[3])*dpscale);
	};
	
	void CalculateFocalPlaneCoords(const vector<double> TGVar,vector<double> &FPVar, double x_tg = 0.0){
		//use a reduction method to determine closest fp values.
	
		float diff;
		float diff_min = 1e10;
	
		//values in cm and rad for edges of FP coverage to consider.
		float cur_xfpmin = -90.0;
		float cur_xfpmax = 90.0;
		float cur_yfpmin = -7.0;
		float cur_yfpmax = 7.0;
		float cur_thfpmin = -0.05;
		float cur_thfpmax = 0.05;
		float cur_phfpmin = -0.08;
		float cur_phfpmax = 0.08;
	
		//number of iterations to perform.  40 provides a ~ 0.1% accuracy, 50 provides ~0.01% accuracy.
		int nit = 40;
	
		float xfpout = 0.0;
		float yfpout = 0.0;
		float thfpout = 0.0;
		float phfpout = 0.0;
	
		for(int i = 0; i < nit; i++){
			diff_min = 1e10;
			bool xb = false;
			float xvar[2];
			xvar[0] = cur_xfpmin + 0.25*(cur_xfpmax - cur_xfpmin);
			xvar[1] = cur_xfpmax - 0.25*(cur_xfpmax - cur_xfpmin);
		
			bool yb = false;
			float yvar[2];
			yvar[0] = cur_yfpmin + 0.25*(cur_yfpmax - cur_yfpmin);
			yvar[1] = cur_yfpmax - 0.25*(cur_yfpmax - cur_yfpmin);
		
			bool thb = false;
			float thvar[2];
			thvar[0] = cur_thfpmin + 0.25*(cur_thfpmax - cur_thfpmin);
			thvar[1] = cur_thfpmax - 0.25*(cur_thfpmax - cur_thfpmin);
		
			bool phb = false;
			float phvar[2];
			phvar[0] = cur_phfpmin + 0.25*(cur_phfpmax - cur_phfpmin);
			phvar[1] = cur_phfpmax - 0.25*(cur_phfpmax - cur_phfpmin);
		
			//try each possible iteration
			vector<double> fp1;
			vector<double> tg1;
		
			tg1.push_back(TGVar[0]);
			tg1.push_back(TGVar[1]);
			tg1.push_back(TGVar[2]);
			tg1.push_back(TGVar[3]);
		
		
			for(int ii = 0; ii < 2; ii++){
				for(int jj = 0; jj < 2; jj++){
					for(int kk = 0; kk < 2; kk++){
						for(int ll = 0; ll < 2; ll++){
							fp1.clear();
						
							fp1.push_back(xvar[ii]);
							fp1.push_back(yvar[jj]);
							fp1.push_back(thvar[kk]);
							fp1.push_back(phvar[ll]);
							diff = TgtCompDiff(fp1,tg1, x_tg);
		
							if(diff < diff_min){
								xb = ii;
								yb = jj;
								thb = kk;
								phb = ll;
								xfpout = fp1[0];
								yfpout = fp1[1];
								thfpout = fp1[2];
								phfpout = fp1[3];
								diff_min = diff;
							}
						
						}
					}
				}
			}
		
			//here we have a randomness to the reduction for the first 3 events of the iteration.  This eliminates the granularity of a rigid-grid search, but also adds a small width to the uncertainty of the search (~50% variation of the original uncertainty).
			float fac = 1.0;
		
			if(i < 3){
				fac = randOpt->Uniform(0.9,1.1);
			}
		
			//note we use 6 arbitrarily here.  A true binary search (change the 6.0 to 2.0) seems to over constrain.
			float scale = 6.0;
		
			if(!xb){
				cur_xfpmax -= (cur_xfpmax - cur_xfpmin)/scale*fac;
			}else{
				cur_xfpmin += (cur_xfpmax - cur_xfpmin)/scale*fac;
			}
			if(!yb){
				cur_yfpmax -= (cur_yfpmax - cur_yfpmin)/scale*fac;
			}else{
				cur_yfpmin += (cur_yfpmax - cur_yfpmin)/scale*fac;
			}
			if(!thb){
				cur_thfpmax -= (cur_thfpmax - cur_thfpmin)/scale*fac;
			}else{
				cur_thfpmin += (cur_thfpmax - cur_thfpmin)/scale*fac;
			}
			if(!phb){
				cur_phfpmax -= (cur_phfpmax - cur_phfpmin)/scale*fac;
			}else{
				cur_phfpmin += (cur_phfpmax - cur_phfpmin)/scale*fac;
			}
		}
	
	
		double x_fp2 = xfpout/100.0;
		double y_fp2 = yfpout/100.0;
		double th_fp2 = thfpout;
		double ph_fp2 = phfpout;
	
	
		FPVar.push_back(x_fp2*100.0);
		FPVar.push_back(y_fp2*100.0);
		FPVar.push_back(th_fp2);
		FPVar.push_back(ph_fp2);
	};
	void GetRot(const double x, vector<float> &vec){
		//gets the rotation matrix.
		CalculateMatrix(x,fFP);
		vec.push_back(fFP[Y000].GetValue());
		vec.push_back(fFP[T000].GetValue());
		vec.push_back(fFP[P000].GetValue());
		vec.push_back(fFP[T000].GetPoly(0));
	};
	void CalculateMatrix(const double x,vector<MatrixElement>& matrix){
		// calculates the values of the matrix elements for a given location
		// by evaluating a polynomial in x of order it->order with 
		// coefficients given by it->poly

		double arg=0; 
		int size = matrix.size(); 

		if(size>0){
			for( vector<MatrixElement>::iterator it=matrix.begin();
					it!=matrix.end(); it++ ){
				it->SetValue(0.0);
				if( it->GetPolyOrder() > 0){
					for(int i=it->GetPolyOrder()-1; i>=1; i--){
						arg = x*( it->GetValue() + it->GetPoly(i) ); 
						it->SetValue(arg);
					}
					arg = it->GetValue() + it->GetPoly(0);
					it->SetValue(arg);
				}
			}
		}

	};
	//_____________________________________________________________________________
	double CalculateTargetVar(vector<MatrixElement>& matrix,
			const double powers[][5]){
		// calculates the value of a variable at the target
		// the x-dependence is already in the matrix, so only 1-3 (or np) used
		double retval= 0.0;
		double v     = 0.0;
		int size = matrix.size(); 

		if(size>0){
			for( vector<MatrixElement>::iterator it=matrix.begin();
					it!=matrix.end(); it++ ){ 
				if(it->GetValue() != 0.0){
					v = it->GetValue();
					unsigned int np = it->GetExpoSize(); // generalize for extra matrix elems.
					for(unsigned int i=0; i<np; i++){
						v *= powers[it->GetExpo(i)][i+1];
					}
					retval += v;
					//      retval += it->v * powers[it->pw[0]][1] 
					//	              * powers[it->pw[1]][2]
					//	              * powers[it->pw[2]][3];
				}
			}
		}

		return retval;

	};
};