//  SAMC19 is written by Michael Paolone (mpaolone@jlab.org).
//  Some code fragments from previous versions of SAMC were updated and reused, so credit 
//  should also be given to the original authors and the many updaters over the years.
//  If you find this code and believe you deserve some authorship credit, 
//  please write me an email and I can update this section accordingly.
//  Also, if you find this code and make updates that seem useful for generic use, also 
//  me so we can update the original version to be the best it can be for public use.



#include "SAMC19.h"
#define SAMC19_H 1
#include "SAMC19_IO.h"
#include "SAMC19_EvProc.h"


int main(int argc, char* argv[]){
	
	if(argc != 2){
		cout << "Usage: SAMC19 input.xml" << endl;
		return 0;
	}
	
	TString inFile = argv[1];
	

	SAMCVars v;     	//initialize variables
	SAMCInput inp(&v);     //Read from input XML
	inp.LoadFile(inFile);
	
	//check for left or right arm:
	if(v.angle < 0) v.armv = 1;  //negative angle means right arm.
	
	rand3->SetSeed(v.setSeed);
	v.fcauchy = new TF1("fcauchy","TMath::CauchyDist(x,0,0.01)",-5,5);
	//v.fcauchy = new TF1("fcauchy","pow(0.01,2)/(fabs(x) + x*x + pow(0.01,2))",-5,5);
	v.fcauchy->SetNpx(10000);
	
	ifstream inputFile;  //this input file is for an external text file
	bool readInput = 0;
	
	if(strcmp(v.inputFile,"")){
		inputFile.open(v.inputFile);
		if(inputFile.is_open()){
			readInput = 1;
			string lineTemp;
			getline(inputFile,lineTemp); //first line is junk
		}else cout << "Cannot find input file " << v.inputFile << endl;
	}
	
	
	
	//initialize the roor tree variables and the output engine
	SAMCRootVars rvi;
	SAMCOutput outp(&v, &rvi);
	
	//initialize optics
	SAMCOptics optics(v.opticsFile);
	
	//set up multithreading
	unsigned num_cpus = std::thread::hardware_concurrency();
	unsigned num_threads = num_cpus;
	// A mutex ensures orderly access to std::cout from multiple threads.  I also use it to reduce issues with root tree filling.  This probably reduces the multithreading efficiency, and should be checked and updated in future versions.
	std::mutex iomutex;
	std::vector<std::thread> threads(num_threads);
	
	int eventCounter = 0;
	
	for (unsigned i = 0; i < num_threads; ++i) {
		threads[i] = std::thread([&iomutex, i, num_threads, &v, &eventCounter, &outp, &optics, readInput, &inputFile] {
		  
		  
			while(eventCounter < v.numEvents - (int)num_threads + 1 ){
              
				//process event here
			  
				{
				  
					std::lock_guard<std::mutex> iolock(iomutex);
					SAMCRootVars rv;
					rv.IsPassed = 1; //start optimisitic!
					if(readInput){
						string line;
						if(!inputFile.eof()){
							inputFile >> rv.beamx;
							inputFile >> rv.beamy;
							inputFile >> rv.zvert;
							inputFile >> rv.thgen;
							inputFile >> rv.phgen;
							inputFile >> rv.dpgen;
							for(int i = 0; i < v.numWeights; i++){
								float curWeight = 0;
								inputFile >> curWeight;
								rv.weight.push_back(curWeight);
							}
						}else{
							//try not to ovbershoot.
							v.numEvents = eventCounter + (int)num_threads - 2;
							rv.IsPassed = 0;
						}
					 
					}
               	 	//this is where all the event specific stuff happens
					SAMCEvProc ev(&v, &rv, &optics);
					
					//update the root variables, and then fill the root tree
					outp.updateVars(&rv);
					outp.fillTree();
					
					//if(!(eventCounter%10000)) cout << "Processing event " << eventCounter << endl; //a counter every 10k events.
					
					if(!(eventCounter%10000)){
						int percent = TMath::Nint(100.0*float(eventCounter)/float(v.numEvents));
						cout << "Processing event " << eventCounter << "/" << v.numEvents << "  ("<< percent << "%)" << "  \t\r" << flush;
					}
					eventCounter++;
				}
			  
			}
		});
	}
	cout << endl;
	//multithreading
    for (auto& t : threads) {
      t.join();
    }
	//write the file and quit.
	inputFile.close();
	outp.writeToTree();
	
	
	return 0;
	
}