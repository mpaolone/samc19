
#if (!SAMC19_H)
#include "SAMC19.h"
#endif

#include "TVector3.h"
#include "TLorentzVector.h"
#include "SAMC19_SnakeFunc.h"

class SAMCEvProc{
public:
	const SAMCVars *v;
	SAMCOptics *op;
	SAMCRootVars *rv;
	TLorentzVector p;  //the particle vector. still use TLorentzVector, or use Math::LorentzVector()?
	TVector3 vertex;
	//float zvert = 0.0;
	SAMCEvProc(const SAMCVars *vin, SAMCRootVars *rvin, SAMCOptics *opin): v(vin), rv(rvin), op(opin){
		//initializer;
		if(!rv->IsPassed) return; //just bail if already not a passed event
		
		//Generate basic event info:
		
		if(!rv->dpgen) rv->dpgen = rand3->Uniform(-v->dpGenLim, v->dpGenLim);
		if(!rv->thgen) rv->thgen = rand3->Uniform(-v->thGenLim, v->thGenLim);
		if(!rv->phgen) rv->phgen = rand3->Uniform(-v->phGenLim, v->phGenLim);
		
		
		if(!rv->beamx) rv->beamx = rand3->Uniform(-v->bxGenLim, v->bxGenLim) + v->bxCent;
		if(!rv->beamy) rv->beamy = rand3->Uniform(-v->byGenLim, v->byGenLim) + v->byCent;
		
		int vTargSize = (int)v->vTarg.size();
		int curTarg = 0;
		if(vTargSize > 1){
			//we should distribute our vertex based on the interaction probabilty from bt.
			float bttot = 0;
			for(int i = 0; i < vTargSize; i++){
				bttot += v->vTarg[i].bt;
			}
			float randTarg = rand3->Uniform(0,1.0);
			float curbt = 0;
			for(int i = 0; i < vTargSize; i++){
				curbt += v->vTarg[i].bt/bttot;
				if(randTarg < curbt){
					curTarg = i;
					break;
				}
			}
		}
		
		float targZperc = rand3->Uniform(0.0,1.0);  //This is where along the target-z, in percent, we generate the vertex.
		//if we have a target rotation, we should address it now:
		Material targMattemp = v->vTarg[curTarg];  //prepare to alter target properties for a rotated target.
		float Lfac = 1.0;
		
		if(v->vTarg[curTarg].angle){
			//ok, rotated target, lets adjust the z-target "length" accordingly:
			//Lfac = 1.0/TMath::Abs(cos(rv->phgen + v->angle*TMath::DegToRad() - v->vTarg[curTarg].angle*TMath::DegToRad()));
			Lfac = 1.0/TMath::Abs(cos(rv->phgen + v->angle*TMath::DegToRad() - v->vTarg[curTarg].angle*TMath::DegToRad()))/cos(rv->thgen);
			//Lfac = 1.0/TMath::Abs(cos(rv->phgen - v->angle*TMath::DegToRad() + v->vTarg[curTarg].angle*TMath::DegToRad()))/cos(rv->thgen);
			if(Lfac > 10.0){
				Lfac = 10.0;  //keep below 10x increase.
			}
			targMattemp.L *= Lfac;
			targMattemp.T *= Lfac;
			targMattemp.TR *= Lfac;
			targMattemp.bt *= Lfac;
		}
		
		//ok, now we distribute the interaction somewhere inside the target length, which means we get a new "effective" length of target material interaction:
		targMattemp.L *= targZperc;
		targMattemp.T *= targZperc;
		targMattemp.TR *= targZperc;
		targMattemp.bt *= targZperc;
		
		if(!rv->zvert) rv->zvert = (targZperc - 0.5)*v->vTarg[curTarg].L;  //if we don't have an ititial zvert we want, then randomly generate it.
		
		if(v->vTarg[curTarg].angle){
			//also add in vertex location according to beam-x and targ-x offset, if there is an angle.
			rv->zvert *= 1.0/fabs(cos(v->vTarg[curTarg].angle*TMath::DegToRad()));
			rv->zvert -= (rv->beamx + v->vTarg[curTarg].xoff)*tan(v->vTarg[curTarg].angle*TMath::DegToRad());
		}
		rv->zvert += v->vTarg[curTarg].Zloc; //add in z-offset
		
		vertex.SetXYZ(rv->beamx, rv->beamy, rv->zvert);
		vertex.RotateZ(TMath::Pi()/2.0);
		vertex.RotateX(v->angle*TMath::DegToRad());
		vertex.SetX(vertex.X() - v->missPointX);
		vertex.SetY(vertex.Y() - v->missPointY);
		
		//create our initial 4-vector;
		TVector3 angTemp(rv->thgen, rv->phgen, 1.0);
		angTemp.SetMag(v->centP*(1.0 + rv->dpgen));
		p.SetVectM(angTemp, v->mass);
		
		//apply material effects for particle leaving target
		MaterialEffects(&p, &vertex, targMattemp);
	    TransportToPlane0(&vertex,p.Vect());
		//now we want to propogate through each material before the magnet:
		
		for(int i = 0; i < (int)v->vPreMag.size(); i++){
			float Zpos = v->vPreMag[i].Zloc - v->vPreMag[i].L; //Zloc tends to be end of material, so we set to front...  we should fix this 
			//propogate forward to zpos:
			vertex.SetXYZ(vertex.X() + Zpos*p.X()/p.Z(), vertex.Y() + Zpos*p.Y()/p.Z(), Zpos);
			//apply material effects which change angle
			MaterialEffects(&p, &vertex, v->vPreMag[i]);
			//take vertex back to z=0 with new angles from z = Zpos
			TransportToPlane0(&vertex,p.Vect());
		}
		
		
		
		
		//at this point we update our root variables with generated + material effects
		rv->th_tg_ref = p.X()/p.Z();
		rv->ph_tg_ref = p.Y()/p.Z();
		rv->dp_ref = (p.Vect().Mag() - v->centP)/v->centP; 
		rv->y_tg_ref = vertex.Y();
		
		
		//check the q1 entrance:
		float q1x = vertex.X() + 40.0*p.X()/p.Z();
		float q1y = vertex.Y() + 40.0*p.Y()/p.Z();
		
		if(q1x*q1x + q1y*q1y > 14.92*14.92){
		//if(q1x*q1x + q1y*q1y > 18.92*18.92){
			rv->IsPassed = 0;
			return;
		} 
		
		
		//handles forward propogation via snake transport.
		float dpOff = 0;
		//if(v->centP < 450.0) dpOff = -0.0035;
		SAMCSnake snForward(v->centP, vertex.X(), vertex.Y(), p.X()/p.Z(), p.Y()/p.Z(), (p.Vect().Mag() - v->centP)/v->centP + dpOff);
		
		//cout << v->armv << endl;
		
		if(v->armv == 0){
			if(v->transOpt == 1){
				rv->IsPassed = snForward.passAllMags();
			}else{
				rv->IsPassed = snForward.passAllMagsOld_e();
			}
		}else{
			rv->IsPassed = snForward.passAllMagsOld_h();
			//rv->IsPassed = snForward.passAllMagsOld_e();
		}
		
		if(rv->IsPassed != 1) return;  //bail
		
		//cout << "passed!" << endl;
		
		//now we need to define some initial focalplane variables:
		float xfpi = 0;
		float yfpi = 0;
		float thfpi = 0;
		float phfpi = 0;
		
		vector<float> rvals;
		
		if(v->transOpt == 2){
			vector<double> tgv = {rv->y_tg_ref, rv->th_tg_ref, rv->ph_tg_ref, rv->dp_ref};
			vector<double> fpv;
			op->CalculateFocalPlaneCoords(tgv,fpv, vertex.X());
			xfpi = fpv[0];
			yfpi = fpv[1];
			thfpi = fpv[2];
			phfpi = fpv[3];
			op->GetRot(xfpi/100.0, rvals);
		}else{
			if(v->armv == 0){
				
				if(v->transOpt == 1){
					xfpi = snForward.x_fp()*100.0;
					yfpi = snForward.y_fp()*100.0;
					thfpi = snForward.th_fp();
					phfpi = snForward.ph_fp();
				}else{
					xfpi = snForward.x_fp_old_e()*100.0;
					yfpi = snForward.y_fp_old_e()*100.0;
					thfpi = snForward.th_fp_old_e();
					phfpi = snForward.ph_fp_old_e();
				}
				
			}else{
				if(v->transOpt == 1){
					//NOTE: NEW SNAKE optics were only calculated for the LHRS. In the case of the RHRS we use the same optics as the LHRS.
					xfpi = snForward.x_fp()*100.0;
					yfpi = snForward.y_fp()*100.0;
					thfpi = snForward.th_fp();
					phfpi = snForward.ph_fp();
				}else{
					xfpi = snForward.x_fp_old_h()*100.0;
					yfpi = snForward.y_fp_old_h()*100.0;
					thfpi = snForward.th_fp_old_h();
					phfpi = snForward.ph_fp_old_h();
				}
			}
			/*
			rvals.push_back(0.0);
			rvals.push_back(-1.0);
			rvals.push_back(0.0);
			rvals.push_back(-1.0);
			*/
			
			//get just the y000 term, keep others initial values
			op->GetRot(xfpi/100.0, rvals);
			//rvals[1] = -1.0;
			//rvals[2] = 0.0;
			//rvals[3] = -1.0;
			
		}
		
		//for analysis:
		//cout << "rot-mat: " << rvals[0] << "  " << rvals[1] << "  " << rvals[2] << "  " << rvals[3] << endl;
		
		//these are our various expected "detected variables";
		
		double tan_rho = rvals[1];
		double tan_rho0 = rvals[3];
		double cos_rho = 1.0/sqrt(1.0+tan_rho*tan_rho);
		double cos_rho0 = 1.0/sqrt(1.0+tan_rho0*tan_rho0);
		double thd = (thfpi - tan_rho)/(1.0 + thfpi*tan_rho);
		double th_tra = (thd + tan_rho0)/(1.0 - thd*tan_rho0);
		double phd = (phfpi)*(cos_rho - thd*sin(atan(tan_rho))) + rvals[2];
		double ph_tra = phd/(cos_rho0 - thd*sin(atan(tan_rho0)));
	    double y_tra = yfpi + rvals[0]*100.0;
		double xd = xfpi/cos_rho0/(1.0 + th_tra*tan_rho0);
		double yd = y_tra - xd*ph_tra*sin(atan(tan_rho0));
		
		//focal plane "vertex"
		TVector3 fp3(xd, yd, 0.0);
		//just to normalize and set p;
		TVector3 fp3ang(thd,phd,1.0);
		fp3ang.SetMag(p.Vect().Mag());
		
		p.SetXYZM(fp3ang.X(), fp3ang.Y(), fp3ang.Z(), p.M());
		
		//cout << xd << "  ";
		
		for(int i = 0; i < (int)v->vPostMag.size(); i++){
			//float Zextra = -xd/fabs(xd)*sqrt(fabs(xd*xd - xfpi*xfpi)); 
			float Zpos = v->vPostMag[i].Zloc; 
			fp3.SetXYZ(fp3.X() + Zpos*p.X()/p.Z(), fp3.Y() + Zpos*p.Y()/p.Z(), Zpos);
		
			//apply material effects which change angle
			MaterialEffects(&p, &fp3, v->vPostMag[i]);
			//take vertex back to z=0 with new angles from z = Zpos
			TransportToPlane0(&fp3, p.Vect());
		}
		
		
		
		
		if(fabs(fp3.Y() - 10.0*p.Y()/p.Z()) > 3.0){
			//additional frame material:
			//cout << "Frame" << endl;
			
			Material AlFrame;
			AlFrame.L = 0.15;
			AlFrame.Z = 13;
			AlFrame.A = 26.928;
			AlFrame.rho = 2.7;
			AlFrame.T = AlFrame.L*AlFrame.rho;
			AlFrame.X0 = 24.01;
			AlFrame.TR = AlFrame.T/AlFrame.X0;
			AlFrame.bt = 4./3.*AlFrame.TR;
			AlFrame.name = "AlFrame";
			
			
			float Zpos = -10.0;
			fp3.SetXYZ(fp3.X() + Zpos*p.X()/p.Z(), fp3.Y() + Zpos*p.Y()/p.Z(), Zpos);
			MaterialEffects(&p, &fp3, AlFrame);
			TransportToPlane0(&fp3, p.Vect());
		}
		
		//Here is where we probably want the "ref variable" for momentum:
		//rv->dp_ref = (p.Vect().Mag() - v->centP)/v->centP;
		
		  
		/*
		if(v->centP < 450 && v->armv == 0){
			fp3.SetX(fp3.X() + 5.0);
		}
		*/
		
		
		
		xd = fp3.X();
		
		yd = fp3.Y();
		
		//lets apply a wire resolution effect (independent of multiple scattering, based on wire seperation):
		double resx = rand3->Gaus(0.0, 1.75e-2 + v->resOff);
		double resy = rand3->Gaus(0.0, 1.75e-2 + sqrt(2)*(v->resOff));
		//cout << 1.75e-2 + v->resScale << endl;
		
		xd += resx;
		yd += resy;
		
		//attempt to add tails
		
		/*
		double resfacx = 0.5*(3.29559 - 0.00174521*p.Vect().Mag());
		double resfacy = (1.15 - 0.000387*p.Vect().Mag());
		if(v->centP > 200 && v->centP < 450){
			resfacy *= 1.1428571 + (2.5/3.5 - 1.0)/(450.0 - 150.0)*v->centP;
		}
		if(v->centP < 450){
			resfacx*= 1.5;
			resfacy*= 1.5; //ok with double counting factor of above.
			
		}
		if(v->centP > 450 && v->centP < 1260){
			resfacx *= 1.5 - 0.5/(1260.0 - 450.0)*(v->centP - 450.0);
			resfacy *= 1.5 - 0.5/(1260.0 - 450.0)*(v->centP - 450.0);
		}
		if(resfacx < 1) resfacx = 1;
		if(resfacy < 2.0/3.0) resfacy = 2.0/3.0;
		double resx = vin->fcauchy->GetRandom()*resfacx;// + rand3->Uniform(-0.002,0.002);
		double resy = vin->fcauchy->GetRandom()*resfacy;// + rand3->Uniform(-0.002,0.002);
		
		//resx = 0;
		//resy = 0;
		//cout << resx << " " << resy << endl;
		//cout << xd << " " << resx << endl;
		xd += resx;
		yd += resy;
		*/
		/*
		double resfacx = 10.0;
		double resfacy = 10.0;
		double resx = vin->fcauchy->GetRandom()*resfacx;// + rand3->Uniform(-0.002,0.002);
		double resy = vin->fcauchy->GetRandom()*resfacy;// + rand3->Uniform(-0.002,0.002);
		xd += resx;
		yd += resy;
		*/
		
		rv->xd = xd;
		rv->yd = yd;
		
	
		
		//bail if outside the possible x or y detected (note that the additional resolution is optics based)
		//if(fabs(xd - resx) > 105.9 || fabs(yd - resy) > 14.4 ){
		
		
		if(fabs(xd) > 105.9 || fabs(yd) > 14.4 ){
			rv->IsPassed = 0;
			return;
		}
		
		
		//lets add a momentum dependent deflection due to the field inside the chamber.  We'll assume the probabilty follows the shape of the expected deflection, i.e. 1/r^2.  To avoid divergence, we translate to an exponential.
		//fieldDeflect(&p, vertex.Y());
		//now we need to add the air between VDC planes:
		
		Material vdcAir;
	
		vdcAir.L = 33.5;
		vdcAir.Z = 7;
		vdcAir.A = 14.02800;
		vdcAir.rho = 0.00120*1.0;
		vdcAir.T = vdcAir.L*vdcAir.rho;
		vdcAir.X0 = 3.666e+01;
		vdcAir.TR = vdcAir.T/vdcAir.X0;
		vdcAir.bt = 4./3.*vdcAir.TR;
		vdcAir.name = "vdcAir";
		
		fp3.SetXYZ(fp3.X() + vdcAir.L*p.X()/p.Z(), fp3.Y() + vdcAir.L*p.Y()/p.Z(), vdcAir.L);
		MaterialEffects(&p, &fp3, vdcAir);
		
		
		
		
		
		/*
		//rv->thd = p.X()/p.Z();
		//rv->phd = p.Y()/p.Z();
		
		double resx2 = vin->fcauchy->GetRandom()*resfacx;// + rand3->Uniform(-0.002,0.002);
		double resy2 = vin->fcauchy->GetRandom()*resfacy;// + rand3->Uniform(-0.002,0.002);
		
		//bail if outside possible reconstruction
		
		if(fabs(fp3.X() + resx2 - 35.0) > 105.9 || fabs(fp3.Y() + resy2) > 14.4){
			rv->IsPassed = 0;
			return;
		}
		
		
		//resx2 = 0;
		//resy2 = 0;
		*/
		
		double resx2 = rand3->Gaus(0.0, 1.75e-2 + v->resOff);
		double resy2 = rand3->Gaus(0.0, 1.75e-2 + sqrt(2)*(v->resOff));
		
		double thres = atan((resx2 - resx)/(vdcAir.L*sqrt(1.0 + pow(p.X()/p.Z(),2))));
		double phres = atan((resy2 - resy)/(vdcAir.L*sqrt(1.0 + pow(p.Y()/p.Z(),2))));
		
		//for additional cauchy tails...  uncomment above and comment below to restore.
		//double thres = 0;
		//double phres = 0;
		
		
		
		//rv->thd = p.X()/p.Z()*(1 - resx/vdcAir.L);
		//rv->phd = p.Y()/p.Z()*(1 - resy/vdcAir.L);
		
		rv->thd = p.X()/p.Z() + thres;
		rv->phd = p.Y()/p.Z() + phres;
		
		
		rv->ph_fp = (rv->phd - rvals[2])/(cos(atan(tan_rho)) - rv->thd*sin(atan(tan_rho)));
		rv->th_fp = (rv->thd + tan_rho)/(1.0 - rv->thd*tan_rho);
		rv->y_fp = rv->yd - rvals[0]*100.0 + (rv->ph_fp)*rv->xd*sin(atan(tan_rho));
		th_tra = (rv->thd + tan_rho0)/(1.0 - rv->thd*tan_rho0);
		rv->x_fp = rv->xd*cos_rho0*(1.0 + th_tra*tan_rho0);
		
		double a1 = 1.0 - 1.0/sqrt(v->centP);
		double a2 = 0.12 + 1.6/sqrt(v->centP) + 1.6/(v->centP);
		double b1 = 0.98 + 2.0/v->centP + 1000.0/pow(v->centP,2);
		double b2 = - v->centP/8000.0;
		
		double newy = (rv->y_fp/100.0 - a2/b1*rv->ph_fp)/(a1 - a2*b2/b1);
		double newyd = newy + rvals[0] - (rv->ph_fp)*rv->xd*sin(atan(tan_rho))/100.0;
		
		double newph = (rv->ph_fp - b2/a1*rv->y_fp/100.0)/(b1 - a2*b2/a1);
		double newphd = newph*(cos(atan(tan_rho)) - rv->thd*sin(atan(tan_rho))) + rvals[2];
		
		
		//cout << rv->y_fp/100.0 << "  " << newy << endl;
		
		if(v->armv == 0 && v->centP < 450.0 && v->transOpt != 2){
			rv->yd2 = newyd;
			rv->phd2 = newphd;
		}else{
			rv->yd2 = yd/100.0;
			rv->phd2 = phd;
		}
		
		//now we reconstruct to the target!!!!!!!!!
		
		
		if(v->reconOpt == 2){
			vector<double> tgvars;
			vector<double> fpvars = {rv->x_fp, rv->y_fp, rv->th_fp, rv->ph_fp};
			op->CalculateTargetCoords(fpvars, tgvars, vertex.X());
			rv->y_tg_rec = tgvars[0]/100.0;
			rv->th_tg_rec = tgvars[1];
			rv->ph_tg_rec = tgvars[2];
			rv->dp_rec = tgvars[3];
		}else{
			SAMCSnake snRecon(v->centP, rv->x_fp, rv->y_fp, rv->th_fp, rv->ph_fp, vertex.X()/100.0);
			double DeltaTh = 0.61*vertex.X()/100.0;  //note that these are hard-coded values for HRS extended target calculations.
			double DeltaDp = vertex.X()/100.0/5.18;
			
			if(v->armv == 0){
				if(v->reconOpt == 1){
					rv->dp_rec = snRecon.dp_tg()  + DeltaDp;
					rv->th_tg_rec = snRecon.th_tg()  + DeltaTh;
					rv->ph_tg_rec = snRecon.ph_tg();
					rv->y_tg_rec = snRecon.y_tg();
				}else{
					rv->dp_rec = snRecon.dp_tg_old_e() + DeltaDp;
					rv->th_tg_rec = snRecon.th_tg_old_e() + DeltaTh;
					rv->ph_tg_rec = snRecon.ph_tg_old_e();
					rv->y_tg_rec = snRecon.y_tg_old_e();
				}
				
			}else{
				if(v->reconOpt == 1){
					//NOTE:  NEW SNAKE was only calculated for LHRS..  the RHRS is set to be the same with this option.
					rv->dp_rec = snRecon.dp_tg()  + DeltaDp;
					rv->th_tg_rec = snRecon.th_tg()  + DeltaTh;
					rv->ph_tg_rec = snRecon.ph_tg();
					rv->y_tg_rec = snRecon.y_tg();
				}else{
					rv->dp_rec = snRecon.dp_tg_old_h() + DeltaDp;
					rv->th_tg_rec = snRecon.th_tg_old_h() + DeltaTh;
					rv->ph_tg_rec = snRecon.ph_tg_old_h();
					rv->y_tg_rec = snRecon.y_tg_old_h();
				}
			}
			
		}
		
	};
	float MultiScattering(const TLorentzVector *pv, const Material mat){
		//note, we do not change the angle here in p, we return the final scattering angle.
		
		int scatOpt = 3;
		
		//we have some different multiple scattering options, so I want to define them here:
		
		//scatOpt = 1:  Standard gaussian with PDG (Highland) formalism for gaussian width
		//scatOpt = 2;  Lynch-Dahl gaussian
		//scatOpt = 3;  Moliere tails
		//scatOpt = 4;  Lynch-Dahl with "tuned" wide gaussian tails.
		//scatOpt = 5;  Moliere AND "tuned" wide gaussian tails.
		
		//only place pv is used
		double p = pv->Vect().Mag();
		double beta     = pv->Beta();
		float scale = v->resScale;
		//float scale = 1.0;
		//cout << scale << endl;
		
		double sig = 0.0;
		double sigM = 0.0;
		float B = 0.0;
		double Chi_c2 = 0.0;
		double Chi_a2 = 0.0;
		
		if(scatOpt == 1){
			sig = (13.6/beta/p)*sqrt(mat.TR)*(1.+0.038*log(mat.TR));
			sig *= scale;
			float retval = rand3->Gaus(0.0,sig);
			return retval;
		}
		if(scatOpt > 1){
			double Z = mat.Z;
			double A = mat.A;
			double X = mat.T;
			double z = -1.0;
			Chi_c2 = 0.157*Z*(Z+1.0)*X/A*pow(z/p/beta,2);  //not sure of units here...  I think it is 1/cm/Mev so multiply by 5.07e10?
			Chi_a2 = 2.007e-5*pow(Z,2.0/3.0)*(1.0 + 3.34*pow(Z*z/beta/137.0,2))/p/p;  //must be the same units as Chi_c2
			double F = 0.98;
			//double F = 0.95;
	
			double Omega = Chi_c2/Chi_a2;
			double v = 0.5*Omega/(1.0-F);
	
			float b_lil = log(Omega/1.167);
			if(b_lil > 10.0){
				B = -2.71204 + 0.97956*b_lil;
			}else if(b_lil > 1.0){
				B = 0.966025 -0.169098*b_lil + 0.213153*b_lil*b_lil -0.0200987*b_lil*b_lil*b_lil + 0.000721211*b_lil*b_lil*b_lil*b_lil;
			}else{
				B = 1.0;
			}
			
			sig = sqrt(Chi_c2/(1.0 + F*F)*((1.0 + v)/v*log(1.0 + v) - 1.0));
			sig *= scale;
			
			//sqrt2 form that seems to work best
			//B *= 1.0/sqrt(2.0);
			//sig = sqrt(Chi_c2/(1.0 + F*F)*((1.0 + v)/v*log(1.0 + v) - 1.0))*sqrt(2.0);
			
			//B *= 1.0/sqrt(3.1415/2.0);
			//sig = sqrt(Chi_c2/(1.0 + F*F)*((1.0 + v)/v*log(1.0 + v) - 1.0))*sqrt(3.1415/2.0);
			
			if(scatOpt == 2)
				return rand3->Gaus(0.0, sig);
		}
		
		if(scatOpt == 3 || scatOpt == 5){
			//sig *= 1.3;
			double sigr =  (3.0435 -0.553706*B + 0.038984*B*B);
			double signew = sig/sigr;
			double ampr = 0.53768*sigr -0.463712;
	
			//this is the transition value, from 2 gaussians into power function.  We need to calculate 2 integrals
			double xtrans =  (1.0/log(B)*4.9)*signew;
			double powc =   B*(2.11052 + -0.313557*B + 0.0156546*B*B);
	
	
			double b = (TMath::Gaus(xtrans,0.0,signew,0.0) + ampr*TMath::Gaus(xtrans,0.0,2.0*signew,0.0))*pow(xtrans,powc);
	
			//combined gaussian up to xtrans
			double integral1 = sqrt(TMath::Pi()/2.0)*signew*(2.0*ampr*TMath::Erf(xtrans/(2.0*sqrt(2.0)*signew)) + TMath::Erf(xtrans/sqrt(2.0)/signew));
	
			//power function integral from xtrans to infinity
			double integral2 = b/(powc -1.0)*pow(xtrans,1.0 - powc);
	
			//wider gaussian integral, to determine when to throw according to this gaussian.
			double integral3 = sqrt(TMath::Pi()/2.0)*signew*(2.0*ampr*TMath::Erf(xtrans/(2.0*sqrt(2.0)*signew)));
	
	
			//we choose which section to use based on which fraction we get:
			double val = integral1/(integral1 + integral2);


			double sigv = signew;	
			double xinv, xinv_min, xinv_max, ymin, ymax, yval;
			float retval = 0.0;
			
			if(rand3->Uniform(0.0,1.0) < val){
				//use gaussians
				//first choose a gaussian:
				if(rand3->Uniform(0.0, integral1) < integral3){
					//second gaussian:
					sigv = 2.0*signew;
				}
				retval = 2.0*xtrans;
				//keep trying until we get one in the rande we want:
				while(fabs(retval) > xtrans){
					retval = rand3->Gaus(0.0,sigv);
				}
			}else{
		
				//if the pdf is b/x^c/I  then the CDF is b/(I*(c-1))*x^(1-c).  The inverse cdf is then pow((x*(c-1)/b/I),(1/(1-c)))
		
				ymin = 0.0;
				ymax = b/(integral2*(powc-1.0))*pow(xtrans,(1.0-powc));
				yval = rand3->Uniform(ymin,ymax);
				retval = pow((yval*(powc-1.0)/b*integral2),(1.0/(1.0-powc)));
		
				if(rand3->Uniform(0.0,1.0) > 0.5){
					retval = -retval;
				}
			}
			
			//retval *= scale;
			
			sigM = retval;
			if(scatOpt == 3){
				return retval;
			}else{
				sig = sigM;
			}
			
		}
		if(scatOpt == 4 || scatOpt == 5){
			
			
			/*
			float fac = 1.1;  //This factor adjuss the total extra tail width of the wide gaussian.
			//sig *= 1.25; 
			
			float norm = 0.155619/(fac + 0.444642) + 1.0 - exp(-fac*fac)/10.0; //empirical fit to normalize the gausian to have an "identical" width below the defined width (good to 1% or 2%). Good between fac = 1.0 to fac = 20.0.  Not good below 1.0;
	
			sig = sig/norm;
			float a = rand3->Gaus(0.0,sig);
			while(fabs(a) > sig){
				 sig = fac*fabs(a);
				 a = rand3->Gaus(0.0,sig);
			}
	
			return rand3->Gaus(0.0,sig);
			*/
			
			/*
			if(scale >= 1){
				sig += v->fcauchy->GetRandom()*sig*(scale - 1.0)*100.0;
				//if(sig > 0) sig += fabs(v->fcauchy->GetRandom()*sig*(scale - 1.0)*100.0);
			}
			//cout << sig << "  " << 1.0/(sig*(scale - 1.0)) << endl;
			//if(scale >= 1) sig *= 1.0/pow(rand3->Uniform(0.001,1.0),2);
			*/
			
			
			double powc = 2.0;
			if(scale > 1){
				double yval = rand3->Uniform(0,1);
				double sign = 1;
				if(sig < 0) sign = -1.0;
				sig += sign*pow( yval*(powc-1.0), 1.0/(1.0-powc))*sig*(scale - 1.0);
				//cout << sig << endl;
			}
			
			
			/*
			double p = rand3->Uniform(0,1);
			if(p < 0.1){
				sig += rand3->Gaus(0, 1000.0*sig);
			}
			*/
			return sig;
			
		}
		
		
		
		//if no option is selected, then: 
		return 0;
	};
	float Bremss_Loss_Exact(float E,float bt){
		// exact form for Bremsstrahlung energy loss 
		// uses the accept/reject method to sample the real distribution 
		// exact function:      f(x) = (bt/E)*(1 + 0.5772*bt)*x^{bt-1}*(1 - x + (3/4)*x*x), x = (Delta E)/E 
		// test function:       g(x) = bt*x^{bt-1}                                                                 
		// comparison function: h(x) = f(x)/g(x)
		// maximum of h:        c    = (1/E)*(1 + 0.5772*bt)                                             
		// MC function:         H(x) = h(x)/c = 1 - x + (3/4)*x*x 

	    double X=0,U=0,H=0; 

	    do{
			X = pow(rand3->Uniform(0.0,1.0),1./bt);   // random variable X according to g(x) on [0,1]  
			U = rand3->Uniform(0.0,1.0);  // random variable according to uniform distribution on [0,1]  
			H = 1.0 - X + 3.0/4.0*X*X; 
		}while(U>H); 
    
		// we found the random variable X, distributed according to the exact function 
		// on 0 < X < 1.  Now, since it's equal to (Delta E)/E, we multiply by the 
		// energy E
		double result = E*X;
		return result;
	};
	float Ion_Loss(const TLorentzVector *pv, const Material mat){
		
		//even though this is programmed for any mass particle, its really only good for electrons, I think.
		
		//double lA       = mat.A;                              // [-]
		//double lZ       = mat.Z;                              // [-]
		//double lrho     = mat.rho;                            // density of absorber [g/cm^3] 
		//double lT       = mat.T;                              // thickness of material [g/cm^2] 
		double lK       = 0.307075;                                 // MeV*cm^2/g    RIGHT 
		double lbetasq = pow(pv->Beta(),2);
		//cout << lbetasq << "  " << pv->Vect().Mag() << endl;
		double lxi=0.,lhbarw=0.,lhbarwsq=0.;
	
	
		if(mat.A!=0 && lbetasq!=0 ){
			lxi = (lK/2.)*(mat.Z/mat.A)*(mat.T/lbetasq);                    // (MeV*cm^2/g)*(mol/g)*(g/cm^2) = MeV*mol (?)   
		}
		if(mat.A!=0){
			lhbarw   = 28.816*sqrt(mat.rho*(mat.Z/mat.A) )*1e-6;            // MeV
			lhbarwsq = lhbarw*lhbarw;                               // MeV^2
			// lhbarwsq = 28.816*28.816*lrho*(lZ/lA)*1e-12;            // MeV^2 
		}
		double j        = 0.200;
		double num      = 2.*(pv->M())*lxi;                     // MeV^2
		double den      = lhbarwsq;                                 // MeV^2  
		double Delta_p=0.; 
		if(den!=0){
			Delta_p  = lxi*(log(num/den)+j);
		}
		// double lw       = 4*lxi;
		double lw       = lxi;                                      // rand3->Landau will use 4 times this value for the width!  Don't need the 4... 
		double result   = 0;

		if (Delta_p && lw) result = rand3->Landau(Delta_p,lw);
		
		//lost all momentum:
		if(result > pv->Vect().Mag()) result = pv->Vect().Mag();
		//lost no momentum
		if(result < 0) result = 0.0;
		
		return result;
	};
	// leave as a function, since we call it a lot.
	void MaterialEffects(TLorentzVector *k, TVector3 *v3, const Material mat){
		//apply material effects to track
		
        float tempTh = atan(k->X()/k->Z());
		float tempPh = atan(k->Y()/k->Z());
		
		float tempX = v3->X();
		float tempY = v3->Y();
		
        TVector3 tempv;
		
		//lets adust the lengths by the angle!
		
		float tempPolTh = sqrt(tempTh*tempTh + tempPh*tempPh);
		
		Material matTemp = mat;
		
		//in the case we have a rotated target, we have already applied a length corretion due to particle trajectory.
		if(!matTemp.angle){
			matTemp.L= mat.L/TMath::Abs(cos(tempPolTh));
			matTemp.T= mat.T/TMath::Abs(cos(tempPolTh));
			matTemp.TR= mat.TR/TMath::Abs(cos(tempPolTh));
			matTemp.bt= mat.bt/TMath::Abs(cos(tempPolTh));
		}
		
		float scatTh = 0;
		float scatPh = 0;
		float offsX = 0;
		float offsY = 0;
		
		if(v->IsMultiScat){
			//tempTh += MultiScattering(k,matTemp);
			//tempPh += MultiScattering(k,matTemp);
			
			scatTh = MultiScattering(k,matTemp)*(1.0 + v->fcauchy->GetRandom()*25.);
			scatPh = MultiScattering(k,matTemp)*(1.0 + v->fcauchy->GetRandom()*25.);
			offsX = MultiScattering(k,matTemp)*(1.0 + v->fcauchy->GetRandom()*25.);
			offsY = MultiScattering(k,matTemp)*(1.0 + v->fcauchy->GetRandom()*25.);
			
			/*
			scatTh = MultiScattering(k,matTemp);
			scatPh = MultiScattering(k,matTemp);
			offsX = MultiScattering(k,matTemp);
			offsY = MultiScattering(k,matTemp);
			*/
			
			tempX += matTemp.L*(-scatTh/2.0 + offsX/sqrt(12.0)); //see PGD, note that the negative is because we are at the front of the scattering material.
			tempY += matTemp.L*(-scatPh/2.0 + offsY/sqrt(12.0));
		}
		
		tempTh += scatTh;
		tempPh += scatPh;
		
		
        tempv.SetXYZ(tan(tempTh), tan(tempPh), 1.0);
        tempv.SetMag(1.0);
		
		float Eloss_Brem = 0.0;
		float Eloss_Ion = 0.0;
		
		if(v->IsEnergyLoss){
			Eloss_Brem = Bremss_Loss_Exact(k->E(),matTemp.bt);
			Eloss_Ion = Ion_Loss(k,matTemp);
		}
		//Eloss_Ion = 0.0;
		//Eloss_Brem = 0.0;
		//cout << k->E() << "  " << Eloss_Brem << "  " << Eloss_Ion << endl;
		double curE = k->E() - Eloss_Brem - Eloss_Ion;
		double curP = sqrt(curE*curE - pow(v->mass,2));
		//cout << curP << endl;
        k->SetXYZT(tempv.X()*curP, tempv.Y()*curP, tempv.Z()*curP , curE);         //set vector with beam energy and new theta/phi 
		v3->SetXYZ(tempX,tempY, v3->Z());
		
	};
	void fieldDeflect(TLorentzVector *k, float y){
        float tempTh = atan(k->X()/k->Z());
		float tempPh = atan(k->Y()/k->Z());
        TVector3 tempv;
		
		double def = 0;
		int n = 0;
		while(n < 1000){ //bail if stuck over 1000
			double testdef = rand3->Uniform(0.0,0.1);
			double testp = rand3->Uniform(0.0,1.0);
			double func = 0.99*exp(-450.0*testdef*(1.0 - 0.4*y)) + 0.01*exp(-50*testdef); //this is emprical to HRS of hall-A
			if(testp < func){
				def = testdef;
				break;
			}
			n++;
		}
		
		double defrot = rand3->Uniform(0.0,2.0*TMath::Pi());
		double defx = cos(defrot)*def;
		double defy = sin(defrot)*def;
		
		tempv.SetXYZ(tan(tempTh + defx), tan(tempPh + defy), 1.0);
		tempv.SetMag(1.0);
		double kp = k->Vect().Mag();
		double kM = k->M();
		k->SetXYZM(tempv.X()*kp, tempv.Y()*kp, tempv.Z()*kp , kM);	
	};
	//leave as a function, since it is used a lot;
	void TransportToPlane0(TVector3 *vert, const TVector3 ang){
        float zdist = -vert->Z();
        float x,y;
		x = vert->X() + zdist*ang.X()/ang.Z();  //should be same as above
		y = vert->Y() + zdist*ang.Y()/ang.Z(); 
        vert->SetXYZ(x,y,0.0);
	};
};
